<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kependudukan
{
    public function validasi($nik)
    {
        $CI = &get_instance();
        $CI->load->database();

   
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => '192.168.1.205/feeder200?nik=' . $nik,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}
