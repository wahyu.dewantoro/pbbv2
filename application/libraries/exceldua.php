<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once("./vendor/autoload.php");


use Box\Spout\Common\Entity\Style\Border;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Exceldua
{
    public function write($title, $header, $data)
    {
        $path = "storage/berkas";
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $writer = WriterEntityFactory::createXLSXWriter();
        $filename = $path . '/' . $title . '.xlsx';
        // $writer->openToFile($filename);
        $writer->openToBrowser($title . '.xlsx');



        $style = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(11)
            ->setFontColor(Color::WHITE)
            ->setShouldWrapText(true)
            ->setBackgroundColor(Color::GREEN)
            ->build();

        // header
        $headere = WriterEntityFactory::createRowFromArray($header, $style);
        $writer->addRow($headere);


        $rows = [];
        foreach ($data as $i) {
            $cell = [];
            foreach ($i as $key => $ia) {
                $cell[] = WriterEntityFactory::createCell($i[$key]);
            }

            $rows[] = WriterEntityFactory::createRow($cell);
        }

        $writer->addRows($rows);
        $writer->close();

        return $filename;
    }

    public function reader($filePath, $index = 0, $getCell = [])
    {

        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($filePath);
        //$reader = ReaderEntityFactory::createReaderFromFile($filePath);
        $baris = [];
        foreach ($reader->getSheetIterator() as $sheet) {
            $no = 0;
            foreach ($sheet->getRowIterator() as $row) {
                $cells = $row->getCells();
                $isi = [];
                for ($i = 0; $i < count($cells); $i++) {
                    if (empty($getCell)) {
                        $isi[] = $cells[$i]->getValue();
                    } else {
                        foreach ($getCell as $a) {
                            if ($i == $a) {
                                $isi[] = $cells[$i]->getValue();
                            }
                        }
                        // break;
                    }
                }
                if ($no >= $index) {
                    $baris[] = $isi;
                }
                $no++;
            }
        }
        $reader->close();
        return $baris;
    }
}
