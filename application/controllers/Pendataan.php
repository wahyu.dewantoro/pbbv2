<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pendataan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);

        // $this->load->model('Mbank');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function formulir()
    {
        return view('pendataan/formulir');
    }

    function json_formulir_spop_lspop()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("NO_FORMULIR FORMULIR_SPOP, NO_FORMULIR_LSPOP FORMULIR_LSPOP,
        TRANSAKSI, THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN , (KD_PROPINSI ||
        KD_DATI2 || KD_KECAMATAN || KD_KELURAHAN || KD_BLOK || NO_URUT || KD_JNS_OP) AS NPWPD");
        $this->datatables->from('FORMULIR_SPOP_LSPOP');
        $this->datatables->join("FORMAT_FORMULIR B", "B.ID=FORMULIR_SPOP_LSPOP.FORMAT_FORMULIR_ID", "LEFT");
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }

    public function formuliradd()
    {
        $compact = [
            'jenis' => $this->db->get('FORMAT_FORMULIR')->result()
        ];
        return view('pendataan/formulir_form', $compact);
    }

    public function getNopNopel()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        // cek jenis penelitian
        $formulir_id = $this->input->get('formulir_id', true);
        $nomer_pelayanan = $this->input->get('nomer_pelayanan');

        $kode = "";
        $res = "";
        $msg = "";

        $exp = explode('.', $nomer_pelayanan);
        $thn_pelayanan = $exp[0];
        $bundel_pelayanan = $exp[1];
        $no_urut = $exp[2];
        // cek ke verifikasi berkas dulu
        $cek = $this->db->query("select pelimpahan from pelayanan_verifikasi 
                            where thn_pelayanan=? and bundel_pelayanan=? and no_urut_pelayanan=? ", [$thn_pelayanan, $bundel_pelayanan, $no_urut])->row();

        if ($cek) {
            if (substr($formulir_id, 0, 1) == '1') {
                // penelitian lapangan
                $cl = $this->db->query("SELECT SURAT_TUGAS_VERLAP.KD_PROPINSI,
                SURAT_TUGAS_VERLAP.KD_DATI2,
                SURAT_TUGAS_VERLAP.KD_KECAMATAN,
                SURAT_TUGAS_VERLAP.KD_KELURAHAN,
                SURAT_TUGAS_VERLAP.KD_BLOK,
                SURAT_TUGAS_VERLAP.NO_URUT,
                SURAT_TUGAS_VERLAP.KD_JNS_OP,
                SURAT_TUGAS_OBJEK.THN_PELAYANAN,
                SURAT_TUGAS_OBJEK.BUNDEL_PELAYANAN,
                SURAT_TUGAS_OBJEK.NO_URUT_PELAYANAN,
                NO_FORMULIR
           FROM SURAT_TUGAS_VERLAP
                JOIN SURAT_TUGAS_OBJEK
                   ON SURAT_TUGAS_OBJEK.ID = SURAT_TUGAS_VERLAP.SURAT_TUGAS_OBJEK_ID
                LEFT JOIN FORMULIR_SPOP_LSPOP SL
                   ON     SL.THN_PELAYANAN = SURAT_TUGAS_OBJEK.THN_PELAYANAN
                      AND SL.BUNDEL_PELAYANAN = SURAT_TUGAS_OBJEK.BUNDEL_PELAYANAN
                      AND SL.NO_URUT_PELAYANAN = SURAT_TUGAS_OBJEK.NO_URUT_PELAYANAN
                      AND SL.KD_PROPINSI = SURAT_TUGAS_VERLAP.KD_PROPINSI
                      AND SL.KD_DATI2 = SURAT_TUGAS_VERLAP.KD_DATI2
                      AND SL.KD_KECAMATAN = SURAT_TUGAS_VERLAP.KD_KECAMATAN
                      AND SL.KD_KELURAHAN = SURAT_TUGAS_VERLAP.KD_KELURAHAN
                      AND SL.KD_BLOK = SURAT_TUGAS_VERLAP.KD_BLOK
                      AND SL.NO_URUT = SURAT_TUGAS_VERLAP.NO_URUT
                      AND SL.KD_JNS_OP = SURAT_TUGAS_VERLAP.KD_JNS_OP
          WHERE     SURAT_TUGAS_OBJEK.THN_PELAYANAN = ?
                AND SURAT_TUGAS_OBJEK.BUNDEL_PELAYANAN = ?
                AND SURAT_TUGAS_OBJEK.NO_URUT_PELAYANAN = ?", [$thn_pelayanan, $bundel_pelayanan, $no_urut])->result_array();
                if ($cl) {
                    $kode = 1;
                    $res = $cl;
                    $msg = "nomer pelayanan terdeteksi";
                } else {
                    $kode = 0;
                    $res = "";
                    $msg = "nomer pelayanan  tidak terdeteksi di penelitian lapangan";
                }
            } else {
                // penelitian kantor
                $cl = $this->db->query("SELECT kd_propinsi_pemohon kd_propinsi,
                kd_dati2_pemohon kd_dati2,
                kd_kecamatan_pemohon kd_kecamatan,
                kd_kelurahan_pemohon kd_kelurahan,
                kd_blok_pemohon kd_blok,
                no_urut_pemohon no_urut,
                kd_jns_op_pemohon kd_jns_op,
                pst_detail.thn_pelayanan,
                pst_detail.bundel_pelayanan,
                pst_detail.no_urut_pelayanan,
                no_formulir
        FROM pst_detail
                JOIN pelayanan_verifikasi
                ON     pelayanan_verifikasi.thn_pelayanan =
                            pst_detail.thn_pelayanan
                    AND pelayanan_verifikasi.bundel_pelayanan =
                            pst_detail.bundel_pelayanan
                    AND pelayanan_verifikasi.no_urut_pelayanan =
                            pst_detail.no_urut_pelayanan
                    LEFT JOIN FORMULIR_SPOP_LSPOP SL
ON  SL.THN_PELAYANAN = pst_detail.THN_PELAYANAN
AND SL.BUNDEL_PELAYANAN = pst_detail.BUNDEL_PELAYANAN
AND SL.NO_URUT_PELAYANAN = pst_detail.NO_URUT_PELAYANAN
AND SL.KD_PROPINSI = pst_detail.KD_PROPINSI_pemohon
AND SL.KD_DATI2 = pst_detail.KD_DATI2_pemohon
AND SL.KD_KECAMATAN = pst_detail.KD_KECAMATAN_pemohon
AND SL.KD_KELURAHAN = pst_detail.KD_KELURAHAN_pemohon
AND SL.KD_BLOK = pst_detail.KD_BLOK_pemohon
AND SL.NO_URUT = pst_detail.NO_URUT_pemohon
AND SL.KD_JNS_OP = pst_detail.KD_JNS_OP_pemohon
                                        WHERE pelimpahan = 2 
                                        and pelayanan_verifikasi.thn_pelayanan=?
                                        and pelayanan_verifikasi.bundel_pelayanan=?
                                        and pelayanan_verifikasi.no_urut_pelayanan=?", [$thn_pelayanan, $bundel_pelayanan, $no_urut])->result_array();
                if ($cl) {
                    $kode = 1;
                    $res = $cl;
                    $msg = "nomer pelayanan terdeteksi";
                } else {
                    $kode = 0;
                    $res = "";
                    $msg = "nomer pelayanan tidak terdeteksi di penelitian kantor";
                }
            }
        } else {
            $kode = 0;
            $res = "";
            $msg = "Nomer pelayanan tidak terdaftar / belum di verifikasi";
        }

        header('Content-Type: application/json');
        $data = [
            'kode' => $kode,
            'hasil' => $res,
            'pesan' => $msg
        ];
        echo json_encode($data);
    }

    public function cekformulir()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }

        if ($this->input->get('jenis') == 1) {
            $this->db->where('NO_FORMULIR', $this->input->get('formulir'));
        } else {
            $this->db->where('NO_FORMULIR_LSPOP', $this->input->get('formulir'));
        }
        $cek = $this->db->get('FORMULIR_SPOP_LSPOP')->row();
        if ($cek) {
            $this->db->where('NO_FORMULIR', $this->input->get('formulir'));
            $cekentri = $this->db->get('TBL_SPOP')->row();
            if ($cekentri) {
                $kode = 2;
                $nop = "";
            } else {
                $nop = $cek->KD_PROPINSI . '.' .
                    $cek->KD_DATI2 . '.' .
                    $cek->KD_KECAMATAN . '.' .
                    $cek->KD_KELURAHAN . '.' .
                    $cek->KD_BLOK . '.' .
                    $cek->NO_URUT . '.' .
                    $cek->KD_JNS_OP;
                $kode = 1;
            }
        } else {
            $kode = 0;
            $nop = "";
        }


        header('Content-Type: application/json');
        $data = [
            'kode' => $kode,
            'nop' => $nop
        ];
        echo json_encode($data);
    }

    public function prosesformulir()
    {
        // dd($this->input->post());

        $nop = $this->input->post('nop');
        $this->db->trans_start();
        foreach ($nop as $op) {
            $vid = $this->input->post('format_formulir_id');
            $nf = $this->db->query("SELECT GET_FORMULIR($vid) SPOP, SUBSTR(GET_FORMULIR($vid),1,8) || LPAD(SUBSTR(GET_FORMULIR($vid),9,3)+1,3,0) LSPOP FROM DUAL")->row();
            $exp = explode('.', $op);

            $exl = explode('.', $this->input->post('nomer_pelayanan'));
            $insert = [
                'NO_FORMULIR' => $nf->SPOP,
                'NO_FORMULIR_LSPOP' => $nf->LSPOP,
                'FORMAT_FORMULIR_ID' => $vid,
                'KD_PROPINSI' => $exp[0],
                'KD_DATI2' => $exp[1],
                'KD_KECAMATAN' => $exp[2],
                'KD_KELURAHAN' => $exp[3],
                'KD_BLOK' => $exp[4],
                'NO_URUT' => $exp[5],
                'KD_JNS_OP' => $exp[6],
                'THN_PELAYANAN' => $exl[0],
                'BUNDEL_PELAYANAN' => $exl[1],
                'NO_URUT_PELAYANAN' => $exl[2],
                'CREATED_BY' => $this->session->userdata('nip'),
            ];
            $this->db->set('CREATED_AT', 'sysdate', false);
            $this->db->insert('FORMULIR_SPOP_LSPOP', $insert);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('pendataan/formulir');
    }

    public function prosessistep()
    {
        // dd($this->input->post());
        
        $nop = $this->input->post('nop_proses');
        $this->db->trans_start();
        foreach ($nop as $op) {
            $vid = $this->input->post('format_formulir_id');
            $nf = $this->db->query("SELECT GET_FORMULIR($vid) SPOP, SUBSTR(GET_FORMULIR($vid),1,8) || LPAD(SUBSTR(GET_FORMULIR($vid),9,3)+1,3,0) LSPOP FROM DUAL")->row();
            $exp = explode('.', $op);

            $insert = [
                'NO_FORMULIR' => $nf->SPOP,
                'NO_FORMULIR_LSPOP' => $nf->LSPOP,
                'FORMAT_FORMULIR_ID' => $vid,
                'KD_PROPINSI' => $exp[0],
                'KD_DATI2' => $exp[1],
                'KD_KECAMATAN' => $exp[2],
                'KD_KELURAHAN' => $exp[3],
                'KD_BLOK' => $exp[4],
                'NO_URUT' => $exp[5],
                'KD_JNS_OP' => $exp[6],
                'CREATED_BY' => $this->session->userdata('nip'),
            ];
            $this->db->set('CREATED_AT', 'sysdate', false);
            $this->db->insert('FORMULIR_SPOP_LSPOP', $insert);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('pendataan/formulir');
    }

    public function json()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo $this->Mbank->json();
    }

    public function spop()
    {
        return view('pendataan/index_spop');
    }

    function json_spop()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("NO_FORMULIR, NOP, TRANSAKSI, NM_WP");
        $this->datatables->from('V_SPOP');
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }


    public function spopadd()
    {

        $jr = array(
            '1' => 'PEREKAMAN DATA OP',
            '2' => 'PEMUTAKHIRAN DATA OP',
            '3' => 'PENGAHPUSAN OP',
            // '4' => 'PENGHAPUSAN STATUS OP BERSAMA'
        );
        $jenis_tanah = array(
            '1' => 'TANAH + BANGUNAN',
            '2' => 'KAVLING SIAP BANGUN',
            '3' => 'TANAH KOSONG',
            '4' => 'FASILITAS UMUM',
            '5' => 'LAIN-LAIN',
        );

        $status_wp = array(
            '1' => 'PEMILIK',
            '2' => 'PENYEWA',
            '3' => 'PENGELOLA',
            '4' => 'PEMAKAI',
            '5' => 'SENGKETA',
        );

        $pekerjaan = array(
            // '0' => 'LAINNYA',
            '1' => 'PNS',
            '2' => 'ABRI',
            '3' => 'PENSIUNAN',
            '4' => 'BADAN',
            '5' => 'LAINNYA'
        );
        $data = [
            'jt' => $jr,
            'tanah' => $jenis_tanah,
            'wp' => $status_wp,
            'pekerjaan' => $pekerjaan
        ];
        return view('pendataan/form_spop', $data);
    }

    public function spopskword($no_formulir, $nop)
    {
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kec = substr($nop, 4, 3);
        $kd_kel = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        $thn = date('Y');

        $q_formulir = $this->db->query("SELECT A.*, B.TRANSAKSI FROM FORMULIR_SPOP_LSPOP A LEFT JOIN 
        FORMAT_FORMULIR B ON A.FORMAT_FORMULIR_ID=B.ID
        WHERE A.NO_FORMULIR='$no_formulir'");

        $q_spop = $this->db->query("SELECT * FROM TBL_SPOP WHERE NO_FORMULIR='$no_formulir'
        AND NOP_PROSES='$nop'");

        $q_wp = $this->db->query("SELECT A.*, B.NM_KECAMATAN, C.NM_KELURAHAN FROM SPPT A
        JOIN REF_KECAMATAN B ON A.KD_KECAMATAN = B.KD_KECAMATAN
        JOIN REF_KELURAHAN C ON A.KD_KECAMATAN = C.KD_KECAMATAN AND A.KD_KELURAHAN = C.KD_KELURAHAN
         WHERE A.KD_PROPINSI = '$kd_propinsi' AND
                A.KD_DATI2 = '$kd_dati2' AND
                A.KD_KECAMATAN = '$kd_kec' AND
                A.KD_KELURAHAN = '$kd_kel' AND
                A.KD_BLOK = '$kd_blok' AND
                A.NO_URUT = '$no_urut' AND
                A.KD_JNS_OP = '$kd_jns_op'
                AND A.THN_PAJAK_SPPT='$thn'");

        $s_formulir = $q_formulir->row();

        $s_spop = $q_spop->row();

        $s_wp = $q_wp->row();

        if(count($q_formulir->result())>0){
            $nopel = $s_formulir->THN_PELAYANAN.'.'.$s_formulir->BUNDEL_PELAYANAN.'.'.$s_formulir->NO_URUT_PELAYANAN;
            $data['nopel'] = str_replace(' ', '', $nopel);
            $data['nm_wp'] = $s_spop->NM_WP;
            $data['kel_wp'] = $s_spop->KELURAHAN_WP;
            $data['tgl_pendataan'] = date_indo(date('Y-m-d', strtotime($s_spop->TGL_PENDATAAN_OP)));
            $data['wp'] = $s_wp;

            // $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='MUTASI OBJEK/SUBJEK'")->row();
            // $data['sk'] = $sk;
            // return view('pendataan/sk/mutasi/gabung/sk_word', $data);
            // exit();
            //pembetulan - penelitian 
            if($s_formulir->FORMAT_FORMULIR_ID=='27'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PEMBETULAN SPPT/SKP/STP'")->row();
                $data['sk'] = $sk;
                return view('pendataan/sk/pembetulan/sk_word', $data);
            }
            //data baru
            
            else if($s_formulir->FORMAT_FORMULIR_ID=='11'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PENDAFTARAN DATA BARU' AND JABATAN='Kepala Bidang PBB P2'")->row();
                $sk2 = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PENDAFTARAN DATA BARU' AND JABATAN='Kasubid Pelayanan'")->row();
                $data['sk'] = $sk;
                $data['sk2'] = $sk2;
                return view('pendataan/sk/baru/sk_word', $data);
            }
            //mutasi gabung
            else if($s_formulir->FORMAT_FORMULIR_ID=='13'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='MUTASI OBJEK/SUBJEK'")->row();
                $data['sk'] = $sk;
                return view('pendataan/sk/mutasi/gabung/sk_word', $data);
            }

            else {
                $transaksi ='';
                if(substr($s_formulir->FORMAT_FORMULIR_ID,0,1)=='1'){
                    $transaksi = $s_formulir->TRANSAKSI.' (Penelitian Lapangan)';
                }else if(substr($s_formulir->FORMAT_FORMULIR_ID,0,1)=='2'){
                    $transaksi = $s_formulir->TRANSAKSI.' (Penelitian Kantor)';
                }
                $this->session->set_flashdata('pesan', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Generate SK Untuk Jenis Formulir <b>'.$transaksi.'</b> Ini Belum Tersedia !</div>');
                redirect('pendataan/spop');
            }
        }else{
            $this->session->set_flashdata('pesan', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Generate SK Gagal !</div>');
            redirect('pendataan/spop');
        }
        
    }

    public function spopskexcel($no_formulir, $nop){
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kec = substr($nop, 4, 3);
        $kd_kel = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        $thn = date('Y');

        $q_formulir = $this->db->query("SELECT A.*, B.TRANSAKSI FROM FORMULIR_SPOP_LSPOP A LEFT JOIN 
        FORMAT_FORMULIR B ON A.FORMAT_FORMULIR_ID=B.ID
        WHERE A.NO_FORMULIR='$no_formulir'");

        $q_spop = $this->db->query("SELECT * FROM TBL_SPOP WHERE NO_FORMULIR='$no_formulir'
        AND NOP_PROSES='$nop'");

        $q_wp = $this->db->query("SELECT A.*, B.NM_KECAMATAN, C.NM_KELURAHAN FROM SPPT A
        JOIN REF_KECAMATAN B ON A.KD_KECAMATAN = B.KD_KECAMATAN
        JOIN REF_KELURAHAN C ON A.KD_KECAMATAN = C.KD_KECAMATAN AND A.KD_KELURAHAN = C.KD_KELURAHAN
         WHERE A.KD_PROPINSI = '$kd_propinsi' AND
                A.KD_DATI2 = '$kd_dati2' AND
                A.KD_KECAMATAN = '$kd_kec' AND
                A.KD_KELURAHAN = '$kd_kel' AND
                A.KD_BLOK = '$kd_blok' AND
                A.NO_URUT = '$no_urut' AND
                A.KD_JNS_OP = '$kd_jns_op'
                AND A.THN_PAJAK_SPPT='$thn'");

        $s_formulir = $q_formulir->row();

        $s_spop = $q_spop->row();

        $s_wp = $q_wp->row();

        if(count($q_formulir->result())>0){
            $nopel = $s_formulir->THN_PELAYANAN.'.'.$s_formulir->BUNDEL_PELAYANAN.'.'.$s_formulir->NO_URUT_PELAYANAN;
            $data['nopel'] = str_replace(' ', '', $nopel);
            $data['nm_wp'] = $s_spop->NM_WP;
            $data['kel_wp'] = $s_spop->KELURAHAN_WP;
            $data['tgl_pendataan'] = date_indo(date('Y-m-d', strtotime($s_spop->TGL_PENDATAAN_OP)));
            $data['wp'] = $s_wp;

            $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='MUTASI OBJEK/SUBJEK'")->row();
            $data['sk'] = $sk;
            return view('pendataan/sk/mutasi/gabung/sk_excel', $data);
            exit();

            //pembetulan - penelitian 
            if($s_formulir->FORMAT_FORMULIR_ID=='27'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PEMBETULAN SPPT/SKP/STP'")->row();
                $data['sk'] = $sk;
                return view('pendataan/sk/pembetulan/sk_excel', $data);
            }
            //data baru
            
            else if($s_formulir->FORMAT_FORMULIR_ID=='11'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PENDAFTARAN DATA BARU' AND JABATAN='Kepala Bidang PBB P2'")->row();
                $sk2 = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='PENDAFTARAN DATA BARU' AND JABATAN='Kasubid Pelayanan'")->row();
                $data['sk'] = $sk;
                $data['sk2'] = $sk2;
                return view('pendataan/sk/baru/sk_excel', $data);
            }
            //mutasi gabung
            else if($s_formulir->FORMAT_FORMULIR_ID=='13'){
                $sk = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='MUTASI OBJEK/SUBJEK'")->row();
                $data['sk'] = $sk;
                return view('pendataan/sk/mutasi/gabung/sk_excel', $data);
                // $this->session->set_flashdata('pesan', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Fitur dalam perbaikan !</div>');
                // redirect('pendataan/spop');
            }
            
            else {
                $transaksi ='';
                if(substr($s_formulir->FORMAT_FORMULIR_ID,0,1)=='1'){
                    $transaksi = $s_formulir->TRANSAKSI.' (Penelitian Lapangan)';
                }else if(substr($s_formulir->FORMAT_FORMULIR_ID,0,1)=='2'){
                    $transaksi = $s_formulir->TRANSAKSI.' (Penelitian Kantor)';
                }
                $this->session->set_flashdata('pesan', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Generate SK Untuk Jenis Formulir <b>'.$transaksi.'</b> Ini Belum Tersedia !</div>');
                redirect('pendataan/spop');
            }
        }else{
            $this->session->set_flashdata('pesan', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Generate SK Gagal !</div>');
            redirect('pendataan/spop');
        }
    }

    public function prosesspop()
    {

        $jns_transaksi = $this->input->post('jns_transaksi');
        $no_formulir = str_replace('.', '', $this->input->post('no_formulir'));
        $nop_proses = str_replace('.', '', $this->input->post('nop_proses'));
        $no_persil = $this->input->post('no_persil');
        $jalan_op = $this->input->post('jalan_op');
        $blok_kav_no_op = $this->input->post('blok_kav_no_op');
        $rt_op = $this->input->post('rt_op');
        $rw_op = $this->input->post('rw_op');
        $luas_bumi = $this->input->post('luas_bumi');
        $kd_znt = $this->input->post('kd_znt');
        $jns_bumi = $this->input->post('jns_bumi');
        $subjek_pajak_id = $this->input->post('subjek_pajak_id');
        $nm_wp = $this->input->post('nm_wp');
        $kd_status_wp = $this->input->post('kd_status_wp');
        $jalan_wp = $this->input->post('jalan_wp');
        $blok_kav_no_wp = $this->input->post('blok_kav_no_wp');
        $rt_wp = $this->input->post('rt_wp');
        $rw_wp = $this->input->post('rw_wp');
        $kelurahan_wp = $this->input->post('kelurahan_wp');
        $kode_pos_wp = $this->input->post('kode_pos_wp');
        $kota_wp = $this->input->post('kota_wp');
        $telp_wp = $this->input->post('telp_wp');
        $npwp = $this->input->post('npwp');
        $status_pekerjaan_wp = $this->input->post('status_pekerjaan_wp');
        $tgl_pendataan_op = $this->input->post('tgl_pendataan_op');
        $nip_pendata = $this->input->post('nip_pendata');
        $tgl_pemeriksaan_op = $this->input->post('tgl_pemeriksaan_op');
        $nip_pemeriksa_op = $this->input->post('nip_pemeriksa_op');

        $this->db->trans_start();

        $datab = array(
            'SUBJEK_PAJAK_ID' => $subjek_pajak_id,
            'NM_WP' => $nm_wp,
            'JALAN_WP' => $jalan_wp,
            'BLOK_KAV_NO_WP' => $blok_kav_no_wp,
            'RW_WP' => $rw_wp,
            'RT_WP' => $rt_wp,
            'KELURAHAN_WP' => $kelurahan_wp,
            'KOTA_WP' => $kota_wp,
            'KD_POS_WP' => $kode_pos_wp,
            'TELP_WP' => $telp_wp,
            'NPWP' => $npwp,
            'STATUS_PEKERJAAN_WP' => $status_pekerjaan_wp,
        );
        $env = ENVIRONMENT;

        // wajib pajak
        if ($env == 'development') {
            $twp = 'DAT_SUBJEK_PAJAK_TEMP';
        } else {
            $twp = 'DAT_SUBJEK_PAJAK';
        }

        $this->db->where('SUBJEK_PAJAK_ID', $subjek_pajak_id);
        $sd = $this->db->get($twp)->row();

        if (empty($sd)) {
            $this->db->insert($twp, $datab);
        } else {
            $this->db->where('SUBJEK_PAJAK_ID', $subjek_pajak_id);
            $this->db->update($twp, $datab);
        }

        // spop
        if ($env == 'development') {
            $this->db->where('NO_FORMULIR', $no_formulir);
            $cs = $this->db->get('TBL_SPOP')->row();
            if (empty($cs)) {
                $this->db->set('NO_FORMULIR', $no_formulir);
                $this->db->set('JNS_TRANSAKSI', $jns_transaksi);
                $this->db->set('NOP_PROSES', $nop_proses);
                $this->db->set('SUBJEK_PAJAK_ID', $subjek_pajak_id);
                $this->db->set('NM_WP', $nm_wp);
                $this->db->set('JALAN_WP', $jalan_wp);
                $this->db->set('BLOK_KAV_NO_WP', $blok_kav_no_wp);
                $this->db->set('RW_WP', $rw_wp);
                $this->db->set('RT_WP', $rt_wp);
                $this->db->set('KELURAHAN_WP', $kelurahan_wp);
                $this->db->set('KOTA_WP', $kota_wp);
                $this->db->set('KD_POS_WP', $kode_pos_wp);
                $this->db->set('TELP_WP', $telp_wp);
                $this->db->set('NPWP', $npwp);
                $this->db->set('STATUS_PEKERJAAN_WP', $status_pekerjaan_wp);
                $this->db->set('NO_PERSIL', $no_persil);
                $this->db->set('JALAN_OP', $jalan_op);
                $this->db->set('BLOK_KAV_NO_OP', $blok_kav_no_op);
                $this->db->set('RW_OP', $rw_op);
                $this->db->set('RT_OP', $rt_op);
                $this->db->set('KD_STATUS_CABANG', $kd_status_cabang ?? null);
                $this->db->set('KD_STATUS_WP', $kd_status_wp);
                $this->db->set('KD_ZNT', $kd_znt);
                $this->db->set('LUAS_BUMI', $luas_bumi);
                $this->db->set('JNS_BUMI', $jns_bumi);
                $this->db->set('TGL_PENDATAAN_OP', "to_date('$tgl_pendataan_op','dd-mm-yyyy')", false);
                $this->db->set('NIP_PENDATA', $nip_pendata, false);
                $this->db->set('TGL_PEMERIKSAAN_OP', "to_date('$tgl_pemeriksaan_op','dd-mm-yyyy')", false);
                $this->db->set('NIP_PEMERIKSA_OP', $nip_pemeriksa_op, false);
                $this->db->set('TGL_PEREKAMAN_OP', 'sysdate', false);
                $this->db->set('NIP_PEREKAM_OP', $this->session->userdata('nip'), false);
                $this->db->insert('TBL_SPOP');
            }
        } else {
            // production ke 1.7
            // langkah 1
            /* $this->db->set('NO_FORMULIR', $no_formulir);
            $this->db->set('JNS_TRANSAKSI', $jns_transaksi);
            $this->db->set('NOP_PROSES', $nop_proses);
            $this->db->set('SUBJEK_PAJAK_ID', $subjek_pajak_id);
            $this->db->set('NM_WP', $nm_wp);
            $this->db->set('JALAN_WP', $jalan_wp);
            $this->db->set('BLOK_KAV_NO_WP', $blok_kav_no_wp);
            $this->db->set('RW_WP', $rw_wp);
            $this->db->set('RT_WP', $rt_wp);
            $this->db->set('KELURAHAN_WP', $kelurahan_wp);
            $this->db->set('KOTA_WP', $kota_wp);
            $this->db->set('KD_POS_WP', $kode_pos_wp);
            $this->db->set('TELP_WP', $telp_wp);
            $this->db->set('NPWP', $npwp);
            $this->db->set('STATUS_PEKERJAAN_WP', $status_pekerjaan_wp);
            $this->db->set('NO_PERSIL', $no_persil);
            $this->db->set('JALAN_OP', $jalan_op);
            $this->db->set('BLOK_KAV_NO_OP', $blok_kav_no_op);
            $this->db->set('RW_OP', $rw_op);
            $this->db->set('RT_OP', $rt_op);
            $this->db->set('KD_STATUS_CABANG', $kd_status_cabang ?? null);
            $this->db->set('KD_STATUS_WP', $kd_status_wp);
            $this->db->set('KD_ZNT', $kd_znt);
            $this->db->set('LUAS_BUMI', $luas_bumi);
            $this->db->set('JNS_BUMI', $jns_bumi);
            $this->db->set('TGL_PENDATAAN_OP', "to_date('$tgl_pendataan_op','dd-mm-yyyy')", false);
            $this->db->set('NIP_PENDATA', $nip_pendata, false);
            $this->db->set('TGL_PEMERIKSAAN_OP', "to_date('$tgl_pemeriksaan_op','dd-mm-yyyy')", false);
            $this->db->set('NIP_PEMERIKSA_OP', $nip_pemeriksa_op, false);
            $this->db->set('TGL_PEREKAMAN_OP', 'sysdate', false);
            $this->db->set('NIP_PEREKAM_OP', $this->session->userdata('nip'), false);
            $this->db->insert('TBL_SPOP@TO17'); */


            $nip_perekam_op = $this->session->userdata('nip');
            $kd_status_cabang = null;

            $this->db->query("insert into tbl_spop@to17 ( NO_FORMULIR, JNS_TRANSAKSI, NOP_PROSES, SUBJEK_PAJAK_ID, NM_WP, JALAN_WP, BLOK_KAV_NO_WP, RW_WP, RT_WP, KELURAHAN_WP, KOTA_WP, KD_POS_WP, TELP_WP, NPWP, STATUS_PEKERJAAN_WP, NO_PERSIL, JALAN_OP, BLOK_KAV_NO_OP, RW_OP, RT_OP, KD_STATUS_CABANG, KD_STATUS_WP, KD_ZNT, LUAS_BUMI, JNS_BUMI, NIP_PENDATA, NIP_PEMERIKSA_OP, NIP_PEREKAM_OP, TGL_PENDATAAN_OP,TGL_PEMERIKSAAN_OP,TGL_PEREKAMAN_OP ) values ( '$no_formulir', '$jns_transaksi', '$nop_proses', '$subjek_pajak_id', '$nm_wp', '$jalan_wp', '$blok_kav_no_wp', '$rw_wp', '$rt_wp', '$kelurahan_wp', '$kota_wp', '$kode_pos_wp', '$telp_wp', '$npwp', '$status_pekerjaan_wp', '$no_persil', '$jalan_op', '$blok_kav_no_op', '$rw_op', '$rt_op', '$kd_status_cabang', '$kd_status_wp', '$kd_znt', '$luas_bumi', '$jns_bumi', '$nip_pendata', '$nip_pemeriksa_op', '$nip_perekam_op', to_date('$tgl_pendataan_op','dd-mm-yyyy'),to_date('$tgl_pemeriksaan_op','dd-mm-yyyy'),sysdate )");

            // langkah 2
            $nop = splitNop($nop_proses);
            $kd_propinsi = $nop['kd_propinsi'];
            $kd_dati2 = $nop['kd_dati2'];
            $kd_kecamatan = $nop['kd_kecamatan'];
            $kd_kelurahan = $nop['kd_kelurahan'];
            $kd_blok = $nop['kd_blok'];
            $no_urut = $nop['no_urut'];
            $kd_jns_op = $nop['kd_jns_op'];

            $sql = "INSERT INTO DAT_OP_BUMI@TO17 ( 'KD_PROPINSI','KD_DATI2','KD_KECAMATAN','KD_KELURAHAN','KD_BLOK','NO_URUT','KD_JNS_OP','NO_BUMI','KD_ZNT','LUAS_BUMI','JNS_BUMI','NILAI_SISTEM_BUMI') values ( '$kd_propinsi', '$kd_dati2', '$kd_kecamatan', '$kd_kelurahan', '$kd_blok', '$no_urut', '$kd_jns_op', '1', '$kd_znt', '$luas_bumi', '$jns_bumi', '0')";
            $this->db->query($sql);

            /* 
            $this->db->set('KD_PROPINSI', $nop['kd_propinsi']);
            $this->db->set('KD_DATI2', $nop['kd_dati2']);
            $this->db->set('KD_KECAMATAN', $nop['kd_kecamatan']);
            $this->db->set('KD_KELURAHAN', $nop['kd_kelurahan']);
            $this->db->set('KD_BLOK', $nop['kd_blok']);
            $this->db->set('NO_URUT', $nop['no_urut']);
            $this->db->set('KD_JNS_OP', $nop['kd_jns_op']);
            $this->db->set('NO_BUMI', 1);
            $this->db->set('KD_ZNT', $kd_znt);
            $this->db->set('LUAS_BUMI', $luas_bumi);
            $this->db->set('JNS_BUMI', $jns_bumi);
            $this->db->set('NILAI_SISTEM_BUMI', 0);
            $this->db->insert('DAT_OP_BUMI@TO17'); */

            // langkah 3
            $tahun = substr($no_formulir, 0, 4);
            $this->db->query("call PENILAIAN_BUMI19('" . $nop['kd_propinsi'] . "','" . $nop['kd_dati2'] . "','" . $nop['kd_kecamatan'] . "','" . $nop['kd_kelurahan'] . "','" . $nop['kd_blok'] . "','" . $nop['no_urut'] . "','" . $nop['kd_jns_op'] . "','" . $jns_bumi . "','" . $kd_znt . "','" . $luas_bumi . "','" . $tahun . "',1)");

            // langkah 4
            $this->db->query("call PENENTUAN_NJOP_BUMI19('" . $nop['kd_propinsi'] . "','" . $nop['kd_dati2'] . "','" . $nop['kd_kecamatan'] . "','" . $nop['kd_kelurahan'] . "','" . $nop['kd_blok'] . "','" . $nop['no_urut'] . "','" . $nop['kd_jns_op'] . "'," . $tahun . ",1)");
            if ($jns_bumi == '3') {

                $sql = "UPDATE DAT_OP_BANGUNAN@TO17 set JNS_TRANSAKSI_BNG='3' where kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'";
                $this->db->query($sql);
                /* $this->db->set('JNS_TRANSAKSI_BNG', 3);
                $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
                $this->db->where('KD_DATI2', $nop['kd_dati2']);
                $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
                $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
                $this->db->where('KD_BLOK', $nop['kd_blok']);
                $this->db->where('NO_URUT', $nop['no_urut']);
                $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
                $this->db->update("DAT_OP_BANGUNAN@TO17"); */
            }

            if ($jns_bumi == 3 || $jns_transaksi == 3) {

                $sql = "UPDATE SPPT@TO17 set STATUS_PEMBAYARAN_SPPT='3' where THN_PAJAK_SPPT='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'";
                $this->db->query($sql);
                /* $this->db->set('STATUS_PEMBAYARAN_SPPT', 3);
                $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
                $this->db->where('KD_DATI2', $nop['kd_dati2']);
                $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
                $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
                $this->db->where('KD_BLOK', $nop['kd_blok']);
                $this->db->where('NO_URUT', $nop['no_urut']);
                $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
                $this->db->where('THN_PAJAK_SPPT', $tahun);
                $this->db->update("SPPT@TO17"); */
            }

            // 


        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('pendataan/spop');
    }

    public function lspop()
    {
        return view('pendataan/index_lspop');
    }

    function json_lspop()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("NO_FORMULIR, NOP, TRANSAKSI, THN_DIBANGUN_BNG, THN_RENOVASI_BNG");
        $this->datatables->from('V_LSPOP');
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }

    public function lspopadd()
    {
        $jenis_lspop = array(
            '1' => 'PEREKAMAN DATA BANGUNAN',
            '2' => 'PEMUTAKHIRAN DATA BANGUNAN',
            '3' => 'PENGAPUSAN DATA BANGUNAN',
            '4' => 'PENILAIAN INDIVIDU',
        );

        $data = [
            'title' => 'Tambah',
            'url' => 'proseslspop',
            'jt' => $jenis_lspop,
            'jns_trx' => set_value("jns_transaksi", ""),
            'no_formulir' => set_value("no_formulir", ""),
            'nop_proses' => set_value("nop_proses", ""),
            'no_bng' => set_value("no_bng", ""),
            'tgl_pendataan_op' => set_value("tgl_pendataan_op", ""),
            'nip_pendata' => set_value("nip_pendata", ""),
            'tgl_pemeriksaan_op' => set_value("tgl_pemeriksaan_op", ""),
            'nip_pemeriksa_op' => set_value("nip_pemeriksa_op", ""),
        ];
        return view('pendataan/form_lspop', $data);
    }

    public function lspopdetail($id)
    {
        $jenis_lspop = array(
            '1' => 'PEREKAMAN DATA BANGUNAN',
            '2' => 'PEMUTAKHIRAN DATA BANGUNAN',
            '3' => 'PENGAPUSAN DATA BANGUNAN',
            '4' => 'PENILAIAN INDIVIDU',
        );

        $row = $this->db->query("SELECT * FROM V_LSPOP WHERE NO_FORMULIR='$id'")->row();

        // dd($row);
        $data = [
            'title' => 'Update',
            'url' => 'updatelspop',
            'jt' => $jenis_lspop,
            'jns_trx' => set_value("jns_transaksi", $row->JNS_TRANSAKSI),
            'no_formulir' => set_value("no_formulir", $row->NO_FORMULIR),
            'nop_proses' => set_value("nop_proses", $row->NOP),
            'no_bng' => set_value("no_bng", $row->NO_BNG),
            'tgl_pendataan_op' => set_value("tgl_pendataan_op", date('d-m-Y',strtotime($row->TGL_PENDATAAN_BNG))),
            'nip_pendata' => set_value("nip_pendata", trim($row->NIP_PENDATA_BNG)),
            'tgl_pemeriksaan_op' => set_value("tgl_pemeriksaan_op", date('d-m-Y',strtotime($row->TGL_PEMERIKSAAN_BNG))),
            'nip_pemeriksa_op' => set_value("nip_pemeriksa_op", trim($row->NIP_PEMERIKSA_BNG)),
        ];
        return view('pendataan/form_lspop', $data);
    }

    function getFormLspop()
    {
        $this->output->enable_profiler(false);
        $kode = $this->input->get('kode');
        if(isset($_GET['no_formulir'])){
            $no_f = str_replace('.', '', $this->input->get('no_formulir'));
            
            $row = $this->db->query("SELECT * FROM V_LSPOP WHERE NO_FORMULIR='$no_f'")->row();
            $data = [
                'KD_JPB' => set_value("KD_JPB", $row->KD_JPB),
                'LUAS_BNG' => set_value("LUAS_BNG", $row->LUAS_BNG),
                'JML_LANTAI_BNG' => set_value("JML_LANTAI_BNG", $row->JML_LANTAI_BNG),
                'THN_DIBANGUN_BNG' => set_value("THN_DIBANGUN_BNG", $row->THN_DIBANGUN_BNG),
                'THN_RENOVASI_BNG' => set_value("THN_RENOVASI_BNG", $row->THN_RENOVASI_BNG),
                'KONDISI_BNG' => set_value("KONDISI_BNG", $row->KONDISI_BNG),
                'JNS_KONSTRUKSI_BNG' => set_value("JNS_KONSTRUKSI_BNG", $row->JNS_KONSTRUKSI_BNG),
                'JNS_ATAP_BNG' => set_value("JNS_ATAP_BNG", $row->JNS_ATAP_BNG),
                'KD_DINDING' => set_value("KD_DINDING", $row->KD_DINDING),
                'KD_LANTAI' => set_value("KD_LANTAI", $row->KD_LANTAI),
                'KD_LANGIT_LANGIT' => set_value("KD_LANGIT_LANGIT", $row->KD_LANGIT_LANGIT),
                'DAYA_LISTRIK' => set_value("DAYA_LISTRIK", $row->DAYA_LISTRIK),
                'ACSPLIT' => set_value("ACSPLIT", $row->ACSPLIT),
                'ACWINDOW' => set_value("ACWINDOW", $row->ACWINDOW),
                'ACSENTRAL' => set_value("ACSENTRAL", $row->ACSENTRAL),
                'LUAS_KOLAM' => set_value("LUAS_KOLAM", $row->LUAS_KOLAM),
                'FINISHING_KOLAM' => set_value("FINISHING_KOLAM", $row->FINISHING_KOLAM),
                'LUAS_PERKERASAN_RINGAN' => set_value("LUAS_PERKERASAN_RINGAN", $row->LUAS_PERKERASAN_RINGAN),
                'LUAS_PERKERASAN_BERAT' => set_value("LUAS_PERKERASAN_BERAT", $row->LUAS_PERKERASAN_BERAT),
                'LUAS_PERKERASAN_SEDANG' => set_value("LUAS_PERKERASAN_SEDANG", $row->LUAS_PERKERASAN_SEDANG),
                'LUAS_PERKERASAN_DG_TUTUP' => set_value("LUAS_PERKERASAN_DG_TUTUP", $row->LUAS_PERKERASAN_DG_TUTUP),
                'LAP_TENIS_LAMPU_BETON' => set_value("LAP_TENIS_LAMPU_BETON", $row->LAP_TENIS_LAMPU_BETON),
                'LAP_TENIS_BETON' => set_value("LAP_TENIS_BETON", $row->LAP_TENIS_BETON),
                'LAP_TENIS_LAMPU_ASPAL' => set_value("LAP_TENIS_LAMPU_ASPAL", $row->LAP_TENIS_LAMPU_ASPAL),
                'LAP_TENIS_ASPAL' => set_value("LAP_TENIS_ASPAL", $row->LAP_TENIS_ASPAL),
                'LAP_TENIS_LAMPU_RUMPUT' => set_value("LAP_TENIS_LAMPU_RUMPUT", $row->LAP_TENIS_LAMPU_RUMPUT),
                'LAP_TENIS_RUMPUT' => set_value("LAP_TENIS_RUMPUT", $row->LAP_TENIS_RUMPUT),
                'LIFT_PENUMPANG' => set_value("LIFT_PENUMPANG", $row->LIFT_PENUMPANG),
                'LIFT_KAPSUL' => set_value("LIFT_KAPSUL", $row->LIFT_KAPSUL),
                'LIFT_BARANG' => set_value("LIFT_BARANG", $row->LIFT_BARANG),
                'TGG_BERJALAN_A' => set_value("TGG_BERJALAN_A", $row->TGG_BERJALAN_A),
                'TGG_BERJALAN_B' => set_value("TGG_BERJALAN_B", $row->TGG_BERJALAN_B),
                'PJG_PAGAR' => set_value("PJG_PAGAR", $row->PJG_PAGAR),
                'BHN_PAGAR' => set_value("BHN_PAGAR", $row->BHN_PAGAR),
                'HYDRANT' => set_value("HYDRANT", $row->HYDRANT),
                'SPRINKLER' => set_value("SPRINKLER", $row->SPRINKLER),
                'FIRE_ALARM' => set_value("FIRE_ALARM", $row->FIRE_ALARM),
                'JML_PABX' => set_value("JML_PABX", $row->JML_PABX),
                'SUMUR_ARTESIS' => set_value("SUMUR_ARTESIS", $row->SUMUR_ARTESIS),
                'NILAI_INDIVIDU' => set_value("NILAI_INDIVIDU", $row->NILAI_INDIVIDU),
                'NILAI_SISTEM' => set_value("NILAI_SISTEM", $row->NILAI_SISTEM),
            ];
        }else{
            $data = [
                'KD_JPB' => set_value("KD_JPB", ""),
                'LUAS_BNG' => set_value("LUAS_BNG", ""),
                'JML_LANTAI_BNG' => set_value("JML_LANTAI_BNG", ""),
                'THN_DIBANGUN_BNG' => set_value("THN_DIBANGUN_BNG", ""),
                'THN_RENOVASI_BNG' => set_value("THN_RENOVASI_BNG", ""),
                'KONDISI_BNG' => set_value("KONDISI_BNG", ""),
                'JNS_KONSTRUKSI_BNG' => set_value("JNS_KONSTRUKSI_BNG", ""),
                'JNS_ATAP_BNG' => set_value("JNS_ATAP_BNG", ""),
                'KD_DINDING' => set_value("KD_DINDING", ""),
                'KD_LANTAI' => set_value("KD_LANTAI", ""),
                'KD_LANGIT_LANGIT' => set_value("KD_LANGIT_LANGIT", ""),
                'DAYA_LISTRIK' => set_value("DAYA_LISTRIK", ""),
                'ACSPLIT' => set_value("ACSPLIT", ""),
                'ACWINDOW' => set_value("ACWINDOW", ""),
                'ACSENTRAL' => set_value("ACSENTRAL", "0"),
                'LUAS_KOLAM' => set_value("LUAS_KOLAM", ""),
                'FINISHING_KOLAM' => set_value("FINISHING_KOLAM", ""),
                'LUAS_PERKERASAN_RINGAN' => set_value("LUAS_PERKERASAN_RINGAN", ""),
                'LUAS_PERKERASAN_BERAT' => set_value("LUAS_PERKERASAN_BERAT", ""),
                'LUAS_PERKERASAN_SEDANG' => set_value("LUAS_PERKERASAN_SEDANG", ""),
                'LUAS_PERKERASAN_DG_TUTUP' => set_value("LUAS_PERKERASAN_DG_TUTUP", ""),
                'LAP_TENIS_LAMPU_BETON' => set_value("LAP_TENIS_LAMPU_BETON", ""),
                'LAP_TENIS_BETON' => set_value("LAP_TENIS_BETON", ""),
                'LAP_TENIS_LAMPU_ASPAL' => set_value("LAP_TENIS_LAMPU_ASPAL", ""),
                'LAP_TENIS_ASPAL' => set_value("LAP_TENIS_ASPAL", ""),
                'LAP_TENIS_LAMPU_RUMPUT' => set_value("LAP_TENIS_LAMPU_RUMPUT", ""),
                'LAP_TENIS_RUMPUT' => set_value("LAP_TENIS_RUMPUT", ""),
                'LIFT_PENUMPANG' => set_value("LIFT_PENUMPANG", ""),
                'LIFT_KAPSUL' => set_value("LIFT_KAPSUL", ""),
                'LIFT_BARANG' => set_value("LIFT_BARANG", ""),
                'TGG_BERJALAN_A' => set_value("TGG_BERJALAN_A", ""),
                'TGG_BERJALAN_B' => set_value("TGG_BERJALAN_B", ""),
                'PJG_PAGAR' => set_value("PJG_PAGAR", ""),
                'BHN_PAGAR' => set_value("BHN_PAGAR", ""),
                'HYDRANT' => set_value("HYDRANT", "0"),
                'SPRINKLER' => set_value("SPRINKLER", "0"),
                'FIRE_ALARM' => set_value("FIRE_ALARM", "0"),
                'JML_PABX' => set_value("JML_PABX", ""),
                'SUMUR_ARTESIS' => set_value("SUMUR_ARTESIS", ""),
                'NILAI_INDIVIDU' => set_value("NILAI_INDIVIDU", ""),
                'NILAI_SISTEM' => set_value("NILAI_SISTEM", ""),
            ];
        }
        

        // dd($data);
        if ($kode <> 4) {
            // $this->load->view('spop/_lspop', $data);
            $this->load->view('spop/_lspop_dev', $data);
        } else {
            $this->load->view('spop/_lspop_dupat_dev', $data);
        }
    }

    public function proseslspop()
    {
        // dd($this->input->post());

        $jns_transaksi = $this->input->post('jns_transaksi');
        $no_formulir = $this->input->post('no_formulir');
        $nop_proses = $this->input->post('nop_proses');
        $no_bng = $this->input->post('no_bng');
        $kd_jpb = $this->input->post('KD_JPB');
        $kondisi_bng = $this->input->post('KONDISI_BNG');
        $luas_bng = $this->input->post('LUAS_BNG');
        $jml_lantai_bng = $this->input->post('JML_LANTAI_BNG');
        $thn_dibangun_bng = $this->input->post('THN_DIBANGUN_BNG');
        $thn_renovasi_bng = $this->input->post('THN_RENOVASI_BNG');
        $jns_konstruksi_bng = $this->input->post('JNS_KONSTRUKSI_BNG');
        $jns_atap_bng = $this->input->post('JNS_ATAP_BNG');
        $kd_dinding = $this->input->post('KD_DINDING');
        $kd_lantai = $this->input->post('KD_LANTAI');
        $kd_langit_langit = $this->input->post('KD_LANGIT_LANGIT');
        $daya_listrik = $this->input->post('DAYA_LISTRIK');
        $acsplit = $this->input->post('ACSPLIT');
        $acwindow = $this->input->post('ACWINDOW');
        $acsentral = $this->input->post('ACSENTRAL');
        $luas_kolam = $this->input->post('LUAS_KOLAM');
        $finishing_kolam = $this->input->post('FINISHING_KOLAM');
        $luas_perkerasan_ringan = $this->input->post('LUAS_PERKERASAN_RINGAN');
        $luas_perkerasan_berat = $this->input->post('LUAS_PERKERASAN_BERAT');
        $luas_perkerasan_sedang = $this->input->post('LUAS_PERKERASAN_SEDANG');
        $luas_perkerasan_dg_tutup = $this->input->post('LUAS_PERKERASAN_DG_TUTUP');
        $lap_tenis_lampu_beton = $this->input->post('LAP_TENIS_LAMPU_BETON');
        $lap_tenis_beton = $this->input->post('LAP_TENIS_BETON');
        $lap_tenis_lampu_aspal = $this->input->post('LAP_TENIS_LAMPU_ASPAL');
        $lap_tenis_aspal = $this->input->post('LAP_TENIS_ASPAL');
        $lap_tenis_lampu_rumput = $this->input->post('LAP_TENIS_LAMPU_RUMPUT');
        $lap_tenis_rumput = $this->input->post('LAP_TENIS_RUMPUT');
        $lift_penumpang = $this->input->post('LIFT_PENUMPANG');
        $lift_kapsul = $this->input->post('LIFT_KAPSUL');
        $lift_barang = $this->input->post('LIFT_BARANG');
        $tgg_berjalan_a = $this->input->post('TGG_BERJALAN_A');
        $tgg_berjalan_b = $this->input->post('TGG_BERJALAN_B');
        $pjg_pagar = $this->input->post('PJG_PAGAR');
        $bhn_pagar = $this->input->post('BHN_PAGAR');
        $hydrant = $this->input->post('HYDRANT');
        $sprinkler = $this->input->post('SPRINKLER');
        $fire_alarm = $this->input->post('FIRE_ALARM');
        $jml_pabx = $this->input->post('JML_PABX');
        $sumur_artesis = $this->input->post('SUMUR_ARTESIS');
        $nilai_sistem_bng = $this->input->post('NILAI_SISTEM');
        $nilai_individu = $this->input->post('NILAI_INDIVIDU');
        $tgl_pendataan_op = $this->input->post('tgl_pendataan_op');
        $nip_pendata = $this->input->post('nip_pendata');
        $tgl_pemeriksaan_op = $this->input->post('tgl_pemeriksaan_op');
        $nip_pemeriksa_op = $this->input->post('nip_pemeriksa_op');


        $env = ENVIRONMENT;
        // lspop

        // langkah 1
        $this->db->set('NO_FORMULIR', str_replace('.', '', $no_formulir));
        $this->db->set('JNS_TRANSAKSI', $jns_transaksi);
        $this->db->set('NOP', str_replace('.', '', $nop_proses));
        $this->db->set('NO_BNG', $no_bng);
        $this->db->set('KD_JPB', $kd_jpb);
        $this->db->set('THN_DIBANGUN_BNG', $thn_dibangun_bng);
        $this->db->set('THN_RENOVASI_BNG', $thn_renovasi_bng);
        $this->db->set('LUAS_BNG', $luas_bng);
        $this->db->set('JML_LANTAI_BNG', $jml_lantai_bng);
        $this->db->set('KONDISI_BNG', $kondisi_bng);
        $this->db->set('JNS_KONSTRUKSI_BNG', $jns_konstruksi_bng);
        $this->db->set('JNS_ATAP_BNG', $jns_atap_bng);
        $this->db->set('KD_DINDING', $kd_dinding);
        $this->db->set('KD_LANTAI', $kd_lantai);
        $this->db->set('KD_LANGIT_LANGIT', $kd_langit_langit);
        $this->db->set('DAYA_LISTRIK', $daya_listrik);
        $this->db->set('ACSPLIT', $acsplit);
        $this->db->set('ACWINDOW', $acwindow);
        $this->db->set('ACSENTRAL', $acsentral ?? null);
        $this->db->set('LUAS_KOLAM', $luas_kolam);
        $this->db->set('FINISHING_KOLAM', $finishing_kolam);
        $this->db->set('LUAS_PERKERASAN_RINGAN', $luas_perkerasan_ringan);
        $this->db->set('LUAS_PERKERASAN_SEDANG', $luas_perkerasan_sedang);
        $this->db->set('LUAS_PERKERASAN_BERAT', $luas_perkerasan_berat);
        $this->db->set('LUAS_PERKERASAN_DG_TUTUP', $luas_perkerasan_dg_tutup);
        $this->db->set('LAP_TENIS_LAMPU_BETON', $lap_tenis_lampu_beton);
        $this->db->set('LAP_TENIS_LAMPU_ASPAL', $lap_tenis_lampu_aspal);
        $this->db->set('LAP_TENIS_LAMPU_RUMPUT', $lap_tenis_lampu_rumput);
        $this->db->set('LAP_TENIS_BETON', $lap_tenis_beton);
        $this->db->set('LAP_TENIS_ASPAL', $lap_tenis_aspal);
        $this->db->set('LAP_TENIS_RUMPUT', $lap_tenis_rumput);
        $this->db->set('LIFT_PENUMPANG', $lift_penumpang);
        $this->db->set('LIFT_KAPSUL', $lift_kapsul);
        $this->db->set('LIFT_BARANG', $lift_barang);
        $this->db->set('TGG_BERJALAN_A', $tgg_berjalan_a);
        $this->db->set('TGG_BERJALAN_B', $tgg_berjalan_b);
        $this->db->set('PJG_PAGAR', $pjg_pagar);
        $this->db->set('BHN_PAGAR', $bhn_pagar);
        $this->db->set('HYDRANT', $hydrant);
        $this->db->set('SPRINKLER', $sprinkler);
        $this->db->set('FIRE_ALARM', $fire_alarm);
        $this->db->set('JML_PABX', $jml_pabx);
        $this->db->set('SUMUR_ARTESIS', $sumur_artesis);
        $this->db->set('NILAI_INDIVIDU', $nilai_individu ?? null);
        $this->db->set('NILAI_SISTEM', $nilai_sistem_bng ?? null);
        // $this->db->set('JPB3_8_TINGGI_KOLOM', $jpb3_8_tinggi_kolom ?? null);
        // $this->db->set('JPB3_8_LEBAR_BENTANG', $jpb3_8_lebar_bentang ?? null);
        // $this->db->set('JPB3_8_DD_LANTAI', $jpb3_8_dd_lantai ?? null);
        // $this->db->set('JPB3_8_KEL_DINDING', $jpb3_8_kel_dinding ?? null);
        // $this->db->set('JPB3_8_MEZZANINE', $jpb3_8_mezzanine ?? null);
        // $this->db->set('JPB5_KLS_BNG', $jpb5_kls_bng ?? null);
        // $this->db->set('JPB5_LUAS_KAMAR', $jpb5_luas_kamar ?? null);
        // $this->db->set('JPB5_LUAS_RNG_LAIN', $jpb5_luas_rng_lain ?? null);
        // $this->db->set('JPB7_JNS_HOTEL', $jpb7_jns_hotel ?? null);
        // $this->db->set('JPB7_BINTANG', $jpb7_bintang ?? null);
        // $this->db->set('JPB7_JML_KAMAR', $jpb7_jml_kamar ?? null);
        // $this->db->set('JPB7_LUAS_KAMAR', $jpb7_luas_kamar ?? null);
        // $this->db->set('JPB7_LUAS_RNG_LAIN', $jpb7_luas_rng_lain ?? null);
        // $this->db->set('JPB13_KLS_BNG', $jpb13_kls_bng ?? null);
        // $this->db->set('JPB13_JML', $jpb13_jml ?? null);
        // $this->db->set('JPB13_LUAS_KAMAR', $jpb13_luas_kamar ?? null);
        // $this->db->set('JPB13_LUAS_RNG_LAIN', $jpb13_luas_rng_lain ?? null);
        // $this->db->set('JPB15_LETAK_TANGKI', $jpb15_letak_tangki ?? null);
        // $this->db->set('JPB15_KAPASITAS_TANGKI', $jpb15_kapasitas_tangki ?? null);
        // $this->db->set('JPB_LAIN_KLS_BNG', $jpb_lain_kls_bng ?? null);
        $this->db->set('TGL_PENDATAAN_BNG', "to_date('$tgl_pendataan_op','dd-mm-yyyy')", false);
        $this->db->set('NIP_PENDATA_BNG', $nip_pendata, false);
        $this->db->set('TGL_PEMERIKSAAN_BNG', "to_date('$tgl_pemeriksaan_op','dd-mm-yyyy')", false);
        $this->db->set('NIP_PEMERIKSA_BNG', $nip_pemeriksa_op, false);
        $this->db->set('TGL_PEREKAMAN_BNG', 'sysdate', false);
        $this->db->set('NIP_PEREKAM_BNG', $this->session->userdata('nip'), false);

        $this->db->insert('TBL_LSPOP');
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('pendataan/lspop');

        exit();
        if ($env == 'development') {
            // dev
            $this->db->insert('TBL_SPOP');
        } else {
            // prod to 1.7
            $this->db->insert('TBL_SPOP@TO17');

            // langkah 2
            $nop = splitNop($nop_proses);
            $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
            $this->db->where('KD_DATI2', $nop['kd_dati2']);
            $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
            $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
            $this->db->where('KD_BLOK', $nop['kd_blok']);
            $this->db->where('NO_URUT', $nop['no_urut']);
            $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
            $cb = $this->db->get('DAT_OP_BANGUNAN@TO17')->row();
            if (count($cb)) {
                $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
                $this->db->where('KD_DATI2', $nop['kd_dati2']);
                $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
                $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
                $this->db->where('KD_BLOK', $nop['kd_blok']);
                $this->db->where('NO_URUT', $nop['no_urut']);
                $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
                $this->db->delete('DAT_OP_BANGUNAN@TO17');
            }

            // INSERT INTO PBB.DAT_OP_BANGUNAN (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, NO_BNG, KD_JPB, NO_FORMULIR_LSPOP, THN_DIBANGUN_BNG,  THN_RENOVASI_BNG, LUAS_BNG, JML_LANTAI_BNG, KONDISI_BNG, JNS_KONSTRUKSI_BNG, JNS_ATAP_BNG, KD_DINDING, KD_LANTAI, KD_LANGIT_LANGIT, NILAI_SISTEM_BNG, JNS_TRANSAKSI_BNG,  TGL_PENDATAAN_BNG, NIP_PENDATA_BNG, TGL_PEMERIKSAAN_BNG, NIP_PEMERIKSA_BNG, TGL_PEREKAMAN_BNG, NIP_PEREKAM_BNG) VALUES  ( '35', '07','240', '009','006','0014','0', '1','01', '20192029078', '2016', '0', '100', '01', '3', '2', '3', '3', '2', '2', 0, '2',  to_date('30/09/2019','dd/mm/yyyy'), '197003232003121007', to_date('30/09/2019','dd/mm/yyyy')  , '197003232003121007',  to_date( '02/12/2019','dd/mm/yyyy')  , '197101262008011005');
            /*    // 
            KD_PROPINSI,
            KD_DATI2,
             KD_KECAMATAN,
             KD_KELURAHAN,
             KD_BLOK,
             NO_URUT,
             KD_JNS_OP,
             NO_BNG,
             KD_JPB,
             NO_FORMULIR_LSPOP,
             THN_DIBANGUN_BNG,
              THN_RENOVASI_BNG,
             LUAS_BNG,
             JML_LANTAI_BNG,
             KONDISI_BNG,
             JNS_KONSTRUKSI_BNG,
             JNS_ATAP_BNG,
             KD_DINDING,
             KD_LANTAI,
             KD_LANGIT_LANGIT,
             NILAI_SISTEM_BNG,
             JNS_TRANSAKSI_BNG,
              TGL_PENDATAAN_BNG,
             NIP_PENDATA_BNG,
             TGL_PEMERIKSAAN_BNG,
             NIP_PEMERIKSA_BNG,
              TGL_PEREKAMAN_BNG, */
            //   NIP_PEREKAM_BNG,

        }
    }

    public function updatelspop()
    {
        // dd($this->input->post());

        $jns_transaksi = $this->input->post('jns_transaksi');
        $no_formulir = $this->input->post('no_formulir');
        $nop_proses = $this->input->post('nop_proses');
        $no_bng = $this->input->post('no_bng');
        $kd_jpb = $this->input->post('KD_JPB');
        $kondisi_bng = $this->input->post('KONDISI_BNG');
        $luas_bng = $this->input->post('LUAS_BNG');
        $jml_lantai_bng = $this->input->post('JML_LANTAI_BNG');
        $thn_dibangun_bng = $this->input->post('THN_DIBANGUN_BNG');
        $thn_renovasi_bng = $this->input->post('THN_RENOVASI_BNG');
        $jns_konstruksi_bng = $this->input->post('JNS_KONSTRUKSI_BNG');
        $jns_atap_bng = $this->input->post('JNS_ATAP_BNG');
        $kd_dinding = $this->input->post('KD_DINDING');
        $kd_lantai = $this->input->post('KD_LANTAI');
        $kd_langit_langit = $this->input->post('KD_LANGIT_LANGIT');
        $daya_listrik = $this->input->post('DAYA_LISTRIK');
        $acsplit = $this->input->post('ACSPLIT');
        $acwindow = $this->input->post('ACWINDOW');
        $acsentral = $this->input->post('ACSENTRAL');
        $luas_kolam = $this->input->post('LUAS_KOLAM');
        $finishing_kolam = $this->input->post('FINISHING_KOLAM');
        $luas_perkerasan_ringan = $this->input->post('LUAS_PERKERASAN_RINGAN');
        $luas_perkerasan_berat = $this->input->post('LUAS_PERKERASAN_BERAT');
        $luas_perkerasan_sedang = $this->input->post('LUAS_PERKERASAN_SEDANG');
        $luas_perkerasan_dg_tutup = $this->input->post('LUAS_PERKERASAN_DG_TUTUP');
        $lap_tenis_lampu_beton = $this->input->post('LAP_TENIS_LAMPU_BETON');
        $lap_tenis_beton = $this->input->post('LAP_TENIS_BETON');
        $lap_tenis_lampu_aspal = $this->input->post('LAP_TENIS_LAMPU_ASPAL');
        $lap_tenis_aspal = $this->input->post('LAP_TENIS_ASPAL');
        $lap_tenis_lampu_rumput = $this->input->post('LAP_TENIS_LAMPU_RUMPUT');
        $lap_tenis_rumput = $this->input->post('LAP_TENIS_RUMPUT');
        $lift_penumpang = $this->input->post('LIFT_PENUMPANG');
        $lift_kapsul = $this->input->post('LIFT_KAPSUL');
        $lift_barang = $this->input->post('LIFT_BARANG');
        $tgg_berjalan_a = $this->input->post('TGG_BERJALAN_A');
        $tgg_berjalan_b = $this->input->post('TGG_BERJALAN_B');
        $pjg_pagar = $this->input->post('PJG_PAGAR');
        $bhn_pagar = $this->input->post('BHN_PAGAR');
        $hydrant = $this->input->post('HYDRANT');
        $sprinkler = $this->input->post('SPRINKLER');
        $fire_alarm = $this->input->post('FIRE_ALARM');
        $jml_pabx = $this->input->post('JML_PABX');
        $sumur_artesis = $this->input->post('SUMUR_ARTESIS');
        $nilai_sistem_bng = $this->input->post('NILAI_SISTEM');
        $nilai_individu = $this->input->post('NILAI_INDIVIDU');
        $tgl_pendataan_op = $this->input->post('tgl_pendataan_op');
        $nip_pendata = $this->input->post('nip_pendata');
        $tgl_pemeriksaan_op = $this->input->post('tgl_pemeriksaan_op');
        $nip_pemeriksa_op = $this->input->post('nip_pemeriksa_op');


        $env = ENVIRONMENT;
        // lspop

        // langkah 1
        $this->db->set('JNS_TRANSAKSI', $jns_transaksi);
        $this->db->set('NOP', str_replace('.', '', $nop_proses));
        $this->db->set('NO_BNG', $no_bng);
        $this->db->set('KD_JPB', $kd_jpb);
        $this->db->set('THN_DIBANGUN_BNG', $thn_dibangun_bng);
        $this->db->set('THN_RENOVASI_BNG', $thn_renovasi_bng);
        $this->db->set('LUAS_BNG', $luas_bng);
        $this->db->set('JML_LANTAI_BNG', $jml_lantai_bng);
        $this->db->set('KONDISI_BNG', $kondisi_bng);
        $this->db->set('JNS_KONSTRUKSI_BNG', $jns_konstruksi_bng);
        $this->db->set('JNS_ATAP_BNG', $jns_atap_bng);
        $this->db->set('KD_DINDING', $kd_dinding);
        $this->db->set('KD_LANTAI', $kd_lantai);
        $this->db->set('KD_LANGIT_LANGIT', $kd_langit_langit);
        $this->db->set('DAYA_LISTRIK', $daya_listrik);
        $this->db->set('ACSPLIT', $acsplit);
        $this->db->set('ACWINDOW', $acwindow);
        $this->db->set('ACSENTRAL', $acsentral ?? null);
        $this->db->set('LUAS_KOLAM', $luas_kolam);
        $this->db->set('FINISHING_KOLAM', $finishing_kolam);
        $this->db->set('LUAS_PERKERASAN_RINGAN', $luas_perkerasan_ringan);
        $this->db->set('LUAS_PERKERASAN_SEDANG', $luas_perkerasan_sedang);
        $this->db->set('LUAS_PERKERASAN_BERAT', $luas_perkerasan_berat);
        $this->db->set('LUAS_PERKERASAN_DG_TUTUP', $luas_perkerasan_dg_tutup);
        $this->db->set('LAP_TENIS_LAMPU_BETON', $lap_tenis_lampu_beton);
        $this->db->set('LAP_TENIS_LAMPU_ASPAL', $lap_tenis_lampu_aspal);
        $this->db->set('LAP_TENIS_LAMPU_RUMPUT', $lap_tenis_lampu_rumput);
        $this->db->set('LAP_TENIS_BETON', $lap_tenis_beton);
        $this->db->set('LAP_TENIS_ASPAL', $lap_tenis_aspal);
        $this->db->set('LAP_TENIS_RUMPUT', $lap_tenis_rumput);
        $this->db->set('LIFT_PENUMPANG', $lift_penumpang);
        $this->db->set('LIFT_KAPSUL', $lift_kapsul);
        $this->db->set('LIFT_BARANG', $lift_barang);
        $this->db->set('TGG_BERJALAN_A', $tgg_berjalan_a);
        $this->db->set('TGG_BERJALAN_B', $tgg_berjalan_b);
        $this->db->set('PJG_PAGAR', $pjg_pagar);
        $this->db->set('BHN_PAGAR', $bhn_pagar);
        $this->db->set('HYDRANT', $hydrant);
        $this->db->set('SPRINKLER', $sprinkler);
        $this->db->set('FIRE_ALARM', $fire_alarm);
        $this->db->set('JML_PABX', $jml_pabx);
        $this->db->set('SUMUR_ARTESIS', $sumur_artesis);
        $this->db->set('NILAI_INDIVIDU', $nilai_individu ?? null);
        $this->db->set('NILAI_SISTEM', $nilai_sistem_bng ?? null);
        $this->db->set('TGL_PENDATAAN_BNG', "to_date('$tgl_pendataan_op','dd-mm-yyyy')", false);
        $this->db->set('NIP_PENDATA_BNG', $nip_pendata, false);
        $this->db->set('TGL_PEMERIKSAAN_BNG', "to_date('$tgl_pemeriksaan_op','dd-mm-yyyy')", false);
        $this->db->set('NIP_PEMERIKSA_BNG', $nip_pemeriksa_op, false);
        $this->db->set('TGL_PEREKAMAN_BNG', 'sysdate', false);
        $this->db->set('NIP_PEREKAM_BNG', $this->session->userdata('nip'), false);

        
        $this->db->where('NO_FORMULIR', str_replace('.', '', $no_formulir));
        $this->db->update('TBL_LSPOP');
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('pendataan/lspop');

        exit();
        if ($env == 'development') {
            // dev
            $this->db->insert('TBL_SPOP');
        } else {
            // prod to 1.7
            $this->db->insert('TBL_SPOP@TO17');

            // langkah 2
            $nop = splitNop($nop_proses);
            $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
            $this->db->where('KD_DATI2', $nop['kd_dati2']);
            $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
            $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
            $this->db->where('KD_BLOK', $nop['kd_blok']);
            $this->db->where('NO_URUT', $nop['no_urut']);
            $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
            $cb = $this->db->get('DAT_OP_BANGUNAN@TO17')->row();
            if (count($cb)) {
                $this->db->where('KD_PROPINSI', $nop['kd_propinsi']);
                $this->db->where('KD_DATI2', $nop['kd_dati2']);
                $this->db->where('KD_KECAMATAN', $nop['kd_kecamatan']);
                $this->db->where('KD_KELURAHAN', $nop['kd_kelurahan']);
                $this->db->where('KD_BLOK', $nop['kd_blok']);
                $this->db->where('NO_URUT', $nop['no_urut']);
                $this->db->where('KD_JNS_OP', $nop['kd_jns_op']);
                $this->db->delete('DAT_OP_BANGUNAN@TO17');
            }

        }
    }

    public function lspopdelete($id)
    {
        $this->db->where('NO_FORMULIR', $id);
        $this->db->delete('TBL_LSPOP');
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }

        redirect('pendataan/lspop');
    }

    public function spopdelete($id)
    {
        $this->db->where('NO_FORMULIR', $id);
        $this->db->delete('TBL_SPOP');
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }

        redirect('pendataan/spop');
    }

    public function test()
    {
        $cb = $this->db->get('DAT_OP_BANGUNAN@TO17')->row();
        echo $this->db->last_query();
    }
}
