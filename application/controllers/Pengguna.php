<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);

        $this->load->model('Mpengguna');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        return view('pengguna/pengguna_list');
    }



    public function json()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo $this->Mpengguna->json();
    }

    public function create()
    {
        $data = array(
            'button'        => 'Tambah Pengguna',
            'action'        => site_url('pengguna/create_action'),
            'KODE_PENGGUNA' => set_value('KODE_PENGGUNA'),
            'USERNAME'      => set_value('USERNAME'),
            'PASSWORD'      => set_value('PASSWORD'),
            'NAMA_PENGGUNA' => set_value('NAMA_PENGGUNA'),
            'KODE_GROUP'    => set_value('KODE_GROUP'),
            'NIP'           => set_value('NIP'),
            'KD_SEKSI'           => set_value('KD_SEKSI'),
            'group'         => $this->db->query("SELECT KODE_GROUP, NAMA_GROUP FROM P_GROUP")->result(),
            'seksi' => $this->db->query("select KD_SEKSI,NM_SEKSI from REF_SEKSI")->result()
        );

        return view('pengguna/pengguna_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'USERNAME'      => $this->input->post('USERNAME', TRUE),
                'PASSWORD'      => $this->input->post('PASSWORD', TRUE),
                'NAMA_PENGGUNA' => $this->input->post('NAMA_PENGGUNA', TRUE),
                'KODE_GROUP'    => $this->input->post('KODE_GROUP', TRUE),
                'NIP'           => $this->input->post('NIP', TRUE),
                'KD_SEKSI'           => $this->input->post('KD_SEKSI', TRUE),
            );

            $this->Mpengguna->insert($data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pengguna'));
        }
    }

    public function update($id)
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $data = array(
                'button'        => 'Update Pengguna',
                'action'        => site_url('pengguna/update_action'),
                'KODE_PENGGUNA' => set_value('KODE_PENGGUNA', $row->KODE_PENGGUNA),
                'USERNAME'      => set_value('USERNAME', $row->USERNAME),
                'NAMA_PENGGUNA' => set_value('NAMA_PENGGUNA', $row->NAMA_PENGGUNA),
                'KODE_GROUP'    => set_value('KODE_GROUP', $row->KODE_GROUP),
                'NIP'           => set_value('NIP', $row->NIP),
                'KD_SEKSI'      => set_value('KD_SEKSI', $row->KD_SEKSI),
                'group'         => $this->db->query("SELECT KODE_GROUP, NAMA_GROUP FROM P_GROUP")->result(),
                'seksi'         => $this->db->query("select KD_SEKSI,NM_SEKSI from REF_SEKSI")->result()
            );
            return view('pengguna/pengguna_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function update_action()
    {
        $this->_rulesas();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('KODE_PENGGUNA', TRUE));
        } else {
            $data = array(
                'USERNAME'      => $this->input->post('USERNAME', TRUE),
                'NAMA_PENGGUNA' => $this->input->post('NAMA_PENGGUNA', TRUE),
                'KODE_GROUP'    => $this->input->post('KODE_GROUP', TRUE),
                'NIP'           => $this->input->post('NIP', TRUE),
                'KD_SEKSI'      => $this->input->post('KD_SEKSI', TRUE),
            );

            $this->Mpengguna->update($this->input->post('KODE_PENGGUNA', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pengguna'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $this->Mpengguna->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pengguna'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('USERNAME', 'username', 'trim|required');
        $this->form_validation->set_rules('PASSWORD', 'password', 'trim|required');
        $this->form_validation->set_rules('NAMA_PENGGUNA', 'nama pengguna', 'trim|required');
        $this->form_validation->set_rules('KODE_GROUP', 'group', 'trim|required');
        $this->form_validation->set_rules('NIP', 'nip', 'trim|required');
        $this->form_validation->set_rules('KD_SEKSI', 'Seksi', 'trim|required');
        $this->form_validation->set_rules('KODE_PENGGUNA', 'KODE_PENGGUNA', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    function _rulesas()
    {
        $this->form_validation->set_rules('USERNAME', 'username', 'trim|required');
        $this->form_validation->set_rules('NAMA_PENGGUNA', 'nama pengguna', 'trim|required');
        $this->form_validation->set_rules('KODE_GROUP', 'group', 'trim|required');
        $this->form_validation->set_rules('NIP', 'nip', 'trim|required');
        $this->form_validation->set_rules('KD_SEKSI', 'Seksi', 'trim|required');
        $this->form_validation->set_rules('KODE_PENGGUNA', 'KODE_PENGGUNA', 'trim');
        $this->form_validation->set_error_delimiters('<span class ="label label-danger ">', '</span>');
    }

    function resetPassword($id)
    {

        $this->db->set('PASSWORD', 'pass');
        $this->db->where('KODE_PENGGUNA', $id);
        $this->db->update('P_PENGGUNA');
        $msg = "Password telah di update";
        $url = base_url() . 'pengguna';
        // header("Location:".$url);
        echo ("<script LANGUAGE='JavaScript'>
                        window.alert('$msg');
                        window.location.href='$url';
                        </script>");
    }

    public function useruptctrl()
    {
        $res = $this->db->query("select a.id,nama_pengguna,nama_upt 
        from P_UPT_USER a
        join p_pengguna b on a.kode_pengguna=b.kode_pengguna
        join p_upt c on a.kode_upt=c.kode_upt")->result();
        $data = array(
            'data' => $res
        );
        return view('pengguna/penggunaupt',$data);
    }

    public function usertpctrl()
    {
        $res = $this->db->query("select a.id,nama_pengguna,nama_upt 
        from p_pengguna b
        join  P_UPT_USER a
            on a.kode_pengguna=b.kode_pengguna
        join p_upt c 
            on c.kode_upt=a.kode_pengguna
         WHERE b.KODE_GROUP='13' ")->result();
        $data = array(
            'data' => $res
        );
        return view('pengguna/penggunatp',$data);
    }

    function useruptctrlcreate()
    {

        // SELECT KODE_PENGGUNA,NAMA_PENGGUNA 
        // FROM P_PENGGUNA WHERE KODE_GROUP=8
        $this->db->where('KODE_GROUP', 8);
        $lp = $this->db->get('P_PENGGUNA')->result();

        $lu = $this->db->where("JENIS","upt")->get('P_UPT')->result();
        $data = array(
            'button' => 'Tambah User UPT',
            'action' => base_url() . 'pengguna/prosestambahupt',
            'lp' => $lp,
            'lu' => $lu
        );
        return view('pengguna/formupt', $data);
    }

    function usertpctrlcreate()
    {

        // SELECT KODE_PENGGUNA,NAMA_PENGGUNA 
        // FROM P_PENGGUNA WHERE KODE_GROUP=8
        $this->db->where('KODE_GROUP', 13);
        $lp = $this->db->get('P_PENGGUNA')->result();

        $lu = $this->db->where("JENIS","tp")->get('P_UPT')->result();
        $data = array(
            'button' => 'Tambah User TP',
            'action' => base_url() . 'pengguna/prosestambahtp',
            'lp' => $lp,
            'lu' => $lu
        );
        return view('pengguna/formtp', $data);
    }

    function prosestambahupt()
    {
        $data = $this->input->post();
        // print_r($data);
        $this->db->insert('P_UPT_USER', $data);
        redirect('userupt');
    }

    function prosestambahtp()
    {
        $data = $this->input->post();
        // print_r($data);
        $this->db->insert('P_UPT_USER', $data);
        redirect('usertp');
    }

    function hapusupt($id){
        $this->db->where('ID',$id);
        $this->db->delete('P_UPT_USER');
        redirect('userupt');
    }

    function hapustp($id){
        $this->db->where('ID',$id);
        $this->db->delete('P_UPT_USER');
        redirect('usertp');
    }
}
