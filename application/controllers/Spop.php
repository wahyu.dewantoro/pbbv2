<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spop extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(false);
		$this->load->model('Mspop');
		$this->load->model('Mpermohonan');
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
	}

	function listformulir()
	{
		$tahun = $this->input->get('tahun');
		$nomer = $this->db->query("select  tahun,jenis_pelayanan,bundel,no_urut,
		substr(nop,1,2) as kd_propinsi,
		substr(nop,3,2) as kd_dati2,
		substr(nop,5,3) as kd_kecamatan,
		substr(nop,8,3) as kd_kelurahan,
		substr(nop,11,3) as kd_blok,
		substr(nop,14,4) as no_urut_op,
		substr(nop,18,1) as kd_jns_op,no_formulir,nop,keterangan_nomor,no_bangunan,status,tgl_perekaman
		from p_nomor where  status=0  and tahun=$tahun")->result();
		$this->load->view('spop/listformulir', array('nomor' => $nomer));
	}

	function form_spop()
	{
		$NIP = $this->session->userdata('nip');
		$kd_formulir = array(
			'0001' => 'Hasil Verifikasi Lapangan',
			'0501' => 'Hasil Verifikasi Kantor',
			'1001' => 'Pembatalan',
			'2001' => 'Pemuktahiran',
			'3001' => 'SISMIOP',
			'5001' => 'Individu & Semi Individu',
			'6001' => 'Keberatan & Banding',
			'7001' => 'Fasum',
			'8001' => 'Penghapusan Bangunan',
			'9001' => 'ZNT'
		);

		// jenis_spop
		$jenis_spop = array(
			'11' => 'PEREKAMAN DATA OP',
			'12' => 'PEMUTAKHIRAN DATA OP',
			'13' => 'PENGAHPUSAN OP',
			'14' => 'PENGHAPUSAN STATUS OP BERSAMA'
		);

		$jenis_lspop = array(
			'21' => 'PEREKAMAN DATA BANGUNAN',
			'22' => 'PEMUTAKHIRAN DATA BANGUNAN',
			'23' => 'PENGAPUSAN DATA BANGUNAN',
			'24' => 'PENILAIAN INDIVIDU',
		);

		$data = array(
			'kd_formulir' => $kd_formulir,
			'tahun' => $this->input->get('tahun'),
			'tab' => $this->input->get('tab'),
			'jenis_spop' => $jenis_spop,
			'jenis_lspop' => $jenis_lspop
		);
		return view('spop/form_spop_new', $data);
	}

	function getFormSpop()
	{
		$kode = $this->input->get('kode');
		$kd_propinsi = $this->input->get('kd_propinsi');
		$kd_dati2 = $this->input->get('kd_dati2');
		$kd_kecamatan = $this->input->get('kd_kecamatan');
		$kd_kelurahan = $this->input->get('kd_kelurahan');
		$kd_blok = $this->input->get('kd_blok');
		$znt = $this->db->query("SELECT * FROM DAT_PETA_ZNT
				WHERE KD_PROPINSI='$kd_propinsi'
				AND KD_DATI2='$kd_dati2'
				AND KD_KECAMATAN='$kd_kecamatan'
				AND KD_KELURAHAN='$kd_kelurahan' 
				AND KD_BLOK='$kd_blok'")->result();
		$jenis_tanah = array(
			'1' => 'TANAH + BANGUNAN',
			'2' => 'KAVLING SIAP BANGUN',
			'3' => 'TANAH KOSONG',
			'4' => 'FASILITAS UMUM',
			'5' => 'LAIN-LAIN',
		);

		$status_wp = array(
			'1' => 'PEMILIK',
			'2' => 'PENYEWA',
			'3' => 'PENGELOLA',
			'4' => 'PEMAKAI',
			'5' => 'SENGKETA',
		);

		$pekerjaan = array(
			'0' => 'LAINNYA',
			'1' => 'PNS',
			'2' => 'ABRI',
			'3' => 'PENSIUNAN',
			'4' => 'BADAN',
			'5' => 'LAINNYA'
		);

		$data = array(
			'status_wp' => $status_wp,
			'pekerjaan' => $pekerjaan,
			'jenis_tanah' => $jenis_tanah,
			'znt' => $znt
		);
		$this->load->view('spop/_11_spop', $data);
	}

	function getFormLspop()
	{
		$kode = $this->input->get('kode');
		$kd_propinsi = $this->input->get('kd_propinsi');
		$kd_dati2 = $this->input->get('kd_dati2');
		$kd_kecamatan = $this->input->get('kd_kecamatan');
		$kd_kelurahan = $this->input->get('kd_kelurahan');
		$kd_blok = $this->input->get('kd_blok');
		$data = [];
		if ($kode <> 24) {
			$this->load->view('spop/_lspop', $data);
		} else {
			$this->load->view('spop/_lspop_dupat', $data);
		}
	}

	function xform_spop($var = "")
	{
		$jns = $this->input->get('JENIS') ? $this->input->get('JENIS') : null;
		echo $jns;


		if ($jns == 'SPOP') {
			$JNS_PELAYANAN     = $this->input->get('KD_JNS_PELAYANAN');
			$THN_PELAYANAN     = $this->input->get('THN_PELAYANAN');
			$BUNDEL_PELAYANAN  = $this->input->get('BUNDEL_PELAYANAN');
			$NO_URUT_DALAM     = $this->input->get('NO_URUT_DALAM');
			$NO_URUT  		   = $this->input->get('NO_URUT');

			$KD_PROPINSI  = $this->input->get('KD_PROPINSI');
			$KD_DATI2     = $this->input->get('KD_DATI2');
			$KD_KECAMATAN = $this->input->get('KD_KECAMATAN');
			$KD_KELURAHAN = $this->input->get('KD_KELURAHAN');
			$KD_BLOK      = $this->input->get('KD_BLOK');
			$NO_URUT_NOP  = $this->input->get('NO_URUT_NOP');
			$KD_JNS_OP    = $this->input->get('KD_JNS_OP');
			$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

			$KD_PROPINSI_ASAL  = $this->input->get('KD_PROPINSI_ASAL');
			$KD_DATI2_ASAL     = $this->input->get('KD_DATI2_ASAL');
			$KD_KECAMATAN_ASAL = $this->input->get('KD_KECAMATAN_ASAL');
			$KD_KELURAHAN_ASAL = $this->input->get('KD_KELURAHAN_ASAL');
			$KD_BLOK_ASAL      = $this->input->get('KD_BLOK_ASAL');
			$NO_URUT_ASAL      = $this->input->get('NO_URUT_ASAL');
			$KD_JNS_OP_ASAL    = $this->input->get('KD_JNS_OP_ASAL');
			$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;


			// data spop
			$this->session->set_userdata('JNS_PELAYANAN_SPOP', $JNS_PELAYANAN);
			$this->session->set_userdata('THN_PELAYANAN_SPOP', $THN_PELAYANAN);
			$this->session->set_userdata('BUNDEL_PELAYANAN_SPOP', $BUNDEL_PELAYANAN);
			$this->session->set_userdata('NO_URUT_DALAM_SPOP', $NO_URUT_DALAM);
			$this->session->set_userdata('NO_URUT_SPOP', $NO_URUT);
			$this->session->set_userdata('KD_PROPINSI_SPOP', $KD_PROPINSI);
			$this->session->set_userdata('KD_DATI2_SPOP', $KD_DATI2);
			$this->session->set_userdata('KD_KECAMATAN_SPOP', $KD_KECAMATAN);
			$this->session->set_userdata('KD_KELURAHAN_SPOP', $KD_KELURAHAN);
			$this->session->set_userdata('KD_BLOK_SPOP', $KD_BLOK);
			$this->session->set_userdata('NO_URUT_NOP_SPOP', $NO_URUT_NOP);
			$this->session->set_userdata('KD_JNS_OP_SPOP', $KD_JNS_OP);

			$this->session->set_userdata('KD_PROPINSI_ASAL_SPOP', $KD_PROPINSI_ASAL);
			$this->session->set_userdata('KD_DATI2_ASAL_SPOP', $KD_DATI2_ASAL);
			$this->session->set_userdata('KD_KECAMATAN_ASAL_SPOP', $KD_KECAMATAN_ASAL);
			$this->session->set_userdata('KD_KELURAHAN_ASAL_SPOP', $KD_KELURAHAN_ASAL);
			$this->session->set_userdata('KD_BLOK_ASAL_SPOP', $KD_BLOK_ASAL);
			$this->session->set_userdata('NO_URUT_ASAL_SPOP', $NO_URUT_ASAL);
			$this->session->set_userdata('KD_JNS_OP_ASAL_SPOP', $KD_JNS_OP_ASAL);
			$this->session->set_userdata('t_spop', "active");
			$this->session->set_userdata('t_lspop', null);
			$this->session->set_userdata('t_formulir', NULL);

			// datalspop
			$this->session->userdata('KD_JNS_PELAYANAN_LSPOP', null);
			$this->session->userdata('THN_PELAYANAN', null);
			$this->session->userdata('BUNDEL_PELAYANAN', null);
			$this->session->userdata('NO_URUT_DALAM', null);
			$this->session->userdata('NO_URUT', null);
			$this->session->userdata('KD_PROPINSI', null);
			$this->session->userdata('KD_DATI2', null);
			$this->session->userdata('KD_KECAMATAN', null);
			$this->session->userdata('KD_KELURAHAN', null);
			$this->session->userdata('KD_BLOK', null);
			$this->session->userdata('NO_URUT_NOP', null);
			$this->session->userdata('KD_JNS_OP', null);

			$this->session->userdata('KD_PROPINSI_ASAL', null);
			$this->session->userdata('KD_DATI2_ASAL', null);
			$this->session->userdata('KD_KECAMATAN_ASAL', null);
			$this->session->userdata('KD_KELURAHAN_ASAL', null);
			$this->session->userdata('KD_BLOK_ASAL', null);
			$this->session->userdata('NO_URUT_ASAL', null);
			$this->session->userdata('KD_JNS_OP_ASAL', null);
			$this->session->userdata('NO_BNG', null);


			$data['NO_FORMULIR_SPOP']	 = $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_DALAM . $NO_URUT;
			$data['nop_asal']			 = $NOP_ASAL;
			$data['isi']				 = $this->db->query("select b.*,a.* from dat_subjek_pajak a
												join dat_objek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
												where a.subjek_pajak_id='$NOP'")->row();
			$data['bumi']				 = $this->db->query("select * from dat_op_bumi
												where kd_propinsi='$KD_PROPINSI'
												and kd_dati2='$KD_DATI2'
												and kd_kecamatan='$KD_KECAMATAN'
												and kd_kelurahan='$KD_KELURAHAN' 
												and kd_blok='$KD_BLOK'
												and no_urut='$NO_URUT_NOP'
												and kd_jns_op='$KD_JNS_OP'")->row();
			$data['znt']				 = $this->db->query("select * from dat_peta_znt
												where kd_propinsi='$KD_PROPINSI'
												and kd_dati2='$KD_DATI2'
												and kd_kecamatan='$KD_KECAMATAN'
												and kd_kelurahan='$KD_KELURAHAN' 
												and kd_blok='$KD_BLOK'")->result();
			$NIP = $this->session->userdata('nip');
			$data['nomor'] = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();

			return view('spop/form_spop', $data);
		}
		if ($jns == 'LSPOP') {
			$JNS_PELAYANAN     = $_GET['KD_JNS_PELAYANAN'];
			//$JNS_FORMULIR      =$_GET['KD_JNS_FORMULIR'];
			$THN_PELAYANAN     = $_GET['THN_PELAYANAN'];
			$BUNDEL_PELAYANAN  = $_GET['BUNDEL_PELAYANAN'];
			$NO_URUT_DALAM     = $_GET['NO_URUT_DALAM'];
			$NO_URUT  		   = $_GET['NO_URUT'];

			$KD_PROPINSI  = $_GET['KD_PROPINSI'];
			$KD_DATI2     = $_GET['KD_DATI2'];
			$KD_KECAMATAN = $_GET['KD_KECAMATAN'];
			$KD_KELURAHAN = $_GET['KD_KELURAHAN'];
			$KD_BLOK      = $_GET['KD_BLOK'];
			$NO_URUT_NOP  = $_GET['NO_URUT_NOP'];
			$KD_JNS_OP    = $_GET['KD_JNS_OP'];
			$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

			$KD_PROPINSI_ASAL  = $_GET['KD_PROPINSI_ASAL'];
			$KD_DATI2_ASAL     = $_GET['KD_DATI2_ASAL'];
			$KD_KECAMATAN_ASAL = $_GET['KD_KECAMATAN_ASAL'];
			$KD_KELURAHAN_ASAL = $_GET['KD_KELURAHAN_ASAL'];
			$KD_BLOK_ASAL      = $_GET['KD_BLOK_ASAL'];
			$NO_URUT_ASAL  	   = $_GET['NO_URUT_ASAL'];
			$KD_JNS_OP_ASAL    = $_GET['KD_JNS_OP_ASAL'];
			$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;
			$NO_BNG    		   = $_GET['NO_BNG'];


			// data lspop
			$_SESSION['KD_JNS_PELAYANAN_LSPOP'] = $JNS_PELAYANAN;
			//$_SESSION['JNS_FORMULIR']    =$JNS_FORMULIR;
			$_SESSION['THN_PELAYANAN']     = $THN_PELAYANAN;
			$_SESSION['BUNDEL_PELAYANAN']  = $BUNDEL_PELAYANAN;
			$_SESSION['NO_URUT_DALAM']     = $NO_URUT_DALAM;
			$_SESSION['NO_URUT']           = $NO_URUT;
			$_SESSION['KD_PROPINSI']       = $KD_PROPINSI;
			$_SESSION['KD_DATI2']          = $KD_DATI2;
			$_SESSION['KD_KECAMATAN']      = $KD_KECAMATAN;
			$_SESSION['KD_KELURAHAN']      = $KD_KELURAHAN;
			$_SESSION['KD_BLOK']           = $KD_BLOK;
			$_SESSION['NO_URUT_NOP']       = $NO_URUT_NOP;
			$_SESSION['KD_JNS_OP']         = $KD_JNS_OP;

			$_SESSION['KD_PROPINSI_ASAL']  = $KD_PROPINSI_ASAL;
			$_SESSION['KD_DATI2_ASAL']     = $KD_DATI2_ASAL;
			$_SESSION['KD_KECAMATAN_ASAL'] = $KD_KECAMATAN_ASAL;
			$_SESSION['KD_KELURAHAN_ASAL'] = $KD_KELURAHAN_ASAL;
			$_SESSION['KD_BLOK_ASAL']      = $KD_BLOK_ASAL;
			$_SESSION['NO_URUT_ASAL']      = $NO_URUT_ASAL;
			$_SESSION['KD_JNS_OP_ASAL']    = $KD_JNS_OP_ASAL;
			$_SESSION['NO_BNG']            = $NO_BNG;
			$_SESSION['t_formulir'] = null;
			$_SESSION['t_spop']     = NULL;
			$_SESSION['t_lspop']    = "active";


			// data spop
			$_SESSION['JNS_PELAYANAN_SPOP']     = null;
			$_SESSION['THN_PELAYANAN_SPOP']     = null;
			$_SESSION['BUNDEL_PELAYANAN_SPOP']  = null;
			$_SESSION['NO_URUT_DALAM_SPOP']     = null;
			$_SESSION['NO_URUT_SPOP']           = null;
			$_SESSION['KD_PROPINSI_SPOP']       = null;
			$_SESSION['KD_DATI2_SPOP']          = null;
			$_SESSION['KD_KECAMATAN_SPOP']      = null;
			$_SESSION['KD_KELURAHAN_SPOP']      = null;
			$_SESSION['KD_BLOK_SPOP']           = null;
			$_SESSION['NO_URUT_NOP_SPOP']       = null;
			$_SESSION['KD_JNS_OP_SPOP']         = null;

			$_SESSION['KD_PROPINSI_ASAL_SPOP']  = null;
			$_SESSION['KD_DATI2_ASAL_SPOP']     = null;
			$_SESSION['KD_KECAMATAN_ASAL_SPOP'] = null;
			$_SESSION['KD_KELURAHAN_ASAL_SPOP'] = null;
			$_SESSION['KD_BLOK_ASAL_SPOP']      = null;
			$_SESSION['NO_URUT_ASAL_SPOP']      = null;
			$_SESSION['KD_JNS_OP_ASAL_SPOP']    = null;


			$data['NO_FORMULIR_SPOP']	 = $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_DALAM . $NO_URUT;
			$data['nop_asal']			 = $NOP_ASAL;
			$data['isi'] = $this->db->query("select * from dat_objek_pajak where subjek_pajak_id='$NOP'")->row();
			// $this->template->load('template', 'spop/form_spop', $data);
			return view('spop/form_spop', $data);
		} else {
			$this->session->set_userdata('JNS_PELAYANAN_SPOP', null);
			$this->session->set_userdata('JNS_FORMULIR_SPOP', null);
			$this->session->set_userdata('THN_PELAYANAN_SPOP', null);
			$this->session->set_userdata('BUNDEL_PELAYANAN_SPOP', null);
			$this->session->set_userdata('NO_URUT_DALAM_SPOP', null);
			$this->session->set_userdata('NO_URUT_SPOP', null);
			$this->session->set_userdata('KD_PROPINSI_SPOP', null);
			$this->session->set_userdata('KD_DATI2_SPOP', null);
			$this->session->set_userdata('KD_KECAMATAN_SPOP', null);
			$this->session->set_userdata('KD_KELURAHAN_SPOP', null);
			$this->session->set_userdata('KD_BLOK_SPOP', null);
			$this->session->set_userdata('NO_URUT_NOP_SPOP', null);
			$this->session->set_userdata('KD_JNS_OP_SPOP', null);

			$this->session->set_userdata('KD_PROPINSI_ASAL_SPOP', null);
			$this->session->set_userdata('KD_DATI2_ASAL_SPOP', null);
			$this->session->set_userdata('KD_KECAMATAN_ASAL_SPOP', null);
			$this->session->set_userdata('KD_KELURAHAN_ASAL_SPOP', null);
			$this->session->set_userdata('KD_BLOK_ASAL_SPOP', null);
			$this->session->set_userdata('NO_URUT_ASAL_SPOP', null);
			$this->session->set_userdata('KD_JNS_OP_ASAL_SPOP', null);


			$this->session->set_userdata('KD_JNS_PELAYANAN_LSPOP', null);
			$this->session->set_userdata('THN_PELAYANAN', null);
			$this->session->set_userdata('BUNDEL_PELAYANAN', null);
			$this->session->set_userdata('NO_URUT_DALAM', null);
			$this->session->set_userdata('NO_URUT', null);
			$this->session->set_userdata('KD_PROPINSI', null);
			$this->session->set_userdata('KD_DATI2', null);
			$this->session->set_userdata('KD_KECAMATAN', null);
			$this->session->set_userdata('KD_KELURAHAN', null);
			$this->session->set_userdata('KD_BLOK', null);
			$this->session->set_userdata('NO_URUT_NOP', null);
			$this->session->set_userdata('KD_JNS_OP', null);

			$this->session->set_userdata('KD_PROPINSI_ASAL', null);
			$this->session->set_userdata('KD_DATI2_ASAL', null);
			$this->session->set_userdata('KD_KECAMATAN_ASAL', null);
			$this->session->set_userdata('KD_KELURAHAN_ASAL', null);
			$this->session->set_userdata('KD_BLOK_ASAL', null);
			$this->session->set_userdata('NO_URUT_ASAL', null);
			$this->session->set_userdata('KD_JNS_OP_ASAL', null);
			$this->session->set_userdata('NO_BNG', null);


			$this->session->set_userdata('t_formulir', 'active');
			$this->session->set_userdata('t_spop', null);
			$this->session->set_userdata('t_lspop', null);
			$data['nop_asal']                   = '';
			$data['JNS_PELAYANAN']              = '';
			$NIP                                = $this->session->userdata('nip');
			$data['nomor']                      = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();
			return view('spop/form_spop', $data);
		}
	}

	function create_spop()
	{

		$KD_PROPINSI  	= $this->input->post('KD_PROPINSI');
		$KD_DATI2     	= $this->input->post('KD_DATI2');
		$KD_KECAMATAN 	= $this->input->post('KD_KECAMATAN');
		$KD_KELURAHAN 	= $this->input->post('KD_KELURAHAN');
		$KD_BLOK      	= $this->input->post('KD_BLOK');
		$NO_URUT_NOP    = $this->input->post('NO_URUT_NOP');
		$KD_JNS_OP    	= $this->input->post('KD_JNS_OP');
		$NOP 			= $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

		$KD_JNS_PELAYANAN = substr($this->input->post('KD_JNS_PELAYANAN_SPOP'), -1);
		// $NO_FORMULIR    = $_POST['NO_FORMULIR_SPOP'];
		$NO_FORMULIR = $_POST['THN_PELAYANAN'] . $_POST['BUNDEL_PELAYANAN'] . $_POST['NO_URUT_DALAM'] . $_POST['NO_URUT'];
		$KTP_ID  		= $this->input->post('KTP_ID');
		$PEKERJAAN     	= $this->input->post('PEKERJAAN');

		$KD_PROPINSI_ASAL  = $this->input->post('KD_PROPINSI_ASAL');
		$KD_DATI2_ASAL     = $this->input->post('KD_DATI2_ASAL');
		$KD_KECAMATAN_ASAL = $this->input->post('KD_KECAMATAN_ASAL');
		$KD_KELURAHAN_ASAL = $this->input->post('KD_KELURAHAN_ASAL');
		$KD_BLOK_ASAL      = $this->input->post('KD_BLOK_ASAL');
		$NO_URUT_ASAL      = $this->input->post('NO_URUT_ASAL');
		$KD_JNS_OP_ASAL    = $this->input->post('KD_JNS_OP_ASAL');
		$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;

		// $NOP_ASAL 		= $_POST['NOP_ASAL'];
		$NPWP  			= $this->input->post('NPWP');
		$JALAN 			= $this->input->post('JALAN');
		$RW_WP 			= $this->input->post('RW_WP');
		$RT_WP 			= $this->input->post('RT_WP');
		$DATI  			= $this->input->post('DATI');
		$PERSIL			= $this->input->post('PERSIL');
		$JALAN_OP	    = $this->input->post('JALAN_OP');
		$BLOK_KAV_OP    = $this->input->post('BLOK_KAV_OP');
		$ZNT    		= $this->input->post('ZNT');
		$LUAS_TANAH     = $this->input->post('LUAS_TANAH');
		$TGL_PENDATAAN  = date('Y-m-d', strtotime($this->input->post('TGL_PENDATAAN', TRUE)));
		$TGL_PENELITIAN = date('Y-m-d', strtotime($this->input->post('TGL_PENELITIAN', TRUE)));
		$STATUS_WP    	= $this->input->post('STATUS_WP');
		$NAMA    		= $this->input->post('NAMA');
		$TELP    		= $this->input->post('TELP');
		$BLOK_KAV_WP    = $this->input->post('BLOK_KAV_WP');
		$KELURAHAN    	= $this->input->post('KELURAHAN');
		$KODE_POS    	= $this->input->post('KODE_POS');
		$RW_OP    		= $this->input->post('RW_OP');
		$RT_OP	    	= $this->input->post('RT_OP');
		// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$CABANG    		= $this->input->post('CABANG');
		$JENIS_TANAH    = $this->input->post('JENIS_TANAH');
		$EXP0			= explode('|', $this->input->post('NIP_PENDATA'));
		$EXP1			= explode('|', $this->input->post('NIP_PENELITI'));
		$NIP_PENDATA    = $EXP0[0];
		$NIP_PENELITI   = $EXP1[0];

		// if ($_POST['KD_JNS_PELAYANAN'] == 1) {
		$datab = array(
			'SUBJEK_PAJAK_ID'	  => $KTP_ID,
			'NM_WP'     	 	  => $NAMA,
			'JALAN_WP'    	 	  => $JALAN,
			'BLOK_KAV_NO_WP' 	  => $BLOK_KAV_WP,
			'RW_WP'     	 	  => $RW_WP,
			'RT_WP' 		 	  => $RT_WP,
			'KELURAHAN_WP' 	 	  => $KELURAHAN,
			'KOTA_WP'      	 	  => $DATI,
			'KD_POS_WP'      	  => $KODE_POS,
			'TELP_WP'    	 	  => $TELP,
			'NPWP'     		 	  => $NPWP,
			'STATUS_PEKERJAAN_WP' => $PEKERJAAN,
		);


		// cek subjek pajak
		$sd = $this->db->query("select * from DAT_SUBJEK_PAJAK_TEMP where subjek_pajak_id='" . $KTP_ID . "'")->row();
		/* $this->db->where('SUBJEK_PAJAK_ID',$id);
		$sd=$this->db->get('DAT_SUBJEK_PAJAK_TEMP')->row(); */
		if (empty($sd)) {
			$this->db->insert('DAT_SUBJEK_PAJAK_TEMP', $datab);
		} else {
			$this->db->where('SUBJEK_PAJAK_ID', $KTP_ID);
			$this->db->update('DAT_SUBJEK_PAJAK_TEMP', $datab);
		}

		$this->db->query("INSERT INTO tbl_spop ( NO_FORMULIR,JNS_TRANSAKSI,NOP_PROSES,NOP_ASAL,SUBJEK_PAJAK_ID,NM_WP,
													 JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,
													 KD_POS_WP,NPWP,TELP_WP,STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,
													 KD_ZNT,LUAS_BUMI,JNS_BUMI,
													 TGL_PENDATAAN_OP,NIP_PENDATA,TGL_PEMERIKSAAN_OP,NIP_PEMERIKSA_OP,TGL_PEREKAMAN_OP, NIP_PEREKAM_OP)  
											VALUES ( '$NO_FORMULIR','$KD_JNS_PELAYANAN','$NOP','$NOP_ASAL','$KTP_ID','$NAMA',
													 '$JALAN','$BLOK_KAV_WP','$RW_WP','$RT_WP','$KELURAHAN','$DATI',
													 '$KODE_POS','$NPWP','$TELP','$PEKERJAAN','$PERSIL','$JALAN_OP','$BLOK_KAV_OP','$RW_OP','$RT_OP','$CABANG','$STATUS_WP',
													 '$ZNT','$LUAS_TANAH','$JENIS_TANAH',
													  TO_DATE('$TGL_PENDATAAN','yyyy-mm-dd'),'$NIP_PENDATA',TO_DATE('$TGL_PENELITIAN','yyyy-mm-dd'),'$NIP_PENELITI',TO_DATE('$TGL_PENDATAAN','yyyy-mm-dd'),'$NIP_PENDATA')");
		// } else {
		// }
		/*$this->db->query("INSERT INTO DAT_OBJEK_PAJAK (KD_PROPINSI,KD_DATI2,KD_KECAMATAN,KD_KELURAHAN,KD_BLOK,NO_URUT,KD_JNS_OP,
										  SUBJEK_PAJAK_ID,NO_FORMULIR_SPOP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,
										  KD_STATUS_CABANG,KD_STATUS_WP,TOTAL_LUAS_BUMI,TOTAL_LUAS_BNG,NJOP_BUMI,NJOP_BNG,
										  
										  STATUS_PETA_OP,JNS_TRANSAKSI_OP,
										  TGL_PENDATAAN_OP,NIP_PENDATA,
										  TGL_PEMERIKSAAN_OP,NIP_PEMERIKSA_OP,
										  TGL_PEREKAMAN_OP,NIP_PEREKAM_OP) 
										  VALUES ('$KD_PROPINSI','$KD_DATI2','$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT_NOP','$KD_JNS_OP',
										  '$KTP_ID','$NO_FORMULIR','$PERSIL','$JALAN','$BLOK_KAV_OP','$RW_OP','$RT_OP',
										  '$CABANG','$STATUS_WP','$LUAS_TANAH','0','0','0',
										  
										  '0','$KD_JNS_PELAYANAN',
										  to_date('20-AUG-09','DD-MON-RR'),'060000000000000000',
										  to_date('20-AUG-09','DD-MON-RR'),'060000000000000000',
										  to_date('13-NOV-09','DD-MON-RR'),'060000000000000000')");*/
		$this->db->query("UPDATE P_NOMOR SET STATUS = '1' WHERE  NO_FORMULIR= '$NO_FORMULIR' AND NOP = '$NOP'");
		redirect('spop/form_spop');
	}

	function create_lspop()
	{
		// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$NIP 						= $this->session->userdata('nip');
		// $KD_JNS_PELAYANAN			= $this->input->post('KD_JNS_PELAYANAN');
		$KD_JNS_PELAYANAN = substr($this->input->post('KD_JNS_PELAYANAN_LSPOP'), -1);

		$KD_PROPINSI  				= $this->input->post('KD_PROPINSI');
		$KD_DATI2     				= $this->input->post('KD_DATI2');
		$KD_KECAMATAN 				= $this->input->post('KD_KECAMATAN');
		$KD_KELURAHAN 				= $this->input->post('KD_KELURAHAN');
		$KD_BLOK      				= $this->input->post('KD_BLOK');
		$NO_URUT_NOP     			= $this->input->post('NO_URUT_NOP');
		$KD_JNS_OP    				= $this->input->post('KD_JNS_OP');
		$NOP 			            = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;
		// $NO_FORMULIR    			= $this->input->post('NO_FORMULIR_SPOP');
		$NO_FORMULIR = $_POST['THN_PELAYANAN'] . $_POST['BUNDEL_PELAYANAN'] . $_POST['NO_URUT_DALAM'] . $_POST['NO_URUT'];
		$NOP_ASAL 					= $this->input->post('NOP_ASAL');
		$NO_BNG  					= $this->input->post('NO_BNG');
		////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$KD_JPB  					= $this->input->post('KD_JPB');
		$LUAS_BNG     				= $this->input->post('LUAS_BNG');
		$JML_LANTAI_BNG  			= $this->input->post('JML_LANTAI_BNG');
		$THN_DIBANGUN_BNG  			= $this->input->post('THN_DIBANGUN_BNG');
		$THN_RENOVASI_BNG  			= $this->input->post('THN_RENOVASI_BNG');
		$KONDISI_BNG  				= $this->input->post('KONDISI_BNG');
		$JNS_KONSTRUKSI_BNG 		= $this->input->post('JNS_KONSTRUKSI_BNG');
		$JNS_ATAP_BNG  				= $this->input->post('JNS_ATAP_BNG');
		$KD_DINDING  				= $this->input->post('KD_DINDING');
		$KD_LANTAI  				= $this->input->post('KD_LANTAI');
		$KD_LANGIT_LANGIT  			= $this->input->post('KD_LANGIT_LANGIT');
		$DAYA_LISTRIK  				= $this->input->post('DAYA_LISTRIK');
		$ACSPLIT  					= $this->input->post('ACSPLIT');
		$ACWINDOW  					= $this->input->post('ACWINDOW');
		$ACSENTRAL  				= $this->input->post('ACSENTRAL');
		$LUAS_KOLAM  				= $this->input->post('LUAS_KOLAM');
		$FINISHING_KOLAM  			= $this->input->post('FINISHING_KOLAM');
		//////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$TGL_PENDATAAN  			= date('Y-m-d', strtotime($this->input->post('TGL_PENDATAAN', TRUE)));
		$NIP_PENDATA  				= $this->input->post('NIP_PENDATA');
		$TGL_PEMERIKSAAN  			= date('Y-m-d', strtotime($this->input->post('TGL_PEMERIKSAAN', TRUE)));
		$NIP_PEMERIKSA  			= $this->input->post('NIP_PEMERIKSA');

		$LIFT_PENUMPANG  			= $this->input->post('LIFT_PENUMPANG');
		$LIFT_KAPSUL  				= $this->input->post('LIFT_KAPSUL');
		$LIFT_BARANG  				= $this->input->post('LIFT_BARANG');
		////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\			
		$TGG_BERJALAN_A  			= $this->input->post('TGG_BERJALAN_A');
		$TGG_BERJALAN_B  			= $this->input->post('TGG_BERJALAN_B');
		$PJG_PAGAR  				= $this->input->post('PJG_PAGAR');
		$BHN_PAGAR  				= $this->input->post('BHN_PAGAR');
		$HYDRANT  					= $this->input->post('HYDRANT');
		$SPRINKLER  				= $this->input->post('SPRINKLER');
		$FIRE_ALARM  				= $this->input->post('FIRE_ALARM');
		$JML_PABX  					= $this->input->post('JML_PABX');
		$SUMUR_ARTESIS  			= $this->input->post('SUMUR_ARTESIS');
		$LUAS_PERKERASAN_RINGAN  	= $this->input->post('LUAS_PERKERASAN_RINGAN');
		$LUAS_PERKERASAN_SEDANG  	= $this->input->post('LUAS_PERKERASAN_SEDANG');
		$LUAS_PERKERASAN_BERAT  	= $this->input->post('LUAS_PERKERASAN_BERAT');
		$LUAS_PERKERASAN_DG_TUTUP  	= $this->input->post('LUAS_PERKERASAN_DG_TUTUP');

		$LAP_TENIS_LAMPU_BETON  	= $this->input->post('LAP_TENIS_LAMPU_BETON');
		$LAP_TENIS_LAMPU_ASPAL  	= $this->input->post('LAP_TENIS_LAMPU_ASPAL');
		$LAP_TENIS_LAMPU_RUMPUT  	= $this->input->post('LAP_TENIS_LAMPU_RUMPUT');
		$LAP_TENIS_BETON  			= $this->input->post('LAP_TENIS_BETON');
		$LAP_TENIS_ASPAL  			= $this->input->post('LAP_TENIS_ASPAL');
		$LAP_TENIS_RUMPUT  			= $this->input->post('LAP_TENIS_RUMPUT');

		$this->db->trans_begin();
		$this->db->query("INSERT INTO TBL_LSPOP (NO_FORMULIR,JNS_TRANSAKSI,NOP,NO_BNG,
											KD_JPB,THN_DIBANGUN_BNG,THN_RENOVASI_BNG,LUAS_BNG,
											JML_LANTAI_BNG,KONDISI_BNG,JNS_KONSTRUKSI_BNG,JNS_ATAP_BNG,KD_DINDING,KD_LANTAI,KD_LANGIT_LANGIT,
											DAYA_LISTRIK,ACSPLIT,ACWINDOW,ACSENTRAL,
											LUAS_KOLAM,FINISHING_KOLAM,
											LUAS_PERKERASAN_RINGAN,LUAS_PERKERASAN_SEDANG,LUAS_PERKERASAN_BERAT,LUAS_PERKERASAN_DG_TUTUP,
											LAP_TENIS_LAMPU_BETON,LAP_TENIS_LAMPU_ASPAL,LAP_TENIS_LAMPU_RUMPUT,LAP_TENIS_BETON,LAP_TENIS_ASPAL,LAP_TENIS_RUMPUT,
											LIFT_PENUMPANG,LIFT_KAPSUL,LIFT_BARANG,TGG_BERJALAN_A,TGG_BERJALAN_B,PJG_PAGAR,BHN_PAGAR,
											HYDRANT,SPRINKLER,FIRE_ALARM,JML_PABX,SUMUR_ARTESIS,NILAI_INDIVIDU,
											JPB3_8_TINGGI_KOLOM,JPB3_8_LEBAR_BENTANG,JPB3_8_DD_LANTAI,JPB3_8_KEL_DINDING,JPB3_8_MEZZANINE,
											JPB5_KLS_BNG,JPB5_LUAS_KAMAR,JPB5_LUAS_RNG_LAIN,
											JPB7_JNS_HOTEL,JPB7_BINTANG,JPB7_JML_KAMAR,JPB7_LUAS_KAMAR,JPB7_LUAS_RNG_LAIN,
											JPB13_KLS_BNG,JPB13_JML,JPB13_LUAS_KAMAR,JPB13_LUAS_RNG_LAIN,
											JPB15_LETAK_TANGKI,JPB15_KAPASITAS_TANGKI,JPB_LAIN_KLS_BNG,
											TGL_PENDATAAN_BNG,NIP_PENDATA_BNG,TGL_PEMERIKSAAN_BNG,NIP_PEMERIKSA_BNG,TGL_PEREKAMAN_BNG,NIP_PEREKAM_BNG) 
											VALUES ('$NO_FORMULIR','$KD_JNS_PELAYANAN','$NOP','$NO_BNG',
											'$KD_JPB','$THN_DIBANGUN_BNG','$THN_RENOVASI_BNG','$LUAS_BNG',
											'$JML_LANTAI_BNG','$KONDISI_BNG','$JNS_KONSTRUKSI_BNG','$JNS_ATAP_BNG','$KD_DINDING','$KD_LANTAI','$KD_LANGIT_LANGIT',
											'$DAYA_LISTRIK','$ACSPLIT','$ACWINDOW','$ACSENTRAL',
											'$LUAS_KOLAM','$FINISHING_KOLAM',
											'$LUAS_PERKERASAN_RINGAN','$LUAS_PERKERASAN_SEDANG','$LUAS_PERKERASAN_BERAT','$LUAS_PERKERASAN_DG_TUTUP',
											'$LAP_TENIS_LAMPU_BETON','$LAP_TENIS_LAMPU_ASPAL','$LAP_TENIS_LAMPU_RUMPUT','$LAP_TENIS_BETON','$LAP_TENIS_ASPAL','$LAP_TENIS_RUMPUT',
											'$LIFT_PENUMPANG','$LIFT_KAPSUL','$LIFT_BARANG','$TGG_BERJALAN_A','$TGG_BERJALAN_B','$PJG_PAGAR','$BHN_PAGAR',
											'$HYDRANT','$SPRINKLER','$FIRE_ALARM','$JML_PABX','$SUMUR_ARTESIS','0',
											'','','','','',
											'','','',
											'','','','','',
											'','','','',
											'','','',
											to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP_PENDATA',to_date('$TGL_PEMERIKSAAN','YYYY-MM-DD'),'$NIP_PEMERIKSA',to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP')");
		$this->db->query("INSERT INTO DAT_OP_BANGUNAN 
												(KD_PROPINSI,KD_DATI2,KD_KECAMATAN,
												KD_KELURAHAN,KD_BLOK,NO_URUT,
												KD_JNS_OP,NO_BNG,KD_JPB,
												NO_FORMULIR_LSPOP,THN_DIBANGUN_BNG,THN_RENOVASI_BNG,
												LUAS_BNG,JML_LANTAI_BNG,KONDISI_BNG,JNS_KONSTRUKSI_BNG,JNS_ATAP_BNG,KD_DINDING,KD_LANTAI,KD_LANGIT_LANGIT,
												NILAI_SISTEM_BNG,JNS_TRANSAKSI_BNG,
												TGL_PENDATAAN_BNG,NIP_PENDATA_BNG,
												TGL_PEMERIKSAAN_BNG,NIP_PEMERIKSA_BNG,
												TGL_PEREKAMAN_BNG,NIP_PEREKAM_BNG) 
												VALUES 
												('$KD_PROPINSI','$KD_DATI2','$KD_KECAMATAN',
												'$KD_KELURAHAN','$KD_BLOK','$NO_URUT_NOP',
												'$KD_JNS_OP','$NO_BNG','$KD_JPB',
												'$NO_FORMULIR','$THN_DIBANGUN_BNG','$THN_RENOVASI_BNG',
												'$LUAS_BNG','$JML_LANTAI_BNG','$KONDISI_BNG','$JNS_KONSTRUKSI_BNG','$JNS_ATAP_BNG','$KD_DINDING','$KD_LANTAI','$KD_LANGIT_LANGIT',
												'0','$KD_JNS_PELAYANAN',
												to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP_PENDATA',
												to_date('$TGL_PEMERIKSAAN','YYYY-MM-DD'),'$NIP_PEMERIKSA',
												to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP')");
		$this->db->query("UPDATE P_NOMOR SET STATUS = '1' WHERE  NO_FORMULIR= '$NO_FORMULIR' AND NOP = '$NOP'");
		$rk = $this->db->query("select NO_FORMULIR from p_nomor where nop='$NOP' and keterangan_nomor='LSPOP' AND STATUS='0' order by no_bangunan asc")->row();

		$this->db->trans_complete();

		echo "<pre>";
		print_r($this->db->trans_status());
		echo "</pre>";

		if ($this->db->trans_status() === TRUE) {
			// if (count($rk)<=0) {

		} else {
			// redirect('spop/form_lspop');
		}
		redirect('lspop/form_spop');
	}

	function generate_nomor()
	{
		$NIP = $this->session->userdata('nip');
		$data['nomor'] = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();
		$this->template->load('template', 'spop/form_generate_nomor', $data);
	}


	function proses_generate_nomor()
	{
		$NIP = $this->session->userdata('nip');
		$TAHUN_AJUAN = $this->input->post('TAHUN_AJUAN');
		$JNS_FORMULIR = $this->input->post('KD_JNS_FORMULIR');
		$NO_BANGUNAN = $this->input->post('NO_BNG');

		$KD_PROPINSI  = $this->input->post('KD_PROPINSI');
		$KD_DATI2     = $this->input->post('KD_DATI2');
		$KD_KECAMATAN = $this->input->post('KD_KECAMATAN');
		$KD_KELURAHAN = $this->input->post('KD_KELURAHAN');
		$KD_BLOK      = $this->input->post('KD_BLOK');
		$NO_URUT_NOP  = $this->input->post('NO_URUT_NOP');
		$KD_JNS_OP    = $this->input->post('KD_JNS_OP');
		$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

		$rk = $this->db->query("SELECT  $TAHUN_AJUAN TAHUN ,  
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) < 200 THEN   MAX( TO_NUMBER(BUNDEL))  ELSE MAX( TO_NUMBER(BUNDEL))+1 END   BUNDEL_PELAYANAN ,
									 CASE WHEN   MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(NO_URUT)) +1 ELSE 1 END  NO_URUT
                                FROM P_NOMOR 
                                WHERE TAHUN=$TAHUN_AJUAN
                                AND jenis_pelayanan='$JNS_FORMULIR'
                                AND BUNDEL IN (SELECT  MAX( TO_NUMBER(BUNDEL)) BUNDEL_PELAYANAN
                                FROM P_NOMOR WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY') AND jenis_pelayanan='$JNS_FORMULIR')")->row();

		$TAHUN  = $rk->TAHUN;
		$JENIS_PELAYANAN = $JNS_FORMULIR;
		$BUNDEL = sprintf('%03s', $rk->BUNDEL_PELAYANAN);
		if ($rk->NO_URUT % 2 == 0) {
			$URUT  = sprintf('%03s', $rk->NO_URUT + 1);
		} else {
			$URUT  = sprintf('%03s', $rk->NO_URUT);
		}
		$NO_FORM = $TAHUN . $JENIS_PELAYANAN . $BUNDEL . $URUT;
		$this->db->query("INSERT INTO P_NOMOR ( NO_FORMULIR,TAHUN,JENIS_PELAYANAN,BUNDEL,NO_URUT,NOP,
 														NIP_PEMBUAT,TGL_PEREKAMAN,KETERANGAN_NOMOR,NO_BANGUNAN)  
											VALUES( '$NO_FORM','$TAHUN','$JENIS_PELAYANAN','$BUNDEL','$URUT','$NOP',
													'$NIP',SYSDATE,'SPOP','0')");

		if ($NO_BANGUNAN != '' or $NO_BANGUNAN != 0) {
			$NO_BANGUNAN1 = $NO_BANGUNAN + 1;
			for ($i = 1; $i < $NO_BANGUNAN1; $i++) {
				$rk = $this->db->query("SELECT  $TAHUN_AJUAN TAHUN ,  
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(BUNDEL))  ELSE MAX( TO_NUMBER(BUNDEL))+1 END   BUNDEL_PELAYANAN ,
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(NO_URUT)) +1 ELSE 1 END  NO_URUT
                                FROM P_NOMOR 
                                WHERE TAHUN=$TAHUN_AJUAN
                                AND jenis_pelayanan='$JNS_FORMULIR'
                                AND BUNDEL IN (SELECT  MAX( TO_NUMBER(BUNDEL)) BUNDEL_PELAYANAN
                                FROM P_NOMOR WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY') AND jenis_pelayanan='$JNS_FORMULIR')")->row();
				$TAHUN = $rk->TAHUN;
				$JENIS_PELAYANAN = $JNS_FORMULIR;
				$BUNDEL = sprintf('%03s', $rk->BUNDEL_PELAYANAN);
				if ($rk->NO_URUT % 2 == '0') {
					$URUT  = sprintf('%03s', $rk->NO_URUT);
				} else {
					$URUT  = sprintf('%03s', $rk->NO_URUT + 1);
				}
				$NO_FORM = $TAHUN . $JENIS_PELAYANAN . $BUNDEL . $URUT;
				$this->db->query("INSERT INTO P_NOMOR ( NO_FORMULIR,TAHUN,JENIS_PELAYANAN,BUNDEL,NO_URUT,NOP,
 														NIP_PEMBUAT,TGL_PEREKAMAN,KETERANGAN_NOMOR,NO_BANGUNAN)  
											VALUES( '$NO_FORM','$TAHUN','$JENIS_PELAYANAN','$BUNDEL','$URUT','$NOP',
													'$NIP',SYSDATE,'LSPOP','$i')");
			}
		}
		redirect('spop/form_spop?tab=formulir&tahun=' . $TAHUN_AJUAN);
	}

	function getformulir()
	{
		if (isset($_POST['nop'])) {
			$NOP = $_POST['nop'];
			$exp = explode('=', $NOP);
			$id = str_replace('|', '', $exp[0]);
			$rk = $this->db->query("select tahun,jenis_pelayanan,bundel,no_urut from p_nomor where nop='$id' and keterangan_nomor='SPOP' AND STATUS='0'")->row();

			echo $rk->TAHUN . '|' . $rk->JENIS_PELAYANAN . '|' . $rk->BUNDEL . '|' . $rk->NO_URUT;
		}
	}

	function getformulirlspop()
	{
		if (isset($_POST['nop'])) {
			$NOP = $_POST['nop'];
			$exp = explode('=', $NOP);
			$id = str_replace('|', '', $exp[0]);
			$rk = $this->db->query("select tahun,jenis_pelayanan,bundel,no_urut from p_nomor where nop='$id' and keterangan_nomor='LSPOP' AND STATUS='0'")->row();

			echo $rk->TAHUN . '|' . $rk->JENIS_PELAYANAN . '|' . $rk->BUNDEL . '|' . $rk->NO_URUT;
		}
	}

	function getNop()
	{
		if (isset($_POST['nop'])) {
			$nop = $_POST['nop'];
			$exp = explode('=', $nop);
			$id = str_replace('|', '', $exp[0]);
			$jns = $exp[1];
			$rk = $this->db->query("select * from dat_objek_pajak
									where subjek_pajak_id='$id'")->row();
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			if (count($rk) >= '1') {
				$cek = '1';
			} else {
				$cek = '0';
			};
			echo $cek . '|' . $jns;
		}
	}

	function lookup_kelurahan()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select nm_kelurahan  from ref_kelurahan where nm_kelurahan like UPPER('%$keyword%')")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NM_KELURAHAN,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function lookup_dati2()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select nm_dati2  from ref_dati2 where nm_dati2 like UPPER('%$keyword%')")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NM_DATI2,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function lookup_karyawan()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select *  from pegawai where nm_pegawai like UPPER('%$keyword%') or nip like '%$keyword%'")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NIP . '|' . $row->NM_PEGAWAI,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function penilaian()
	{
		$data['kecamatan'] = $this->db->query("select kd_kecamatan,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan kode,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||' - '||nm_kecamatan nama
 															from ref_kecamatan where kd_dati2='07'")->result_array();
		$data['button'] = 'Penilaian';
		return view('spop/penilaian', $data);
	}
	function proses_penilaian()
	{
		$param  = $this->input->post('param');
		$tahun  = $this->input->post('tahun');
		$njoptkp = $this->input->post('njoptkp');
		$exp = explode('|', $param);


		$jumlah = count($exp);
		if ($njoptkp == 'true') {
		} else {
			for ($i = 0; $i < $jumlah; $i++) {
				$pecah = explode('.', $exp[$i]);
				error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
				$this->db->query("call penilaian_massal('$tahun','$pecah[0]','$pecah[1]','$pecah[2]','$pecah[3]')");
			}
		}
	}
	function tes_jason()
	{
		$param = $this->input->post('param');
		$exp = explode('|', $param);
		$jumlah = count($exp);
		for ($i = 0; $i < $jumlah; $i++) {
			$pecah = explode('.', $exp[$i]);
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			//echo '<pre>'; print_r($pecah[2].','.$pecah[3]); echo '</pre>';
			echo $pecah[2];
			echo $pecah[3];
			$data['data1']	= $this->db->query("select c.nm_kecamatan,b.nm_kelurahan, a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan||'.'||a.kd_kelurahan kode,count(*)jumlah 
			from dat_objek_pajak a
											JOIN ref_kelurahan b on a.kd_kelurahan=b.kd_kelurahan and a.kd_kecamatan=b.kd_kecamatan
											JOIN ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan 
											where a.kd_kecamatan in('$pecah[2]') and a.kd_kelurahan in('$pecah[3]')
											group by c.nm_kecamatan,b.nm_kelurahan, a.kd_kecamatan,a.kd_kelurahan,a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan")->result();
			$this->load->view('spop/ajax_penilaian', $data);
		}
	}


	function dataSpop()
	{
		// $data['data']=$this->db->query("select * from tbl_spop")->result();
		$nop = urldecode($this->input->get('NOP'));
		if ($nop <> '') {
			$this->db->select('*');
			$this->db->where('NOP_PROSES', str_replace('.', '', $this->input->get('NOP')));
			$data = $this->db->get('TBL_SPOP')->row();
			$this->db->select('*');
			$this->db->where('NOP', str_replace('.', '', $this->input->get('NOP')));
			$dat = $this->db->get('TBL_LSPOP')->row();
		} else {
			$data = array();
			$dat = array();
		}
		// echo $this->db->last_query();

		return view('spop/dataSpop', array('res' => $data, 'nop' => $nop, 'ls' => $dat));
	}

	function getform()
	{
		if (isset($_POST['nop'])) {
			$this->load->view('spop/ajax_perekaman_wp');
		}
	}
}
