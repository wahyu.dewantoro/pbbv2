<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Verlap extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);

        $this->load->model('Mbank');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function surat()
    {
        return view('verlap/index');
    }

    public function entri()
    {
        return view('verlap/entri');
    }

    function json_surat_tugas()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("V_SURAT_TUGAS.*,B.STATUS VERLAP");
        $this->datatables->from('V_SURAT_TUGAS');
        $this->datatables->join("(SELECT DISTINCT SURAT_TUGAS_ID,STATUS FROM SURAT_TUGAS_OBJEK
        WHERE STATUS=1) B", "B.SURAT_TUGAS_ID=V_SURAT_TUGAS.ID", "LEFT");
        $this->db->order_by('ID', 'DESC');
        return print_r($this->datatables->generate());
    }


    public function json_hasil_verlap()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }

        $this->load->library('datatables');
        $this->datatables->select("V_SURAT_TUGAS.ID, KODE, TGL_SURAT, NAMA_PEGAWAI, PANGKAT_GOLONGAN, JABATAN_PEGAWAI, NAMA_KABID, NIP_KABID, PANGKAT_GOLONGAN_KABID, KECAMATAN, OBJEK,nvl(SUM( CASE WHEN SURAT_TUGAS_OBJEK.STATUS IS NULL THEN 1 ELSE NULL END),0) BELUM, nvl(SUM(CASE WHEN SURAT_TUGAS_OBJEK.STATUS IS NOT NULL THEN 1 ELSE NULL END),0) SUDAH", false);
        $this->datatables->from("V_SURAT_TUGAS");
        $this->datatables->join("SURAT_TUGAS_OBJEK", "SURAT_TUGAS_OBJEK.SURAT_TUGAS_ID=V_SURAT_TUGAS.ID");
        $this->db->group_by("V_SURAT_TUGAS.ID, KODE, TGL_SURAT, NAMA_PEGAWAI, PANGKAT_GOLONGAN, JABATAN_PEGAWAI, NAMA_KABID, NIP_KABID, PANGKAT_GOLONGAN_KABID, KECAMATAN, OBJEK", false);
        $this->db->having("SUM(CASE WHEN SURAT_TUGAS_OBJEK.STATUS IS NOT NULL THEN 1 ELSE NULL END) = OBJEK");


        return print_r($this->datatables->generate());
    }

    function json_entri_verlap()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }

        $this->load->library('datatables');
        // $this->datatables->from('V_ENTRI_VERLAP');
        $this->datatables->select("V_SURAT_TUGAS.*,B.STATUS VERLAP");
        $this->datatables->from('V_SURAT_TUGAS');
        $this->datatables->join("(SELECT DISTINCT SURAT_TUGAS_ID,STATUS FROM SURAT_TUGAS_OBJEK
        WHERE STATUS=1) B", "B.SURAT_TUGAS_ID=V_SURAT_TUGAS.ID", "LEFT");
        $this->datatables->where("B.STATUS is null", '', false);
        $this->db->order_by('ID', 'DESC');
        return print_r($this->datatables->generate());
    }

    private  function getCode()
    {
        $q = $this->db->query("SELECT  case when max(kode) is null then '0000'  else substr(max(kode),5,4)   end kode 
                                    from surat_tugas
                                    where to_char(created_at,'yymm') =to_char(sysdate,'yymm') ")->row();

        $tmp = (int)$q->KODE + 1;

        $kd = sprintf("%04s", $tmp);


        date_default_timezone_set('Asia/Jakarta');
        return date('ym') . $kd;
    }


    public function tambahst()
    {
        $data = array(
            'action' => base_url('verlap/prosescreate'),
            'title' => 'Tambah surat tugas',
            'jenis' => '1'

        );
        return view('verlap/formst', $data);
    }

    public function editst($id)
    {
        $data = array(
            'action' => base_url('verlap/prosesupdate/' . $id),
            'title' => 'Edit surat tugas',
            'jenis' => '0',
            'surat' => $this->db->query("select * from surat_tugas where id=?", [$id])->row(),
            'objek' => $this->db->query("select surat_tugas_objek.*,get_kecamatan(kd_kecamatan) kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) kelurahan from surat_tugas_objek where surat_tugas_id=?", [$id])->result(),
        );
        return view('verlap/formst', $data);
    }

    public function prosesupdate($id)
    {

        $exp = explode(',', $this->input->post('list_objek'));
        $this->db->trans_start();
        // induk surat
        $surat = [
            'NAMA_PEGAWAI' => $this->input->post('nama'),
            'NIP_PEGAWAI' => $this->input->post('nip'),
            'PANGKAT_GOLONGAN' => $this->input->post('pangkat_golongan'),
            'JABATAN_PEGAWAI' => $this->input->post('jabatan'),
        ];
        $this->db->update('SURAT_TUGAS', $surat, ['ID' => $id]);
        $insert_id = $id;

        // objek surat tugas
        $this->db->delete('SURAT_TUGAS_OBJEK', ['SURAT_TUGAS_ID' => $id]);
        $exp = explode(',', $this->input->post('list_objek'));
        foreach ($exp as $obj) {
            $robj = splitNopelNop($obj);
            $detail = [
                'SURAT_TUGAS_ID' => $insert_id,
                'THN_PELAYANAN' => $robj['thn_pelayanan'],
                'BUNDEL_PELAYANAN' => $robj['bundel_pelayanan'],
                'NO_URUT_PELAYANAN' => $robj['no_urut_pelayanan'],
                'KD_PROPINSI' => $robj['kd_propinsi'],
                'KD_DATI2' => $robj['kd_dati2'],
                'KD_KECAMATAN' => $robj['kd_kecamatan'],
                'KD_KELURAHAN' => $robj['kd_kelurahan'],
                'KD_BLOK' => $robj['kd_blok'],
                'NO_URUT' => $robj['no_urut'],
                'KD_JNS_OP' => $robj['kd_jns_op'],
            ];
            $this->db->insert('SURAT_TUGAS_OBJEK', $detail);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('verlap/surat');
    }

    public function hapusst($id)
    {

        $this->db->trans_start();
        $this->db->where('SURAT_TUGAS_ID', $id);
        $this->db->delete('SURAT_TUGAS_OBJEK');

        $this->db->where('ID', $id);
        $this->db->delete('SURAT_TUGAS');

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di hapus";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di hapus";
        }

        // dd($msg);
        redirect('verlap/surat');
    }

    public function suratdt($id)
    {
        if ($id == "") {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }
        $surat = $this->db->query("SELECT * from V_SURAT_TUGAS where id=?", [$id])->row();

        if (!$surat) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }

        $objek = $this->db->query("SELECT surat_tugas_objek.*,get_kecamatan(kd_kecamatan) kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) kelurahan from surat_tugas_objek where surat_tugas_id=? order by thn_pelayanan asc , bundel_pelayanan asc,no_urut_pelayanan asc", [$id])->result();
        $data = [
            'title' => "Detail Surat Tugas",
            'surat' => $surat,
            'objek' => $objek
        ];

        return view('verlap/detailst', $data);
    }

    public function cetakst($id)
    {
        $this->output->enable_profiler(false);
        if ($id == "") {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }
        $surat = $this->db->query("SELECT * from V_SURAT_TUGAS where id=?", [$id])->row();

        if (!$surat) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }

        $objek = $this->db->query("SELECT surat_tugas_objek.*,get_kecamatan(kd_kecamatan) kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) kelurahan from surat_tugas_objek where surat_tugas_id=? order by thn_pelayanan asc , bundel_pelayanan asc,no_urut_pelayanan asc", [$id])->result();
        $data = [
            'title' => "Detail Surat Tugas",
            'surat' => $surat,
            'objek' => $objek
        ];

        $html = $this->load->view('verlap/cetak_surat_tugas', $data, true);
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [216, 270]]);

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output('surat_tugas_' . $surat->KODE . '.pdf', 'I');
    }

    public function objek_modal($jenis)
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }

        $this->load->library('datatables');

        $this->datatables->select("PST_DETAIL.THN_PELAYANAN||PST_DETAIL.BUNDEL_PELAYANAN||PST_DETAIL.NO_URUT_PELAYANAN||SPPT.KD_PROPINSI||SPPT.KD_DATI2||SPPT.KD_KECAMATAN||SPPT.KD_KELURAHAN||SPPT.KD_BLOK||SPPT.NO_URUT|| SPPT.KD_JNS_OP id, PST_DETAIL.THN_PELAYANAN,PST_DETAIL.BUNDEL_PELAYANAN,PST_DETAIL.NO_URUT_PELAYANAN,PST_DETAIL.KD_JNS_PELAYANAN,NM_JENIS_PELAYANAN,SPPT.KD_PROPINSI,SPPT.KD_DATI2,SPPT.KD_KECAMATAN,SPPT.KD_KELURAHAN,SPPT.KD_BLOK,SPPT.NO_URUT, SPPT.KD_JNS_OP,SPPT.NM_WP_SPPT,GET_KELURAHAN(SPPT.KD_KELURAHAN,SPPT.KD_KECAMATAN) NAMA_KELURAHAN,GET_KECAMATAN(SPPT.KD_KECAMATAN) NAMA_KECAMATAN", false);
        $this->datatables->from('PST_DETAIL', false);
        $this->datatables->join('PELAYANAN_VERIFIKASI', 'PST_DETAIL.THN_PELAYANAN=PELAYANAN_VERIFIKASI.THN_PELAYANAN AND PST_DETAIL.BUNDEL_PELAYANAN=PELAYANAN_VERIFIKASI.BUNDEL_PELAYANAN AND PST_DETAIL.NO_URUT_PELAYANAN=PELAYANAN_VERIFIKASI.NO_URUT_PELAYANAN', false);
        $this->datatables->join('SPPT', 'SPPT.KD_PROPINSI=PST_DETAIL.KD_PROPINSI_PEMOHON AND
                                        SPPT.KD_DATI2=PST_DETAIL.KD_DATI2_PEMOHON AND
                                        SPPT.KD_KECAMATAN=PST_DETAIL.KD_KECAMATAN_PEMOHON AND
                                        SPPT.KD_KELURAHAN=PST_DETAIL.KD_KELURAHAN_PEMOHON AND
                                        SPPT.KD_BLOK=PST_DETAIL.KD_BLOK_PEMOHON AND
                                        SPPT.NO_URUT=PST_DETAIL.NO_URUT_PEMOHON AND
                                        SPPT.KD_JNS_OP=PST_DETAIL.KD_JNS_OP_PEMOHON AND SPPT.THN_PAJAK_SPPT=PST_DETAIL.THN_PELAYANAN', false);
        $this->datatables->join('REF_JNS_PELAYANAN', 'REF_JNS_PELAYANAN.KD_JNS_PELAYANAN=PST_DETAIL.KD_JNS_PELAYANAN', false);
        if ($jenis == '1') {
            // LEFT JOIN SURAT_TUGAS_OBJEK E ON 
            $this->datatables->join('SURAT_TUGAS_OBJEK', 'PST_DETAIL.THN_PELAYANAN=SURAT_TUGAS_OBJEK.THN_PELAYANAN AND
                PST_DETAIL.BUNDEL_PELAYANAN=SURAT_TUGAS_OBJEK.BUNDEL_PELAYANAN AND
                PST_DETAIL.NO_URUT_PELAYANAN=SURAT_TUGAS_OBJEK.NO_URUT_PELAYANAN AND
                PST_DETAIL.KD_PROPINSI_PEMOHON=SURAT_TUGAS_OBJEK.KD_PROPINSI AND
                PST_DETAIL.KD_DATI2_PEMOHON=SURAT_TUGAS_OBJEK.KD_DATI2 AND
                PST_DETAIL.KD_KECAMATAN_PEMOHON=SURAT_TUGAS_OBJEK.KD_KECAMATAN AND
                PST_DETAIL.KD_KELURAHAN_PEMOHON=SURAT_TUGAS_OBJEK.KD_KELURAHAN AND
                PST_DETAIL.KD_BLOK_PEMOHON=SURAT_TUGAS_OBJEK.KD_BLOK AND
                PST_DETAIL.NO_URUT_PEMOHON=SURAT_TUGAS_OBJEK.NO_URUT AND
                PST_DETAIL.KD_JNS_OP_PEMOHON=SURAT_TUGAS_OBJEK.KD_JNS_OP', 'left');
            $this->db->where("SURAT_TUGAS_OBJEK.THN_PELAYANAN IS NULL", null, false);
        }
        $this->datatables->where('PELIMPAHAN', '1');
        // $this->db->order_by("PST_DETAIL.THN_PELAYANAN||PST_DETAIL.BUNDEL_PELAYANAN||PST_DETAIL.NO_URUT_PELAYANAN","ASC");
        return print_r($this->datatables->generate());
    }

    public function prosescreate()
    {
        $kode = $this->getCode();
        /*  $arr = [
            'kode' => $kode,
            'post' => $this->input->post(),
            'data' => [
                'KODE' => $kode,
                'NAMA_PEGAWAI' => $this->input->post('nama'),
                'NIP_PEGAWAI' => $this->input->post('nip'),
                'PANGKAT_GOLONGAN' => $this->input->post('pangkat_golongan'),
                'JABATAN_PEGAWAI' => $this->input->post('jabatan'),
                'NAMA_KABID' => 'Drs. Jajok Sumanrianto',
                'NIP_KABID' => '19680131 199003 1 004',
                'PANGKAT_GOLONGAN_KABID' => 'Pembina',
                'TGL_SURAT' => date('Ymd'),
            ]
        ]; */
        $exp = explode(',', $this->input->post('list_objek'));
        $this->db->trans_start();
        // induk surat
        $surat = [

            'KODE' => $kode,
            'NAMA_PEGAWAI' => $this->input->post('nama'),
            'NIP_PEGAWAI' => $this->input->post('nip'),
            'PANGKAT_GOLONGAN' => $this->input->post('pangkat_golongan'),
            'JABATAN_PEGAWAI' => $this->input->post('jabatan'),
            'NAMA_KABID' => 'Drs. Jajok Sumanrianto',
            'NIP_KABID' => '19680131 199003 1 004',
            'PANGKAT_GOLONGAN_KABID' => 'Pembina',
            'CREATED_BY' => $this->session->userdata('nip'),
        ];
        $id = $this->db->query("select sq_surat_tugas.NEXTVAL id from dual ")->row()->ID;

        $tgl = date('Ymd');
        $this->db->set('ID', $id);
        $this->db->set('CREATED_AT', 'sysdate', false);
        $this->db->set('TGL_SURAT', "to_date(" . $tgl . ",'yyyymmdd')", false);
        $this->db->insert('SURAT_TUGAS', $surat);
        $insert_id = $id;

        // objek surat tugas
        $list = [];
        $exp = explode(',', $this->input->post('list_objek'));
        foreach ($exp as $obj) {
            $robj = splitNopelNop($obj);
            $detail = [
                'SURAT_TUGAS_ID' => $insert_id,
                'THN_PELAYANAN' => $robj['thn_pelayanan'],
                'BUNDEL_PELAYANAN' => $robj['bundel_pelayanan'],
                'NO_URUT_PELAYANAN' => $robj['no_urut_pelayanan'],
                'KD_PROPINSI' => $robj['kd_propinsi'],
                'KD_DATI2' => $robj['kd_dati2'],
                'KD_KECAMATAN' => $robj['kd_kecamatan'],
                'KD_KELURAHAN' => $robj['kd_kelurahan'],
                'KD_BLOK' => $robj['kd_blok'],
                'NO_URUT' => $robj['no_urut'],
                'KD_JNS_OP' => $robj['kd_jns_op'],
            ];
            $this->db->insert('SURAT_TUGAS_OBJEK', $detail);
            // $list[] = $detail;
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('verlap/surat');
    }

    public function formverlap($id)
    {
        if ($id == "") {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }

        // $objek = $this->db->query("SELECT * from V_ENTRI_VERLAP where id=?", [$id])->row();
        // $this->db->where('ID', $id);
        // $objek = $this->db->get('V_ENTRI_VERLAP')->row();
        /* $objek = $this->db->query("SELECT V_ENTRI_VERLAP.*,DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID,NM_WP,TOTAL_LUAS_BUMI LUAS_BUMI,TOTAL_LUAS_BNG LUAS_BNG,GET_KELURAHAN(DAT_OBJEK_PAJAK.KD_KELURAHAN,DAT_OBJEK_PAJAK.KD_KECAMATAN) KELURAHAN,GET_KECAMATAN(DAT_OBJEK_PAJAK.KD_KECAMATAN) KECAMATAN
        from V_ENTRI_VERLAP
        join dat_objek_pajak on  v_entri_verlap.KD_PROPINSI=dat_objek_pajak.KD_PROPINSI and
        v_entri_verlap.KD_DATI2=dat_objek_pajak.KD_DATI2 and
        v_entri_verlap.KD_KECAMATAN=dat_objek_pajak.KD_KECAMATAN and
        v_entri_verlap.kd_kelurahan=dat_objek_pajak.kd_kelurahan and
        v_entri_verlap.KD_BLOK=dat_objek_pajak.KD_BLOK and
        v_entri_verlap.NO_URUT=dat_objek_pajak.NO_URUT and
        v_entri_verlap.KD_JNS_OP=dat_objek_pajak.KD_JNS_OP 
        join dat_subjek_pajak on trim(dat_objek_pajak.subjek_pajak_id)=trim(dat_subjek_pajak.subjek_pajak_id)
        where V_ENTRI_VERLAP.ID=?", [$id])->row(); */


        // $surat = $this->db->query("SELECT * from V_SURAT_TUGAS where id=?", [$id])->row();
        $this->db->where('id', $id, false);
        $surat = $this->db->get('V_SURAT_TUGAS')->row();
        if (!$surat) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }
        $objek = $this->db->query("SELECT V_ENTRI_VERLAP.*,DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID,NM_WP,TOTAL_LUAS_BUMI LUAS_BUMI,TOTAL_LUAS_BNG LUAS_BNG,GET_KELURAHAN(DAT_OBJEK_PAJAK.KD_KELURAHAN,DAT_OBJEK_PAJAK.KD_KECAMATAN) KELURAHAN,GET_KECAMATAN(DAT_OBJEK_PAJAK.KD_KECAMATAN) KECAMATAN
        from V_ENTRI_VERLAP
        join dat_objek_pajak on  v_entri_verlap.KD_PROPINSI=dat_objek_pajak.KD_PROPINSI and
        v_entri_verlap.KD_DATI2=dat_objek_pajak.KD_DATI2 and
        v_entri_verlap.KD_KECAMATAN=dat_objek_pajak.KD_KECAMATAN and
        v_entri_verlap.kd_kelurahan=dat_objek_pajak.kd_kelurahan and
        v_entri_verlap.KD_BLOK=dat_objek_pajak.KD_BLOK and
        v_entri_verlap.NO_URUT=dat_objek_pajak.NO_URUT and
        v_entri_verlap.KD_JNS_OP=dat_objek_pajak.KD_JNS_OP 
        join dat_subjek_pajak on trim(dat_objek_pajak.subjek_pajak_id)=trim(dat_subjek_pajak.subjek_pajak_id)
        where surat_tugas_id=?", [$id])->result();
        $data = [
            'objek' => $objek,
            'surat' => $surat,
            'title' => "Form Hasil Penlitian"
        ];
        return view('verlap/form_entri', $data);
    }


    public function prosesentriverlap($id)
    {
        // dd($this->input->post());
        $rnop = $this->input->post('nop');
        $this->db->trans_start();
        foreach ($rnop as $key => $vnop) {
            $exnop = explode('.', $vnop);
            $data = [
                'KD_PROPINSI' => trim($exnop[0]),
                'KD_DATI2' => trim($exnop[1]),
                'KD_KECAMATAN' => trim($exnop[2]),
                'KD_KELURAHAN' => trim($exnop[3]),
                'KD_BLOK' => trim($exnop[4]),
                'NO_URUT' => trim($exnop[5]),
                'KD_JNS_OP' => trim($exnop[6]),
                'NM_WP' => strtoupper($this->input->post('nama_wp')[$key]),
                'L_BUMI' => str_replace(',', '.', $this->input->post('lbumi')[$key]),
                'L_BNG' => str_replace(',', '.', $this->input->post('lbng')[$key] ?? 0),
                'KETERANGAN' => $this->input->post('keterangan')[$key],
                'SURAT_TUGAS_OBJEK_ID' => $this->input->post('SURAT_TUGAS_OBJEK_ID')[$key],
                'CREATED_BY' => $this->session->userdata('nip'),
            ];
            $this->db->set('CREATED_AT', 'sysdate', false);
            // dd($data);
            // die();
            $this->db->insert('SURAT_TUGAS_VERLAP', $data);
        }

        $this->db->where('SURAT_TUGAS_ID', $id);
        $this->db->set('STATUS', 1);
        $this->db->update('SURAT_TUGAS_OBJEK');


        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }
        redirect('verlap/entri');
    }

    public function hasil()
    {
        return view('verlap/hasil');
    }

    public function verlapdt($id)
    {
        if ($id == "") {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }
        $surat = $this->db->query("SELECT * from V_SURAT_TUGAS where id=?", [$id])->row();

        if (!$surat) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }

        // $objek = JoinTable('SURAT_TUGAS_OBJEK', ['surat_tugas_id' => $surat->ID])->result();
        // dd($objek);

        // dd($surat);
        $data = [
            'surat' => $surat,
            'objek' => $objek = JoinTable('SURAT_TUGAS_OBJEK', ['surat_tugas_id' => $surat->ID])->result(),
            'title' => 'Hasil penelitian lapangan'
        ];

        return view('verlap/detail_hasil_verlap', $data);
    }

    public function print_uraian($id)
    {
        if (!isset($id)) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }
        $surat = $this->db->query("SELECT * from V_SURAT_TUGAS where id=?", [$id])->row();

        if (!$surat) {
            show_error('Halaman tidak di temukan', '404', $heading = 'An Error Was Encountered');
        }

        $data = [
            'surat' => $surat,
            'objek' => JoinTable('SURAT_TUGAS_OBJEK', ['surat_tugas_id' => $surat->ID], "SURAT_TUGAS_OBJEK.*,get_kelurahan(kd_kelurahan,kd_kecamatan) kelurahan,get_kecamatan(kd_kecamatan) kecamatan")->result(),
            'title' => 'Hasil penelitian lapangan'
        ];
        $uraian = $this->load->view('verlap/cetak_uraian_verlap', $data, true);
        $pengantar = $this->load->view('verlap/cetak_pengantar_verlap', ['surat' => $surat], true);

        // $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
        $mpdf = new \Mpdf\Mpdf(['format' => 'Legal']);

        // $mpdf->SetDisplayMode('fullpage');
        // $mpdf->WriteHTML($pengantar);
        // 
        // $mpdf->AddPage('L');
        $mpdf->WriteHTML($uraian);
        $mpdf->Output();
        // $mpdf->Output('uraian_verlap' . $surat->KODE . '.pdf', 'I');
    }
}
