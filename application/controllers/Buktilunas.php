<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
    require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

    use Box\Spout\Writer\WriterFactory;
    use Box\Spout\Common\Type;
    use Box\Spout\Writer\Style\StyleBuilder;
    

class Buktilunas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);

        // $this->load->model('Mbank');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function index(){
        $data['action_cek'] = 'buktilunas/index';
        $data['nop'] = '';
        if(isset($_GET['nop'])){
            $data['nop'] = $_GET['nop'];
            $nop = $_GET['nop'];
            $kd_propinsi= substr($nop, 0, 2);
            $kd_dati2=  substr($nop, 2, 2);
            $kd_kecamatan=substr($nop, 4, 3);
            $kd_kelurahan=substr($nop, 7, 3);
            $kd_blok=substr($nop, 10, 3);
            $no_urut=substr($nop, 13, 4);
            $kd_jns_op=substr($nop, 17, 1);

            if(ENVIRONMENT=='development'){
                $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
            }else{
                $tbl = 'SPO.PEMBAYARAN_SPPT';
            }
            $data['res']=$this->db->query("SELECT A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NM_WP_SPPT FROM $tbl A
            LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN
            LEFT JOIN REF_KELURAHAN C ON A.KD_KECAMATAN=C.KD_KECAMATAN AND A.KD_KELURAHAN=C.KD_KELURAHAN
            LEFT JOIN SPPT_OLTP D ON A.KD_PROPINSI=D.KD_PROPINSI AND A.KD_DATI2=D.KD_DATI2 AND A.KD_KECAMATAN=D.KD_KECAMATAN
            AND A.KD_KELURAHAN=D.KD_KELURAHAN AND A.KD_BLOK=D.KD_BLOK AND A.NO_URUT=D.NO_URUT 
            AND A.KD_JNS_OP=D.KD_JNS_OP AND A.THN_PAJAK_SPPT=D.THN_PAJAK_SPPT
            WHERE A.KD_PROPINSI='$kd_propinsi' AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' 
            AND A.KD_KELURAHAN='$kd_kelurahan' AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'
            ORDER BY A.THN_PAJAK_SPPT DESC")->result();
            
        }
        // dd($data);
        return view('buktilunas/index', $data);
    }

    
    function bukti_pembayaran($nop, $tahun_pajak){
        $exp = explode(".",$nop);
        $kd_propinsi = $exp[0];
        $kd_dati2 = $exp[1];
        $kd_kecamatan = $exp[2];
        $kd_kelurahan = $exp[3];
        $kd_blok = $exp[4];
        $no_urut = $exp[5];
        $kd_jns_op = $exp[6];

        $nop2 = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'-'.$no_urut.'.'.$kd_jns_op;

            // $kd_propinsi = substr($nop, 0, 2);
            // $kd_dati2 = substr($nop, 2, 2);
            // $kd_kecamatan = substr($nop, 4, 3);
            // $kd_kelurahan = substr($nop, 7, 3);
            // $kd_blok = substr($nop, 10, 3);
            // $no_urut = substr($nop, 13, 4);
            // $kd_jns_op = substr($nop, 17, 1);
        if(ENVIRONMENT=='development'){
            $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
        }else{
            $tbl = 'SPO.PEMBAYARAN_SPPT';
        }
        $data['cetak'] = true;
        $res = $this->db->query("SELECT * FROM $tbl WHERE KD_PROPINSI='$kd_propinsi' 
        AND KD_DATI2='$kd_dati2' AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' 
        AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut' AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$tahun_pajak'")->row();

        $cari = $this->db->query("SELECT A.JLN_WP_SPPT, B.NM_KECAMATAN, C.NM_KELURAHAN, A.NM_WP_SPPT
        FROM sppt_oltp A LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN LEFT JOIN REF_KELURAHAN C ON A.KD_KELURAHAN=C.KD_KELURAHAN
        AND A.KD_KECAMATAN=C.KD_KECAMATAN WHERE A.KD_PROPINSI='$kd_propinsi' 
        AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' AND A.KD_KELURAHAN='$kd_kelurahan' 
        AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'")->row();

        $thn_pajak = $tahun_pajak;

        if(ENVIRONMENT=='development'){
            $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
        }else{
            $tbl = 'SPO.PEMBAYARAN_SPPT';
        }

        $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
        END BANK FROM $tbl WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
        AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
        AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();

        $kode_bank = '';
        if($s_ntpd->KODE_BANK_BAYAR==''){
            $kode_bank = $s_ntpd->BANK;
        }else{
            $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
        }
        $this->load->library('ciqrcode');
        $nqr = 'qr_ntpd/' . $s_ntpd->PENGESAHAN .'_'.'_'. str_replace(".", "", $nop) . '.png';

        @unlink($nqr);

        $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt-ntpd?ntpd=' . encrypt_url($s_ntpd->PENGESAHAN) . '&nop=' . encrypt_url($nop);
        $params['level'] = 'L';
        $params['size'] = 2;
        $params['savename'] = FCPATH . $nqr;
        $this->ciqrcode->generate($params);

        $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();
        
        $data['ntpd'] = $s_ntpd->PENGESAHAN;
        $data['nop'] = $nop2;
        $data['nm_wp'] = $cari->NM_WP_SPPT;
        $data['alamat'] = $cari->JLN_WP_SPPT;
        $data['nm_kec'] = $cari->NM_KECAMATAN;
        $data['nm_kel'] = $cari->NM_KELURAHAN;
        $data['thn_pajak'] = $res->THN_PAJAK_SPPT;
        $data['tgl_bayar'] = date_indo(date('Y-m-d'));
        // $data['pokok'] = number_format($res->POKOK,0,',','.');
        // $data['denda'] = number_format($res->DENDA,0,',','.');
        $data['total'] = number_format($res->JML_SPPT_YG_DIBAYAR,0,',','.');
        $data['terbilang'] = penyebut_cap($res->JML_SPPT_YG_DIBAYAR).' Rupiah';
        $data['bank'] = $bank->NAMA_BANK;
        $data['qr'] = $nqr;

        // dd($data);

        $this->load->view('buktilunas/buktipembayaran', $data);
    }


    function cetak_data_pelunasan($nop){
        $data['cetak'] = true;
        $kd_propinsi= substr($nop, 0, 2);
        $kd_dati2=  substr($nop, 2, 2);
        $kd_kecamatan=substr($nop, 4, 3);
        $kd_kelurahan=substr($nop, 7, 3);
        $kd_blok=substr($nop, 10, 3);
        $no_urut=substr($nop, 13, 4);
        $kd_jns_op=substr($nop, 17, 1);

        if(ENVIRONMENT=='development'){
            $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
        }else{
            $tbl = 'SPO.PEMBAYARAN_SPPT';
        }

        $data['res']=$this->db->query("SELECT A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NM_WP_SPPT FROM $tbl A
            LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN
            LEFT JOIN REF_KELURAHAN C ON A.KD_KECAMATAN=C.KD_KECAMATAN AND A.KD_KELURAHAN=C.KD_KELURAHAN
            LEFT JOIN SPPT_OLTP D ON A.KD_PROPINSI=D.KD_PROPINSI AND A.KD_DATI2=D.KD_DATI2 AND A.KD_KECAMATAN=D.KD_KECAMATAN
            AND A.KD_KELURAHAN=D.KD_KELURAHAN AND A.KD_BLOK=D.KD_BLOK AND A.NO_URUT=D.NO_URUT 
            AND A.KD_JNS_OP=D.KD_JNS_OP AND A.THN_PAJAK_SPPT=D.THN_PAJAK_SPPT
            WHERE A.KD_PROPINSI='$kd_propinsi' AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' 
            AND A.KD_KELURAHAN='$kd_kelurahan' AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'
            ORDER BY A.THN_PAJAK_SPPT DESC")->result();

        $this->load->view('buktilunas/cetakdatapelunasan', $data);
    }

    function realisasi_pembayaran(){
        
        $data['tahun'] = date('Y');
        $data['tahun_start'] = 2003;
        $data['tahun_end'] = date('Y');
        $data['tgl_awal'] = date('d-m-Y');
        $data['tgl_akhir'] = date('d-m-Y');

        $kd_pengguna = $_SESSION['pbb_ku'];
        $upt = $this->db->query("SELECT * FROM P_UPT_USER WHERE KODE_PENGGUNA='$kd_pengguna'")->row();
        $kode_upt = $upt->KODE_UPT;
        $data['ref_kec'] = $this->db->query("SELECT A.*, B.* FROM P_UPT_KECAMATAN A LEFT JOIN REF_KECAMATAN B 
                            ON A.KODE_KECAMATAN=B.KD_KECAMATAN WHERE A.KODE_UPT=$kode_upt ORDER BY B.NM_KECAMATAN ASC")->result();

        if(isset($_GET['btn_cari'])){

            $data['tahun'] = $_GET['thn_pajak'];
            $data['tgl_awal'] = $_GET['tgl_awal'];
            $data['tgl_akhir'] = $_GET['tgl_akhir'];
            $kec = $_GET['kecamatan'];
            $kel = $_GET['kelurahan'];
            $data['ref_kel'] = $this->db->query("SELECT A.* FROM REF_KELURAHAN A LEFT JOIN REF_KECAMATAN B ON
            A.KD_KECAMATAN=B.KD_KECAMATAN WHERE A.KD_KECAMATAN='$kec' ORDER BY A.NM_KELURAHAN ASC")->result();
            $data['kec'] = $kec;
            $data['kel'] = $kel;
            $thn_pajak = $_GET['thn_pajak'];
            
            $tgl1= date('d/m/Y H:i:s', strtotime($_GET['tgl_awal'].' 00:00:00'));
            // $tgl2= date('d-m-Y', strtotime($_GET['tgl_akhir']. ' +1 day'));
            $tgl2= date('d/m/Y H:i:s', strtotime($_GET['tgl_akhir'].' 00:00:00'));
            $wh = "AND TRUNC(A.TGL_PEMBAYARAN_SPPT) between  to_date('$tgl1','dd-mm-yyyy hh24:mi:ss') and to_date('$tgl2','dd-mm-yyyy hh24:mi:ss')";

            $wh_thn = '';
            if($thn_pajak !='all'){
                $wh_thn = "AND A.THN_PAJAK_SPPT='$thn_pajak'";
            }
            
            if(ENVIRONMENT=='development'){
                $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
            }else{
                $tbl = 'SPO.PEMBAYARAN_SPPT';
            }

            $data['list_nop'] = $this->db->query("SELECT A.*, NM_WP_SPPT
            FROM $tbl A
            LEFT JOIN SPPT B ON A.THN_PAJAK_SPPT = B.THN_PAJAK_SPPT AND A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN
            AND A.KD_BLOK=B.KD_BLOK AND A.NO_URUT=B.NO_URUT AND A.KD_JNS_OP=B.KD_JNS_OP WHERE A.KD_KECAMATAN='$kec' AND A.KD_KELURAHAN='$kel'
            $wh_thn $wh ORDER BY NM_WP_SPPT ASC")->result();

            // dd($data);

        }

        
        // $data['ref_tahun'] = $this->Mdafnom->ref_tahun($kd_pengguna);
        return view('buktilunas/realisasipembayarantes', $data);
        // return view('buktilunas/realisasipembayaran', $data);
    }

    function ajax_realisasi($thn, $tgl_awal, $tgl_akhir)
    {
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        
        if($tgl_awal!="-"){
            $tgl1= date('d-m-Y', strtotime($tgl_awal));
            $tgl2= date('d-m-Y', strtotime($tgl_akhir. ' +1 day'));
            $wh = "TGL_BAYAR between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";
        }


        $this->load->library('datatables');
        $this->datatables->select("ID, KOBIL, NAMA_KECAMATAN, NAMA_KELURAHAN, NAMA_WP, TAHUN_PAJAK, CREATED_AT, TGL_BAYAR");
        $this->datatables->where('CREATED_BY', $kd_pengguna);
        $this->datatables->where('KD_STATUS', '1');
        
        if($tgl_awal!="-"){
        $this->datatables->where($wh);
        }
        
        if($thn != 'all'){
            $this->datatables->where('TAHUN_PAJAK', $thn);
        }
        $this->datatables->from('DATA_BILLING');
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }

    function cetak_realisasi_pdf($thn, $tgl_awal, $tgl_akhir, $kec, $kel){
        
        $tgl1= date('d-m-Y', strtotime($tgl_awal));
        $tgl2= date('d-m-Y', strtotime($tgl_akhir));
        $wh = "AND A.TGL_PEMBAYARAN_SPPT between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";

        $wh_thn = '';
        if($thn !='all'){
            $wh_thn = "AND A.THN_PAJAK_SPPT='$thn'";
        }

        if(ENVIRONMENT=='development'){
            $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
        }else{
            $tbl = 'SPO.PEMBAYARAN_SPPT';
        }
            
        $data['list_nop'] = $this->db->query("SELECT A.*, NM_WP_SPPT
            FROM $tbl A
            LEFT JOIN SPPT_OLTP B ON A.THN_PAJAK_SPPT = B.THN_PAJAK_SPPT AND A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN
            AND A.KD_BLOK=B.KD_BLOK AND A.NO_URUT=B.NO_URUT AND A.KD_JNS_OP=B.KD_JNS_OP WHERE A.KD_KECAMATAN='$kec' AND A.KD_KELURAHAN='$kel'
            $wh_thn $wh ORDER BY NM_WP_SPPT ASC")->result();

        // dd($data);

        $this->load->view('buktilunas/cetakrealisasipdf', $data);
    }

    function cetak_realisasi_excel($thn, $tgl_awal, $tgl_akhir, $kec, $kel){

        $this->load->library('exceldua');

        $ref_kec = $this->db->query("SELECT NM_KECAMATAN FROM REF_KECAMATAN WHERE KD_KECAMATAN='$kec'")->row();
        $ref_kel = $this->db->query("SELECT NM_KELURAHAN FROM REF_KELURAHAN WHERE KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel'")->row();
        $title = '';
        if ($kec <> '') {
            $title.=" kec ".$ref_kec->NM_KECAMATAN;
        }
        if ($kel <> '') {
            $title.=" kel ".$ref_kel->NM_KELURAHAN;
        }
        
        $tgl1= date('d-m-Y', strtotime($tgl_awal));
        $tgl2= date('d-m-Y', strtotime($tgl_akhir));
        $wh = "AND A.TGL_PEMBAYARAN_SPPT between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";

        $wh_thn = '';
        if($thn !='all'){
            $wh_thn = "AND A.THN_PAJAK_SPPT='$thn'";
        }

        $header = ['No','NOP', 'Nama WP', 'Tanggal Bayar', 'PBB'];
        
        if(ENVIRONMENT=='development'){
            $tbl = 'SPO_DEV.PEMBAYARAN_SPPT';
        }else{
            $tbl = 'SPO.PEMBAYARAN_SPPT';
        }

        $query = $this->db->query("SELECT A.*, NM_WP_SPPT
        FROM $tbl A
        LEFT JOIN SPPT_OLTP B ON A.THN_PAJAK_SPPT = B.THN_PAJAK_SPPT AND A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN
        AND A.KD_BLOK=B.KD_BLOK AND A.NO_URUT=B.NO_URUT AND A.KD_JNS_OP=B.KD_JNS_OP WHERE A.KD_KECAMATAN='$kec' AND A.KD_KELURAHAN='$kel'
        $wh_thn $wh ORDER BY NM_WP_SPPT ASC")->result();
        $arrisi = array();
        $PBB = 0;
        $no = 1;
        foreach ($query as $a) {
            $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
            $PBB += $a->JML_SPPT_YG_DIBAYAR;
            // $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
            $ff = array($no++, $nop, $a->NM_WP_SPPT, $a->TGL_PEMBAYARAN_SPPT, number_format($a->JML_SPPT_YG_DIBAYAR, 0, '', '.'));
            array_push($arrisi, $ff);
        }


        $ffa = array('', '', '', '', number_format($PBB, 0, '', '.'));
        array_push($arrisi, $ffa);
        
        $this->exceldua->write('export data', $header, $arrisi);
    }


}
