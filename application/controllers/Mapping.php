<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mapping extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
    }

    public function index()
    {
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN order by kd_kecamatan asc")->result();
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $BLOK         = $this->input->get('BLOK', true);

        if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '' && strlen($BLOK) == 3) {
            $max = $this->db->query("select lpad(max( to_number(no_urut)),4,0) max_no_urut  from dat_objek_pajak where kd_propinsi = '35'
               AND kd_dati2 = '07'
               AND kd_kecamatan = '$KD_KECAMATAN'
               AND kd_kelurahan ='$KD_KELURAHAN'
               AND kd_blok = '$BLOK'")->row()->MAX_NO_URUT;

            if (!empty($max)) {
                $ln = $this->db->query("select * from (               
                                 SELECT '35' prop,'07' dati2,'$KD_KECAMATAN' kecamatan,'$KD_KELURAHAN' kelurahan,'$BLOK' blok,lpad(1 + LEVEL-1,4,0) no_urut,cek_nop_kosong('35','07','$KD_KECAMATAN','$KD_KELURAHAN','$BLOK', lpad(1 + LEVEL-1,4,0)) cek
                                  FROM dual
                                CONNECT BY LEVEL <= $max
                                ) where cek !=1 order by to_number(no_urut) desc")->result();
            } else {
                $max = null;
                $ln = null;
            }
        } else {
            $max = null;
            $ln = null;
        }
       
        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        $data = array(
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'BLOK'         => $BLOK,
            'max'          => $max,
            'ln'           => $ln
        );
        // $this->template->load('template', );
        return view('mapping/v_list', $data);
    }
}
