<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 
 */
class Expired extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function index()
    {
        $row = $this->db->query("SELECT * FROM CONFIG_EXPIRED WHERE ID=1")->row();

        if ($row) {
            $data = array(
                'button'      => 'Update Status',
                'action'      => site_url('expired/update_action'),
                'id'   => set_value('id', $row->ID),
                'expired'   => set_value('expired', $row->EXPIRED),
            );
            return view('expired/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('expired'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'EXPIRED' => $this->input->post('expired', TRUE),
            );

            $this->db->where("ID", $this->input->post('id', TRUE));
            $query = $this->db->update("CONFIG_EXPIRED", $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');

            $base = base_url();
            if ($query) {
                echo "<script>alert('Berhasil Update Status !'); document.location= '" . "$base" . "expired'</script>";
            } else {
                echo "<script>alert('Gagal Update Status !'); document.location= '" . "$base" . "expired'</script>";
            }
        }
    }


    public function _rules()
    {
        $this->form_validation->set_rules('expired', 'keterangan', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }
   
}
