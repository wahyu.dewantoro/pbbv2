<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Rekon extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->model('Mrekon');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>000 Semua Kelurahan</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }

    function rekonpembayaran()
    {
        $bank = $this->db->query("SELECT KODE_BANK,NAMA_BANK 
        FROM REF_BANK
        WHERE STATUS=1")->result();
        return view('rekon/rekonpembayaran', array('bank' => $bank));
    }

    function laprekonpembayaranbni()
    {

        $bln = $this->input->post('bulan', true);
        $thn = $this->input->post('tahun', true);

        $bulan = "";
        $tahun = "";

        if (isset($bln)) {
            $bulan = $bln;
        } else {
            $bulan = date('m');
        }

        if (isset($thn)) {
            $tahun = $thn;
        } else {
            $tahun = date('Y');
        }

        if (isset($_GET['t'])) {
            $tanggal = $_GET['t'];
            $bulan = substr($tanggal, 3, 2);
            $tahun = substr($tanggal, 6, 4);
            $data['drk'] = $this->db->query("SELECT a.*, to_char(TANGGAL,'DD-MM-YYYY HH:MI:SS') as TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 a WHERE to_char(a.TANGGAL,'dd-mm-yyyy') = '$tanggal' ORDER BY TO_CHAR(TANGGAL, 'dd-mm-yyyy hh:mi:ss') ASC ")->result();
        }

        $data['bln'] = $bulan;
        $data['thn'] = $tahun;


        $data['rk'] = $this->db->query("SELECT COUNT(NOP) AS JML_NOP, TO_CHAR(TANGGAL, 'dd-mm-yyyy') AS TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 WHERE extract(YEAR from TANGGAL) = '$tahun' AND extract(MONTH from TANGGAL) = '$bulan' GROUP BY TO_CHAR(TANGGAL, 'dd-mm-yyyy') ORDER BY TO_CHAR(TANGGAL, 'dd-mm-yyyy') ASC")->result();

        // $this->template->load('template',);
        return view('rekon/laprekonpembayaran', $data);
    }

    function dlaprekonpembayaranbni()
    {
        if (isset($_POST['tgl1'])) {
            $tgl1       = $_POST['tgl1'];
        } else {
            $tgl1 = date('d-m-Y');
        }

        if (isset($_POST['cari'])) {
            $data['rk'] = $this->db->query("SELECT a.*, to_char(TANGGAL,'DD-MM-YYYY HH:MI:SS') as TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 a WHERE to_char(a.TANGGAL,'dd-mm-yyyy') = '$tgl1'")->result();
        }
        $data['tgl1'] = $tgl1;


        $this->template->load('template', 'rekon/laprekonpembayaran', $data);
    }


    public function datarekon()
    {

        $q = $this->input->get('cari');
        $start = intval($this->input->get('start'));
        return view('rekon.index', array(
            'q' => $q,
            'start' => $start
        ));
    }

    public function datarekon_data()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'rekon/datarekon?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'rekon/datarekon?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'rekon/datarekon';
            $config['first_url'] = base_url() . 'rekon/datarekon';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mrekon->total_rows($q);
        $rekon                        = $this->Mrekon->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'data'  => $rekon,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,

        );

        // $this->template->load('layout', 'menu/ms_menu_list', $data);
        $this->load->view('rekon/index_', $data);
    }



    function prosesimport()
    {
        $this->load->library('exceldua');
        $cell = [0, 1, 2, 3, 4, 5, 6];

        // dd($_FILES['data_excel']);
        $exp = explode('.', $_FILES['data_excel']['name']);
        $exe = $exp[count($exp) - 1];

        if ($exe == 'xlsx') {


            $reader = $this->exceldua->reader($_FILES['data_excel']['tmp_name'], 1, $cell);
            // unlink($file['full_path']);

            $data = [
                'status' => 1,
                'data' => $reader
            ];
        } else {
            $data = [
                'status' => 0,
                'data' => 'File harus .xlsx'
            ];
        }
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo json_encode($data);
    }



    function submitrekon()
    {

        $data = [];
        foreach ($this->input->post('kode_bank') as $i => $cell) {
            $data[] = array(
                'KODE_BANK' => $this->input->post('kode_bank')[$i],
                'NOP' => $this->input->post('NOP')[$i],
                'TAHUN_PAJAK' => $this->input->post('THN_PAJAK')[$i],
                'NAMA' => $this->input->post('NAMA')[$i],
                'POKOK' => $this->input->post('POKOK')[$i],
                'DENDA' => $this->input->post('DENDA')[$i],
                'JUMLAH' => $this->input->post('JUMLAH')[$i],
                'TANGGAL' => $this->input->post('TGL')[$i],
            );
        }
        // dd($data);

        $this->db->trans_start();
        $this->db->insert_batch('EXCEL_PEMBAYARAN', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $msg = "Berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $msg = "gagal di proses";
        }
        $this->session->set_flashdata('notif', $msg);
        redirect('rekon/rekonpembayaran');
    }

    function dhr()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }



        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $kode_cari = $this->input->post('cari', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if (count($_POST['jns_bumi']) == 0) {
        } else {
            for ($i = 0; $i < count($_POST['jns_bumi']); $i++) {
                $where .= " AND JNS_BUMI='" . $_POST['jns_bumi'][$i] . "'";
            }
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if (isset($kode_cari)) {
            $query = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' $where")->result();
            $data = array(
                'rk'           => $query,
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        } else {

            $data = array(
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        }


        $this->template->load('template', 'datahasilrekaman/dhr', $data);
    }

    function dhrLengkap()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        //$kode_cari=$this->input->post('cari',true);

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        $data = array(
            'tahun'        => $tahun,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN
        );


        $this->template->load('template', 'datahasilrekaman/dhrlengkap', $data);
    }

    function dhrexcel()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL'];

        $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        }

        echo 'JUMLAH DATA : ' . $no;
        exit();
        //setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('datahasilrekaman' . date('YmdHis') . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL)->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';

                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $rk->ALAMAT_OP . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL;

                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;

                // echo json_encode(array('result' => $arrisi ));
                //echo $cc."<br><hr>";
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }

    public function rekonharianbank()
    {
        $data = $this->input->get();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $kode_bank = $this->input->get('kode_bank');
        $data = $this->Mrekon->RekapBayarHarian($tahun, $bulan, $kode_bank);
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function rekonGlobal()
    {
        $tahun = $this->input->get('tahun');

        $kode_bank = $this->input->get('kode_bank');
        $data = $this->Mrekon->rekapBayarGlobal($tahun, $kode_bank);
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo json_encode($data);
    }


    //     function dhrLengkapexcel(){
    //         //ini_set('max_execution_time', 300);
    //         $KD_KECAMATAN=urldecode($this->input->get('KD_KECAMATAN',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('KD_KELURAHAN',true));
    //         $tahun =urldecode($this->input->get('tahun')); 
    // //exit();
    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND a.kd_kecamatan='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND a.kd_kelurahan='". $KD_KELURAHAN."'";
    //         }

    //         $kecamatan=$this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
    //                                     SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
    //                                         FROM MV_DHR a
    //                                     LEFT JOIN  REF_KECAMATAN b
    //                                     ON a.kd_kecamatan = b.kd_kecamatan
    //                                     JOIN REF_KELURAHAN c
    //                                     ON a.kd_kecamatan = c.kd_kecamatan
    //                                     AND a.kd_kelurahan = c.kd_kelurahan
    //                                     where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where                                    
    //                                 ) ORDER BY nm_kelurahan asc")->result();

    //                 $header=['NO','NOP','FORMULIR SPOP','JUMLAH BNG','ALAMAT OP','RT/RW','NAMA WP', 'LUAS BUMI','ZNT','NILAI BUMI','KTP','NPWP','STATUS WP','PEKERJAAN WP','PERSIL','FORMULIR LSPOP','NO. BANGUNAN','LUAS BANGUNAN','JUMLAH LANTAI','TAHUN DIBANGUN','TAHUN RENOVASI','KONDISI UMUM BANGUNAN','JENIS KONSTRUKSI','JENIS ATAP','JENIS DINDING','JENIS LANTAI','JENIS LANGIT - LANGIT'];

    // //                 $no=1;
    // //                 foreach ($kecamatan as $key) {
    // //                      echo $key->KODE_KEC.'/'.$key->KODE_KEL.' : '.$key->NM_KECAMATAN.'/'.$key->NM_KELURAHAN;
    // //                      echo '<br><hr>';
    // //                      $no++;
    // //                 }

    // //                 echo 'JUMLAH DATA : '.$no;
    // // exit();
    //           //setup Spout Excel Writer, set tipenya xlsx
    //         $writer = WriterFactory::create(Type::XLSX);
    //         // download to browser
    //         $writer->openToBrowser('datahasilrekamanlengkap'.date('YmdHis').'.xlsx');
    //         // set style untuk header
    //         $headerStyle = (new StyleBuilder())
    //                ->setFontBold()
    //                ->build();

    //          $first=1;
    // foreach($kecamatan as $res){

    //             // echo $res->KODE_KEC.'/'.$res->KODE_KEL;
    //             // echo '<br><hr>';
    //             // exit();

    //             $sql=$this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND ROWNUM <=10 AND KD_KECAMATAN=".$res->KODE_KEC." AND KD_KELURAHAN =".$res->KODE_KEL.)->result();

    //                 $arrisi=array();
    //                 $no=1;
    //                 foreach($sql as $rk){
    //                     $nop = $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'.'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP;
    //                     $rt_rw = $rk->RT_OP.'/'.$rk->RW_OP;
    //                     $ktp = "'".$rk->KD_PROPINSI.$rk->KD_DATI2.$rk->KD_KECAMATAN.$rk->KD_KELURAHAN.$rk->KD_BLOK.$rk->NO_URUT.$rk->KD_JNS_OP;
    //                     $sts_wp = $rk->KD_STATUS_WP.' - PEMILIK';

    //                     $cc  =$no .'+'. $nop .'+'. $rk->NO_FORMULIR_SPOP .'+'. '' .'+'. $rk->ALAMAT_OP.'+'. $rt_rw.'+'.$rk->NM_WP_SPPT.'+'. $rk->LUAS_BUMI.'+'. $rk->KD_ZNT.'+'. $rk->NILAI_SISTEM_BUMI.'+'.$ktp.'+'.''.'+'.$sts_wp.'+'.''.'+'.$rk->NO_PERSIL.'+'.$rk->NO_FORMULIR_LSPOP.'+'.$rk->NO_BNG.'+'.$rk->LUAS_BNG.'+'.$rk->JML_LANTAI_BNG.'+'.$rk->THN_DIBANGUN_BNG.'+'.$rk->THN_RENOVASI_BNG.'+'.$rk->KONDISI_BNG.'+'.$rk->JNS_KONSTRUKSI_BNG.'+'.$rk->JNS_ATAP_BNG.'+'.$rk->KD_DINDING.'+'.$rk->KD_LANTAI.'+'.$rk->KD_LANGIT_LANGIT;

    //                     $ff  =explode('+',$cc);
    //                     array_push($arrisi,$ff);
    //                     $no++;

    //                     //echo json_encode(array('result' => $arrisi ));
    //                     //echo $cc."<br><hr>";
    //                 }

    //         //     exit();
    //         if($first==1){
    //               // write ke Sheet pertama
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 $writer->getCurrentSheet()->setName($nama);
    //                 // header Sheet pertama
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }else{
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 // write ke Sheet kedua
    //                 $writer->addNewSheetAndMakeItCurrent()->setName($nama);
    //                 // header Sheet kedua
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }
    //         $first++;
    //         }
    //         $writer->close();

    //     }

    //     function dhrexcel2(){
    //         $KD_KECAMATAN=urldecode($this->input->get('kec',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('kel',true));
    //         $tahun =urldecode($this->input->get('tahun')); 

    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND KD_KECAMATAN='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND KD_KELURAHAN='". $KD_KELURAHAN."'";
    //         }

    //         $data['rk'] = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' $where")->result();

    //          //print_r($data);
    //         $this->load->view('datahasilrekaman/dhrexcel',$data);
    //     }

}
