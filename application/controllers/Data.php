<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require_once("./vendor/autoload.php");

use Box\Spout\Common\Entity\Style\Border;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class Data extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>000 Semua Kelurahan</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }


    function dhr()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }

        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $jns_bumi = $this->input->post('jns_bumi') ? $this->input->post('jns_bumi') : [];
        $kode_cari = $this->input->post('cari', true);

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        $jenis = array(
            1 => '1 - TANAH + BANGUNAN',
            '2 - KAVLING SIAP BANGUN',
            '3 - TANAH KOSONG',
            '4 - FASILITAS UMUM',
            '5 - LAIN-LAIN'
        );

        $data = array(
            'tahun'        => $tahun,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'jns_bumi' => $jns_bumi,
            'jenis' => $jenis
        );

        return view('datahasilrekaman/dhr', $data);
    }

    function dhrLengkap()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }


        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $kode_cari = $this->input->post('cari', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if (isset($_POST['jns_bumi'])) {
            for ($i = 0; $i < count($_POST['jns_bumi']); $i++) {
                $where .= " AND JNS_BUMI='" . $_POST['jns_bumi'][$i] . "'";
            }
        }



        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if (isset($kode_cari)) {
            $query = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' $where order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC")->result();
            $data = array(
                'rk'           => $query,
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        } else {

            $data = array(
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        }
        return view('datahasilrekaman/dhrlengkap', $data);
    }

    function dhrexcel()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where THN_PAJAK_SPPT = '$tahun' $where
                                ) ORDER BY kode_kel asc")->result();

        $kolomheader = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL'];


        // $writer = WriterFactory::create(Type::XLSX);

        $nama_file = $this->db->query("SELECT KD_KECAMATAN||' - '||NM_KECAMATAN NAMAFILE FROM REF_KECAMATAN
        WHERE KD_KECAMATAN=$KD_KECAMATAN")->row()->NAMAFILE;

        // dd($nama_file);


        $writer = WriterEntityFactory::createXLSXWriter();
        $writer->openToBrowser($nama_file . '.xlsx');
        $style = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(11)
            ->setFontColor(Color::WHITE)
            ->setShouldWrapText(true)
            ->setBackgroundColor(Color::GREEN)
            ->build();
        $header = WriterEntityFactory::createRowFromArray($kolomheader, $style);

        $first = 1;
        foreach ($kecamatan as $res) {
            $sql = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL . " order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC")->result();
            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';
                $alamat = $rk->ALAMAT_OP . ', ' . $rk->BLOK_KAV_NO_OP;
                $ff = array(
                    WriterEntityFactory::createCell($no),
                    WriterEntityFactory::createCell($nop), 
                    WriterEntityFactory::createCell($rk->NO_FORMULIR_SPOP),
                    WriterEntityFactory::createCell(NULL), 
                    WriterEntityFactory::createCell($alamat), 
                    WriterEntityFactory::createCell($rt_rw), 
                    WriterEntityFactory::createCell($rk->NM_WP_SPPT), 
                    WriterEntityFactory::createCell($rk->LUAS_BUMI), 
                    WriterEntityFactory::createCell($rk->KD_ZNT), 
                    WriterEntityFactory::createCell($rk->NILAI_SISTEM_BUMI), 
                    WriterEntityFactory::createCell($ktp), 
                    WriterEntityFactory::createCell(null), 
                    WriterEntityFactory::createCell($sts_wp), 
                    WriterEntityFactory::createCell(NULL), 
                    WriterEntityFactory::createCell($rk->NO_PERSIL)
                );
                // array_push($arrisi, $ff);
                $arrisi[]=WriterEntityFactory::createRow($ff);
                $no++;
            }

            $nkl = $res->KODE_KEL . ' ' . $res->NM_KELURAHAN;
            if ($first == 1) {
                $writer->getCurrentSheet()->setName($nkl);
                $writer->addRow($header)->addRows($arrisi);
/*                 $writer->getCurrentSheet()->setName($nkl);
                $writer->addRowWithStyle($header, $headerStyle);
                $writer->addRows($arrisi); */
            } else {
                $writer->addNewSheetAndMakeItCurrent()->setName($nkl);
                $writer->addRow($header)->addRows($arrisi);
              /*   $writer->addNewSheetAndMakeItCurrent()->setName($nkl);
                $writer->addRowWithStyle($header, $headerStyle);
                $writer->addRows($arrisi); */
            }
            $first++;
        }


        $writer->close();

        die();
        // lama
        /*    $writer->openToBrowser($nama_file . '.xlsx');
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {
            $sql = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL . " order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC")->result();
            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';
                $alamat = $rk->ALAMAT_OP . ', ' . $rk->BLOK_KAV_NO_OP;
                $ff = array(
                    $no, $nop, $rk->NO_FORMULIR_SPOP, '', $alamat, $rt_rw, $rk->NM_WP_SPPT, $rk->LUAS_BUMI, $rk->KD_ZNT, $rk->NILAI_SISTEM_BUMI, $ktp, '', $sts_wp, '', $rk->NO_PERSIL
                );
                array_push($arrisi, $ff);
                $no++;
            }

            $nkl = $res->KODE_KEL . ' ' . $res->NM_KELURAHAN;
            if ($first == 1) {
                $writer->getCurrentSheet()->setName($nkl);
                $writer->addRowWithStyle($header, $headerStyle);
                $writer->addRows($arrisi);
            } else {

                $writer->addNewSheetAndMakeItCurrent()->setName($nkl);
                $writer->addRowWithStyle($header, $headerStyle);
                $writer->addRows($arrisi);
            }
            $first++;
        }


        $writer->close(); */
    }

    function dhrexcelLengkap()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where THN_PAJAK_SPPT = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL', 'FORMULIR LSPOP', 'NO. BANGUNAN', 'LUAS BANGUNAN', 'JUMLAH LANTAI', 'TAHUN DIBANGUN', 'TAHUN RENOVASI', 'KONDISI UMUM BANGUNAN', 'JENIS KONSTRUKSI', 'JENIS ATAP', 'JENIS DINDING', 'JENIS LANTAI', 'JENIS LANGIT-LANGIT'];

        /* $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        } */



        $nama_file = $this->db->query("SELECT KD_KECAMATAN||' - '||NM_KECAMATAN NAMAFILE FROM REF_KECAMATAN
        WHERE KD_KECAMATAN=$KD_KECAMATAN")->row()->NAMAFILE;

        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser($nama_file . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL)->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';
                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $rk->ALAMAT_OP . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL . '+' . $rk->NO_FORMULIR_SPOP . '+' . $rk->NO_BNG . '+' . $rk->LUAS_BNG . '+' . $rk->JML_LANTAI_BNG . '+' . $rk->THN_DIBANGUN_BNG . '+' . $rk->THN_RENOVASI_BNG . '+' . $rk->KONDISI_BNG . '+' . $rk->JNS_KONSTRUKSI_BNG . '+' . $rk->JNS_ATAP_BNG . '+' . $rk->KD_DINDING . '+' . $rk->KD_LANTAI . '+' . $rk->KD_LANGIT_LANGIT;
                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN;

                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                $writer->addRowWithStyle($header, $headerStyle);
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }


    function jsondatadhr()
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);

        $this->load->library('datatables');

        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN');
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN');
        $tahun = $this->input->post('tahun');
        $jns_bumi = $this->input->post('jns_bumi');

        $where = "THN_PAJAK_SPPT = '$tahun'";


        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if ($jns_bumi <> '') {
            $where .= " AND JNS_BUMI IN(" . $jns_bumi . ")";
        }

        $this->datatables->select('KD_PROPINSI, KD_DATI2, KD_KECAMATAN, 
        KD_KELURAHAN, KD_BLOK, NO_URUT, 
        KD_JNS_OP, NO_FORMULIR_SPOP, ALAMAT_OP, 
        BLOK_KAV_NO_OP, RT_OP, RW_OP, 
        NM_WP_SPPT, LUAS_BUMI, KD_ZNT, 
        NILAI_SISTEM_BUMI, KD_STATUS_WP, NO_PERSIL, 
        NO_FORMULIR_LSPOP, NO_BNG, LUAS_BNG, 
        JML_LANTAI_BNG, THN_DIBANGUN_BNG, THN_RENOVASI_BNG, 
        KONDISI_BNG, JNS_KONSTRUKSI_BNG, JNS_ATAP_BNG, 
        KD_DINDING, KD_LANTAI, KD_LANGIT_LANGIT, 
        TGL_PENDATAAN_OP, JNS_BUMI, THN_PAJAK_SPPT');
        $this->datatables->from('MV_DHR');
        $this->datatables->where($where, '', false);
        return print_r($this->datatables->generate());
    }
}
