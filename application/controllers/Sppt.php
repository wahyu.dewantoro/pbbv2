<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sppt extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        $this->load->model('Msppt');
        $this->load->library('datatables');
        $this->load->library("pagination");
    }

    function riwayat()
    {

        $hasil = [];
        $var = $this->input->get('var') ?? 'NOP';
        $nop = $this->input->get('NOP') ?? null;
        if ($nop <> '') {
            $res = str_replace('.', '', $nop);
            if ($var == 'scan') {
                $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
                $res = $dd->NOP;
            }
            // cek pembatalan
            $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
            $data['KD_PROPINSI']  = substr($res, 0, 2);
            $data['KD_DATI2']     = substr($res, 2, 2);
            $data['KD_KECAMATAN'] = substr($res, 4, 3);
            $data['KD_KELURAHAN'] = substr($res, 7, 3);
            $data['KD_BLOK']      = substr($res, 10, 3);
            $data['NO_URUT']      = substr($res, 13, 4);
            $data['KD_JNS_OP']    = substr($res, 17, 1);
            $data['cetak'] = false;
            $ceknop = $this->Msppt->getRIwayatbyNop($data, $bb->BATAL);
            $hasil['res'] = $ceknop;
            $hasil['nop'] = $nop;
            $hasil['pembatalan'] = $bb->BATAL;
        }
        return view('sppt/formriwayat', $hasil);
    }

    function riwayatnonlunas()
    {
        return view('sppt/formriwayatbelumlunas');
    }


    function sistep()
    {
        // $this->template->load('template', 'sppt/formsistep');
        return view('sppt/formsistep');
    }

    function ceknop()
    {
        $res = str_replace('.', '', $this->input->post('nop'));
        $var = $this->input->post('var');


        if ($var == 'scan') {
            $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
            $res = $dd->NOP;
        }
        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['cetak'] = false;
        $ceknop = $this->Msppt->getRIwayatbyNop($data, $bb->BATAL);
        $hasil['res'] = $ceknop;
        $hasil['nop'] = $this->input->post('nop');
        $hasil['pembatalan'] = $bb->BATAL;


        if ($ceknop) {
            echo base_url() . 'sppt/cetaksppt/' . trim($hasil['nop']);
        } else {
            echo "0";
        }

        /* $res=$this->load->view('sppt/datasppt', $hasil,true);
        echo $res; */
    }

    function ceknopsistep()
    {
        $res = str_replace('.', '', $this->input->post('nop'));
        $var = $this->input->post('var');

        if ($var == 'scan') {
            $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
            $res = $dd->NOP;
        }


        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();


        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['cetak'] = false;
        $hasil['res'] = $this->Msppt->getRIwayatbyNopSistep($data, $bb->BATAL);

        /* echo "<pre>";
        print_r($hasil);
        echo "</pre>";
        die(); */

        $hasil['nop'] = $this->input->post('nop');
        $hasil['pembatalan'] = $bb->BATAL;
        $this->load->view('sppt/datasppt', $hasil);
    }

    function ceknopbelumlunas()
    {
        $res = str_replace('.', '', $this->input->post('nop'));
        $var = $this->input->post('var');

        if ($var == 'scan') {
            $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
            $res = $dd->NOP;
        }
        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();

        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['cetak'] = false;
        $hasil['res'] = $this->Msppt->getRIwayatbyNopBelumLunas($data, $bb->BATAL);
        $hasil['nop'] = $this->input->post('nop');
        $hasil['pembatalan'] = $bb->BATAL;
        $this->load->view('sppt/dataspptdua', $hasil);
    }


    function cetakspptdua($nop)
    {
        // echo $nop;

        $res                  = str_replace('.', '', $nop);

        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['NOP']          = $nop;
        $data['cetak'] = true;
        $bb                   = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
        $rk = $this->Msppt->getRIwayatbyNopBelumLunas($data, $bb->BATAL);
        if (count($rk) > 0) {

            $data['res'] = $rk;
            $data['pembatalan'] = $bb->BATAL;
            $this->load->view('sppt/cetakbayarsppt', $data);
        }
    }

    function cetaksppt($nop)
    {
        // echo $nop;

        $res                  = str_replace('.', '', $nop);
        $nopx= substr($res, 0, 2).'.'.substr($res, 2, 2).'.'.substr($res, 4, 3).'.'.substr($res, 7, 3).'.'.substr($res, 10, 3).'-'.substr($res, 13, 4).'.'.substr($res, 17, 1);
        // echo $nop;
        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['NOP']          = $nop;
        $data['NOPX']          = $nopx;
        $data['cetak'] = true;
        $bb                   = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
        $rk = $this->Msppt->getRIwayatbyNop($data, $bb->BATAL);
        if (count($rk) > 0) {

            $data['res'] = $rk;
            $data['pembatalan'] = $bb->BATAL;
            $this->load->view('sppt/cetakbayarsppt', $data);
        }
    }

    function pencarian()
    {
        $data['kec'] = $this->db->query("select * from REF_KECAMATAN order by kd_kecamatan asc")->result();
        // $this->template->load('template', );
        return view('sppt/form_pencarian', $data);
    }

    function getKelurahan()
    {
        $kode_kec = $this->input->post('kode_kec');
        $kel = $this->db->query("select * from ref_kelurahan where kd_kecamatan='$kode_kec' order by nm_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        foreach ($kel as $kk) {
            $option .= "<option value='" . $kk->KD_KELURAHAN . "'>" . $kk->KD_KELURAHAN . ' ' . $kk->NM_KELURAHAN . "</option>";
        }
        echo $option;
    }

    function listpencarian()
    {
        $data = $this->input->post();
        $this->load->view('sppt/listpencarian', $data);
    }

    function jsonspptpencarian()
    {
        $kode_kec = $this->input->post('KODE_KEC');
        $kode_kel = $this->input->post('KODE_KEL');
        $nama = $this->input->post('NAMA');
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo $this->Msppt->jsonpencarian($kode_kec, $kode_kel, $nama);
    }

    function cekdetail()
    {
        return view('sppt/detail_sppt');
    }

    function getDataNop()
    {
        $var = $this->input->get('nop');
        $sql = "SELECT a.*,get_alamat_op('ALAMAT',SUBSTR ('$var', 1, 18)) alamat_op, get_alamat_op('RT',SUBSTR ('$var', 1, 18)) RT_op, get_alamat_op('RW',SUBSTR ('$var', 1, 18)) RW_op
          FROM sppt a
         WHERE      thn_pajak_sppt = SUBSTR ('$var', 19, 4)
               AND kd_propinsi = SUBSTR ('$var', 1, 2)
               AND kd_dati2 = SUBSTR ('$var', 3, 2)
               AND kd_kecamatan = SUBSTR ('$var', 5, 3)
               AND kd_kelurahan = SUBSTR ('$var', 8, 3)
               AND kd_blok = SUBSTR ('$var', 11, 3)
               AND no_urut = SUBSTR ('$var', 14, 4)
               AND kd_jns_op = SUBSTR ('$var', 18, 1)";
        $res = $this->db->query($sql)->row();

        if ($res) {
            $KD_PROPINSI               = $res->KD_PROPINSI <> '' ? $res->KD_PROPINSI : '';
            $KD_DATI2                  = $res->KD_DATI2 <> '' ? $res->KD_DATI2 : '';
            $KD_KECAMATAN              = $res->KD_KECAMATAN <> '' ? $res->KD_KECAMATAN : '';
            $KD_KELURAHAN              = $res->KD_KELURAHAN <> '' ? $res->KD_KELURAHAN : '';
            $KD_BLOK                   = $res->KD_BLOK <> '' ? $res->KD_BLOK : '';
            $NO_URUT                   = $res->NO_URUT <> '' ? $res->NO_URUT : '';
            $KD_JNS_OP                 = $res->KD_JNS_OP <> '' ? $res->KD_JNS_OP : '';
            $THN_PAJAK_SPPT            = $res->THN_PAJAK_SPPT <> '' ? $res->THN_PAJAK_SPPT : '';
            $SIKLUS_SPPT               = $res->SIKLUS_SPPT <> '' ? $res->SIKLUS_SPPT : '';
            $KD_KANWIL                 = $res->KD_KANWIL <> '' ? $res->KD_KANWIL : '';
            $KD_KANTOR                 = $res->KD_KANTOR <> '' ? $res->KD_KANTOR : '';
            $KD_TP                     = $res->KD_TP <> '' ? $res->KD_TP : '';
            $NM_WP_SPPT                = $res->NM_WP_SPPT <> '' ? $res->NM_WP_SPPT : '';
            $JLN_WP_SPPT               = $res->JLN_WP_SPPT <> '' ? $res->JLN_WP_SPPT : '';
            $BLOK_KAV_NO_WP_SPPT       = $res->BLOK_KAV_NO_WP_SPPT <> '' ? $res->BLOK_KAV_NO_WP_SPPT : '';
            $RW_WP_SPPT                = $res->RW_WP_SPPT <> '' ? $res->RW_WP_SPPT : '';
            $RT_WP_SPPT                = $res->RT_WP_SPPT <> '' ? $res->RT_WP_SPPT : '';
            $KELURAHAN_WP_SPPT         = $res->KELURAHAN_WP_SPPT <> '' ? $res->KELURAHAN_WP_SPPT : '';
            $KOTA_WP_SPPT              = $res->KOTA_WP_SPPT <> '' ? $res->KOTA_WP_SPPT : '';
            $KD_POS_WP_SPPT            = $res->KD_POS_WP_SPPT <> '' ? $res->KD_POS_WP_SPPT : '';
            $NPWP_SPPT                 = $res->NPWP_SPPT <> '' ? $res->NPWP_SPPT : '';
            $NO_PERSIL_SPPT            = $res->NO_PERSIL_SPPT <> '' ? $res->NO_PERSIL_SPPT : '';
            $KD_KLS_TANAH              = $res->KD_KLS_TANAH <> '' ? $res->KD_KLS_TANAH : '';
            $THN_AWAL_KLS_TANAH        = $res->THN_AWAL_KLS_TANAH <> '' ? $res->THN_AWAL_KLS_TANAH : '';
            $KD_KLS_BNG                = $res->KD_KLS_BNG <> '' ? $res->KD_KLS_BNG : '';
            $THN_AWAL_KLS_BNG          = $res->THN_AWAL_KLS_BNG <> '' ? $res->THN_AWAL_KLS_BNG : '';
            $TGL_JATUH_TEMPO_SPPT      = $res->TGL_JATUH_TEMPO_SPPT <> '' ? $res->TGL_JATUH_TEMPO_SPPT : '';
            $LUAS_BUMI_SPPT            = $res->LUAS_BUMI_SPPT <> '' ? number_format($res->LUAS_BUMI_SPPT, 0, '', '.') : '';
            $LUAS_BNG_SPPT             = $res->LUAS_BNG_SPPT <> '' ? number_format($res->LUAS_BNG_SPPT, 0, '', '.') : '';
            $NJOP_BUMI_SPPT            = $res->NJOP_BUMI_SPPT <> '' ? number_format($res->NJOP_BUMI_SPPT, 0, '', '.') : '';
            $NJOP_BNG_SPPT             = $res->NJOP_BNG_SPPT <> '' ? number_format($res->NJOP_BNG_SPPT, 0, '', '.') : '';
            $NJOP_SPPT                 = $res->NJOP_SPPT <> '' ? number_format($res->NJOP_SPPT, 0, '', '.') : '';
            $NJOPTKP_SPPT              = $res->NJOPTKP_SPPT <> '' ? number_format($res->NJOPTKP_SPPT, 0, '', '.') : '';
            $PBB_TERHUTANG_SPPT        = $res->PBB_TERHUTANG_SPPT <> '' ? number_format($res->PBB_TERHUTANG_SPPT, 0, '', '.') : '';
            $FAKTOR_PENGURANG_SPPT     = $res->FAKTOR_PENGURANG_SPPT <> '' ? $res->FAKTOR_PENGURANG_SPPT : '';
            $PBB_YG_HARUS_DIBAYAR_SPPT = $res->PBB_YG_HARUS_DIBAYAR_SPPT <> '' ? number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT, 0, '', '.') : '';
            $STATUS_PEMBAYARAN_SPPT    = $res->STATUS_PEMBAYARAN_SPPT <> '' ? $res->STATUS_PEMBAYARAN_SPPT : '';
            $STATUS_TAGIHAN_SPPT       = $res->STATUS_TAGIHAN_SPPT <> '' ? $res->STATUS_TAGIHAN_SPPT : '';
            $STATUS_CETAK_SPPT         = $res->STATUS_CETAK_SPPT <> '' ? $res->STATUS_CETAK_SPPT : '';
            $TGL_TERBIT_SPPT           = $res->TGL_TERBIT_SPPT <> '' ? $res->TGL_TERBIT_SPPT : '';
            $TGL_CETAK_SPPT            = $res->TGL_CETAK_SPPT <> '' ? $res->TGL_CETAK_SPPT : '';
            $NIP_PENCETAK_SPPT         = $res->NIP_PENCETAK_SPPT <> '' ? $res->NIP_PENCETAK_SPPT : '';
            $ALAMAT_OP = $res->ALAMAT_OP <> '' ? $res->ALAMAT_OP : '';
            $RT_OP = $res->RT_OP <> '' ? $res->RT_OP : '';
            $RW_OP = $res->RW_OP <> '' ? $res->RW_OP : '';
        } else {
            $KD_PROPINSI               = '';
            $KD_DATI2                  = '';
            $KD_KECAMATAN              = '';
            $KD_KELURAHAN              = '';
            $KD_BLOK                   = '';
            $NO_URUT                   = '';
            $KD_JNS_OP                 = '';
            $THN_PAJAK_SPPT            = '';
            $SIKLUS_SPPT               = '';
            $KD_KANWIL                 = '';
            $KD_KANTOR                 = '';
            $KD_TP                     = '';
            $NM_WP_SPPT                = '';
            $JLN_WP_SPPT               = '';
            $BLOK_KAV_NO_WP_SPPT       = '';
            $RW_WP_SPPT                = '';
            $RT_WP_SPPT                = '';
            $KELURAHAN_WP_SPPT         = '';
            $KOTA_WP_SPPT              = '';
            $KD_POS_WP_SPPT            = '';
            $NPWP_SPPT                 = '';
            $NO_PERSIL_SPPT            = '';
            $KD_KLS_TANAH              = '';
            $THN_AWAL_KLS_TANAH        = '';
            $KD_KLS_BNG                = '';
            $THN_AWAL_KLS_BNG          = '';
            $TGL_JATUH_TEMPO_SPPT      = '';
            $LUAS_BUMI_SPPT            = '';
            $LUAS_BNG_SPPT             = '';
            $NJOP_BUMI_SPPT            = '';
            $NJOP_BNG_SPPT             = '';
            $NJOP_SPPT                 = '';
            $NJOPTKP_SPPT              = '';
            $PBB_TERHUTANG_SPPT        = '';
            $FAKTOR_PENGURANG_SPPT     = '';
            $PBB_YG_HARUS_DIBAYAR_SPPT = '';
            $STATUS_PEMBAYARAN_SPPT    = '';
            $STATUS_TAGIHAN_SPPT       = '';
            $STATUS_CETAK_SPPT         = '';
            $TGL_TERBIT_SPPT           = '';
            $TGL_CETAK_SPPT            = '';
            $NIP_PENCETAK_SPPT         = '';
            $ALAMAT_OP = '';
            $RT_OP     = '';
            $RW_OP     = '';
        }

        $row['KD_PROPINSI']               = $KD_PROPINSI;
        $row['KD_DATI2']                  = $KD_DATI2;
        $row['KD_KECAMATAN']              = $KD_KECAMATAN;
        $row['KD_KELURAHAN']              = $KD_KELURAHAN;
        $row['KD_BLOK']                   = $KD_BLOK;
        $row['NO_URUT']                   = $NO_URUT;
        $row['KD_JNS_OP']                 = $KD_JNS_OP;
        $row['THN_PAJAK_SPPT']            = $THN_PAJAK_SPPT;
        $row['SIKLUS_SPPT']               = $SIKLUS_SPPT;
        $row['KD_KANWIL']                 = $KD_KANWIL;
        $row['KD_KANTOR']                 = $KD_KANTOR;
        $row['KD_TP']                     = $KD_TP;
        $row['NM_WP_SPPT']                = $NM_WP_SPPT;
        $row['JLN_WP_SPPT']               = $JLN_WP_SPPT;
        $row['BLOK_KAV_NO_WP_SPPT']       = $BLOK_KAV_NO_WP_SPPT;
        $row['RW_WP_SPPT']                = $RW_WP_SPPT;
        $row['RT_WP_SPPT']                = $RT_WP_SPPT;
        $row['KELURAHAN_WP_SPPT']         = $KELURAHAN_WP_SPPT;
        $row['KOTA_WP_SPPT']              = $KOTA_WP_SPPT;
        $row['KD_POS_WP_SPPT']            = $KD_POS_WP_SPPT;
        $row['NPWP_SPPT']                 = $NPWP_SPPT;
        $row['NO_PERSIL_SPPT']            = $NO_PERSIL_SPPT;
        $row['KD_KLS_TANAH']              = $KD_KLS_TANAH;
        $row['THN_AWAL_KLS_TANAH']        = $THN_AWAL_KLS_TANAH;
        $row['KD_KLS_BNG']                = $KD_KLS_BNG;
        $row['THN_AWAL_KLS_BNG']          = $THN_AWAL_KLS_BNG;
        $row['TGL_JATUH_TEMPO_SPPT']      = $TGL_JATUH_TEMPO_SPPT;
        $row['LUAS_BUMI_SPPT']            = $LUAS_BUMI_SPPT;
        $row['LUAS_BNG_SPPT']             = $LUAS_BNG_SPPT;
        $row['NJOP_BUMI_SPPT']            = $NJOP_BUMI_SPPT;
        $row['NJOP_BNG_SPPT']             = $NJOP_BNG_SPPT;
        $row['NJOP_SPPT']                 = $NJOP_SPPT;
        $row['NJOPTKP_SPPT']              = $NJOPTKP_SPPT;
        $row['PBB_TERHUTANG_SPPT']        = $PBB_TERHUTANG_SPPT;
        $row['FAKTOR_PENGURANG_SPPT']     = $FAKTOR_PENGURANG_SPPT;
        $row['PBB_YG_HARUS_DIBAYAR_SPPT'] = $PBB_YG_HARUS_DIBAYAR_SPPT;
        $row['STATUS_PEMBAYARAN_SPPT']    = $STATUS_PEMBAYARAN_SPPT;
        $row['STATUS_TAGIHAN_SPPT']       = $STATUS_TAGIHAN_SPPT;
        $row['STATUS_CETAK_SPPT']         = $STATUS_CETAK_SPPT;
        $row['TGL_TERBIT_SPPT']           = $TGL_TERBIT_SPPT;
        $row['TGL_CETAK_SPPT']            = $TGL_CETAK_SPPT;
        $row['NIP_PENCETAK_SPPT']         = $NIP_PENCETAK_SPPT;
        $row['ALAMAT_OP'] = $ALAMAT_OP;
        $row['RT_OP'] = $RT_OP;
        $row['RW_OP'] = $RW_OP;
        $row_set[]                 = $row;
        echo $return = json_encode($row_set);
    }


    function esppt()
    {
        $var = $this->input->get('nop');
        $tahun = $this->input->get('tahun');
        $sql = "SELECT a.*,GET_KELURAHAN(KD_KELURAHAN,KD_KECAMATAN) KELURAHAN_OP,GET_KECAMATAN(KD_KECAMATAN) KECAMATAN_OP,get_alamat_op('ALAMAT',SUBSTR ('$var', 1, 18)) alamat_op, get_alamat_op('RT',SUBSTR ('$var', 1, 18)) RT_op, get_alamat_op('RW',SUBSTR ('$var', 1, 18)) RW_op,TO_CHAR(TGL_JATUH_TEMPO_SPPT,'yyyy-mm-dd') jatuh_tempo,TO_CHAR(TGL_CETAK_SPPT,'yyyy-mm-dd') TGL_CETAK
          FROM sppt a
         WHERE      thn_pajak_sppt =? 
               AND kd_propinsi = SUBSTR(?, 1, 2)
               AND kd_dati2 = SUBSTR(?, 3, 2)
               AND kd_kecamatan = SUBSTR(?, 5, 3)
               AND kd_kelurahan = SUBSTR(?, 8, 3)
               AND kd_blok = SUBSTR(?, 11, 3)
               AND no_urut = SUBSTR(?, 14, 4)
               AND kd_jns_op = SUBSTR(?, 18, 1)";
        $res = $this->db->query($sql, array($tahun, $var, $var, $var, $var, $var, $var, $var))->row();
        $ttd = $this->db->query("SELECT PEGAWAI.*
                                    FROM PEGAWAI@to17  pegawai
                                JOIN  POSISI_PEGAWAI@to17 posisi_pegawai ON PEGAWAI.NIP=POSISI_PEGAWAI.NIP
                                    WHERE KD_JABATAN=10 ")->row();
        $ta = $tahun - 5;

        $resop = str_replace('.', '', $var);
        
        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$resop') batal from dual")->row();
        $datanop['KD_PROPINSI']  = substr($resop, 0, 2);
        $datanop['KD_DATI2']     = substr($resop, 2, 2);
        $datanop['KD_KECAMATAN'] = substr($resop, 4, 3);
        $datanop['KD_KELURAHAN'] = substr($resop, 7, 3);
        $datanop['KD_BLOK']      = substr($resop, 10, 3);
        $datanop['NO_URUT']      = substr($resop, 13, 4);
        $datanop['KD_JNS_OP']    = substr($resop, 17, 1);
        $datanop['cetak'] = false;
        $riwayat = $this->Msppt->getRIwayatbyNop($datanop, $bb->BATAL);



        /* $riwayat = $this->db->query("SELECT DISTINCT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,case when spt.thn_pajak_sppt >= 2003 and spt.thn_pajak_sppt <=2020  then 0 else  get_denda@to17(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,spt.pbb_yg_harus_dibayar_sppt,spt.tgl_jatuh_tempo_sppt,sysdate) end denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
        FROM (
            SELECT * FROM SPPT
            WHERE KD_PROPINSI = SUBSTR($var, 1, 2)
            AND KD_DATI2      = SUBSTR($var, 3, 2)
            AND KD_KECAMATAN  = SUBSTR($var, 5, 3)
            AND KD_KELURAHAN  = SUBSTR($var, 8, 3)
            AND KD_BLOK       = SUBSTR($var, 11, 3)
            AND NO_URUT       = SUBSTR($var, 14, 4)
            AND KD_JNS_OP     =SUBSTR($var, 18, 1)
            and SPPT.thn_pajak_sppt >= $ta and SPPT.thn_pajak_sppt <= $tahun
            ) SPT
            left join  PEMBAYARAN_SPPT PS
            on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
            AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
            AND SPT.KD_BLOK =  PS.KD_BLOK
            AND SPT.NO_URUT =  PS.NO_URUT
            AND SPT.KD_JNS_OP = PS.KD_JNS_OP
            AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT order by SPT.THN_PAJAK_SPPT asc")->result(); */

        $lb = $this->db->query("SELECT upper(nama_bank) nama_bank, upper(nama_bank) ||'.png' logo FROM REF_BANK WHERE STATUS=1")->result();

        foreach ($lb as $rlb) {
            $tb[] = $rlb->NAMA_BANK;
        }
        $tb = implode(' / ', $tb);

        if ($res) {

            $this->load->library('ciqrcode');
            $nqr = 'qr/' . $var . $tahun . '.png';

            @unlink($nqr);

            $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt/?nop=' . encrypt_url($var) . '&tahun=' . encrypt_url($tahun);
            $params['level'] = 'L';
            $params['size'] = 2;
            $params['savename'] = FCPATH . $nqr;
            $this->ciqrcode->generate($params);

            $html = $this->load->view('sppt/esppt', array('sppt' => $res, 'plt' => false, 'ttd' => $ttd, 'tb' => $tb, 'qr' => $nqr, 'lb' => $lb, 'riwayat' => $riwayat), true);
            // echo $html;
            $filename = time() . ".pdf";
            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P', 'orientation' => 'P']);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetWatermarkImage(
                'kabmalang.png',
                0.1,
                '',
                ''
            );

            $mpdf->showWatermarkImage = true;
            $mpdf->AddPageByArray([
                'margin-left' => 3,
                'margin-right' => 3,
                'margin-top' => 3,
                'margin-bottom' => 3,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->debug = true;
            $mpdf->Output($filename, 'I');
        } else {
            $this->session->set_flashdata('notif', 'Data tidak di temukan');
            redirect(base_url() . 'sppt/ceksppt');
        }
    }

    function espptdua()
    {
        $this->esppt();
        die();
        $var = decrypt_url($this->input->get('nop'));
        $tahun = decrypt_url($this->input->get('tahun'));
        $sql = "SELECT a.*,GET_KELURAHAN(KD_KELURAHAN,KD_KECAMATAN) KELURAHAN_OP,GET_KECAMATAN(KD_KECAMATAN) KECAMATAN_OP,get_alamat_op('ALAMAT',SUBSTR ('$var', 1, 18)) alamat_op, get_alamat_op('RT',SUBSTR ('$var', 1, 18)) RT_op, get_alamat_op('RW',SUBSTR ('$var', 1, 18)) RW_op,TO_CHAR(TGL_JATUH_TEMPO_SPPT,'yyyy-mm-dd') jatuh_tempo,TO_CHAR(TGL_CETAK_SPPT,'yyyy-mm-dd') TGL_CETAK
        FROM sppt a
       WHERE      thn_pajak_sppt =? 
             AND kd_propinsi = SUBSTR(?, 1, 2)
             AND kd_dati2 = SUBSTR(?, 3, 2)
             AND kd_kecamatan = SUBSTR(?, 5, 3)
             AND kd_kelurahan = SUBSTR(?, 8, 3)
             AND kd_blok = SUBSTR(?, 11, 3)
             AND no_urut = SUBSTR(?, 14, 4)
             AND kd_jns_op = SUBSTR(?, 18, 1)";
        $res = $this->db->query($sql, array($tahun, $var, $var, $var, $var, $var, $var, $var))->row();
        $ttd = $this->db->query("SELECT PEGAWAI.*
                                  FROM PEGAWAI@to17  pegawai
                              JOIN  POSISI_PEGAWAI@to17 posisi_pegawai ON PEGAWAI.NIP=POSISI_PEGAWAI.NIP
                                  WHERE KD_JABATAN=10 ")->row();
        $ta = $tahun - 5;
        $riwayat = $this->db->query("SELECT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,case when spt.thn_pajak_sppt >= 2003 and spt.thn_pajak_sppt <=2020  then 0 else  get_denda@to17(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,spt.pbb_yg_harus_dibayar_sppt,spt.tgl_jatuh_tempo_sppt,sysdate) end denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
      FROM (
          SELECT * FROM SPPT
          WHERE KD_PROPINSI = SUBSTR($var, 1, 2)
          AND KD_DATI2      = SUBSTR($var, 3, 2)
          AND KD_KECAMATAN  = SUBSTR($var, 5, 3)
          AND KD_KELURAHAN  = SUBSTR($var, 8, 3)
          AND KD_BLOK       = SUBSTR($var, 11, 3)
          AND NO_URUT       = SUBSTR($var, 14, 4)
          AND KD_JNS_OP     =SUBSTR($var, 18, 1)
          and thn_pajak_sppt >= $ta and thn_pajak_sppt <= $tahun
          ) SPT
          left join  PEMBAYARAN_SPPT PS
          on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
          AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
          AND SPT.KD_BLOK =  PS.KD_BLOK
          AND SPT.NO_URUT =  PS.NO_URUT
          AND SPT.KD_JNS_OP = PS.KD_JNS_OP
          AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT order by SPT.THN_PAJAK_SPPT asc")->result();

        $lb = $this->db->query("SELECT upper(nama_bank) nama_bank, upper(nama_bank) ||'.png' logo FROM REF_BANK WHERE STATUS=1")->result();

        foreach ($lb as $rlb) {
            $tb[] = $rlb->NAMA_BANK;
        }
        $tb = implode(' / ', $tb);

        if ($res) {

            $this->load->library('ciqrcode');
            $nqr = 'qr/' . $var . $tahun . '.png';

            @unlink($nqr);

            $params['data'] = 'http://36.89.91.154:8082/pbbv2/e-sppt/?nop=' . encrypt_url($var) . '&tahun=' . encrypt_url($tahun);
            $params['level'] = 'L';
            $params['size'] = 2;
            $params['savename'] = FCPATH . $nqr;
            $this->ciqrcode->generate($params);

            $html = $this->load->view('sppt/esppt', array('sppt' => $res, 'plt' => false, 'ttd' => $ttd, 'tb' => $tb, 'qr' => $nqr, 'lb' => $lb, 'riwayat' => $riwayat), true);
            // echo $html;
            $filename = time() . ".pdf";
            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P', 'orientation' => 'P']);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetWatermarkImage(
                'kabmalang.png',
                0.1,
                '',
                ''
            );

            $mpdf->showWatermarkImage = true;
            $mpdf->AddPageByArray([
                'margin-left' => 3,
                'margin-right' => 3,
                'margin-top' => 3,
                'margin-bottom' => 3,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->debug = true;
            $mpdf->Output($filename, 'I');
        } else {
            $this->session->set_flashdata('notif', 'Data tidak di temukan');
            redirect(base_url() . 'sppt/ceksppt');
        }
    }

    function tanpapotongan()
    {
        $data = array(
            'data' => $this->db->get('SPPT_TANPA_POTONGAN')->result()
        );
        return view('sppt/tanpapotongan', $data);
    }

    function prosestanpapotongan()
    {

        $this->db->trans_start(FALSE);
        $var = str_replace('.', '', $this->input->post('NOP'));
        //cek di sppt
        $sppt = $this->db->query("select count(1) res from SPPT@to17
        where KD_PROPINSI= SUBSTR(" . $var . ", 1, 2) and
       KD_DATI2= SUBSTR(" . $var . ", 3, 2) and
       KD_KECAMATAN= SUBSTR(" . $var . ", 5, 3) and
       KD_KELURAHAN= SUBSTR(" . $var . ", 8, 3) and
       KD_BLOK= SUBSTR(" . $var . ", 11, 3) and
       NO_URUT= SUBSTR(" . $var . ", 14, 4) and
        KD_JNS_OP= SUBSTR(" . $var . ", 18, 1) and
        THN_PAJAK_SPPT=" . $this->input->post('THN_PAJAK_SPPT'))->row();

        if ($sppt->RES > 0) {

            $pot = $this->db->query("select count(1) res from SPPT_TANPA_POTONGAN@to17
        where KD_PROPINSI= SUBSTR(" . $var . ", 1, 2) and
       KD_DATI2= SUBSTR(" . $var . ", 3, 2) and
       KD_KECAMATAN= SUBSTR(" . $var . ", 5, 3) and
       KD_KELURAHAN= SUBSTR(" . $var . ", 8, 3) and
       KD_BLOK= SUBSTR(" . $var . ", 11, 3) and
       NO_URUT= SUBSTR(" . $var . ", 14, 4) and
        KD_JNS_OP= SUBSTR(" . $var . ", 18, 1) and
        THN_PAJAK_SPPT=" . $this->input->post('THN_PAJAK_SPPT'))->row();
            if ($pot->RES == 0) {



                $res = $this->db->query("INSERT INTO SPPT_TANPA_POTONGAN@to17 (
                KD_PROPINSI, KD_DATI2, KD_KECAMATAN, 
                KD_KELURAHAN, KD_BLOK, NO_URUT, 
                KD_JNS_OP, THN_PAJAK_SPPT) 
             VALUES (
                 SUBSTR(" . $var . ", 1, 2),
              SUBSTR(" . $var . ", 3, 2),
              SUBSTR(" . $var . ", 5, 3),
              SUBSTR(" . $var . ", 8, 3),
              SUBSTR(" . $var . ", 11, 3),
              SUBSTR(" . $var . ", 14, 4),
              SUBSTR(" . $var . ", 18, 1),
              " . $this->input->post('THN_PAJAK_SPPT') . ")");
                $this->db->query("commit");

                $pesan = "Berhasil di tambahkan";
            } else {
                $pesan = "gagal di tambahkan";
            }
        } else {
            $pesan = "Nop tidak ada dalam SPPT";
        }

        $this->session->set_flashdata('notif', $pesan);
        // print_r($pesan);
        redirect('sppt/tanpapotongan');
    }

    function hapustanpapotongan()
    {
        // print_r($this->input->get());
        $var = str_replace('.', '', $this->input->get('nop'));
        $this->db->query("DELETE from SPPT_TANPA_POTONGAN@to17
        where KD_PROPINSI= SUBSTR(" . $var . ", 1, 2) and
       KD_DATI2= SUBSTR(" . $var . ", 3, 2) and
       KD_KECAMATAN= SUBSTR(" . $var . ", 5, 3) and
       KD_KELURAHAN= SUBSTR(" . $var . ", 8, 3) and
       KD_BLOK= SUBSTR(" . $var . ", 11, 3) and
       NO_URUT= SUBSTR(" . $var . ", 14, 4) and
        KD_JNS_OP= SUBSTR(" . $var . ", 18, 1) and
        THN_PAJAK_SPPT=" . $this->input->get('tahun'));
        $this->session->set_flashdata('notif', 'Data telah di hapus');
        // print_r($pesan);
        redirect('sppt/tanpapotongan');
    }

    public function unflag()
    {
        $hasil = [];
        $tahun = $this->input->get('TAHUN') ?? date('Y');
        $nop = $this->input->get('NOP') ?? null;
        $hasil['nop'] = $nop;
        $hasil['tahun'] = $tahun;
        if ($nop <> '') {
            $var = str_replace('.', '', $nop);
            $hasil['pb'] = $this->db->query("SELECT NM_WP_SPPT,A.* 
            FROM PEMBAYARAN_SPPT A
            JOIN SPPT B ON A.KD_PROPINSI=B.KD_PROPINSI AND
            A.KD_DATI2=B.KD_DATI2 AND
            A.KD_KECAMATAN=B.KD_KECAMATAN AND
            A.KD_KELURAHAN=B.KD_KELURAHAN AND
            A.KD_BLOK=B.KD_BLOK AND
            A.NO_URUT=B.NO_URUT AND
            A.KD_JNS_OP=B.KD_JNS_OP AND
            A.THN_PAJAK_SPPT=B.THN_PAJAK_SPPT
            where A.KD_PROPINSI= SUBSTR(" . $var . ", 1, 2) and
       A.KD_DATI2= SUBSTR(" . $var . ", 3, 2) and
       A.KD_KECAMATAN= SUBSTR(" . $var . ", 5, 3) and
       A.KD_KELURAHAN= SUBSTR(" . $var . ", 8, 3) and
       A.KD_BLOK= SUBSTR(" . $var . ", 11, 3) and
       A.NO_URUT= SUBSTR(" . $var . ", 14, 4) and
       A.KD_JNS_OP= SUBSTR(" . $var . ", 18, 1) and
             A.THN_PAJAK_SPPT=$tahun
            ")->result();
        }
        return view('sppt/formunflag', $hasil);
    }

    public function unflagprosess()
    {
        $nop = $this->input->get('nop');
        $tahun = $this->input->get('tahun');
        $this->db->trans_start();
        $merchant=$this->input->get('merchat');
        $nop = str_replace('.', '', $nop);
        $this->db->query("INSERT INTO REVERSAL_PEMBAYARAN_SPPT (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, 
        THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL_BANK, KD_KPPBB_BANK, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT, 
        KODE_BANK_BAYAR, KODE_BANK_REVERSAL, KD_BANK_TUNGGAL, KD_BANK_PERSEPSI, TANGGAL_REVERSAL) 
                                 SELECT A.*,'1234' kd_bank_bayar,'999' kd_bank_reversal, '00' kd_bank_tunggal, '00' kd_bank_persepsi, SYSDATE tanggal FROM PEMBAYARAN_SPPT A
                                         WHERE KD_PROPINSI =SUBSTR('$nop',1,2) 
                                         AND KD_DATI2=SUBSTR('$nop',3,2)
                                         AND KD_KECAMATAN=SUBSTR('$nop',5,3)
                                         AND KD_KELURAHAN=SUBSTR('$nop',8,3)
                                         AND KD_BLOK=SUBSTR('$nop',11,3) 
                                         AND NO_URUT=SUBSTR('$nop',14,4) 
                                         AND KD_JNS_OP=SUBSTR('$nop',18,1)
                                         AND THN_PAJAK_SPPT='$tahun'");

        //spo 
        $this->db->query("DELETE  FROM SPO.PEMBAYARAN_SPPT
                                             where kd_propinsi  =substr('$nop',1,2) 
                                             and kd_dati2       =substr('$nop',3,2)
                                             and kd_kecamatan   =substr('$nop',5,3)
                                             and kd_kelurahan   =substr('$nop',8,3)
                                             and kd_blok        =substr('$nop',11,3) 
                                             and no_urut        =substr('$nop',14,4) 
                                             and kd_jns_op      =substr('$nop',18,1)
                                             and thn_pajak_sppt =$tahun");

        // $this->db->query("DELETE  FROM SPO_DEV.PEMBAYARAN_SPPT
        //         where kd_propinsi  =substr('$nop',1,2) 
        //         and kd_dati2       =substr('$nop',3,2)
        //         and kd_kecamatan   =substr('$nop',5,3)
        //         and kd_kelurahan   =substr('$nop',8,3)
        //         and kd_blok        =substr('$nop',11,3) 
        //         and no_urut        =substr('$nop',14,4) 
        //         and kd_jns_op      =substr('$nop',18,1)
        //         and thn_pajak_sppt =$tahun");

        $this->db->query("UPDATE  sppt@to17 set status_pembayaran_sppt=0 where thn_pajak_sppt='$tahun'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1)");

        $this->db->query("DELETE  from PEMBAYARAN_SPPT@to17 
                                             where kd_propinsi  =substr('$nop',1,2) 
                                             and kd_dati2       =substr('$nop',3,2)
                                             and kd_kecamatan   =substr('$nop',5,3)
                                             and kd_kelurahan   =substr('$nop',8,3)
                                             and kd_blok        =substr('$nop',11,3) 
                                             and no_urut        =substr('$nop',14,4) 
                                             and kd_jns_op      =substr('$nop',18,1)
                                             and thn_pajak_sppt =$tahun");
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Reversal Telah Berhasil !";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }

        header('Content-Type: application/json');

        $data_json = [
            'status' => $kode,
            'data' => $msg
        ];

        echo json_encode($data_json);
        // redirect($_SERVER['HTTP_REFERER']);
    }

    public function piutang($tahun = null)
    {
        $tahun = $tahun <> '' ? $tahun : date('Y') - 1;
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'sppt/piutang/' . $tahun . '?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sppt/piutang/' . $tahun . '?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sppt/piutang/' . $tahun;
            $config['first_url'] = base_url() . 'sppt/piutang/' . $tahun;
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Msppt->piutang_count($tahun, $q);
        $c_url = $this->Msppt->piutang_data($tahun, $config["per_page"], $start, $q);
        // \$this->" . $m . "->get_limit_data(\$config['per_page'], \$start, \$q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'data' => $c_url,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'tahun' => $tahun
        );

        return view('sppt/piutang', $data);
    }

    function piutang_excel($tahun)
    {
        $this->load->library('exceldua');
        $title = "piutang";
        $header = [
            'NOP',
            'Nama Wp',
            'Alamat',
            'Jalan',
            'Kelurahan',
            'Kecamatan',
            'Tahun',
            'PBB',
            'Piutang'
        ];
        $result = $this->Msppt->piutang_excel($tahun);
        $this->exceldua->write($title, $header, $result);
    }
}
