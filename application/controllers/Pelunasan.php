<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

// require_once APPPATH.'third_party/Spout/Autoloader/autoload.php';

/* use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type; */

//lets Use the Spout Namespaces
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;


// use Box\Spout\Writer\WriterFactory;
/* use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type; */
// use Box\Spout\Writer\Style\StyleBuilder;


// require_once APPPATH.'third_party/Spout/Autoloader/autoload.php';


// use Box\Spout\Common\Type;


class Pelunasan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        $this->load->model('Msppt');
    }

    public function index()
    {
        return view('sppt/form_upload_bj');
    }

    public function prosesupload()
    {
        $file = $_FILES['file'];

        try {

            $config['upload_path']      = './temp_doc/'; //siapkan path untuk upload file
            $config['allowed_types']    = 'xlsx|xls'; //siapkan format file
            $config['file_name']        = 'doc' . time(); //rename file yang diupload

            $this->load->library('upload', $config);
            $this->upload->do_upload('file');
            //Lokasi file excel	      
            // $file_path = "C:\file_excel.xlsx";
            $file   = $this->upload->data();
            $file_path = 'temp_doc/' . $file['file_name'];
            $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
            $reader->open($file_path); //open the file	  	      

            // echo "<pre>";
            $i = 0;

            $berhasil = 0;
            $lunas = 0;
            $gagal = 0;
            $record = 0;
            $nopgagal = '';
            $noplunas = '';
            /**                  
             * Sheets Iterator. Kali aja multiple sheets                  
             **/
            foreach ($reader->getSheetIterator() as $sheet) {

                //Rows iterator	               

                foreach ($sheet->getRowIterator() as $row) {
                    if ($i > 0) {
                        $record += 1;
                        $pas = [
                            'nop' => $row[0],
                            'nama' => $row[1],
                            'tahun' => $row[2],
                            'pokok' => $row[3],
                            'denda' => $row[4],
                            'jumlah' => $row[5],
                            'tanggal' => $row[6] . str_pad($row[7], 6, 0, STR_PAD_LEFT)
                        ];
                        $has = $this->Msppt->pelunasanSppt($pas);

                        switch ($has) {
                            case 0;
                                $gagal += 1;
                                $nopgagal .= '- ' . $row[0] . '<br>';
                                break;
                            case 2;
                                $lunas += 1;
                                $noplunas .= '- ' . $row[0] . '<br>';
                                break;
                            default:
                                $berhasil += 1;
                        }
                    }
                    ++$i;
                }
            }

            // echo "<br> Total Rows : " . $i . " <br>";
            $reader->close();

            unlink('temp_doc/' . $file['file_name']);
            // echo "Peak memory:", (memory_get_peak_usage(true) / 1024 / 1024), " MB", "<br>";
            $pesan = "<div class='box'><div class='box-body'>";
            // $pesan.="<b>Peak memory</b> : ". (memory_get_peak_usage(true) / 1024 / 1024). " MB". "<br>";
            $pesan .= 'Total data  : ' . $record . '<br>';
            if ($berhasil > 0) {
                $pesan .= 'Berhasil di lunaskan  : ' . $berhasil . ' data <br>';
            }
            if ($lunas > 0) {
                $pesan .= 'Gagal di proses karena sudah lunas  : <br>';
                $pesan .= $noplunas;
            }
            if ($gagal > 0) {
                $pesan .= 'Gagal di lunaskan : <br>';
                $pesan .= $nopgagal;
            }

            $pesan .= '</div></div>';
            $type = 'success';
        } catch (Exception $e) {

            // echo $e->getMessage();
            $pesan = $e->getMessage();
            $type = 'error';
            // exit;
        }

        $array = array(
            'type' => $type,
            'title' => 'Notifikasi',
            'pesan' => $pesan
        );

        echo json_encode($array);


        /*  if (!empty($file)) {
            $config['upload_path']      = './temp_doc/'; //siapkan path untuk upload file
            $config['allowed_types']    = 'xlsx|xls'; //siapkan format file
            $config['file_name']        = 'doc' . time(); //rename file yang diupload

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $file   = $this->upload->data();
                $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
                $reader->open('temp_doc/' . $file['file_name']); //open file xlsx

                //looping pembacaat sheet dalam file        
                foreach ($reader->getSheetIterator() as $sheet) {
                    $numRow = 1;
                    $save   = array();

                    //looping pembacaan row dalam sheet
                    foreach ($sheet->getRowIterator() as $row) {
                        if ($numRow > 1) {

                            // NOP	NAMA	TH_PAJAK	POKOK	DENDA	JUMLAH	TGL	JAM
                            
                            $data=array(
                                'nop'=> $row[0],
                                'nama'=> $row[1],
                                'thn_pajak'=> $row[2],
                                'pokok'=> $row[3],
                                'denda'=> $row[4],
                                'jumlah'=> $row[5],
                                'tanggal'=> $row[6],
                            );

                           
                            //tambahkan array $data ke $save
                            array_push($save, $data);
                        }

                        $numRow++;
                    }
                 
                    $reader->close();

                    //hapus file yang sudah diupload
                    unlink('temp_doc/' . $file['file_name']);

                  
                }
                $ts="success";
                $ps="Berhasil di proses";
            } else {
                $ps = "Error :" . $this->upload->display_errors(); //tampilkan pesan error jika file gagal diupload
                $tp = "error";
            }


            $pesan=$save;
             
        } else {
            $pesan = array(
                'type' => 'warning',
                'title' => 'Error',
                'pesan' => 'Tidak ada file yang di upload'
            );
        }
        echo json_encode($pesan);
         */
    }
}
