<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sistem extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
$this->output->enable_profiler(false);
		$this->load->model('msistem');
		 
	}


	function privilege()
	{
		$data['data'] = $this->db->query("SELECT * FROM p_group ORDER BY nama_group ASC")->result_array();
        // $this->template->load('template', 'privilege', $data);
        return view('sistem/privilege',$data);

	}


	function settingPrivilege($kode)
	{
		$data['role']        = $this->msistem->getMenu($kode);
		$data['kode_groupq'] = $kode;
		$data['ng']          = $this->db->query("SELECT nama_group FROM p_group WHERE kode_group='$kode'")->row()->NAMA_GROUP;
        // $this->template->load('template', 'settingPrivilege', $data);
        return view('sistem/settingPrivilege', $data);

	}

	function do_role()
	{
		$kode_group = $_POST['kode_group'];
		$role       = $_POST['role'];
		$role = $this->msistem->do_role($kode_group, $role);
		if ($role) {
			$this->session->set_flashdata('message', '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Role berhasil disimpan</div>');
			// echo "<script>window.alert('Data tersimpan !');
			// window.location='".site_url()."sistem/privilege';</script>";
			redirect('sistem/privilege');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Role tidak tersimpan</div>');
			redirect('sistem/privilege');
		}
	}

	function changepassword(){
		
        return view('sistem/ubahpassword');
	}

	function simpanpassword(){
        $password_lama = $_POST['password_lama'];
        $password_baru = $_POST['password_baru'];
        $ulangi_password = $_POST['ulangi_password'];
        
        $kd_pengguna = $_SESSION['pbb_ku'];

        $ambilpass=$this->db->query("SELECT PASSWORD FROM P_PENGGUNA where KODE_PENGGUNA='$kd_pengguna'")->row();
        $password_db=$ambilpass->PASSWORD;
        if($password_db==$password_lama){
            if($password_baru==$ulangi_password){
                $update=$this->db->query("update P_PENGGUNA set PASSWORD='$password_baru' where KODE_PENGGUNA='$kd_pengguna'");
                if($update) { 
					$data_json = [
                        'status' => 1,
                        'data' => 'Ganti Password Berhasil',
                    ];
                }else{
                    $data_json = [
                        'status' => 0,
                        'data' => 'Gagal ganti password ! Password Yang anda masukan tidak sesuai',
                    ];
                    
                }
            }
            else{ 
				$data_json = [
					'status' => 0,
					'data' => 'Gagal ganti password ! Password Yang anda masukan tidak sesuai',
				];
        	}
        }
        else{
            $data_json = [
				'status' => 0,
				'data' => 'Gagal ganti password ! Password Yang anda masukan tidak sesuai',
			];
    	}

		if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data_json);
    } 
}
