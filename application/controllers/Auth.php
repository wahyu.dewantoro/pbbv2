<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
    }

    function index()
    {

        $this->load->view('Vlogin');
    }

    function proses()
    {
        $username = strtoupper(trim($this->input->post('username')));
        $password = strtoupper(trim($this->input->post('password')));
        // $unit_kantor = $this->input->post('unit_kantor');
        $unit_kantor = 'kota';

        $sql = "SELECT KODE_PENGGUNA,NAMA_PENGGUNA,A.KODE_GROUP,NAMA_GROUP,A.KD_SEKSI,NM_SEKSI,NIP
                FROM (select * from P_PENGGUNA where kode_group not in (8, 13, 14)) A
                JOIN P_GROUP B ON A.KODE_GROUP=B.KODE_GROUP
                LEFT JOIN REF_SEKSI C ON C.KD_SEKSI=A.KD_SEKSI
                WHERE UPPER(USERNAME)=? AND UPPER(PASSWORD)=?  ";
        $query    = $this->db->query($sql, array($username, $password))->row();
        if (isset($query)) {
            if (count((array)$query) > 0) {
                $data['pbb']         = '1';
                $data['pbb_nama']    = $query->NAMA_PENGGUNA;
                $data['pbb_ku']      = $query->KODE_PENGGUNA;
                $data['pbb_kg']      = $query->KODE_GROUP;
                $data['pbb_ng']      = $query->NAMA_GROUP;
                $data['kd_seksi']    = $query->KD_SEKSI;
                $data['nip']         = $query->NIP;
                $data['seksi']       = $query->NM_SEKSI;
                $data['unit_kantor'] = $unit_kantor;
                $data['jam_login']   = date('Y-m-d H:i:s');
                $data['username']    = $username;
                $this->session->set_userdata($data);

                $this->session->set_flashdata('notif', 'Hore, Berhasil login ');

                switch ($query->KODE_GROUP) {
                    case '3':
                        # code...
                        // redirect('permohonan/create');
                        redirect('welcome');
                        break;
                    case '41':
                        # code...
                        // redirect('disposisi');
                        redirect('replikasi/belumverlap');
                        break;

                    default:
                        # PDI I, PDI II, PEL II, Pedanil

                        // redirect('permohonan/listdokumen');
                        redirect('welcome');
                        break;
                }


                //	echo "oke";
            } else {
                $this->session->set_flashdata('notif', '<div class="badge">
														         <p> Username dan password anda salah !</p>
														        </div>');
                redirect('Auth');
            }
        } else {
            $this->session->set_flashdata('notif', '<div class="badge">
														         <p>password anda salah !</p>
														        </div>');
            redirect('Auth');
        }
    }

    function logout()
    {
        $this->db->cache_delete();
        $this->db->cache_delete_all();
        $this->session->sess_destroy();
        $this->session->set_flashdata('notif', '<div class="badge">
													         <p> Berhasil logout !</p>
													        </div>');
        redirect('auth');
    }

    public function aksescli(){
		$res=shell_exec('php index.php clictrl prosesPelayananDua 2021 0001 001');
        print_r($res);
	}

    public function cli_test(){
        $res=$this->db->query("select * from PST_PERMOHONAN
        where thn_pelayanan='2021' and bundel_pelayanan='0001'
        and no_urut_pelayanan='001'")->result();
        print_r($res);
    }

    /* function test()
	{
		echo "827ccb0eea8a706c4c34a16891f84e7b";
		echo "<hr>";
		echo md5('PEDANIL');
	} */
}
