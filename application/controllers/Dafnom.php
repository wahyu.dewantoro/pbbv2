<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;


class Dafnom extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);

        // $this->load->model('Mdafnom');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function index(){
        
        $data['tahun'] = date('Y');
        $data['tahun_start'] = 2003;
        $data['tahun_end'] = date('Y');

        if(isset($_GET['thn_pajak'])){
            $data['tahun'] = $_GET['thn_pajak'];
        }
        
        $KD_KECAMATAN = ($this->input->get('KD_KECAMATAN', true))?$this->input->get('KD_KECAMATAN', true):"all";
        $KD_KELURAHAN = ($this->input->get('KD_KELURAHAN', true))?$this->input->get('KD_KELURAHAN', true):"all";
        $where = '';
        $and="";
        $pbb_ku = $this->session->userdata('pbb_ku');
        if ($this->session->userdata('pbb_kg') == 8) {
            $where = " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$pbb_ku."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
        }
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN  $where order by kd_kecamatan asc")->result();
        $kel=false;
        if ($KD_KECAMATAN) {
            $kel=(object)array("KD_KELURAHAN"=>"","NM_KELURAHAN"=>"Semua Kelurahan");
            $kel=(object)array($kel);
            if($KD_KECAMATAN!="all"){
                $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                from ref_kelurahan
                where kd_kecamatan='$KD_KECAMATAN' ". $and."
                order by kd_kecamatan asc,kd_kelurahan asc")->result();
            }
        }
        $data['kec']=$kec;
        $data['kel']=$kel;
        $data['KD_KECAMATAN']=$KD_KECAMATAN;
        $data['KD_KELURAHAN']=$KD_KELURAHAN;
        $data['direct_data']=$this->input->get(null,true);

        return view('dafnom/index_dafnom', $data);
    }
    function daftar_nop(){
        
        $data['tahun'] = date('Y');
        $data['tahun_start'] = 2003;
        $data['tahun_end'] = date('Y');
        $data['tgl_bayar'] = date('d-m-Y');

        if(isset($_GET['thn_pajak'])){
            $data['tahun'] = $_GET['thn_pajak'];
        }

        if(isset($_GET['tgl_bayar'])){
            $data['tgl_bayar'] = $_GET['tgl_bayar'];
        }
        
        $KD_KECAMATAN = ($this->input->get('KD_KECAMATAN', true))?$this->input->get('KD_KECAMATAN', true):"all";
        $KD_KELURAHAN = ($this->input->get('KD_KELURAHAN', true))?$this->input->get('KD_KELURAHAN', true):"all";
        $where = '';
        $and="";
        $pbb_ku = $this->session->userdata('pbb_ku');

        if ($this->session->userdata('pbb_kg') == 8) {
            $where = " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$pbb_ku."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
        }
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN  $where order by kd_kecamatan asc")->result();
        $kel=false;
        if ($KD_KECAMATAN) {

            if($KD_KECAMATAN!="all"){
                $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                from ref_kelurahan
                where kd_kecamatan='$KD_KECAMATAN' ". $and."
                order by kd_kecamatan asc,kd_kelurahan asc")->result();
            }
        }
        $data['kec']=$kec;
        $data['kel']=$kel;
        $data['KD_KECAMATAN']=$KD_KECAMATAN;
        $data['KD_KELURAHAN']=$KD_KELURAHAN;
        $data['direct']=($this->input->get(null,true));
        $data['kobil']=($this->input->get('kobil', true))?$this->input->get('kobil', true):false;
        $data['kd_status']=$this->input->get('kd_status', true);
        $data['tgl_show']=($this->session->userdata("pbb_kg")=="22")?"TGL_BAYAR":"CREATED_AT";
        $data['tgl_show_text']=($this->session->userdata("pbb_kg")=="22")?"Tgl Bayar ":"Tgl Buat";
        $attr="class='btn btn-primary btn-flat pull-right' ";
        $data['export_link']="";
        if($this->session->userdata("pbb_kg")=="0"){
            $data['export_link']=anchor("dafnom/cetak_daftar_nop_excel?".$this->input->server('QUERY_STRING'),'<i class="fa fa-book"></i> Export Excel',$attr);
        }
        return view('dafnom/daftar_nop', $data);
    }

    function json_dafnom($kec,$kel,$thn)
    {
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("ID, KOBIL, NAMA_KECAMATAN, NAMA_KELURAHAN, KETERANGAN, TAHUN_PAJAK, CREATED_AT, STATUS, JML_NOP, PBB");
        if($this->session->userdata("pbb_kg")!="1"&&$this->session->userdata("pbb_kg")!="22"){
            $this->datatables->where('CREATED_BY', $kd_pengguna);
        }
        if($this->session->userdata("pbb_kg")=="22"){
            $this->datatables->where('STATUS','1');
        }
        if($thn != 'all'){
            $this->datatables->where('TAHUN_PAJAK', $thn);
        }
        if($kec != 'all'){
            $this->datatables->where('KD_KECAMATAN', $kec);
        }
        if($kel != 'all'){
            $this->datatables->where('KD_KELURAHAN', $kel);
        }
        $this->datatables->from('V_DAFNOM');
        $this->datatables->order_by('TAHUN_PAJAK',"DESC");
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }
    function json_daftar_nop($kec,$kel,$thn)
    {
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $tgl=($this->session->userdata("pbb_kg")=="22")?"TGL_BAYAR":"CREATED_AT";
        // $this->datatables->select("ID, KOBIL, NAMA_KECAMATAN, NAMA_KELURAHAN,NOP,NM_WP, TAHUN_PAJAK, ".$tgl.", KD_STATUS, TOTAL");
        $this->datatables->select("ID, KOBIL, NAMA_KECAMATAN, NAMA_KELURAHAN,NOP,NM_WP, TAHUN_PAJAK, CREATED_AT, KD_STATUS, TOTAL, TGL_BAYAR");
        if($this->session->userdata("pbb_kg")!="1"&&$this->session->userdata("pbb_kg")!="22"&&$this->session->userdata("pbb_kg")!="0"){
            $this->datatables->where('CREATED_BY', $kd_pengguna);
        }
        if($this->session->userdata("pbb_kg")=="22"){
            //$this->datatables->where('KD_STATUS','1');
        }
        if($thn != 'all'){
            $this->datatables->where('TAHUN_PAJAK', $thn);
        }
        if($kec != 'all'){
            $this->datatables->where('KD_KECAMATAN', $kec);
        }
        if($kel != 'all'){
            $this->datatables->where('KD_KELURAHAN', $kel);
        }
        if($this->input->get("kobil")){
            $this->datatables->like('KOBIL', $this->input->get("kobil"));
        }
        if($this->input->get("kd_status")!==""){
            $this->datatables->where('KD_STATUS', $this->input->get("kd_status"));
        }
        $tgl_bayar = $this->input->get("tgl_bayar");
        if($this->input->get("kd_status")=="1"){
            $this->datatables->where("TGL_BAYAR LIKE '$tgl_bayar%'");
        }
        $this->datatables->from('V_DAFNOM_NOP');
        $this->datatables->order_by('TAHUN_PAJAK,NOP,NM_WP',"DESC");
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }

    function ajax_list_dafnom(){
        $id = $_POST['kode'];
        $row = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();
        $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id' 
        ORDER BY kd_blok, no_urut, kd_jns_op ASC")->result();
        if(count($res)>0){
            $tr = "";
            $no = 1;
            $total = 0;
            $jml_nop = 0;
            foreach ($res as $a) {
                $kd_propinsi = $a->KD_PROPINSI;
                $kd_dati2 = $a->KD_DATI2;
                $kd_kecamatan = $a->KD_KECAMATAN;
                $kd_kelurahan = $a->KD_KELURAHAN;
                $kd_blok = $a->KD_BLOK;
                $no_urut = $a->NO_URUT;
                $kd_jns_op = $a->KD_JNS_OP;
                $thn_pajak = $a->THN_PAJAK_SPPT;

                if(ENVIRONMENT=='development'){
                    $s_ntpd = $this->db->query("SELECT A.*, TO_CHAR(TGL_REKAM_BYR_SPPT, 'YYYY-MM-DD HH24:MI:SS') AS TGL_BAYAR FROM SPO_DEV.PEMBAYARAN_SPPT A WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                    AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                    AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'");
                }else{
                    $s_ntpd = $this->db->query("SELECT A.*, TO_CHAR(TGL_REKAM_BYR_SPPT, 'YYYY-MM-DD HH24:MI:SS') AS TGL_BAYAR FROM SPO.PEMBAYARAN_SPPT A WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                    AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                    AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'");
                }
                
                $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                $nop2 = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                // $btn_cetak = "<a href='".base_url('dafnom/bukti_pembayaran/').$nop."' class='btn btn-xs btn-danger' title='Cetak PDF'><i class='fa fa-file-pdf-o'></i></a>";
                $btn_cetak = "";
                if(count($s_ntpd->result()) > 0){

                    $p_sppt = $s_ntpd->row();
                    $btn_cetak = "<a href='".base_url('dafnom/bukti_pembayaran/').$nop."' class='btn btn-xs btn-danger' title='Cetak PDF' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                    $tr .= "
                    <tr id=''>
                        <th style='text-align: center'>".$no++."</th>
                        <td>".$nop2."</td>
                        <td style='text-align: center'>".$a->THN_PAJAK_SPPT."</td>
                        <td style='text-align: left'>".$a->NM_WP."</td>
                        <td style='text-align: center'>".$btn_cetak."</td>
                        <td style='text-align: right'>".number_format($a->TOTAL,0,',','.')."</td>
                    </tr>";
                }else{
                    $btn_cetak = "<button class='btn btn-xs btn-danger' title='Cetak PDF' disabled><i class='fa fa-file-pdf-o'></i></button>";
                    $tr .= "
                    <tr id=''>
                        <th style='text-align: center'>".$no++."</th>
                        <td>".$nop."</td>
                        <td style='text-align: center'>".$a->THN_PAJAK_SPPT."</td>
                        <td style='text-align: left'>".$a->NM_WP."</td>
                        <td style='text-align: center'>".$btn_cetak."</td>
                        <td style='text-align: right'>".number_format($a->TOTAL,0,',','.')."</td>
                    </tr>";
                }

                
                $total += $a->TOTAL;
                
            }

            $jml_nop = $no - 1;

            if($row->STATUS==1){
                $cetak_bukti_all = "Bukti Pembayaran Semua NOP : <a href='".base_url('dafnom/bukti_pembayaran_dafnom/').$row->KOBIL."/".$id."' class='btn btn-xs btn-danger' title='Cetak PDF' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
            }else{
                $cetak_bukti_all = "Bukti Pembayaran Semua NOP : <button class='btn btn-xs btn-danger' title='Cetak PDF' disabled><i class='fa fa-file-pdf-o'></i></button>";
            }
            

            $data = [
                'status' => 1,
                'tr' => $tr,
                'kobil' => $row->KOBIL,
                'jml_nop' => $jml_nop,
                'total' => number_format($total,0,',','.'),
                'cetak_bukti_all'=> $cetak_bukti_all

            ];
        }else{
            $data = [
                'status' => 0,
                'data' => 'Data tidak ditemukan !'
            ];
        }

        
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data);
        
    }

    function dafnom_cetak($id){
        $data['cetak'] = true;
        $rk = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id' 
        ORDER BY kd_blok, no_urut, kd_jns_op ASC")->result();
        $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();
        $query2 = $this->db->query("SELECT TO_CHAR(EXPIRED_AT, 'YYYY-MM-DD HH24:MI:SS') AS EXPIRED, TO_CHAR(CREATED_AT, 'YYYY-MM-DD HH24:MI:SS') AS CREATED_AT FROM DATA_BILLING WHERE ID='$id'")->row();
        if (count($rk) > 0) {
            $data['res'] = $rk;
            $data['kobil'] = $query->KOBIL;
            $data['kec'] = $query->NAMA_KECAMATAN;
            $data['kel'] = $query->NAMA_KELURAHAN;
            $data['ket'] = $query->KETERANGAN;
            $data['expired'] = $query2->EXPIRED;
            $data['created'] = $query2->CREATED_AT;
            // dd($data);
            $this->load->view('dafnom/cetakdafnom', $data);
        }
    }

    function dafnom_cetak_lunas($id){
        $data['cetak'] = true;
        $rk = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id' 
        ORDER BY kd_blok, no_urut, kd_jns_op ASC");
        $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();
        $query2 = $this->db->query("SELECT TO_CHAR(EXPIRED_AT, 'YYYY-MM-DD HH24:MI:SS') AS EXPIRED,
        TO_CHAR(CREATED_AT, 'YYYY-MM-DD HH24:MI:SS') AS CREATED_AT,
        TO_CHAR(TGL_BAYAR, 'YYYY-MM-DD HH24:MI:SS') AS TGL_BAYAR FROM DATA_BILLING WHERE ID='$id'")->row();
        $row = $rk->row();
        $kd_propinsi = $row->KD_PROPINSI;
        $kd_dati2 = $row->KD_DATI2;
        $kd_kecamatan = $row->KD_KECAMATAN;
        $kd_kelurahan = $row->KD_KELURAHAN;
        $kd_blok = $row->KD_BLOK;
        $no_urut = $row->NO_URUT;
        $kd_jns_op = $row->KD_JNS_OP;
        $thn_pajak = $row->THN_PAJAK_SPPT;

        if(ENVIRONMENT=='development'){
            $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
            END BANK FROM SPO_DEV.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
            AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
            AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
        }else{
            $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
            END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
            AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
            AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
        }

        $kode_bank = '';
        if($s_ntpd->KODE_BANK_BAYAR==''){
            $kode_bank = $s_ntpd->BANK;
        }else{
            $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
        }

        $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();

        if (count($rk->result()) > 0) {
            $data['res'] = $rk->result();
            $data['kobil'] = $query->KOBIL;
            $data['kec'] = $query->NAMA_KECAMATAN;
            $data['kel'] = $query->NAMA_KELURAHAN;
            $data['ket'] = $query->KETERANGAN;
            $data['expired'] = $query2->EXPIRED;
            $data['created'] = $query2->CREATED_AT;
            $data['tgl_bayar'] = date('d-m-Y H:i:s', strtotime($query2->TGL_BAYAR));
            $data['ntpd'] = $s_ntpd->PENGESAHAN;
            $data['bank'] = $bank->NAMA_BANK;

            // dd($data);
            $this->load->view('dafnom/cetakdafnomlunas', $data);
        }
    }

    function bukti_pembayaran($nop){
        $exp = explode(".",$nop);
        $kd_propinsi = $exp[0];
        $kd_dati2 = $exp[1];
        $kd_kecamatan = $exp[2];
        $kd_kelurahan = $exp[3];
        $kd_blok = $exp[4];
        $no_urut = $exp[5];
        $kd_jns_op = $exp[6];

        $nop2 = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'-'.$no_urut.'.'.$kd_jns_op;
        
        $data['cetak'] = true;
        $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE KD_PROPINSI='$kd_propinsi' 
        AND KD_DATI2='$kd_dati2' AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' 
        AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut' AND KD_JNS_OP='$kd_jns_op'")->row();

        $id = $res->DATA_BILLING_ID;
        $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();

        $cari = $this->db->query("SELECT A.JLN_WP_SPPT, B.NM_KECAMATAN, C.NM_KELURAHAN
        FROM sppt_oltp A LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN LEFT JOIN REF_KELURAHAN C ON A.KD_KELURAHAN=C.KD_KELURAHAN
        AND A.KD_KECAMATAN=C.KD_KECAMATAN WHERE A.KD_PROPINSI='$kd_propinsi' 
        AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' AND A.KD_KELURAHAN='$kd_kelurahan' 
        AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'")->row();

        $thn_pajak = $res->THN_PAJAK_SPPT;

        if(ENVIRONMENT=='development'){
            $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
            END BANK FROM SPO_DEV.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
            AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
            AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
        }else{
            $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
            END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
            AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
            AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
        }

        // $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
        // END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
        // AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
        // AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='2020'")->row();

        $this->load->library('ciqrcode');
        $nqr = 'qr_ntpd/' . $s_ntpd->PENGESAHAN .'_'. $query->KOBIL .'_'. str_replace(".", "", $nop) . '.png';

        @unlink($nqr);

        $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt-ntpd?ntpd=' . encrypt_url($s_ntpd->PENGESAHAN) . '&kobil=' . encrypt_url($query->KOBIL) . '&nop=' . encrypt_url($nop);
        $params['level'] = 'L';
        $params['size'] = 2;
        $params['savename'] = FCPATH . $nqr;
        $this->ciqrcode->generate($params);

        $kode_bank = '';
        if($s_ntpd->KODE_BANK_BAYAR==''){
            $kode_bank = $s_ntpd->BANK;
        }else{
            $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
        }

        $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();

        $data['kobil'] = $query->KOBIL;
        $data['kec'] = $query->NAMA_KECAMATAN;
        $data['kel'] = $query->NAMA_KELURAHAN;
        $data['ket'] = $query->KETERANGAN;
        
        $data['ntpd'] = $s_ntpd->PENGESAHAN;
        $data['nop'] = $nop2;
        $data['nm_wp'] = $res->NM_WP;
        $data['alamat'] = $cari->JLN_WP_SPPT;
        $data['nm_kec'] = $cari->NM_KECAMATAN;
        $data['nm_kel'] = $cari->NM_KELURAHAN;
        $data['thn_pajak'] = $res->THN_PAJAK_SPPT;
        $data['tgl_bayar'] = date_indo(date('Y-m-d', strtotime($query->TGL_BAYAR)));
        $data['pokok'] = number_format($res->POKOK,0,',','.');
        $data['denda'] = number_format($res->DENDA,0,',','.');
        $data['total'] = number_format($res->TOTAL,0,',','.');
        $data['terbilang'] = penyebut_cap($res->TOTAL).' Rupiah';
        $data['bank'] = $bank->NAMA_BANK;
        $data['qr'] = $nqr;

        $this->load->view('dafnom/buktipembayaran', $data);
    }
    private  function _directory_exist($path_folder){
		$path = FCPATH .$path_folder;
		$return=true;
		if(!file_exists($path)){
			$return=false;
		}
		return $return;
	}
    function cli_generatebukti_pembayaran_dafnom($create_limit=0){
        if($this->input->is_cli_request()){
            $cek_sql="SELECT V_DAFNOM.ID,V_DAFNOM.KOBIL,count(BILLING_KOLEKTIF.DATA_BILLING_ID) total
                    FROM V_DAFNOM
                    LEFT OUTER JOIN BILLING_KOLEKTIF
                        ON BILLING_KOLEKTIF.DATA_BILLING_ID=V_DAFNOM.ID
                    having count(BILLING_KOLEKTIF.DATA_BILLING_ID)>".$create_limit."
                    GROUP BY V_DAFNOM.ID,V_DAFNOM.KOBIL";
            $cek=$this->db->query($cek_sql);
            if($cek->num_rows>0){
                foreach($cek->result_array() as $item){
                    if(!$this->_directory_exist("pdf_dafnom/".$item['KOBIL']."_".$item['ID'].".pdf")){
                        // $this->_generate_bukti_pembayaran_dafnom($kobil,$id);
                         $this->_generate_bukti_pembayaran_dafnom($item['KOBIL'],$item['ID']);
                    }
                }
            }
        }
    }
    private function _generate_bukti_pembayaran_dafnom($kobil, $id,$data=false){
        $result['data'] =($data)?$data: $this->_getData_pembayaran_dafnom($kobil, $id,false);
        $html=$this->load->view('dafnom/buktipembayarandafnom', $result,true);
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P', 'orientation' => 'P']);
        $mpdf->SetDisplayMode('fullpage');
        //$mpdf->SetWatermarkImage('kabmalang.png',  0.1,'','');
        $mpdf->showWatermarkImage = true;
        $mpdf->AddPageByArray([
            'margin-left' => 3,
            'margin-right' => 3,
            'margin-top' => 3,
            'margin-bottom' => 3,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->debug = true;
        $mpdf->Output("pdf_dafnom/". $kobil."_".$id.".pdf", "F");
    }
    private function _getData_pembayaran_dafnom($kobil,$id,$create_local=false,$create_limit=0){
        $return=false;
        if($kobil&&$id){
            $data['cetak'] = true;
            $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id'");

            $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();

            $data_array = array();

            foreach ($res->result() as $a) {

                $kd_propinsi = $a->KD_PROPINSI;
                $kd_dati2 = $a->KD_DATI2;
                $kd_kecamatan = $a->KD_KECAMATAN;
                $kd_kelurahan = $a->KD_KELURAHAN;
                $kd_blok = $a->KD_BLOK;
                $no_urut = $a->NO_URUT;
                $kd_jns_op = $a->KD_JNS_OP;

                $nop = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'.'.$no_urut.'.'.$kd_jns_op;
                $nop2 = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'-'.$no_urut.'.'.$kd_jns_op;

                $cari = $this->db->query("SELECT A.JLN_WP_SPPT, B.NM_KECAMATAN, C.NM_KELURAHAN
                FROM sppt_oltp A LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN LEFT JOIN REF_KELURAHAN C ON A.KD_KELURAHAN=C.KD_KELURAHAN
                AND A.KD_KECAMATAN=C.KD_KECAMATAN WHERE A.KD_PROPINSI='$kd_propinsi' 
                AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' AND A.KD_KELURAHAN='$kd_kelurahan' 
                AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'")->row();

                $thn_pajak = $a->THN_PAJAK_SPPT;

                if(ENVIRONMENT=='development'){
                    $s_ntpd = $this->db->query("SELECT TO_CHAR(TGL_PEMBAYARAN_SPPT, 'DD-MM-YYYY HH24:MI:SS') AS TGL_BAYAR, PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
                    END BANK FROM SPO_DEV.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                    AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                    AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
                }else{
                    $s_ntpd = $this->db->query("SELECT TO_CHAR(TGL_PEMBAYARAN_SPPT, 'DD-MM-YYYY HH24:MI:SS') AS TGL_BAYAR, PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
                    END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                    AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                    AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
                }
                // $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
                // END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                // AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                // AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='2020'")->row();

                $kode_bank = '';
                if($s_ntpd->KODE_BANK_BAYAR==''){
                    $kode_bank = $s_ntpd->BANK;
                }else{
                    $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
                }

                $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();

                $this->load->library('ciqrcode');
                $nqr = 'qr_ntpd/' . $s_ntpd->PENGESAHAN .'_'. $query->KOBIL .'_'. str_replace(".", "", $nop) . '.png';

                @unlink($nqr);

                $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt-ntpd?ntpd=' . encrypt_url($s_ntpd->PENGESAHAN) . '&kobil=' . encrypt_url($query->KOBIL) . '&nop=' . encrypt_url($nop);
                $params['level'] = 'L';
                $params['size'] = 2;
                $params['savename'] = FCPATH . $nqr;
                $this->ciqrcode->generate($params);

                $data['kobil'] = $query->KOBIL;
                $data['kec'] = $query->NAMA_KECAMATAN;
                $data['kel'] = $query->NAMA_KELURAHAN;
                $data['ket'] = $query->KETERANGAN;
                
                $data['ntpd'] = $s_ntpd->PENGESAHAN;
                $data['nop'] = $nop2;
                $data['nm_wp'] = $a->NM_WP;
                $data['alamat'] = $cari->JLN_WP_SPPT;
                $data['nm_kec'] = $cari->NM_KECAMATAN;
                $data['nm_kel'] = $cari->NM_KELURAHAN;
                $data['thn_pajak'] = $a->THN_PAJAK_SPPT;
                $data['tgl_bayar'] = date_indo(date('Y-m-d', strtotime($query->TGL_BAYAR)));
                $data['pokok'] = number_format($a->POKOK,0,',','.');
                $data['denda'] = number_format($a->DENDA,0,',','.');
                $data['total'] = number_format($a->TOTAL,0,',','.');
                $data['terbilang'] = penyebut_cap($a->TOTAL).' Rupiah';
                $data['bank'] = $bank->NAMA_BANK;
                $data['qr'] = $nqr;
                array_push($data_array, $data);
            }
            $return=$data_array;
            if($res->num_rows>$create_limit){
                $this->_generate_bukti_pembayaran_dafnom($kobil,$id, $return);
            }
        }
        return $return;
    }
    function bukti_pembayaran_dafnom($kobil, $id){
        if($this->_directory_exist("pdf_dafnom/".$kobil."_".$id.".pdf")){
            $name=$kobil."_".$id.".pdf";
            $file="pdf_dafnom/".$name;
            header("Content-type: application/pdf");
            header("Content-Length:".filesize($file));
            header("Content-Disposition: inline; filename=".$name);
            readfile($file);
        }else{
            $result['data'] = $this->_getData_pembayaran_dafnom($kobil, $id,true);
            $this->load->view('dafnom/buktipembayarandafnom', $result);
        }
    }

    public function cek_list_dafnom(){
        $return=array();
        $getData=$this->input->post(null,true);
        if ($this->input->is_ajax_request()&&$getData) {
            $null="<span>Data belum ditemukan !</span>";
            $resultData=array("listNop"=>$null,"listNop_temp"=>$null);
            $this->output->enable_profiler(false);
            $kd_pengguna = $_SESSION['pbb_ku'];
            $kec = (isset($getData['kecamatan']))?$getData['kecamatan']:null;
            $kel = (isset($getData['kelurahan']))?$getData['kelurahan']:null;
            $thn_pajak = (isset($getData['thn_pajak']))?$getData['thn_pajak']:null;
            $nominal = (isset($getData['batas_nominal']))?$getData['batas_nominal']:null;
            $blok = (isset($getData['blok']))?$getData['blok']:null;
            $wh=(trim($blok)!="")?"AND KD_BLOK='$blok'":"";
            $strListNop_temp="SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' $wh ORDER BY NM_WP ASC";
            $getListNop_temp=$this->db->query($strListNop_temp);
            $total=0;
            $batas_nominal = str_replace('.','',$nominal);

            if($getListNop_temp->num_rows()>0){
                $no=1;
                $dataZ="";
                foreach ($getListNop_temp->result() as $a) {
                    $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                    $nopx = $a->KD_PROPINSI.$a->KD_DATI2.$a->KD_KECAMATAN.$a->KD_KELURAHAN.$a->KD_BLOK.$a->NO_URUT.$a->KD_JNS_OP;
                    $dataZ .= "
                        <tr id=''>
                            <th style='text-align: center'>".$no++."</th>
                            <td>".$nop."</td>
                            <td style='text-align: left'>".$a->NM_WP."</td>
                            <td style='text-align: right'>".number_format($a->TOTAL,0,',','.')."</td>
                            <td style='text-align: center'><button data-nop='".$nopx."' class='hapus_nop btn btn-danger btn-xs'><i class='fa fa-trash'></i></button></td>
                        </tr>";
                    $total += $a->TOTAL;
                }
                $resultData["listNop_temp"]="<span style='float: left'><b><i>Jumlah NOP</i>&emsp;&emsp;: <span id='nop_terpilih'>".($no-1)."</span></b></span><br>
                <span style='float: left'><b><i>Jumlah PBB</i>&emsp;&emsp;: <span id='jml_pbb'>Rp. ".number_format($total,0,',','.')."</span></b></span><br><br>
                <table class='table table-bordered table-striped'>
                <thead>
                    <tr>
                        <th style='text-align: center'>No</th>
                        <th style='text-align: center'>NOP</th>
                        <th style='text-align: center'>Nama WP</th>
                        <th style='text-align: center'>Nominal</th>
                        <th style='text-align: center'>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                ".$dataZ."
                </tbody>
                <tfoot>
                <tr>
                    <th colspan='3' style='text-align: center'>Total</th>
                    <th style='text-align: right'>".number_format($total,0,',','.')."</th>
                </tr>
                </tfoot>
                </table>";
            }
            $batas_nominal=$batas_nominal-$total;
            $strListNop="SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok,
            no_urut, kd_jns_op, thn_pajak_sppt, pbb_yg_harus_dibayar_sppt pbb,
            nm_wp_sppt, pbb_yg_harus_dibayar_sppt pokok, CASE WHEN thn_pajak_sppt
            >=2003 AND thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 
            (kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END Denda,
            pbb_yg_harus_dibayar_sppt + CASE WHEN thn_pajak_sppt >= 2003 AND
            thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 (kd_dati2, kd_kecamatan,
            kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END total
            FROM sppt_oltp WHERE status_pembayaran_sppt=0 AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' AND pbb_yg_harus_dibayar_sppt <= 500000 $wh 
            and  (kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op) not in 
                (SELECT kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op 
                FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel')
            and (kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op) not in 
                (SELECT kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
            from BILLING_KOLEKTIF where THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel')
            ORDER BY nm_wp_sppt ASC";
            $getListNop=$this->db->query($strListNop);
            if($getListNop->num_rows()>0&&$batas_nominal){
                $cek_total=0;
                $datax="";
                $no=1;
                $total=0;
                foreach ($getListNop->result() as $a) {
                    $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                    $nopx = str_replace('.','',$nop);
                    $cek_total += $a->TOTAL;
                    if($cek_total <= $batas_nominal){
                        $datax .= "
                        <tr>
                            <th style='text-align: center' id='nomer'>".$no++."</th>
                            <td><input type='checkbox' id='cek_satu[]' name='cek_satu[]' class='cek_satu' value='".$nop.'|'.str_replace("'", "`",$a->NM_WP_SPPT).'|'.$a->POKOK.'|'.$a->DENDA.'|'.$a->TOTAL.'|'.$a->THN_PAJAK_SPPT.'|'.$a->PBB.'|'.$nominal."'></td>
                            <td>".$nop."</td>
                            <td style='text-align: left'>".$a->NM_WP_SPPT."</td>
                            <td style='text-align: right' id='jml'>".number_format($a->TOTAL,0,',','.')."</td>
                        </tr>";
                            
                        $total += $a->TOTAL;
                    }
                    
                }
                $resultData["listNop"]="
                <span style='float: left'><b><i>Jumlah NOP</i> &emsp;&emsp;: <span id='akumulasi_nop'>0</span></b></span><br>
                <span style='float: left'><b><i>Akumulasi</i>&emsp;&emsp;&emsp;: Rp. <span id='akumulasi'>0</span></b></span><br>
                <br>
                <table id='tblNOP' class='table table-bordered table-striped'>
                    <thead>
                        <tr>
                            <th style='text-align: center'>No</th>
                            <th style='text-align: center'><input type='checkbox' id='cek_all' name='cek_all' class='cek_all' value='0' ></th>
                            <th style='text-align: center'>NOP</th>
                            <th style='text-align: center'>Nama WP</th>
                            <th style='text-align: center'>Nominal</th>
                        </tr>
                    </thead>
                    <tbody id='tbody_nop'>
                    ".$datax."
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan='4' style='text-align: center'>Total</th>
                        <th style='text-align: right'><span id='total'>".number_format($total,0,',','.')."</span><input type='hidden' id='total_nop' value='".($no-1)."'></th>
                    </tr>
                    </tfoot>
                </table>";
            }
            
            $return=$resultData;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($return)); 
    }
    public function add()
    {
        $kd_pengguna = $_SESSION['pbb_ku'];
        if($this->session->userdata("pbb_kg")=="14"){
            $getKec="SELECT rk.KD_KECAMATAN, rk.KD_KELURAHAN, rk.NM_KELURAHAN
                    FROM P_PENGGUNA pp 
                    LEFT OUTER JOIN P_KELURAHAN_USER pku 
                        ON pku.KODE_PENGGUNA =pp.KODE_PENGGUNA 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON concat( rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL 
                    WHERE pp.KODE_PENGGUNA='".$kd_pengguna."'";
            $getKec=$this->db->query($getKec);
            $where=($getKec->num_rows()>0)?" WHERE rk.KD_KECAMATAN='".$getKec->row()->KD_KECAMATAN."'":" ";
            $str="SELECT rk.KD_KECAMATAN,rk.NM_KECAMATAN 
                    FROM
                        REF_KECAMATAN rk ".$where;
            $data["ref_kel"]=$getKec->result();
            
        }else{
            $where="";
            if($this->session->userdata("pbb_kg")!="1"){
                $upt = $this->db->query("SELECT * FROM P_UPT_USER WHERE KODE_PENGGUNA='$kd_pengguna'")->row();
                $del_temp = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna'");
                $kode_upt = $upt->KODE_UPT;
                $where="WHERE A.KODE_UPT='".$kode_upt."'";
            }
            $str="SELECT A.*, B.* FROM P_UPT_KECAMATAN A  LEFT JOIN REF_KECAMATAN B 
                ON A.KODE_KECAMATAN=B.KD_KECAMATAN ".$where." ORDER BY B.NM_KECAMATAN ASC";
            if($this->session->userdata("pbb_kg")=="1"){
                $str="SELECT B.* FROM REF_KECAMATAN B 
                        ORDER BY B.NM_KECAMATAN ASC";
            }
            if($this->session->userdata("pbb_kg")=="13"){
                $strKec="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
                $get=$this->db->query($strKec);
                $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:"-";
                $getKel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                        from ref_kelurahan
                                        where kd_kecamatan='".$KD_KECAMATAN."' 
                                        order by kd_kecamatan asc,kd_kelurahan asc");
                $data["ref_kel"]=$getKel->result();
            }
        }
        $data['ref_kec'] = $this->db->query($str)->result();
        $data['action_cek'] = 'dafnom/cek_list_dafnom';
        $data['action_temp'] = 'dafnom/add_temp';
        $data['action'] = 'dafnom/create_action';
        $data['judul'] = 'Tambah';
        $data['tahun'] = date('Y');
        $data['status'] = '';
        $data['status_btn'] = '';
        $data['jml_temp'] = 0;
        return view('dafnom/add_tes', $data);
    }

    public function add_temp(){

        $this->db->trans_start();
        $kd_pengguna = $_SESSION['pbb_ku'];
        $kd_kecamatan = '';
        $kd_kelurahan = '';
        $thn_pajak = '';
        $nominal = '';

        foreach ($_POST['cek_satu'] as $key => $value) {
            $data = explode("|",$_POST['cek_satu'][$key]);
            $nop = explode(".", $data[0]);
            $nm_wp = $data[1];
            $pokok = $data[2];
            $denda = $data[3];
            $total = $data[4];
            $thn_pajak = $data[5];
            $pbb = $data[6];
            $nominal = $data[7];

            //NOP
            $kd_propinsi = $nop[0];
            $kd_dati2 = $nop[1];
            $kd_kecamatan = $nop[2];
            $kd_kelurahan = $nop[3];
            $kd_blok = $nop[4];
            $no_urut = $nop[5];
            $kd_jns_op = $nop[6];

            $nopx = str_replace('.', '', $data[0]);

            //cek data sudah ada / belum
            $cek = $this->db->query("SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' 
            AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut' AND KD_JNS_OP='$kd_jns_op'")->result();

            //jika belum ada => insert ke tabel
            if(count($cek) == 0){
                $this->db->set('KD_PROPINSI', $kd_propinsi);
                $this->db->set('KD_DATI2', $kd_dati2);
                $this->db->set('KD_KECAMATAN', $kd_kecamatan);
                $this->db->set('KD_KELURAHAN', $kd_kelurahan);
                $this->db->set('KD_BLOK', $kd_blok);
                $this->db->set('NO_URUT', $no_urut);
                $this->db->set('KD_JNS_OP', $kd_jns_op);
                $this->db->set('THN_PAJAK_SPPT', $thn_pajak);
                $this->db->set('PBB', $pbb);
                $this->db->set('NM_WP', $nm_wp);
                $this->db->set('POKOK', $pokok);
                $this->db->set('DENDA', $denda);
                $this->db->set('TOTAL', $total);

                $this->db->set('CREATED_AT', 'sysdate', false);
                $this->db->set('CREATED_BY', $kd_pengguna);
                $this->db->set('NOP', $nopx);

                $this->db->insert('BILLING_KOLEKTIF_TEMP'); 
            }           
        }

        $this->db->trans_complete();
        $this->db->query("commit");

		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();

            $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' ORDER BY NM_WP ASC")->result();

            if(count($res)>0){
                $datax = "";
                $no = 1;
                $total = 0;
                foreach ($res as $a) {
                    $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                    $nopx = $a->KD_PROPINSI.$a->KD_DATI2.$a->KD_KECAMATAN.$a->KD_KELURAHAN.$a->KD_BLOK.$a->NO_URUT.$a->KD_JNS_OP;
                    $datax .= "
                        <tr id=''>
                            <th style='text-align: center'>".$no++."</th>
                            <td>".$nop."</td>
                            <td style='text-align: left'>".$a->NM_WP."</td>
                            <td style='text-align: right'>".number_format($a->TOTAL,0,',','.')."</td>
                            <td style='text-align: center'><button data-nop='".$nopx."' class='hapus_nop btn btn-danger btn-xs'><i class='fa fa-trash'></i></button></td>
                        </tr>";
                    $total += $a->TOTAL;
                    
                }
    
                $datay = "
                <span style='float: left'><b><i>Jumlah NOP</i>&emsp;&emsp;: <span id='nop_terpilih'>".($no-1)."</span></b></span><br>
                <span style='float: left'><b><i>Jumlah PBB</i>&emsp;&emsp;: <span id='jml_pbb'>Rp. ".number_format($total,0,',','.')."</span></b></span><br><br>
                <table class='table table-bordered table-striped'>
                <thead>
                    <tr>
                        <th style='text-align: center'>No</th>
                        <th style='text-align: center'>NOP</th>
                        <th style='text-align: center'>Nama WP</th>
                        <th style='text-align: center'>Nominal</th>
                        <th style='text-align: center'>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                ".$datax."
                </tbody>
                <tfoot>
                <tr>
                    <th colspan='3' style='text-align: center'>Total</th>
                    <th style='text-align: right'>".number_format($total,0,',','.')."</th>
                </tr>
                </tfoot>
                </table>
                ";

                echo $datay;
                
            }else{
                echo "0";
            }
		} else {
			$this->db->trans_rollback();
            echo "0";
		}
    }

    public function create_action(){
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        
        //mencari max no_urut
        $this->db->trans_start();
        $query = $this->db->query("SELECT MAX(NO_URUT) as KODEX FROM DATA_BILLING")->row();
        $kode = $query->KODEX;
        $kode++;

        //Kode Billing
        $kd_propinsi = '35';
        $kd_dati2= '07';
        $kd_kecamatan =  $_POST['kec'];
        $kd_kelurahan =  $_POST['kel'];
        $kd_blok = '999';
        $no_urut = sprintf("%04s", $kode);
        $kd_jns_op = '1';
        $nop = $kd_propinsi.$kd_dati2.$kd_kecamatan.$kd_kelurahan.$kd_blok.$no_urut.$kd_jns_op;

        $blok = $_POST['blok'];
        $wh="";
        if(trim($blok)!=""){
            $wh = "AND KD_BLOK='$blok'";
        }
        

        $thn_pajak =  $_POST['thn_pajak'];
        //mencari nama kec, kel
        $kec = $this->db->query("SELECT * FROM REF_KECAMATAN WHERE KD_KECAMATAN='$kd_kecamatan'")->row();
        $kel = $this->db->query("SELECT * FROM REF_KELURAHAN WHERE KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan'")->row();
        $nm_kec = $kec->NM_KECAMATAN;
        $nm_kel = $kel->NM_KELURAHAN;

        //mencari jumlah nop yang termasuk dalam dafnom
        $cari = $this->db->query("SELECT COUNT(*) JML FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' 
        AND THN_PAJAK_SPPT='$thn_pajak' AND
        KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' $wh")->row();

        $nama_wp = 'Billing Kolektif UPT (DS. '.$nm_kel.', KEC. '.$nm_kec.') - Jumlah NOP : '.$cari->JML;
        
        //setting expired date
        $now = date('d/m/Y H:i:s');
        $s_expired = $this->db->query("SELECT EXPIRED FROM LAYANAN_DEV.CONFIG_EXPIRED WHERE ID='1'")->row();
        $kadaluwarsa = $s_expired->EXPIRED;

        $this->db->set('KD_PROPINSI', $kd_propinsi);
        $this->db->set('KD_DATI2', $kd_dati2);
        $this->db->set('KD_KECAMATAN', $kd_kecamatan);
        $this->db->set('KD_KELURAHAN', $kd_kelurahan);
        $this->db->set('KD_BLOK', $kd_blok);
        $this->db->set('NO_URUT', $no_urut);
        $this->db->set('KD_JNS_OP', $kd_jns_op);
        $this->db->set('TAHUN_PAJAK', $thn_pajak);
        $this->db->set('KD_STATUS', '0');
        $this->db->set('NAMA_WP', $nama_wp);
        $this->db->set('NAMA_KELURAHAN', $nm_kel);
        $this->db->set('NAMA_KECAMATAN', $nm_kec);
        $this->db->set('EXPIRED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS') +".$kadaluwarsa, false);
        $this->db->set('CREATED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS')", false);
        $this->db->set('CREATED_BY', $kd_pengguna);
        $this->db->set('KOBIL', $nop);
        $this->db->insert('DATA_BILLING');

        //mencari max id dari header billing kolektif
        $max = $this->db->query("SELECT MAX(ID) as IDX FROM DATA_BILLING")->row();
        $data_billing_id = $max->IDX;

        $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' 
        AND THN_PAJAK_SPPT='$thn_pajak' AND
        KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' $wh")->result();
        
        foreach ($res as $a) {
            $this->db->set('DATA_BILLING_ID', $data_billing_id);
            $this->db->set('KD_PROPINSI', $a->KD_PROPINSI);
            $this->db->set('KD_DATI2', $a->KD_DATI2);
            $this->db->set('KD_KECAMATAN', $a->KD_KECAMATAN);
            $this->db->set('KD_KELURAHAN', $a->KD_KELURAHAN);
            $this->db->set('KD_BLOK', $a->KD_BLOK);
            $this->db->set('NO_URUT', $a->NO_URUT);
            $this->db->set('KD_JNS_OP', $a->KD_JNS_OP);
            $this->db->set('THN_PAJAK_SPPT', $a->THN_PAJAK_SPPT);
            $this->db->set('PBB', $a->PBB);
            $this->db->set('NM_WP', $a->NM_WP);
            $this->db->set('POKOK', $a->POKOK);
            $this->db->set('DENDA', $a->DENDA);
            $this->db->set('TOTAL', $a->TOTAL);
            $this->db->set('EXPIRED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS') +".$kadaluwarsa, false);
            $this->db->insert('BILLING_KOLEKTIF');
        }

        $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' 
        AND THN_PAJAK_SPPT='$thn_pajak' AND
        KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' $wh");
        
        $this->db->trans_complete();
        
        $this->db->query("commit");
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
            $pesan = "sukses";
        } else {
			$this->db->trans_rollback();
            $pesan = "gagal";
                
		}
        
        echo $pesan;
    }

    public function fetch_kel(){
        if(isset($_POST['kd_kec']))
        {

        $kec = $_POST['kd_kec'];
        $res = $this->db->query("SELECT * FROM REF_KELURAHAN WHERE KD_KECAMATAN='$kec' ORDER BY NM_KELURAHAN ASC")->result();
        foreach ($res as $kel) {
            echo "<option value=".$kel->KD_KELURAHAN.">".$kel->NM_KELURAHAN."</option>";
        }
        exit;
        }
    }

    public function delete_temp(){

        $kd_pengguna = $_SESSION['pbb_ku'];
        $upt = $this->db->query("SELECT * FROM P_UPT_USER WHERE KODE_PENGGUNA='$kd_pengguna'")->row();
        $kode_upt = $upt->KODE_UPT;
        $data['ref_kec'] = $this->db->query("SELECT A.*, B.* FROM P_UPT_KECAMATAN A LEFT JOIN REF_KECAMATAN B 
                            ON A.KODE_KECAMATAN=B.KD_KECAMATAN WHERE A.KODE_UPT=$kode_upt ORDER BY B.NM_KECAMATAN ASC")->result();
        $data['action_cek'] = 'dafnom/add';
        $data['action_temp'] = 'dafnom/add_temp';
        $data['action'] = 'dafnom/create_action';
        $data['judul'] = 'Tambah';
        
        $kec = $_POST['kecamatan'];
        $kel = $_POST['kelurahan'];
        $thn_pajak = $_POST['thn_pajak'];

        //delete data di temporary dengan where thn_pajak, kec, kel dan kd_pengguna (user login)
        // $delete = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE THN_PAJAK_SPPT='$thn_pajak' AND
        // KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' AND CREATED_BY='$kd_pengguna'");
        
        //delete data di temporary dengan where kd_pengguna (user login)
        $delete = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna'");

        $data['ref_kel'] = $this->db->query("SELECT A.* FROM REF_KELURAHAN A LEFT JOIN REF_KECAMATAN B ON
            A.KD_KECAMATAN=B.KD_KECAMATAN WHERE A.KD_KECAMATAN='$kec' ORDER BY A.NM_KELURAHAN ASC")->result();
        $data['kec'] = $kec;
        $data['kel'] = $kel;
        $data['tahun'] = $thn_pajak;
        $data['nominal'] = $_POST['batas_nominal'];
        $data['list_nop'] = $this->db->query("SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok,
            no_urut, kd_jns_op, thn_pajak_sppt, pbb_yg_harus_dibayar_sppt pbb,
            nm_wp_sppt, pbb_yg_harus_dibayar_sppt pokok, CASE WHEN thn_pajak_sppt
            >=2003 AND thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 
            (kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END Denda,
            pbb_yg_harus_dibayar_sppt + CASE WHEN thn_pajak_sppt >= 2003 AND
            thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 (kd_dati2, kd_kecamatan,
            kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END total
            FROM sppt_oltp WHERE status_pembayaran_sppt=0 AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' AND pbb_yg_harus_dibayar_sppt <= 500000
            and  (kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op) not in 
                (SELECT kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel')
            ORDER BY nm_wp_sppt ASC")->result();
            
        $data['status'] = 'readonly';
        $data['status_btn'] = 'disabled';
        
        return view('dafnom/add_tes', $data);
    }

    function delete_temp2(){
        $kd_pengguna = $_SESSION['pbb_ku'];
        $thn_pajak = $_POST['thn_pajak'];
        $kec = $_POST['kec'];
        $kel = $_POST['kel'];
        $nominal = $_POST['nominal'];
        $blok = $_POST['blok'];
        $wh="";
        if(trim($blok)!=""){
            $wh = "AND KD_BLOK='$blok'";
        }

        $this->db->trans_start();
        $del_temp = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna'");
        $this->db->query("commit");

        // $list_nop = $this->db->query("SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok,
        //     no_urut, kd_jns_op, thn_pajak_sppt, pbb_yg_harus_dibayar_sppt pbb,
        //     nm_wp_sppt, pbb_yg_harus_dibayar_sppt pokok, CASE WHEN thn_pajak_sppt
        //     >=2003 AND thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 
        //     (kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
        //     pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END Denda,
        //     pbb_yg_harus_dibayar_sppt + CASE WHEN thn_pajak_sppt >= 2003 AND
        //     thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 (kd_dati2, kd_kecamatan,
        //     kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
        //     pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END total
        //     FROM sppt_oltp WHERE status_pembayaran_sppt=0 AND THN_PAJAK_SPPT='$thn_pajak' AND
        //     KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' $wh ORDER BY nm_wp_sppt ASC")->result();

        // $datax = "";
        // $no = 1;
        // $cek_total = 0;
        // $total = 0;

        // $batas_nominal = str_replace(',','',$nominal);
        // foreach ($list_nop as $a) {
        //     $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
        //     $nopx = str_replace('.','',$nop);

        //     $cek_total += $a->TOTAL;
        //     if($cek_total <= $batas_nominal){
        //         $datax .= "
        //         <tr>
        //             <th style='text-align: center' id='nomer'>".$no++."</th>
        //             <td><input type='checkbox' id='cek_satu[]' name='cek_satu[]' class='cek_satu' value='".$nop.'|'.str_replace("'", "`",$a->NM_WP_SPPT).'|'.$a->POKOK.'|'.$a->DENDA.'|'.$a->TOTAL.'|'.$a->THN_PAJAK_SPPT.'|'.$a->PBB.'|'.$nominal."'></td>
        //             <td>".$nop."</td>
        //             <td style='text-align: left'>".$a->NM_WP_SPPT."</td>
        //             <td style='text-align: right' id='jml'>".number_format($a->TOTAL,0,',','.')."</td>
        //         </tr>";
                    
        //         $total += $a->TOTAL;
        //     }
            
        // }

        // $data['list_nop'] = "
        // <span style='float: left'><b><i>Jumlah NOP</i> &emsp;&emsp;: <span id='akumulasi_nop'>0</span></b></span><br>
        // <span style='float: left'><b><i>Akumulasi</i>&emsp;&emsp;&emsp;: Rp. <span id='akumulasi'>0</span></b></span><br>
        // <br>
        // <table id='tblNOP' class='table table-bordered table-striped'>
        // <thead>
        //     <tr>
        //         <th style='text-align: center'>No</th>
        //         <th style='text-align: center'><input type='checkbox' id='cek_all' name='cek_all' class='cek_all' value='0' ></th>
        //         <th style='text-align: center'>NOP</th>
        //         <th style='text-align: center'>Nama WP</th>
        //         <th style='text-align: center'>Nominal</th>
        //     </tr>
        // </thead>
        // <tbody id='tbody_nop'>
        // ".$datax."
        // </tbody>
        // <tfoot>
        // <tr>
        //     <th colspan='4' style='text-align: center'>Total</th>
        //     <th style='text-align: right'><span id='total'>".number_format($total,0,',','.')."</span><input type='hidden' id='total_nop' value='".($no-1)."'></th>
        // </tr>
        // </tfoot>
        // </table>
        // ";

        $data['list_dafnom'] = '0';
        
        $this->db->trans_complete();

        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function realisasi(){
        
        $data['tahun'] = date('Y');
        $data['tahun_start'] = 2003;
        $data['tahun_end'] = date('Y');

        if(isset($_GET['thn_pajak'])){
            $data['tahun'] = $_GET['thn_pajak'];
        }

        $KD_KECAMATAN = ($this->input->get('KD_KECAMATAN', true))?$this->input->get('KD_KECAMATAN', true):"all";
        $KD_KELURAHAN = ($this->input->get('KD_KELURAHAN', true))?$this->input->get('KD_KELURAHAN', true):"all";
        $where = '';
        $and="";
        $pbb_ku = $this->session->userdata('pbb_ku');
        if ($this->session->userdata('pbb_kg') == 8) {
            $where = " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$pbb_ku."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
        }
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $where = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN  $where order by kd_kecamatan asc")->result();
        $kel=false;
        if ($KD_KECAMATAN) {
            $kel=(object)array("KD_KELURAHAN"=>"","NM_KELURAHAN"=>"Semua Kelurahan");
            $kel=(object)array($kel);
            if($KD_KECAMATAN!="all"){
                $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' ". $and."
                                    order by kd_kecamatan asc,kd_kelurahan asc")->result();
            }
        }
        $data['kec']=$kec;
        $data['kel']=$kel;
        $data['KD_KECAMATAN']=$KD_KECAMATAN;
        $data['KD_KELURAHAN']=$KD_KELURAHAN;
        $data['direct_data']=$this->input->get(null,true);
        // $data['ref_tahun'] = $this->Mdafnom->ref_tahun($kd_pengguna);
        return view('dafnom/realisasi', $data);
    }

    function ajax_realisasi($kec,$kel,$thn)
    {
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        $this->load->library('datatables');
        $this->datatables->select("ID, KOBIL, NAMA_KECAMATAN, NAMA_KELURAHAN,TAHUN_PAJAK, KETERANGAN, CREATED_AT, TGL_BAYAR, JML_NOP, PBB");
        if($this->session->userdata("pbb_kg")!="1"&&$this->session->userdata("pbb_kg")!="0"&&$this->session->userdata("pbb_kg")!="22"){
            $this->datatables->where('CREATED_BY', $kd_pengguna);
        }
        $this->datatables->where('STATUS', '1');
        if($thn != 'all'){
            $this->datatables->where('TAHUN_PAJAK', $thn);
        }
        if($kec != 'all'){
            $this->datatables->where('KD_KECAMATAN', $kec);
        }
        if($kel != 'all'){
            $this->datatables->where('KD_KELURAHAN', $kel);
        }
        $this->datatables->from('V_DAFNOM');
        $this->datatables->order_by('TAHUN_PAJAK',"DESC");
        header('Content-Type: application/json');
        echo $this->datatables->generate();
    }

    public function import_excel()
    {
        // return view('dafnom/import_excel');
        return view('dafnom/import_excel2');
    }

    function prosesimport()
    {
        $this->load->library('exceldua');
        $cell = [0, 1, 2, 3, 4];

        // dd($_FILES['data_excel']);
        $exp = explode('.', $_FILES['data_excel']['name']);
        $exe = $exp[count($exp) - 1];

        if ($exe == 'xlsx') {

            $reader = $this->exceldua->reader($_FILES['data_excel']['tmp_name'], 6, $cell);
            // unlink($file['full_path']);

            // dd($reader);

            $data = [
                'status' => 1,
                'data' => $reader
            ];
        } else {
            $data = [
                'status' => 0,
                'data' => 'File harus .xlsx'
            ];
        }
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function prosesimport2()
    {
        $data_json = [
            'status' => 0,
            'data' => 'File harus .xlsx'
        ];
        try{   
        
            if($this->session->userdata()&&$_FILES){
                $kd_pengguna = $_SESSION['pbb_ku'];
                $this->load->library('exceldua');
                $cell = [0, 1, 2, 3, 4, 5];
                // dd($_FILES['data_excel']);
                $exp = explode('.', $_FILES['data_excel']['name']);
                $exe = $exp[count($exp) - 1];
                $userdata=$this->session->userdata();
                
                $this->db->trans_start();
                
                if ($exe == 'xlsx') {
                    $reader = $this->exceldua->reader($_FILES['data_excel']['tmp_name'], 1, $cell);
                    // dd($reader);
                    // unlink($file['full_path']);
                    $del_temp = $this->db->query("DELETE FROM TEMP_DAFNOM WHERE CREATED_BY='$kd_pengguna'");
                    $data = [];
                    // $thn_pajak = date('Y');
                    $jml = count($reader);
                    $inputCheck=true;
                    $no = 1;
                    $getKec=false;
                    $getKel=false;
                    foreach ($reader as $item) {
                        if($no < $jml){
                            $nop = str_replace("-", ".", $item[1]);
                            $exp = explode('.', $nop);
                            $kd_propinsi = $exp[0];
                            $kd_dati2 = $exp[1];
                            $kd_kecamatan = $exp[2];
                            $kd_kelurahan = $exp[3];
                            $kd_blok = $exp[4];
                            $no_urut = $exp[5];
                            $kd_jns_op = $exp[6];                   
                            if($no==1){
                                $getKec=$kd_kecamatan;
                                $getKel=$kd_kelurahan;
                            }elseif($getKec!=$kd_kecamatan||$getKel!=$kd_kelurahan){
                                $inputCheck=false;
                            }
                            $data[] = array(
                                'KD_PROPINSI' => $kd_propinsi,
                                'KD_DATI2' => $kd_dati2,
                                'KD_KECAMATAN' => $kd_kecamatan,
                                'KD_KELURAHAN' => $kd_kelurahan,
                                'KD_BLOK' => $kd_blok,
                                'NO_URUT' => $no_urut,
                                'KD_JNS_OP' => $kd_jns_op,
                                'THN_PAJAK_SPPT' => $item[4],
                                'PBB' => $item[5],
                                'NM_WP' => $item[2],
                                'CREATED_BY' => $kd_pengguna,
                            );
                        }
                        $no++;
                    }
                    if($inputCheck){
                        if($this->session->userdata("pbb_kg")=="14"){
                            $str="SELECT rk.KD_KECAMATAN, rk.KD_KELURAHAN, rk.NM_KELURAHAN
                                    FROM P_PENGGUNA pp 
                                    LEFT OUTER JOIN P_KELURAHAN_USER pku 
                                        ON pku.KODE_PENGGUNA =pp.KODE_PENGGUNA 
                                    LEFT OUTER JOIN REF_KELURAHAN rk 
                                        ON concat( rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL 
                                    WHERE pp.KODE_PENGGUNA='".$kd_pengguna."'
                                        and rk.KD_KECAMATAN='".$getKec."'
                                        and rk.KD_KELURAHAN='".$getKel."'";
                            $check=$this->db->query($str);
                            $inputCheck=($check->num_rows()>0)?$inputCheck:false;
                            
                        }elseif($this->session->userdata("pbb_kg")=="8"||$this->session->userdata("pbb_kg")=="13"){
                            $str="SELECT puk.KODE_KECAMATAN 
                                FROM P_UPT_USER puu 
                                LEFT OUTER JOIN P_UPT_KECAMATAN puk 
                                    ON REGEXP_REPLACE(puk.KODE_UPT,' ','') =REGEXP_REPLACE(puu.KODE_UPT,' ','')
                                WHERE puu.KODE_PENGGUNA ='".$userdata["pbb_ku"]."'
                                AND puk.KODE_KECAMATAN='".$getKec."'";
                            $check=$this->db->query($str);
                            $inputCheck=($check->num_rows()>0)?$inputCheck:false;
                        }
                    }
                    $data_json = [
                        'status' => 0,
                        'data' => 'File Excel tidak sesuai dengan Kecamatan/Kelurahan!!!'
                    ];
                    if($inputCheck){
                        $this->db->insert_batch('TEMP_DAFNOM', $data);
                        $this->db->trans_complete();
                        
                        $res = $this->db->query("SELECT a.* ,
                        case when (c.status_pembayaran_sppt=0 or c.status_pembayaran_sppt is null) and b.thn_pajak_sppt is null then 1 else 0 end status,
                        case when status_pembayaran_sppt=1 then 'Pbb Telah lunas' else 
                        case when b.thn_pajak_sppt is not null then 'Masuk dafnom lain' else '----' end  
            end keterangan 
                        from temp_dafnom a
                        left join billing_kolektif b on a.kd_propinsi=b.kd_propinsi
                                                    and a.kd_dati2=b.kd_dati2
                                                    and a.kd_kecamatan=b.kd_kecamatan
                                                    and a.kd_kelurahan=b.kd_kelurahan
                                                    and a.kd_blok=b.kd_blok
                                                    and a.no_urut =B.NO_URUT
                                                    and a.kd_jns_op=b.kd_jns_op
                                                    and a.thn_pajak_sppt=b.thn_pajak_sppt 
                        left join sppt_oltp c on a.kd_propinsi=c.kd_propinsi
                                                    and a.kd_dati2=c.kd_dati2
                                                    and a.kd_kecamatan=c.kd_kecamatan
                                                    and a.kd_kelurahan=c.kd_kelurahan
                                                    and a.kd_blok=c.kd_blok
                                                    and a.no_urut =c.NO_URUT
                                                    and a.kd_jns_op=c.kd_jns_op
                                                    and a.thn_pajak_sppt=c.thn_pajak_sppt
                        where created_by='$kd_pengguna'")->result();
                        // echo $this->db->last_query();
                        $no_urut = 0;
                        $tr = "";
                        $input = "";
                        $jml_pbb = 0;
                        $btn = '';
                        $tr_double="";
                        $no_urut_double = 1;
                        $jml_pbb_double = 0;
                        $jml_double= 0;
            
            
                        foreach ($res as $a) {
                            if($a->STATUS == '1'){
                                $input .= '<input type="hidden" value="'.$no_urut.'" name="kode_inc[]">';
                                
                                $no_urut++;
            
                                $tr .= '<tr>';
                                $tr .= '<td style="text-align: center">'.$no_urut.'</td>';
                                $tr .= '<td style="text-align: center">'.$a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP.'</td>';
                                $tr .= '<td>'.$a->NM_WP.'</td>';
                                $tr .= '<td style="text-align: center">'.$a->THN_PAJAK_SPPT.'</td>';
                                $tr .= '<td style="text-align: right">'.number_format($a->PBB,0,',','.').'</td>';
                                $tr .= '</tr>';
                                $jml_pbb += $a->PBB;
                                
                                $input .= '<input type="hidden" value="'.$no_urut.'" name="No[]">';
                                $input .= '<input type="hidden" value="'.$a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP.'" name="NOP[]">';
                                $input .= '<input type="hidden" value="'.$a->NM_WP.'" name="NAMA[]">';
                                $input .= '<input type="hidden" value="'.$a->THN_PAJAK_SPPT.'" name="THN_PAJAK[]">';
                                $input .= '<input type="hidden" value="'.$a->PBB.'" name="PBB[]">';
                                $btn = '<button id="subnop" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button>';
                            }else{
                                $jml_double++;
                                $tr_double .= '<tr>';
                                $tr_double .= '<td style="text-align: center">'.$no_urut_double.'</td>';
                                $tr_double .= '<td style="text-align: center">'.$a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP.'</td>';
                                $tr_double .= '<td>'.$a->NM_WP.'</td>';
                                $tr_double .= '<td>'.$a->KETERANGAN.'</td>';
                                $tr_double .= '<td style="text-align: right">'.number_format($a->PBB,0,',','.').'</td>';
                                $tr_double .= '</tr>';
                                $jml_pbb_double += $a->PBB;
                                $no_urut_double++;
                            }
                        }
            
                        $preview = '<form  id="submitdafnom" role="form" method="post" action="'.base_url('dafnom/submitdafnom').'" enctype="multipart/form-data"><div class="box box-primary">'.
                                    '<div class="box-header with-border">'.
                                    '<div class="box-title">'.
                                    '<input type="hidden" value="'.$no_urut.'" name="jml_nop">'.
                                    '<span style="float: left; font-size: 11pt"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'.$no_urut.'</span></b></span><br>'.
                                    '<span style="float: left; font-size: 11pt"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '.number_format($jml_pbb,0,',','.').'</span></b></span>'.
                                    '</div>'.
                                    '<div class="pull-right">'.$btn.'</div></div>'.
                                    '<div class="box-body"><div class="table-responsive">'.$input.
                                    '<table class="table table-bordered" id="table">'.
                                    '<thead>'.
                                    '<tr>'.
                                    '<th width="3%" style="text-align: center">No.</th>'.
                                    '<th style="text-align: center">NOP</th>'.
                                    '<th style="text-align: center">NAMA</th>'.
                                    '<th style="text-align: center">THN PAJAK</th>'.
                                    '<th style="text-align: center">PBB</th>'.
                                    '</tr>'.
                                    '</thead>'.
                                    '<tbody >'.
                                    $tr.
                                    '</tbody>'.
                                    '<tfoot><th colspan="4" style="text-align: center">Total</th><td style="text-align: right">'.number_format($jml_pbb,0,',','.').'</td></tfoot>'.
                                    '</table>'.
                                    '</div>'.
                                    '</div></div></form>';
            
                        $data_double = '<div class="box box-danger"><div class="box-header with-border">'.
                                    '<div class="box-title" width="100%">'.
                                    '<div class="col-md-12"><span style="float: left; font-size: 12pt"><b>Data Tidak Valid Untuk Masuk Dafnom</b></span></div><hr>'.
                                    '<span style="float: left; font-size: 11pt"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'.$jml_double.'</span></b></span><br>'.
                                    '<span style="float: left; font-size: 11pt"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '.number_format($jml_pbb_double,0,',','.').'</span></b></span>'.
                                    '</div>'.
                                    '</div>'.
                                    '<div class="box-body"><div class="table-responsive">'.
                                    '<table class="table table-bordered" id="table2">'.
                                    '<thead>'.
                                    '<tr>'.
                                    '<th width="3%" style="text-align: center">No.</th>'.
                                    '<th style="text-align: center">NOP</th>'.
                                    '<th style="text-align: center">NAMA</th>'.
                                    '<th style="text-align: center">KETERANGAN</th>'.
                                    '<th style="text-align: center">PBB</th>'.
                                    '</tr>'.
                                    '</thead>'.
                                    '<tbody >'.
                                    $tr_double.
                                    '</tbody>'.
                                    '<tfoot><th colspan="4" style="text-align: center">Total</th><td style="text-align: right">'.number_format($jml_pbb_double,0,',','.').'</td></tfoot>'.
                                    '</table>'.
                                    '</div>'.
                                    '</div></div>';
            
                        $data_json = [
                            'status' => 1,
                            'data' => 'sukses',
                            'preview' => $preview,
                            'data_double' => $data_double
                        ];
                    }
                }
            }
        } catch (Exception $e) {

            // echo $e->getMessage();
            $pesan = $e->getMessage();
            $type = 'error';
            $data_json = [
                'status' => 0,
                'data' => $pesan
            ];
            // exit;
        }
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data_json);
    }

    function submitdafnom()
    {
        $kd_pengguna = $_SESSION['pbb_ku'];
        $msg = "";
        //mencari max no_urut
        $this->db->trans_start();
        $query = $this->db->query("SELECT MAX(NO_URUT) as KODEX FROM DATA_BILLING")->row();
        $kode = $query->KODEX;
        $kode++;

        //NOP
        $nop_ubah = str_replace("-", ".", $this->input->post('NOP')[0]);
        $nop_cari = explode(".", $nop_ubah);
        $kd_propinsi = '35';
        $kd_dati2= '07';
        $kd_kecamatan =  $nop_cari[2];
        $kd_kelurahan =  $nop_cari[3];
        $kd_blok = '999';
        $no_urut = sprintf("%04s", $kode);
        $kd_jns_op = '1';
        $nop = $kd_propinsi.$kd_dati2.$kd_kecamatan.$kd_kelurahan.$kd_blok.$no_urut.$kd_jns_op;

        //cek data dafnom dari file excel dengan parameter kecamatan apakah termasuk kedalam upt bersangkutan.
        $row_upt = $this->db->query("SELECT * FROM P_UPT_USER WHERE KODE_PENGGUNA='$kd_pengguna'")->row();
        $kode_upt = trim($row_upt->KODE_UPT);
        $kec_upt = $this->db->query("SELECT * FROM P_UPT_KECAMATAN WHERE KODE_UPT='$kode_upt'")->result();
        $status_upt = "0";
        foreach ($kec_upt as $x) {
            if($x->KODE_KECAMATAN == $kd_kecamatan){
                $status_upt = "1";
            }

            // echo $x->KODE_KECAMATAN;
        }

        // dd($kec_upt);

        if($status_upt == "1"){
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 30000);
            $thn_pajak =  $this->input->post('THN_PAJAK')[0];
            //mencari nama kec, kel
            $kec = $this->db->query("SELECT * FROM REF_KECAMATAN WHERE KD_KECAMATAN='$kd_kecamatan'")->row();
            $kel = $this->db->query("SELECT * FROM REF_KELURAHAN WHERE KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan'")->row();
            $nm_kec = $kec->NM_KECAMATAN;
            $nm_kel = $kel->NM_KELURAHAN;
    
            //mencari jumlah nop yang termasuk dalam dafnom
            $jml_nop = $this->input->post('jml_nop');
            
            $nama_wp = 'Billing Kolektif UPT (DS. '.$nm_kel.', KEC. '.$nm_kec.') - Jumlah NOP : '.$jml_nop;
            
            //setting expired date
            $now = date('d/m/Y H:i:s');
            $s_expired = $this->db->query("SELECT EXPIRED FROM LAYANAN_DEV.CONFIG_EXPIRED WHERE ID='1'")->row();
            $kadaluwarsa = $s_expired->EXPIRED;
    
            $this->db->set('KD_PROPINSI', $kd_propinsi);
            $this->db->set('KD_DATI2', $kd_dati2);
            $this->db->set('KD_KECAMATAN', $kd_kecamatan);
            $this->db->set('KD_KELURAHAN', $kd_kelurahan);
            $this->db->set('KD_BLOK', $kd_blok);
            $this->db->set('NO_URUT', $no_urut);
            $this->db->set('KD_JNS_OP', $kd_jns_op);
            $this->db->set('TAHUN_PAJAK', $thn_pajak);
            $this->db->set('KD_STATUS', '0');
            $this->db->set('NAMA_WP', $nama_wp);
            $this->db->set('NAMA_KELURAHAN', $nm_kel);
            $this->db->set('NAMA_KECAMATAN', $nm_kec);
            $this->db->set('EXPIRED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS') +".$kadaluwarsa, false);
            $this->db->set('CREATED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS')", false);
            $this->db->set('CREATED_BY', $kd_pengguna);
            $this->db->set('KOBIL', $nop);
            $this->db->insert('DATA_BILLING');
    
            //mencari max id dari header billing kolektif
            $max = $this->db->query("SELECT MAX(ID) as IDX FROM DATA_BILLING")->row();
            $data_billing_id = $max->IDX;
            
            // $data = [];
            $no = 0;
            foreach ($this->input->post('kode_inc') as $i => $cell) {
                $str_ubah = str_replace("-", ".", $this->input->post('NOP')[$i]);
                $nopx = explode(".", $str_ubah);
                $thn_pajak = $this->input->post('THN_PAJAK')[$i];

                $q_wp = $this->db->query("SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok,
                no_urut, kd_jns_op, thn_pajak_sppt, pbb_yg_harus_dibayar_sppt pbb,
                nm_wp_sppt, pbb_yg_harus_dibayar_sppt pokok, CASE WHEN thn_pajak_sppt
                >=2003 AND thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 
                (kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
                pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END Denda,
                pbb_yg_harus_dibayar_sppt + CASE WHEN thn_pajak_sppt >= 2003 AND
                thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 (kd_dati2, kd_kecamatan,
                kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
                pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END total
                FROM sppt_oltp WHERE THN_PAJAK_SPPT='$thn_pajak' AND
                KD_KECAMATAN='$nopx[2]' AND KD_KELURAHAN='$nopx[3]' AND KD_BLOK='$nopx[4]' AND NO_URUT='$nopx[5]' AND KD_JNS_OP='$nopx[6]' 
                ORDER BY THN_PAJAK_SPPT DESC");

                if(ENVIRONMENT=='development'){
                    $lunas = $this->db->query("SELECT * FROM SPO_DEV.PEMBAYARAN_SPPT WHERE THN_PAJAK_SPPT='$thn_pajak' AND
                    KD_KECAMATAN='$nopx[2]' AND KD_KELURAHAN='$nopx[3]' AND KD_BLOK='$nopx[4]' AND NO_URUT='$nopx[5]' AND KD_JNS_OP='$nopx[6]'")->result();
                }else{
                    $lunas = $this->db->query("SELECT * FROM SPO.PEMBAYARAN_SPPT WHERE THN_PAJAK_SPPT='$thn_pajak' AND
                    KD_KECAMATAN='$nopx[2]' AND KD_KELURAHAN='$nopx[3]' AND KD_BLOK='$nopx[4]' AND NO_URUT='$nopx[5]' AND KD_JNS_OP='$nopx[6]'")->result();
                }
                
                
                $nama_wp = str_replace("'", "`", $this->input->post('NAMA')[$i]);
                $s_wp = $q_wp->row();

                $q_dafnom = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE THN_PAJAK_SPPT='$thn_pajak' AND
                KD_KECAMATAN='$nopx[2]' AND KD_KELURAHAN='$nopx[3]' AND KD_BLOK='$nopx[4]' AND NO_URUT='$nopx[5]' AND KD_JNS_OP='$nopx[6]'")->result();

                // echo count($lunas);
                
                if( (count($lunas)==1) || (count($q_dafnom)>1) ){
                    // echo 'terbayar'.'<br>';
                }else if((count($lunas)==0) && (count($q_dafnom)==0)){
                    // echo count($lunas).count($q_dafnom).'belum'.'<br>';
                    // dd($s_wp);
                    $this->db->set('DATA_BILLING_ID', $data_billing_id);
                    $this->db->set('KD_PROPINSI', $nopx[0]);
                    $this->db->set('KD_DATI2', $nopx[1]);
                    $this->db->set('KD_KECAMATAN', $nopx[2]);
                    $this->db->set('KD_KELURAHAN', $nopx[3]);
                    $this->db->set('KD_BLOK', $nopx[4]);
                    $this->db->set('NO_URUT', $nopx[5]);
                    $this->db->set('KD_JNS_OP', $nopx[6]);
                    $this->db->set('THN_PAJAK_SPPT', $thn_pajak);
                    $this->db->set('PBB', $this->input->post('PBB')[$i]);
                    $this->db->set('NM_WP', $nama_wp);
                    $this->db->set('POKOK', $s_wp->POKOK);
                    $this->db->set('DENDA', $s_wp->DENDA);
                    $this->db->set('TOTAL', $s_wp->TOTAL);
                    $this->db->set('EXPIRED_AT', "TO_DATE('$now','DD/MM/YYYY HH24:MI:SS') +".$kadaluwarsa, false);
                    $this->db->insert('BILLING_KOLEKTIF');
                    $no++;
                }
                
            }

            // exit();
            if($no == 0){
                $this->db->query("DELETE FROM DATA_BILLING WHERE ID='$data_billing_id'");
            }else{
                $nama_wp = 'Billing Kolektif UPT (DS. '.$nm_kel.', KEC. '.$nm_kec.') - Jumlah NOP : '.$no;
    
                $this->db->query("UPDATE DATA_BILLING SET NAMA_WP='$nama_wp' WHERE ID='$data_billing_id'");
            }

            $this->db->query("commit");

            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                $this->db->trans_commit();
                $msg = "Berhasil di proses";
            } else {
                $this->db->trans_rollback();
                $msg = "Gagal di proses";
            }
            $return=$msg;
            //$this->session->set_flashdata('notif', $msg);
            //redirect('dafnom/index');
        }else{
            $msg = '0';
            $return=$msg;
            //$this->session->set_flashdata('notif', $msg);
            //redirect('dafnom/import_excel');
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($return)); 
        
    }

    function cek_dafnom(){
        
        $kd_pengguna = $_SESSION['pbb_ku'];
        $thn_pajak = $_POST['thn_pajak'];
        $kec = $_POST['kec'];
        $kel = $_POST['kel'];
        $nominal = $_POST['nominal'];
        $nop_hapus = $_POST['nop_hapus'];
        $blok = $_POST['blok'];
        $wh="";
        if(trim($blok)!=""){
            $wh = "AND KD_BLOK='$blok'";
        }

        $this->db->trans_start();
        $del_temp = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE NOP='$nop_hapus'");
        $this->db->query("commit");
        // if($del_temp){
        //     $data['pesan'] = 'terhapus = '.$nop_hapus;
        // }else{
        //     $data['pesan'] = 'error';
        // }
        // if ($this->input->is_ajax_request()) {
        //     $this->output->enable_profiler(false);
        // }
        // header('Content-Type: application/json');
        // echo json_encode($data);

        // exit();

        $list_nop = $this->db->query("SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok,
            no_urut, kd_jns_op, thn_pajak_sppt, pbb_yg_harus_dibayar_sppt pbb,
            nm_wp_sppt, pbb_yg_harus_dibayar_sppt pokok, CASE WHEN thn_pajak_sppt
            >=2003 AND thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 
            (kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END Denda,
            pbb_yg_harus_dibayar_sppt + CASE WHEN thn_pajak_sppt >= 2003 AND
            thn_pajak_sppt <= 2020 THEN 0 ELSE get_denda@to17 (kd_dati2, kd_kecamatan,
            kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) END total
            FROM sppt_oltp WHERE status_pembayaran_sppt=0 AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' AND pbb_yg_harus_dibayar_sppt <= 500000 $wh 
            and  (kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op) not in 
                (SELECT kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
            KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel')
            ORDER BY nm_wp_sppt ASC")->result();

        // $data['cek'] = $list_nop;

        // if ($this->input->is_ajax_request()) {
        //     $this->output->enable_profiler(false);
        // }
        // header('Content-Type: application/json');
        // echo json_encode($data);
        // exit();

        // $datax = "";
        // $no = 1;
        // $cek_total = 0;
        // $total = 0;

        // $batas_nominal = str_replace(',','',$nominal);
        // foreach ($list_nop as $a) {
        //     $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
        //     $nopx = str_replace('.','',$nop);

        //     $cek_total += $a->TOTAL;
        //     if($cek_total <= $batas_nominal){

        //         //mencari data apakah NOP sudah ada pada BILLING_KOLEKTIF_TEMP
        //         $res_temp = $this->db->query("SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND NOP='$nopx'")->result();
                
        //         if(count($res_temp) == 0){
        //             $datax .= "
        //             <tr>
        //                 <th style='text-align: center' id='nomer'>".$no++."</th>
        //                 <td><input type='checkbox' id='cek_satu[]' name='cek_satu[]' class='cek_satu' value='".$nop.'|'.str_replace("'", "`",$a->NM_WP_SPPT).'|'.$a->POKOK.'|'.$a->DENDA.'|'.$a->TOTAL.'|'.$a->THN_PAJAK_SPPT.'|'.$a->PBB.'|'.$nominal."'></td>
        //                 <td>".$nop."</td>
        //                 <td style='text-align: left'>".$a->NM_WP_SPPT."</td>
        //                 <td style='text-align: right' id='jml'>".number_format($a->TOTAL,0,',','.')."</td>
        //             </tr>";
                    
        //             $total += $a->TOTAL;
        //         }
        //     }
            
        // }

        // $data['list_nop'] = "
        // <span style='float: left'><b><i>Jumlah NOP</i> &emsp;&emsp;: <span id='akumulasi_nop'>0</span></b></span><br>
        // <span style='float: left'><b><i>Akumulasi</i>&emsp;&emsp;&emsp;: Rp. <span id='akumulasi'>0</span></b></span><br>
        // <br>
        // <table id='tblNOP' class='table table-bordered table-striped'>
        // <thead>
        //     <tr>
        //         <th style='text-align: center'>No</th>
        //         <th style='text-align: center'><input type='checkbox' id='cek_all' name='cek_all' class='cek_all' value='0' ></th>
        //         <th style='text-align: center'>NOP</th>
        //         <th style='text-align: center'>Nama WP</th>
        //         <th style='text-align: center'>Nominal</th>
        //     </tr>
        // </thead>
        // <tbody id='tbody_nop'>
        // ".$datax."
        // </tbody>
        // <tfoot>
        // <tr>
        //     <th colspan='4' style='text-align: center'>Total</th>
        //     <th style='text-align: right'><span id='total'>".number_format($total,0,',','.')."</span><input type='hidden' id='total_nop' value='".($no-1)."'></th>
        // </tr>
        // </tfoot>
        // </table>
        // ";

        // $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF_TEMP WHERE CREATED_BY='$kd_pengguna' AND THN_PAJAK_SPPT='$thn_pajak' AND
        // KD_KECAMATAN='$kec' AND KD_KELURAHAN='$kel' $wh ORDER BY NM_WP ASC")->result();

        // $datay = "";
        // $no = 1;
        // $total = 0;
        // if(count($res)>0){
        //     foreach ($res as $a) {
        //         $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
        //         $nopx = $a->KD_PROPINSI.$a->KD_DATI2.$a->KD_KECAMATAN.$a->KD_KELURAHAN.$a->KD_BLOK.$a->NO_URUT.$a->KD_JNS_OP;
        //         $datay .= "
        //             <tr id=''>
        //                 <th style='text-align: center'>".$no++."</th>
        //                 <td>".$nop."</td>
        //                 <td style='text-align: left'>".$a->NM_WP."</td>
        //                 <td style='text-align: right'>".number_format($a->TOTAL,0,',','.')."</td>
        //                 <td style='text-align: center'><button data-nop='".$nopx."' class='hapus_nop btn btn-danger btn-xs'><i class='fa fa-trash'></i></button></td>
        //             </tr>";
        //         $total += $a->TOTAL;
                    
        //     }
    
        //     $data['list_dafnom'] = "
        //         <span style='float: left'><b><i>Jumlah NOP</i>&emsp;&emsp;: <span id='nop_terpilih'>".($no-1)."</span></b></span><br>
        //         <span style='float: left'><b><i>Jumlah PBB</i>&emsp;&emsp;: <span id='jml_pbb'>Rp. ".number_format($total,0,',','.')."</span></b></span><br><br>
        //         <table class='table table-bordered table-striped'>
        //         <thead>
        //             <tr>
        //                 <th style='text-align: center'>No</th>
        //                 <th style='text-align: center'>NOP</th>
        //                 <th style='text-align: center'>Nama WP</th>
        //                 <th style='text-align: center'>Nominal</th>
        //                 <th style='text-align: center'>Aksi</th>
        //             </tr>
        //         </thead>
        //         <tbody>
        //         ".$datay."
        //         </tbody>
        //         <tfoot>
        //         <tr>
        //             <th colspan='3' style='text-align: center'>Total</th>
        //             <th style='text-align: right'>".number_format($total,0,',','.')."</th>
        //         </tr>
        //         </tfoot>
        //         </table>
        //     ";
        // }else{
            $data['list_dafnom'] = '0';
        // }
        
        $this->db->trans_complete();

        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }    
    function hapus_nop_temp(){
        
        // $kd_pengguna = $_SESSION['pbb_ku'];
        // $thn_pajak = $_POST['thn_pajak'];
        // $kec = $_POST['kec'];
        // $kel = $_POST['kel'];
        // $nominal = $_POST['nominal'];
        $nop_hapus = $_POST['nop_hapus'];
        // $blok = $_POST['blok'];
        // $wh="";
        // if(trim($blok)!=""){
        //     $wh = "AND KD_BLOK='$blok'";
        //}
        $this->db->trans_start();
        $del_temp = $this->db->query("DELETE FROM BILLING_KOLEKTIF_TEMP WHERE NOP='$nop_hapus'");
        $this->db->query("commit");
        $data['pesan'] = 'terhapus = '.$nop_hapus;
        $this->db->trans_complete();
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }    
    
    function esppt_ntpd()
    {
        $var = $this->input->get('ntpd');
        $kobil = $this->input->get('kobil');
        $nop = $this->input->get('nop');
        if(ENVIRONMENT=='development'){
            $q_ntpd = $this->db->query("SELECT * FROM SPO_DEV.PEMBAYARAN_SPPT WHERE PENGESAHAN='$var'");
        }else{
            $q_ntpd = $this->db->query("SELECT * FROM SPO.PEMBAYARAN_SPPT WHERE PENGESAHAN='$var'");
        }
        

        if(count($q_ntpd->result()) > 0){
            $row = $q_ntpd->row();
            // $nop = $row->KD_PROPINSI.'.'.$row->KD_DATI2.'.'.$row->KD_KECAMATAN.'.'.$row->KD_KELURAHAN.'.'.$row->KD_BLOK.'.'.$row->NO_URUT.'.'.$row->KD_JNS_OP;
            $exp = explode('.', $nop);
            $kd_propinsi = $exp[0];
            $kd_dati2 = $exp[1];
            $kd_kecamatan = $exp[2];
            $kd_kelurahan = $exp[3];
            $kd_blok = $exp[4];
            $no_urut = $exp[5];
            $kd_jns_op = $exp[6];
            
            $data['cetak'] = true;
            $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE KD_PROPINSI='$kd_propinsi' 
            AND KD_DATI2='$kd_dati2' AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' 
            AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut' AND KD_JNS_OP='$kd_jns_op'")->row();

            $id = $res->DATA_BILLING_ID;
            $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();

            $cari = $this->db->query("SELECT A.JLN_WP_SPPT, B.NM_KECAMATAN, C.NM_KELURAHAN
            FROM sppt_oltp A LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN LEFT JOIN REF_KELURAHAN C ON A.KD_KELURAHAN=C.KD_KELURAHAN
            AND A.KD_KECAMATAN=C.KD_KECAMATAN WHERE A.KD_PROPINSI='$kd_propinsi' 
            AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' AND A.KD_KELURAHAN='$kd_kelurahan' 
            AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'")->row();

            $thn_pajak = $res->THN_PAJAK_SPPT;

            if(ENVIRONMENT=='development'){
                $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
                END BANK FROM SPO_DEV.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();
                
            }else{
                $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
                END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
                AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
                AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();

            }

            // $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
            // END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
            // AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
            // AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='2020'")->row();

            $kode_bank = '';
            if($s_ntpd->KODE_BANK_BAYAR==''){
                $kode_bank = $s_ntpd->BANK;
            }else{
                $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
            }

            $this->load->library('ciqrcode');
            $nqr = 'qr_ntpd/' . $s_ntpd->PENGESAHAN .'_'. $query->KOBIL .'_'. str_replace(".", "", $nop) . '.png';

            @unlink($nqr);

            $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt-ntpd?ntpd=' . encrypt_url($s_ntpd->PENGESAHAN) . '&kobil=' . encrypt_url($query->KOBIL) . '&id=' . encrypt_url($id);
            $params['level'] = 'L';
            $params['size'] = 2;
            $params['savename'] = FCPATH . $nqr;
            $this->ciqrcode->generate($params);

            $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();

            $data['kobil'] = $query->KOBIL;
            $data['kec'] = $query->NAMA_KECAMATAN;
            $data['kel'] = $query->NAMA_KELURAHAN;
            $data['ket'] = $query->KETERANGAN;
            
            $data['ntpd'] = $s_ntpd->PENGESAHAN;
            $data['nop'] = $nop;
            $data['nm_wp'] = $res->NM_WP;
            $data['alamat'] = $cari->JLN_WP_SPPT;
            $data['nm_kec'] = $cari->NM_KECAMATAN;
            $data['nm_kel'] = $cari->NM_KELURAHAN;
            $data['thn_pajak'] = $res->THN_PAJAK_SPPT;
            $data['tgl_bayar'] = date_indo(date('Y-m-d'));
            $data['pokok'] = number_format($res->POKOK,0,',','.');
            $data['denda'] = number_format($res->DENDA,0,',','.');
            $data['total'] = number_format($res->TOTAL,0,',','.');
            $data['terbilang'] = penyebut_cap($res->TOTAL).' Rupiah';
            $data['bank'] = $bank->NAMA_BANK;
            $data['qr'] = $nqr;

            $this->load->view('dafnom/buktipembayaran', $data);
        }
        // else if(count($q_ntpd->result()) > 1){

        //     $data['cetak'] = true;
        //     $res = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id'")->result();

        //     $query = $this->db->query("SELECT * FROM V_DAFNOM WHERE ID='$id'")->row();

        //     $data_array = array();

        //     foreach ($res as $a) {

        //         $kd_propinsi = $a->KD_PROPINSI;
        //         $kd_dati2 = $a->KD_DATI2;
        //         $kd_kecamatan = $a->KD_KECAMATAN;
        //         $kd_kelurahan = $a->KD_KELURAHAN;
        //         $kd_blok = $a->KD_BLOK;
        //         $no_urut = $a->NO_URUT;
        //         $kd_jns_op = $a->KD_JNS_OP;

        //         $nop = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'.'.$no_urut.'.'.$kd_jns_op;

        //         $cari = $this->db->query("SELECT A.JLN_WP_SPPT, B.NM_KECAMATAN, C.NM_KELURAHAN
        //         FROM sppt_oltp A LEFT JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN LEFT JOIN REF_KELURAHAN C ON A.KD_KELURAHAN=C.KD_KELURAHAN
        //         AND A.KD_KECAMATAN=C.KD_KECAMATAN WHERE A.KD_PROPINSI='$kd_propinsi' 
        //         AND A.KD_DATI2='$kd_dati2' AND A.KD_KECAMATAN='$kd_kecamatan' AND A.KD_KELURAHAN='$kd_kelurahan' 
        //         AND A.KD_BLOK='$kd_blok' AND A.NO_URUT='$no_urut' AND A.KD_JNS_OP='$kd_jns_op'")->row();

        //         $thn_pajak = $a->THN_PAJAK_SPPT;

        //         $s_ntpd = $this->db->query("SELECT TO_CHAR(TGL_PEMBAYARAN_SPPT, 'DD-MM-YYYY HH24:MI:SS') AS TGL_BAYAR, PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
        //         END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
        //         AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
        //         AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='$thn_pajak'")->row();

        //         // $s_ntpd = $this->db->query("SELECT PENGESAHAN, KODE_BANK_BAYAR, CASE WHEN KODE_BANK_BAYAR IS NULL THEN 114
        //         // END BANK FROM SPO.PEMBAYARAN_SPPT WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' 
        //         // AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut'
        //         // AND KD_JNS_OP='$kd_jns_op' AND THN_PAJAK_SPPT='2020'")->row();

        //         $kode_bank = '';
        //         if($s_ntpd->KODE_BANK_BAYAR==''){
        //             $kode_bank = $s_ntpd->BANK;
        //         }else{
        //             $kode_bank = $s_ntpd->KODE_BANK_BAYAR;
        //         }

        //         $bank = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK='$kode_bank'")->row();

        //         $this->load->library('ciqrcode');
        //         $nqr = 'qr_ntpd/' . $s_ntpd->PENGESAHAN . $query->KOBIL . $id . '.png';

        //         @unlink($nqr);

        //         $params['data'] = 'http://sipanji.id:8085/pbbv2/e-sppt-ntpd?ntpd=' . encrypt_url($s_ntpd->PENGESAHAN) . '&kobil=' . encrypt_url($query->KOBIL) . '&id=' . encrypt_url($id);
        //         $params['level'] = 'L';
        //         $params['size'] = 2;
        //         $params['savename'] = FCPATH . $nqr;
        //         $this->ciqrcode->generate($params);

        //         $data['kobil'] = $query->KOBIL;
        //         $data['kec'] = $query->NAMA_KECAMATAN;
        //         $data['kel'] = $query->NAMA_KELURAHAN;
        //         $data['ket'] = $query->KETERANGAN;
                
        //         $data['ntpd'] = $s_ntpd->PENGESAHAN;
        //         $data['nop'] = $nop;
        //         $data['nm_wp'] = $a->NM_WP;
        //         $data['alamat'] = $cari->JLN_WP_SPPT;
        //         $data['nm_kec'] = $cari->NM_KECAMATAN;
        //         $data['nm_kel'] = $cari->NM_KELURAHAN;
        //         $data['thn_pajak'] = $a->THN_PAJAK_SPPT;
        //         $data['tgl_bayar'] = date_indo(date('Y-m-d', strtotime($s_ntpd->TGL_BAYAR)));
        //         $data['pokok'] = number_format($a->POKOK,0,',','.');
        //         $data['denda'] = number_format($a->DENDA,0,',','.');
        //         $data['total'] = number_format($a->TOTAL,0,',','.');
        //         $data['terbilang'] = penyebut_cap($a->TOTAL).' Rupiah';
        //         $data['bank'] = $bank->NAMA_BANK;
        //         $data['qr'] = $nqr;
                
        //         array_push($data_array, $data);
        //     }

        //     $result['data'] = $data_array;

        //     $this->load->view('dafnom/buktipembayarandafnom', $result);
        // }
        else{
            $this->session->set_flashdata('notif', 'Data tidak di temukan');
            redirect(base_url() . 'sppt/ceksppt');
        }
        
    }

    function dafnomdelete($id){
        $this->db->where('DATA_BILLING_ID', $id);
        $this->db->delete('BILLING_KOLEKTIF');

        $this->db->where('ID', $id);
        $this->db->delete('DATA_BILLING');
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Data berhasil di proses";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }

        redirect('dafnom/index');
    }

    function cetak_daftar_nop_excel(){
        $getData=$this->input->get(null,true);
        if($getData){
            $this->load->helper('array');
            $kd_pengguna = $_SESSION['pbb_ku'];
            if ($this->input->is_ajax_request()) {
                $this->output->enable_profiler(false);
            }
            $this->load->library('exceldua');
            $title = 'Export_Daftara_NOP_Billing';
            
            $tgl=($this->session->userdata("pbb_kg")=="22")?"TGL_BAYAR":"CREATED_AT";
            $tglH=($this->session->userdata("pbb_kg")=="22")?"TGL BAYAR":"TGL BUAT";
            // $select = array("NO","KOBIL","NOP","NM_WP","NAMA_KECAMATAN","NAMA_KELURAHAN","TAHUN_PAJAK","TOTAL",$tgl,"KD_STATUS");
            $select = array("NO","KOBIL","NOP","NM_WP","NAMA_KECAMATAN","NAMA_KELURAHAN","TAHUN_PAJAK","TOTAL","CREATED_AT","TGL_BAYAR","KD_STATUS");
            // $header = array("NO","KOBIL","NOP","NAMA WAJIB PAJAK","KECAMATAN","KELURAHAN","TAHUN","PBB",$tglH,"STATUS");
            $header = array("NO","KOBIL","NOP","NAMA WAJIB PAJAK","KECAMATAN","KELURAHAN","TAHUN","PBB","TGL BUAT","TGL BAYAR","STATUS");
            if($this->session->userdata("pbb_kg")!="1"&&$this->session->userdata("pbb_kg")!="22"&&$this->session->userdata("pbb_kg")!="0"){
                $this->db->where('CREATED_BY', $kd_pengguna);
            }
            if($getData["thn_pajak"] != 'all'){
                $this->db->where('TAHUN_PAJAK', $getData["thn_pajak"]);
            }
            if($getData["KD_KECAMATAN"] != 'all'){
                $this->db->where('KD_KECAMATAN', $getData["KD_KECAMATAN"]);
            }
            if($getData["KD_KELURAHAN"] != 'all'){
                $this->db->where('KD_KELURAHAN', $getData["KD_KELURAHAN"]);
            }
            if($getData["kobil"]){
                $this->db->like('KOBIL', $getData["kobil"]);
            }
            if($getData["kd_status"]!==""){
                $this->db->where('KD_STATUS', $getData["kd_status"]);
            }
            $tgl_bayar = $getData["tgl_bayar"];
            if($getData["kd_status"]=="1"){
                $this->datatables->where("TGL_BAYAR LIKE '$tgl_bayar%'");
            }
            $getData=$this->db->order_by('TAHUN_PAJAK,NOP,NM_WP',"DESC")
                            ->get('V_DAFNOM_NOP');
            $data=array();
            if($getData->num_rows()>0){
                $no=1;
                $total=0;
                foreach($getData->result_array() as $item){
                    $item["NO"]=$no;
                    $item["KD_STATUS"]=($item["KD_STATUS"]=="1")?"Lunas":"Belum Lunas";
                    $total=$total+$item["TOTAL"];
                    $data[]=elements($select,$item);
                    $no++;
                }
                $data["last"]=array('','','','','','','',$total,'','','');
            }
           $this->exceldua->write($title, $header, $data);
        }
    }
    function cetak_dafnom_excel($id){

        $this->load->library('exceldua');

        $cek = $this->db->query("SELECT KOBIL FROM V_DAFNOM WHERE ID='$id'")->row();
        $title = 'Export_Data_Billing_'.$cek->KOBIL;
        $header = ['NO','NOP', 'NAMA', 'TAHUN PAJAK', 'PBB TERHUTANG'];

        $query = $this->db->query("SELECT * FROM BILLING_KOLEKTIF WHERE DATA_BILLING_ID='$id'
        ORDER BY KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP ASC")->result();
        $arrisi = array();
        $PBB = 0;
        $no = 1;
        foreach ($query as $a) {
            $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
            $PBB += $a->TOTAL;
            // $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
            // $ff = array($no++.'.', $nop, $a->NM_WP, $a->THN_PAJAK_SPPT, number_format($a->TOTAL, 0, '', '.'));
            $ff = array($no++.'.', $nop, $a->NM_WP, $a->THN_PAJAK_SPPT, $a->TOTAL);
            array_push($arrisi, $ff);
        }


        $ffa = array('', '', '', '', $PBB);
        array_push($arrisi, $ffa);
        
        $this->exceldua->write($title, $header, $arrisi);
    }

    function unflag(){
        $data['action_cek'] = 'dafnom/unflag';
        $data['kobil'] = '';

        //default (environtment production) pakai spo
        $tabel = 'SPO.ws_pembayaran_tahun A';   
        $tabel2 = 'SPO.PEMBAYARAN_SPPT A'; 

        //jika di development pakai spo_dev
        if(ENVIRONMENT=='development'){
            $tabel = 'SPO_DEV.ws_pembayaran_tahun A';
            $tabel2 = 'SPO_DEV.PEMBAYARAN_SPPT A'; 
        }


        if(isset($_GET['kobil'])){
            $data['kobil'] = $_GET['kobil'];
            $kobil = $_GET['kobil'];

            //cek apakah termasuk dafnom atau tidak
            $cek = $this->db->query("SELECT COUNT(*) JML_DATA FROM V_DAFNOM WHERE KOBIL='$kobil'")->row();

            if($cek->JML_DATA > 0){
                $query = "SELECT A.NOP, A.TAHUN_PAJAK, A.KODEPENGESAHAN,
                A.TOTAL, TO_CHAR(A.CREATED_AT, 'YYYY-MM-DD HH24:MI:SS') AS TGL_BUAT, 
                B.KETERANGAN AS NAMA_WP 
                FROM (select * from $tabel
                where nop = '$kobil') A LEFT JOIN V_DAFNOM B ON A.NOP=B.KOBIL 
                AND A.TAHUN_PAJAK=B.TAHUN_PAJAK
                WHERE NOP='$kobil' 
                ORDER BY TAHUN_PAJAK DESC";
            }else{
                $query = "SELECT NM_WP_SPPT NAMA_WP,A.KD_PROPINSI||A.KD_DATI2||A.KD_KECAMATAN||A.KD_KELURAHAN||A.KD_BLOK||A.NO_URUT||A.KD_JNS_OP AS NOP,  A.THN_PAJAK_SPPT AS TAHUN_PAJAK,
                A.PENGESAHAN AS KODEPENGESAHAN, A.JML_SPPT_YG_DIBAYAR TOTAL, TO_CHAR(A.TGL_REKAM_BYR_SPPT, 'YYYY-MM-DD HH24:MI:SS') AS TGL_BUAT
                       FROM $tabel2
                       JOIN SPPT B ON A.KD_PROPINSI=B.KD_PROPINSI AND
                       A.KD_DATI2=B.KD_DATI2 AND
                       A.KD_KECAMATAN=B.KD_KECAMATAN AND
                       A.KD_KELURAHAN=B.KD_KELURAHAN AND
                       A.KD_BLOK=B.KD_BLOK AND
                       A.NO_URUT=B.NO_URUT AND
                       A.KD_JNS_OP=B.KD_JNS_OP AND
                       A.THN_PAJAK_SPPT=B.THN_PAJAK_SPPT
                       where A.KD_PROPINSI= SUBSTR('$kobil', 1, 2) and
                  A.KD_DATI2= SUBSTR('$kobil', 3, 2) and
                  A.KD_KECAMATAN= SUBSTR('$kobil', 5, 3) and
                  A.KD_KELURAHAN= SUBSTR('$kobil', 8, 3) and
                  A.KD_BLOK= SUBSTR('$kobil', 11, 3) and
                  A.NO_URUT= SUBSTR('$kobil', 14, 4) and
                  A.KD_JNS_OP= SUBSTR('$kobil', 18, 1) ORDER BY A.THN_PAJAK_SPPT DESC";
            }
            $data['res']=$this->db->query($query)->result();
            
        }
        // dd($data);
        return view('dafnom/unflag', $data);
    }

    function reversal(){
        $pengesahan = $_GET['pengesahan'];
        
        $this->db->query("call spo.proc_unflag('$pengesahan')");
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $kode = 1;
            $msg = "Reversal Telah Berhasil !";
        } else {
            $this->db->trans_rollback();
            $kode = 0;
            $msg = "Data gagal di proses";
        }

        header('Content-Type: application/json');

        $data_json = [
            'status' => $kode,
            'data' => $msg
        ];

        echo json_encode($data_json);
    }

    function realisasi_harian(){
        
        $data['tgl_bayar'] = date('d-m-Y');

        $kd_pengguna = $_SESSION['pbb_ku'];

        if(isset($_GET['btn_cari'])){

            $data['tgl_bayar'] = $_GET['tgl_bayar'];
            
            $tgl= date('d-m-Y', strtotime($_GET['tgl_bayar']));
            
            $data['list_nop'] = $this->db->query("SELECT A.ID,
            A.KOBIL,
            A.KD_KECAMATAN,
            A.KD_KELURAHAN,
            A.NAMA_KECAMATAN,
            A.NAMA_KELURAHAN,
            A.NAMA_WP,
            A.TAHUN_PAJAK,
            A.KD_STATUS,
            A.CREATED_BY,
            TO_CHAR (A.CREATED_AT, 'DD-MM-YYYY HH24:MI:SS') TGL_BUAT,
            TO_CHAR (A.EXPIRED_AT, 'DD-MM-YYYY HH24:MI:SS') TGL_EXPIRED,
            TO_CHAR (A.TGL_BAYAR, 'DD-MM-YYYY HH24:MI:SS') TGL_BAYAR, 
            COUNT(B.DATA_BILLING_ID) JML_NOP,
            SUM(B.TOTAL) PBB
            FROM DATA_BILLING A JOIN BILLING_KOLEKTIF B ON A.ID=B.DATA_BILLING_ID
            WHERE to_char(A.TGL_BAYAR, 'DD-MM-YYYY') = '$tgl'
            GROUP BY A.ID, A.KOBIL,
            A.KD_KECAMATAN,
            A.KD_KELURAHAN,
            A.NAMA_KECAMATAN,
            A.NAMA_KELURAHAN,
            A.NAMA_WP,
            A.TAHUN_PAJAK,
            A.KD_STATUS,
            A.CREATED_BY, A.CREATED_AT, A.EXPIRED_AT, A.TGL_BAYAR")->result();


        }
        return view('dafnom/realisasiharian', $data);
    }

    function cetak_realisasi_harian_excel($tgl){

        $this->load->library('exceldua');

        $title = 'Export_Realisasi_Harian_'.$tgl;
        $header = ['KOBIL','NOP', 'THN PAJAK SPPT', 'NAMA WP', 'POKOK', 'DENDA', 'TOTAL', 'TGL_BAYAR'];

        $query = $this->db->query("SELECT A.*, B.KOBIL, TO_CHAR (B.TGL_BAYAR, 'DD-MM-YYYY HH24:MI:SS') TGL_BAYAR 
        FROM BILLING_KOLEKTIF A 
        LEFT JOIN DATA_BILLING B ON A.DATA_BILLING_ID=B.ID 
        WHERE to_char(B.TGL_BAYAR, 'DD-MM-YYYY') = '$tgl' ORDER BY B.TGL_BAYAR ASC")->result();
        $arrisi = array();
        foreach ($query as $a) {
            $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
            
            $ff = array($a->KOBIL, $nop, $a->THN_PAJAK_SPPT, $a->NM_WP, $a->POKOK, $a->DENDA, $a->TOTAL, $a->TGL_BAYAR);
            array_push($arrisi, $ff);
        }
        
        
        $this->exceldua->write($title, $header, $arrisi);
    }

    function tes(){
        echo ENVIRONMENT;
    }
}
