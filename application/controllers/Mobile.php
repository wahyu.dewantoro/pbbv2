<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 
 */
class Mobile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        $this->load->library('form_validation');
        $this->load->model('Mmobileuser');
        $this->load->model('Mmobilemenu');
        $this->load->library('datatables');
    }

    function index()
    {
        $row = $this->db->query("SELECT * FROM CONFIG_MOBILE WHERE ID=1")->row();

        if ($row) {
            $data = array(
                'button'      => 'Update Status',
                'action'      => site_url('mobile/update_action'),
                'kode_config'   => set_value('kode_config', $row->ID),
                'status'   => set_value('active', $row->ACTIVE),
                'keterangan'   => set_value('keterangan', $row->KETERANGAN),
            );
            
            return view('mobile/notifikasi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mobile'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_config', TRUE));
        } else {
            $data = array(
                'KETERANGAN' => $this->input->post('keterangan', TRUE),
                'ACTIVE' => $this->input->post('active', TRUE),
            );

            $this->db->where("ID", $this->input->post('kode_config', TRUE));
            $query = $this->db->update("CONFIG_MOBILE", $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');

            $base = base_url();
            if ($query) {
                echo "<script>alert('Berhasil Update Status !'); document.location= '" . "$base" . "mobile'</script>";
            } else {
                echo "<script>alert('Gagal Update Status !'); document.location= '" . "$base" . "mobile'</script>";
            }
        }
    }

    public function user()
    {
        return view('mobile/user_list');
    }


    public function json_user()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo $this->Mmobileuser->json();
    }

    function resetPassword($id)
    {

        $this->db->set('PASSWORD', 'pass');
        $this->db->where('ID_INC', $id);
        $this->db->update('MOBILE_USERS');
        $msg = "Password telah di update";
        $url = base_url() . 'pengguna';
        // header("Location:".$url);
        echo ("<script LANGUAGE='JavaScript'>
                        window.alert('$msg');
                        window.location.href='$url';
                        </script>");
    }

    public function delete_user($id)
    {
        $row = $this->Mmobileuser->get_by_id($id);

        if ($row) {
            $this->Mmobileuser->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('mobile/user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mobile/user'));
        }
    }

    public function menu()
    {
        return view('mobile/menu_list');
    }


    public function json_menu()
    {
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo $this->Mmobilemenu->json();
    }

    public function create_menu()
    {
        $data = array(
            'button'      => 'Tambah Menu Mobile',
            'action'      => site_url('mobile/create_action'),
            'id_inc'   => set_value('id_inc'),
            'nama_menu'   => set_value('nama_menu'),
            'parent_menu' => set_value('parent_menu'),
            'active'      => set_value('active'),
            'parent' => $this->db->query("SELECT id_inc,nama_menu FROM mobile_menus WHERE parent='0' ORDER BY id_inc ASC")->result(),
        );
        // $this->template->load('template', 'menu/menu_form', $data);
        return view('mobile/menu_form', $data);
    }

    public function create_action()
    {
        $this->_rulesmenu();

        if ($this->form_validation->run() == FALSE) {
            $this->create_menu();
        } else {
            $data = array(
                'NAMA_MENU' => $this->input->post('nama_menu', TRUE),
                'PARENT' => $this->input->post('parent_menu', TRUE),
                'STATUS' => $this->input->post('active', TRUE),
                
            );

            $this->Mmobilemenu->insert($data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('mobile/menu'));
        }
    }

    public function update_menu($id)
    {
        $row = $this->Mmobilemenu->get_by_id($id);

        if ($row) {
            $data = array(
                'button'      => 'Edit Menu',
                'action'      => site_url('mobile/update_action_menu'),
                'id_inc'   => set_value('id_inc', $row->ID_INC),
                'nama_menu'   => set_value('nama_menu', $row->NAMA_MENU),
                'parent_menu' => set_value('parent_menu', $row->PARENT),
                'active'      => set_value('active', $row->STATUS),
                'parent' => $this->db->query("SELECT id_inc,nama_menu FROM mobile_menus WHERE parent='0' ORDER BY id_inc ASC")->result(),
            );
            
            return view('mobile/menu_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mobile/menu'));
        }
    }

    public function update_action_menu()
    {
        $this->_rulesmenu();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
                'NAMA_MENU' => $this->input->post('nama_menu', TRUE),
                'PARENT' => $this->input->post('parent_menu', TRUE),
                'STATUS' => $this->input->post('active', TRUE),
            );

            $this->Mmobilemenu->update($this->input->post('id_inc', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('mobile/menu'));
        }
    }

    public function delete_menu($id)
    {
        $row = $this->Mmobilemenu->get_by_id($id);

        if ($row) {
            $this->Mmobilemenu->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            $this->db->query("commit");
            redirect(site_url('mobile/menu'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mobile/menu'));
        }
    }


    public function _rules()
    {
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('active', 'active', 'trim|required');

        $this->form_validation->set_rules('kode_config', 'kode_config', 'trim');
        $this->form_validation->set_rules('nama_menu', 'nama menu', 'trim|required');
        $this->form_validation->set_rules('parent_menu', 'parent menu', 'trim|required');
        $this->form_validation->set_rules('active', 'active', 'trim|required');

        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    public function _rulesmenu()
    {
        $this->form_validation->set_rules('nama_menu', 'nama menu', 'trim|required');
        $this->form_validation->set_rules('parent_menu', 'parent menu', 'trim|required');
        $this->form_validation->set_rules('active', 'active', 'trim|required');

        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }
}
