<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

require_once("./vendor/autoload.php");

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
/* 
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder; */
// use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;


class Master extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);

        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function wajibpajak()
    {
        $this->template->load('template', 'master/wajibpajak');
    }

    public function wpjson()
    {
        memory_get_usage();
        if ($this->input->is_ajax_request()) {
            $this->output->enable_profiler(false);
        }
header('Content-Type: application/json');
        echo $this->Mmaster->wpjson();
    }

    function objekpajak()
    {
        $this->template->load('template', 'master/objekpajak');
    }

    function kecamatan()
    {
        $this->template->load('template', 'master/kecamatan');
    }


    function kelurahan()
    {
        $this->template->load('template', 'master/kelurahan');
    }

    function mspelayanan()
    {
        $data['data'] = $this->db->query("SELECT * FROM REF_JNS_PELAYANAN ORDER BY TO_NUMBER(KD_JNS_PELAYANAN) ASC")->result();
        return view('master/mspelayanan', $data);
    }

    function blokirNop()
    {
        // $data['data'] = $this->db->query("select a.*,to_char(date_insert,'dd-mm-YYYY HH:ii:ss') tgl_masuk from sppt_blokir@to17 a order by date_insert desc")->result();
        $data['data'] = $this->db->query("SELECT A.*, TO_CHAR (DATE_INSERT, 'dd-mm-YYYY HH:ii:ss') TGL_MASUK,THN_PELAYANAN,BUNDEL_PELAYANAN,NO_URUT_PELAYANAN
        FROM SPPT_BLOKIR@TO17 A
        LEFT JOIN PST_DETAIL B ON B.KD_PROPINSI_PEMOHON =A.KD_PROPINSI AND
    B.KD_DATI2_PEMOHON =A.KD_DATI2 AND
    B.KD_KECAMATAN_PEMOHON =A.KD_KECAMATAN AND
    B.KD_KELURAHAN_PEMOHON =A.KD_KELURAHAN AND
    B.KD_BLOK_PEMOHON =A.KD_BLOK AND
    B.NO_URUT_PEMOHON =A.NO_URUT AND
    B.KD_JNS_OP_PEMOHON =A.KD_JNS_OP AND
    B.THN_PAJAK_PERMOHONAN=A.THN_PAJAK_SPPT
    ORDER BY DATE_INSERT DESC")->result();
        return view('master/nopBlokir', $data);
    }

    function bukablokir()
    {
        $THN_PAJAK_SPPT = $this->input->get('THN_PAJAK_SPPT');
        $KD_KECAMATAN   = $this->input->get('KD_KECAMATAN');
        $KD_KELURAHAN   = $this->input->get('KD_KELURAHAN');
        $KD_BLOK        = $this->input->get('KD_BLOK');
        $NO_URUT        = $this->input->get('NO_URUT');
        $KD_JNS_OP      = $this->input->get('KD_JNS_OP');


        $this->db->query("call blokir_buka_nop@to17($THN_PAJAK_SPPT,'$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT','$KD_JNS_OP')");

        redirect('master/blokirNop');
    }

    function bukablokirmasal()
    {
        if (count($this->input->post('nop')) > 0) {
            $this->db->trans_start();
            for ($i = 0; $i < count($this->input->post('nop')); $i++) {
                $nop = $this->input->post('nop')[$i];
                $exp = explode('.', $nop);
                $this->db->query("call blokir_buka_nop@to17('" . $exp[7] . "','" . $exp[2] . "','" . $exp[3] . "','" . $exp[4] . "','" . $exp[5] . "','" . $exp[6] . "')");
            }
            $this->db->trans_complete();
        }
        redirect('master/blokirNop');
    }

    function delblokir()
    {

        $THN_PAJAK_SPPT = $this->input->get('THN_PAJAK_SPPT');
        $KD_KECAMATAN   = $this->input->get('KD_KECAMATAN');
        $KD_KELURAHAN   = $this->input->get('KD_KELURAHAN');
        $KD_BLOK        = $this->input->get('KD_BLOK');
        $NO_URUT        = $this->input->get('NO_URUT');
        $KD_JNS_OP      = $this->input->get('KD_JNS_OP');


        $this->db->query("call blokir_nop_delete@to17($THN_PAJAK_SPPT,'$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT','$KD_JNS_OP')");

        redirect('master/blokirNop');
    }

    function blokirnopsppt()
    {
        $nop = $this->input->post('NOP', true);
        $ss = explode('.', $nop);
        $cc = count($ss);
        $tahun = $this->input->post('TAHUN');
        if ($cc == 7) {
            $this->db->query("call blokir_nop@to17($tahun,'" . $ss[2] . "','" . $ss[3] . "','" . $ss[4] . "','" . $ss[5] . "','" . $ss[6] . "')");
        }
        redirect('master/blokirNop');
    }

    function nomorsk()
    {
        $data['data'] = $this->db->query("SELECT * FROM P_MASTER_SK")->result();
        return view('master/nomorsk', $data);
    }

    function updatenomorsk()
    {
        $get = $this->input->get('p');
        $vv = urldecode(urldecode(urldecode($get)));
        $res = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='$vv'")->row();
        if ($res) {
            $data = array(
                'JENIS' => set_value('JENIS', $res->JENIS),
                'NOMOR' => set_value('NOMOR', $res->NOMOR),
                'NAMA_PEGAWAI' => set_value('NAMA_PEGAWAI', $res->NAMA_PEGAWAI),
                'JABATAN' => set_value('JABATAN', $res->JABATAN),
                'NIP' => set_value('NIP', $res->NIP),
            );

            return view('master/formsk', $data);
        }
    }




    function prosesupdatesk()
    {
        $JENIS = $this->input->post('JENIS');
        $NOMOR = $this->input->post('NOMOR');
        $NAMA_PEGAWAI = $this->input->post('NAMA_PEGAWAI');
        $JABATAN = $this->input->post('JABATAN');
        $NIP = $this->input->post('NIP');


        $this->db->where('JENIS', $JENIS);
        $this->db->set('NOMOR', $NOMOR);
        $this->db->set('NAMA_PEGAWAI', $NAMA_PEGAWAI);
        $this->db->set('JABATAN', $JABATAN);
        $this->db->set('NIP', $NIP);
        $as = $this->db->update('P_MASTER_SK');
        if ($as) {
            redirect('master/nomorsk');
        } else {
            redirect('master/updatenomorsk?p=' . urlencode(urlencode(urlencode($JENIS))));
        }


        /*$res=$this->input->post();
    echo "<pre>";
    print_r($res);
    echo "</pre>";*/
    }


    function prosesimportNopblokir()
    {
        // error_reporting(E_ALL ^ E_NOTICE);


        // $file = $_FILES['nop_blokir']['tmp_name'];

        $config['upload_path']      = 'blokir/'; //siapkan path untuk upload file
        $config['allowed_types']    = 'xlsx'; //siapkan format file
        $config['file_name']        = 'doc' . time(); //rename file yang diupload

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('nop_blokir')) {
            $file   = $this->upload->data();

            $reader = ReaderEntityFactory::createReaderFromFile($file['full_path']);

            $reader->open($file['full_path']);

            // $data=[];
            foreach ($reader->getSheetIterator() as $sheet) {
                $numRow = 1;

                //siapkan variabel array kosong untuk menampung variabel array data
                $save   = array();
                $win = '';
                //looping pembacaan row dalam sheet
                $baris=0;
                foreach ($sheet->getRowIterator() as $row) {

                    if ($numRow > 1) {
                        //ambil cell
                        $cells = $row->getCells();
                        $win .= " select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||'.'||kd_kelurahan||'.'||kd_blok||'.'||no_urut||'.'||kd_jns_op nop , nm_wp_sppt,thn_pajak_sppt
                        from sppt 
                        where  thn_pajak_sppt='" . $cells[7] . "'
                        and kd_propinsi='" . $cells[0] . "'
                        and kd_dati2='" .$cells[1]. "'
                        and kd_kecamatan ='" . $cells[2] . "'
                        and kd_kelurahan='" . $cells[3] . "'
                        and kd_blok='" . $cells[4] . "'
                        and no_urut='" . $cells[5] . "'
                        and kd_jns_op ='" . $cells[6] . "'
                         union";
                         $baris+=1;
                         
                    }

                    $numRow++;
                }
                $vwin = substr($win, 0, -5);
                $dd = $this->db->query($vwin)->result();

                $array = array(
                    'data' => $dd,
                    'count' => $baris
                );
              
                    return view('master/previewnopBlokir', $array);
                die();
                

            }
        }


 
    }

    function submitblokir()
    {


        // echo "<pre>";
        // print_r($this->input->post());
        //     die();
        $KD_KECAMATAN   = $this->input->post('KD_KECAMATAN');
        $KD_KELURAHAN   = $this->input->post('KD_KELURAHAN');
        $KD_BLOK        = $this->input->post('KD_BLOK');
        $NO_URUT        = $this->input->post('NO_URUT');
        $KD_JNS_OP      = $this->input->post('KD_JNS_OP');
        $THN_PAJAK_SPPT = $this->input->post('THN_PAJAK_SPPT');

        $baris = $this->input->post('count');
        $this->db->trans_start();
        $jum = 0;
        for ($i = 0; $i < count($KD_KECAMATAN); $i++) {


            $rr = $this->db->query("call blokir_nop@to17(" . $THN_PAJAK_SPPT[$i] . ",'" . $KD_KECAMATAN[$i] . "','" . $KD_KELURAHAN[$i] . "','" . $KD_BLOK[$i] . "','" . $NO_URUT[$i] . "','" . $KD_JNS_OP[$i] . "')");
            if ($rr) {
                $jum += 1;
            }
        }
        $this->db->trans_complete();

        echo "<script>alert('data berhasil di proses :'" . $jum . ");</script>";
        redirect('master/blokirNop');
    }
}

/* End of file Group.php */
/* Location: ./application/controllers/Group.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-28 01:25:45 */
