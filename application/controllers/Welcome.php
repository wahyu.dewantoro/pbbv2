<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(false);
		$this->group = $this->session->userdata('pbb_kg');
	}

	function countPengguna()
	{
		$res = $this->db->query("select count(1) jum from p_pengguna")->row();
		return $res->JUM;
	}

	function countPermohonan()
	{
		$res = $this->db->query("select  count(1) jum from pst_permohonan where to_char(tgl_terima_dokumen_wp,'yyyymm')=to_char(sysdate,'yyyymm')")->row();
		return $res->JUM;
	}

	public function index()
	{
		if ($this->group == 1) {
			$data = array(
				'countPengguna' => $this->countPengguna(),
				'countPermohonan' => $this->countPermohonan()
			);

			return view('blankdashboard', $data);
		} else {
			$kg = $this->db->query("select nama_group from p_group where kode_group='" . $this->group . "'")->row();
			$dok = $this->db->query(" select  count(1) jum
                 from PV_DOKUMEN a
                 where  kode_group='" . $this->group . "' and  tahun=to_number(to_char(sysdate,'yyyy'))")->row();
			$rekap = $this->db->query("select nama_group ,count(1) dok
										 from pv_dokumen a
										 join p_group b on a.kode_group=b.kode_group
										 where status!='Selesai' and  tahun=to_number(to_char(sysdate,'yyyy'))
										 group by nama_group
										 order by nama_group desc")->result();
			$data = array(
				'nama_role' => $kg->NAMA_GROUP,
				'jumDok'    => $dok->JUM,
				'rekap'     => $rekap
			);
			$tahun = date('Y');
			$data['thn'] = $tahun;

			return view('blank', $data);
		}
		// $this->load->view('page/master');
	}

	public function ceknik()
	{
		// if ($this->input->is_ajax_request()) {
		$this->output->enable_profiler(false);
		// }
		$nik = $this->input->get('nik');
		$this->load->library('kependudukan');
		$res = $this->kependudukan->validasi($nik);
		echo $res;
	}

	public function test()
	{
		dd(ENVIRONMENT);
	}

	public function ambilrealisasi()
	{

		$kd_pengguna = $this->session->userdata('pbb_ku');
		$wh = "";
		if ($this->group == 13) {
			$cari = $this->db->query("SELECT KODE_KECAMATAN FROM P_UPT_KECAMATAN WHERE KODE_UPT='$kd_pengguna'")->row();
			$kec = $cari->KODE_KECAMATAN;
		} else if ($this->group == 14) {
			$cari = $this->db->query("SELECT KODE_KEC_KEL FROM P_KELURAHAN_USER WHERE KODE_PENGGUNA='$kd_pengguna'")->row();
			$kec = substr($cari->KODE_KEC_KEL, 0, 3);
			$kel = substr($cari->KODE_KEC_KEL, 3, 3);
			$wh = "KD_KELURAHAN='$kel'";
		}

		$tahun = $_POST['tahun'];
		$res = $this->db->query("SELECT A.KD_KECAMATAN, A.KD_KELURAHAN, A.BAKU, A.THN_PAJAK_SPPT, A.REALISASI, A.NM_KELURAHAN, COUNT(B.NM_WP_SPPT) NOP FROM MV_REALISASI_PEMBAYARAN A  
        JOIN (select * from SPPT where THN_PAJAK_SPPT='$tahun'
        AND KD_KECAMATAN='$kec' AND $wh )B ON A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN AND A.THN_PAJAK_SPPT=B.THN_PAJAK_SPPT
        WHERE A.THN_PAJAK_SPPT='$tahun'
        AND A.KD_KECAMATAN='$kec' AND A.".$wh."
        GROUP BY A.KD_KECAMATAN, A.KD_KELURAHAN, A.BAKU, A.THN_PAJAK_SPPT, A.REALISASI, A.NM_KELURAHAN
        ORDER BY A.NM_KELURAHAN ASC")->result();

		$data = "";
		$no = 1;
		foreach ($res as $a) {
			// $data .= "<tr>
			// <td>".$no++.".</td>
			// <td>".$a->NM_KELURAHAN."</td>
			// <td align='right'>".number_format($a->JML_DATA ,0,',','.')."</td>
			// <td><button class='btn btn-xs btn-info' id='btn_realisasi' data-kelurahan='".$a->KD_KELURAHAN."'><i class='fa fa-search'></i></button></td>
			// </tr>";
			$data .= "<tr>
			<td>" . $no++ . ".</td>
			<td>" . $a->NM_KELURAHAN . "</td>
			<td align='right'>" . number_format($a->NOP, 0, ',', '.') . "</td>
			<td align='right'>" . number_format($a->BAKU, 0, ',', '.') . "</td>
			<td align='right'>" . number_format($a->REALISASI, 0, ',', '.') . "</td>
			<td align='right'>" . round(($a->REALISASI / $a->BAKU) * 100, 2) . " %</td>
			</tr>";
		}
		echo $data;
	}

	function cek(){
		$total = 144;
		$hasil = $total/3;
		
		if(is_double($hasil)){
			echo $hasil.'='.floor($hasil);
		}else if(is_int($hasil)){
			echo $hasil;
		}
	}
}
