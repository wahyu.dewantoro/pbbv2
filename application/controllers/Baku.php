<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once("./vendor/autoload.php");

use Box\Spout\Common\Entity\Style\Border;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;


class Baku extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
    }

    function ringkasan()
    {
        /* $th = date('Y');

        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        } */



        /*         $rr = '';
        $upt = false;

        if ($this->session->userdata('pbb_kg') == 8) {
            $upt = true;
            $pbb_ku = $this->session->userdata('pbb_ku');
            $rr .= " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN " . $rr . " order by kd_kecamatan asc")->result(); */

        /*  $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " where A.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " and A.KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        
 */

        /* $query = [];
        if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '') {

            // $query=$this->db->query("")

            $query = $this->db->query("select * 
            from ringkasan_baku
            where thn_pajak_sppt=$tahun and kd_kecamatan='$KD_KECAMATAN' and kd_kelurahan='$KD_KELURAHAN' order by buku asc")->result();
        } */

        if ($this->session->userdata('pbb_kg') == 8 || $this->session->userdata('pbb_kg') == 13|| $this->session->userdata('pbb_kg') == 14) {
            $upt = true;
        }
        $this->load->model('Mrealisasi');

        $tahun = $this->input->get('TAHUN') ?? date('Y');

        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $rr = "";
       
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
        }
        $and="";
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN " . $rr . " order by kd_kecamatan asc")->result();
        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("SELECT kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' ".$and." 
                                    order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '') {
            $query = $this->Mrealisasi->baku($tahun, $KD_KECAMATAN, $KD_KELURAHAN);
        }
        $data = array(
            'judul'        => 'Ringkasan Baku Tahun ' . $tahun,
            'data'         => $query ?? [],
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'tahun'        => $tahun,
            'upt' => $upt ?? ''
        );

        return view('baku/ringkasanbaku', $data);
    }
    function dataDHKP12()
    {
        $th = date('Y');
        $tahun=(isset($_GET['TAHUN'])) ?$_GET['TAHUN']:date('Y');

        $rr = '';
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $rr="WHERE KD_KECAMATAN ='".$KD_KECAMATAN."'";
        }
        if ($this->session->userdata('pbb_kg') == 8) {
            $pbb_ku = $this->session->userdata('pbb_ku');
            $rr .= " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        $and="";
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN " . $rr . " order by kd_kecamatan asc")->result();
        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' ".$and." 
                                    order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
            
        }
        $andkel=($KD_KELURAHAN)?" AND KD_KELURAHAN='".$KD_KELURAHAN."'":"";
        $rr =" WHERE thn_pajak_sppt='".$this->_getDateM("Y")."'";
        $str="SELECT *  FROM MV_REALISASI_SATU_DUAKELURAHAN ". $rr." AND KD_KECAMATAN = '".$KD_KECAMATAN."' ".$andkel;
        $query = $this->db->query($str)->result();
        $data = array(
            'judul'        => 'DHKP buku 1,2 Tahun ' . $tahun,
            'data'         => $query,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'tahun'        => $tahun
        );

        return view('baku/viewdataDHKP_TP', $data);
    }
    function dataDHKP()
    {
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $rr = '';
        $upt = false;
        if ($this->session->userdata('pbb_kg') == 8) {
            $upt = true;
            $pbb_ku = $this->session->userdata('pbb_ku');
            $rr .= " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        $andKel="";
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $andKel=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN " . $rr . " order by kd_kecamatan asc")->result();
        
        
        $and = "";
        if ($KD_KECAMATAN <> '') {
            $and .= " and  substr(nop,7,3)='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $and .= " and substr(nop,11,3)='" . $KD_KELURAHAN . "'";
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' ".$andKel." 
                                    order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if ($and <> '') {
            $query = $this->db->query("select * from dhkp_buku_345 where thn_pajak_sppt=$tahun $and order by nop asc")->result();
        } else {
            $query = [];
        }


        $data = array(
            'judul'        => 'DHKP buku 3,4,5 Tahun ' . $tahun,
            'data'         => $query,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'tahun'        => $tahun,
            'upt' => $upt
        );

        return view('baku/viewdataDHKP', $data);
    }

    function DownloadDhkpDesa()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        if (isset($_GET['tgl1'])) {
            $tgl1       = $_GET['tgl1'];
        } else {
            $tgl1 = date('d-m-Y');
        }

        $exp = explode('-', $tgl1);

        $kec = $this->db->query("SELECT A.*, B.NM_KECAMATAN FROM P_DHKP_KEC_FILE A  JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN WHERE TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$tgl1','DD-MM-YYYY')
            and file_dhkp like '%" . $exp[2] . $exp[1] . $exp[0] . "%'
            ")->result();

        $data = array(
            'judul'        => 'Download DHKP Desa',
            'data'         => $kec,
            'tgl1'         => $tgl1
        );
        return view('baku/viewdownloaddhkpdesa', $data);
    }

    function tes()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 30000);
        $out = shell_exec('cd C:\xampp\htdocs\layanan200\PythonDHKP && myenv\Scripts\activate.bat && python export.py');
        echo $out;
    }
    function dhkpDesa()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $rr = '';
        if ($this->session->userdata('pbb_kg') == 8) {
            $pbb_ku = $this->session->userdata('pbb_ku');
            $rr .= " WHERE KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }
        
        $required="";
        $BUKU = $this->input->get('BUKU', true);
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $required="required";
        }
        $and="";
        if($this->session->userdata("pbb_kg")=="14"){
            $str="select rk.KD_KECAMATAN,rk.KD_KELURAHAN 
                    from P_KELURAHAN_USER pku 
                    LEFT OUTER JOIN REF_KELURAHAN rk 
                        ON CONCAT(rk.KD_KECAMATAN ,rk.KD_KELURAHAN)=pku.KODE_KEC_KEL
                    where KODE_PENGGUNA='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KD_KECAMATAN:$KD_KECAMATAN;
            $KD_KELURAHAN=($get->num_rows()>0)?$get->row()->KD_KELURAHAN:$KD_KELURAHAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
            $and=" AND kd_kelurahan='".$KD_KELURAHAN."' ";
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN  $rr order by kd_kecamatan asc")->result();
        if ($BUKU == '') {
            $BUKU = [];
        }

        if ($KD_KECAMATAN <> '') {
           
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' ". $and."
                                    order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }



        $data = array(
            'judul'        => 'DHKP atau Update BAKU  ',
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'tahun'        => $tahun,
            'buku'         => array('Buku I', 'Buku II', 'Buku III', 'Buku IV', 'Buku V'),
            'lb' => $BUKU,
            'required'=>$required,
            "direct_load_ajx"=>($this->input->get(null,true))?true:false,
            'sessionCheck'=>$this->session->userdata("pbb_kg")
        );
        return view('baku/viewdhkpdesa', $data);
    }

    function jsondatabaku()
    {
        ini_set('memory_limit', '2G');


        $this->load->library('datatables');
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN');
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN');
        $tahun = $this->input->post('tahun');
        $jenis_buku = $this->input->post('jenis_buku');
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
        }

        $exb = explode(',', $jenis_buku);
        $jbb = '';
        foreach ($exb as $rf) {
            $jbb .= "'" . $rf . "',";
        }
        $jbb = substr($jbb, 0, -1);
        $where = "THN_PAJAK_SPPT = '$tahun'";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if ($jenis_buku <> '') {
            $where .= " AND BUKU IN(" . $jbb . ")";
        }

        $this->datatables->select('THN_PAJAK_SPPT, KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK,  NO_URUT, KD_JNS_OP, NOP,  ALAMAT_OP, RWRT_OP, LUAS_BUMI_SPPT,    LUAS_BNG_SPPT, NM_WP_SPPT, ALAMAT_WP, RWRT_WP, PBB,PBB_AKHIR, TGL_TERBIT_SPPT,   TGL_PEMBAYARAN_SPPT, BUKU, KELURAHAN_WP_SPPT,KOTA_WP_SPPT, KET');

        switch ($tahun) {
            case '2020':
                # code...
                $mv = 'MV_DHKP_DESA_' . $tahun;
                break;
            default:
                # code...
                $mv = 'MV_DHKP_DESA_TAHUN';
                break;
        }

        $this->datatables->from($mv);
        $this->datatables->where($where, '', false);
        return print_r($this->datatables->generate());
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }


    function cetakRingkasanbaku()
    {
        /*  $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);



        $where = "";
        if ($this->session->userdata('pbb_kg') == 8) {
            $pbb_ku = $this->session->userdata('pbb_ku');
            $where = "where KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }

        if ($KD_KECAMATAN <> '') {
            $where .= "where KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " and KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        $sql = $this->db->query("select * from mv_ringkasan_baku@to17 " . $where)->result(); */

        $this->load->model('Mrealisasi');

        $tahun = $this->input->get('TAHUN') ?? date('Y');

        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$this->session->userdata("pbb_ku")."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:$KD_KECAMATAN;
            $rr = "where KD_KECAMATAN='".$KD_KECAMATAN."'";
        }
        /* $rr = "";
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN " . $rr . " order by kd_kecamatan asc")->result();
        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("SELECT kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN'  order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        } */

        // if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '') {
        $sql = $this->Mrealisasi->baku($tahun, $KD_KECAMATAN, $KD_KELURAHAN);
        // }

        $border = (new BorderBuilder())
            ->setBorderBottom(Color::GREEN, Border::WIDTH_THIN, Border::STYLE_DASHED)
            ->build();

        $style = (new StyleBuilder())
            ->setBorder($border)
            ->build();

        $writer = WriterEntityFactory::createXLSXWriter();
        $filePath = "tmp/ringkasanbaku" . time() . ".xlsx";
        $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $style = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(11)
            ->setFontColor(Color::WHITE)
            ->setShouldWrapText(true)
            ->setBackgroundColor(Color::GREEN)
            ->build();

        $header = WriterEntityFactory::createRowFromArray(['Kecamatan', 'Kelurahan', 'Buku', 'Baku', 'SPPT', 'Realisasi', 'SPPT Realisasi', 'Piutang', 'SPPt Piutang'], $style);

        $arrisi = array();
        $SPPT_BAKU        = 0;
        $JUMLAH_BAKU      = 0;
        $SPPT_REALISASI   = 0;
        $JUMLAH_REALISASI = 0;
        $SPPT_PIUTANG     = 0;
        $JUMLAH_PIUTANG   = 0;
        foreach ($sql as $rk) {
            $SPPT_BAKU += $rk->SPPT;
            $JUMLAH_BAKU += $rk->BAKU;
            $SPPT_REALISASI += $rk->SPPT_REALISASI;
            $JUMLAH_REALISASI += $rk->REALISASI;
            $SPPT_PIUTANG += $rk->SPPT_PIUTANG;
            $JUMLAH_PIUTANG += $rk->PIUTANG;
            $ff = array(
                WriterEntityFactory::createCell($rk->KD_KECAMATAN . ' - ' . $rk->NM_KECAMATAN),
                WriterEntityFactory::createCell($rk->KD_KELURAHAN . ' - ' . $rk->NM_KELURAHAN),
                WriterEntityFactory::createCell($rk->BUKU),
                WriterEntityFactory::createCell((int) $rk->SPPT),
                WriterEntityFactory::createCell((int) $rk->BAKU),
                WriterEntityFactory::createCell((int) $rk->SPPT_REALISASI),
                WriterEntityFactory::createCell((int) $rk->REALISASI),
                WriterEntityFactory::createCell((int) $rk->SPPT_PIUTANG),
                WriterEntityFactory::createCell((int) $rk->PIUTANG)
            );
            // array_push($arrisi, $ff);
            $arrisi[] =  WriterEntityFactory::createRow($ff);
        }
        $ffa = array(
            WriterEntityFactory::createCell(null),
            WriterEntityFactory::createCell(null),
            WriterEntityFactory::createCell(null),
            WriterEntityFactory::createCell(number_format($SPPT_BAKU, 0, '', '.')),
            WriterEntityFactory::createCell(number_format($JUMLAH_BAKU, 0, '', '.')),
            WriterEntityFactory::createCell(number_format($SPPT_REALISASI, 0, '', '.')),
            WriterEntityFactory::createCell(number_format($JUMLAH_REALISASI, 0, '', '.')),
            WriterEntityFactory::createCell(number_format($SPPT_PIUTANG, 0, '', '.')),
            WriterEntityFactory::createCell(number_format($JUMLAH_PIUTANG, 0, '', '.'))
        );
        array_push($arrisi,  WriterEntityFactory::createRow($ffa));
        $writer->getCurrentSheet()->setName('Ringkasan Baku');
        $writer->addRow($header)->addRows($arrisi);
        /* $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($arrisi); */
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
        die();
    }
    private function _getDateM($str){
		date_default_timezone_set("Asia/Bangkok");
		return date($str);	
	}
    function cetakBakuDHKP12(){
        $tag_html="";
        $pbb_ku = $this->session->userdata('pbb_ku');
        $rr =" WHERE thn_pajak_sppt='".$this->_getDateM("Y")."'";
        if($this->session->userdata("pbb_kg")=="13"){
            $str="select KODE_KECAMATAN from P_UPT_KECAMATAN where KODE_UPT='".$pbb_ku."'";
            $get=$this->db->query($str);
            $KD_KECAMATAN=($get->num_rows()>0)?$get->row()->KODE_KECAMATAN:'';
            $rr="AND  KD_KECAMATAN ='".$KD_KECAMATAN."'";
        }elseif ($this->session->userdata('pbb_kg') == 8) {
            $rr = " AND KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }

        $str="SELECT *  FROM MV_REALISASI_SATU_DUAKELURAHAN ".$rr;
        $data= $this->db->query($str)->result();
        foreach ($data as $rk) {
            $tag_html = '<tr>
                                    <td>' . $rk->NM_KELURAHAN . '</td>
                                    <td align="center">' . $rk->JUM_SPPT . '</td>
                                    <td align="right">' . number_format($rk->BAKU, '0', '', '.') . '</td>
                                    <td align="center">' . $rk->SPPT_REALISASI . '</td>
                                    <td align="right">' . number_format($rk->POKOK_REALISASI, '0', '', '.') . '</td>
                                    <td align="right">' . number_format($rk->DENDA_REALISASI, '0', '', '.') . '</td>
                                    <td align="right">' . number_format($rk->JUMLAH_REALISASI, '0', '', '.') . '</td>
                                    <td>' . number_format($rk->PERSEN_REALISASI, '2', ',', '.') . '</td>
                                    <td align="center">' . $rk->SPPT_SISA . '</td>
                                    <td align="right">' . number_format($rk->POKOK_SISA, '0', '', '.') . '</td>
                            </tr>';
        }
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=laporan_realisasi.xls");
        $html = ' <table width="100%" border="1">
                    <thead>
                        <tr>
                            <th rowspan="2">Kecamatan</th>
                            <th rowspan="2">Jumalah SPPT</th>
                            <th  rowspan="2">Baku</th>
                            <th colspan="4">Realisasi</th>
                            <th rowspan="2">%</th>
                            <th colspan="2">Sisa</th>
                        </tr>
                        <tr>
                            
                            <th>Sppt</th>
                            <th>Pokok</th>
                            <th>Denda</th>
                            <th>Jumlah</th>
                            <th>SPPT</th>
                            <th>Pokok</th>
                            
                        </tr>
                    </thead>
                    <tbody>'.$tag_html.'</tbody>
                </table>';
        echo $html;
    }
    function cetakBakuDHKP()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $and = "";

        $rr = '';
        if ($this->session->userdata('pbb_kg') == 8) {
            $pbb_ku = $this->session->userdata('pbb_ku');
            $rr .= " and substr(nop,7,3) in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }

    $title='DHKP 345 ';
        if ($KD_KECAMATAN <> '') {
            $and .= " and  substr(nop,7,3)='" . $KD_KECAMATAN . "'";
            $title.=" kec ".$KD_KECAMATAN;
        }
        if ($KD_KELURAHAN <> '') {
            $and .= " and substr(nop,11,3)='" . $KD_KELURAHAN . "'";
            $title.=" kel ".$KD_KELURAHAN;
        }

        $query = $this->db->query("select nop,alamat_op,RWRT_OP,get_kecamatan(substr(nop,7,3)) nm_kec,LUAS_BUMI_SPPT, LUAS_BNG_SPPT, NM_WP_SPPT, ALAMAT_WP, RWRT_WP, KELURAHAN_WP_SPPT, KOTA_WP_SPPT, PBB, TGL_TERBIT_SPPT, case when TGL_PEMBAYARAN_SPPT is null then 'belum' else 'lunas' end lunas, TGL_PEMBAYARAN_SPPT from dhkp_buku_345 a where thn_pajak_sppt=$tahun $and order by nop asc")->result_array();
        $header = ['NOP', 'Alamat OP', 'RW / RT OP', 'Kecamatan', 'Luas Bumi', 'Luas Bangunan', 'Nama WP', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB', 'Terbit SPPT', 'Lunas', 'Pembayaran SPPT'];
        $this->load->library('exceldua');
        $this->exceldua->write($title, $header,$query);

        die();

        $writer = WriterFactory::create(Type::XLSX);

        $writer->openToBrowser('DHKPbuku-345.xlsx');
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();
        $first = 1;
        $query = $this->db->query("select a.*,get_kecamatan(substr(nop,7,3)) nm_kec from dhkp_buku_345 a where thn_pajak_sppt=$tahun $and order by nop asc")->result();
        $arrisi = array();
        $PBB = 0;
        foreach ($query as $rk) {
            $PBB += $rk->PBB;
            $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
            $ff = array($rk->NOP, $rk->ALAMAT_OP, $rk->RWRT_OP, $rk->NM_KEC, $rk->LUAS_BUMI_SPPT, $rk->LUAS_BNG_SPPT, $rk->NM_WP_SPPT, $rk->ALAMAT_WP, $rk->RWRT_WP, $rk->KELURAHAN_WP_SPPT, $rk->KOTA_WP_SPPT, $rk->PBB, $rk->TGL_TERBIT_SPPT, $lunas, $rk->TGL_PEMBAYARAN_SPPT);
            array_push($arrisi, $ff);
        }


        $ffa = array('', '', '', '', '', '', '', '', '', '', '', number_format($PBB, 0, '', '.'), '', '', '');
        array_push($arrisi, $ffa);
        $writer->getCurrentSheet()->setName("Buku 3,4,5");
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($arrisi);
        $writer->close();
    }

    function cetakBakuDHKPdesa()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        if (!is_dir('tmp')) {
            mkdir('./tmp', 0777, TRUE);
        }

        // query ne
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }

        switch ($tahun) {
            case '2020':
                # code...
                $mv = 'MV_DHKP_DESA_' . $tahun;
                break;
            default:
                # code...
                $mv = 'MV_DHKP_DESA_TAHUN';
                break;
        }

        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $BUKU         = $this->input->get('BUKU', true);
        $and = '';
        if ($this->session->userdata('pbb_kg') == 8) {
            $pbb_ku = $this->session->userdata('pbb_ku');
            $and .= "  KD_KECAMATAN in (SELECT KODE_KECAMATAN 
            FROM P_UPT_KECAMATAN A
            JOIN P_UPT_USER B ON TRIM(A.KODE_UPT)=TRIM(B.KODE_UPT)
            WHERE KODE_PENGGUNA='" . $pbb_ku . "' )";
        }

        $and .= " and kd_kecamatan='$KD_KECAMATAN'";
        if ($KD_KELURAHAN <> '') {
            $and .= "and kd_kelurahan='$KD_KELURAHAN'";
        }

        $filter_buku = "";
        if ($BUKU <> '') {
            $str_buku = explode(",", $BUKU);
            $w_buku = "";
            if (count($str_buku) == 0) {
            } else {
                if (isset($_GET['CARI'])) {
                    for ($i = 0; $i < count($str_buku); $i++) {
                        $w_buku .= "" . $str_buku[$i] . ",";
                    }
                } else {
                    for ($i = 0; $i < count($str_buku); $i++) {
                        $w_buku .= "'" . $str_buku[$i] . "',";
                    }
                }
                $r_buku = rtrim($w_buku, ',');
                $and .= " and buku in(" . $r_buku . ")";
                $filter_buku = " and buku in(" . $r_buku . ")";
            }
        }
        $header = ['NOP', 'Alamat OP', 'RW / RT OP', 'Luas Bumi', 'Luas Bangunan', 'Nama', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB Awal', 'PBB Akhir', 'Tahun', 'Terbit SPPT', 'Lunas', 'Bayar SPPT', 'Keterangan'];



        $desa = "SELECT NM_KELURAHAN NAMA_KEL,KD_KELURAHAN,KD_KECAMATAN
          FROM REF_KELURAHAN
          WHERE KD_KECAMATAN=$KD_KECAMATAN AND KD_KELURAHAN IN (SELECT KD_KELURAHAN FROM " . $mv . " WHERE THN_PAJAK_SPPT=$tahun $and ) order by KD_KELURAHAN asc";

        $des = $this->db->query($desa)->result();
        $cbv = $this->db->query("select get_kecamatan('$KD_KECAMATAN') kec from dual")->row();

        $border = (new BorderBuilder())
            ->setBorderBottom(Color::GREEN, Border::WIDTH_THIN, Border::STYLE_DASHED)
            ->build();

        $style = (new StyleBuilder())
            ->setBorder($border)
            ->build();

        $writer = WriterEntityFactory::createXLSXWriter();
        $filePath = "tmp/DHKP_" . $cbv->KEC . ".xlsx";
        $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $style = (new StyleBuilder())
            ->setFontBold()
            ->setFontSize(11)
            ->setFontColor(Color::WHITE)
            ->setShouldWrapText(true)
            ->setBackgroundColor(Color::GREEN)
            ->build();

        $header = WriterEntityFactory::createRowFromArray(['NOP', 'Alamat OP', 'RW / RT OP', 'Luas Bumi', 'Luas Bangunan', 'Nama', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB Awal', 'PBB Akhir', 'Tahun', 'Terbit SPPT', 'Lunas', 'Bayar SPPT', 'Keterangan'], $style);



        $first = 1;
        foreach ($des as $kec) {

            $anddua = " AND KD_KECAMATAN='" . $kec->KD_KECAMATAN . "' AND KD_KELURAHAN='" . $kec->KD_KELURAHAN . "' " . $filter_buku;
            $sql = "SELECT *   FROM " . $mv . " WHERE THN_PAJAK_SPPT=$tahun $anddua ";
            $query  = $this->db->query($sql)->result();
            $arrisi = array();

            $pbb_akhir = 0;
            $pbb = 0;
            $multipleRows = [];
            foreach ($query as $rk) {
                $pbb += $rk->PBB;
                $pbb_akhir += $rk->PBB_AKHIR;
                $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
                $cells = [
                    WriterEntityFactory::createCell($rk->NOP),
                    WriterEntityFactory::createCell($rk->ALAMAT_OP),
                    WriterEntityFactory::createCell($rk->RWRT_OP),
                    WriterEntityFactory::createCell($rk->LUAS_BUMI_SPPT),
                    WriterEntityFactory::createCell($rk->LUAS_BNG_SPPT),
                    WriterEntityFactory::createCell($rk->NM_WP_SPPT),
                    WriterEntityFactory::createCell($rk->ALAMAT_WP),
                    WriterEntityFactory::createCell($rk->RWRT_WP),
                    WriterEntityFactory::createCell($rk->KELURAHAN_WP_SPPT),
                    WriterEntityFactory::createCell($rk->KOTA_WP_SPPT),
                    WriterEntityFactory::createCell((int) $rk->PBB),
                    WriterEntityFactory::createCell((int) $rk->PBB_AKHIR),
                    WriterEntityFactory::createCell($rk->THN_PAJAK_SPPT),
                    WriterEntityFactory::createCell($rk->TGL_TERBIT_SPPT),
                    WriterEntityFactory::createCell($lunas),
                    WriterEntityFactory::createCell($rk->TGL_PEMBAYARAN_SPPT),
                    WriterEntityFactory::createCell($rk->KET),
                ];
                $multipleRows[] = WriterEntityFactory::createRow($cells);
            }


            for ($e = 0; $e < 10; $e++) {
            }

            if ($first == 1) {
                $writer->getCurrentSheet()->setName($kec->KD_KELURAHAN . ' ' . $kec->NAMA_KEL);
                $writer->addRow($header)->addRows($multipleRows);
            } else {
                $writer->addNewSheetAndMakeItCurrent()->setName($kec->KD_KELURAHAN . ' ' . $kec->NAMA_KEL);
                $writer->addRow($header)->addRows($multipleRows);
            }

            $first++;
        }

        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
        die();
    }

    function downloaddhkp()
    {
        $this->load->helper('download');
        $file = 'C:/xampp/htdocs/layanan200/' . str_replace('_', '/', $this->input->get('file'));
        force_download($file, NULL);
    }
}
