
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clictrl extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(false);
    }

    function message($to = 'World')
    {
        echo "Hello {$to}!" . PHP_EOL;
    }

    function test()
    {
        $kd_kecamatan = $_POST['kd_kecamatan'];
        $this->benchmark->mark('code_start');
        $arr = $this->db->query("select kd_kecamatan, kd_kelurahan,count(1) op,sum(pbb_yg_harus_dibayar_sppt) jumlah
        from sppt 
        where thn_pajak_sppt=2020
        and kd_kecamatan='$kd_kecamatan'
        group by kd_kecamatan,kd_kelurahan")->result_array();
        $this->benchmark->mark('code_end');

        print_r($arr);
        // if($arr){
        //     return 'true';
        // }else{
        //     return 'false';
        // }

        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function coba()
    {
        $this->load->library('mylibrary');
        $url = base_url() . "cli/test";
        $param = array(
            'kd_kecamatan' => '260'
        );
        $this->mylibrary->do_in_background($url, $param);
    }

    public function prosesPelayananDua($thn, $bundel, $nomer, $tujuan = 2)
    {
        $this->load->model('PstPermohonan');
        /* 
            1 -> verlap
            2 -> PDI
        */

        $this->db->where('THN_PELAYANAN', $thn);
        $this->db->where('BUNDEL_PELAYANAN', $bundel);
        $this->db->where('NO_URUT_PELAYANAN', $nomer);
        $this->db->select("PST_PERMOHONAN.*,to_char(TGL_SURAT_PERMOHONAN ,'yyyy-mm-dd') tgl_masuk", false);
        $pst = $this->PstPermohonan->get()->result();
        print_r($pst);
    }


    public function prosesVerifikasi($tahun,$bundel,$nomer){
        $this->load->model('PstPermohonan');
        $this->db->where('THN_PELAYANAN', $tahun);
		$this->db->where('BUNDEL_PELAYANAN', $bundel);
		$this->db->where('NO_URUT_PELAYANAN', $nomer);
		$this->db->select("PST_PERMOHONAN.*,to_char(TGL_SURAT_PERMOHONAN ,'yyyy-mm-dd') tgl_masuk", false);
		$pst = $this->PstPermohonan->get()->row();

		$induk = [
			'NO_PERMOHONAN' =>null,
			'PELIMPAHAN' => 2,
			'THN_PELAYANAN' => $pst->THN_PELAYANAN,
			'BUNDEL_PELAYANAN' => $pst->BUNDEL_PELAYANAN,
			'NO_URUT_PELAYANAN' => $pst->NO_URUT_PELAYANAN,
			'NAMA_PEMOHON' => $pst->NAMA_PEMOHON,
			'ALAMAT_PEMOHON' => $pst->ALAMAT_PEMOHON,
			'TAHUN_PAJAK' => $pst->THN_PELAYANAN,
			'STATUS_VERIFIKASI' => 1,
			'NAMA_KASUBID' => 'Rachmat Hamd, S.Kom',
			'NIP_KASUBID' => '19830413 201001 1 013',
			// 'NAMA_PENELITI' => $this->session->userdata('pbb_nama'),
			'NIP_PENELITI' => $pst->NIP_PENERIMA,
			'CREATED_BY' => $pst->NIP_PENERIMA,
		];


		$this->db->trans_start();
		// proses disini
		$this->db->set('TGL_PERMOHONAN', "to_date('" . $pst->TGL_MASUK . "','yyyy-mm-dd')", false);
		$this->db->set('TGL_PENELITI', "to_date('" . date('Y-m-d') . "','yyyy-mm-dd')", false);
		$this->db->set('CREATED_AT', 'sysdate', false);
		$this->db->insert('PELAYANAN_VERIFIKASI', $induk);

	/* 	// BERKAS_PERSYARATAN
		$berkas = $this->input->post('KODE');
		foreach ($berkas as  $a) {
			$this->db->where('KODE', $a);
			$persyaratan = $this->db->get('BERKAS_PERSYARATAN')->row();
			$bb = [
				'THN_PELAYANAN' => $pst->THN_PELAYANAN,
				'BUNDEL_PELAYANAN' => $pst->BUNDEL_PELAYANAN,
				'NO_URUT_PELAYANAN' => $pst->NO_URUT_PELAYANAN,
				'KODE_BERKAS' => $persyaratan->KODE,
				'NAMA_BERKAS' => $persyaratan->NAMA_BERKAS,
				'STATUS_BERKAS' => 1
			];
			$this->db->insert('PELAYANAN_VERIFIKASI_BERKAS', $bb);
		} */

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			$kode = 1;
			$msg = "Data berhasil di proses";
		} else {
			$this->db->trans_rollback();
			$kode = 0;
			$msg = "Data gagal di proses";
		}

        echo $kode;
    }
}
