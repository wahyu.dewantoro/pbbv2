<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Check_session
{
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
    }

    public function validate()
    {
        // $CI = & get_instance();
        /* $this->CI->load->helper('file');

        $data = 'Some file data';
        if (!write_file('./path/to/file.php', $data)) {
            echo 'Unable to write the file';
        } else {
            echo 'File written!';
        } */
        // log_message('info', 'GET --> ' . var_export($this->CI->input->get(), true));
        // log_message('info', 'POST --> ' . var_export($this->CI->input->post(), true));                

        $log = [
            'SERVER_NAME' => $_SERVER['SERVER_NAME'],
            'SERVER_ADDR' => $_SERVER['SERVER_ADDR'],
            'SERVER_PORT' => $_SERVER['SERVER_PORT'],
            'REQUEST_URI' => $_SERVER['REQUEST_URI'],
            'REQUEST_IP' => $this->CI->input->ip_address(),
            // 'REQUEST_POST'=>json_encode($this->CI->input->post(NULL, TRUE)),
            'REQUEST_GET' => json_encode($this->CI->input->get(NULL, TRUE)),
            'REQUEST_SESSION' => json_encode($this->CI->session->all_userdata())
        ];
        $this->CI->db->set('request_date', 'sysdate', false);
        $this->CI->db->insert('LOG_REQUEST', $log);
        // log_message('info', '$_SERVER -->' . var_export($log, true));

        $exp = array('auth', 'clictrl', 'e-sppt', 'tandaterima', 'e-sppt-ntpd');
        $ctrl = $this->CI->uri->segment(1);
        $var = $this->CI->session->userdata('pbb');
        if (!in_array(strtolower($ctrl), $exp)) {
            if ($var <> 1) {
                $this->CI->session->set_flashdata('notif', ' <div class="note note-warning">Silahkan login terlebih dahulu</div>');
                redirect('auth');
                die();
            }

           /*  $roleblok = ['8', '13', '14'];
            if (in_array($this->CI->session('pbb_kg'), $roleblok)) {
                // $this->CI->session->set_flashdata('notif', ' <div class="note note-warning"></div>');
                // redirect('http://sipanji.id/simpbb');
                if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
                    $uri = 'https://';
                } else {
                    $uri = 'http://';
                }
                $uri .= $_SERVER['HTTP_HOST'];
                echo $pbb = $uri . '/simpbb';
                // header('Location: '.$pbb);
                die();
            } */
        }
    }
}
