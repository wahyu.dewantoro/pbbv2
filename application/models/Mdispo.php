<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdispo extends CI_Model
{
    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
        $this->kg = $this->session->userdata('pbb_kg');
    }

    function jsonmonitoringdispo()
    {
        $kode_group = $this->session->userdata('pbb_kg');
        $nip = $this->session->userdata('nip');
        $this->datatables->select("ID,NO_LAYANAN,NOP,LAYANAN,TAHUN,STATUS,NIP_PEDANIL");
        $this->datatables->from("PV_DOKUMEN");
        $this->datatables->where("KODE_GROUP='$kode_group'");
        if ($kode_group == '41') {
            $this->datatables->where("NIP_PEDANIL='$nip'");
        }
        $this->datatables->where("UNIT_SEBELUMNYA!='41'");
        $this->datatables->add_column('action',  anchor(site_url('permohonan/detailmonitoring/$1'), '<i class="fa fa-share-alt"></i>', 'data-toggle="tooltip" title="Pelimpahan  " class="btn btn-xs btn-primary"'), 'ID');
        $this->datatables->add_column('detail',  anchor(site_url('permohonan/detaildua/$1'), '<i class="fa fa-binoculars"></i>', 'data-toggle="tooltip" title="detail" class="btn btn-xs btn-success"'), 'NO_LAYANAN');
        return $this->datatables->generate();
    }

    function getDataShare($id)
    {
        $data['rk'] = $this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
                                        A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
                                        A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
                                        B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST
                                        FROM PST_DETAIL A
                                        JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
                                        AND D.KD_KANTOR = A.KD_KANTOR
                                        AND D.THN_PELAYANAN = A.THN_PELAYANAN
                                        AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
                                        AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
                                        JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                        WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$id'")->row();
        $data['rn'] = $this->db->query("SELECT to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
                                    from p_riwayat a
                                    join p_group b on a.kode_group=b.kode_group
                                    where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();

        $kg = $this->kg;
        $hd = $this->db->query("select count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
        $shd = $this->db->query("select count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id'  and kode_group='$kg'")->row();



        $data['button'] = $hd->RES;
        $data['btn']    = $shd->RES;

        $data['unit'] = $this->db->query("SELECT NIP,NAMA_PENGGUNA  FROM P_PENGGUNA WHERE KODE_GROUP=41")->result();
        return $data;
    }

    function getDataShareSelesai($id)
    {
        $data['rk'] = $this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
                                        A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
                                        A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
                                        B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST
                                        FROM PST_DETAIL A
                                        JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
                                        AND D.KD_KANTOR = A.KD_KANTOR
                                        AND D.THN_PELAYANAN = A.THN_PELAYANAN
                                        AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
                                        AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
                                        JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                        WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$id'")->row();
        $data['rn'] = $this->db->query("SELECT to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
                                    from p_riwayat a
                                    join p_group b on a.kode_group=b.kode_group
                                    where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();

        $kg = $this->kg;
        $hd = $this->db->query("SELECT count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
        $shd = $this->db->query("SELECT count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id'  and kode_group='$kg'")->row();
        $data['button'] = $hd->RES;
        $data['btn']    = $shd->RES;
        $data['id'] = $id;
        $data['dispo']  = $this->db->query("SELECT a.nip,nama_pengguna pegawai,to_char(tgl_awal,'dd/mm/yyyy') ta  ,to_char(tgl_perkiraan_selesai,'dd/mm/yyyy') tps
                                            from P_DISPOSISIPEDANIL a
                                            join p_pengguna b on a.nip=b.nip
                                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP='$id'")->row();
        return $data;
    }





    function insertDisposisi($data = array())
    {
        /*$this->db->set('name', $name);
        $this->db->set('title', $title);
        $this->db->set('status', $status);*/
        $this->db->set('KD_KANWIL', $data['KD_KANWIL']);
        $this->db->set('KD_KANTOR', $data['KD_KANTOR']);
        $this->db->set('THN_PELAYANAN', $data['THN_PELAYANAN']);
        $this->db->set('BUNDEL_PELAYANAN', $data['BUNDEL_PELAYANAN']);
        $this->db->set('NO_URUT_PELAYANAN', $data['NO_URUT_PELAYANAN']);
        $this->db->set('KD_PROPINSI', $data['KD_PROPINSI']);
        $this->db->set('KD_DATI2', $data['KD_DATI2']);
        $this->db->set('KD_KECAMATAN', $data['KD_KECAMATAN']);
        $this->db->set('KD_KELURAHAN', $data['KD_KELURAHAN']);
        $this->db->set('KD_BLOK', $data['KD_BLOK']);
        $this->db->set('NO_URUT', $data['NO_URUT']);
        $this->db->set('KD_JNS_OP', $data['KD_JNS_OP']);
        $this->db->set('NIP', $data['NIP']);
        $this->db->set('TGL_AWAL', "to_date('" . $data['TGL_AWAL'] . "','yyyy-mm-dd')", false);
        $this->db->set('TGL_PERKIRAAN_SELESAI', "to_date('" . $data['TGL_PERKIRAAN_SELESAI'] . "','yyyy-mm-dd')", false);
        $this->db->set('KETERANGAN', $data['KETERANGAN']);
        $this->db->insert('P_DISPOSISIPEDANIL');
    }

    function updatedisposisi($data = array())
    {
        $this->db->set('STATUS', $data['STATUS']);
        $this->db->set('TGL_SELESAI', "to_date('" . $data['TGL_SELESAI'] . "','yyyy-mm-dd')", false);
        $this->db->where('KD_KANWIL', $data['KD_KANWIL']);
        $this->db->where('KD_KANTOR', $data['KD_KANTOR']);
        $this->db->where('THN_PELAYANAN', $data['THN_PELAYANAN']);
        $this->db->where('BUNDEL_PELAYANAN', $data['BUNDEL_PELAYANAN']);
        $this->db->where('NO_URUT_PELAYANAN', $data['NO_URUT_PELAYANAN']);
        $this->db->where('KD_PROPINSI', $data['KD_PROPINSI']);
        $this->db->where('KD_DATI2', $data['KD_DATI2']);
        $this->db->where('KD_KECAMATAN', $data['KD_KECAMATAN']);
        $this->db->where('KD_KELURAHAN', $data['KD_KELURAHAN']);
        $this->db->where('KD_BLOK', $data['KD_BLOK']);
        $this->db->where('NO_URUT', $data['NO_URUT']);
        $this->db->where('KD_JNS_OP', $data['KD_JNS_OP']);

        $this->db->update('P_DISPOSISIPEDANIL');
    }
}

/* End of file Mgroup.php */
/* Location: ./application/models/Mgroup.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-28 01:25:45 */
/* http://harviacode.com */
