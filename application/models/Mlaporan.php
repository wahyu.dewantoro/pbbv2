<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mlaporan extends CI_Model
{


    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
    }


    public function barubataldua($tahun, $jenis = 0){
           // 
           switch ($jenis) {
            case '1':
                # code...
                $vjenis = " and b.jns_transaksi_op=1 ";
                break;
            case '3':
                # code...
                $vjenis = " and b.jns_transaksi_op=3 ";
                break;
            default:
                # code...
                $vjenis = " and b.jns_transaksi_op in (1,3) ";
                break;
        }


        $sql = "SELECT 
        a.kd_kecamatan,
        nm_kecamatan,
        case when b.jns_transaksi_op=1 then 'OP Baru' when b.jns_transaksi_op=3 then 'Pembatalan' else 'other' end AS transaksi,
        sum(a.pbb_yg_harus_dibayar_sppt) jumlah
   FROM (SELECT *
           FROM sppt@to17
          WHERE thn_pajak_sppt = '$tahun') a,
        dat_objek_pajak@to17 b,
        ref_kecamatan@to17 d
  WHERE     b.kd_propinsi = a.kd_propinsi
        AND b.kd_dati2 = a.kd_dati2
        AND b.kd_kecamatan = a.kd_kecamatan
        AND b.kd_kelurahan = a.kd_kelurahan
        AND b.kd_blok = a.kd_blok
        AND b.no_urut = a.no_urut
        AND b.kd_jns_op = a.kd_jns_op
        AND d.kd_propinsi = a.kd_propinsi
        AND d.kd_dati2 = a.kd_dati2
        AND d.kd_kecamatan = a.kd_kecamatan
        $vjenis
        and to_char(b.tgl_perekaman_op,'yyyy')='$tahun'
        group by a.kd_kecamatan,
        nm_kecamatan,jns_transaksi_op";
        return $this->db->query($sql)->result_array();
    }

    public function barubatal($tahun, $jenis = 0)
    {
        // 
        switch ($jenis) {
            case '1':
                # code...
                $vjenis = " and b.jns_transaksi_op=1 ";
                break;
            case '3':
                # code...
                $vjenis = " and b.jns_transaksi_op=3 ";
                break;
            default:
                # code...
                $vjenis = " and b.jns_transaksi_op in (1,3) ";
                break;
        }


        $sql = "SELECT 
        a.kd_kecamatan,
        nm_kecamatan,
        case when b.jns_transaksi_op=1 then 'OP Baru' when b.jns_transaksi_op=3 then 'Pembatalan' else 'other' end AS transaksi,
        sum(a.pbb_yg_harus_dibayar_sppt) jumlah
   FROM (SELECT *
           FROM sppt@to17
          WHERE thn_pajak_sppt = '$tahun') a,
        dat_objek_pajak@to17 b,
        ref_kecamatan@to17 d
  WHERE     b.kd_propinsi = a.kd_propinsi
        AND b.kd_dati2 = a.kd_dati2
        AND b.kd_kecamatan = a.kd_kecamatan
        AND b.kd_kelurahan = a.kd_kelurahan
        AND b.kd_blok = a.kd_blok
        AND b.no_urut = a.no_urut
        AND b.kd_jns_op = a.kd_jns_op
        AND d.kd_propinsi = a.kd_propinsi
        AND d.kd_dati2 = a.kd_dati2
        AND d.kd_kecamatan = a.kd_kecamatan
        $vjenis
        and to_char(b.tgl_perekaman_op,'yyyy')='$tahun'
        group by a.kd_kecamatan,
        nm_kecamatan,jns_transaksi_op";
        return $this->db->query($sql)->result();
    }
}
