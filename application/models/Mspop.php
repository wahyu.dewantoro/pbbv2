<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mspop extends CI_Model
{

    function getNoLayanan()
    {
        $rk = $this->db->query("SELECT  TO_CHAR(CURRENT_DATE,'YYYY') TAHUN ,
        							  CASE WHEN  MAX( TO_NUMBER(NO_URUT_PELAYANAN)) <200 THEN   MAX( TO_NUMBER(BUNDEL_PELAYANAN))  ELSE MAX( TO_NUMBER(BUNDEL_PELAYANAN))+1 END   BUNDEL_PELAYANAN ,
        							  CASE WHEN  MAX( TO_NUMBER(NO_URUT_PELAYANAN)) <200 THEN   MAX( TO_NUMBER(NO_URUT_PELAYANAN)) +1 ELSE 1 END  NO_URUT_PELAYANAN
                                FROM PST_PERMOHONAN 
                                WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY')
                                AND BUNDEL_PELAYANAN IN (SELECT  MAX( TO_NUMBER(BUNDEL_PELAYANAN)) BUNDEL_PELAYANAN
                                FROM PST_PERMOHONAN WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY'))")->row();

        // sprintf('%03s', $i)
        return array(
            'tahun' => $rk->TAHUN,
            'bundel' => sprintf('%04s', $rk->BUNDEL_PELAYANAN),
            'no_urut' => sprintf('%03s', $rk->NO_URUT_PELAYANAN)
        );
    }
}
