<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpermohonan extends CI_Model
{

    public $table = 'PST_PERMOHONAN';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
    }

    // datatables
    function json($t1, $t2)
    {
        // $t1 = $this->session->userdata('p_tanggal1');
        // $t2 = $this->session->userdata('p_tanggal2');
        $wh = "TGL_TERIMA_DOKUMEN_WP between  to_date('$t1','dd-mm-yyyy') and to_date('$t2','dd-mm-yyyy')";
        $this->datatables->select("NO_LAYANAN,NAMA_PEMOHON,TGL_TERIMA,TGL_SELESAI,STATUS_KOLEKTIF,NAMA_PENERIMA,IS_ONLINE");
        $this->datatables->from("(select * from  PV_PERMOHONAN order by no_layanan desc)");
        $this->datatables->where($wh);


        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('permohonan/detail/$1'), '<i class="fa fa-binoculars"></i>', 'class="btn btn-xs btn-primary"') . anchor(site_url('permohonan/updatepermohonan/$1'), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') . anchor(site_url('permohonan/delete/$1'), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"') . '</div></div>', 'NO_LAYANAN');
        return $this->datatables->generate();
    }
    function json_berkas($t1, $t2)
    {
        $wh = "TGL_TERIMA_DOKUMEN_WP between  to_date('$t1','dd-mm-yyyy') and to_date('$t2','dd-mm-yyyy') AND JUMLAH_FILE>'0'";
        $this->datatables->select("NO_LAYANAN,NAMA_PEMOHON,TGL_TERIMA,TGL_SELESAI,STATUS_KOLEKTIF,JUMLAH_FILE");
        $this->datatables->from("(select * from  PV_PERMOHONAN_BERKAS order by no_layanan desc)");
        $this->datatables->where($wh);
        //add this line for join

        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('permohonan/form_upload/$1'), '<i class="fa fa-upload"></i>', 'class="btn btn-xs btn-primary"') . '</div>', 'NO_LAYANAN');
        return $this->datatables->generate();
    }
    function json_berkas_kurang($t1, $t2)
    {

        $wh = "TGL_TERIMA_DOKUMEN_WP between  to_date('$t1','dd-mm-yyyy') and to_date('$t2','dd-mm-yyyy') AND JUMLAH_FILE <='0'";
        $this->datatables->select("NO_LAYANAN,NAMA_PEMOHON,TGL_TERIMA,TGL_SELESAI,STATUS_KOLEKTIF,JUMLAH_FILE");
        $this->datatables->from("(select * from  PV_PERMOHONAN_BERKAS order by no_layanan desc)");
        $this->datatables->where($wh);
        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('permohonan/form_upload/$1'), '<i class="fa fa-upload"></i>', 'class="btn btn-xs btn-primary"') . '</div>', 'NO_LAYANAN');
        return $this->datatables->generate();
    }
    function json_ferifikasi_berkas($data = null)
    {

        $this->datatables->select('THN_PELAYANAN,BUNDEL_PELAYANAN,NO_URUT_PELAYANAN,NAMA_PEMOHON,ALAMAT_PEMOHON,KOLEKTIF,TGL_MASUK');
        $this->datatables->from('VW_VERIFIKASI_PERMOHONAN');
        return $this->datatables->generate();
    }

    function json_ferifikasi_berkas_list()
    {
        /* select * from pelayanan_verifikasi
order by created_at desc */
        // $this->datatables->select('THN_PELAYANAN,BUNDEL_PELAYANAN,NO_URUT_PELAYANAN,NAMA_PEMOHON,ALAMAT_PEMOHON,KOLEKTIF,TGL_MASUK');
        $this->datatables->from('PELAYANAN_VERIFIKASI');
        $this->db->order_by('CREATED_AT','DESC');
        return $this->datatables->generate();
    }


    function jsonNjop($res = array())
    {
        $t1 = $res['p_tanggal1'];
        $t2 = $res['p_tanggal2'];

        $this->datatables->select("NOPEL,TGL_PERMOHONAN,NAMA_PEMOHON,JUMLAH");
        $this->datatables->from("(SELECT THN_PELAYANAN || '.' || BUNDEL_PELAYANAN || '.' || NO_URUT_PELAYANAN   NOPEL,  TGL_SURAT_PERMOHONAN TGL_PERMOHONAN, NAMA_PEMOHON, count(1) jumlah FROM (SELECT *
        FROM VSK_NJOP
       WHERE TGL_SURAT_PERMOHONAN BETWEEN TO_DATE ('$t1', 'dd-mm-yyyy') AND TO_DATE ('$t2', 'dd-mm-yyyy'))
       group by THN_PELAYANAN || '.' || BUNDEL_PELAYANAN || '.' || NO_URUT_PELAYANAN,TGL_SURAT_PERMOHONAN,NAMA_PEMOHON)");
        return $this->datatables->generate();
    }


    function jsondatadetailpermohonan()
    {
        $t1 = $this->session->userdata('p_tanggal1');
        $t2 = $this->session->userdata('p_tanggal2');
        $wh = "TGL_TERIMA_DOKUMEN_WP between  to_date('$t1','dd-mm-yyyy') and to_date('$t2','dd-mm-yyyy')";
        $this->datatables->select("ID,NO_LAYANAN,NOP,NAMA,LAYANAN,TGL_TERIMA,TGL_SELESAI,TGLSELESAI,STATUS");
        $this->datatables->from("PV_PERMOHONANDETAIL");
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }

    function jsonmonitoring($where)
    {
        $kode_group = $this->session->userdata('pbb_kg');

        $from = "(
                 select  get_last_divisi( replace(no_Layanan||nop,'.','')) last_divisi ,a.*
                 from PV_DOKUMEN a
                 where layanan<>'SK NJOP' and kode_group='$kode_group' $where
                 )";


        $this->datatables->select("ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,STATUS,NM_PENGAJUAN,KD_PENGAJUAN,LAST_DIVISI,NM_KELURAHAN,TGL_MASUK");
        $this->datatables->from($from);
        $this->datatables->add_column('action',  anchor(site_url('permohonan/edittempatlayanan/$1'), '<i class="fa fa-edit"></i>', 'data-toggle="tooltip" title="Edit Tempat Layanan  " class="btn btn-xs btn-success"'), 'NO_LAYANAN');
        $this->datatables->add_column('detail',  anchor(site_url('permohonan/detaildua/urlencode($1)'), '<i class="fa fa-binoculars"></i>', 'data-toggle="tooltip" title="detail" class="btn btn-xs btn-success"'), 'NO_LAYANAN');
        return $this->datatables->generate();
    }

    function jsontempatlayanan($where)
    {
        $kode_group = $this->session->userdata('pbb_kg');

        $from = "(
                 select  get_last_divisi( replace(no_Layanan||nop,'.','')) last_divisi ,ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,STATUS,NM_PENGAJUAN,KD_PENGAJUAN,NM_KELURAHAN,CASE b.unit_kantor WHEN 'kota' THEN 'Kantor Pendopo' WHEN 'kabupaten' THEN 'Kantor Kepanjen' ELSE '' END TEMPAT_LAYANAN
                 from PV_DOKUMEN a LEFT JOIN P_IP b ON IP_THN_PELAYANAN||'.'||IP_BUNDEL_PELAYANAN||'.'||IP_NO_URUT_PELAYANAN=a.NO_LAYANAN
WHERE   b.UNIT_KANTOR IS NOT NULL $where
                 )";

        $this->datatables->select("ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,STATUS,NM_PENGAJUAN,KD_PENGAJUAN,LAST_DIVISI,NM_KELURAHAN,TEMPAT_LAYANAN");
        $this->datatables->from($from);

        $this->datatables->add_column('action',  anchor(site_url('permohonan/edittempatlayanan/$1'), '<i class="fa fa-edit"></i>', 'data-toggle="tooltip" title="Edit Tempat Layanan  " class="btn btn-xs btn-success"'), 'NO_LAYANAN');
        $this->datatables->add_column('detail',  anchor(site_url('permohonan/detaildua/$1'), '<i class="fa fa-binoculars"></i>', 'data-toggle="tooltip" title="detail" class="btn btn-xs btn-success"'), 'NO_LAYANAN');
        return $this->datatables->generate();
    }

    function jsondatask()
    {
        $this->datatables->select("ID,JENIS_SK,TANGGALSK,NOMOR_SK,TANGGALPERMOHONAN,NOP,NOPEL");
        $this->datatables->from("PV_DATA_SK");
        return $this->datatables->generate();
    }


    function jsonpengambilan()
    {
        $kode_group = $this->session->userdata('pbb_kg');
        $this->datatables->select("ID,NO_LAYANAN,NOP,LAYANAN,TAHUN,STATUS");
        $this->datatables->from("PV_DOKUMEN");
        $this->datatables->where("KODE_GROUP='$kode_group'");
        $this->datatables->add_column('action',  anchor(site_url('permohonan/detailpengambilan/$1'), '<i class="fa fa-check-circle"></i> Proses', 'class="btn btn-xs btn-success"'), 'ID');

        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        // 2017.0001.207
        return $this->db->query("select KD_KANWIL,
                                KD_KANTOR,
                                THN_PELAYANAN,
                                BUNDEL_PELAYANAN,
                                NO_URUT_PELAYANAN,
                                NO_SRT_PERMOHONAN,
                                TO_CHAR(TGL_SURAT_PERMOHONAN,'dd-mm-yyyy') TGL_SURAT_PERMOHONAN,
                                NAMA_PEMOHON,
                                ALAMAT_PEMOHON,
                                KETERANGAN_PST,
                                CATATAN_PST,
                                STATUS_KOLEKTIF,
                                TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'dd-mm-yyyy') TGL_TERIMA_DOKUMEN_WP,
                                TO_CHAR(TGL_PERKIRAAN_SELESAI,'dd-mm-yyyy') TGL_PERKIRAAN_SELESAI,
                                NIP_PENERIMA 
                                 from pst_permohonan
                            where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'")->row();
        /*$this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();*/
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('', $q);
        $this->db->or_like('KD_KANWIL', $q);
        $this->db->or_like('KD_KANTOR', $q);
        $this->db->or_like('THN_PELAYANAN', $q);
        $this->db->or_like('BUNDEL_PELAYANAN', $q);
        $this->db->or_like('NO_URUT_PELAYANAN', $q);
        $this->db->or_like('NO_SRT_PERMOHONAN', $q);
        $this->db->or_like('TGL_SURAT_PERMOHONAN', $q);
        $this->db->or_like('NAMA_PEMOHON', $q);
        $this->db->or_like('ALAMAT_PEMOHON', $q);
        $this->db->or_like('KETERANGAN_PST', $q);
        $this->db->or_like('CATATAN_PST', $q);
        $this->db->or_like('STATUS_KOLEKTIF', $q);
        $this->db->or_like('TGL_TERIMA_DOKUMEN_WP', $q);
        $this->db->or_like('TGL_PERKIRAAN_SELESAI', $q);
        $this->db->or_like('NIP_PENERIMA', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('', $q);
        $this->db->or_like('KD_KANWIL', $q);
        $this->db->or_like('KD_KANTOR', $q);
        $this->db->or_like('THN_PELAYANAN', $q);
        $this->db->or_like('BUNDEL_PELAYANAN', $q);
        $this->db->or_like('NO_URUT_PELAYANAN', $q);
        $this->db->or_like('NO_SRT_PERMOHONAN', $q);
        $this->db->or_like('TGL_SURAT_PERMOHONAN', $q);
        $this->db->or_like('NAMA_PEMOHON', $q);
        $this->db->or_like('ALAMAT_PEMOHON', $q);
        $this->db->or_like('KETERANGAN_PST', $q);
        $this->db->or_like('CATATAN_PST', $q);
        $this->db->or_like('STATUS_KOLEKTIF', $q);
        $this->db->or_like('TGL_TERIMA_DOKUMEN_WP', $q);
        $this->db->or_like('TGL_PERKIRAAN_SELESAI', $q);
        $this->db->or_like('NIP_PENERIMA', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->query("delete from pst_permohonan where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'");
        $this->db->query("delete from pst_detail where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'");
        $this->db->query("delete from pst_lampiran where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'");
        $this->db->query("delete from p_riwayat where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'");
        $this->db->query("commit");
    }

    function getDetailpermohonan($id)
    {
        return $this->db->query("SELECT CEK_ONLINE(THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN) IS_ONLINE,THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN NO_LAYANAN,NO_SRT_PERMOHONAN,TO_CHAR(TGL_SURAT_PERMOHONAN,'YYYY-MM-DD') TGL_SURAT_PERMOHONAN,NAMA_PEMOHON,ALAMAT_PEMOHON,KETERANGAN_PST,CATATAN_PST,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY-MM-DD') TGL_TERIMA_DOKUMEN_WP,TO_CHAR(TGL_PERKIRAAN_SELESAI,'YYYY-MM-DD') TGL_PERKIRAAN_SELESAI,NIP_PENERIMA,B.NAMA_PENGGUNA,STATUS_KOLEKTIF,IP_EMAIL,IP_HP,NIK
                                    FROM PST_PERMOHONAN A
                                    LEFT JOIN P_PENGGUNA B ON A.NIP_PENERIMA=B.NIP
                                     LEFT JOIN P_IP C ON C.IP_THN_PELAYANAN=A.THN_PELAYANAN AND C.IP_BUNDEL_PELAYANAN=A.BUNDEL_PELAYANAN AND C.IP_NO_URUT_PELAYANAN=A.NO_URUT_PELAYANAN
                                    WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN=?", array($id))->row();
    }

    function getAnggotapermohonan($id)
    {
        $tahun = date('Y');
        return $this->db->query("SELECT DISTINCT  THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN no_layanan,a.kd_propinsi_pemohon||'.'||a.kd_dati2_pemohon||'.'||a.kd_kecamatan_pemohon||'.'||a.kd_kelurahan_pemohon||'.'||a.kd_blok_pemohon||'-'||a.no_urut_pemohon||'.'||a.kd_jns_op_pemohon  nop,
                                    nm_jenis_pelayanan layanan,THN_PELAYANAN, case status_selesai when 1 then 'Selesai' else 'Proses' end  status  ,tgl_selesai,nm_seksi,
                                    jalan_wp JALAN_OP,nm_kecamatan,nm_kelurahan, NM_WP,get_status_layanan(THN_PELAYANAN,BUNDEL_PELAYANAN,NO_URUT_PELAYANAN,a.kd_propinsi_pemohon,a.kd_dati2_pemohon,a.kd_kecamatan_pemohon,a.kd_kelurahan_pemohon,a.kd_blok_pemohon,a.no_urut_pemohon,a.kd_jns_op_pemohon  ) status_pelayanan
                                    from pst_detail a
                                    left join ref_jns_pelayanan b on a.kd_jns_pelayanan=b.kd_jns_pelayanan
                                    left join dat_objek_pajak d on  a.kd_propinsi_pemohon=d.kd_propinsi 
                                    and a.kd_dati2_pemohon=d.kd_dati2
                                    and a.kd_kecamatan_pemohon=d.kd_kecamatan 
                                    and  a.kd_kelurahan_pemohon=d.kd_kelurahan 
                                    and a.kd_blok_pemohon=d.kd_blok 
                                    and  a.no_urut_pemohon=d.no_urut 
                                    and  a.kd_jns_op_pemohon=d.kd_jns_op
                                   left join dat_subjek_pajak g on g.subjek_pajak_id=d.subjek_pajak_id
                                    left join ref_seksi c on c.kd_seksi=a.kd_seksi_berkas
                                    left join ref_kecamatan e on e.kd_kecamatan=a.kd_kecamatan_pemohon
                                    left join ref_kelurahan f on f.kd_kelurahan=a.kd_kelurahan_pemohon and f.kd_kecamatan=a.kd_kecamatan_pemohon
                                    where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN=?
                                    ", array($id))->result_array();
        //and thn_pajak_sppt='$tahun'
    }

    function getNamaWP($nop)
    {
        $rk = $this->db->query("select nm_wp from dat_objek_pajak@to17 a
                                join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
                                where KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP='$nop'")->row();
        if (count($rk) > 0) {
            $res = $rk->NM_WP;
        } else {
            $res = '-';
        }
        return $res;
    }

    function getAnggotapermohonan_a($id)
    {
        $tahun = date('Y');
        return $this->db->query("select  THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN no_layanan, kd_propinsi_pemohon||'.'||kd_dati2_pemohon||'.'||kd_kecamatan_pemohon||'.'||kd_kelurahan_pemohon||'.'||kd_blok_pemohon||'-'||no_urut_pemohon||'.'||kd_jns_op_pemohon  nop,
                                    nm_jenis_pelayanan layanan,THN_PELAYANAN, case status_selesai when 1 then 'Selesai' else 'Proses' end  status  ,tgl_selesai,nm_seksi,
                                    jalan_op,nm_kecamatan,nm_kelurahan,nm_wp,npwp
                                    from pst_detail a
                                    join ref_jns_pelayanan b on a.kd_jns_pelayanan=b.kd_jns_pelayanan
                                    left join dat_objek_pajak d on  a.kd_propinsi_pemohon=d.kd_propinsi 
                                    and a.kd_dati2_pemohon=d.kd_dati2
                                    and a.kd_kecamatan_pemohon=d.kd_kecamatan 
                                    and  a.kd_kelurahan_pemohon=d.kd_kelurahan 
                                    and a.kd_blok_pemohon=d.kd_blok 
                                    and  a.no_urut_pemohon=d.no_urut 
                                    and  a.kd_jns_op_pemohon=d.kd_jns_op
                                    left join ref_seksi c on c.kd_seksi=a.kd_seksi_berkas
                                    join ref_kecamatan e on e.kd_kecamatan=a.kd_kecamatan_pemohon
                                    join ref_kelurahan f on f.kd_kelurahan=a.kd_kelurahan_pemohon and f.kd_kecamatan=a.kd_kecamatan_pemohon
                                    left join dat_subjek_pajak i on i.subjek_pajak_id=d.subjek_pajak_id
                                    where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'
                                    ")->result_array();
        //and thn_pajak_sppt='$tahun'
    }

    function getDOkterlampir($id)
    {
        return $this->db->query("SELECT  L_PERMOHONAN,L_SURAT_KUASA,L_KTP_WP,L_SERTIFIKAT_TANAH,L_SPPT,L_IMB,L_AKTE_JUAL_BELI,L_SK_PENSIUN,L_SPPT_STTS,L_STTS,L_SK_PENGURANGAN,L_SK_KEBERATAN,L_SKKP_PBB,L_SPMKP_PBB,L_LAIN_LAIN,L_SKET_TANAH,L_SKET_LURAH,L_NPWPD,L_PENGHASILAN,L_CAGAR
                                 FROM PST_LAMPIRAN
                                    WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN=?", array($id))->row();
    }


    function insertriwayat($data = array())
    {
        // $this->db->insert('P_RIWAYAT',$data);
        $KD_KANWIL            = $data['KD_KANWIL'];
        $KD_KANTOR            = $data['KD_KANTOR'];
        $THN_PELAYANAN        = $data['THN_PELAYANAN'];
        $BUNDEL_PELAYANAN     = $data['BUNDEL_PELAYANAN'];
        $NO_URUT_PELAYANAN    = $data['NO_URUT_PELAYANAN'];
        $KD_PROPINSI_RIWAYAT  = $data['KD_PROPINSI_RIWAYAT'];
        $KD_DATI2_RIWAYAT     = $data['KD_DATI2_RIWAYAT'];
        $KD_KECAMATAN_RIWAYAT = $data['KD_KECAMATAN_RIWAYAT'];
        $KD_KELURAHAN_RIWAYAT = $data['KD_KELURAHAN_RIWAYAT'];
        $KD_BLOK_RIWAYAT      = $data['KD_BLOK_RIWAYAT'];
        $NO_URUT_RIWAYAT      = $data['NO_URUT_RIWAYAT'];
        $KD_JNS_OP_RIWAYAT    = $data['KD_JNS_OP_RIWAYAT'];
        $KODE_GROUP           = $data['KODE_GROUP'];
        $NIP                  = $data['NIP'];
        $TANGGAL_AWAL         = $data['TANGGAL_AWAL'];
        $TANGGAL_AKHIR        = $data['TANGGAL_AKHIR'];
        $KETERANGAN           = $data['KETERANGAN'];
        $STATUS_BERKAS        = $data['STATUS_BERKAS'];
        $UNIT_SEBEUMNYA = $data['UNIT_SEBEUMNYA'];
        $this->db->query("INSERT INTO P_RIWAYAT (KD_KANWIL, KD_KANTOR, THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, KD_PROPINSI_RIWAYAT, KD_DATI2_RIWAYAT, KD_KECAMATAN_RIWAYAT, KD_KELURAHAN_RIWAYAT, KD_BLOK_RIWAYAT, NO_URUT_RIWAYAT, KD_JNS_OP_RIWAYAT, KODE_GROUP, NIP, TANGGAL_AWAL, TANGGAL_AKHIR, KETERANGAN,STATUS_BERKAS,UNIT_SEBEUMNYA) VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$KD_PROPINSI_RIWAYAT', '$KD_DATI2_RIWAYAT', '$KD_KECAMATAN_RIWAYAT', '$KD_KELURAHAN_RIWAYAT', '$KD_BLOK_RIWAYAT', '$NO_URUT_RIWAYAT', '$KD_JNS_OP_RIWAYAT', '$KODE_GROUP', '$NIP',TO_DATE('$TANGGAL_AWAL', 'yyyy-mm-dd hh24:mi:ss') , TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss') , '$KETERANGAN','$STATUS_BERKAS','$UNIT_SEBEUMNYA')");
    }

    function updateriwayat($data = array())
    {
        $KD_KANWIL            = $data['KD_KANWIL'];
        $KD_KANTOR            = $data['KD_KANTOR'];
        $THN_PELAYANAN        = $data['THN_PELAYANAN'];
        $BUNDEL_PELAYANAN     = $data['BUNDEL_PELAYANAN'];
        $NO_URUT_PELAYANAN    = $data['NO_URUT_PELAYANAN'];
        $KD_PROPINSI_RIWAYAT  = $data['KD_PROPINSI_RIWAYAT'];
        $KD_DATI2_RIWAYAT     = $data['KD_DATI2_RIWAYAT'];
        $KD_KECAMATAN_RIWAYAT = $data['KD_KECAMATAN_RIWAYAT'];
        $KD_KELURAHAN_RIWAYAT = $data['KD_KELURAHAN_RIWAYAT'];
        $KD_BLOK_RIWAYAT      = $data['KD_BLOK_RIWAYAT'];
        $NO_URUT_RIWAYAT      = $data['NO_URUT_RIWAYAT'];
        $KD_JNS_OP_RIWAYAT    = $data['KD_JNS_OP_RIWAYAT'];
        $KODE_GROUP           = $data['KODE_GROUP'];
        $NIP                  = $data['NIP'];
        $TANGGAL_AKHIR        = $data['TANGGAL_AKHIR'];
        $KETERANGAN           = $data['KETERANGAN'];
        $STATUS_BERKAS        = $data['STATUS_BERKAS'];
        $this->db->query("UPDATE p_riwayat set status_berkas='$STATUS_BERKAS',keterangan='$KETERANGAN',tanggal_akhir=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss'),nip='$NIP' 
                            where KD_KANWIL            ='$KD_KANWIL'
                            and KD_KANTOR            ='$KD_KANTOR'
                            and THN_PELAYANAN        ='$THN_PELAYANAN'
                            and BUNDEL_PELAYANAN     ='$BUNDEL_PELAYANAN'
                            and NO_URUT_PELAYANAN    ='$NO_URUT_PELAYANAN'
                            and KD_PROPINSI_RIWAYAT  ='$KD_PROPINSI_RIWAYAT'
                            and KD_DATI2_RIWAYAT     ='$KD_DATI2_RIWAYAT'
                            and KD_KECAMATAN_RIWAYAT ='$KD_KECAMATAN_RIWAYAT'
                            and KD_KELURAHAN_RIWAYAT ='$KD_KELURAHAN_RIWAYAT'
                            and KD_BLOK_RIWAYAT      ='$KD_BLOK_RIWAYAT'
                            and NO_URUT_RIWAYAT      ='$NO_URUT_RIWAYAT'
                            and KD_JNS_OP_RIWAYAT    ='$KD_JNS_OP_RIWAYAT'
                            and KODE_GROUP          ='$KODE_GROUP'
                            and TANGGAL_AKHIR is null");
    }

    /*  function getNamaWP($nop)
    {
        $rk = $this->db->query("select nm_wp from dat_objek_pajak a
                                join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
                                where KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP='$nop'")->row();
        if (count($rk) > 0) {
            $res = $rk->NM_WP;
        } else {
            $res = '-';
        }
        return $res;
    }
 */

    function getNoLayanan()
    {
        $rk = $this->db->query("SELECT  TO_CHAR(CURRENT_DATE,'YYYY') TAHUN ,  CASE WHEN  MAX( TO_NUMBER(NO_URUT_PELAYANAN)) < 200 THEN   MAX( TO_NUMBER(BUNDEL_PELAYANAN))  ELSE MAX( TO_NUMBER(BUNDEL_PELAYANAN))+1 END   BUNDEL_PELAYANAN , CASE WHEN   MAX( TO_NUMBER(NO_URUT_PELAYANAN)) <200 THEN   MAX( TO_NUMBER(NO_URUT_PELAYANAN)) +1 ELSE 1 END  NO_URUT_PELAYANAN
                                FROM PST_PERMOHONAN 
                                WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY')
                                AND BUNDEL_PELAYANAN IN (SELECT  MAX( TO_NUMBER(BUNDEL_PELAYANAN)) BUNDEL_PELAYANAN
                                FROM PST_PERMOHONAN WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY'))")->row();

        // sprintf('%03s', $i)
        return array(
            'tahun' => $rk->TAHUN,
            'bundel' => sprintf('%04s', $rk->BUNDEL_PELAYANAN),
            'no_urut' => sprintf('%03s', $rk->NO_URUT_PELAYANAN)
        );
    }

    function getNopeNJOP()
    {
        $rk = $this->db->query("SELECT tahun,case when  bundel_pelayanan is null then 1 else bundel_pelayanan end bundel_pelayanan,no_urut_pelayanan from (
                                SELECT  TO_CHAR(CURRENT_DATE,'YYYY') TAHUN ,   CASE WHEN  MAX( TO_NUMBER(NO_URUT_PELAYANAN)) < 200 THEN   MAX( TO_NUMBER(BUNDEL_PELAYANAN))  ELSE MAX( TO_NUMBER(BUNDEL_PELAYANAN))+1 END   BUNDEL_PELAYANAN , CASE WHEN   MAX( TO_NUMBER(NO_URUT_PELAYANAN)) <200 THEN   MAX( TO_NUMBER(NO_URUT_PELAYANAN)) +1 ELSE 1 END  NO_URUT_PELAYANAN
                                                                FROM P_SKNJOP 
                                                                WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY')
                                                                AND BUNDEL_PELAYANAN IN (SELECT  MAX( TO_NUMBER(BUNDEL_PELAYANAN)) BUNDEL_PELAYANAN
                                                                FROM P_SKNJOP WHERE THN_PELAYANAN=TO_CHAR(CURRENT_DATE,'YYYY'))
                                )")->row();
        return array(
            'tahun' => $rk->TAHUN,
            'bundel' => sprintf('%04s', $rk->BUNDEL_PELAYANAN),
            'no_urut' => sprintf('%03s', $rk->NO_URUT_PELAYANAN)
        );
    }

    function getDok_Ketlampiran($id)
    {
        return $this->db->query("SELECT  L_PERMOHONAN,L_SURAT_KUASA,L_KTP_WP,L_SERTIFIKAT_TANAH,L_SPPT,L_IMB,L_AKTE_JUAL_BELI,L_SK_PENSIUN,L_SPPT_STTS,L_STTS,L_SK_PENGURANGAN,L_SK_KEBERATAN,L_SKKP_PBB,L_SPMKP_PBB,L_LAIN_LAIN,L_SKET_TANAH,L_SKET_LURAH,L_NPWPD,L_PENGHASILAN,L_CAGAR,L_SPOP_LSPOP,
                                    L_BAHASA,
                                    L_BPD,
                                    L_SPPT_T,
                                    L_WAKTU,
                                    L_TUNGGAKAN,
                                    L_SKD_PO,
                                    L_WP,
                                    L_SK_PENDUKUNG,L_PERSENTASE,
                                    L_BANDING
                                 FROM PST_LAMPIRAN a
                                 left join p_lampiran b on a.THN_PELAYANAN=b.THN_PELAYANAN
                                 and a.BUNDEL_PELAYANAN=b.BUNDEL_PELAYANAN
                                 and a.NO_URUT_PELAYANAN=b.NO_URUT_PELAYANAN
                                WHERE a.THN_PELAYANAN|| '.'|| a.BUNDEL_PELAYANAN|| '.'|| a.NO_URUT_PELAYANAN='$id'")->row();
    }
    function jsonmonitoring_dokumen($id = "")
    {
        $this->datatables->select("ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,STATUS,TGL,NM_KECAMATAN,NM_KELURAHAN,NAMA_GROUP");
        $this->datatables->from("V_DETAIL_MONITORING");
        $this->datatables->where("KODE_GROUP='$id'");
        /* $this->datatables->add_column('action',  anchor(site_url('permohonan/detailmonitoring/$1'),'<i class="fa fa-share-alt"></i>','data-toggle="tooltip" title="Pelimpahan  " class="btn btn-xs btn-primary"'),'ID');
        $this->datatables->add_column('detail',  anchor(site_url('permohonan/detaildua/$1'),'<i class="fa fa-binoculars"></i>','data-toggle="tooltip" title="detail" class="btn btn-xs btn-success"'),'NO_LAYANAN');*/
        return $this->datatables->generate();
    }


    function getSKnjop($nopel)
    {

        $res = $this->db->query("SELECT B.*,to_char(TANGGAL_SK,'dd/mm/yyyy') TANGGAL_SK,NOMOR_SK,NAMA_PEGAWAI,JABATAN,NIP ,A.KD_PROPINSI||'.'||A.KD_DATI2||'.'||A.KD_KECAMATAN||'.'||A.KD_KELURAHAN||'.'||A.KD_BLOK||'.'||A.NO_URUT||'.'||A.KD_JNS_OP NOP,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,NPWP,get_kelurahan(b.kd_kelurahan,b.kd_kecamatan) nm_kel,get_kecamatan(b.kd_kecamatan) nm_kec,jalan_OP,RT_OP,RW_OP
                                FROM P_SKNJOP A
                                 JOIN (select * from sppt where thn_pajak_sppt= to_char( sysdate,'yyyy')) B ON A.KD_PROPINSI=B.KD_PROPINSI AND
                                A.KD_DATI2=B.KD_DATI2 AND
                                A.KD_KECAMATAN=B.KD_KECAMATAN AND
                                A.KD_KELURAHAN=B.KD_KELURAHAN AND
                                A.KD_BLOK=B.KD_BLOK AND
                                A.NO_URUT=B.NO_URUT AND
                                A.KD_JNS_OP=B.KD_JNS_OP  
                               JOIN DAT_SUBJEK_PAJAK C ON B.nm_wp_sppt=C.nm_wp
                               join dat_objek_pajak d on A.KD_PROPINSI=d.KD_PROPINSI AND
                                A.KD_DATI2=d.KD_DATI2 AND
                                A.KD_KECAMATAN=d.KD_KECAMATAN AND
                                A.KD_KELURAHAN=d.KD_KELURAHAN AND
                                A.KD_BLOK=d.KD_BLOK AND
                                A.NO_URUT=d.NO_URUT AND
                                A.KD_JNS_OP=d.KD_JNS_OP
                               WHERE THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN='$nopel'")->result();
        return $res;
    }


    function jsonuntuksk($data = array())
    {
        $NM_PENGAJUAN = $data['var'];
        $KD_PENGAJUAN = $data['kode'];
        $tahun = $data['tahun'];
        $this->datatables->select("ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,NM_PENGAJUAN,KD_PENGAJUAN,NOMOR_SK");
        $this->datatables->from("PV_DATA_SK");
        $this->datatables->where("ACC='1'");
        $this->datatables->where("TAHUN='$tahun'");
        $this->datatables->where("NM_PENGAJUAN='$NM_PENGAJUAN'");
        $this->datatables->where("KD_PENGAJUAN='$KD_PENGAJUAN'");

        return $this->datatables->generate();
    }
}

/* End of file Mpermohonan.php */
/* Location: ./application/models/Mpermohonan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-29 08:09:04 */
/* http://harviacode.com */
