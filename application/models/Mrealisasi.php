<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mrealisasi extends CI_Model
{

    public $table = 'REF_BANK';
    public $id = 'ID';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(true);
    }

    // datatables
    function json()
    {
        $this->datatables->select('ID,NAMA_BANK,KODE_BANK');
        $this->datatables->from('REF_BANK');
        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('bank/update/$1'), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') . anchor(site_url('bank/delete/$1'), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"') . '</div>', 'ID');
        return $this->datatables->generate();
    }


    function jsonRealisasiNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $tb = $this->queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku);
        $this->datatables->select("NOP,ALAMAT_OP,RTRW_OP,LUAS_BUMI_SPPT,LUAS_BNG_SPPT,NM_WP_SPPT,ALAMAT_WP,RTRW_WP,KELURAHAN_WP_SPPT,KOTA_WP_SPPT,PBB,BUKU");
        $this->datatables->from("( $tb )");

        if ($buku != null) {
            $this->db->where('BUKU', $buku);
        }

        return $this->datatables->generate();
    }

    function queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku = null)
    {


        return $tb = "SELECT    A.KD_PROPINSI || '.'|| A.KD_DATI2 || '.'|| A.KD_KECAMATAN || '.'|| A.KD_KELURAHAN || '.'|| A.KD_BLOK || '-'|| A.NO_URUT || '.'|| A.KD_JNS_OP NOP, GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN) ALAMAT_OP, RT_OP || '/' || RW_OP RTRW_OP, LUAS_BUMI_SPPT, LUAS_BNG_SPPT, NM_WP_SPPT, JLN_WP_SPPT || ' ' || BLOK_KAV_NO_WP_SPPT ALAMAT_WP, RT_WP_SPPT || '/' || RW_WP_SPPT RTRW_WP, KELURAHAN_WP_SPPT, KOTA_WP_SPPT, PBB_YG_HARUS_DIBAYAR_SPPT PBB, CASE WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT <= 100000 THEN 'Buku I'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 100000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 500000 THEN 'Buku II'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 500000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 2000000 THEN 'Buku III'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 2000000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 5000000 THEN 'Buku IV'ELSE 'Buku V'END BUKU, THN_PAJAK_SPPT 
        FROM    SPPT A
          JOIN DAT_OBJEK_PAJAK B ON     A.KD_PROPINSI = B.KD_PROPINSI AND A.KD_DATI2 = B.KD_DATI2 AND A.KD_KECAMATAN = B.KD_KECAMATAN AND A.KD_KELURAHAN = B.KD_KELURAHAN AND A.KD_BLOK = B.KD_BLOK AND A.NO_URUT = B.NO_URUT AND A.KD_JNS_OP = B.KD_JNS_OP where thn_pajak_sppt ='$tahun' and a.kd_kecamatan='$kd_kecamatan' and a.kd_kelurahan ='$kd_kelurahan' ";
    }


    function getDataNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $tb = $this->queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan);
        if ($buku != null) {
            $this->db->where('BUKU', $buku);
        }
        return $this->db->get("(" . $tb . ")")->result();
    }

    function jsondesalunas()
    {

        $tb = "SELECT  ID,NM_KECAMATAN,NM_KELURAHAN,TAHUN_PAJAK,BAKU,to_char(tgl_lunas,'dd-mm-yyyy') TGL_LUNAS
FROM DESA_LUNAS A
JOIN REF_KECAMATAN B ON B.KD_KECAMATAN=A.KD_KECAMATAN
JOIN REF_KELURAHAN C ON C.KD_KELURAHAN=A.KD_KELURAHAN AND C.KD_KECAMATAN=A.KD_KECAMATAN
ORDER BY TGL_LUNAS ASC";

        $this->datatables->select("ID,NM_KECAMATAN,NM_KELURAHAN,TAHUN_PAJAK,BAKU,TGL_LUNAS");
        $this->datatables->from("( $tb )");

        return $this->datatables->generate();
    }


    function jsonrealisasiPokok()
    {
        $this->datatables->select("BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,PERSEN,SISA");
        $this->datatables->from('(select BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,round(persen) PERSEN,SISA from MV_REALISASI)');
        return  $this->datatables->generate();
    }


    function baku($tahun, $kec = null, $kel = null)
    {


        $wh = "";
        if ($kec <> '' && $kel == '') {
            $wh = " where b.kd_kecamatan='$kec'";
        } else if ($kec <> '' && $kel <> '') {
            $wh = " where b.kd_kecamatan='$kec' and b.kd_kelurahan='$kel'";
        }

        $sql = "SELECT a.kd_kecamatan,
         a.nm_kecamatan,
         b.kd_kelurahan,
         b.nm_kelurahan,
         b.buku,
         baku,
         sppt,
         realisasi,
         sppt_realisasi,
         piutang,
         sppt_piutang
        FROM ref_kecamatan a
         JOIN buku_kelurahan b
            ON     a.kd_kecamatan = b.kd_kecamatan
         left JOIN
         (  SELECT kd_kecamatan,
                   kd_kelurahan,
                   buku,
                   SUM (baku) baku,
                   SUM (sppt) sppt,
                   SUM (realisasi) realisasi,
                   SUM (sppt_realisasi) sppt_realisasi,
                   SUM (piutang) piutang,
                   SUM (sppt_piutang) sppt_piutang
              FROM (  SELECT kd_kecamatan,
                             kd_kelurahan,
                             buku (pbb_yg_harus_dibayar_sppt) BUKU,
                             SUM (pbb_yg_harus_dibayar_sppt) baku,
                             COUNT (1) sppt,
                             CASE
                                WHEN status_pembayaran_sppt = 1
                                THEN
                                   SUM (pbb_yg_harus_dibayar_sppt)
                                ELSE
                                   0
                             END
                                realisasi,
                             CASE
                                WHEN status_pembayaran_sppt = 1 THEN COUNT (1)
                                ELSE 0
                             END
                                sppt_realisasi,
                             CASE
                                WHEN status_pembayaran_sppt <> 1
                                THEN
                                   SUM (pbb_yg_harus_dibayar_sppt)
                                ELSE
                                   0
                             END
                                piutang,
                             CASE
                                WHEN status_pembayaran_sppt <> 1 THEN COUNT (1)
                                ELSE 0
                             END
                                sppt_piutang
                        FROM sppt
                       WHERE     thn_pajak_sppt = $tahun
                             AND status_pembayaran_sppt <> 3
                    GROUP BY kd_kecamatan,
                             kd_kelurahan,
                             buku (pbb_yg_harus_dibayar_sppt),
                             status_pembayaran_sppt)
          GROUP BY kd_kecamatan, kd_kelurahan, buku) c
            ON     c.kd_kecamatan = b.KD_KECAMATAN
               AND c.kd_kelurahan = b.kd_kelurahan
               and c.buku=b.kode_buku " . $wh;

        return $this->db->query($sql)->result();
    }

    // data pembayaran bank
    public function PembayaranBank($tahun, $bulan, $bank, $q = null)
    {
        $pbb = $this->load->database('pbb', TRUE);
        // $pbb->where("tgl_pembayaran_sppt >=TO_DATE ('$tahun' || '0101', 'yyyy' || 'MMDD') AND tgl_pembayaran_sppt < LAST_DAY ( TO_DATE ('$tahun' || '1201', 'yyyy' || 'MMDD')) + 1 AND TO_CHAR (TGL_PEMBAYARAN_SPPT, 'mm')='$bulan'", null, false);
        $pbb->limit(5);
        return $pbb->get('v_pembayaran_bank')->result_array();


        // return $query;
    }


    public function bayar_total_rows($tahun, $bulan, $kd_bank)
    {
        $pbb = $this->load->database('pbb', TRUE);
        // $pbb->limit($limit, $start);
        $wh_ = "A.TGL_PEMBAYARAN_SPPT >=
          TO_DATE ('" . $tahun . "' || '" . $bulan . "01', 'yyyy' || 'MMDD')
        AND A.TGL_PEMBAYARAN_SPPT <
          LAST_DAY (TO_DATE ('" . $tahun . "' || '" . $bulan . "01', 'yyyy' || 'MMDD')) + 1 and";

        if ($kd_bank <> '') {
            $wh_ .= " KODE_BANK_BAYAR='" . $kd_bank . "' and";
        }

        if ($wh_ <> '') {
            $wh_ = substr($wh_, 0, -3);
        }
        
        $pbb->select(" A.KD_PROPINSI, A.KD_DATI2, A.KD_KECAMATAN, A.KD_KELURAHAN, A.KD_BLOK, A.NO_URUT, A.KD_JNS_OP, B.NM_WP_SPPT, C.NM_KECAMATAN, D.NM_KELURAHAN, A.JML_SPPT_YG_DIBAYAR - A.DENDA_SPPT AS POKOK, A.DENDA_SPPT AS DENDA, A.JML_SPPT_YG_DIBAYAR AS TOTAL, A.TGL_PEMBAYARAN_SPPT, A.TGL_REKAM_BYR_SPPT, A.THN_PAJAK_SPPT, PENGESAHAN, NAMA_BANK", false);
        $pbb->join('SPPT B', ' A.KD_PROPINSI=B.KD_PROPINSI AND A.KD_DATI2=B.KD_DATI2 AND A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN AND A.KD_BLOK=B.KD_BLOK AND A.NO_URUT=B.NO_URUT AND A.KD_JNS_OP=B.KD_JNS_OP AND A.THN_PAJAK_SPPT=B.THN_PAJAK_SPPT');
        $pbb->join('REF_KECAMATAN C', 'A.KD_KECAMATAN = C.KD_KECAMATAN');
        $pbb->join('REF_KELURAHAN D', 'A.KD_KECAMATAN=D.KD_KECAMATAN AND A.KD_KELURAHAN=D.KD_KELURAHAN');
        $pbb->join('MV_PEMBAYARAN_SPPT_SPO E', 'A.KD_PROPINSI=E.KD_PROPINSI AND A.KD_DATI2=E.KD_DATI2 AND A.KD_KECAMATAN=E.KD_KECAMATAN AND A.KD_KELURAHAN=E.KD_KELURAHAN AND A.KD_BLOK=E.KD_BLOK AND A.NO_URUT=E.NO_URUT AND A.KD_JNS_OP=E.KD_JNS_OP AND A.THN_PAJAK_SPPT=E.THN_PAJAK_SPPT');
        $pbb->join('REF_BANK F', 'F.KODE_BANK = E.KODE_BANK_BAYAR');


        if ($wh_ <> '') {
            $pbb->where($wh_, null, false);
        }
        $pbb->from('PEMBAYARAN_SPPT A');
        return $pbb->count_all_results();
    }

    public function bayar_get_limit_data($limit, $start = 0, $tahun, $bulan, $kd_bank)
    {
        $pbb = $this->load->database('pbb', TRUE);
        $pbb->limit($limit, $start);
        $wh_ = "A.TGL_PEMBAYARAN_SPPT >=
          TO_DATE ('" . $tahun . "' || '" . $bulan . "01', 'yyyy' || 'MMDD')
        AND A.TGL_PEMBAYARAN_SPPT <
          LAST_DAY (TO_DATE ('" . $tahun . "' || '" . $bulan . "01', 'yyyy' || 'MMDD')) + 1 and";

        if ($kd_bank <> '') {
            $wh_ .= " KODE_BANK_BAYAR='" . $kd_bank . "' and";
        }

        if ($wh_ <> '') {
            $wh_ = substr($wh_, 0, -3);
        }
        
        $pbb->select(" A.KD_PROPINSI, A.KD_DATI2, A.KD_KECAMATAN, A.KD_KELURAHAN, A.KD_BLOK, A.NO_URUT, A.KD_JNS_OP, B.NM_WP_SPPT, C.NM_KECAMATAN, D.NM_KELURAHAN, A.JML_SPPT_YG_DIBAYAR - A.DENDA_SPPT AS POKOK, A.DENDA_SPPT AS DENDA, A.JML_SPPT_YG_DIBAYAR AS TOTAL, A.TGL_PEMBAYARAN_SPPT, A.TGL_REKAM_BYR_SPPT, A.THN_PAJAK_SPPT, PENGESAHAN, NAMA_BANK", false);

        $pbb->join('SPPT B', ' A.KD_PROPINSI=B.KD_PROPINSI AND A.KD_DATI2=B.KD_DATI2 AND A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN AND A.KD_BLOK=B.KD_BLOK AND A.NO_URUT=B.NO_URUT AND A.KD_JNS_OP=B.KD_JNS_OP AND A.THN_PAJAK_SPPT=B.THN_PAJAK_SPPT');
        $pbb->join('REF_KECAMATAN C', 'A.KD_KECAMATAN = C.KD_KECAMATAN');
        $pbb->join('REF_KELURAHAN D', 'A.KD_KECAMATAN=D.KD_KECAMATAN AND A.KD_KELURAHAN=D.KD_KELURAHAN');
        $pbb->join('MV_PEMBAYARAN_SPPT_SPO E', 'A.KD_PROPINSI=E.KD_PROPINSI AND A.KD_DATI2=E.KD_DATI2 AND A.KD_KECAMATAN=E.KD_KECAMATAN AND A.KD_KELURAHAN=E.KD_KELURAHAN AND A.KD_BLOK=E.KD_BLOK AND A.NO_URUT=E.NO_URUT AND A.KD_JNS_OP=E.KD_JNS_OP AND A.THN_PAJAK_SPPT=E.THN_PAJAK_SPPT');
        $pbb->join('REF_BANK F', 'F.KODE_BANK = E.KODE_BANK_BAYAR');


        if ($wh_ <> '') {
            $pbb->where($wh_, null, false);
        }
        return $pbb->get('PEMBAYARAN_SPPT A')->result();
        /* $pbb->select("ID, TANGGAL,NOMOR,SIFAT,PERIHAL,JUM,DISPOSISI,PROSES_KASI,PROSES_KABID,PROSES_KABAN");
        $pbb->order_by("ID", 'DESC');
        return $pbb->get("(SELECT A.ID,DISPOSISI,PROSES_KASI,PROSES_KABID,PROSES_KABAN,TO_CHAR(TANGGAL,'yyyy-mm-dd') TANGGAL,NOMOR,SIFAT,PERIHAL,COUNT(1) JUM
        FROM NOTADINAS A
        JOIN NOTADINAS_SSPD B ON A.ID=B.NOTADINAS_ID
        GROUP BY A.ID,DISPOSISI,PROSES_KASI,PROSES_KABID,PROSES_KABAN,TO_CHAR(TANGGAL,'yyyy-mm-dd') ,NOMOR,SIFAT,PERIHAL)")->result(); */
    }
}
