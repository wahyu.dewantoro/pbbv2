<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mrekon extends CI_Model
{

    public $table = 'EXCEL_PEMBAYARAN';
    // public $id = 'KODE_GROUP';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
$this->output->enable_profiler(false);
    }


    // get total rows
    function total_rows($q = NULL)
    {
        if ($q <> null) {
            $q = '%' . strtolower($q) . '%';
            $this->db->where("( lower(kode_bank) like '" . $q . "' or lower(nop) like '" . $q . "' or lower(tahun_pajak) like '" . $q . "' or lower(nama) like '" . $q . "' )", null, false);
        }
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        if ($q <> null) {
            $q = '%' . strtolower($q) . '%';
            $this->db->where("( lower(kode_bank) like '" . $q . "' or lower(nop) like '" . $q . "' or lower(tahun_pajak) like '" . $q . "' or lower(nama) like '" . $q . "' )", null, false);
        }
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    public function rekapBayarGlobal($tahun, $bank = null)
    {
        $gb = "";
        $and = "";
        $select = " count(1) sppt_bayar,sum(a.jumlah) jumlah_bayar, sum(case when b.jumlah is null then 0 else 1 end ) rekon,sum(b.jumlah) jumlah_rekon ";
        if ($bank != null) {
            $and = " and kode_bank_bayar='$bank' ";
            $gb = " group by a.kode_bank ";
            $select = " a.kode_bank,count(1) sppt_bayar,sum(a.jumlah) jumlah_bayar, sum(case when b.jumlah is null then 0 else 1 end ) rekon,sum(b.jumlah) jumlah_rekon ";
        }

        return $this->db->query("SELECT $select
        from (
        SELECT kode_bank_bayar kode_bank,a.kd_propinsi,
               a.kd_dati2,
               a.kd_kecamatan,
               a.kd_kelurahan,
               a.kd_blok,
               a.no_urut,
               a.kd_jns_op,
               a.thn_pajak_sppt,
               a.jml_sppt_yg_dibayar - a.denda_sppt pokok,
               a.denda_sppt denda,
               a.jml_sppt_yg_dibayar jumlah,
               to_date(to_char(a.tgl_pembayaran_sppt,'yyyymmdd'),'yyyymmdd') tanggal
          FROM pembayaran_sppt@to17 a
               JOIN spo.pembayaran_sppt b
                  ON     a.kd_propinsi = b.kd_propinsi
                     AND a.kd_dati2 = b.kd_dati2
                     AND a.kd_kecamatan = b.kd_kecamatan
                     AND a.kd_kelurahan = b.kd_kelurahan
                     AND a.kd_blok = b.kd_blok
                     AND a.no_urut = b.no_urut
                     AND a.kd_jns_op = b.kd_jns_op
                     AND a.thn_pajak_sppt = b.thn_pajak_sppt
                     where  to_char(A.tgl_pembayaran_sppt,'yyyy')='$tahun'
                       $and ) a
            left join data_pembayaran_excel b 
        on a.kd_propinsi=b.kd_propinsi
        and a.kd_dati2=b.kd_dati2
        and a.kd_kecamatan=b.kd_kecamatan
        and a.kd_kelurahan=b.kd_kelurahan
        and a.kd_blok=b.kd_blok
        and a.no_urut=b.no_urut
        and a.kd_jns_op=b.kd_jns_op
        and a.thn_pajak_sppt=b.tahun_pajak
        and a.tanggal=b.tanggal
        and a.kode_bank=b.kode_bank
        " . $gb)->row();
    }

    public function RekapBayarHarian($tahun, $bulan, $bank)
    {
        return $this->db->query("SELECT a.tanggal,count(1) sppt_bayar,sum(a.jumlah) jumlah_bayar, sum(case when b.jumlah is null then 0 else 1 end ) rekon,sum(b.jumlah) jumlah_rekon
        from (
        SELECT kode_bank_bayar kode_bank,a.kd_propinsi,
               a.kd_dati2,
               a.kd_kecamatan,
               a.kd_kelurahan,
               a.kd_blok,
               a.no_urut,
               a.kd_jns_op,
               a.thn_pajak_sppt,
               a.jml_sppt_yg_dibayar - a.denda_sppt pokok,
               a.denda_sppt denda,
               a.jml_sppt_yg_dibayar jumlah,
               to_date(to_char(a.tgl_pembayaran_sppt,'yyyymmdd'),'yyyymmdd') tanggal
          FROM pembayaran_sppt@to17 a
               JOIN spo.pembayaran_sppt b
                  ON     a.kd_propinsi = b.kd_propinsi
                     AND a.kd_dati2 = b.kd_dati2
                     AND a.kd_kecamatan = b.kd_kecamatan
                     AND a.kd_kelurahan = b.kd_kelurahan
                     AND a.kd_blok = b.kd_blok
                     AND a.no_urut = b.no_urut
                     AND a.kd_jns_op = b.kd_jns_op
                     AND a.thn_pajak_sppt = b.thn_pajak_sppt
                     where  to_char(A.tgl_pembayaran_sppt,'yyyy')='$tahun'
                    and to_char(A.tgl_pembayaran_sppt,'mm')='$bulan'
            and kode_bank_bayar='$bank' ) a
            left join data_pembayaran_excel b 
        on a.kd_propinsi=b.kd_propinsi
        and a.kd_dati2=b.kd_dati2
        and a.kd_kecamatan=b.kd_kecamatan
        and a.kd_kelurahan=b.kd_kelurahan
        and a.kd_blok=b.kd_blok
        and a.no_urut=b.no_urut
        and a.kd_jns_op=b.kd_jns_op
        and a.thn_pajak_sppt=b.tahun_pajak
        and a.tanggal=b.tanggal
        and a.kode_bank=b.kode_bank
        group by a.tanggal
        order by a.tanggal asc")->result_array();
    }


    /* 
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    } */
}
