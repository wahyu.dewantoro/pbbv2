<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msppt extends CI_Model
{



    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
    }

    function getRIwayatbyNopBelumLunas($nop = array(), $batal = null)
    {
        $KD_PROPINSI  = $nop['KD_PROPINSI'];
        $KD_DATI2     = $nop['KD_DATI2'];
        $KD_KECAMATAN = $nop['KD_KECAMATAN'];
        $KD_KELURAHAN = $nop['KD_KELURAHAN'];
        $KD_BLOK      = $nop['KD_BLOK'];
        $NO_URUT      = $nop['NO_URUT'];
        $KD_JNS_OP    = $nop['KD_JNS_OP'];
        $cetak        = $nop['cetak'];
        $and = ' ';
        // if ($cetak == true) {
        $and .= "AND STATUS_PEMBAYARAN_SPPT=0 AND THN_PAJAK_SPPT >=2003";
        // }
        $res = $this->db->query("SELECT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,get_denda(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt, case when STATUS_PEMBAYARAN_SPPT= 0 then sysdate else TGL_PEMBAYARAN_SPPT end ) denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
                            FROM (
                                SELECT * FROM SPPT
                                WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   
                                $and
                                ) SPT
                                left join  PEMBAYARAN_SPPT PS
                                on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
                                AND SPT.KD_BLOK =  PS.KD_BLOK
                                AND SPT.NO_URUT =  PS.NO_URUT
                                AND SPT.KD_JNS_OP = PS.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT
                                and ps.tgl_pembayaran_sppt in (
                                    select max(tgl_pembayaran_sppt)
                                    from pembayaran_sppt  
                                    WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   
                                group by thn_pajak_sppt)
                                 order by SPT.THN_PAJAK_SPPT asc")->result();
        return $res;
    }

    function getRIwayatbyNop($nop = array(), $batal = null)
    {
        $KD_PROPINSI  = $nop['KD_PROPINSI'];
        $KD_DATI2     = $nop['KD_DATI2'];
        $KD_KECAMATAN = $nop['KD_KECAMATAN'];
        $KD_KELURAHAN = $nop['KD_KELURAHAN'];
        $KD_BLOK      = $nop['KD_BLOK'];
        $NO_URUT      = $nop['NO_URUT'];
        $KD_JNS_OP    = $nop['KD_JNS_OP'];
        $cetak        = $nop['cetak'];
        $and = ' ';
        if ($cetak == true) {
            $and .= " AND THN_PAJAK_SPPT >=2003";
        }
        $res = $this->db->query("SELECT SPT.KD_PROPINSI,
        SPT.KD_DATI2,
        SPT.KD_KECAMATAN,
        SPT.KD_KELURAHAN,
        SPT.KD_BLOK,
        SPT.NO_URUT,
        SPT.KD_JNS_OP,
           SPT.KD_PROPINSI
        || '.'
        || SPT.KD_DATI2
        || '.'
        || SPT.KD_KECAMATAN
        || '.'
        || SPT.KD_KELURAHAN
        || '.'
        || SPT.KD_BLOK
        || '-'
        || SPT.NO_URUT
        || '.'
        || SPT.KD_JNS_OP
           NOP,
        NM_WP_SPPT,
        'RT ' || RT_WP_SPPT || 'RW ' || RW_WP_SPPT || KELURAHAN_WP_SPPT ALAMAT,
        NJOP_BUMI_SPPT,
        NJOP_BNG_SPPT,
        CASE
            WHEN STATUS_PEMBAYARAN_SPPT = 0 THEN spt.pbb_yg_harus_dibayar_sppt
            ELSE 
            case when jml_sppt_yg_dibayar is null then spt.pbb_yg_harus_dibayar_sppt else jml_sppt_yg_dibayar end
         END
           PBB_YG_HARUS_DIBAYAR_SPPT,
              case when STATUS_PEMBAYARAN_SPPT=1 then 
               denda_sppt
              else 
                   case when spt.thn_pajak_sppt >=2003 and spt.thn_pajak_sppt<=2020 then 0 else    
                 get_denda@to17(
           spt.kd_dati2,
           spt.kd_kecamatan,
           spt.kd_kelurahan,
           spt.kd_blok,
           spt.no_urut,
           spt.kd_jns_op,
           spt.thn_pajak_sppt,
           pbb_yg_harus_dibayar_sppt,
           tgl_jatuh_tempo_sppt,
           CASE
              WHEN STATUS_PEMBAYARAN_SPPT = 0 THEN SYSDATE
              ELSE TGL_PEMBAYARAN_SPPT
           END)  
                   end
              end
           denda,
        STATUS_PEMBAYARAN_SPPT,
        TO_CHAR (TGL_PEMBAYARAN_SPPT, 'dd/mm/yyyy') TGL_BAYAR,
        SPT.THN_PAJAK_SPPT,
        UNFLAG_DESC,
        KOBIL
                            FROM (
                                SELECT * FROM SPPT
                                WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   
                                $and
                                ) SPT
                                left join UNFLAG_HISTORY@to17 uf
                                on  SPT.KD_KECAMATAN = uf.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = uf.KD_KELURAHAN
                                AND SPT.KD_BLOK =  uf.KD_BLOK
                                AND SPT.NO_URUT =  uf.NO_URUT
                                AND SPT.KD_JNS_OP = uf.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = uf.THN_PAJAK_SPPT 
                                left join  PEMBAYARAN_SPPT PS
                                on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
                                AND SPT.KD_BLOK =  PS.KD_BLOK
                                AND SPT.NO_URUT =  PS.NO_URUT
                                AND SPT.KD_JNS_OP = PS.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT 
                                left join  (SELECT BK.KD_KECAMATAN, BK.KD_KELURAHAN, BK.KD_BLOK, BK.NO_URUT, BK.KD_JNS_OP, BK.THN_PAJAK_SPPT, VD.KOBIL FROM BILLING_KOLEKTIF BK 
                                LEFT JOIN V_DAFNOM VD ON BK.DATA_BILLING_ID=VD.ID) BK
                                on  SPT.KD_KECAMATAN = BK.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = BK.KD_KELURAHAN
                                AND SPT.KD_BLOK =  BK.KD_BLOK
                                AND SPT.NO_URUT =  BK.NO_URUT
                                AND SPT.KD_JNS_OP = BK.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = BK.THN_PAJAK_SPPT 
                                where spt.THN_PAJAK_SPPT >=2003
                                order by SPT.THN_PAJAK_SPPT DESC")->result();

        /* and ps.tgl_pembayaran_sppt in (
    select max(tgl_pembayaran_sppt)
    from pembayaran_sppt  
    WHERE KD_PROPINSI = '$KD_PROPINSI'
AND KD_DATI2      = '$KD_DATI2'
AND KD_KECAMATAN  = '$KD_KECAMATAN'
AND KD_KELURAHAN  = '$KD_KELURAHAN'
AND KD_BLOK       = '$KD_BLOK'
AND NO_URUT       = '$NO_URUT'
AND KD_JNS_OP     ='$KD_JNS_OP'   
group by thn_pajak_sppt) */
        return $res;
    }

    function getRIwayatbyNopSistep($nop = array(), $batal = null)
    {
        $KD_PROPINSI  = $nop['KD_PROPINSI'];
        $KD_DATI2     = $nop['KD_DATI2'];
        $KD_KECAMATAN = $nop['KD_KECAMATAN'];
        $KD_KELURAHAN = $nop['KD_KELURAHAN'];
        $KD_BLOK      = $nop['KD_BLOK'];
        $NO_URUT      = $nop['NO_URUT'];
        $KD_JNS_OP    = $nop['KD_JNS_OP'];
        $cetak        = $nop['cetak'];
        $and = ' ';
        if ($cetak == true) {
            $and .= "AND STATUS_PEMBAYARAN_SPPT<>3 ";
        }
        $res = $this->db->query("SELECT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,get_denda(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt, case when STATUS_PEMBAYARAN_SPPT= 0 then sysdate else TGL_PEMBAYARAN_SPPT end ) denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
                            FROM (
                                SELECT * FROM SPPT_SISTEP@to17
                                WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   

                                $and
                                ) SPT
                                left join  PEMBAYARAN_SPPT PS
                                on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
                                AND SPT.KD_BLOK =  PS.KD_BLOK
                                AND SPT.NO_URUT =  PS.NO_URUT
                                AND SPT.KD_JNS_OP = PS.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT order by SPT.THN_PAJAK_SPPT asc")->result();
        return $res;
    }


    function jsonpencarian($kode_kec, $kode_kel, $nama = null)
    {
        $nama = strtolower($nama);
        $this->datatables->select("THN_PAJAK_SPPT,
                                NOP,
                                NM_WP_SPPT,
                                ALAMAT,
                                LUAS_BUMI_SPPT,
                                LUAS_BNG_SPPT,
                                NJOP_BUMI_SPPT,
                                NJOP_BNG_SPPT,
                                PBB_YG_HARUS_DIBAYAR_SPPT");
        $this->datatables->from("( SELECT THN_PAJAK_SPPT,KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'|| KD_KELURAHAN||'.'||KD_BLOK||'-'||NO_URUT||'.'||KD_JNS_OP NOP,NM_WP_SPPT,JLN_WP_SPPT||' '||BLOK_KAV_NO_WP_SPPT||' RW '||RW_WP_SPPT||' RT '||RT_WP_SPPT||' '||KELURAHAN_WP_SPPT||' '||KOTA_WP_SPPT ALAMAT,LUAS_BUMI_SPPT,LUAS_BNG_SPPT,NJOP_BUMI_SPPT,NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT  
                                FROM SPPT  WHERE kd_kecamatan='$kode_kec' and kd_kelurahan='$kode_kel' and lower(nm_wp_sppt) like '%$nama%')");
        return $this->datatables->generate();
    }

    function pelunasanSppt($data = [])
    {
        $nop = $data['nop'];
        $tahun = $data['tahun'];
        $datetime = $data['tanggal'];
        $jml_sppt_yg_dibayar = $data['jumlah'];
        $denda = $data['denda'];

        try {
            $this->db->trans_begin();
            $cek = $this->db->query("SELECT STATUS_PEMBAYARAN_SPPT FROM SPPT@to17
                                    WHERE     thn_pajak_sppt =$tahun
                                AND kd_propinsi = SUBSTR ('$nop', 1, 2)
                                AND kd_dati2 = SUBSTR ('$nop', 3, 2)
                                AND kd_kecamatan = SUBSTR ('$nop', 5, 3)
                                AND kd_kelurahan = SUBSTR ('$nop', 8, 3)
                                AND kd_blok = SUBSTR ('$nop', 11, 3)
                                AND no_urut = SUBSTR ('$nop', 14, 4)
                                AND kd_jns_op = SUBSTR ('$nop', 18, 1)")->row();

            if ($cek) {
                if ($cek->STATUS_PEMBAYARAN_SPPT == 0) {
                    $this->db->query("INSERT INTO PEMBAYARAN_SPPT@to17 (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE,KD_KANWIL,KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT)
                                            VALUES ( substr('$nop',1,2),substr('$nop',3,2),substr('$nop',5,3),substr('$nop',8,3),substr('$nop',11,3),substr('$nop',14,4),substr('$nop',18,1), '$tahun', '1','01','01' , '04', '$denda', '$jml_sppt_yg_dibayar', to_date('$datetime','yyyymmddhh24:mi:ss'), sysdate, '060000000000000000')");
                    $this->db->query("UPDATE  sppt@to17 set status_pembayaran_sppt=1 where thn_pajak_sppt='$tahun'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1)");
                    $res = 1;
                } else {
                    $res = 2;
                }
            } else {
                $res = 0;
            }



            if ($this->db->trans_status() === FALSE) {
                $r = 0;
                $this->db->trans_rollback();
            } else {
                $r = $res;
                $this->db->trans_commit();
            }
        } catch (Exception $e) {

            $r = 0;
        }

        return $r;
    }


    // paging piutang
    public function dat_objek_pajak()
    {
    }

    public function core_piutang($tahun)
    {

        $query =  $this->db->from('SPPT')
            ->join('DAT_OBJEK_PAJAK', ' SPPT.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
                                                AND SPPT.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
                                                AND SPPT.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
                                                AND SPPT.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
                                                AND SPPT.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK
                                                AND SPPT.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT
                                                AND SPPT.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP')
            ->join('REF_KECAMATAN', 'REF_KECAMATAN.KD_PROPINSI = SPPT.KD_PROPINSI
        AND REF_KECAMATAN.KD_DATI2 = SPPT.KD_DATI2
        AND REF_KECAMATAN.KD_KECAMATAN = SPPT.KD_KECAMATAN')
            ->join('REF_KELURAHAN', 'REF_KELURAHAN.KD_PROPINSI = SPPT.KD_PROPINSI
        AND REF_KELURAHAN.KD_DATI2 = SPPT.KD_DATI2
        AND REF_KELURAHAN.KD_KECAMATAN = SPPT.KD_KECAMATAN
        AND REF_KELURAHAN.KD_KELURAHAN = SPPT.KD_KELURAHAN')->where(" SPPT.THN_PAJAK_SPPT = '$tahun'
        AND SPPT.STATUS_PEMBAYARAN_SPPT = '0'", "", false);

        // $query = $this->db->get();
        return $query;
    }


    public function piutang_count($tahun)
    {
        $this->core_piutang($tahun);
        return $this->db->count_all_results();
    }

    public function piutang_data($tahun, $limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->core_piutang($tahun);
        $this->db->select(" SPPT.KD_PROPINSI,
         SPPT.KD_DATI2,
         SPPT.KD_KECAMATAN,
         SPPT.KD_KELURAHAN,
         SPPT.KD_BLOK,
         SPPT.NO_URUT,
         SPPT.KD_JNS_OP,
        SPPT.NM_WP_SPPT AS NAMA_WP,
           SPPT.JLN_WP_SPPT
        || ' '
        || BLOK_KAV_NO_WP_SPPT
        || ' rt.'
        || RW_WP_SPPT
        || ' rw.'
        || RT_WP_SPPT
        || ' Kel.'
        || KELURAHAN_WP_SPPT
        || ' '
        || KOTA_WP_SPPT
           AS ALAMAT_WP,
           DAT_OBJEK_PAJAK.JALAN_OP || ' ' || BLOK_KAV_NO_OP AS JALAN_OP,
           REF_KELURAHAN.NM_KELURAHAN AS KELURAHAN,
           REF_KECAMATAN.NM_KECAMATAN AS KECAMATAN,
        SPPT.THN_PAJAK_SPPT AS TAHUN_PAJAK,
        SPPT.PBB_YG_HARUS_DIBAYAR_SPPT AS BAYAR,
        SPPT.PBB_TERHUTANG_SPPT AS PIUTANG,
        SPPT.STATUS_PEMBAYARAN_SPPT AS STATUS", false);
        return $this->db->get()->result();
    }

    function piutang_excel($tahun)
    {
        $this->core_piutang($tahun);
        $this->db->select("SPPT.KD_PROPINSI||'.'||
         SPPT.KD_DATI2||'.'||
         SPPT.KD_KECAMATAN||'.'||
         SPPT.KD_KELURAHAN||'.'||
         SPPT.KD_BLOK||'.'||
         SPPT.NO_URUT||'.'||
         SPPT.KD_JNS_OP as NOP,
        SPPT.NM_WP_SPPT AS NAMA_WP,
           SPPT.JLN_WP_SPPT
        || ' '
        || BLOK_KAV_NO_WP_SPPT
        || ' rt.'
        || RW_WP_SPPT
        || ' rw.'
        || RT_WP_SPPT
        || ' Kel.'
        || KELURAHAN_WP_SPPT
        || ' '
        || KOTA_WP_SPPT
           AS ALAMAT_WP,
           DAT_OBJEK_PAJAK.JALAN_OP || ' ' || BLOK_KAV_NO_OP AS JALAN_OP,
           REF_KELURAHAN.NM_KELURAHAN AS KELURAHAN,
           REF_KECAMATAN.NM_KECAMATAN AS KECAMATAN,
        SPPT.THN_PAJAK_SPPT AS TAHUN_PAJAK,
        SPPT.PBB_YG_HARUS_DIBAYAR_SPPT AS BAYAR,
        SPPT.PBB_TERHUTANG_SPPT AS PIUTANG", false);
        // $this->db->limit(10, 1);
        return $this->db->get()->result_array();
    }
}
