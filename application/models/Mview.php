<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mview extends CI_Model
{

    function getSubjekPajak($nop = "")
    {
        $id_op = str_replace('.', '', $nop);
        $data = $this->db->query("SELECT * FROM dat_subjek_pajak a 
                                WHERE subjek_pajak_id='$id_op'")->row();

        return $data;
    }
    function getObjekPajak($nop = "")
    {
        $id_op = str_replace('.', '', $nop);
        $data = $this->db->query("SELECT a.KD_PROPINSI||'.'||a.KD_DATI2||'.'||a.KD_KECAMATAN||'.'||a.KD_KELURAHAN||'.'||a.KD_BLOK||'.'||a.NO_URUT||'.'||a.KD_JNS_OP as NOP,
                                a.SUBJEK_PAJAK_ID,a.NO_FORMULIR_SPOP,a.NO_PERSIL,a.JALAN_OP,a.BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,
                                KD_STATUS_WP,TOTAL_LUAS_BUMI,TOTAL_LUAS_BNG,NJOP_BUMI,NJOP_BNG,STATUS_PETA_OP,JNS_TRANSAKSI_OP,TGL_PENDATAAN_OP,
                                NIP_PENDATA,to_char(TGL_PEMERIKSAAN_OP,'d Mon YYYY' ) TGL_PEMERIKSAAN,NIP_PEMERIKSA_OP,to_char(TGL_PEREKAMAN_OP,'d Mon YYYY' ) TGL_PEREKAMAN,NIP_PEREKAM_OP,a.kd_kelurahan||' - '||b.nm_kelurahan kelurahan, a.kd_kecamatan||' - '||c.nm_kecamatan kecamatan
                                FROM dat_objek_pajak a 
                                JOIN ref_kelurahan b on a.kd_kelurahan=b.kd_kelurahan and a.kd_kecamatan=b.kd_kecamatan
                                JOIN ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan
                                WHERE subjek_pajak_id='$id_op'")->row();

        return $data;
    }
    function getDataBangunan($nop = "")
    {
        $exp = explode('.', $nop);
        $a = $exp[0];
        $b = $exp[1];
        $c = $exp[2];
        $d = $exp[3];
        $e = $exp[4];
        $f = $exp[5];
        $g = $exp[6];
        $data = $this->db->query("select * from dat_op_bangunan
                                where kd_propinsi='$a'
                                and kd_dati2='$b'
                                and kd_kecamatan='$c'
                                and kd_kelurahan='$d' 
                                and kd_blok='$e'
                                and no_urut='$f'
                                and kd_jns_op='$g'")->row();
        return $data;
    }
    function getSPPT($nop = "")
    {
        $exp = explode('.', $nop);
        $a = $exp[0];
        $b = $exp[1];
        $c = $exp[2];
        $d = $exp[3];
        $e = $exp[4];
        $f = $exp[5];
        $g = $exp[6];
        $data = $this->db->query("SELECT * from sppt
                                WHERE kd_propinsi='$a'
                                AND kd_dati2='$b'
                                AND kd_kecamatan='$c'
                                AND kd_kelurahan='$d' 
                                AND kd_blok='$e'
                                AND no_urut='$f'
                                AND kd_jns_op='$g'")->result();

        return $data;
    }

    function wp_getObjekPajak($nop = "")
    {
        $id_op = str_replace('.', '', $nop);
        $data = $this->db->query("SELECT a.KD_PROPINSI||'.'||a.KD_DATI2||'.'||a.KD_KECAMATAN||'.'||a.KD_KELURAHAN||'.'||a.KD_BLOK||'-'||a.NO_URUT||'.'||a.KD_JNS_OP as NOP,
                                a.SUBJEK_PAJAK_ID,a.NO_FORMULIR_SPOP,a.NO_PERSIL,a.JALAN_OP,a.BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,
                                KD_STATUS_WP,TOTAL_LUAS_BUMI,TOTAL_LUAS_BNG,NJOP_BUMI,NJOP_BNG,STATUS_PETA_OP,JNS_TRANSAKSI_OP,TGL_PENDATAAN_OP,
                                NIP_PENDATA,to_char(TGL_PEMERIKSAAN_OP,'d Mon YYYY' ) TGL_PEMERIKSAAN,NIP_PEMERIKSA_OP,to_char(TGL_PEREKAMAN_OP,'d Mon YYYY' ) TGL_PEREKAMAN,NIP_PEREKAM_OP,b.nm_kelurahan kelurahan,c.nm_kecamatan kecamatan
                                FROM dat_objek_pajak a 
                                JOIN ref_kelurahan b on a.kd_kelurahan=b.kd_kelurahan and a.kd_kecamatan=b.kd_kecamatan
                                JOIN ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan
                                WHERE subjek_pajak_id='$id_op'")->result();

        return $data;
    }
}
/*
    function getSPPT($nop=""){
        $data=$this->db->query("SELECT * from sppt
                                WHERE kd_propinsi='35'
                                AND kd_dati2='07'
                                AND kd_kecamatan='110'
                                AND kd_kelurahan='008' 
                                AND kd_blok='012'
                                AND no_urut='0226'
                                AND kd_jns_op='0'")->result();
        
        return $data;
*/
