<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PstPermohonan extends CI_Model
{

    public $table = 'PST_PERMOHONAN';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    // $this->output->enable_profiler(false)
    }

   

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table,false)->result();
    }

    function get()
    {
        return $this->db->get($this->table);
    }
 
}
 