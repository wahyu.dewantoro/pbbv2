@extends('page.master')
@section('judul')

<h1>
    Data Sudah Verlap
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="box">
    <div class="box-body no-padding">
        <?php if (isset($rk)) { ?>
            
            <table class="table table-bordered table-striped" id="example3">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="13%">NOP</th>
                        <!-- <th width="9%">NO Layanan</th> -->
                        <th>Nama WP</th>
                        <th>Alamat WP</th>
                        <th width="8%">Luas Bumi</th>
                        <th width="8%">Luas BNG</th>
                        <th width="10%">Tgl Penelitian</th>
                        <th width="5%">Aksi</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                        <tr>
                            <td align="center"><?php echo $no ?></td>
                            <td><?= $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '-' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP ?></td>
                            <!-- <td><?= $rk->THN_PELAYANAN . '.' . $rk->BUNDEL_PELAYANAN . '.' . $rk->NO_URUT_PELAYANAN ?></td> -->
                            <td><?= $rk->NM_WP_SPPT ?></td>
                            <td><?= 'DS. ' . $rk->NM_KELURAHAN ?></td>
                            <td align="right"><?= $rk->LUAS_TANAH ?></td>
                            <td align="right"><?= $rk->LUAS_BANGUNAN ?></td>
                            <td align="center"><?= $rk->TANGGAL_PENELITIAN ?></td>
                            <td align="center"><a href="<?= site_url('replikasi/editverlap') . '?id=' . $rk->ID ?>"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></button></a></td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>

        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true

        });

        

        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'data/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });
    });
</script>
@endsection