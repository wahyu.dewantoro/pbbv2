    @extends('page.master')
    @section('judul')
    <h1>
        Data Belum Verlap
    </h1>
    @endsection
    @section('content')

    <div class="box">
        <div class="box-body">
            <table class="table table-bordered table-striped" id="example3">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="15%">NOP</th>
                        <!-- <th width="15%">NO Layanan</th> -->
                        <th>Nama WP</th>
                        <th>Alamat WP</th>
                        <th width="5%">Aksi</th>

                    </tr>
                </thead>
                <?php if (isset($rk)) { ?>
                    <tbody>
                        <?php $no = 1;
                        foreach ($rk as $rk) {

                        ?>
                            <tr>
                                <td align="center"><?php echo $no ?></td>
                                <td><?= $rk->KD_PROPINSI_RIWAYAT . '.' . $rk->KD_DATI2_RIWAYAT . '.' . $rk->KD_KECAMATAN_RIWAYAT . '.' . $rk->KD_KELURAHAN_RIWAYAT . '.' . $rk->KD_BLOK_RIWAYAT . '.' . $rk->NO_URUT_RIWAYAT . '.' . $rk->KD_JNS_OP_RIWAYAT ?></td>
                                <td><?= $rk->NM_WP ?></td>
                                <td><?= 'DS. ' . $rk->NM_KELURAHAN ?></td>
                                <td align="center"><a href="<?= site_url('replikasi/verlap') . '?id=' . $rk->KD_PROPINSI_RIWAYAT . '.' . $rk->KD_DATI2_RIWAYAT . '.' . $rk->KD_KECAMATAN_RIWAYAT . '.' . $rk->KD_KELURAHAN_RIWAYAT . '.' . $rk->KD_BLOK_RIWAYAT . '.' . $rk->NO_URUT_RIWAYAT . '.' . $rk->KD_JNS_OP_RIWAYAT ?>"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Verlap"><i class="fa fa-edit"></i></button></a></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                    </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    @endsection
    @section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    @endsection
    @section('script')
    <!-- DataTables -->
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example3').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true

            });

        });
    </script>

    @endsection