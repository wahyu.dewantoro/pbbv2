@extends('page.master')
@section('judul')
<h1>
    Data Hasil Rekaman
</h1>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'data/dhr' ?>">
            <div class="form-group">
                Tahun Pajak
            </div>
            <div class="form-group">
                <select class="form-control" name="tahun">
                    <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                        <option <?php if ($q == $tahun) {
                                    echo "selected";
                                } ?> value="<?= $q ?>"><?= $q ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="cari" value="1">
            </div>
            <div class="form-group">
                Kecamatan
            </div>
            <div class="form-group">
                <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                    <option value="">Pilih</option>
                    <?php foreach ($kec as $kec) { ?>
                        <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                    echo "selected";
                                } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                Kelurahan
            </div>
            <div class="form-group">
                <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                    <option value="">Pilih</option>
                    <?php if (isset($kel)) { ?><option value="">000 Semua Kelurahan</option><?php } ?>
                    <?php
                    foreach ($kel as $kela) { ?>
                        <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                    echo "selected";
                                } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                    <?php  } ?>
                </select>
            </div>
            <div class="form-group">
                Jenis Bumi
            </div>
            <div class="form-group">
                <select class="form-control" name="jns_bumi[]" id="example-getting-started" multiple="multiple">
                    <?php foreach ($jenis as $rj) {
                        $key = array_search($rj, $jenis)
                    ?>
                        <option value="<?= $key ?>" <?php if (in_array($key, $jns_bumi)) {
                                                        echo "selected";
                                                    } ?>><?= $rj ?></option>
                    <?php } ?>
                </select>
            </div>
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
            <?php
            $where = "";
            if (isset($KD_KECAMATAN)) {
                $where = "&kec=" . urlencode($KD_KECAMATAN);
            }
            if (isset($KD_KELURAHAN)) {
                $where = "&kec=" . urlencode($KD_KECAMATAN) . "&kel=" . urlencode($KD_KELURAHAN);
            }
            if (isset($KD_KELURAHAN)) {
                echo anchor("data/dhrexcel?tahun=" . urlencode($tahun) . $where, 'Excel', 'class="btn btn-success"');
            }
            ?>
        </form>
        <?php if ($KD_KECAMATAN <> '') { ?>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="mytable">
                    <thead>
                        <tr>
                            <th width="3%">No</th>
                            <th width="12%">NOP</th>
                            <th>Formulir SPOP</th>
                            <th>Jumlah BNG</th>
                            <th>Alamat OP</th>
                            <th>RT/RW</th>
                            <th>Nama WP</th>
                            <th>Luas Bumi</th>
                            <th>ZNT</th>
                            <th>Nilai Bumi</th>
                            <th>NPWP</th>
                            <th>Status WP</th>
                            <th>Pekerjaan WP</th>
                            <th>Persil</th>
                        </tr>
                    </thead>
                </table>
            </div>
        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        <?php $ss = implode(',', $jns_bumi); ?>
        var t = $("#mytable").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                'url': '<?= base_url() ?>data/jsondatadhr',
                'type': 'POST',
                "data": {
                    'KD_KECAMATAN': '<?= $KD_KECAMATAN ?>',
                    'KD_KELURAHAN': '<?= $KD_KELURAHAN ?>',
                    'tahun': '<?= $tahun ?>',
                    'jns_bumi': '<?PHP echo $ss ?>'
                }
            },
            columns: [{
                    "data": "KD_PROPINSI",
                    "className": "text-center",
                    "orderable": false
                },
                {

                    "data": "KD_PROPINSI",
                    render: function(data, type, row) {
                        return row.KD_PROPINSI + '.' + row.KD_DATI2 + '.' + row.KD_KECAMATAN + '.' + row.KD_KELURAHAN + '.' + row.KD_BLOK + '-' + row.NO_URUT + '.' + row.KD_JNS_OP;

                    }
                },
                {
                    "data": "NO_FORMULIR_SPOP"
                },
                {
                    "data": "ALAMAT_OP",
                    render: function(data, type, row) {
                        return '-'
                    }
                },
                {
                    "data": "ALAMAT_OP",
                    render: function(data, type, row) {
                        return row.ALAMAT_OP + ', ' + row.BLOK_KAV_NO_OP;
                    }
                },
                {

                    "data": "RT_OP",
                    render: function(data, type, row) {
                        return row.RT_OP + '/' + row.RW_OP;
                    }

                },
                {
                    "data": "NM_WP_SPPT"
                },
                {
                    "data": "LUAS_BUMI"
                },
                {
                    "data": "KD_ZNT"
                },
                {
                    "data": "NILAI_SISTEM_BUMI"
                },
                {
                    "data": "NILAI_SISTEM_BUMI",
                },
                {
                    "data": "KD_STATUS_WP",
                    render: function(data, type, row) {
                        return data + ' - Pemilik';
                    }
                },
                {
                    "data": "NILAI_SISTEM_BUMI",
                }, {
                    "data": "NO_PERSIL"
                },

            ],
            order: [],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });


        $('#example-getting-started').select2({
            allowClear: true
        });

        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'data/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }
            });
        });



    });
</script>
@endsection