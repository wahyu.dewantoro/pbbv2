<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')
<h1>
  Dashboard
  <!-- <small>it all starts here</small> -->
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  <!-- <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li> -->
</ol>
@endsection
@section('content')
<div class="row">
<?php if ($CI->session->userdata('pbb_kg') != 22) { ?>
  <div class="col-md-3">
    <div class="row">
      <div class="col-md-12">
        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img style="margin-right: auto;margin-left: auto;" class="profile-user-img img-responsive img-circle" src="<?= base_url() . 'noimage.png' ?>" alt="User profile picture">
            <h3 class="profile-username text-center"><?= $CI->session->userdata('pbb_nama') ?></h3>
            <p class="text-muted text-center"><?php echo $nama_role ?></p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Tempat Pelayanan</b> <a class="pull-right"><?php $tt = $CI->session->userdata('unit_kantor');
                                                              if ($tt == 'kota') {
                                                                echo  "Pendopo";
                                                              } else {
                                                                echo "Kepanjen";
                                                              } ?></a>
              </li>
              <li class="list-group-item">
                <b>Login pada </b> <a class="pull-right"><?= $CI->session->userdata('jam_login') ?></a>
              </li>

            </ul>

            <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </div>
  <?php }?>
  <?php if ($CI->session->userdata('pbb_kg') == 8) { ?>
    <div class="col-md-6">
      <div class="row">

        <!-- /.col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Dokumen pada divisi
                <div class="pull-right">
                  <select class="form-control" id="tahun" class="form-control">
                    <!-- <option ></option> -->
                    <?php for ($da = date('Y'); $da >= 2018; $da--) { ?>
                      <option value="<?= $da ?>"><?= $da ?></option>
                    <?php } ?>
                  </select>
                </div>
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">


                <table class="table no-margin">

                  <tbody id="ambilrekap">
                    <?php foreach ($rekap as $rekap) { ?>
                      <tr>
                        <td><a href="#"><?= $rekap->NAMA_GROUP ?></a></td>
                        <td align="center">
                          <div class="label  <?php
                                              if ($rekap->DOK <= 50) {

                                                echo "label-success";
                                              } else if ($rekap->DOK > 50 && $rekap->DOK <= 100) {
                                                echo "label-warning";
                                              } else {
                                                echo "label-danger";
                                              }

                                              ?> "><?= $rekap->DOK ?> Berkas</div>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12">
          <a href="<?= base_url() . 'permohonan/listdokumen.html' ?>">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="ion ion-ios-paper"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Todo list</span>
                <br><br>
                <span class="info-box-number" id="todo"><?= $jumDok ?> Berkas</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </a>

        </div>
      </div>

    </div>
  <?php }else if($CI->session->userdata('pbb_kg') == '13' || $CI->session->userdata('pbb_kg') == '14'){
  ?>
     <div class="col-md-7">
      <div class="row">

        <!-- /.col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Realisasi Pembayaran
                
              </h3>
              <div class="pull-right">
                  <select class="form-control" id="thn_realisasi" class="form-control" >
                    <?php
                      foreach ( range( date('Y'), 2003 ) as $i ) {
                        print '<option value="'.$i.'"'.($i == $thn ? 'selected' : '').'>'.$i.'</option>';
                      }
                    ?>
                  </select>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">


                <table class="table table-bordered table-striped">
                  <thead>
                    <th width="3%" style="text-align: center">No.</th>
                    <th style="text-align: center">Desa/Kelurahan</th>                    
                    <th style="text-align: center">Jumlah NOP</th>
                    <th style="text-align: center">Baku</th>
                    <th style="text-align: center">Realisasi</th>
                    <th style="text-align: center">Persen</th>
                    <!-- <th width="5%" align="center">Aksi</th> -->
                  </thead>
                  <tbody id="ambilrealisasi">
                    <tr>
                      <td colspan="6" align="center">Loading ...</td>
                    </tr>
                    <!-- <?php $no=1; foreach ($realisasi as $realisasi) { ?>
                      <tr>
                        <td><?= $no++.'.'; ?></td>
                        <td><?= $realisasi->NM_KELURAHAN ?></td>
                        <td align="right"><?= number_format($realisasi->BAKU ,0,',','.') ?>
                        </td>
                        <td align="right"><?= number_format($realisasi->POKOK_REALISASI ,0,',','.') ?>
                        </td>
                        <td align="right"><?= round(($realisasi->POKOK_REALISASI/$realisasi->BAKU)*100, 2) ?> %</td>
                        
                      </tr>
                    <?php } ?> -->
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

      </div>
    </div>
  <?php
  } ?>
 
</div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    // The Calender
    var tahun = $('#thn_realisasi option:selected').val();
    $.ajax({
        url: "<?= base_url() . 'welcome/ambilrealisasi' ?>",
        type: 'POST',
        data: {
          tahun: tahun
        },
        success: function(data) {
          swal.close();
          $('#ambilrealisasi').html(data);
        },
    });

    $('#calendar').datepicker();

    $('select').on('change', function() {
      // alert( this.value );
      datarekap(this.value);
      todo(this.value);
    });

    
    function datarekap(thn) {
      $.ajax({
        url: "<?= base_url() . 'welcome/ambilrekap' ?>",
        type: 'GET',
        data: {
          tahun: thn
        },
        success: function(data) {
          $('#ambilrekap').html(data);
        },
      });
    }

    function todo(thn) {
      $.ajax({
        url: "<?= base_url() . 'welcome/ambiltodo' ?>",
        type: 'GET',
        data: {
          tahun: thn
        },
        success: function(data) {
          $('#todo').html(data);
        },
      });
    }

  });

  $('#thn_realisasi').on('change', function() {
      // alert( this.value );
      realisasi(this.value);
  });
  
  function realisasi(thn) {
      Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
      $.ajax({
        url: "<?= base_url() . 'welcome/ambilrealisasi' ?>",
        type: 'POST',
        data: {
          tahun: thn
        },
        success: function(data) {
          swal.close();
          $('#ambilrealisasi').html(data);
        },
      });
    }

    $(document).on("click", "#btn_realisasi", function (evt) {
      var kel = $(this).data("kelurahan");
    });

</script>
@endsection