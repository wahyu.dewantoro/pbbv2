<?php
$fileName = $nopel.'-2'. ".xls"; 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
header("Content-Type: application/vnd.ms-excel"); 
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<title>Pembetulan</title>
<style>
    body {
        font-family:"Calibri";
    }

    @page Section1{
            size: 35.56cm 21.59cm;
            margin: 1cm 1cm 1cm 1cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
    div.Section1 { page:Section1;}
</style>
</head>
<body>
<div class="Section1">
<table style="font-size: 11pt;" width="100%" border="0">
    <tr>
        <td width="3%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td></td>
        <td></td>
        <td></td>
        <td width="10%">Lampiran Kepala </td>
        <td width="30%" colspan="3">: Badan Pendapatan Daerah Kabupaten Malang</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Nomor</td>
        <td colspan="3">: <?php echo str_replace("#", "&emsp;&emsp;&emsp;", $sk->NOMOR.date('Y'));?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Tanggal</td>
        <td colspan="3">: <?= date('d').' '.bulan(date('m')).' '.date('Y'); ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>   
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="7"><center><b>DAFTAR PEMBETULAN/MUTASI/SPPT/SKP/STP *) TAHUN <?= date('Y')?></b></center></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Kabupaten</td>   
        <td>: <b>MALANG</b></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Kecamatan</td>   
        <td>: <b><?= $wp->NM_KECAMATAN; ?></b></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Desa/Kelurahan</td>   
        <td>: <b><?= $wp->NM_KELURAHAN; ?></b></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>   
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table style="font-size: 11pt;" width="100%" border="1" cellpadding="0" cellspacing="0">
    
    <!-- inti -->
    <tr style="text-align: center">
        <td rowspan="4" width="3%" valign="center">NO</td>
        <td colspan="3">LAMA</td>
        <td colspan="3">BARU</td>
        <td width="10%">SELISIH</td>
        <td width="10%" rowspan="4" valign="center">KETERANGAN</td>
        <td width="10%" rowspan="4" valign="center">DASAR HUKUM</td>
    </tr>
    <tr style="text-align: center">
        <td rowspan="3" valign="center">NOP</td>
        <td>NAMA DAN ALAMAT</td>   
        <td>JUMLAH PBB</td>
        <td rowspan="3" valign="center">NOP</td>
        <td>NAMA DAN ALAMAT</td>
        <td>JUMLAH PBB</td>
        <td>(7-4)</td>
    </tr>
    <tr style="text-align: center">
        <td>WAJIB PAJAK</td>   
        <td>TERHUTANG</td>
        <td>WAJIB PAJAK</td>
        <td>TERHUTANG</td>
        <td>(+) ATAU (-)</td>
    </tr>
    <tr style="text-align: center">
        <td></td>   
        <td>(RP)</td>
        <td></td>
        <td>(RP)</td>
        <td></td>
    </tr>
    <tr style="text-align: center">
        <td>1</td>
        <td>2</td>
        <td>3</td>   
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
        <td>10</td>
    </tr>
    <tr>
        <td style="text-align: center">1</td>
        <td><?= $wp->KD_PROPINSI.'.'.$wp->KD_DATI2.'.'.$wp->KD_KECAMATAN.'.'.$wp->KD_KELURAHAN.'.'.
        $wp->KD_BLOK.'.'.$wp->NO_URUT.'.'.$wp->KD_JNS_OP; ?></td>
        <td><?= $wp->NM_WP_SPPT; ?></td>   
        <td style="text-align: right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td><?= $wp->KD_PROPINSI.'.'.$wp->KD_DATI2.'.'.$wp->KD_KECAMATAN.'.'.$wp->KD_KELURAHAN.'.'.
        $wp->KD_BLOK.'.'.$wp->NO_URUT.'.'.$wp->KD_JNS_OP; ?></td>
        <td><?= $wp->NM_WP_SPPT; ?></td>
        <td style="text-align: right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td style="text-align: right">-</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <td style="text-align: center" colspan="3">JUMLAH</td>
        <td style="text-align: right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td colspan="2"></td>
        <td style="text-align: right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td style="text-align: right">-</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>
<table style="font-size: 11pt;" width="100%" border="0">
    <tr>
        <td width="3%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td width="10%"></td>
        <td width="23%" colspan="2"></td>
    </tr>
    <tr style="text-align: center">
        <td></td>
        <td></td>
        <td></td>   
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="2">
        <?= $sk->JABATAN?><br>
        Kabupaten Malang <br>
        &emsp;<br>
        &emsp;<br>
        &emsp;<br>
        <u><b><?= $sk->NAMA_PEGAWAI?></b></u><br>
        <!-- Pembina Utama Muda<br> -->
        NIP. <?= $sk->NIP?>
        </td>
    </tr>
</table>
</div>
</body>
</html>