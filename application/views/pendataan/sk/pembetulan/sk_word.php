<?php
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=".$nopel."-1".".doc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<title>Pembetulan</title>
<style>
    body {
        font-family:"Times New Roman";
    }

    @page Section1{
            size: 21cm 29.7cm;
            margin: 1cm 1cm 1cm 1cm;
            mso-page-orientation: portrait;
            mso-footer:f1;
        }
    div.Section1 { page:Section1;}
</style>
</head>
<body>
<div class="Section1">
<table width="100%">
<tr>
    <td width="15%"><center><img width="10%" src="<?php echo base_url('logo/malang_hp.jpeg')?>"></center></td>
    <td>
    <center>
    <p style="font-size: 16pt; margin-block-start: 0em; margin-block-end: 0em">
    PEMERINTAH KABUPATEN MALANG<br>
    <b>BADAN PENDAPATAN DAERAH</b><br>
    <span style="font-size: 12pt;">JL. K.H. AGUS SALIM 7 TELEPON (0341)367994</span><br>
    <span style="font-size: 11pt;"><b><u>MALANG - 65119</u></b></span>
    </p>
    </center>
    </td>
</tr>
</table>
<hr style="height:4px;border-width:0;color:black;background-color:black;margin-bottom:0px;">
<center>
    <p style="font-size: 11pt; margin-block-start: 0em; margin-block-end: 0em">
    <b>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH<br>
    Nomor : <?php echo str_replace("#", "&emsp;&emsp;&emsp;", $sk->NOMOR.date('Y'));?><br>
    <br>
    TENTANG<br>
    <br>
    PEMBETULAN PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
    ATAS SPPT TAHUN PAJAK <?= date('Y'); ?> <br>
    DESA/KELURAHAN <?= $wp->NM_KELURAHAN ?>
    <br>
    </b>
    </p>
</center>
<table style="font-size: 11pt;" width="100%">
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    Membaca
    </p>
    </td>
    <td width="3%" valign="top">
    :
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Surat permohonan Pembetulan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan 
    oleh Kuasa Wajib Pajak Tanggal <?= $tgl_pendataan ?> Desa/Kelurahan <?= $wp->NM_KELURAHAN; ?> Kecamatan 
    <?= $wp->NM_KECAMATAN; ?> Nopel <?= $nopel; ?>
    </p>
    </td>
</tr>
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    Menimbang
    </p>
    </td>
    <td width="3%" valign="top">
    :
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Bahwa berdasarkan hasil penelitian Kantor terdapat cukup alasan untuk pembetulan kesalahan tulis, kesalahan hitung dan / atau kekeliruan penerapan ketentuan tertentu dalam pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.
    </p>
    </td>
</tr>
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    Mengingat
    </p>
    </td>
    <td width="3%" valign="top">
    :
    </td>
    <td valign="top">
    
    <table style="font-size: 11pt;"  width="100%">
        <tr>
        <td valign="top">1.</td>
        <td valign="top"><p style="text-align: justify">Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</p></td>
        </tr>
        <tr>
        <td valign="top">2.</td>
        <td valign="top"><p style="text-align: justify">Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah.</p></td>
        </tr>
        <tr>
        <td valign="top">3.</td>
        <td valign="top">
        <p style="text-align: justify">Peraturan Bupati Malang Nomor 57 Tahun 2017 tentang Perubahan atas Peraturan Bupati Malang Nomor 53 Tahun 2013 tentang Tata Cara Pembetulan kesalahan tulis, kesalahan hitung dan / atau kekeliruan penerapan ketentuan tertentu dalam pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.</p>
        </td>
        </tr>
    </table>
    </td>
</tr>
</table>
<center>
    <p style="font-size: 11pt; margin-block-start: 0em; margin-block-end: 0em">
    <b>MEMUTUSKAN</b><br>
    </p>
</center>
<table style="font-size: 11pt;"  width="100%">
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    Menetapkan
    </p>
    </td>
    <td width="3%" valign="top">
    :
    </td>
    <td valign="top">
    </td>
</tr>
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    PERTAMA
    </p>
    </td>
    <td width="3%" valign="top">
    :
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Menerima seluruhnya permohonan Pembetulan PBB dengan Nama Kuasa Wajib Pajak <?= $nm_wp ?> 
    atas SPPT NOP. Terlampir Tahun <?= date('Y'); ?>, Nopel <?= $nopel ?> Tanggal <?= $tgl_pendataan ?>
    </p>
    </td>
</tr>
<tr>
    <td width="15%" valign="top">
    <p style="margin-block-start: 0em; margin-block-end: 0em">
    KEDUA
    </p>
    </td>
    <td width="3%" svalign="top">
    :
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Sesuai Diktum PERTAMA, rincian pembetulan adalah sebagaimana terlampir :
    </p>
    </td>
</tr>
</table>
<br>
<table style="font-size: 11pt;" width="100%">
<tr>
    <td width="68%"></td>
    <td>
    <center>
    <table width="100%">
        <tr>
            <td>Ditetapkan di : <b>MALANG</b><br>
            Pada Tanggal  : &emsp;&emsp; <?php echo bulan(date('m')); ?> <?= date('Y')?><br>
            <center>Kepala Badan Pendapatan Daerah<br>
            Kabupaten Malang</center><br>
            </td>
        </tr>
        <tr height="300px">
            <td>
            &emsp;<br>
            &emsp;<br>
            &emsp;<br>
            &emsp;<br>
            </td>
        </tr>
        <tr>
            <td><center>
                <b><u><?= $sk->NAMA_PEGAWAI?></u></b><br>
                <!-- Pembina Utama Muda<br> -->
                NIP. <?= $sk->NIP?>  <br>
                </center>
            </td>
        </tr>
    </table>
    </center>
    </td>
</tr>
</table>
<p style="font-size: 11pt; margin-block-start: 0em; margin-block-end: 0em">
Salinan, Keputusan ini disampaikan Kepada Yth<br>
1.	Kepala Desa/Lurah <b><?= $wp->NM_KELURAHAN; ?></b>
</p>
</div>
</body>
</html>