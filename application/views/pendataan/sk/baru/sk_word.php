<?php
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=".$nopel."-1".".doc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<title>Data Baru - <?= $nopel; ?></title>
<style>
    body {
        font-family:"Times New Roman";
    }

    @page Section1{
            size: 21,5cm 33cm;
            margin: 1cm 1cm 1cm 1cm;
            mso-page-orientation: portrait;
            mso-footer:f1;
        }
    div.Section1 { page:Section1;}
</style>
</head>
<body>
<div class="Section1">
<table width="100%">
<tr>
    <td width="5%"><center><img width="5%" src="<?php echo base_url('logo/malang_hp.jpeg')?>"></center></td>
    <td>
    <center>
    <p style="font-size: 14pt; margin-block-start: 0em; margin-block-end: 0em; margin-bottom: 0px">
    PEMERINTAH KABUPATEN MALANG<br>
    <b>BADAN PENDAPATAN DAERAH</b><br>
    <span style="font-size: 12pt;">JL. K.H. AGUS SALIM 7 TELEPON (0341)367994</span><br>
    <span style="font-size: 11pt;"><b><u>MALANG - 65119</u></b></span>
    </p>
    </center>
    </td>
</tr>
</table>
<hr style="height:4px;border-width:0;color:black;background-color:black;margin-bottom:0px;margin-top:0px;">
<center>
    <p style="font-size: 12pt; margin-block-start: 0em; margin-block-end: 0em; margin-top: 0px; margin-bottom: 0px">
    BERITA ACARA<br>
    PENYERAHAN SURAT PEMBERITAHUAN PAJAK TERUTANG (SPPT)<br>
    DAFTAR HIMPUNAN KETETAPAN DAN PEMBAYARAN (DHKP)<br>
    PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
    NOMOR : <?php echo str_replace("#", "&emsp;&emsp;&emsp;", $sk->NOMOR.date('Y'));?><br>
    </p>
</center>
<p style="text-align: justify; margin-block-start: 0em; margin-block-end: 0em; margin-top: 5px; margin-bottom: 0px">
Pada hari ini Tanggal <?= penyebut_cap(date('d')); ?> Bulan  <?= bulan(date('m')); ?> Tahun  <span ><?= penyebut_cap(date('Y')); ?></span> yang bertanda tangan di bawah ini :
</p>
<table style="font-size: 11pt;" width="100%">
<tr>
    <td width="13%" valign="top" rowspan="3">
    <center>I.</center>
    </td>
    <td width="10%" valign="top">
    Nama
    </td>
    <td valign="top">
    : <?= $sk2->NAMA_PEGAWAI?>
    </td>
</tr>
<tr>
    <td width="10%" valign="top">
    NIP
    </td>
    <td valign="top">
    : <?= $sk2->NIP?>
    </td>
</tr>
<tr>
    <td width="10%" valign="top">
    Jabatan
    </td>
    <td valign="top">
    : <?= $sk2->JABATAN?>
    </td>
</tr>
<tr>
    <td colspan="3">
    Selanjutnya disebut sebagai PIHAK KESATU 
    </td>
</tr>
<tr>
    <td width="13%" valign="top" rowspan="2">
    <center>II.</center>
    </td>
    <td width="10%" valign="top">
    Nama
    </td>
    <td valign="top">
    : <?= $nm_wp?>
    </td>
</tr>
<tr>
    <td width="10%" valign="top">
    Jabatan
    </td>
    <td valign="top">
    : -
    </td>
</tr>
<tr>
    <td colspan="3">
    Selanjutnya disebut sebagai PIHAK KEDUA 
    </td>
</tr>

</table>

<p style="text-align: justify; margin-block-start: 0em; margin-block-end: 0em; margin-top: 0px; margin-bottom: 0px">
Pihak Kesatu  menyerahkan kepada Pihak Kedua dan Pihak Kedua menerima penyerahan dari Pihak Kesatu  berupa Surat Pemberitahuan PajakTerutang (SPPT), :
</p>

<table style="font-size: 11pt;" width="100%">
<tr>
    <td width="8%" valign="top" style="padding-left: 30px">
    Tahun
    </td>
    <td valign="top">
    : <?= date('Y'); ?>
    </td>
</tr>
<tr>
    <td width="8%" valign="top" style="padding-left: 30px">
    Sektor
    </td>
    <td valign="top">
    : Perdesaan/Perkotaan Buku  I - II
    </td>
</tr>
<tr>
    <td width="8%" valign="top" style="padding-left: 30px">
    Kota/Kab.
    </td>
    <td valign="top">
    : Kabupaten Malang
    </td>
</tr>
<tr>
    <td colspan="2">
    Dengan jumlah pajak terutang sebesar Rp. <?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?>,- <span ><?= '('.penyebut_cap($wp->PBB_YG_HARUS_DIBAYAR_SPPT).' Rupiah)'?></span>
    </td>
</tr>
<tr>
    <td colspan="2">
    Dengan uraian sebagai berikut :
    </td>
</tr>
<tr>
    <td width="8%" valign="top" style="padding-left: 30px">
    1.
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Jumlah Surat Pemberitahuan Pajak Terutang  (SPPT)  sebanyak 1 lembar (Obyek Pajak)  merupakan Data Baru,berdasarkan ajuan Tanggal <?= $tgl_pendataan ?> Nomor Pelayanan <?= $nopel ?>.
    </p>
    </td>
</tr>
<tr>
    <td width="8%" valign="top" style="padding-left: 30px">
    2.
    </td>
    <td valign="top">
    <p style="text-align: justify">
    Data Baru tersebut dapat menambah jumlah pokok ketetapan Desa
    </p>
    </td>
</tr>
</table>
<br>
<table style="font-size: 11pt;" width="100%">
<tr>
    <td width="50%" valign="top">
        <center>
        <b>PIHAK KEDUA<br>
        Wajib Pajak / Kuasa
        </b><br>
        &emsp;<br>
        &emsp;<br>
        &emsp;<br>
        <hr style="height:1.5px;border-width:0;color:black;background-color:black;margin-bottom:0px;width:30%">
        </center>
    </td>
    <td width="50%" valign="top">
        <center>
        <b>PIHAK KESATU<br>
        Kasubid Pelayanan
        </b><br>
        &emsp;<br>
        &emsp;<br>
        &emsp;<br>
        <u><b><?= $sk2->NAMA_PEGAWAI?></b></u><br>
        Nip. <?= $sk2->NIP?>
        </center>
    </td>
</tr>
<tr>
    <td colspan="2">
        <center>Mengetahui,<br>
        a.n. KEPALA BADAN PENDAPATAN<br> 
        DAERAH KABUPATEN MALANG<br>
        Sekretaris<br>
        u.b.<br>
        <b>Kepala Bidang PBB P2</b><br>
        &emsp;<br>
        &emsp;<br>
        &emsp;<br>
        <u><b><?= $sk->NAMA_PEGAWAI?></b></u><br>
        Nip. <?= $sk->NIP?>
        </center>
    </td>
</tr>
</table>
</div>
</body>
</html>