<?php
$fileName = $nopel.'-2'. ".xls"; 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
header("Content-Type: application/vnd.ms-excel"); 
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<title>Pembetulan</title>
<style>
    body {
        font-family:"Calibri";
    }

    @page Section1{
            size: 21cm 29.7cm;
            margin: 1cm 1cm 1cm 1cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
    div.Section1 { page:Section1;}
</style>
</head>
<body>
<div class="Section1">
<table style="font-size: 11pt;" width="100%" border="0">
    <tr>
        <td width="3%"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td width="10%">Berita Acara</td>
        <td width="20%" colspan="3"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>No</td>
        <td colspan="3">: <?php echo str_replace("#", "&emsp;&emsp;&emsp;", $sk->NOMOR.date('Y'));?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Tanggal</td>
        <td colspan="3">: <?= date('d').' '.bulan(date('m')).' '.date('Y'); ?></td>
    </tr>
    <tr>
        <td colspan="9"><center><b><span style="font-size: 14pt;">DAFTAR RINCIAN JUMLAH SPPT PBB P2</span></b></center></td>
    </tr>
    <tr>
        <td colspan="9"><center><b><span style="font-size: 12pt;">Sektor : Perdesaan / Perkotaan <?= date('Y')?></span></b></center></td>
    </tr>
</table>
<table style="font-size: 11pt;" width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr style="text-align: center">
        <td width="3%" rowspan="2">NO</td>
        <td>KECAMATAN</td>
        <td>SPPT</td>
        <td rowspan="2">LEMBAR</td>
        <td rowspan="2">Luas Tanah / Bangunan</td>
        <td rowspan="2">NJOP Tanah / Bangunan</td>
        <td rowspan="2">Total NJOP (Rp)</td>
        <td width="10%">Jumlah Nominal</td>
        <td width="20%" rowspan="2">Keterangan</td>
    </tr>
    <tr style="text-align: center">
        <td>KELURAHAN/DESA</td>
        <td>NOP</td>
        <td>(Rp)</td>
    </tr>
    <tr>
        <td></td>
        <td>KEC. <?= $wp->NM_KECAMATAN; ?><br>
        DESA/KEL. <?= $wp->NM_KELURAHAN; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td align="center">1</td>
        <td><?= $wp->NM_WP_SPPT; ?></td>
        <td align="center"><?= $wp->KD_PROPINSI.'.'.$wp->KD_DATI2.'.'.$wp->KD_KECAMATAN.'.'.$wp->KD_KELURAHAN.'.'.
        $wp->KD_BLOK.'.'.$wp->NO_URUT.'.'.$wp->KD_JNS_OP; ?></td>
        <td align="center">1</td>
        <td align="center"><?= $wp->LUAS_BUMI_SPPT; ?>/<?= $wp->LUAS_BNG_SPPT; ?></td>
        <td align="center"><?= number_format($wp->NJOP_BUMI_SPPT,0,',','.'); ?>/ <?= number_format($wp->NJOP_BNG_SPPT,0,',','.'); ?></td>
        <td align="right"><?= number_format($wp->NJOP_SPPT,0,',','.'); ?></td>
        <td align="right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td>-</td>
    </tr>
    <tr>
        <td align="center"></td>
        <td colspan="2" align="center">TOTAL</td>
        <td align="center">1</td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="right"></td>
        <td align="right"><?= number_format($wp->PBB_YG_HARUS_DIBAYAR_SPPT,0,',','.')?></td>
        <td></td>
    </tr>
</table>
<br>
<table style="font-size: 11pt;" width="100%" border="0">
    <tr>
        <td width="3%"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td width="10%"></td>
        <td width="20%"></td>
    </tr>
    <tr style="text-align: center">
        <td></td>
        <td></td>
        <td></td>   
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="2">
        Kasubid Pelayanan<br>
        &emsp;<br>
        &emsp;<br>
        &emsp;<br>
        <u><b><?= $sk2->NAMA_PEGAWAI?></b></u><br>
        NIP. <?= $sk2->NIP?>
        </td>
    </tr>
</table>
</div>
</body>
</html>