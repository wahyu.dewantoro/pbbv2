@extends('page.master')
@section('judul')
<h1>
    Form SPOP
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<form action="<?= base_url('pendataan/prosesspop') ?>" method="post">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Objek Pajak</h4>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Jenis Transaksi</label>
                        <!-- <input type="text" > -->
                        <select name="jns_transaksi" id="jns_transaksi" class="form-control form-control-sm">
                            <option value="">Pilih</option>
                            @foreach($jt as $kjt=> $rjt)
                            <option value="{{ $kjt }}">{{ $rjt }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>No Formulir</label>
                        <input type="text" name="no_formulir" id="no_formulir" class="form-control form-control-sm formulir">
                    </div>
                    <div class="form-group">
                        <label>NOP</label>
                        <input type="text" name="nop_proses" id="nop_proses" class="form-control form-control-sm nop">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>No Persil</label>
                                <input type="text" name="no_persil" id="no_persil" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jalan</label>
                                <input type="text" name="jalan_op" id="jalan_op" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Blok / No</label>
                                <input type="text" name="blok_kav_no_op" id="blok_kav_no_op" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>RT</label>
                                <input type="text" name="rt_op" id="rt_op" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>RW</label>
                                <input type="text" name="rw_op" id="rw_op" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Luas Tanah</label>
                                <input type="text" name="luas_bumi" id="luas_bumi" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>ZNT</label>
                                <input type="text" name="kd_znt" id="kd_znt" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-grop">
                                <label for="">Jenis Bumi</label>
                                <select name="jns_bumi" id="jns_bumi" class="form-control form-control-sm" required>
                                    <option value="">Pilih</option>
                                    @foreach($tanah as $krt=> $rt)
                                    <option value="{{ $krt }}">{{ $rt }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-header">
            <h3 class="box-title">Subjek Pajak</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>NIK/ NO KTP</label>
                        <input type="text" name="subjek_pajak_id" id="subjek_pajak_id" class="form-control form-control-sm" required>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nm_wp" id="nm_wp" class="form-control form-control-sm" required>
                    </div>
                    <div class="form-group">
                        <label for="">Status WP</label>
                        <select class="form-control form-control-sm" required name="kd_status_wp" id="kd_status_wp">
                            <option value="">Pilih</option>
                            @foreach($wp as $iwp=>$wp)
                            <option value="{{ $iwp }}">{{ $wp }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jalan</label>
                                <input type="text" name="jalan_wp" id="jalan_wp" class="form-control form-control-sm" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Blok /No</label>
                                        <input type="text" name="blok_kav_no_wp" id="blok_kav_no_wp" class="form-control form-control-sm">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>RT</label>
                                        <input type="text" name="rt_wp" id="rt_wp" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>RW</label>
                                        <input type="text" name="rw_wp" id="rw_wp" class="form-control form-control-sm" required>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kelurahan</label>
                                <input type="text" name="kelurahan_wp" id="kelurahan_wp" class="form-control form-control-sm" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kode POS</label>
                                <input type="text" name="kode_pos_wp" id="kode_pos_wp" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kota /Kab</label>
                                <input type="text" name="kota_wp" id="kota_wp" class="form-control form-control-sm" required>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" name="telp_wp" id="telp_wp" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>NPWP</label>
                                <input type="text" name="npwp" id="npwp" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Pekerjaan</label>
                                <!-- <input type="text" > -->
                                <select name="status_pekerjaan_wp" id="status_pekerjaan_wp" class="form-control form-control-sm" required>
                                    <option value="">Pilih</option>
                                    @foreach($pekerjaan as $kpkj=> $pkj)
                                    <option value="{{ $kpkj }}">{{ $pkj }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-header">
            <h3 class="box-title">Pendata & Pemeriksa</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Pendataan</label>
                                <input type="text" name="tgl_pendataan_op" id="tgl_pendataan_op" class="tanggal form-control-sm form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Pendata</label>
                                <input type="text" name="nip_pendata" id="nip_pendata" class="form-control-sm form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Pemeriksaan</label>
                                <input type="text" name="tgl_pemeriksaan_op" id="tgl_pemeriksaan_op" class="tanggal form-control-sm form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Pemeriksa</label>
                                <input type="text" name="nip_pemeriksa_op" id="nip_pemeriksa_op" class="form-control-sm form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">

                <button class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection
@section('css')
<!-- DataTables -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<style>
    <style>.biru {
        outline: 1px solid blue;
    }

    .hijau {
        outline: 1px solid green;
    }
</style>
</style>
@endsection
@section('script')

<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $('.nop').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = formatnop(val_);
            $(this).val(nop);
        });

        $('.formulir').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = formatformulir(val_);
            $(this).val(nop);
        });


        $('#no_formulir').on('keyup', function() {
            input = $(this).val();
            nomer = input.replace(/[^\d]/g, "");
            $.ajax({
                url: "<?= base_url('pendataan/cekformulir') ?>",
                data: {
                    jenis: 1,
                    formulir: nomer
                },
                success: function(e) {
                    console.log(e);
                    $('#nop_proses').val(e.nop);
                    if (e.kode == 2) {
                        $('#no_formulir').val('');
                        Swal.fire(
                            'Nomer formulir ' + input + ' telah di entri',
                            '',
                            'warning'
                        );
                    }

                }
            });
        });


        $('#luas_bumi').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = angka_koma(val_);
            $(this).val(nop);
        });

        $('#subjek_pajak_id').keyup(function() {
            nik = $('#subjek_pajak_id').val();
            console.log(nik.length);
            if (nik.length == '16') {
                $.ajax({
                    url: "<?= base_url('welcome/ceknik') ?>?nik=" + nik,
                    beforeSend: function() {
                        $('html, body').css("cursor", "wait");
                    },
                    success: function(data) {
                        $('html, body').css("cursor", "default");
                        res = $.parseJSON(data);
                        // res=data;
                        console.log(res.raw);
                        if (res.kode == '0') {
                            // $("#VAL_NIK_WP").html("<span style='color:red'>" + res.keterangan + "</span>");
                            Swal.fire(
                                'Error',
                                res.keterangan,
                                'warning'
                            );
                            $(this).val('');
                            $('#nm_wp').val('');
                            $('#jalan_wp').val('');
                            $('#rt_wp').val('');
                            $('#rw_wp').val('');
                            // $('#KODE_KEC').val('');
                            // $('#NAMA_KEC').val('');
                            // $('#KODE_KEL').val('');
                            $('#kelurahan_wp').val('');
                            // $('#KODE_KAB').val('');
                            $('#kota_wp').val('');
                        } else {
                            $("#VAL_NIK_WP").html("");

                            $('#nm_wp').val(res.raw.nama_lgkp);
                            $('#jalan_wp').val(res.raw.alamat);
                            $('#rt_wp').val(res.raw.no_rt);
                            $('#rw_wp').val(res.raw.no_rw);
                            // $('#KODE_KEC').val(res.raw.no_kec);
                            // $('#NAMA_KEC').val(res.raw.kec_name);
                            // $('#KODE_KEL').val(res.raw.no_kel);
                            $('#kelurahan_wp').val(res.raw.kel_name);
                            // $('#KODE_KAB').val(res.raw.no_kab);
                            $('#kota_wp').val(res.raw.kab_name);

                        }
                    },
                    error: function(e) {
                        Swal.fire(
                            'Error',
                            'Sistem bermasalah',
                            'warning'
                        );
                    }
                });


            }
        });
    });
</script>
@endsection