@extends('page.master')
@section('judul')
<h1>
    <?= $title ?> LSPOP
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<form action="<?= base_url('pendataan/'.$url) ?>" method="post">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Objek Pajak</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Jenis Transaksi<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                        <!-- <input type="text" > -->
                        <select name="jns_transaksi" id="jns_transaksi" class="form-control form-control-sm" required>
                            <option value="">Pilih</option>
                            
                            @foreach($jt as $kjt=> $rjt)

                                @php
                                    $sjt = '';
                                @endphp

                                @if ($kjt == $jns_trx)
                                    @php
                                        $sjt = 'selected';
                                    @endphp
                                @endif

                            <option value="{{ $kjt }}" {{ $sjt }}>{{ $rjt }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>No Formulir<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                        <input type="text" required name="no_formulir" id="no_formulir" class="form-control form-control-sm formulir" value="{{$no_formulir}}">
                        <input type="hidden" name="formulir_id" id="formulir_id" class="form-control form-control-sm" value="{{$no_formulir}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>NOP<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                        <input required type="text" name="nop_proses" id="nop_proses" class="form-control form-control-sm nop" value="{{$nop_proses}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="">Nomor Bangunan<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                        <input type="text" tabIndex="24" required class="form-control " id="no_bng" name="no_bng" placeholder="No Bangunan" value="{{$no_bng}}"/>

                    </div>
                </div>
                <div class="col-md-12">
                    <div id="form_lspop"></div>

                </div>
            </div>
        </div>

        <div class="box-header">
            <h3 class="box-title">Pendata & Pemeriksa</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Pendataan<br>
                                <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                                <input type="text" name="tgl_pendataan_op" id="tgl_pendataan_op" class="tanggal form-control-sm form-control" required value="{{$tgl_pendataan_op}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Pendata<br>
                                <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                                <input type="text" name="nip_pendata" id="nip_pendata" class="form-control-sm form-control" required value="{{$nip_pendata}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Pemeriksaan<br>
                                <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                                <input type="text" name="tgl_pemeriksaan_op" id="tgl_pemeriksaan_op" class="tanggal form-control-sm form-control" required value="{{$tgl_pemeriksaan_op}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Pemeriksa<br>
                                <font color="red" style='font-size: 11px;'>*wajib diisi</font></label>
                                <input type="text" name="nip_pemeriksa_op" id="nip_pemeriksa_op" class="form-control-sm form-control" required value="{{$nip_pemeriksa_op}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">

                <button class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection
@section('css')
<!-- DataTables -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<style>
    <style>.biru {
        outline: 1px solid blue;
    }

    .hijau {
        outline: 1px solid green;
    }
</style>
</style>
@endsection
@section('script')

<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            
            valx_ = $('.nop').val();
            nopx = formatnop(valx_);
            $('.nop').val(nopx);

            valy_ = $('.formulir').val();
            nopy = formatformulir(valy_);
            $('.formulir').val(nopy);

            kode = $("#jns_transaksi").val();
            if (kode != '') {
                
                $.ajax({
                    url: "<?= base_url() . 'pendataan/getFormLspop' ?>",
                    data: {
                        'kode': kode,
                        'no_formulir' : $("#formulir_id").val(),
                    },
                    type: 'get',
                    success: function(result) {
                        $('#form_lspop').html(result);
                    }
                });
            } else {
                $('#form_lspop').html('');
            }

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $('.nop').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = formatnop(val_);
            $(this).val(nop);
        });

        $('.formulir').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = formatformulir(val_);
            $(this).val(nop);
        });


        $('#no_formulir').on('keyup', function() {
            input = $(this).val();
            nomer = input.replace(/[^\d]/g, "");
            $.ajax({
                url: "<?= base_url('pendataan/cekformulir') ?>",
                data: {
                    jenis: 2,
                    formulir: nomer
                },
                success: function(e) {
                    console.log(e);
                    $('#nop_proses').val(e.nop);
                    if (e.kode == 2) {
                        $('#no_formulir').val('');
                        Swal.fire(
                            'Nomer formulir ' + input + ' telah di entri',
                            '',
                            'warning'
                        );
                    }

                }
            });
        });


        $('#luas_bumi').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = angka_koma(val_);
            $(this).val(nop);
        });

        $("#jns_transaksi").change(function(e) {
            kode = $(this).val();
            if (kode != '') {
                $.ajax({
                    url: "<?= base_url() . 'pendataan/getFormLspop' ?>",
                    data: {
                        'kode': kode,
                    },
                    type: 'get',
                    success: function(result) {
                        $('#form_lspop').html(result);
                    }
                });
            } else {
                $('#form_lspop').html('');
            }
        });
    });
</script>
@endsection