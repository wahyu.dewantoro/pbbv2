@extends('page.master')
@section('judul')
<h1>
    Generate Formulir
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="row">
    
    <div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">SPOP/LSPOP</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">SISTEP/SISMIOP</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form action="<?= base_url('pendataan/prosesformulir') ?>" method="post">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Jenis Formulir</label>
                                    <select required class="form-control" name="format_formulir_id" id="format_formulir_id">
                                        <option value="">Pilih</option>
                                        <optgroup label="Penelitian Lapangan">

                                            @foreach($jenis as $rj)
                                            @if(substr($rj->ID,0,1)=='1')
                                            <option value="{{ $rj->ID }}">{{ $rj->TRANSAKSI }}</option>
                                            @endif
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Penelitian Kantor">
                                            @foreach($jenis as $rjo)
                                            @if(substr($rjo->ID,0,1)=='2')
                                            <option value="{{ $rjo->ID }}">{{ $rjo->TRANSAKSI }}</option>
                                            @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">No Pelayanan</label>
                                    <input required type="text" name="nomer_pelayanan" id="nomer_pelayanan" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-hover" id="table_nop" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>NOP</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button class="btn btn-sm btn-primary">
                                <i class="fa fa-save"></i> Submit
                            </button>
                        </div>
                    </div>
                </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
              <form action="<?= base_url('pendataan/prosessistep') ?>" method="post">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Jenis Formulir</label>
                                    <select required class="form-control" name="format_formulir_id" id="format_formulir_id">
                                        <option value="">Pilih</option>
                                        <optgroup label="Penelitian Lapangan">

                                            @foreach($jenis as $rj)
                                            @if(substr($rj->ID,0,1)=='1')
                                            <option value="{{ $rj->ID }}">{{ $rj->TRANSAKSI }}</option>
                                            @endif
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Penelitian Kantor">
                                            @foreach($jenis as $rjo)
                                            @if(substr($rjo->ID,0,1)=='2')
                                            <option value="{{ $rjo->ID }}">{{ $rjo->TRANSAKSI }}</option>
                                            @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">NOP</label>
                                    <div class="input-group">
                                            <input name="nop_proses[]" id="nop_proses[]" value="" type="text" class="form-control nop" required>
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-flat add_button"><i class="fa fa-plus"></i></button>
                                            </span>
                                    </div>
                                    <!-- <input required type="text" name="nop" id="nop" class="form-control"> -->
                                </div>
                                <div class="field_wrapper">                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button class="btn btn-sm btn-primary">
                                <i class="fa fa-save"></i> Submit
                            </button>
                        </div>
                    </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')

<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#format_formulir_id,#nomer_pelayanan').on('change keyup', function() {
            console.log('berfungsi ' + this.value+" - "+$('#nomer_pelayanan').val().length);
            $("#table_nop tbody").html('');
            if ($('#format_formulir_id').val() != '' && $('#nomer_pelayanan').val() != '' && $('#nomer_pelayanan').val().length==13) {
                
                $.ajax({
                    data: {
                        formulir_id: $('#format_formulir_id').val(),
                        nomer_pelayanan: $('#nomer_pelayanan').val()
                    },
                    url: "<?= base_url('pendataan/getNopNopel') ?>",
                    success: function(res) {
                        // $("#table_nop tbody").html('');
                        if (res.kode == 1) {
                            console.log(res.hasil);
                            tableobj = "";

                            $.each(res.hasil, function(i, a) {
                                /// do stuff
                                console.log(a.KD_PROPINSI);
                                tableobj += "<tr><td align='left'>" + a.KD_PROPINSI + '.' + a.KD_DATI2 + '.' + a.KD_KECAMATAN + '.' + a.KD_KELURAHAN + '.' + a.KD_BLOK + '.' + a.NO_URUT + '.' + a.KD_JNS_OP + "</td><td align='center'>";
                                if (a.NO_FORMULIR == null) {
                                    tableobj += "<input type='checkbox' name='nop[]' value='" + +a.KD_PROPINSI + '.' + a.KD_DATI2 + '.' + a.KD_KECAMATAN + '.' + a.KD_KELURAHAN + '.' + a.KD_BLOK + '.' + a.NO_URUT + '.' + a.KD_JNS_OP + "'>";
                                } else {
                                    tableobj += "Nomer formulir telah di generate :" + a.NO_FORMULIR;
                                }
                                tableobj += "</td></tr>";
                            });


                            $("#table_nop tbody").append(tableobj);
                            // $("#table_nop tbody").html(res.hasil);
                        } else {
                            $("#table_nop tbody").html('');
                            alert(res.pesan);
                        }

                    },
                    error: function(res) {
                        console.log(res);
                        $("#table_nop tbody").html('');
                        alert(res.pesan);
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    // $('.nop').on('keyup', function() {
    //     console.log('keyup');
    //     val_ = $(this).val();
    //     nop = formatnop(val_);
    //     $(this).val(nop);
        
    // });

    $(document).on('keyup','.nop', function () {
        console.log('keyup');
        val_ = $(this).val();
        nop = formatnop(val_);
        $(this).val(nop);
    });
    
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div class="form-group barisdiag">'+
                        '<div class="input-group">'+
                        '<input name="nop_proses[]" id="nop_proses[]" type="text" class="form-control nop">'+
                        '<span class="input-group-btn">'+
                        '<button type="button" class="btn btn-danger btn-flat remove_button"><i class="fa fa-trash"></i></button>'+
                        '</span>'+
                        '</div>'+
                        '</div>';
        
        //Once add button is clicked
        $(addButton).click(function(){
            $(wrapper).append(fieldHTML); 
        });
        
        $(wrapper).on('click','.remove_button',function(){ 
            $(this).parents(".barisdiag").remove();  
        });
    });
</script>
@endsection