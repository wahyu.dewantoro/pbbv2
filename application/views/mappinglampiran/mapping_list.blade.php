@extends('page.master')
@section('judul')
<h1>
    Mapping Lampiran
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li class="active">Mapping Lampiran</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Layanan</th>
                    <th width="10%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;
                foreach ($data as $rk) { ?>
                    <tr>
                        <td align="center"><?php echo $no ?></td>
                        <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                        <td align="center"><?php echo anchor('mappinglampiran/setting/' . $rk->KD_JNS_PELAYANAN, '<i class="fa fa-cog"></i>', 'class="btn btn-xs btn-success"'); ?></td>
                    </tr>
                <?php $no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
@endsection