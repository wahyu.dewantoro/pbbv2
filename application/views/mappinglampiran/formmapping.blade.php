  @extends('page.master')
  @section('judul')
  <h1>
      Mapping Lampiran
  </h1>
  <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
      <li>Mapping Lampiran</li>
      <li class="active"><?= $layanan ?></li>
  </ol>
  @endsection
  @section('content')

  <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

      <div class="box">
          <div class="box-header with-border">
              <div class="box-title">
                  List Lampiran
              </div>
              <div class="box-tools">
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                  <button type="button" class="btn btn-sm btn-info" onclick="cek(this.form.cekbox)"><i class="fa fa-check"></i> Selcet all</button>
                  <button type="button" class="btn btn-sm btn-warning" onclick="uncek(this.form.cekbox)"><i class="fa fa-close"></i> Clear all</button>

                  <?php echo anchor('mappinglampiran', '<i class="fa fa-list"></i> List', 'class="btn btn-sm btn-success"'); ?>
              </div>
          </div>
          <div class="box-body">
              <input type="hidden" name="KD_JNS_PELAYANAN" value="<?php echo $KD_JNS_PELAYANAN ?>">
              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th width="5%">Aktif</th>
                          <th>Lampiran</th>

                      </tr>
                  </thead>
                  <tbody>
                      <?php $no = 1;
                        foreach ($res as $rk) { ?>
                          <tr>
                              <td align="center"><?php echo $no ?></td>
                              <td align="center"><input type="checkbox" name="KODE[]" id="cekbox" <?php if ($rk->ST == '1') {
                                                                                                        echo "checked";
                                                                                                    } ?> value="<?php echo $rk->KODE ?>"></td>
                              <td><?php echo $rk->NAMA_LAMPIRAN ?></td>

                          </tr>
                      <?php $no++;
                        } ?>
                  </tbody>
              </table>
          </div>
      </div>
  </form>
  @endsection
  @section('script')

  <script>
      function cek(cekbox) {
          for (i = 0; i < cekbox.length; i++) {
              cekbox[i].checked = true;
          }
      }

      function uncek(cekbox) {
          for (i = 0; i < cekbox.length; i++) {
              cekbox[i].checked = false;
          }
      }
  </script>
  @endsection