@extends('page.master')
@section('judul')
<h1>
    Setting Expired
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li class="active">Expired</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Expired</label>
                        <div class="input-group input-group-md">
                            <input type="number" class="form-control" name="expired" id="expired" placeholder="Expired" value="<?php echo $expired; ?>">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat">Hari</button>
                            </span>
                        </div>
                        <?php echo form_error('expired') ?>    
                    </div>  
                    
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success pull-left"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
 