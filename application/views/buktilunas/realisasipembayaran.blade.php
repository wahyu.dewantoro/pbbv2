@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Laporan Realisasi Pembayaran
</h1>
@endsection
@section('content')
<div class="box box-danger">
    <form action="<?= base_url('buktilunas/realisasi_pembayaran')?>" method="GET">
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tgl Bayar Awal</label>
                        <input required type="text" class="form-control pull-right tanggal" id="tgl_awal" name="tgl_awal" value="<?= $tgl_awal ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tgl Bayar Akhir</label>
                        <input required type="text" class="form-control pull-right tanggal" id="tgl_akhir" name="tgl_akhir" value="<?= $tgl_akhir ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tahun Pajak</label>
                        <div class="input-group input-group-md">
                        <select name="thn_pajak" class="form-control" required>
                                <option value="">Pilih</option>
                                <option <?php if($tahun=='all'){ echo "selected"; }?> value="all">Semua</option>
                                <?php
                                    foreach ( range( $tahun_end, $tahun_start ) as $i ) {
                                        print '<option value="'.$i.'"'.($i == $tahun ? 'selected' : '').'>'.$i.'</option>';
                                    }
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-warning btn-flat">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-md-3" style="text-align: right">
                <a href="<?= base_url('buktilunas/cetak_realisasi_pdf/'.$tahun.'/'.$tgl_awal.'/'.$tgl_akhir)?>" title="Cetak PDF" class="btn btn-sm btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                <a href="<?= base_url('buktilunas/cetak_realisasi_excel/'.$tahun.'/'.$tgl_awal.'/'.$tgl_akhir)?>" title="Cetak Excel" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                </div>
            </div>
            
        </div>
    </form>
</div>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">Kode Billing</th>
                    <th style="text-align: center">Kecamatan</th>
                    <th style="text-align: center">Kelurahan</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">Keterangan</th>
                    <th style="text-align: center">Tgl Buat</th>
                    <th style="text-align: center">Tgl Bayar</th>
                    <th width="8%" style="text-align: center">Aksi</th>
                </tr>
            </thead>

        </table>
    </div>
</div>

<div class="modal fade" id="modal-detail">
          <div class="modal-dialog">
            <div class="modal-content" style="width: 700px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Daftar Nominative</h4>
              </div>
              <div class="modal-body">
                <div id="list_dafnom" style="text-align: center"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },

            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url(); ?>buktilunas/ajax_realisasi/<?= $tahun ?>"+"/<?= $tgl_awal ?>"+"/<?= $tgl_akhir ?>",
                "type": "post",
                
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    "data": "KOBIL"
                },
                {
                    "data": "NAMA_KECAMATAN"
                },
                {
                    "data": "NAMA_KELURAHAN"
                },
                {
                    "data": "TAHUN_PAJAK"
                },
                {
                    "data": "NAMA_WP"
                },
                {
                    "data": "CREATED_AT"
                },{
                    "data": "TGL_BAYAR"
                },
                {
                    "data": "ID",
                    'class': 'text-center',
                    render: function(data, type, row) {
                        this.url = '<?php echo base_url(); ?>dafnom/';
                        var d = '<button type="button" data-id="'+row.ID+'" title="Detail" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal-detail" onclick="detail_dafnom('+row.ID+')"><i class="fa fa-search"></i></button>'+' <a href="' + this.url + 'dafnom_cetak/'+row.ID+'" title="Cetak PDF" class="btn btn-xs btn-danger " target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
                        return d;
                    }
                },
            ],
            order: ['2','asc'],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
<script>
function konfirmasi(){
    confirm('Lanjut hapus data ini ?');
}
function detail_dafnom(id){
    $('#list_dafnom').html("<img src='<?= base_url('logo/')?>loading.gif' width='15%'/>");
    console.log(id);
    $.ajax({
            url: "<?= base_url('dafnom/ajax_list_dafnom'); ?>",
            dataType: 'json',
            type: "POST",
            data:{ kode: id},
            success: function(response)
            {
                console.log(response);
                if(response.status=='1'){
                    preview_ = 
                            '<div class="row">'+
                            '<div class="col-md-6">'+
                            '<span style="float: left"><b><i>Kode Billing</i>&emsp;&emsp;: <span>'+response.kobil+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'+response.jml_nop+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '+response.total+'</span></b></span><br>'+
                            '</div>'+
                            '<div class="col-md-6" style="text-align: right">'+
                            response.cetak_bukti_all +
                            '</div>'+
                            '</div>'+
                            '<hr>' +
                            '<div class="box-body"><div class="table-responsive">' +
                            '<table class="table table-bordered table-striped" id="table">' +
                            '<thead>' +
                            '<tr>' +
                            '<th width="3%" style="text-align: center">No</th>' +
                            '<th width="25%" style="text-align: center">NOP</th>' +
                            '<th style="text-align: center">Tahun Pajak</th>' +
                            '<th style="text-align: center">Nama WP</th>' +
                            '<th style="text-align: center">File</th>' +
                            '<th style="text-align: center">Nominal</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody >' +
                            response.tr +
                            '</tbody>' +
                            '<tfoot>'+
                            '<tr>' +
                            '<th colspan="5" style="text-align: center">Total</th>'+
                            '<th id="total_order" style="text-align: right"></th>'+
                            '</tr>' +
                            '</tfoot>' +
                            '</table>' +
                            '</div></div>';

                    $("#list_dafnom").html(preview_);
                    $('#total_order').html(response.total);

                    $('#table').DataTable({
                            ordering: false,
                            "iDisplayLength": 10
                        });
                }else{
                    document.getElementById("list_dafnom").innerHTML= "<span>Data tidak ditemukan !</span>";
                }
            }
    });
}
</script>

@endsection