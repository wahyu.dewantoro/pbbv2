@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Laporan Realisasi Pembayaran
</h1>
@endsection
@section('content')
<div class="box box-danger">
    <form action="<?= base_url('buktilunas/realisasi_pembayaran')?>" method="GET">
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tgl Bayar Awal</label>
                        <input required type="text" class="form-control pull-right tanggal" id="tgl_awal" name="tgl_awal" value="<?= $tgl_awal ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tgl Bayar Akhir</label>
                        <input required type="text" class="form-control pull-right tanggal" id="tgl_akhir" name="tgl_akhir" value="<?= $tgl_akhir ?>">
                    </div>
                </div>
               
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Kecamatan</label>
                        <select required class="form-control" name="kecamatan" id="kecamatan" onchange="fetch_kel(this.value);">
                            <option value="">Pilih</option>
                            @foreach($ref_kec as $a)
                            <option <?php if(isset($kec)){ if($kec==$a->KD_KECAMATAN){ echo "selected"; }}?> value="{{ $a->KD_KECAMATAN }}">{{ $a->NM_KECAMATAN }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Kelurahan</label>
                        <select required class="form-control kelurahan" name="kelurahan" id="kelurahan" style="width: 100%;" <?php if(!isset($ref_kel)){ echo "disabled"; }?>>
                            <option value="">Pilih</option>
                            <?php if(isset($ref_kel)){
                                foreach ($ref_kel as $b) {
                            ?>
                            <option <?php if(isset($kel)){ if($kel==$b->KD_KELURAHAN){ echo "selected"; }}?> value="{{ $b->KD_KELURAHAN }}">{{ $b->NM_KELURAHAN }}</option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tahun Pajak</label>
                        <div class="input-group input-group-md">
                        <select name="thn_pajak" class="form-control" required>
                                <option value="">Pilih</option>
                                <option <?php if($tahun=='all'){ echo "selected"; }?> value="all">Semua</option>
                                <?php
                                    foreach ( range( $tahun_end, $tahun_start ) as $i ) {
                                        print '<option value="'.$i.'"'.($i == $tahun ? 'selected' : '').'>'.$i.'</option>';
                                    }
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-warning btn-flat" name="btn_cari">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: right">
                <?php
                    if(isset($list_nop)){
                        if(count($list_nop)>0){
                ?>
                <a href="<?= base_url('buktilunas/cetak_realisasi_pdf/'.$tahun.'/'.$tgl_awal.'/'.$tgl_akhir.'/'.$kec.'/'.$kel)?>" title="Cetak PDF" class="btn btn-sm btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                <a href="<?= base_url('buktilunas/cetak_realisasi_excel/'.$tahun.'/'.$tgl_awal.'/'.$tgl_akhir.'/'.$kec.'/'.$kel)?>" title="Cetak Excel" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel-o"></i></a>                
                <?php
                        }
                    }
                ?>
                </div>
            </div>
            
        </div>
    </form>
</div> 
<?php
    $total = 0;
    if(isset($list_nop)){
?>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">NOP</th>
                    <th style="text-align: center">Nama WP</th>
                    <th style="text-align: center">Tanggal Bayar</th>
                    <th style="text-align: center">PBB</th>
                </tr>
            </thead>
           
            <tbody>
                <?php
                $no = 1;
                foreach ($list_nop as $a) {
                    $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                    
                    $kd_propinsi = $a->KD_PROPINSI;
                    $kd_dati2 = $a->KD_DATI2;
                    $kd_kecamatan = $a->KD_KECAMATAN;
                    $kd_kelurahan = $a->KD_KELURAHAN;
                    $kd_blok = $a->KD_BLOK;
                    $no_urut = $a->NO_URUT;
                    $kd_jns_op = $a->KD_JNS_OP;
                    $thn_pajak = $a->THN_PAJAK_SPPT;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td align="center"><?= $nop; ?></td>
                    <td><?= $a->NM_WP_SPPT; ?></td>
                    <td align="center"><?= $a->TGL_PEMBAYARAN_SPPT ?></td>
                    <td align="right"><?= number_format($a->JML_SPPT_YG_DIBAYAR,0,',','.') ?></td>
                </tr>
                <?php
                $total += $a->JML_SPPT_YG_DIBAYAR;
                }
                ?>
                
            </tbody>          
            <tfoot>
                <tr>
                    <th style="text-align: center" colspan="4">Total</th>
                    <td align="right"><?= number_format($total,0,',','.') ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php
    }
?>  


@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $('#example2').DataTable({
                            ordering: false,
                            "iDisplayLength": 10
                        });
    });
</script>
<script>

function fetch_kel(val)
{

 $('#label_kel').html("Kelurahan &emsp;<img src='<?= base_url('logo/')?>loading.gif' width='8%'/>");
  $.ajax({
    type: 'post',
    url: '<?= base_url('dafnom/fetch_kel')?>',
    data: {
      kd_kec:val
    },
    success: function (response) {
      $('.kelurahan').prop("disabled",false);
      document.getElementById("kelurahan").innerHTML=response; 
      $('#label_kel').html("Kelurahan");
    }
  });
}

</script>

@endsection