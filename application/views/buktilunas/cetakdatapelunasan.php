	<style type="text/css">
		body {
			font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
			font-size: 10px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		}

		.inti {
			border: 1px solid #C6C6C6;
			margin: auto;
		}

		.inti td {
			border-right: 1px solid #C6C6C6;
		}

		.inti th {
			border-right: 1px solid #C6C6C6;
			border-bottom: 1px solid #C6C6C6;
		}
	</style>
	<style type="text/css" media="print">
		@page {
			size: landscape;
		}
	</style>
	<table width="100%">
	<tr>
		<td width="25%"><center><img width="38%" src="<?php echo base_url('logo/kab_malang.jpg')?>"></center></td>
		<td>
		<center>
		<p style="font-size: 12pt; margin-block-start: 0em; margin-block-end: 0em; margin-bottom: 0px">
		<b>PEMERINTAH KABUPATEN MALANG</b><br>
		<b>BADAN PENDAPATAN DAERAH</b><br>
		<span style="font-size: 12pt;"><b>JL. K.H. Agus Salim 7 Telp (0341) 362372 Fax (0341) 355708</b></span><br>
		<span style="font-size: 12pt;"><b>MALANG - 65119</b></span>
		</p>
		</center>
		</td>
		<td width="25%"></td>
	</tr>
	</table>
	<hr>
	<br>
	<br>
	<table width="100%" border="1" cellspacing="0" cellpadding="1">
		<tr>
			<th width="5%" style="text-align: center">No</th>
            <th style="text-align: center">NOP</th>
            <th>Nama WP</th>
            <th style="text-align: center">Tahun Pajak</th>
            <th style="text-align: center">Kelurahan</th>
            <th style="text-align: center">Kecamatan</th>
            <th style="text-align: center">PBB</th>
            <th style="text-align: center">Tgl Bayar</th>
		</tr>
		<?php
		$no=1;
		foreach ($res as $a) {
			$nopx = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'-'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
			$kd_propinsi = $a->KD_PROPINSI;
			$kd_dati2 = $a->KD_DATI2;
			$kd_kecamatan = $a->KD_KECAMATAN;
			$kd_kelurahan = $a->KD_KELURAHAN;
			$kd_blok = $a->KD_BLOK;
			$no_urut = $a->NO_URUT;
			$kd_jns_op = $a->KD_JNS_OP;
		?>
		<tr>
            <td align="center"><?php echo $no++; ?></td>
            <td align="center"><?= $nopx ?></td>
            <td><?= $a->NM_WP_SPPT ?></td>
            <td align="center"><?= $a->THN_PAJAK_SPPT ?></td>
            <td><?= $a->NM_KELURAHAN ?></td>
            <td><?= $a->NM_KECAMATAN ?></td>
            <td align="right"><?= number_format($a->JML_SPPT_YG_DIBAYAR,0,',','.') ?></td>
            <td align="center"><?= $a->TGL_PEMBAYARAN_SPPT ?></td>
        </tr>
		<?php
		}
		?>
	</table>
	<script type="text/javascript">
	
		window.print();
		// window.open();
		// window.print();
		// window.close();
	</script>