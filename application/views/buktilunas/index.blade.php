@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Cetak Pelunasan NOP
</h1>
@endsection
@section('content')
<div class="box box-danger">
        
        <form action="<?= base_url($action_cek)?>" method="GET">
        <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                <div class="input-group input-group-md">
                <input type="number" id="nop" name="nop" class="form-control" placeholder="Masukkan NOP" value="<?= $nop ?>" onkeyup="myCek()">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Cari</button>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
            <div class="col-md-3">
                
            </div>
            <div class="col-md-3" style="text-align: right">
                <?php if($nop != ''){
                ?>
                <a href="<?= base_url('buktilunas/cetak_data_pelunasan/'.$nop)?>" title="Cetak PDF" class="btn btn-sm btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                <?php
                }
                ?>
                
            </div>
          </div>
        </div>
        </form>
      </div>
      <!-- /.box -->
<?php if(isset($res)){
?>

<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">NOP</th>
                    <th>Nama WP</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">Kelurahan</th>
                    <th style="text-align: center">Kecamatan</th>
                    <th style="text-align: center">PBB</th>
                    <th style="text-align: center">Tgl Bayar</th>
                    <th style="text-align: center" width="8%">Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $no = 1;
                foreach ($res as $a) {
                $nopx = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                $kd_propinsi = $a->KD_PROPINSI;
                $kd_dati2 = $a->KD_DATI2;
                $kd_kecamatan = $a->KD_KECAMATAN;
                $kd_kelurahan = $a->KD_KELURAHAN;
                $kd_blok = $a->KD_BLOK;
                $no_urut = $a->NO_URUT;
                $kd_jns_op = $a->KD_JNS_OP;
                
                
            ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td align="center"><?= $nopx ?></td>
                    <td><?= $a->NM_WP_SPPT ?></td>
                    <td align="center"><?= $a->THN_PAJAK_SPPT ?></td>
                    <td><?= $a->NM_KELURAHAN ?></td>
                    <td><?= $a->NM_KECAMATAN ?></td>
                    <td align="right"><?= number_format($a->JML_SPPT_YG_DIBAYAR,0,',','.') ?></td>
                    <td align="center"><?= $a->TGL_PEMBAYARAN_SPPT ?></td>
                    <td align="center"><a href="<?= base_url('buktilunas/bukti_pembayaran/'.$nopx.'/'.$a->THN_PAJAK_SPPT)?>" title="Cetak PDF" class="btn btn-xs btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a></td>
                </tr>
            <?php
                }
            ?>
            </tbody>

        </table>
    </div>
</div>

<?php
}
?>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
function myCek() {
    var x = $('#nop').val();
    // Replace dot with a comma
    var num = x.toString().replace(/\./g, '');
    console.log(num);
    $('#nop').val(num)
}
</script>

@endsection