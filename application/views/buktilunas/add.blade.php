@extends('page.master')
@section('judul')
<h1>
    <?= $judul; ?> Daftar Nominative
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')

<div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Filter</h3>

        </div>
        <!-- /.box-header -->
        
        <form id="cek_nop" action="<?= base_url($action_cek)?>" method="POST">
        <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Tahun Pajak</label>
                <input type="number" class="form-control" name="thn_pajak" id="thn_pajak" onKeyPress="if(this.value.length==4) return false;" value="<?= $tahun ?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Kecamatan</label>
                <select required class="form-control" name="kecamatan" id="kecamatan" onchange="fetch_kel(this.value);">
                        <option value="">Pilih</option>
                        @foreach($ref_kec as $a)
                        <option <?php if(isset($kec)){ if($kec==$a->KD_KECAMATAN){ echo "selected"; }}?> value="{{ $a->KD_KECAMATAN }}">{{ $a->NM_KECAMATAN }}</option>
                        @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label id="label_kel">Kelurahan</label>
                <select required class="form-control kelurahan" name="kelurahan" id="kelurahan" style="width: 100%;" <?php if(!isset($ref_kel)){ echo "disabled"; }?>>
                  <option value="">Pilih</option>
                  <?php if(isset($ref_kel)){
                    foreach ($ref_kel as $b) {
                  ?>
                  <option <?php if(isset($kel)){ if($kel==$b->KD_KELURAHAN){ echo "selected"; }}?> value="{{ $b->KD_KELURAHAN }}">{{ $b->NM_KELURAHAN }}</option>
                  <?php
                    }
                  } ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>Batas Nominal</label>
                <input type="text" class="form-control" name="batas_nominal" id="batas_nominal" value="<?php if(isset($nominal)){ echo $nominal; }?>">
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
        <button type="submit" name="btn_cek" class="btn btn-info pull-left">Cek</button>
        </div>
        </form>
      </div>
      <!-- /.box -->
<div class="row">
        <div class="col-md-6">
        <div class="box box-info">
        
        <form id="add_temp" action="<?= base_url($action_temp)?>">
        <div class="box-header with-border">
          <h3 class="box-title">List NOP</h3>
          <button id="tambah_dafnom" type="submit" class="btn btn-warning pull-right tambah_dafnom" <?php if(isset($list_nop)){ if(count($list_nop)  == 0 ){ echo "disabled"; } }else{ echo "disabled"; } ?>>Tambah</button>

        </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div id="div_nop" style="text-align: center">
                <?php if(isset($list_nop)){
                  if(count($list_nop)>0){
                    $datax = "";
                    $no = 1;
                    $cek_total = 0;
                    $total = 0;
                    
                    $batas_nominal = str_replace(',','',$nominal);
                    foreach ($list_nop as $a) {
                        $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                        $cek_total += $a->TOTAL;
                        if($cek_total <= $batas_nominal){
                            $datax .= "
                            <tr>
                                <th style='text-align: center' id='nomer'>".$no++."</th>
                                <td><input type='checkbox' id='cek_satu[]' name='cek_satu[]' class='cek_satu' value='".$nop.'|'.str_replace("'", "`",$a->NM_WP_SPPT).'|'.$a->POKOK.'|'.$a->DENDA.'|'.$a->TOTAL.'|'.$a->THN_PAJAK_SPPT.'|'.$a->PBB.'|'.$nominal."'></td>
                                <td>".$nop."</td>
                                <td style='text-align: left'>".$a->NM_WP_SPPT."</td>
                                <td style='text-align: right' id='jml'>".number_format($a->TOTAL,0,',','.')."</td>
                            </tr>";
                            $total += $a->TOTAL;
                        }
                        
                    }
        
                    $data = "
                    <span style='float: left'><b><i>Jumlah NOP</i> &emsp;&emsp;: <span id='akumulasi_nop'>0</span></b></span><br>
                    <span style='float: left'><b><i>Akumulasi</i>&emsp;&emsp;&emsp;: Rp. <span id='akumulasi'>0</span></b></span><br><br>
                    <table id='tblNOP' class='table table-bordered table-striped'>
                    <thead>
                        <tr>
                            <th style='text-align: center'>No</th>
                            <th style='text-align: center'><input type='checkbox' id='cek_all' name='cek_all' class='cek_all' value='0' ></th>
                            <th style='text-align: center'>NOP</th>
                            <th style='text-align: center'>Nama WP</th>
                            <th style='text-align: center'>Nominal</th>
                        </tr>
                    </thead>
                    <tbody id='tbody_nop'>
                    ".$datax."
                    <tr>
                        <th colspan='4' style='text-align: center'>Total</th>
                        <td style='text-align: right'><span id='total'>".number_format($total,0,',','.')."</span><input type='hidden' id='total_nop' value='".($no-1)."'></td>
                    </tr>
                    </tbody>
                    </table>
                    ";
                    
                    echo $data;
                  }else{
                      $data = "<span>Data belum ditemukan !</span>";
                      
                      echo $data;
                  }
                }else{
                ?>
                <span>Data belum ditemukan !</span>
                <?php
                }?>
                
                </div>                
              </div>
              
              
              <!-- <div class="box-footer">
                <button type="submit" class="btn btn-warning pull-right tambah_dafnom" disabled>Tambah</button>
                </div> -->
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <div class="col-md-6" id="div_dafnom">

          <div class="box box-success">
            <div class="box-header with-border">
            <h3 class="box-title">Daftar Nominative</h3>
            </div>
            <div class="box-body">
            <div id="div_list_dafnom" style="text-align: center">
                <span>Data belum ditemukan !</span>
                </div> 
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right simpan_dafnom" disabled>Simpan</button>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')

<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script>
$(function(){
  // add multiple select / deselect functionality
  $("#cek_all").click(function () {
      $('.cek_satu').prop('checked', this.checked);
      var total = $("#total").text();
      var total_nop = $("#total_nop").val();
      if ( this.checked ) {
        $("#akumulasi").text(total);
        $("#akumulasi_nop").text(total_nop);
      } else {
        $("#akumulasi").text("0");
        $("#akumulasi_nop").text("0");
      }
  });

  $(".cek_satu").click(function(){
    var bayar = $(this).val();
    var akumulasi = $("#akumulasi").text();
    var akumulasi_nop = $("#akumulasi_nop").text();
    var arrayOfStrings = bayar.split("|");
    var hasil;
    var total_nop;
    console.log(arrayOfStrings[4]);
    

    if ( this.checked ) {
      hasil = (parseInt(akumulasi.replace('.',''))) + (parseInt(arrayOfStrings[4]));
      total_nop = (parseInt(akumulasi_nop)) + 1;
    } else {
      hasil = (parseInt(akumulasi.replace('.',''))) - (parseInt(arrayOfStrings[4]));
      total_nop = (parseInt(akumulasi_nop)) - 1;
    }

    
    console.log(hasil);
    console.log(total_nop);

    var total = hasil.toString();
    
    if(total != "") {
      valArr = total.split('.');
      valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
      total = valArr.join('.');
    }
    
    $("#akumulasi").text(total.replace(",", "."));
    $("#akumulasi_nop").text(total_nop.toString());

    if($(".cek_satu").length == $(".cek_satu:checked").length) {
      $("#cek_all").prop("checked", true);
    } else {
      $("#cek_all").prop("checked", false);
    }

  });
});
</script> 
<script>

function fetch_kel(val)
{

 $('#label_kel').html("Kelurahan &emsp;<img src='<?= base_url('logo/')?>loading.gif' width='8%'/>");
 $.ajax({
 type: 'post',
 url: '<?= base_url('dafnom/fetch_kel')?>',
 data: {
  kd_kec:val
 },
 success: function (response) {
  $('.kelurahan').prop("disabled",false);
  document.getElementById("kelurahan").innerHTML=response; 
  $('#label_kel').html("Kelurahan");
 }
 });
}

</script>

<script>
var myinput = document.getElementById('batas_nominal');

myinput.addEventListener('keyup', function() {
  var val = this.value;
  val = val.replace(/[^0-9\.]/g,'');
  
  if(val != "") {
    valArr = val.split('.');
    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
    val = valArr.join('.');
  }
  
  this.value = val;
});

</script>

<script type="text/javascript">
    $("#add_temp").submit(function(e) {
      
      var checked = $("#tblNOP input[type=checkbox]:checked").length;
 
      if (checked > 0) {

        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        $('#div_list_dafnom').html("<img src='<?= base_url('logo/')?>loading.gif' width='15%'/>");

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
              
              if(data=='0'){

                $('.simpan_dafnom').prop("disabled",true);
                
              }else{
                //mengkosongkan list nop
                // $("#tbody_nop").empty();
                $("#akumulasi").text("0");
                $("#akumulasi_nop").text("0");
                $("#cek_all").prop("checked", false);
                // $("#tbody_nop").html(data[0].list_nop);

                //menampilkan data list dafnom
                $('.simpan_dafnom').prop("disabled",false);
                document.getElementById("div_list_dafnom").innerHTML=data;

                var arr = [];
                $('.cek_satu:checked').each(function () {
                  $(this).closest('tr').remove();
                });

                var TotalValue = 0;
                $("tr #jml").each(function(index,value){
                  var jml = $(this).text()
                  currentRow = parseInt(jml.replace(".", ""));
                  TotalValue += currentRow
                });

                i = 1
                $("tr #nomer").each(function(index,value){
                  $(this).text(i++)
                });

                
                var tot = TotalValue.toString()
                if(tot != "") {
                  valArr = tot.split('.');
                  valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
                  tot = valArr.join('.');
                }
                $('#total').text(tot.replace(",", "."));
              }
            }
        });
      } else {
                alert("Silahkan pilih NOP terlebih dahulu !");
                return false;
            }

    });

    $('.simpan_dafnom').click(function(){
      var thn_pajak = $('#thn_pajak').val();
      var kec = $('#kecamatan').val();
      var kel = $('#kelurahan').val();
      $.ajax({
        type: "POST",
        url: "<?= base_url('').$action; ?>",
        data: { thn_pajak: thn_pajak, kec: kec, kel: kel},
        success: function (result) {
          // alert(result)
            if(result == 'sukses'){
              var jml_nop = $('#nop_terpilih').text();
              var pbb = $('#jml_pbb').text();
              $('#div_list_dafnom').html('<span>Data belum ditemukan !</span>');
              $('#div_nop').html('<span>Data belum ditemukan !</span>');
              $('.simpan_dafnom').prop("disabled",true);
              $('.tambah_dafnom').prop("disabled",true);
              if(confirm("Berhasil Menyimpan NOP sejumlah "+jml_nop+" dengan PBB sejumlah "+pbb)) {
                    window.location.href = "<?= base_url('dafnom/index')?>"
              }
            }else{
              alert("Gagal Menyimpan Data !");
            }
        }
      });
    });

    // $('.tambah_dafnom').click(function(){
      
    //   var arr = [];
    //   $('.cek_satu:checked').each(function () {
    //     $(this).closest('tr').remove();
    //   });

    //   var TotalValue = 0;
    //   $("tr #jml").each(function(index,value){
    //     var jml = $(this).text()
    //      currentRow = parseInt(jml.replace(".", ""));
    //      TotalValue += currentRow
    //   });

    //   i = 1
    //   $("tr #nomer").each(function(index,value){
    //     $(this).text(i++)
    //   });

      
    //   var tot = TotalValue.toString()
    //   if(tot != "") {
    //     valArr = tot.split('.');
    //     valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
    //     tot = valArr.join('.');
    //   }
    //   $('#total').text(tot.replace(",", "."));
    // });
</script>

@endsection