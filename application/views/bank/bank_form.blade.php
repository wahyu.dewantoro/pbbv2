@extends('page.master')
@section('judul')
<h1>
    Bank
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>Bank</li>
    <li class="active"><?= $button ?></li>
</ol>
@endsection
@section('content')

<div class="box">
    <div class="box-body">
        <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Kode Bank </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="KODE_BANK" id="KODE_BANK" placeholder="Nama Group" value="<?php echo $KODE_BANK; ?>" />
                            <?php echo form_error('KODE_BANK') ?>
                        </div>
                    </div>
                    <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Bank </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="NAMA_BANK" id="NAMA_BANK" placeholder="Nama Group" value="<?php echo $NAMA_BANK; ?>" />
                            <?php echo form_error('NAMA_BANK') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('bank') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>





        </form>
    </div>
</div>
@endsection