@extends('page.master')
@section('judul')

<h1>
    User TP
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li class="active">Pengguna</li>
</ol>

@endsection
@section('content')
<div class="box">
    <div class="box-header with-border">
        <div class="box-title">
            Data Pengguna
        </div>
        <div class="box-tools">
            <?php echo anchor(site_url('usertp/create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-sm btn-success"'); ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <table class="table table-bordered table-striped" id="example1">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Nama</th>
                    <th>Kecamatan</th>
                    <th width="10%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach($data as $rd){?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $rd->NAMA_PENGGUNA?></td>
                        <td><?= $rd->NAMA_UPT?></td>
                        <td align="center"><a href="<?= base_url().'pengguna/hapustp/'.$rd->ID ?>" onclick="return confirm('Apakah anda yakin?')" class="btn btn-sm btn-danger"><i class="fa fa-trash" ></i></a> </td>
                    </tr>

                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        $("#example1").dataTable();
        /* var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },



            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?= base_url() ?>pengguna/json",
                "type": "POST"
            },
            columns: [{
                    "data": "KODE_PENGGUNA",
                    "orderable": false,
                    "className": "text-center"
                }, {
                    "data": "USERNAME"
                }, {
                    "data": "NAMA_PENGGUNA"
                }, {
                    "data": "NIP"
                }, {
                    "data": "NAMA_GROUP"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        }); */
    });
</script>

@endsection