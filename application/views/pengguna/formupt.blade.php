@extends('page.master')
@section('judul')
<h1>
    Form Pengguna
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>User UPT</li>
    <li class="active"><?php echo $button ?></li>
</ol>

@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class='form-horizontal' action="<?php echo $action; ?>" method="post" >
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">User UTP </label>
                        <div class="col-md-8">
                            <select class="form-control" name="KODE_PENGGUNA" required id="KODE_PENGGUNA">
                                <option value="">Pilih user</option>
                                <?php foreach ($lp as $lp) { ?>
                                    <option value="<?= $lp->KODE_PENGGUNA ?>"><?= $lp->NAMA_PENGGUNA ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">UPT </label>
                        <div class="col-md-8">
                            <select class="form-control" name="KODE_UPT" required id="KODE_UPT">
                                <option value="">Pilih UPT</option>
                                <?php foreach ($lu as $lu) { ?>
                                    <option value="<?= $lu->KODE_UPT ?>"><?= $lu->NAMA_UPT ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button class="btn btn-sm btn-success"><?= $button ?></button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
@endsection
@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
@endsection
@section('script')
<!-- Select2 -->
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('.chosen').select2({
            placeholder: "Silahkan pilih",
            allowClear: true
        });
    });
</script>
@endsection