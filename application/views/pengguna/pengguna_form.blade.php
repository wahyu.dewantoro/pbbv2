@extends('page.master')
@section('judul')
<h1>
    Form Pengguna
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>Pengguna</li>
    <li class="active"><?php echo $button ?></li>
</ol>

@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Username </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="USERNAME" id="USERNAME" placeholder="USERNAME" value="<?php echo $USERNAME; ?>" />
                            <?php echo form_error('USERNAME') ?>
                        </div>
                    </div>
                    <?php if ($button != 'Update Pengguna') { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password </label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="PASSWORD" id="PASSWORD" placeholder="PASSWORD" value="<?php echo $PASSWORD; ?>" />
                                <?php echo form_error('PASSWORD') ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="NAMA_PENGGUNA" id="NAMA_PENGGUNA" placeholder="NAMA PENGGUNA" value="<?php echo $NAMA_PENGGUNA; ?>" />
                            <?php echo form_error('NAMA_PENGGUNA') ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">NIP </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="NIP" id="NIP" placeholder="NIP" value="<?php echo $NIP; ?>" />
                            <?php echo form_error('NIP') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Seksi </label>
                        <div class="col-md-8">
                            <select class="form-control chosen" name="KD_SEKSI" id="KD_SEKSI">
                                <option value=""></option>
                                <?php foreach ($seksi as $seksi) { ?>
                                    <option <?php if ($seksi->KD_SEKSI == $KD_SEKSI) {
                                                echo "selected";
                                            } ?> value="<?php echo $seksi->KD_SEKSI ?>"><?php echo $seksi->NM_SEKSI ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('KD_SEKSI') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"> Group </label>
                        <div class="col-md-8">
                            <select class="form-control chosen" name="KODE_GROUP" id="KODE_GROUP">
                                <option value=""></option>
                                <?php foreach ($group as $group) { ?>
                                    <option <?php if ($KODE_GROUP == $group->KODE_GROUP) {
                                                echo "selected";
                                            } ?> value="<?php echo $group->KODE_GROUP ?>"><?php echo $group->NAMA_GROUP ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('KODE_GROUP') ?>
                        </div>
                    </div>
                    <input type="hidden" name="KODE_PENGGUNA" value="<?php echo $KODE_PENGGUNA; ?>" />

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('pengguna') ?>" class="btn btn-sm btn-warning"><i class="fa fa-close"></i> Batal</a>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
@endsection
@section('script')
<!-- Select2 -->
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('.chosen').select2({
            placeholder: "Silahkan pilih",
            allowClear: true
        });
    });
</script>
@endsection