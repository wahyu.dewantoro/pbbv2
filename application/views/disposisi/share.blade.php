<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')
<h1>
    Internal Pedanil
</h1>
<!-- <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
    <li class="active">Dokumen</li>
</ol> -->
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
  
      <section class="content">
          <div class="box">
            <div class='box-header with-border'>
              <h3 class='box-title'> <i class="fa fa-globe"></i> Ojek Pajak</h3>
              <div class="pull-right">
                <?php if($button=='1'){?>
                    <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs"><i class="fa fa-send"></i> Kirim</a>
                <?php } ?>
              </div>
            </div>
            <div class="box-body">
                <table class="table"  style="font-size:12px">  
                    <tbody>
                      <tr>
                        <td>No Layanan</td>
                        <td>:</td>
                        <td><?php echo $rk->NO_LAYANAN?></td>
                      </tr>
                      <tr>
                        <td>NOP</td>
                        <td>:</td>
                        <td><?php echo $rk->NOP?></td>
                      </tr>
                      <tr>
                        <td>Nama WP</td>
                        <td>:</td>
                        <td><?php //echo $this->Mpermohonan->getNamaWP($rk->NOP)?></td>
                      </tr>
                      
                      <tr>
                        <td>Layanan</td>
                        <td>:</td>
                        <td><?php echo $rk->LAYANAN?></td>
                      </tr>
                      <tr>
                        <td>Tahun Pajak  </td>
                        <td>:</td>
                        <td><?php echo $rk->TAHUN?></td>
                      </tr>
                      <tr>
                        <td>Catatan  </td>
                        <td>:</td>
                        <td><?php echo $rk->CATATAN_PST?></td>
                      </tr>
                    </tbody>
                </table>
            </div>
          </div>     
      </section>
  </div>
  <div class="col-md-8">
      <section class="content">
          <div class="box">
            <div class='box-header with-border'>
              <h3 class='box-title'><i class="fa fa-history"></i> Riwayat Dokumen</h3>
              <div class="pull-right">
              <a href="<?php echo $_SERVER['HTTP_REFERER']?>" class="btn btn-xs btn-success"><i class="fa fa-list"></i> List</a>
              </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Posisi</th>
                        <th>Status</th>
                        <th>Keterangan</th>
                        <!-- <th>Aksi</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(count($rn)>0){ 
                       $no=1; foreach($rn as $rn){   ?>
                       <tr>
                         <td align="center"><?php echo $no++;?></td>
                         <td><?php echo $rn->AWAL.' - '.$rn->AKHIR?></td>
                         <td><?php echo $rn->POSISI?></td>
                         <td><?php echo $rn->STATUS_BERKAS?></td>
                         <td><?php echo $rn->KETERANGAN ?></td>
                         <!-- <td></td> -->
                       </tr>
                      <?php  } }else{?>
                        <tr>
                          <td colspan="5">Tidak ada data</td>
                        </tr>

                       <?php  } ?>
                    </tbody>
                </table>
            </div>
          </div>     
      </section>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url()."disposisi/proseskirimdokumen"?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Pelimpahan Dokumen</h4>
      </div>
      <div class="modal-body">
          <?php $exp=explode('.',$rk->NO_LAYANAN);
                $asp=explode('.', $rk->NOP);
          ?>
          <input type='hidden' name='KD_KANWIL' value='01'>
          <input type='hidden' name='KD_KANTOR' value='01'>
          <input type='hidden' name='THN_PELAYANAN' value='<?php echo $exp[0]?>'>
          <input type='hidden' name='BUNDEL_PELAYANAN' value='<?php echo $exp[1]?>'>
          <input type='hidden' name='NO_URUT_PELAYANAN' value='<?php echo $exp[2]?>'>
          <input type='hidden' name='KD_PROPINSI_RIWAYAT' value='<?php echo $asp[0]?>'>
          <input type='hidden' name='KD_DATI2_RIWAYAT' value='<?php echo $asp[1]?>'>
          <input type='hidden' name='KD_KECAMATAN_RIWAYAT' value='<?php echo $asp[2]?>'>
          <input type='hidden' name='KD_KELURAHAN_RIWAYAT' value='<?php echo $asp[3]?>'>
          <input type='hidden' name='KD_BLOK_RIWAYAT' value='<?php echo $asp[4]?>'>
          <input type='hidden' name='NO_URUT_RIWAYAT' value='<?php echo $asp[5]?>'>
          <input type='hidden' name='KD_JNS_OP_RIWAYAT' value='<?php echo $asp[6]?>'>
          <input type='hidden' name='NIP' value='<?php echo $CI->session->userdata('nip')?>'>
          <input type='hidden' name='TANGGAL_AWAL' value='<?php echo date('Y-m-d H:i:s');?>'>
          <input type='hidden' name='TANGGAL_AKHIR' value='<?php echo date('Y-m-d H:i:s');?>'>
          <input type="hidden" name="KODE_GROUP" value="41">
          <input type="hidden" name="STATUS_BERKAS" value="Proses">
          <div class="form-group">
              <label>Pegawai</label>
              <select class="form-control"  name="NIP_DISPOSISI" required>
                  <option value="">Pilih</option>
                  <?php foreach($unit as $unit){?>
                  <option value="<?php echo $unit->NIP?>"><?php echo $unit->NAMA_PENGGUNA?></option>
                  <?php }?>
              </select>
          </div>
          <div class="form-group">
            <label>Tanggal Mulai</label>
            <input type="text" name="TGL_AWAL" class="form-control tanggal" value="<?php echo date('Y-m-d')?>">
          </div>
          <div class="form-group">
            <label>Tanggal Prakiraan Selesai</label>
            <input type="text" name="TGL_PERKIRAAN_SELESAI" class="form-control tanggal" value="<?php echo  date('Y-m-d', strtotime('+3 days', strtotime(date('Y-m-d')))); ?>">
          </div>
          
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="Submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>

 @endsection