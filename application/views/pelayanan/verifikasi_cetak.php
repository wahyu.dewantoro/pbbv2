<html>

<head>
    <style>
        body {
            /* font-family: Courier New, Courier, monospace; */
            font-family: 'Times New Roman', Times, serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }
        }

        p {
            margin: 5px;
        }

        @page {
            margin-top: 10mm;
            margin-bottom: 10mm;
            margin-left: 10mm;
            margin-right: 10mm;
        }

        .tengah {
            text-align: center;
        }

        .kanankiri {
            text-align: justify;
        }

        th,
        td {
            vertical-align: top;
        }
    </style>
</head>

<body>
    <table width="100%">
        <tbody>
            <tr>
                <td rowspan="3"><img width="60px" src="<?= base_url('malang.png') ?>"></td>
                <td align="center">PEMERINTAH KABUPATEN MALANG</td>
            </tr>
            <tr>
                <td align="center"><b>BADAN PENDAPATAN DAERAH</b></td>
            </tr>
            <tr>
                <td align="center">JL. K.H. AGUS SALIM 7 TELEPON (0341)367994 FAX (0341)355708<br> MALANG - 65119</td>
            </tr>
        </tbody>
    </table>
    <hr style="margin: 5px;">
    <p class="tengah"><b>LEMBAR PENELITIAN PERSYARATAN PERMOHONAN <?= $nama_permohonan ?> YANG DI AJUKAN SECARA <?= $jumlah_sppt>1?'KOLEKTIF':'INDIVIDU' ?></b>
    <!-- <p class="tengah"><b>NOMOR : </b></p> -->
    <p><strong>I. Surat Permohonan</strong></p>
    <table>
        <tr>
            <td width="200px">Kode Berkas </td>
            <td>:</td>
            <td><?= $verifikasi->NO_PERMOHONAN ?></td>
        </tr>
        <tr>
            <td width="200px">Tanggal Terima Surat</td>
            <td>:</td>
            <td><?= $verifikasi->TGL_PERMOHONAN ?></td>
        </tr>
        <tr>
            <td width="200px">Nomer Pelayanan </td>
            <td>:</td>
            <td><?= $verifikasi->THN_PELAYANAN . '.' . $verifikasi->BUNDEL_PELAYANAN . '.' . $verifikasi->NO_URUT_PELAYANAN ?></td>
        </tr>
    </table>



    <p><strong>II. Identitas Pemohon/Kuasa</strong></p>
    <table>
        <tr>
            <td width="200px">Nama</td>
            <td>:</td>
            <td><?= $verifikasi->NAMA_PEMOHON  ?></td>
        </tr>
        <tr>
            <td width="200px">Alamat</td>
            <td>:</td>
            <td><?= $verifikasi->ALAMAT_PEMOHON ?></td>
        </tr>
    </table>
    <p><strong>III. Daftar Pemohon Kolektif</strong></p>
    <table>
        <tr>
            <td width="200px">Jumlah SPPT PBB</td>
            <td>:</td>
            <td><?= $jumlah_sppt ?></td>
        </tr>
        <tr>
            <td width="200px">Tahun Pajak</td>
            <td>:</td>
            <td><?= $verifikasi->TAHUN_PAJAK ?></td>
        </tr>
    </table>
    <p><strong>IV. Penelitian Persyaratan</strong></p>
    <table width="100%" style="border-collapse: collapse;  border: 1px solid black;">
        <tr>
            <th style="  border: 1px solid black;" width="30px">No</th>
            <th style="  border: 1px solid black;">Uraian</th>
            <th style="  border: 1px solid black;" width="50px">Ya</th>
            <th style="  border: 1px solid black;" width="50px">Tidak</th>
            <th style="  border: 1px solid black;" width="200px">Keterangan</th>
        </tr>
        <?php foreach ($berkas as $rb) { ?>
            <tr>
                <td style=" text-align:center;  border: 1px solid black;" width="30px"><?= $rb->KODE ?></td>
                <td style="  border: 1px solid black;"><?= $rb->NAMA_BERKAS ?></td>
                <td style="text-align:center;  border: 1px solid black;" width="50px"><?php echo  $rb->YA == 1 ? 'V' : ''; ?></td>
                <td style="text-align:center;  border: 1px solid black;" width="50px"><?php echo  $rb->TIDAK == 1 ? 'V' : ''; ?></td>
                <td style="  border: 1px solid black;" width="200px"></td>
            </tr>
        <?php $no = $rb->KODE;
        } ?>
        <tr>
            <td style="text-align:center;  border: 1px solid black;" width="30px"><?= $no + 1 ?></td>
            <td style="  border: 1px solid black;">surat keterangan Desa yang menyatakan alasan pengahpusan obyek pajak</td>
            <td style="  border: 1px solid black;" width="50px">SKPOP</td>
            <td style="  border: 1px solid black;" width="50px">NOTKOP</td>
            <td style="  border: 1px solid black;" width="200px"></td>
        </tr>
    </table>
    <p><strong>V. Berdasarkan penlitian persyaratan sebagaimana dimaksud pada angka romawi IV, permohonan <?= $nama_permohonan ?> dimaksud  <?= $verifikasi->PELIMPAHAN=='0'?'Tidak Memenuhi':'Memenuhi' ?>   persyaratan.</strong></p>
    <table width="100%">
        <tr>
            <td width="50%" align="center">
                Mengetahui,<br>
                Kasubid Pelayanan<br>
                <br>
                <br>
                <br><br>
                <b><?= $verifikasi->NAMA_KASUBID ?></b><br>
                NIP. <?= $verifikasi->NIP_KASUBID ?>
            </td>
            <td align="center">
                Malang, <?= $verifikasi->TGL_PENELITIAN ?><br>
                Petugas Peneliti<br>
                <br>
                <br>
                <br>
                <br>
                <b><?= $verifikasi->NAMA_PENELITI ?></b><br>
                NIP. <?= $verifikasi->NIP_PENELITI ?>
            </td>
        </tr>
    </table>

</body>

</html>