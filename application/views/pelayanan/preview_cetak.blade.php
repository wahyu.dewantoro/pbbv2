@extends('page.master')
@section('judul')
<h1>
    Preview Cetak

    <div class="pull-right">
        <!-- <button class="btn btn-info">Kembali</button> -->
        <?= anchor('permohonan/verifikasi_berkas','Kembali','class="btn btn-sm btn-info"') ?>
    </div>
</h1>
@endsection
@section('content')
<div class="box">
    <div class="">
        <iframe height="400px" id="" width=100% src="<?= $url ?>"></iframe>
    </div>
</div>
@endsection
@script('script')
<script>
    function resizeIFrameToFitContent(iFrame) {

        iFrame.width = iFrame.contentWindow.document.body.scrollWidth;
        iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
    }

    window.addEventListener('DOMContentLoaded', function(e) {

        var iFrame = document.getElementById('iFrame1');
        resizeIFrameToFitContent(iFrame);

        // or, to resize all iframes:
        var iframes = document.querySelectorAll("iframe");
        for (var i = 0; i < iframes.length; i++) {
            resizeIFrameToFitContent(iframes[i]);
        }
    });
</script>
@endscript