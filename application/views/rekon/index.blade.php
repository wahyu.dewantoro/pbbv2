<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')
<h1>
    Data Excek dari tempat pembayaran
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data</h3>

        <div class="box-tools">
            <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                <input type="text" name="cari" value="{{ $q }}" id="cari" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                    <button type="button" id="btncari" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <span id="table_data"></span>
    </div>
    <!-- /.box-body -->
</div>


@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        var ss = "<?= $start ?>";
        if (ss != '0') {
            fetch_data(ss);
        } else {
            fetch_data(0);
        }


        $(document).on('click', '.page-link', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('start=')[1];

            // alert(page)
            fetch_data(page);
        });


        $('#btncari').click(function(e) {
            fetch_data(0);
        });

        $("#cari").keyup(function(e) {
            if (e.keyCode == 13) {
                fetch_data(0);
            }
        });

        function fetch_data(page) {
            Swal.fire({
                imageUrl: "<?= base_url('loading.gif') ?>",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false,
                heightAuto: false
            });
            q = $('#cari').val();
            $('.loading').show();
            $.ajax({
                url: "{{ base_url('rekon/datarekon_data') }}?start=" + page + "&q=" + q,
                success: function(data) {
                    $('#table_data').html(data);
                    // $('.loading').hide();
                    swal.close();
                },
                error: function(e) {
                    Swal.fire({
                        icon: 'error',
                        title: 'opps',
                        text: 'Sistem error ,kontak administrator'
                    });
                }
            });
        }

    });
</script>
@endsection