<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')
<h1>
    Rekon Pembayaran SPPT
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua">
                <img src="<?= base_url('logo/realisasi.png') ?>" alt="" width="70px">
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Realisasi</span>
                <div id="realisasi">
                    <img width="120px" src="<?= base_url() ?>loading3.gif">
                </div>

            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box ">
            <span class="info-box-icon bg-white">
                <img src="<?= base_url('logo/bj.png') ?>" alt="" width="70px">
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Pembayaran BJ</span>
                <div id="bj">
                    <img width="120px" src="<?= base_url() ?>loading3.gif">
                </div>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon  bg-white">
                <img src="<?= base_url('logo/bni.png') ?>" alt="" width="70px">
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Pembayaran BNI</span>
                <div id="bni">
                    <img width="120px" src="<?= base_url() ?>loading3.gif">
                </div>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-white">
                <img src="<?= base_url('logo/pos.png') ?>" alt="" width="70px">
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Pembayaran POS</span>
                <div id="pos">
                    <img width="120px" src="<?= base_url() ?>loading3.gif">
                </div>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-6 ">
        <div class="box box-success">
            <div class="box-body">
                <form role="form" id="form" method="post" action="#" enctype="multipart/form-data">
                    <div class="form-group has-feedback">
                        <label for="">Tempat pembayaran</label>
                        <div class="row">
                            @foreach($bank as $rb)
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="kode_bank" id="kode_bank" value="{{ $rb->KODE_BANK }}">
                                        {{ $rb->NAMA_BANK }}
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">File Pembayaran</label>
                        <input type="file" name="data_excel" id="data_excel" class="form-control" required="">
                    </div>
                    <button type="submit" id="upload" class="btn btn-success"><i class="fa fa-upload"></i> Preview</button>
                </form>
            </div>

            <div id="preview"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header ui-sortable-handle">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">Pembayaran Harian</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <select name="kode_bank2" id="kode_bank2" class="form-control">
                            @foreach($bank as $bb)
                            <option <?php $kb = $kb ?? '114';
                                    if ($kb == $bb->KODE_BANK) {
                                        echo "selected";
                                    } ?> value="<?= $bb->KODE_BANK ?>"><?= $bb->NAMA_BANK ?></option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="bulan2" id="bulan2">
                            <?php for ($bul = 1; $bul <= 12; $bul++) { ?>
                                <option value="<?php echo $kb = sprintf("%02d",  $bul); ?>" <?php if ($kb == date('m')) {
                                                                                                echo "selected";
                                                                                            } ?>><?= bulan($bul) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="tahun2" id="tahun2">
                            <?php for ($thn = date('Y'); $thn >= 2020; $thn--) { ?>
                                <option value="<?= $thn ?>"><?= $thn ?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped" id="tabledatarekon" style="padding:3px">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Tanggal</th>
                                <th>Pembayaran</th>
                                <th>Rekon</th>
                            </tr>
                        </thead>
                        <tbody id="datarekon">
                            <tr>
                                <td align="center" colspan="4"><img width="100px" src="<?= base_url() ?>loading2.gif"> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>plugins/iCheck/square/blue.css">
<style>
    .ui-datepicker-calendar {
        display: none;
    }

    .ui-widget {
        font-size: .7em;
    }
</style>
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('lte/') ?>plugins/iCheck/icheck.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });

    function formatnop(objek) {
        // a = objek.value;
        b = objek.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 2) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 2 && panjang <= 4) {
            // 07. -> 2,2
            c = b.substr(0, 2) + '.' + b.substr(2, 2);
        } else if (panjang > 4 && panjang <= 7) {
            // 123 -> 4,3
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
        } else if (panjang > 7 && panjang <= 10) {
            // .123. ->
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
        } else if (panjang > 10 && panjang <= 13) {
            // 123.
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
        } else if (panjang > 13 && panjang <= 17) {
            // 1234
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
        } else {
            // .0
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
            // alert(panjang);
        }
        // objek.value = c;
        return c;
    }




    function formatRupiah(angka = 0) {
        if (angka == null) {
            angka = 0;
        }
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;

    }


    Rekonharian($('#tahun2').val(), $('#bulan2').val(), $('#kode_bank2').val());

    $("#tahun2, #bulan2, #kode_bank2").change(function() {
        // loading e
        $('#datarekon').html('<tr><td align="center" colspan="4"><img width="100px" src="<?= base_url() ?>loading2.gif"></td></tr>');
        Rekonharian($('#tahun2').val(), $('#bulan2').val(), $('#kode_bank2').val());
    });



    //list pembayaran rekon
    function Rekonharian(tahun, bulan, bank) {
        $.ajax({
            url: '<?php echo site_url("rekon/rekonharianbank") ?>',
            dataType: 'json',
            data: {
                kode_bank: bank,
                bulan: bulan,
                tahun: tahun
            },
            success: function(response) {
                var tr_ = "";

                $.each(response, function(i, item) {
                    tr_ += "<tr><td align='center'>" + (i + 1) + "</td><td>" + item.TANGGAL + "</td><td>" + formatRupiah(item.SPPT_BAYAR) + "<br><small class='text-info'>" + formatRupiah(item.JUMLAH_BAYAR) + "</small></td><td>" + formatRupiah(item.REKON) + "<br><small class='text-info'>" + formatRupiah(item.JUMLAH_REKON) + "</small></td></tr>";
                });
                $('#datarekon').html(tr_);

            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    year = "<?= date('Y') ?>";

    RekonGlobal('realisasi', year, null);
    RekonGlobal('bj', year, '114');
    RekonGlobal('bni', year, '009');
    RekonGlobal('pos', year, '991');
    // rekon bayar global
    function RekonGlobal(obj, tahun, bank = null) {
        $.ajax({
            url: '<?php echo site_url("rekon/rekonGlobal") ?>',
            dataType: 'json',
            data: {
                kode_bank: bank,
                tahun: tahun
            },
            success: function(response) {
                $('#' + obj).html('<span class="info-box-number" style="font-size: 12px;">' + formatRupiah(response.JUMLAH_BAYAR) + '</span>' +
                    '<span class="info-box-text">Rekon</span>' +
                    '<span class="info-box-number" style="font-size: 12px;">' + formatRupiah(response.JUMLAH_REKON) + '</span>');
            },
            error: function(data) {
                console.log(data);
                $('#' + obj).html('');
            }
        });
    }

    // rekon/prosesimport
    $(document).on('click', '#upload', function(e) {
        e.preventDefault();
        radio = $('input:radio[name=kode_bank]:checked').val();

        // if radio hv checked
        if (radio == undefined || $('#data_excel').val() == '') {
            Swal.fire({
                title: 'Peringatan',
                text: 'Tempat pembayaran / File  harus di isi',
                icon: 'warning',
                showConfirmButton: true,
                allowOutsideClick: false,
            })
            return false;
        }

        var file_data = $('#data_excel').prop('files')[0];
        var form_data = new FormData();
        form_data.append('data_excel', file_data);
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("rekon/prosesimport") ?>',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    var tr = "";
                    var names = ['NOP', 'NAMA', 'THN_PAJAK', 'POKOK', 'DENDA', 'JUMLAH', 'TGL'];

                    input_ = '';
                    $.each(response.data, function(i, item) {
                        input_ += '<input type="hidden" value="' + $('#kode_bank').val() + '" name="kode_bank[]">';
                        tr += '<tr>';
                        $.each(item, function(a, cell) {
                            console.log(a);
                            input_ += '<input type="hidden" value="' + cell + '" name="' + names[a] + '[]">';
                            if (a == '0') {
                                tr += '<td>' + formatnop(cell) + '</td>';
                            } else if (a == 5 || a == 3 || a == 4) {
                                tr += '<td>' + formatRupiah(cell) + '</td>';
                            } else {
                                tr += '<td>' + cell + '</td>';
                            }

                        });
                        tr += '</tr>';
                    });

                    preview_ = '<form  role="form" method="post" action="<?= base_url('rekon/submitrekon') ?>" enctype="multipart/form-data"><div class="box">' +
                        '<div class="box-header"><div class="box-title">Preview Data Excel</div><div class="pull-right"><button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button></div></div>' +
                        '<div class="box-body"><div class="table-responsive">' +
                        input_ +
                        '<table class="table table-bordered" id="table">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>NOP</th>' +
                        '<th>NAMA</th>' +
                        '<th>TAHUN PAJAK</th>' +
                        '<th>POKOK</th>' +
                        '<th>DENDA</th>' +
                        '<th>JUMLAH</th>' +
                        '<th>TANGGAL</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody >' +
                        tr +
                        '</tbody>' +
                        '</table>' +
                        '</div>' +
                        '</div></div></form>';

                    $("#preview").html(preview_);

                    $('#table').DataTable({
                        ordering: false
                    });
                } else {
                    $("#preview").html('');
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>
@endsection