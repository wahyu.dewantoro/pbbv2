<table class="table  table-bordered">
    <thead class="bg-info text-white">
        <tr>
            <th>No</th>
            <th style="text-align:center">Kode Pembayaran</th>
            <th style="text-align:center">NOP</th>
            <th style="text-align:center">Tahun Pajak</th>
            <th style="text-align:center">Wajib Pajak</th>
            <th style="text-align:center">Pokok</th>
            <th style="text-align:center">Denda</th>
            <th style="text-align:center">Jumlah</th>
            <th style="text-align:center">Tanggal Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($total_rows > 0) {
            $no=$start+1;
            foreach ($data as $rk) { ?>
                <tr>
                <td><?=  $no++ ?></td>
                    <td><?= $rk->KODE_BANK ?></td>
                    <td><?= formatnop($rk->NOP) ?></td>
                    <td><?= $rk->TAHUN_PAJAK ?></td>
                    <td><?= $rk->NAMA ?></td>
                    <td align="right"><?= angka($rk->POKOK) ?></td>
                    <td align="right"><?= angka($rk->DENDA) ?></td>
                    <td align="right"><?= angka($rk->JUMLAH) ?></td>
                    <td align="center"><?= $rk->TANGGAL ?></td>
                </tr>
            <?php  }
        } else { ?>
            <tr>
                <th colspan="8"> Tidak ada data.</th>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div class="row">
    <div class="col-md-6">
        <button class="btn btn-space btn-secondary"><span> Record : <?php echo angka($total_rows) ?></span></button>
    </div>
    <div class="col-md-6">
        <div class="pull-right">
            <?php echo $pagination ?>
        </div>
    </div>
</div>