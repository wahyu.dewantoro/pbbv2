<!DOCTYPE html>
<html>
@include('page.head')

<body class="hold-transition skin-green sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    @include('page.header')
    @include('page.left')
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('judul')
      </section>
      <section class="content">
        @yield('content')
      </section>
    </div>
    <!-- /.content-wrapper -->

    @include('page.footer')
    <!-- <div class="control-sidebar-bg"></div> -->
    <div data-flashdata="<?= getFlashdata('notif') ?>" id='flashdata'></div>
  </div>
  @include('page.script')
  @yield('script')
</body>

</html>