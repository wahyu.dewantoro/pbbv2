<!DOCTYPE html>
<html>
@include('page.head')
<style>
  .content-wrapper{
    background-image: url("pendopo.jpg");
  background-color: #cccccc;
  background-repeat: no-repeat;
  background-size: cover;
  }
</style>

<body class="hold-transition skin-blue fixed sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    @include('page.header')

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    @include('page.left')

    <div class="content-wrapper"> 
      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('judul')
      </section>
      <section class="content">
        @yield('content')
      </section>
    </div>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2
      </div>
      <strong>Copyright  <a href="sipanji.id">SIPANJI</a>.</strong> All rights
      reserved.
    </footer>
 
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  @include('page.script')
  @yield('script')
</body>

</html>