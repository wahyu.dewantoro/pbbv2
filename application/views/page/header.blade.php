<?php
$CI = &get_instance();
$CI->load->library('session');
?>
<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>PBB</b>P2</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>PBB</b> P2</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">


                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('lte/') ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= inisial($CI->session->userdata('pbb_nama')); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= base_url('lte/') ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                <?= $CI->session->userdata('pbb_nama'); ?>
                                <small>.:<?= $CI->session->userdata('pbb_ng') ?>:. <br> <?php $tt = $CI->session->userdata('unit_kantor');
                                                                                        if ($tt == 'kota') {
                                                                                            echo  "Tempat Layanan: Pendopo";
                                                                                        } else {
                                                                                            echo "Tempat Layanan: Kepanjen";
                                                                                        } ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= base_url('auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
    </nav>
</header>