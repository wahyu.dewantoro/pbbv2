<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?= base_url('lte/') ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('lte/') ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('lte/') ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('lte/') ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('lte/') ?>dist/js/demo.js"></script>
<!-- swall -->
<script src="<?= base_url() ?>swal/sweetalert2.all.min.js"></script>
<script>
  $(document).ready(function() {
    //format nop



    $('.sidebar-menu').tree();
    // swal
    const flashData = $('#flashdata').data('flashdata');
    if (flashData) {
      Swal.fire({
        title: 'Notifikasi',
        text: flashData,
        icon: 'success',
        showConfirmButton: false,
        allowOutsideClick: false,
        timer: 3000,
      })
    }
  });

  $(function() {
    $('.sidebar-menu a[href~="' + location.href + '"]').parents('li').addClass('active');
  });

  function formatformulir(a) {
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    if (panjang <= 4) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 4 && panjang <= 8) {
      // 07. -> 2,2
      c = b.substr(0, 4) + '.' + b.substr(4, 4);
    } else {
      c = b.substr(0, 4) + '.' + b.substr(4, 4) + '.' + b.substr(8, 3);
    }
    return c;
  }

  function formatnop(a) {
    // a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    // objek.value = c;
    return c;
  }

  function angka_koma(objek) {
    // var currentInput = objek.value;
    var currentInput = objek;
    var fixedInput = currentInput.replace(/[A-Za-z!@#$%^&*().]/g, '');
    // objek.value = fixedInput;
    console.log(fixedInput);
    return fixedInput;

  }
</script>