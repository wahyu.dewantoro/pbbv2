<?php
$CI = &get_instance();
$CI->load->library('session', 'database');
?>


<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url('lte/') ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= $CI->session->userdata('pbb_nama'); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->

    <ul class="sidebar-menu" data-widget="tree">
      
      <?php
      $kode_group = $CI->session->userdata('pbb_kg');
      $CI->db->cache_off();
      $qmenu0  = $CI->db->query("SELECT a.*
                                                  FROM p_menu a
                                                  JOIN p_role b ON a.KODE_MENU=b.KODE_MENU
                                                  WHERE kode_group='$kode_group' AND status_role='1'  AND active='1' and parent_menu='0' order by sort_menu asc")->result_array();
      foreach ($qmenu0 as $row0) {
        $parent  = $row0['KODE_MENU'];
        $CI->db->cache_off();
        $qmenu1  = $CI->db->query("SELECT a.*
                                                  FROM p_menu a
                                                  JOIN p_role b ON a.KODE_MENU=b.KODE_MENU
                                                  WHERE kode_group='$kode_group' AND status_role='1'  AND active='1' and parent_menu='$parent' order by sort_menu asc");
        $cekmenu = $qmenu1->num_rows();
        $dmenu1  = $qmenu1->result_array();
        // " . $row0['LINK_MENU'] . "
        if ($cekmenu > 0) {
          echo "<li class='treeview'>
                              <a href='#' > <i class='" . $row0['ICON_MENU'] . "'></i>  <span>" . ucwords($row0['NAMA_MENU']) . "</span> <span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i> </span></a>";
          echo "<ul class='treeview-menu'>";
          foreach ($dmenu1 as $row1) {
            echo "<li>" . anchor($row1['LINK_MENU'], '<i class="fa fa-circle-o"></i>  ' . ucwords($row1['NAMA_MENU'])) . "</li>";
          }
          echo "</ul>
                              </li>";
        } else {
          echo "<li>" . anchor($row0['LINK_MENU'], " <i class='" . $row0['ICON_MENU'] . "'></i> <span>" . ucwords($row0['NAMA_MENU'])) . "</span></li>";
        }
      }
      ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>