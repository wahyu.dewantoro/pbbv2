<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Import Dafnom.
</h1>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <form role="form" id="form" method="post" action="#" enctype="multipart/form-data">
            <div class="box-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="input-group input-group-md" style="margin-bottom: 5px">
                    <input type="file" name="data_excel" id="data_excel" class="form-control" required>
                        <span class="input-group-btn">
                        <button type="submit" id="upload" class="btn btn-info btn-flat"><i class="fa fa-upload"></i> Preview</button>
                        </span>
                    </div>
                    <a href="<?= base_url('Format_Import_Daftar_Nominative.xlsx')?>" class="btn btn-success btn-block">Download Template</a> 
                </div>
                <div class="col-md-7">
                    <ol>
                        <li>Pastikan file excel yg di import berjenis *.xlsx</li>
                        <li>Format file Excel harus sama dengan format yang telah di sediakan</li>
                        <li>Maksimal data yg di proses dalam satu kali proses import Excel tidak lebih dari 700 data</li>
                    </ol>
                </div>
                <!-- <div class="col-md-2"></div>
                <div class="col-md-6" style="text-align: right">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-md" style="margin-bottom: 10px">
                                <input type="text" name="total_data" id="total_data" class="form-control" readonly>
                                    <span class="input-group-btn">
                                    Total Data
                                    </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group input-group-md" style="margin-bottom: 10px">
                            <input type="text" name="data_terupload" id="data_terupload" class="form-control" readonly>
                                <span class="input-group-btn">
                                <button class="btn btn-success btn-flat">Data Diupload</button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group input-group-md" style="margin-bottom: 10px">
                            <input type="text" name="tidak_terupload" id="tidak_terupload" class="form-control" readonly>
                                <span class="input-group-btn">
                                <button class="btn btn-warning btn-flat">Data Tidak Valid</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div> -->
                
            </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    <?php 
        // if(isset($_SESSION['notif'])) {
        //     if($_SESSION['notif']=='0'){
    ?>
        <!-- <div id="div_peringatan" class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Peringatan</h4>
            Data Kecamatan dan Kelurahan yang terdapat pada file upload tidak termasuk ke dalam UPT yang bersangkutan !
        </div> -->
    <?php
        //     }
        // }
    ?>
      <div id="preview"></div>
    </div>
    <div class="col-md-6">
        <div id="data_double"></div>
    </div>
</div>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    function formatRupiah(angka = 0) {
        if (angka == null) {
            angka = 0;
        }
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;

    }
    $(document).on('submit', '#submitdafnom', function(e) {
        e.preventDefault();
        var uri=$(this).attr("action"),
        form_data=$(this).serializeArray(),
        lengthData=Math.floor((form_data.length-1)/6),
        requiredLength=700;
        if(lengthData>requiredLength){
            let txt='Data Melebihi kapasitas Proses Validasi Kode Biling \n  Maksimal Proses Validasi '+requiredLength+' data.';
            return Swal.fire('Peringatan!',txt,"warning");
        }
        Swal.fire({
                    title: 'Peringatan!',
                    text:  'Apakah anda yakin membuat Kode Billing? \n dengan jumlah '+lengthData+" Nop",
                    icon: 'warning',
                    allowOutsideClick: false,
                }).then((res) => {
            if (res) {
                Swal.fire({
                    title: 'Mohon Tunggu beberapa saat \n Sedang melakukan proses validasi kode billing',
                    html:'<div class="fa-3x"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                $.ajax({
                    url: uri,
                    type: 'POST',
                    data: form_data,
                    success:function(result){
                        swal.fire('Pesan',result,'success').then(()=>{
                            window.location.href = "<?= base_url('dafnom/index')?>";
                        });
                    },
                    error:function(){
                        swal.fire('Peringatan!',result,'warning');
                    }
                });
            };
        });
    });
    $(document).on('click', '#upload', function(e) {
        e.preventDefault();

        var file_data = $('#data_excel').prop('files')[0];
        var form_data = new FormData();
        form_data.append('data_excel', file_data);
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("dafnom/prosesimport2") ?>',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    
                    $("#preview").html(response.preview);

                    // $("#subnop").on("click",function(){
                    //     $("#subnop").attr("disabled",true);
                    // });


                    $('#table').DataTable({
                        ordering: false
                    });

                    $("#data_double").html(response.data_double);

                    $('#table2').DataTable({
                        ordering: false
                    });
                } else {
                    $("#preview").html('');
                    $("#data_double").html('');
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                console.log(data.responseText);
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>
@endsection

