@extends('page.master')
@section('judul')
    <h1>
        Pembuatan Dafnom
        <div class="pull-right">
            <?= anchor('dafnom/import_excel', '<i class="fa fa-file-excel-o"></i> Import Excel', 'class="btn btn-sm btn-success"') ?>
        </div>
    </h1>
@endsection
@section('content')

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>

        </div>
        <!-- /.box-header -->

        <form id="cek_nop" action="<?= base_url($action_cek) ?>" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tahun Pajak</label>
                            <input <?= $status ?> required type="number" class="form-control" name="thn_pajak"
                                id="thn_pajak" onKeyPress="if(this.value.length==4) return false;" value="<?= $tahun ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <select <?= $status ?> required class="form-control change_kecamatan" name="kecamatan"
                                id="kecamatan">
                                <?php
                                if (isset($ref_kec) && count($ref_kec) > 1) {
                                    echo "<option value=''>Pilih</option>";
                                }
                                ?>
                                @foreach ($ref_kec as $a)
                                    <option <?php if (isset($kec)) {
    if ($kec == $a->KD_KECAMATAN) {
        echo 'selected';
    }
} ?> value="{{ $a->KD_KECAMATAN }}">{{ $a->NM_KECAMATAN }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label id="label_kel">Kelurahan</label>
                            <select <?= $status ?> required class="form-control kelurahan" name="kelurahan" id="kelurahan"
                                style="width: 100%;" <?php if (!isset($ref_kel)) {
    echo 'disabled';
} ?>>
                                <?php
                                if (isset($ref_kel) && count($ref_kel) > 1) {
                                    echo "<option value=''>Pilih</option>";
                                }
                                ?>
                                <?php if(isset($ref_kel)){
                    foreach ($ref_kel as $b) {
                  ?>
                                <option <?php if (isset($kel)) {
    if ($kel == $b->KD_KELURAHAN) {
        echo 'selected';
    }
} ?> value="{{ $b->KD_KELURAHAN }}">{{ $b->NM_KELURAHAN }}
                                </option>
                                <?php
                    }
                  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Blok</label>
                            <input <?= $status ?> type="number" class="form-control" name="blok" id="blok"
                                value="<?php if (isset($blok)) {
                                    echo $blok;
                                } ?>">
                            <span style="font-size: 10pt; color: red">*)<i>Blok bisa dikosongi</i> </span>
                            <!-- <input type="hidden" name="blok2" id="blok2" value="<?php if (isset($blok2)) {
    echo $blok2;
} ?>"> -->
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Rencana Bayar</label>
                            <input <?= $status ?> required type="text" class="form-control" name="batas_nominal"
                                id="batas_nominal" value="<?php if (isset($nominal)) {
    echo $nominal;
} ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>WP Belum Bayar</label>
                            <input disabled type="text" class="form-control" name="total_wp" id="total_wp"
                                value="<?php if (isset($list_nop)) {
                                    echo number_format(count($list_nop), 0, ',', '.');
                                } ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" name="btn_cek" class="btn btn-info pull-left" <?= $status_btn ?>>Cek</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">

                <form id="add_temp" action="<?= base_url($action_temp) ?>">
                    <div class="box-header with-border">
                        <h3 class="box-title">List NOP</h3>
                        <button id="tambah_dafnom" type="submit" class="btn btn-warning pull-right tambah_dafnom"
                            <?php if (isset($list_nop)) {
                                if (count($list_nop) == 0) {
                                    echo 'disabled';
                                }
                            } else {
                                echo 'disabled';
                            } ?>>Tambah</button>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div id="div_nop" style="text-align: center">
                            <?php if(isset($list_nop)){
                  if(count($list_nop)>0){
                    $datax = "";
                    $no = 1;
                    $cek_total = 0;
                    $total = 0;
                    
                    $batas_nominal = str_replace(',','',$nominal);
                    foreach ($list_nop as $a) {
                        $nop = $a->KD_PROPINSI.'.'.$a->KD_DATI2.'.'.$a->KD_KECAMATAN.'.'.$a->KD_KELURAHAN.'.'.$a->KD_BLOK.'.'.$a->NO_URUT.'.'.$a->KD_JNS_OP;
                        $cek_total += $a->TOTAL;
                        if($cek_total <= $batas_nominal){
                            $datax .= "
                            <tr>
                                <th style='text-align: center' id='nomer'>".$no++."</th>
                                <td><input type='checkbox' id='cek_satu[]' name='cek_satu[]' class='cek_satu' value='".$nop.'|'.str_replace("'", "`",$a->NM_WP_SPPT).'|'.$a->POKOK.'|'.$a->DENDA.'|'.$a->TOTAL.'|'.$a->THN_PAJAK_SPPT.'|'.$a->PBB.'|'.$nominal."'></td>
                                <td>".$nop."</td>
                                <td style='text-align: left'>".$a->NM_WP_SPPT."</td>
                                <td style='text-align: right' id='jml'>".number_format($a->TOTAL,0,',','.')."</td>
                            </tr>";
                            $total += $a->TOTAL;
                        }
                        
                    }
        
                    $data = "
                    <span style='float: left'><b><i>Jumlah NOP</i> &emsp;&emsp;: <span id='akumulasi_nop'>0</span></b></span><br>
                    <span style='float: left'><b><i>Akumulasi</i>&emsp;&emsp;&emsp;: Rp. <span id='akumulasi'>0</span></b></span><br>
                    <br>
                    <table id='tblNOP' class='table table-bordered table-striped'>
                    <thead>
                        <tr>
                            <th style='text-align: center'>No</th>
                            <th style='text-align: center'><input type='checkbox' id='cek_all' name='cek_all' class='cek_all' value='0' ></th>
                            <th style='text-align: center'>NOP</th>
                            <th style='text-align: center'>Nama WP</th>
                            <th style='text-align: center'>Nominal</th>
                        </tr>
                    </thead>
                    <tbody id='tbody_nop'>
                    ".$datax."
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan='4' style='text-align: center'>Total</th>
                        <th style='text-align: right'><span id='total'>".number_format($total,0,',','.')."</span><input type='hidden' id='total_nop' value='".($no-1)."'></th>
                    </tr>
                    </tfoot>
                    </table>
                    ";
                    
                    echo $data;
                  }else{
                      $data = "<span>Data belum ditemukan !</span>";
                      
                      echo $data;
                  }
                }else{
                ?>
                            <span>Data belum ditemukan !</span>
                            <?php
                }?>

                        </div>
                    </div>


                    <!-- <div class="box-footer">
                    <button type="submit" class="btn btn-warning pull-right tambah_dafnom" disabled>Tambah</button>
                    </div> -->
                    <!-- /.box-body -->

                    <!-- /.box-footer -->
                </form>
            </div>

        </div>
        <div class="col-md-6" id="div_dafnom">

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Nominative</h3>
                </div>
                <div class="box-body">
                    <div id="div_list_dafnom" style="text-align: center">
                        <?php
            if(isset($list_temp)){
              if(count($list_temp) > 0){
                $data_temp = "";
                $no_temp = 1;
                $total_temp = 0;
                foreach ($list_temp as $b) {
                    $nop_temp = $b->KD_PROPINSI.'.'.$b->KD_DATI2.'.'.$b->KD_KECAMATAN.'.'.$b->KD_KELURAHAN.'.'.$b->KD_BLOK.'.'.$b->NO_URUT.'.'.$b->KD_JNS_OP;
                    $nopx_temp = $b->KD_PROPINSI.$b->KD_DATI2.$b->KD_KECAMATAN.$b->KD_KELURAHAN.$b->KD_BLOK.$b->NO_URUT.$b->KD_JNS_OP;
                    $data_temp .= "
                        <tr id=''>
                            <th style='text-align: center'>".$no_temp++."</th>
                            <td>".$nop_temp."</td>
                            <td style='text-align: left'>".$b->NM_WP."</td>
                            <td style='text-align: right'>".number_format($b->TOTAL,0,',','.')."</td>
                            <td style='text-align: center'><button data-nop='".$nopx_temp."' class='hapus_nop btn btn-danger btn-xs'><i class='fa fa-trash'></i></button></td>
                        </tr>";
                    $total_temp += $b->TOTAL;
                    
                }
    
                $datax_temp = "
                <span style='float: left'><b><i>Jumlah NOP</i>&emsp;&emsp;: <span id='nop_terpilih'>".($no_temp-1)."</span></b></span><br>
                <span style='float: left'><b><i>Jumlah PBB</i>&emsp;&emsp;: <span id='jml_pbb'>Rp. ".number_format($total_temp,0,',','.')."</span></b></span><br><br>
                <table class='table table-bordered table-striped'>
                <thead>
                    <tr>
                        <th style='text-align: center'>No</th>
                        <th style='text-align: center'>NOP</th>
                        <th style='text-align: center'>Nama WP</th>
                        <th style='text-align: center'>Nominal</th>
                        <th style='text-align: center'>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                ".$data_temp."
                </tbody>
                <tfoot>
                <tr>
                    <th colspan='3' style='text-align: center'>Total</th>
                    <th style='text-align: right'>".number_format($total_temp,0,',','.')."</th>
                </tr>
                </tfoot>
                </table>
                ";

                echo $datax_temp;
              }else{
                echo "<span>Data belum ditemukan !</span>";
              } 
            }else{
            ?>
                        <span>Data belum ditemukan !</span>
                        <?php
            }
            ?>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" class="btn btn-danger pull-right cancel_dafnom"
                        <?php if ($jml_temp == 0) {
                            echo 'disabled';
                        } ?>>Batal</button>
                    <button type="submit" class="btn btn-success pull-left simpan_dafnom"
                        <?php if ($jml_temp == 0) {
                            echo 'disabled';
                        } ?>>Simpan</button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet"
        href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet"
        href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')

    <!-- DataTables -->
    <script type="text/javascript" src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript"
        src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script type="text/javascript"
        src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        function formatangka(objek) {
            a = objek.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + "." + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            objek.value = c;
        };



        $(document).on("submit", "#cek_nop", function(evt) {
            var e = $(this),
                dataForm = e.serializeArray();
            $('#div_nop').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");
            $('#div_list_dafnom').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");
            $.ajax({
                type: 'post',
                url: e.attr("action"),
                data: dataForm,
                success: function(response) {
                    if (response.listNop || response.listNop_temp) {
                        $("#tambah_dafnom").removeAttr("disabled");
                        $("#cek_nop button[type='submit']").attr("disabled", "disabled");
                        $("#cek_nop .form-control").attr("readonly", "readonly");
                        $("#cek_nop select.form-control").find('option').not(':selected').remove();
                    };
                    $("#div_nop").html(response.listNop);
                    $("#div_list_dafnom").html(response.listNop_temp);
                }
            });
            return false;
        });
        $(document).on("change", ".change_kecamatan", function(evt) {
            var val = $(this).val();
            $('#label_kel').html("Kelurahan &emsp;<img src='<?= base_url('logo/') ?>loading.gif' width='8%'/>");
            $.ajax({
                type: 'post',
                url: '<?= base_url('dafnom/fetch_kel') ?>',
                data: {
                    kd_kec: val
                },
                success: function(response) {
                    $('.kelurahan').prop("disabled", false);
                    document.getElementById("kelurahan").innerHTML = response;
                    $('#label_kel').html("Kelurahan");
                }
            });
        });
        $(document).on("click", "#cek_all", function(evt) {
            $('.cek_satu').prop('checked', this.checked);
            var total = $("#total").text();
            var total_nop = $("#total_nop").val();
            if (this.checked) {
                $("#akumulasi").text(total);
                $("#akumulasi_nop").text(total_nop);
            } else {
                $("#akumulasi").text("0");
                $("#akumulasi_nop").text("0");
            }
        });
        $(document).on("click", ".cek_satu", function(evt) {
            var hasil = total_nop = 0;
            $(".cek_satu:checked").each(function() {
                var value = $(this).val().split("|");
                hasil = parseInt(hasil) + parseInt(value[4]);
                total_nop = total_nop + 1;
            });
            var total = hasil.toString().split('.');
            total[0] = (parseInt(total[0], 10)).toLocaleString();
            $("#akumulasi").text(total.join('.').replace(",", "."));
            $("#akumulasi_nop").text(total_nop.toString());
            if ($("#cek_all:checked").length) {
                $("#cek_all").prop("checked", false);
            };
        });
        var myinput = document.getElementById('batas_nominal');
        myinput.addEventListener('keyup', function() {
            /*  
              val = val.replace(/[^0-9\.]/g,'');
              if(val != "") {
                valArr = val.split('.');
                valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
                val = valArr.join('.');
              }; */
            /* var val = formatangka(this.value);
            this.value = val; */

            a = this.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + "." + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            this.value = c;
        });
        $("#add_temp").submit(function(e) {
            var checked = $("#tblNOP input[type=checkbox]:checked").length;

            if (checked > 0) {

                e.preventDefault();

                var form = $(this);
                var url = form.attr('action');

                $('#div_list_dafnom').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    success: function(data) {

                        if (data == '0') {

                            $('.simpan_dafnom').prop("disabled", true);
                            $('.cancel_dafnom').prop("disabled", true);

                        } else {
                            //mengkosongkan list nop
                            // $("#tbody_nop").empty();
                            $("#akumulasi").text("0");
                            $("#akumulasi_nop").text("0");
                            $("#cek_all").prop("checked", false);
                            // $("#tbody_nop").html(data[0].list_nop);

                            //menampilkan data list dafnom
                            $('.simpan_dafnom').prop("disabled", false);
                            $('.cancel_dafnom').prop("disabled", false);
                            document.getElementById("div_list_dafnom").innerHTML = data;

                            var arr = [];
                            $('.cek_satu:checked').each(function() {
                                $(this).closest('tr').remove();
                            });

                            var TotalValue = 0;
                            $("tr #jml").each(function(index, value) {
                                var jml = $(this).text()
                                currentRow = parseInt(jml.replace(".", ""));
                                TotalValue += currentRow
                            });

                            i = 1
                            $("tr #nomer").each(function(index, value) {
                                $(this).text(i++)
                            });


                            var tot = TotalValue.toString()
                            if (tot != "") {
                                valArr = tot.split('.');
                                valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
                                tot = valArr.join('.');
                            }
                            $('#total').text(tot.replace(",", "."));
                        }
                    }
                });
            } else {
                alert("Silahkan pilih NOP terlebih dahulu !");
                return false;
            }

        });
        $(document).on("click", ".simpan_dafnom", function(evt) {
            var action = "<?= base_url($action) ?>";
            var thn_pajak = $('#thn_pajak').val();
            var kec = $('#kecamatan').val();
            var kel = $('#kelurahan').val();
            var blok = $('#blok').val();
            $.ajax({
                type: "POST",
                url: action,
                data: {
                    thn_pajak: thn_pajak,
                    kec: kec,
                    kel: kel,
                    blok: blok
                },
                success: function(result) {
                    if (result == 'sukses') {
                        var jml_nop = $('#nop_terpilih').text();
                        var pbb = $('#jml_pbb').text();
                        $('#div_list_dafnom').html('<span>Data belum ditemukan !</span>');
                        $('#div_nop').html('<span>Data belum ditemukan !</span>');
                        $('.simpan_dafnom').prop("disabled", true);
                        $('.tambah_dafnom').prop("disabled", true);
                        if (confirm("Berhasil Menyimpan NOP sejumlah " + jml_nop +
                                " WP dengan nilai PBB sejumlah " + pbb)) {
                            window.location.href = "<?= base_url('dafnom/index?thn_pajak=') ?>" +
                                thn_pajak;
                        }
                    } else {
                        alert("Gagal Menyimpan Data !");
                    };
                }
            });
        });
        $(document).on("click", ".cancel_dafnom", function(evt) {
            var thn_pajak = $('#thn_pajak').val();
            var kec = $('#kecamatan').val();
            var kel = $('#kelurahan').val();
            var nominal = $('#batas_nominal').val();
            var blok = '';
            if ($('#blok').val() != '') {
                blok = $('#blok').val();
            }
            $('#div_nop').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");
            $('#div_list_dafnom').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");

            $.ajax({
                type: "POST",
                url: "<?= base_url('dafnom/delete_temp2') ?>",
                dataType: 'json',
                global: false,
                cache: false,
                async: false,
                data: {
                    thn_pajak: '<?= $tahun ?>',
                    kec: kec,
                    kel: kel,
                    blok: blok,
                    nominal: nominal,
                },
                success: function(response) {
                    $("#cek_nop").trigger("submit");
                    // console.log(response);
                    //   $("#akumulasi").text("0");
                    //   $("#akumulasi_nop").text("0");
                    //   $("#cek_all").prop("checked", false);

                    //   //menampilkan data list dafnom
                    //   if(response.list_dafnom != '0'){
                    //     $('.simpan_dafnom').prop("disabled",false);
                    //     $('.cancel_dafnom').prop("disabled",false);
                    //     document.getElementById("div_list_dafnom").innerHTML=response.list_dafnom;
                    //   }else{
                    //     $('.simpan_dafnom').prop("disabled",true);
                    //     $('.cancel_dafnom').prop("disabled",true);
                    //     document.getElementById("div_list_dafnom").innerHTML='<span>Data belum ditemukan !</span>';
                    //   }


                    //   //menampilkan data list nop
                    //   document.getElementById("div_nop").innerHTML=response.list_nop;
                }
            });
        });
        $(document).on("click", ".hapus_nop", function(evt) {
            //console.log(nop_hapus);
            var nop_hapus = $(this).attr("data-nop");
            var thn_pajak = $('#thn_pajak').val();
            var kec = $('#kecamatan').val();
            var kel = $('#kelurahan').val();
            var nominal = $('#batas_nominal').val();
            var blok = $('#blok').val();
            $('#div_nop').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");
            $('#div_list_dafnom').html("<img src='<?= base_url('logo/') ?>loading.gif' width='15%'/>");

            $.ajax({
                type: "POST",
                url: "<?= base_url('dafnom/hapus_nop_temp') ?>",
                dataType: 'json',
                global: false,
                cache: false,
                async: false,
                data: {
                    thn_pajak: '<?= $tahun ?>',
                    kec: kec,
                    kel: kel,
                    blok: blok,
                    nominal: nominal,
                    nop_hapus: nop_hapus,
                },
                success: function(response) {
                    // console.log(response);
                    //   $("#akumulasi").text("0");
                    //   $("#akumulasi_nop").text("0");
                    //   $("#cek_all").prop("checked", false);

                    //   //menampilkan data list dafnom
                    //   if(response.list_dafnom != '0'){
                    //       $('.simpan_dafnom').prop("disabled",false);
                    //       $('.cancel_dafnom').prop("disabled",false);
                    //     document.getElementById("div_list_dafnom").innerHTML=response.list_dafnom;
                    //   }else{
                    //       $('.simpan_dafnom').prop("disabled",true);
                    //       $('.cancel_dafnom').prop("disabled",true);
                    //     document.getElementById("div_list_dafnom").innerHTML='<span>Data belum ditemukan !</span>';
                    //   }


                    //   //menampilkan data list nop
                    //   document.getElementById("div_nop").innerHTML=response.list_nop;
                    $("#cek_nop").trigger("submit");
                }
            });
        });



        // dafnom/add_tes
    </script>
@endsection
