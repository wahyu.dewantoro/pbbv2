@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Laporan Realisasi Pembayaran
</h1>
@endsection
@section('content')
<div class="box box-danger">
    <form action="<?= base_url('dafnom/realisasi_harian')?>" method="GET">
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group" style="margin-bottom: 0px; padding: 0px">
                        <label for="exampleInputEmail1">Tanggal Bayar</label>
                        <div class="input-group input-group-md">
                        <input required type="text" class="form-control pull-right tanggal" id="tgl_bayar" name="tgl_bayar" value="<?= $tgl_bayar ?>">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-warning btn-flat" name="btn_cari">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-10" style="text-align: right">
                <?php
                    if(isset($list_nop)){
                        if(count($list_nop)>0){
                ?>
                <!-- <a href="<?= base_url('dafnom/cetak_realisasi_harian_pdf/'.$tgl_bayar)?>" title="Cetak PDF" class="btn btn-sm btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a> -->
                <a href="<?= base_url('dafnom/cetak_realisasi_harian_excel/'.$tgl_bayar)?>" title="Cetak Excel" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel-o"></i></a>                
                <?php
                        }
                    }
                ?>
                </div>
            </div>
            
        </div>
    </form>
</div> 
<?php
    $total = 0;
    if(isset($list_nop)){
?>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="3%" style="text-align: center">No</th>
                    <th style="text-align: center">Kode Billing</th>
                    <th style="text-align: center">Kecamatan</th>
                    <th style="text-align: center">Kelurahan</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">Keterangan</th>
                    <th style="text-align: center">Tgl Buat</th>
                    <th style="text-align: center">JML NOP</th>
                    <th style="text-align: center">PBB</th>
                </tr>
            </thead>
           
            <tbody>
                <?php
                $no = 1;
                foreach ($list_nop as $a) {
                   
                ?>
                <tr>
                    <td align="center"><?= $no++; ?></td>
                    <td align="center"><?= $a->KOBIL; ?></td>
                    <td><?= $a->NAMA_KECAMATAN; ?></td>
                    <td><?= $a->NAMA_KELURAHAN; ?></td>
                    <td align="center"><?= $a->TAHUN_PAJAK ?></td>
                    <td align="left"><?= $a->NAMA_WP ?></td>
                    <td align="center"><?= $a->TGL_BUAT ?></td>
                    <td align="right"><?= number_format($a->JML_NOP,0,',','.') ?></td>
                    <td align="right"><?= number_format($a->PBB,0,',','.') ?></td>
                </tr>
                <?php
                $total += $a->PBB;
                }
                ?>
                
            </tbody>          
            <tfoot>
                <tr>
                    <th style="text-align: center" colspan="8">Total</th>
                    <td align="right"><?= number_format($total,0,',','.') ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php
    }
?>  


@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $('#example2').DataTable({
                            ordering: false,
                            "iDisplayLength": 10
                        });
    });
</script>

@endsection