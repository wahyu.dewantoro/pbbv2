<style type="text/css">
		body {
			font-family: Courier New, Courier, monospace;
			font-size: 11px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		}

		.inti {
			border: 1px solid #C6C6C6;
			margin: auto;
		}

		.inti td {
			border-right: 1px solid #C6C6C6;
		}

		.inti th {
			border-right: 1px solid #C6C6C6;
			border-bottom: 1px solid #C6C6C6;
		}
	</style>
	<style type="text/css" media="print">
		@page {
			size: 8.27in 11.69in; /* <length>{1,2} | auto | portrait | landscape */
	      	/* 'em' 'ex' and % are not allowed; length values are width height */
			margin: 5%; /* <any of the usual CSS values for margins> */
						/*(% of page-box width for LR, of height for TB) */
			margin-header: 5mm; /* <any of the usual CSS values for margins> */
			margin-footer: 5mm;
		}
		@media print {
			footer {page-break-after: always;}
		}
	</style>
	<?php
		$no = 1;
		$page = 1;
		$jml_data = count($data);
		$hasil = $jml_data / 2;
		$total_page=0;
		if(is_double($hasil)){
			$total_page = floor($hasil)+1;
		}else if(is_int($hasil)){
			$total_page = $hasil;
		}
		
		foreach($data as $a) {
			$ntpd = $a['ntpd'];
			$bank = $a['bank'];
			$nop = $a['nop'];
			$thn_pajak = $a['thn_pajak'];
			$nm_wp = $a['nm_wp'];
			$alamat = $a['alamat'];
			$kel = $a['kel'];
			$kec = $a['kec'];
			$tgl_bayar = $a['tgl_bayar'];
			$pokok = $a['pokok'];
			$denda = $a['denda'];
			$total = $a['total'];
			$terbilang = $a['terbilang'];
			$kobil = $a['kobil'];
			$ket = $a['ket'];
			$qr = $a['qr'];
	?>
	<?php
		if($no % 2 == 0){
	?>
		<img width="100%" height="5%" src="<?= base_url('logo/dash.png'); ?>" style="float: center">
	<?php
		}else{
			echo '<div style="text-align: left"><span style="font-size: 8pt; "><b>[Halaman '.$page++.' dari '.$total_page.']</b></span></div>';
		}
	?>
	<table width="100%">
        <tr>
            <th width="25%"></th>
            <th>
			<p align="center" style="font-size: 10pt;"><b>SURAT SETORAN PAJAK DAERAH (SSPD) <br>PAJAK BUMI DAN BANGUNAN</b></p>
            </th>
            <th width="25%" style="text-align: center"><img width="80px" src="<?= base_url($qr) ?>"></th>
        </tr>
    </table>
	<table width="100%" style="font-size: 9pt;">
		<hr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NTPD</h4></td>
			<td valign="top">:</td>
			<td><b><?= $ntpd ?></b></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">BANK BAYAR</h4></td>
			<td valign="top">:</td>
			<td><?= $bank ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NOP</h4></td>
			<td valign="top">:</td>
			<td><?= $nop ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TAHUN PAJAK</h4></td>
			<td valign="top">:</td>
			<td><?= $thn_pajak ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NAMA WP</h4></td>
			<td valign="top">:</td>
			<td><?= $nm_wp ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">ALAMAT</h4></td>
			<td valign="top">:</td>
			<td><?= $alamat ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KELURAHAN</h4></td>
			<td valign="top">:</td>
			<td><?= $kel ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KECAMATAN</h4></td>
			<td valign="top">:</td>
			<td><?= $kec ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TGL PEMBAYARAN</h4></td>
			<td valign="top">:</td>
			<td><?= $tgl_bayar ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">JUMLAH</h4></td>
			<td valign="top">:</td>
			<td><?= $pokok ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">DENDA</h4></td>
			<td valign="top">:</td>
			<td><?= $denda ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TOTAL</h4></td>
			<td valign="top">:</td>
			<td><?= $total ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TERBILANG</h4></td>
			<td valign="top">:</td>
			<td><?= $terbilang ?></td>
		</tr>
		<tr>
			<td colspan="3"><hr></td>
		</tr>
		<tr>
			<td width="30%"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KODE BILLING KOLEKTIF </h4></td>
			<td valign="top">:</td>
			<td><?= $kobil ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KETERANGAN</h4></td>
			<td valign="top">:</td>
			<td><?= $ket ?></td>
		</tr>
		
	</table>
	<br>
	<span style="font-size: 9pt;"><u><b>CATATAN :</b></u></span>
	<table width="100%" style="font-size: 9pt;">
				<tr>
					<td valign="top" width="5px"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">1.</h4></td>
					<td valign="top"><i>Dokumen ini merupakan pembayaran yang SAH PBB P2 Bapenda Kab. Malang</i></td>
				</tr>
				<tr>
					<td valign="top" width="5px"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">2.</h4></td>
					<td valign="top"><i>Untuk pengecekan, silahkan scan QRCODE diatas</i></td>
				</tr>
			</table>
	<?php
			if($no % 2 == 0){
				// echo '<br><br><div style="text-align: right"><span style="font-size: 8pt; "><b>[Halaman '.$page++.' dari '.$total_page.']</b></span></div>';
	?>
			<footer>
				
			</footer>
	<?php
			}
	?>
	
	<?php
		$no++;
		}
	?>
	
	<script type="text/javascript">
	
		window.print();
		// window.open();
		// window.print();
		// window.close();
	</script>