<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Import Dafnom
</h1>
@endsection
@section('content')

<div class="row">
    <div class="col-md-4">
        <div class="box box-danger">
            <form role="form" id="form" method="post" action="#" enctype="multipart/form-data">
            <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group input-group-md" style="margin-bottom: 10px">
                    <input type="file" name="data_excel" id="data_excel" class="form-control" required>
                        <span class="input-group-btn">
                        <button type="submit" id="upload" class="btn btn-success btn-flat"><i class="fa fa-upload"></i> Preview</button>
                        </span>
                    </div>
                    <span style="display: none"><a href="<?= base_url('template_dafnom.xlsx')?>" class="btn btn-warning btn-sm">Download Template</a></span>
                </div>
            </div>
            </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
    <?php 
        if(isset($_SESSION['notif'])) {
            if($_SESSION['notif']=='0'){
    ?>
    <div id="div_peringatan" class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Peringatan</h4>
        Data Kecamatan dan Kelurahan yang terdapat pada file upload tidak termasuk ke dalam UPT yang bersangkutan !
    </div>
    <?php
            }
        }
    ?>
        
      <div id="preview"></div>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    function formatRupiah(angka = 0) {
        if (angka == null) {
            angka = 0;
        }
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;

    }
    $(document).on('click', '#upload', function(e) {
        e.preventDefault();

        var file_data = $('#data_excel').prop('files')[0];
        var form_data = new FormData();
        form_data.append('data_excel', file_data);
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("dafnom/prosesimport") ?>',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    var tr = "";
                    var names = ['No','NOP', 'NAMA', 'ALAMAT', 'PBB'];
                    input_ = '';
                    var no = 0, jml_pbb = 0;
                    var count_data = 1;
                    var max_data = response.data.length;
                    $.each(response.data, function(i, item) {
                        if(count_data < max_data){
                            input_ += '<input type="hidden" value="' + no + '" name="kode_inc[]">';
                            tr += '<tr>';
                            $.each(item, function(a, cell) {
                                if(a=='0'){
                                    no++;
                                    tr += '<td style="text-align: center">'+no+'.</td>';
                                }
                                else if(a=='4'){
                                    jml_pbb += cell;
                                    tr += '<td style="text-align: right">' + formatRupiah(cell) + '</td>';
                                }else{
                                    tr += '<td>' + cell + '</td>';
                                }
                                input_ += '<input type="hidden" value="' + cell + '" name="' + names[a] + '[]">';

                            });
                            tr += '</tr>';
                            console.log('belum = '+count_data);
                        }else{
                            console.log('sudah = '+count_data);
                        }
                        count_data++;
                    });

                    preview_ = '<form  role="form" method="post" action="<?= base_url('dafnom/submitdafnom') ?>" enctype="multipart/form-data"><div class="box">' +
                        '<div class="box-header with-border">'+
                        '<div class="box-title">'+
                        '<input type="hidden" value="'+no+'" name="jml_nop">'+
                        '<span style="float: left; font-size: 11pt"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'+no+'</span></b></span><br>'+
                        '<span style="float: left; font-size: 11pt"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '+formatRupiah(jml_pbb)+'</span></b></span>'+
                        '</div>'+
                        '<div class="pull-right"><button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button></div></div>' +
                        '<div class="box-body"><div class="table-responsive">' +
                        input_ +
                        '<table class="table table-bordered" id="table">' +
                        '<thead>' +
                        '<tr>' +
                        '<th width="3%" style="text-align: center">No.</th>' +
                        '<th width="25%" style="text-align: center">NOP</th>' +
                        '<th style="text-align: center">NAMA</th>' +
                        '<th style="text-align: center">ALAMAT</th>' +
                        '<th style="text-align: center">PBB</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody >' +
                        tr +
                        '</tbody>' +
                        '</table>' +
                        '</div>' +
                        '</div></div></form>';

                    $("#preview").html(preview_);

                    $('#table').DataTable({
                        ordering: false
                    });
                } else {
                    $("#preview").html('');
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>
@endsection

