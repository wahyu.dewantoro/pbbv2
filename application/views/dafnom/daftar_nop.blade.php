<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } 
if ($CI->session->userdata('pbb_kg') != 22){
?>
<h1>Daftar NOP Billing Kolektif</h1>
<?php }?>
@endsection
@section('content')
<div class="box box-danger">
<div class="box-header with-border">
        <h3 class="box-title">Form pencarian</h3>
    </div>
    <div class="box-body">
    <form action="<?= base_url('dafnom/daftar_nop')?>" method="GET" class="form-inline">
            <div class="form-group">
                <select name="thn_pajak" class="form-control" required>
                    <option <?php if($tahun=='all'){ echo "selected"; }?> value="all">Semua</option>
                    <?php
                        foreach ( range( $tahun_end, $tahun_start ) as $i ) {
                            print '<option value="'.$i.'"'.($i == $tahun ? 'selected' : '').'>'.$i.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                    <?php  if(isset($kec)&&count($kec)>1){
                            echo "<option value='all'>Semua Kecamatan</option>";
                        }
                    ?>
                    <?php foreach ($kec as $kec) { ?>
                        <option <?php echo ($kec->KD_KECAMATAN == $KD_KECAMATAN)?"selected":"";?> 
                            value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                    <option value='all'>Semua Kelurahan</option>
                    <?php
                    foreach ($kel as $kela) { ?>
                        <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                    echo "selected";
                                } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                    <?php  } ?>
                </select>
            </div>
            <div class="form-group">
                 <input type="text" class="form-control" name="kobil" id="kobil" value="<?php echo (isset($kobil)&&$kobil>1)? $kobil:"";?>" placeholder="Kode Billing ...">
            </div>
            <div class="form-group">
                <select class="form-control" name="kd_status" id="kd_status">
                    <option value='' <?php echo (!isset($kd_status)||$kd_status==='')? "selected":"";?>>Semua</option>
                    <option value='1' <?php echo (isset($kd_status)&&$kd_status=="1")? "selected":"";?>>Lunas</option>
                    <option value='0' <?php echo (isset($kd_status)&&$kd_status==="0")? "selected":"";?>>Belum Lunas</option>
                </select>
            </div>
            <div class="form-group" id="div_tgl_bayar" style="<?php echo (isset($kd_status)&&$kd_status=="1")? "":"display: none";?>">
                <input style="width: 120px" required type="text" class="form-control tanggal" id="tgl_bayar" name="tgl_bayar" value="<?= $tgl_bayar ?>">
            </div>
            <button type="submit" class="btn btn-warning btn-flat">Cari</button>
            <?php echo $export_link;?>
        </form>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">Kode Billing</th>
                    <th style="text-align: center">NOP</th>
                    <th style="text-align: center">NAMA WP</th>
                    <th style="text-align: center">Kecamatan</th>
                    <th style="text-align: center">Kelurahan</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">PBB</th>
                    <!-- <th style="text-align: center"><?php echo $tgl_show_text;?></th> -->                    
                    <th style="text-align: center">Tgl Buat</th>                    
                    <th style="text-align: center">Tgl Bayar</th>
                    <th style="text-align: center">Status</th>
                </tr>
            </thead>

        </table>
    </div>
</div>

<div class="modal fade" id="modal-detail">
          <div class="modal-dialog">
            <div class="modal-content" style="width: 700px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Daftar Nominative</h4>
              </div>
              <div class="modal-body">
                <div id="list_dafnom" style="text-align: center"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        $(document).bind("ajaxStart.mine", function () {
            Swal.fire({
                title: "Sedang melakukan proses pencarian data.",
                html:'<div class="fa-3x"><i class="fa fa-spinner fa-pulse"></i></div>',
                showConfirmButton: false,
                allowOutsideClick: false,
            });
        });
        $(document).bind("ajaxStop.mine", function () {
            swal.close();
        });
        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                global:false,
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        var tanggal_bayar = $('#tgl_bayar').val();

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        var status = 0;
        <?php if($direct){?>
        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },

            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url(); ?>dafnom/json_daftar_nop/<?=$KD_KECAMATAN?>/<?= $KD_KELURAHAN ?>/<?= $tahun ?>?kobil=<?= $kobil ?>&kd_status=<?= $kd_status ?>&tgl_bayar="+tanggal_bayar,
                "type": "post",
                
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    "data": "KOBIL"
                },
                {
                    "data": "NOP"
                },
                {
                    "data": "NM_WP"
                },
                {
                    "data": "NAMA_KECAMATAN"
                },
                {
                    "data": "NAMA_KELURAHAN",
                },
                {
                    "data": "TAHUN_PAJAK",
                },
                {
                    "data": "TOTAL",
                    'class': 'text-right',
                    render: function(data, type, row) {
                        var angka = row.TOTAL;
                        var ribuan=0;
                        if(angka){
                            var reverse = angka.toString().split('').reverse().join(''),
                            ribuan = reverse.match(/\d{1,3}/g);
                            ribuan = ribuan.join('.').split('').reverse().join('');
                        }
                        
                        return ribuan;
                    }
                },
                {
                    "data": "CREATED_AT"
                },
                {
                    "data": "TGL_BAYAR"
                },
                {
                    "data": "KD_STATUS",
                    'class': 'text-center',
                    render: function(data, type, row) {
                        if(row.KD_STATUS=='1'){
                            var d = '<span class="label label-success">Lunas</span>';
                            status = 1;
                        }else{
                            var d = '<span class="label label-warning">Belum Lunas</span>';
                            status = 0;
                        }
                        return d;
                    }
                },
            ],
            order: ['9','desc'],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        <?php }?>
    });
</script>
<script>
function konfirmasi(){
    confirm('Lanjut hapus data ini ?');
}
function detail_dafnom(id){
    $('#list_dafnom').html("<img src='<?= base_url('logo/')?>loading.gif' width='15%'/>");

    $.ajax({
            type: "POST",
            url: "<?= base_url('dafnom/ajax_list_dafnom'); ?>",
            dataType: 'json',
            data:{ kode: id},
            success: function(response)
            {
                console.log(response);
                if(response.status=='1'){
                    preview_ = 
                            '<div class="row">'+
                            '<div class="col-md-6">'+
                            '<span style="float: left"><b><i>Kode Billing</i>&emsp;&emsp;: <span>'+response.kobil+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'+response.jml_nop+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '+response.total+'</span></b></span><br>'+
                            '</div>'+
                            '<div class="col-md-6" style="text-align: right">'+
                            response.cetak_bukti_all +
                            '</div>'+
                            '</div>'+
                            '<div class="box-body"><div class="table-responsive">' +
                            '<table class="table table-bordered table-striped" id="table">' +
                            '<thead>' +
                            '<tr>' +
                            '<th width="3%" style="text-align: center">No</th>' +
                            '<th width="25%" style="text-align: center">NOP</th>' +
                            '<th style="text-align: center">Tahun Pajak</th>' +
                            '<th style="text-align: center">Nama WP</th>' +
                            '<th style="text-align: center">File</th>' +
                            '<th style="text-align: center">Nominal</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody >' +
                            response.tr +
                            '<tfoot>'+
                            '<tr>' +
                            '<th colspan="5" style="text-align: center">Total</th>'+
                            '<th id="total_order" style="text-align: right"></th>'+
                            '</tr>' +
                            '</tfoot>' +
                            '</tbody>' +
                            '</table>' +
                            '</div></div>';

                    $("#list_dafnom").html(preview_);
                    $('#total_order').html(response.total);

                    $('#table').DataTable({
                            ordering: false,
                            "iDisplayLength": 10
                        });
                }else{
                    document.getElementById("list_dafnom").innerHTML= "<span>Data tidak ditemukan !</span>";
                }
            }
            // success: function(data)
            // {
              
            //   if(data=='0'){
            //     document.getElementById("list_dafnom").innerHTML= "<span>Data tidak ditemukan !</span>";
            //   }else{
            //     document.getElementById("list_dafnom").innerHTML=data;
            //   }
            // }
    });
}
</script>
<script>
    $('#kd_status').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if(valueSelected=='1'){
            $("#div_tgl_bayar").show();
        }else{
            $("#div_tgl_bayar").hide();
        }
    });
</script>

@endsection