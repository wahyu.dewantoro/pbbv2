@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Unflag
</h1>
@endsection
@section('content')
<div class="box box-danger">
        
        <form action="<?= base_url($action_cek)?>" method="GET">
        <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                <div class="input-group input-group-md">
                <input type="number" id="kobil" name="kobil" class="form-control" placeholder="Masukkan Kode Billing atau NOP" value="<?= $kobil ?>" onkeyup="myCek()">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Cari</button>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
            <div class="col-md-3">
                
            </div>
            <div class="col-md-3" style="text-align: right">
                
                
            </div>
          </div>
        </div>
        </form>
      </div>
      <!-- /.box -->
<?php if(isset($res)){
?>

<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">NOP</th>                    
                    <th style="text-align: center">Nama WP</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">Kode Pengesahan</th>
                    <th style="text-align: center">Total</th>
                    <th style="text-align: center">Tgl Bayar</th>
                    <th width="5%" style="text-align: center">Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $no = 1;
                foreach ($res as $a) {
                $nop = $a->NOP;
                $kd_propinsi = substr($nop, 0, 2);
                $kd_dati2 =substr($nop, 2, 2);
                $kd_kecamatan =substr($nop, 4, 3);
                $kd_kelurahan =substr($nop, 7, 3);
                $kd_blok =substr($nop, 10, 3);
                $no_urut =substr($nop, 13, 4);
                $kd_jns_op =substr($nop, 17, 1);
                $nop2 = $kd_propinsi.'.'.$kd_dati2.'.'.$kd_kecamatan.'.'.$kd_kelurahan.'.'.$kd_blok.'-'.$no_urut.'.'.$kd_jns_op;
            ?>
                <tr>
                    <td align="center"><?php echo $no++; ?></td>
                    <td align="center"><?= $nop2 ?></td>
                    <td align="center"><?= $a->NAMA_WP ?></td>
                    <td align="center"><?= $a->TAHUN_PAJAK ?></td>
                    <td align="center"><?= $a->KODEPENGESAHAN ?></td>
                    <td align="right"><?= number_format($a->TOTAL,0,',','.') ?></td>
                    <td align="center"><?= $a->TGL_BUAT ?></td>
                    <td align="center"><button id="btn_reversal" data-pengesahan="<?= $a->KODEPENGESAHAN ?>" title="Reversal" class="btn btn-xs btn-warning" onclick="return confirm('Apakah anda yakin reversal data ini ?')"><i class="fa fa-undo"></i></button></td>
                </tr>
            <?php
                }
            ?>
            </tbody>

        </table>
    </div>
</div>

<?php
}
?>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
function myCek() {
    var x = $('#kobil').val();
    // Replace dot with a comma
    var num = x.toString().replace(/\./g, '');
    console.log(num);
    $('#kobil').val(num)
}
$(document).on('click', '#btn_reversal', function(e) {
        e.preventDefault();
        var pengesahan = $('#btn_reversal').attr("data-pengesahan");
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("dafnom/reversal?pengesahan=") ?>'+pengesahan,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    // console.log(response.data);
                    Swal.fire({
                        title: 'Sukses',
                        text: response.data,
                        icon: 'success',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    }).then(function() {
                        // location.reload();
                        window.location = "<?= base_url('dafnom/unflag'); ?>";
                    });
                } else {
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                console.log(data.responseText);
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>

@endsection