	<style type="text/css">
		body {
			font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
			font-size: 10px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		}

		.inti {
			border: 1px solid #C6C6C6;
			margin: auto;
		}

		.inti td {
			border-right: 1px solid #C6C6C6;
		}

		.inti th {
			border-right: 1px solid #C6C6C6;
			border-bottom: 1px solid #C6C6C6;
		}
	</style>
	<style type="text/css" media="print">
		@page {
			size: landscape;
		}
	</style>
	<?php
		$pbb = 0;
		foreach ($res as $a) {
			$pbb += $a->TOTAL;
		}
	?>
	<!-- <table width="100%">
	<tr>
		<td width="25%"><center><img width="38%" src="<?php echo base_url('logo/kab_malang.jpg')?>"></center></td>
		<td>
		<center>
		<p style="font-size: 12pt; margin-block-start: 0em; margin-block-end: 0em; margin-bottom: 0px">
		<b>PEMERINTAH KABUPATEN MALANG</b><br>
		<b>BADAN PENDAPATAN DAERAH</b><br>
		<span style="font-size: 12pt;"><b>JL. K.H. Agus Salim 7 Telp (0341) 362372 Fax (0341) 355708</b></span><br>
		<span style="font-size: 12pt;"><b>MALANG - 65119</b></span>
		</p>
		</center>
		</td>
		<td width="25%"></td>
	</tr>
	</table>
	<hr> -->
	<h1 align="center">PELUNASAN DAFTAR NOMINATIVE</h1>
	<hr>
	<br>
	<table width="100%">
		<tr>
			<td width="20%"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NTPD</h4></td>
			<td valign="top" width="3%">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $ntpd ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">BANK BAYAR</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $bank ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TANGGAL BAYAR</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $tgl_bayar ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">JUMLAH NOP</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= count($res) ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">JUMLAH PBB</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= 'Rp. '.number_format($pbb,0,',','.') ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KECAMATAN</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $kec ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KELURAHAN</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $kel ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TANGGAL REKAM</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= date('d-m-Y H:i:s', strtotime($created)) ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">EXPIRED</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= date('d-m-Y H:i:s', strtotime($expired)) ?></h4></td>
		</tr>
	</table>
	<hr>
	<table width="100%">
		<tr>
			<td width="20%"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KODE BILLING </h4></td>
			<td valign="top" width="3%">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $kobil ?></h4></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KETERANGAN</h4></td>
			<td valign="top">:</td>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px"><?= $ket ?></h4></td>
		</tr>
	</table>
	<br>
	<table class="inti" width="100%" cellpadding="3" cellspacing="0" style="font-size:14px">
		<thead>
			<tr>
				<th>No</th>
				<th width="27%">NOP</th>
				<th width="30%">Nama WP</th>
				<th width="10%">Tahun Pajak</th>
				<th>Pokok</th>
				<th>Denda</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
			$total = 0;
			$pokok = 0;
			$denda = 0;
			foreach ($res as $res) {			
				$total += $res->TOTAL;
				$pokok += $res->POKOK;
				$denda += $res->DENDA;
				$NOP = $res->KD_PROPINSI.'.'.$res->KD_DATI2.'.'.$res->KD_KECAMATAN.'.'.$res->KD_KELURAHAN.'.'.$res->KD_BLOK.'-'.$res->NO_URUT.'.'.$res->KD_JNS_OP; ?>
				<tr>
					<td align="center"><?php echo $no; ?></td>
					<td align="center"><?= $NOP ?></td>
					<td><?= $res->NM_WP ?></td>
					<td align="center"><?= $res->THN_PAJAK_SPPT ?></td>
					<td align="right"><?= number_format($res->POKOK, '0', '', '.'); ?></td>
					<td align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
					<td align="right"><?= number_format($res->TOTAL, '0', '', '.') ?></td>
				</tr>
			<?php $no++;
			} ?>
		</tbody>

		<thead border=1>
			<tr>
				<th colspan="4">Total</th>
				<th align="right"><?= number_format($pokok, 0, '', '.') ?></th>
				<th align="right"><?= number_format($denda, 0, '', '.') ?></th>
				<th align="right"><?= number_format($total, 0, '', '.') ?></th>
			</tr>
		</thead>

	</table>
	<br><br>
	<table width="100%">
		<tr>
			<td></td>
			<td align="center" width="40%">
				MALANG, <?= date('d M Y') ?>
				<br>
				<br>
				<br>
				<?php echo $this->session->userdata('pbb_nama') ?>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	
		window.print();
		// window.open();
		// window.print();
		// window.close();
	</script>