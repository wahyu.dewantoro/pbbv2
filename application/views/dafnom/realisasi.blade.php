@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Realisasi
</h1>
@endsection
@section('content')
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Form pencarian</h3>
    </div>
    <div class="box-body">
    <form action="<?= base_url('dafnom/realisasi')?>" method="GET" class="form-inline">

            <div class="form-group">
                <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                    <?php  if(isset($kec)&&count($kec)>1){
                            echo "<option value='all'>Semua Kecamatan</option>";
                        }
                    ?>
                    <?php foreach ($kec as $kec) { ?>
                        <option <?php echo ($kec->KD_KECAMATAN == $KD_KECAMATAN)?"selected":"";?> 
                            value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                    <?php  if(isset($kel)&&count($kel)>1){
                            echo "<option value='all'>Semua Kelurahan</option>";
                        }
                    ?>
                    <?php
                    foreach ($kel as $kela) { ?>
                        <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                    echo "selected";
                                } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                    <?php  } ?>
                </select>
            </div>
            <div class="form-group">
                <select name="thn_pajak" class="form-control" required>
                    <option value="">Pilih</option>
                    <option <?php if($tahun=='all'){ echo "selected"; }?> value="all">Semua</option>
                    <?php
                        foreach ( range( $tahun_end, $tahun_start ) as $i ) {
                            print '<option value="'.$i.'"'.($i == $tahun ? 'selected' : '').'>'.$i.'</option>';
                        }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-warning btn-flat">Cari</button>
        </form>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%" style="text-align: center">No</th>
                    <th style="text-align: center">Kode Billing</th>
                    <th style="text-align: center">Kecamatan</th>
                    <th style="text-align: center">Kelurahan</th>
                    <th style="text-align: center">Tahun Pajak</th>
                    <th style="text-align: center">JML NOP</th>
                    <th style="text-align: center">PBB</th>
                    <th style="text-align: center">Keterangan</th>
                    <th style="text-align: center">Tgl Buat</th>
                    <th style="text-align: center">Tgl Bayar</th>
                    <th width="10%" style="text-align: center">Aksi</th>
                </tr>
            </thead>

        </table>
    </div>
</div>

<div class="modal fade" id="modal-detail">
          <div class="modal-dialog">
            <div class="modal-content" style="width: 750px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Daftar Nominative</h4>
              </div>
              <div class="modal-body">
                <div id="list_dafnom" style="text-align: center"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("ajaxStart.mine", function () {
            Swal.fire({
                title: "Sedang melakukan proses pencarian data.",
                html:'<div class="fa-3x"><i class="fa fa-spinner fa-pulse"></i></div>',
                showConfirmButton: false,
                allowOutsideClick: false,
            });
        });
        $(document).bind("ajaxStop.mine", function () {
            swal.close();
        });
        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                global: false,
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        <?php if($direct_data){?>
        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },

            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url(); ?>dafnom/ajax_realisasi/<?=$KD_KECAMATAN?>/<?= $KD_KELURAHAN ?>/<?= $tahun ?>",
                "type": "post",
                
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    "data": "KOBIL"
                },
                {
                    "data": "NAMA_KECAMATAN"
                },
                {
                    "data": "NAMA_KELURAHAN"
                },
                {
                    "data": "TAHUN_PAJAK"
                },
                {
                    "data": "JML_NOP",
                    'class': 'text-right'
                },
                {
                    "data": "PBB",
                    'class': 'text-right',
                    render: function(data, type, row) {
                        var angka = row.PBB;
                        var reverse = angka.toString().split('').reverse().join(''),
                            ribuan = reverse.match(/\d{1,3}/g);
                        ribuan = ribuan.join('.').split('').reverse().join('');
                        return ribuan;
                    }
                },
                {
                    "data": "KETERANGAN"
                },
                {
                    "data": "CREATED_AT"
                },{
                    "data": "TGL_BAYAR"
                },
                {
                    "data": "ID",
                    'class': 'text-center',
                    render: function(data, type, row) {
                        this.url = '<?php echo base_url(); ?>dafnom/';
                        var d = '<a href="' + this.url + 'cetak_dafnom_excel/'+row.ID+'" title="Cetak Excel" class="btn btn-xs btn-success " target="_blank"><i class="fa fa-file-excel-o"></i></a>'+' <a href="' + this.url + 'dafnom_cetak_lunas/'+row.ID+'" title="Cetak PDF" class="btn btn-xs btn-danger " target="_blank"><i class="fa fa-file-pdf-o"></i></a>'+' <button type="button" data-id="'+row.ID+'" title="Detail" class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-detail" onclick="detail_dafnom('+row.ID+')"><i class="fa fa-search"></i></button>';
                            
                        return d;
                    }
                },
            ],
            order: ['8','desc'],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        <?php }?>
    });
</script>
<script>
function konfirmasi(){
    confirm('Lanjut hapus data ini ?');
}
function detail_dafnom(id){
    $('#list_dafnom').html("<img src='<?= base_url('logo/')?>loading.gif' width='15%'/>");
    console.log(id);
    $.ajax({
            url: "<?= base_url('dafnom/ajax_list_dafnom'); ?>",
            dataType: 'json',
            type: "POST",
            data:{ kode: id},
            success: function(response)
            {
                console.log(response);
                if(response.status=='1'){
                    preview_ = 
                            '<div class="row">'+
                            '<div class="col-md-6">'+
                            '<span style="float: left"><b><i>Kode Billing</i>&emsp;&emsp;: <span>'+response.kobil+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah NOP</i>&emsp;&emsp;: <span>'+response.jml_nop+'</span></b></span><br>'+
                            '<span style="float: left"><b><i>Jumlah PBB</i>&emsp;&emsp;: <span> Rp. '+response.total+'</span></b></span><br>'+
                            '</div>'+
                            '<div class="col-md-6" style="text-align: right">'+
                            response.cetak_bukti_all +
                            '</div>'+
                            '</div>'+
                            '<hr>' +
                            '<div class="box-body"><div class="table-responsive">' +
                            '<table class="table table-bordered table-striped" id="table">' +
                            '<thead>' +
                            '<tr>' +
                            '<th width="3%" style="text-align: center">No</th>' +
                            '<th width="25%" style="text-align: center">NOP</th>' +
                            '<th style="text-align: center">Tahun Pajak</th>' +
                            '<th style="text-align: center">Nama WP</th>' +
                            '<th style="text-align: center">File</th>' +
                            '<th style="text-align: center">Nominal</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody >' +
                            response.tr +
                            '</tbody>' +
                            '<tfoot>'+
                            '<tr>' +
                            '<th colspan="5" style="text-align: center">Total</th>'+
                            '<th id="total_order" style="text-align: right"></th>'+
                            '</tr>' +
                            '</tfoot>' +
                            '</table>' +
                            '</div></div>';

                    $("#list_dafnom").html(preview_);
                    $('#total_order').html(response.total);

                    $('#table').DataTable({
                            ordering: false,
                            "iDisplayLength": 10
                        });
                }else{
                    document.getElementById("list_dafnom").innerHTML= "<span>Data tidak ditemukan !</span>";
                }
            }
    });
}
</script>

@endsection