	<style type="text/css">
		body {
			font-family: Courier New, Courier, monospace;
			font-size: 10px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		}

		.inti {
			border: 1px solid #C6C6C6;
			margin: auto;
		}

		.inti td {
			border-right: 1px solid #C6C6C6;
		}

		.inti th {
			border-right: 1px solid #C6C6C6;
			border-bottom: 1px solid #C6C6C6;
		}

		footer{
			position: relative;
			left: 0;
			bottom:0;
			background-color:#000;
			width:100%;
			bottom:0;
			padding:5px 0;
		}
		.clearfix{
			clear:both;
		}
	</style>
	<style type="text/css" media="print">
		@page {
			size: portrait;
		}
	</style>
	<table width="100%">
        <tr>
            <th width="25%"></th>
            <th>
			<p align="center" style="font-size: 13pt;"><b>SURAT SETORAN PAJAK DAERAH (SSPD) <br>PAJAK BUMI DAN BANGUNAN</b></p>
            </th>
            <th width="25%" style="text-align: center"><img width="100px" src="<?= base_url($qr) ?>"></th>
        </tr>
    </table>
	<hr>
	<table width="100%">
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NTPD</h4></td>
			<td valign="top">:</td>
			<td><b><?= $ntpd ?></b></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">BANK BAYAR</h4></td>
			<td valign="top">:</td>
			<td><?= $bank ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NOP</h4></td>
			<td valign="top">:</td>
			<td><?= $nop ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TAHUN PAJAK</h4></td>
			<td valign="top">:</td>
			<td><?= $thn_pajak ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">NAMA WP</h4></td>
			<td valign="top">:</td>
			<td><?= $nm_wp ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">ALAMAT</h4></td>
			<td valign="top">:</td>
			<td><?= $alamat ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KELURAHAN</h4></td>
			<td valign="top">:</td>
			<td><?= $kel ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KECAMATAN</h4></td>
			<td valign="top">:</td>
			<td><?= $kec ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TGL PEMBAYARAN</h4></td>
			<td valign="top">:</td>
			<td><?= $tgl_bayar ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">JUMLAH</h4></td>
			<td valign="top">:</td>
			<td><?= $pokok ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">DENDA</h4></td>
			<td valign="top">:</td>
			<td><?= $denda ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TOTAL</h4></td>
			<td valign="top">:</td>
			<td><?= $total ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">TERBILANG</h4></td>
			<td valign="top">:</td>
			<td><?= $terbilang ?></td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="3"><hr></td>
		</tr>
		<tr>
			<td width="30%"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KODE BILLING KOLEKTIF </h4></td>
			<td valign="top">:</td>
			<td><?= $kobil ?></td>
		</tr>
		<tr>
			<td><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">KETERANGAN</h4></td>
			<td valign="top">:</td>
			<td><?= $ket ?></td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<span><u><h2 style="margin-top: 0px; margin-bottom: 0px">CATATAN :</h2></u></span>
	<table width="100%">
		<tr>
			<td valign="top" width="5px"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">1.</h4></td>
			<td valign="top"><i>Dokumen ini merupakan pembayaran yang SAH PBB P2 Bapenda Kab. Malang</i></td>
		</tr>
		<tr>
			<td valign="top" width="5px"><h4 align="left" style="margin-top: 0px; margin-bottom: 0px">2.</h4></td>
			<td valign="top"><i>Untuk pengecekan, silahkan scan QRCODE diatas</i></td>
		</tr>
	</table>
	<script type="text/javascript">
	
		window.print();
		// window.open();
		// window.print();
		// window.close();
	</script>