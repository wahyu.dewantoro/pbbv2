 
@extends('page.master')
@section('judul')
<h1>
    Detail Wajib Pajak
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>
    <li class="active">Detail WP</li>
</ol>
@endsection
@section('content')
  <div class="row">
    <form class='form-horizontal' action="<?php echo base_url('view/detail_wp'); ?>" method="post" enctype="multipart/form-data">
      <div class="col-md-8">
        <div class="form-group">
          <label class="col-md-3 control-label">Subjek Pajak ID </label>
          <div class="col-md-4">
            <input type="text" class="form-control" name="SUBJEK_PAJAK_ID" id="" placeholder="SUbjek Pajak ID" value="<?php echo $_SESSION['SUBJEK_PAJAK_ID']; ?>" />
          </div>
          <div class="col-md-1">
            <button type="submit" class="btn btn-success" value='cari'><i class="fa fa-search"></i> Lihat</button>
          </div>
        </div>
      </div>
    </form>
  </div><br>
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">Informasi</a></li>
      <li><a href="#tab_2" data-toggle="tab">Objek Pajak</a></li>
      <li><a href="#tab_5" data-toggle="tab">Piutang</a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
      <div class="tab-pane active" id="tab_1">
        <!-- form tab 2 -->
        <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
          <div class="box box-success">
            <h4 class='box-title'> <i class="fa fa-globe"></i> Data Subjek Pajak</h4>
            <div class="box-body">
              <div id="idn">
                <div class="row"><?php error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); ?>
                  <div class="col-md-6">
                    <table class="table" style="font-size:13.4px" width='100%'>
                      <tbody>
                        <tr>
                          <td width='35%'>Nama </td>
                          <td width='1%'>:</td>
                          <td><?php echo $informasi->NM_WP; ?></td>
                        </tr>
                        <tr>
                          <td>Alamat</td>
                          <td>:</td>
                          <td><?php echo $informasi->JALAN_WP; ?></td>
                        </tr>
                        <tr>
                          <td>RW / RT</td>
                          <td>:</td>
                          <td><?php echo $informasi->RW_WP . ' / ' . $informasi->RT_WP; ?></td>
                        </tr>
                        <tr>
                          <td>Kelurahan</td>
                          <td>:</td>
                          <td><?php echo $informasi->KELURAHAN_WP; ?></td>
                        </tr>
                        <tr>
                          <td>Kota</td>
                          <td>:</td>
                          <td><?php echo $informasi->KOTA_WP; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <table class="table" style="font-size:13.4px" width='100%'>
                      <tbody>
                        <tr>
                          <td width='35%'>Kode Pos</td>
                          <td width='1%'>:</td>
                          <td><?php echo $subjek_pajak->KD_POS_WP; ?></td>
                        </tr>
                        <tr>
                          <td>No. Telp</td>
                          <td>:</td>
                          <td><?php echo $subjek_pajak->TELP_WP; ?></td>
                        </tr>
                        <tr>
                          <td>NPWP</td>
                          <td>:</td>
                          <td><?php echo $subjek_pajak->NPWP; ?></td>
                        </tr>
                        <tr>
                          <td>Pekerjaan</td>
                          <td>:</td>
                          <td><?php if ($subjek_pajak->STATUS_PEKERJAAN_WP == 5) {
                                echo $subjek_pajak->STATUS_PEKERJAAN_WP . ' - LAINNYA';
                              } else {
                                echo $subjek_pajak->STATUS_PEKERJAAN_WP;
                              } ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div> <!-- body -->
          </div>
        </form>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">
        <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
          <div class="box box-success">
            <div class="box-body">
              <div id="idn">
                <div class="row">
                  <div class="col-md-8">
                    <table class="table table-bordered table-hover table-striped">
                      <thead>
                        <tr>
                          <th>NOP</th>
                          <th>Jenis Bumi</th>
                          <th>Luas Bumi</th>
                          <th>Luas Bng</th>
                          <th>Kecamatan</th>
                          <th>Kelurahan</th>
                          <th>Alamat</th>
                          <th>RW / RT</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (count($objek_pajak) > 0) {
                          $no = 1;
                          foreach ($objek_pajak as $rn) {   ?>
                            <tr>
                              <td><?php echo $rn->NOP ?></td>
                              <td><?php if ($rn->KD_STATUS_WP == '1') {
                                    echo "TANAH + BANGUNAN";
                                  } else if ($rn->KD_STATUS_WP == '1') {
                                    echo "";
                                  } else {
                                    # code...
                                  } ?></td>
                              <td align='right'><?php echo number_format($rn->TOTAL_LUAS_BUMI, '0', ',', '.') ?></td>
                              <td align='right'><?php echo number_format($rn->TOTAL_LUAS_BNG, '0', ',', '.') ?></td>
                              <td><?php echo $rn->KECAMATAN ?></td>
                              <td><?php echo $rn->KELURAHAN ?></td>
                              <td><?php echo $rn->JALAN_OP ?></td>
                              <td><?php echo $rn->RW_OP . ' / ' . $rn->RT_OP; ?></td>
                            </tr>
                          <?php  }
                        } else { ?>
                          <tr>
                            <td colspan="8">Tidak ada data</td>
                          </tr>

                        <?php  } ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4">

                  </div>
                </div>
              </div>
            </div> <!-- body -->
          </div>
        </form>
      </div>
      <!-- tab 5 !-->
      <div class="tab-pane" id="tab_5">
        <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
          <div class="box box-success">
            <div class="box-body">
              <div id="idn">
                <div class="row">
                  <div class="col-md-8">
                    <table class="table table-bordered table-hover table-striped">
                      <thead>
                        <tr>
                          <th>NOP</th>
                          <th>Tahun</th>
                          <th>NJOP Bumi</th>
                          <th>NJOP Bng</th>
                          <th>Tgl Jatuh Tempo</th>
                          <th>PBB</th>
                          <th>Denda (Perkiraan)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (count($objek_pajak) > 0) {
                          $no = 1;
                          foreach ($objek_pajak as $rn) {   ?>
                            <tr>
                              <td><?php echo "" ?></td>
                              <td></td>
                              <td align='right'><?php echo number_format("", '0', ',', '.') ?></td>
                              <td align='right'><?php echo number_format("", '0', ',', '.') ?></td>
                              <td><?php echo "" ?></td>
                              <td><?php echo "" ?></td>
                              <td><?php echo "" ?></td>
                              <td></td>
                            </tr>
                          <?php  }
                        } else { ?>
                          <tr>
                            <td colspan="7">Tidak ada data</td>
                          </tr>

                        <?php  } ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4">

                  </div>
                </div>
              </div>
            </div> <!-- body -->
          </div>
        </form>
      </div>
      <!-- tab 6 !-->
    </div>
    <!-- /.tab-content -->
  </div>
@endsection
@section('css')


<style type="text/css">
  .control-label {
    text-align: left;
  }

  .table>tbody>tr>td,
  .table>tbody>tr>th,
  .table>tfoot>tr>td,
  .table>tfoot>tr>th,
  .table>thead>tr>td,
  .table>thead>tr>th {
    padding: 3px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #f9f9f900;
  }
</style>
@endsection
@section('script')
<script type="text/javascript">
  function myFunction() {
    if (true) {
      $(".form-control").keyup(function() {
        if (this.value.length == this.maxLength) {
          var nextIndex = $('input:text').index(this) + 1;
          $('input:text')[nextIndex].focus();
        }
      });

    };
  };
</script>
@endsection