@extends('page.master')
@section('judul')
<h1>
    Laporan Pembayaran Bank
</h1>
@endsection
<!-- Main content -->
@section('content')
<!-- Default box -->
<div class="box">

    <div class="box-body">
        <form class="form-inline" method="get" action="<?php echo base_url() . 'laporan/lapPembayaranBank' ?>">
            <div class="form-group">
                Tahun Pembayaran
            </div>
            <div class="form-group">
                <select class="form-control" name="tahun">
                    <option value="all">Semua</option>
                    <?php for ($q = date('Y'); $q >= 2019; $q--) { ?>
                        <option <?php if ($q == $tahun) {
                                    echo "selected";
                                } ?> value="<?= $q ?>"><?= $q ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                Bank
            </div>
            <div class="form-group">
                <select class="form-control" name="bank">
                    <option value="">Semua</option>
                    <?php foreach ($bank as $bank) { ?>
                        <option <?php if ($bank->KODE_BANK == $KD_BANK) {
                                    echo "selected";
                                } ?> value="<?= $bank->KODE_BANK ?>"><?= $bank->KODE_BANK . ' ' . $bank->NAMA_BANK ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                Bulan
            </div>
            <div class="form-group">
                <select class="form-control" name="bulan" id="bulan">
                    <option value="">Pilih</option>
                    <?php for ($bln = 1; $bln <= 12; $bln++) { ?>
                        <option <?php if ($bln == $bulan) {
                                    echo "selected";
                                } ?> value="<?= str_pad($bln, 2, "0", STR_PAD_LEFT) ?>"><?= str_pad($bln, 2, "0", STR_PAD_LEFT) ?></option>
                    <?php } ?>
                </select>
            </div>


            <input type="hidden" name="cari" value="1">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
            <?php if (isset($rk)) {
                echo anchor('laporan/excellappermohonanbank?' . $where, '<i class="fa fa-print"></i> Excel', 'class="btn btn-sm btn-success"');
            } ?>

        </form>

        <?php if (isset($data)) {?>
        <hr>
        <table class="table table-bordered table-striped" id="exampl_e2">
            <thead>
                <tr>
                    <th width="3%">No</th>
                    <th align="center" width="12%">NOP</th>
                    <th align="center" width="10%">Nama WP</th>
                    <th align="center">Pokok</th>
                    <th align="center">Denda</th>
                    <th align="center">Total</th>
                    <th align="center" width="7%">Tanggal Bayar</th>
                    <th align="center" width="10%">Kode Pengesahan</th>
                    <th align="center" width="8%">Bank</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach($data as $row){ ?>
                    <tr>
                        <td align="center"><?= $no ?></td>
                        {{-- S TOTAL, A.TGL_PEMBAYARAN_SPPT, A.TGL_REKAM_BYR_SPPT, A.THN_PAJAK_SPPT, PENGESAHAN, NAMA_BANK --}}
                        <td>
                            <?= $row->KD_PROPINSI.$row->KD_DATI2.$row->KD_KECAMATAN.$row->KD_KELURAHAN.$row->KD_BLOK.$row->NO_URUT.$row->KD_JNS_OP ?>
                        </td>
                        <td>
                            <?= $row->NM_WP_SPPT ?>
                        </td>
                        <td>
                            <?= number_format($row->POKOK,0,'','.') ?>
                        </td>
                        <td>
                            <?= number_format($row->DENDA,0,'','.') ?>
                        </td>
                        <td>
                            <?= number_format($row->TOTAL,0,'','.') ?>
                        </td>
                        <td>
                            <?= $row->TGL_PEMBAYARAN_SPPT ?>
                        </td>
                        <td>
                            <?=  $row->PENGESAHAN ?>
                        </td>
                        <td>
                            <?=  $row->NAMA_BANK ?>
                        </td>
                    </tr>
                    <?php $no++; } ?>
            </tbody>
        </table>
        <button class="btn btn-space btn-secondary"><span> Record : <?php echo angka($total_rows) ?></span></button>
<div class="float-right">
    <?php echo $pagination ?>
</div>
<?php } ?>

    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        // $("#example2").dataTable();

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });



        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'laporan/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });


        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },

            processing: true,
            serverSide: true,
            ordering: false,

            ajax: {
                "url": "<?php echo base_url() ?>laporan/jsonpembayaranbank",
                // "type": "POST",
                "data": {
                    'tahun': '<?= $tahun ?>',
                    'bulan': '<?= $bulan ?>',
                    'bank': '<?= $KD_BANK ?>'
                }
            },
            columns: [
               
                {
                    data: null,
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "KD_PROPINSI",
                    render: function(data, type, row) {
                        return row.KD_PROPINSI + '.' +
                            row.KD_DATI2 + '.' +
                            row.KD_KECAMATAN + '.' +
                            row.KD_KELURAHAN + '.' +
                            row.KD_BLOK + '.' +
                            row.NO_URUT + '.' +
                            row.KD_JNS_OP+"<br><small class='text-danger'>"+row.THN_PAJAK_SPPT+"</small>";
                    }

                },
                {
                    "data": "NM_WP_SPPT",
                    render: function(data, type, row) {
                        return data+"<br><small class='text-danger'>"+row.NM_KELURAHAN + ' - ' + row.NM_KECAMATAN+"<small>";
                    }

                },
                {
                    "data": "POKOK"
                },
                {
                    "data": "DENDA"
                },
                {
                    "data": "TOTAL"
                },
                {
                    "data": "TGL_PEMBAYARAN_SPPT"
                },
                
                {
                    "data": "PENGESAHAN"
                },
                {
                    "data": "NAMA_BANK"
                },

            ],
            // order: [
            //     [0, 'desc']
            // ],
            rowCallback: function(row, data, iDisplayIndex) {

            }
        });
    });
</script>

@endsection