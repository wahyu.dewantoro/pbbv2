 <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=detailpermohonan" . date('YmdHis') . ".xls");
    // echo $judul;

    ?>
 LAPORAN DETAIL PERMOHONAN

 <table style="font-size:12px; padding:2px">
     <thead>
         <tr>
             <th>No</th>
             <th>Tanggal</th>
             <th>No Layanan</th>
             <th>Nama Kuasa</th>
             <th>Subjek Pajak</th>
             <th>NOP</th>
             <th>Jenis Pelayanan</th>
             <th>Keterangan</th>
         </tr>
     </thead>
     <tbody>
         <?php $no = 1;
            foreach ($rk as $rk) { ?>
             <tr>
                 <td align="center"><?php echo $no ?></td>
                 <td align="center"><?php echo $rk->TGL_TERIMA ?></td>
                 <td><?php echo $rk->NO_LAYANAN ?></td>

                 <td><?php echo $rk->NAMA_PEMOHON ?></td>
                 <td><?php echo $rk->NM_WP_SPPT  ?></td>
                 <td><?php echo $rk->NOP ?></td>
                 <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                 <td><?php echo $rk->CATATAN_PST ?></td>
             </tr>
         <?php $no++;
            } ?>
     </tbody>
 </table>
 <br><br><br>
 <table style="font-size:12px; padding:2px">
     <thead>
         <tr>
             <th>Layanan</th>
             <th>Jumlah</th>
         </tr>
     </thead>
     <tbody>
         <?php foreach($rekap as $rekap){?>
            <tr>
                <td><?= $rekap->LAYANAN ?></td>
                <td><?= $rekap->JUMLAH ?></td>
            </tr>
         <?php } ?>
     </tbody>
 </table>