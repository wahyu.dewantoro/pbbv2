@extends('page.master')
@section('judul')
<h1>Rekap Pelayanan</h1>
@endsection
@section('content')

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">

    </div>
    <div class="box-body">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/permohonanrekap' ?>">
            <div class="form-group">
                Tanggal permohonan
            </div>
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
            </div>
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
            </div>
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
            <?php if (isset($rk)) {
                echo anchor('laporan/execelJumlahPelayanan/' . $tgl1 . '/' . $tgl2, '<i class="fa fa-print"></i> Excel', 'class="btn btn-sm btn-success"');
            } ?>
        </form>

        <?php if (isset($rk)) { ?>
            <hr>
            <table class="table table-bordered table-striped" id="example3">
                <thead>
                    <tr>
                        <th width="3%">No</th>
                        <th align="center">Jenis Pelayanan</th>
                        <th align="center">Jumlah Pelayanan</th>
                        <!-- <th>Jumlah</th> -->
                        <th align="center">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                        <tr>
                            <td align="center"><?php echo $no ?></td>
                            <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                            <td align="center"><?php echo $rk->JUM ?></td>
                            <td align="center">
                                <!-- <a href="#" class="edit-record" data-id="<?php echo $rk->KD_JNS_PELAYANAN ?>">Show</a> | -->
                                <?php echo anchor('laporan/permohonanDetailNopel/' . $tgl1 . '/' . $tgl2 . '/' . $rk->KD_JNS_PELAYANAN, '<i class="fa fa-binoculars"></i> ', 'class="badge"'); ?>
                            </td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>

        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
    $(function() {
        $('#example3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        });

        $(document).on('click', '.edit-record', function(e) {
            e.preventDefault();
            $("#myModal").modal('show');
            $.post('<?php echo base_url() . "laporan/detailmodalrekappermohonan" ?>', {
                    id: $(this).attr('data-id'),
                    tgl1: '<?php echo $tgl1 ?>',
                    tgl2: '<?php echo $tgl2 ?>'
                },
                function(html) {
                    $(".modal-content").html(html);
                }
            );
        });
    });
</script>
@endsection