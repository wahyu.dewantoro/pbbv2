        @extends('page.master')
        @section('judul')
        <h1>
            <?php echo $judul ?>
            <div class="pull-right">
                <?php echo anchor('laporan/permohonanrekap_sppt_layanan', '<i class="fa fa-list"></i> Kembali', 'class="btn btn-sm btn-success"'); ?>
            </div>
        </h1>
        @endsection
        @section('content')
        <!-- Main content -->

        <!-- Default box -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><b>Detail SPPT</b></a></li>
                <li><a href="#tab_2" data-toggle="tab"><b>Detail Layanan</b></a></li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_1">
                    <!-- form tab 2 -->
                    <table class="table table-bordered table-striped" id="example4">
                        <thead>
                            <tr>
                                <th width="3%">No</th>
                                <th>No Layanan</th>
                                <th>NOP</th>
                                <th>Pemohon</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($sppt as $rk) { ?>
                                <tr>
                                    <td><?php echo $no ?></td>
                                    <td><?php echo $rk->NO_LAYANAN ?></td>
                                    <td><?php echo $rk->NOP ?></td>
                                    <td><?php echo $rk->NAMA_PEMOHON ?></td>
                                    <td><?php echo $rk->TANGGAL_TERIMA ?></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                        <div class="box box-success">
                            <div class="box-body">
                                <div id="idn">
                                    <div class="row">
                                        <table class="table table-bordered table-striped" id="example3" style="font-size:12px; padding:2px">
                                            <thead>
                                                <tr>
                                                    <th align="center">No</th>
                                                    <th align="center">No Layanan</th>
                                                    <th>Nama Pemohon</th>
                                                    <th>Status</th>
                                                    <th>Tanggal Terima</th>
                                                    <th>Tanggal Selesai</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1;
                                                foreach ($nopel as $rk) { ?>
                                                    <tr>
                                                        <td align="center"><?php echo $no ?></td>
                                                        <td><?php echo $rk->NO_LAYANAN ?></td>
                                                        <td><?php echo $rk->NAMA_PEMOHON ?></td>
                                                        <td><?php echo $rk->STATUS_KOLEKTIF; ?></td>
                                                        <td><?php echo $rk->TGL_TERIMA ?></td>
                                                        <td><?php echo $rk->TGL_SELESAI ?></td>
                                                        <!-- <td align="center"><?php echo $rk->TGL_TERIMA ?></td>
                                      <td align="center"><?php echo $rk->TGL_SELESAI ?></td> -->
                                                    </tr>
                                                <?php $no++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- body -->
                        </div>
                    </form>
                </div>
                <!-- tab 5 !-->

                <!-- tab 6 !-->
            </div>
            <!-- /.tab-content -->
        </div>
        @endsection