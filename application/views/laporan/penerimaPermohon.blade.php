@extends('page.master')
@section('judul')
<h1>
    Laporan Penerima
</h1>
@endsection
<!-- Main content -->
@section('content')
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">

    </div>
    <div class="box-body">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/penerimaPermohon.html' ?>">
            <div class="form-group">
                Tanggal permohonan
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
                <?php if (isset($rk)) {
                    echo anchor('laporan/execelPenerimaPermohonan/' . $tgl1 . '/' . $tgl2, '<i class="fa fa-print"></i> Excel', 'class="btn btn-sm btn-success"');
                } ?>
            </div>

        </form>

        <?php if (isset($rk)) { ?>
            <hr>
            <table class="table table-bordered table-striped" id="example2">
                <thead>
                    <tr>
                        <th width="3%">No</th>
                        <th>NIP</th>
                        <th>Pegawai</th>
                        <th>Bulan</th>
                        <th>Jumlah</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                        <tr>
                            <td align="center"><?php echo $no ?></td>
                            <td><?php echo $rk->NIP_PENERIMA ?></td>
                            <td><?php echo $rk->NAMA_PEGAWAI ?></td>
                            <td><?php echo $rk->BULAN . ' ' . $rk->TAHUN ?></td>
                            <td align="center"> <?php echo $rk->JUMLAH ?></td>
                            <td align="center"> <?php echo anchor('laporan/detailpenerimapermohonan/' . $rk->NIP_PENERIMA . '/' . $tgl1 . '/' . $tgl2, '<i class="fa fa-binoculars"></i>'); ?></td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>

        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $("#example2").dataTable();

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });


    });
</script>
@endsection