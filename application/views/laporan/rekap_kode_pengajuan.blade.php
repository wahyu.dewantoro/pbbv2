@extends('page.master')
@section('judul')
<h1>
    Rekap Kode Pengajuan Berkas
</h1>
@endsection
@section('content')
<!-- Main content -->

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">

    </div>
    <div class="box-body">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/rekap_kode_pengajuan.html' ?>">
            <div class="form-group">
                Tanggal permohonan
            </div>
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
            </div>
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
            </div>

            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
        </form>

        <?php if (isset($rk)) { ?>
            <hr>
            <table class="table table-bordered table-striped" id="example2">
                <thead>
                    <tr>
                        <th width="3%">No</th>
                        <th>Labe Pengajuan</th>
                        <th>Jenis Pengajuan</th>
                        <th>Jumlah</th>
                        <!-- <th>Detail</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                        <tr>
                            <td align="center"><?php echo $no ?></td>
                            <td><?php if ($rk->KD_PENGAJUAN != null) {
                                    echo $rk->KD_PENGAJUAN;
                                } else {
                                    echo "Belum Terdeteksi";
                                } ?></td>
                            <td><?php if ($rk->NM_PENGAJUAN != null) {
                                    echo $rk->NM_PENGAJUAN;
                                } else {
                                    echo "Belum Terdeteksi";
                                } ?></td>
                            <td align="center"><?php echo $rk->JUM ?></td>
                            <td align="center">
                                <!-- <a href="#" class="edit-record" data-id="<?php echo $rk->KD_JNS_PELAYANAN ?>">Show</a> | -->
                                <?php /*echo anchor('laporan/detailRekappermohonan/'.$tgl1.'/'.$tgl2.'/'.$rk->KD_PENGAJUAN,'<i class="fa fa-binoculars"></i> ','class="badge"');*/ ?>
                            </td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>

        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $("#example2").dataTable();

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });


    });

    $(function() {

        $(document).on('click', '.edit-record', function(e) {
            e.preventDefault();
            $("#myModal").modal('show');
            $.post('<?php echo base_url() . "laporan/detailmodalrekappermohonan" ?>', {
                    id: $(this).attr('data-id'),
                    tgl1: '<?php echo $tgl1 ?>',
                    tgl2: '<?php echo $tgl2 ?>'
                },
                function(html) {
                    $(".modal-content").html(html);
                }
            );
        });
    });
</script>
@endsection