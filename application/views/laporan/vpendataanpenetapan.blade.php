@extends('page.master')
@section('judul')
<h1>
    Pendataan dan Penetapan Objek Pajak
</h1>
@endsection
@section('content')

<div class="box">
    <div class="box-header with-border">
        <?= anchor('laporan/excelpenetapan', '<i class="fa fa-file-text"></i>Excel', 'class="btn btn-success btn-sm"') ?>
    </div>
    <div class="box-body">
        <table class="table table-striped" id="example2">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NOP</th>
                    <th>Kelurahan</th>
                    <th>Kecamatan</th>
                    <th>Nama WP</th>
                    <th>Pendataan</th>
                    <th>Penetapan</th>
                </tr>
            </thead>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url() ?>laporan/jsonpendataanpenetapan",
                "type": "POST"
            },
            columns: [{
                    "data": "KD_PROPINSI",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "KD_JNS_OP",
                    render: function(data, type, row) {
                        return row.KD_PROPINSI + '.' + row.KD_DATI2 + '.' + row.KD_KECAMATAN + '.' + row.KD_KELURAHAN + '.' + row.KD_BLOK + '.' + row.NO_URUT + '.' + row.KD_JNS_OP;
                    }
                },
                {
                    "data": "KELURAHAN"
                },
                {
                    "data": "KECAMATAN"
                },
                {
                    "data": "NM_WP"
                },
                {
                    "data": "PENDATAAN"
                },
                {
                    "data": "PENETAPAN"
                },
            ],
            order: [],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
@endsection