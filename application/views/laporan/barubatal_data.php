<table class="table table-bordered">
    <thead>
        <tr>
            <th width="3px">No</th>
            <th>Kecamatan</th>
            <th>Jenis</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data as $row) { ?>
            <tr>
                <td align="center"><?= $no++ ?></td>
                <td><?= $row->NM_KECAMATAN ?></td>
                <td><?= $row->TRANSAKSI ?></td>
                <td align="right">Rp.<?= number_format($row->JUMLAH, 0, '', '.') ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>