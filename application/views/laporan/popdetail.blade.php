@extends('page.master')
@section('judul')
<h1><?= $title ?></h1>
@endsection

<!-- Main content -->
@section('content')
<!-- Default box -->
<div class="box">
    <div class="box-header">
        <div class="box-title">

        </div>
        <div class="box-tools">
            <?php echo anchor('laporan/permohonanSelesai', 'Kembali', 'class="btn btn-sm btn-success"') ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?php if (isset($rk)) { ?>
            <table class="table table-bordered table-striped" id="example3">
                <thead>
                    <tr>
                        <th>Tahun</th>
                        <th>Nopel</th>
                        <th>NOP</th>
                        <th>Kolektif / Individu</th>
                        <th>Layanan</td>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($rk as $rk) { ?>
                        <tr>
                            <td align="center"><?= $rk->THN_PELAYANAN ?> </td>
                            <td><?= $rk->NO_LAYANAN ?> </td>
                            <td><?= $rk->NOP ?> </td>
                            <td><?= $rk->STATUS_KOLEKTIF ?> </td>
                            <td><?= $rk->NM_JENIS_PELAYANAN ?> </td>
                            <td><?php if ($rk->STT == 1) {
                                    echo "Selesai";
                                } else {
                                    echo "Proses";
                                } ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        <?php } ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection