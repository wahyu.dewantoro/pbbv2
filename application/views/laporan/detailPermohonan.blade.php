@extends('page.master')
@section('judul')
<h1> Laporan detail permohonan</h1>
@endsection


@section('content')
<!-- Default box -->
<div class="box box-success">

  <div class="box-body">
    <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/permohonanDetail' ?>">
      <div class="form-group">
        Tanggal permohonan
      </div>
      <div class="form-group">
        <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
      </div>
      <div class="form-group">
        <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
      </div>

      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
      <?php echo anchor("laporan/detailpermohonanexcel?t1=" . urlencode($tgl1) . "&t2=" . urlencode($tgl2), 'Excel', 'class="btn btn-success"'); ?>
    </form>

    <?php if (isset($rk)) { ?>
      <hr>

      <table class="table table-bordered table-striped" id="example3">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>No Layanan</th>
            <th>Nama Kuasa</th>
            <th>Subjek Pajak</th>
            <th>NOP</th>
            <th>Jenis Pelayanan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($rk as $rk) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td align="center"><?php echo $rk->TGL_TERIMA ?></td>
              <td><?php echo $rk->NO_LAYANAN ?></td>
              
              <td><?php echo $rk->NAMA_PEMOHON ?></td>
              <td><?php echo $rk->NM_WP_SPPT  ?></td>
              <td><?php echo $rk->NOP ?></td>
              <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
              
            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>

    <?php } ?>
  </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.chosen').select2({
      allow_single_deselect: true
    });

    $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

    $('#example3').DataTable({
      'paging': true,
      'lengthChange': false,
      'searching': false,
      'ordering': true,
      'info': true,
      'autoWidth': false
    });

  });
</script>
@endsection