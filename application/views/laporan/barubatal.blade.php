@extends('page.master')
@section('judul')
<h1>Laporan Pembatalan & Data Baru</h1>
@endsection
@section('content')
<div class="panel box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data</h3>
    </div>
    <div class="box-body">
        <form id="form" method="post" action="#" class="form-inline">
            <div class="form-group">
                <label for="tahun">Tahun</label>
                <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tahun" value="<?= date('Y') ?>">
            </div>
            <div class="form-group">
                <label for="jenis">Transaksi</label>
                <!-- <input type="email" class="form-control" id="jenis" placeholder="jane.doe@example.com"> -->
                <select name="jenis" id="jenis" class="form-control">
                    <option value="0">Semua</option>
                    <option value="1">Data Baru</option>
                    <option value="3">Pembatalan</option>
                </select>
            </div>
            <button id="submit" type="submit" class="btn btn-info"><i class="fa fa-search"></i> Tampilkan</button>
            <span id="btn"></span>
        </form>
        <div id="hasil"></div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        // data :   $("form").serialize()

        $("#form").on("submit", function(event) {
            event.preventDefault();

            console.log('sumited');
            $('#hasil').html('sedang di proses');
            $('#btn').html('');

            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: '<?= base_url() . 'laporan/taxobject_ajax' ?>', // the url where we want to POST
                data: $(this).serialize(), // our data object
                // dataType: 'json',
                success: function(res) {
                    $('#hasil').html(res);
                    $('#btn').html('<button type="button" class="btn btn-success" id="cetak">Excell</button>');
                    $('#cetak').click(function() {
                        location.href = "<?= base_url() . 'laporan/exceldatabarubatal' ?>?tahun=" + $('#tahun').val() + "&jenis=" + $('#jenis').val();
                    });
                },
                error: function(e) {
                    $('#hasil').html('error');
                }
            });
        });

        function cetak() {

        }

    });
</script>
@endsection