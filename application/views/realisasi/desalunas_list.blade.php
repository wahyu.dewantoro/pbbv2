@extends('page.master')
@section('judul')
<h1>
    Desa Lunas
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Realisasi</a></li>
    <li class="active"> Desa Lunas</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-header">
        <div class="box-title">
            data
        </div>
        <div class="box-tools">
        <?php echo anchor(site_url('realisasi/desa_create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-success"'); ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <table class="table table-bordered table-hover" id="example2">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Kecamatan</th>
                    <th>kelurahan</th>
                    <th>Tahun</th>
                    <th>BAKU</th>
                    <th>Tanggal</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="4">Jumlah</th>
                    <th align="right"><?= number_format($jumlah, 0, '', '.'); ?></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },


            "pageLength": 10,
            "searching": false,
            "bLengthChange": false,
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?= base_url() ?>realisasi/jsondesalunas",
                "type": "POST"
            },
            columns: [{
                    "data": "NM_KECAMATAN",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "NM_KECAMATAN"
                },
                {
                    "data": "NM_KELURAHAN"
                },
                {
                    "data": "TAHUN_PAJAK",
                    "className": "text-center"
                },
                {
                    "data": "BAKU",
                    "className": "text-right",
                    // render: function(data, type, row) {
                    //     return ribuan(data);
                    // }
                },
                {
                    "data": "TGL_LUNAS",
                    "className": "text-center"
                },
                {
                    "data": "NM_KELURAHAN",
                    "className": "text-center",
                    render: function(data, type, row) {
                        ue = '<?= base_url() ?>realisasi/editdesa?id=' + row.ID;
                        uh = '<?= base_url() ?>realisasi/hapusdesa?id=' + row.ID;
                        aksi = "<a class='btn btn-xs btn-success' href='" + ue + "'><i class='fa fa-edit'></i></a> <a onclick='return confirm(\"Apakah anda yakin?\")' class='btn btn-xs btn-danger' href='" + uh + "'><i class='fa fa-trash'></i></a>";
                        return aksi;
                    }
                }

            ],
            order: [
                [5, 'Asc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });


    $('#kd_kecamatan').change(function() {
        // alert($(this).val());
        var kd_kec = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>realisasi/getkelurahan",
            data: {
                'kd_kecamatan': kd_kec
            },
            success: function(response) {
                $("#kd_kelurahan").html(response);
            }
        });
    });
</script>
@endsection