@extends('page.master')
@section('judul')
<h1>
    Realisasi Target APBD
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Realisasi</a></li>
    <li class="active"> Realisasi Target APBD</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-header">
        <div class="box-title">
            List data
        </div>
        <div class="box-tools">
            <form class="form-inline" role="form" method="get" action="<?= base_url() . 'realisasi/apbd' ?>">
                <div class="form-group">
                    <select class="form-control" name="tahun">
                        <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                            <option <?php if ($q == $tahun) {
                                        echo "selected";
                                    } ?> value="<?= $q ?>"><?= $q ?></option>
                        <?php } ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
                <a href="<?= base_url('realisasi/targetsetup/'. $tahun) ?>" class="btn btn-info"><i class="fa fa-cogs"></i> Pengaturan </a>
            </form>
        </div>
    </div>
    <div class="box-body no-padding">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Buku</th>
                    <th>Sektor</th>
                    <th>Target</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $a=0;
                $b=0;
                $c=0;
                $no = 1;
                foreach ($rk as $rk) {
                    $a+=$rk->AWAL;
                    ?>
                    <tr>
                        <td align="center"><?= $no ?></td>
                        <td> <?= $rk->BUKU ?></td>
                        <td> <?= $rk->SEKTOR ?></td>
                        <td align="right"><?= $rk->AWAL<>'' ?number_format($rk->AWAL,0,'','.'):'' ?></td>
                    </tr>
                <?php $no++;
                } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td align="center" colspan="3" ><b>Total</b></td>
                    <td align="right"><b><?= $a<>'' ?number_format($a,0,'','.'):'' ?></b></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection