@extends('page.master')
@section('judul')
    <h1>
        Realisasi Target APBD Tahun <?= $tahun ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cogs"></i> Realisasi</a></li>
        <li class="active"> Realisasi Target APBD</li>
    </ol>
@endsection
@section('content')

    <div class="box">
        <div class="box-body">

            <form class="form-horizontal" action="<?= base_url('realisasi/prosestarget') ?>" method="post">

                <div class="row">
                    <div class="col-md-12">
                        <b>Buku 1 dan 2</b>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="control-label col-md-4">Perdesaan</label>
                            <div class="col-md-8">
                                <input type="text" name="perdesaan_12" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="control-label col-md-4">Perkotaan</label>
                            <div class="col-md-8">
                                <input type="text" name="perkotaan_12" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <b>Buku 3,4, dan 5</b>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="control-label col-md-4">Perdesaan</label>
                            <div class="col-md-8">
                                <input type="text" name="perdesaan_345" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="control-label col-md-4">Perkotaan</label>
                            <div class="col-md-8">
                                <input type="text" name="perkotaan_345" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="tahun" value="{{ $tahun }}">
                <button class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
            </form>

        </div>
    </div>
    <script>

    </script>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.form-control').keyup();

            $('.form-control').on('keyup',function(){
                var this_=$(this).val();
                console.log(this_)
                var res=formatangka(this_);
                $(this).val(res);
            }); 

        });

        function formatangka(objek) {
            a = objek;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + "." + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            // objek.value = c;
            return c;
        };



    </script>

@endsection
