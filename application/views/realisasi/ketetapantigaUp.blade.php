@extends('page.master')
@section('judul')
<h1>
    Laporan realisasi Buku 3,4 dan 5
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Realisasi</a></li>
    <li class="active"> Laporan realisasi Buku 3,4 dan 5</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-header">
        <div class="box-title">
            List Data
        </div>
        <div class="box-tools">
            <?= anchor('realisasi/cetaketetapantigaUp', '<i class="fa fa-file"></i> Excel', 'class="btn btn-sm btn-success"') ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table width="100%" class="table table-bordered table-hover" id="example3">
                <thead>
                    <tr>
                        <th rowspan="2">Kecamatan</th>
                        <th rowspan="2">Jumalah SPPT</th>
                        <th rowspan="2">Baku</th>
                        <th colspan="4">Realisasi</th>
                        <th rowspan="2">%</th>
                        <th colspan="2">Sisa</th>
                    </tr>
                    <tr>

                        <th>Sppt</th>
                        <th>Pokok</th>
                        <th>Denda</th>
                        <th>Jumlah</th>
                        <th>SPPT</th>
                        <th>Pokok</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $a = 0;
                    $b = 0;
                    $c = 0;
                    $d = 0;
                    $e = 0;
                    $f = 0;
                    $g = 0;
                    $h = 0;
                    $i = 0;

                    foreach ($data as $rk) { ?>
                        <tr>
                            <td><?php echo $rk->NM_KECAMATAN ?></td>
                            <td align="center"><?php echo $rk->JUM_SPPT;
                                                $a += $rk->JUM_SPPT; ?></td>
                            <td align="right"><?php echo number_format($rk->BAKU, '0', '', '.');
                                                $b += $rk->BAKU; ?></td>
                            <td align="center"><?php echo $rk->SPPT_REALISASI;
                                                $c += $rk->SPPT_REALISASI; ?></td>
                            <td align="right"><?php echo number_format($rk->POKOK_REALISASI, '0', '', '.');
                                                $d += $rk->POKOK_REALISASI; ?></td>
                            <td align="right"><?php echo number_format($rk->DENDA_REALISASI, '0', '', '.');
                                                $e += $rk->DENDA_REALISASI; ?></td>
                            <td align="right"><?php echo number_format($rk->JUMLAH_REALISASI, '0', '', '.');
                                                $f += $rk->JUMLAH_REALISASI; ?></td>
                            <td align="center"><?php echo number_format($rk->PERSEN_REALISASI, '2', ',', '.');
                                                $g += $rk->PERSEN_REALISASI; ?></td>
                            <td align="center"><?php echo $rk->SPPT_SISA;
                                                $h += $rk->SPPT_SISA; ?></td>
                            <td align="right"><?php echo number_format($rk->POKOK_SISA, '0', '', '.');
                                                $i += $rk->POKOK_SISA; ?></td>

                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Jumlah</th>
                        <td align="center"><b><?= number_format($a, 0, '', '.') ?></b></td>
                        <td align="right"><b><?= number_format($b, 0, '', '.') ?></b></td>
                        <td align="center"><b><?= number_format($c, 0, '', '.') ?></b></td>
                        <td align="right"><b><?= number_format($d, 0, '', '.') ?></b></td>
                        <td align="right"><b><?= number_format($e, 0, '', '.') ?></b></td>
                        <td align="right"><b><?= number_format($f, 0, '', '.') ?></b></td>
                        <td align="center"><b><?= number_format($g, 0, '', '.') ?> %</b></td>
                        <td align="right"><b><?= number_format($h, 0, '', '.') ?> </b></td>
                        <td align="right"><b><?= number_format($i, 0, '', '.') ?></b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection