@extends('page.master')
@section('judul')
<h1>
    Nomor SK
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>Nomor SK</li>
    <li class="active"><?= $JENIS ?></li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-header with-border">
        <div class="box-title">
            Form
        </div>
        <div class="box-tools">
            <?= anchor('master/nomorsk', '<i class="fa fa-angle-double-left"></i> kembali', 'class="btn btn-sm btn-success"') ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" method="post" action="<?= base_url() . 'master/prosesupdatesk' ?>">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4">Jenis SK <sup>*</sup></label>
                        <div class="col-md-8">
                            <input type="text" name="JENIS" id="JENIS" readonly class="form-control form-control-sm" value="<?= $JENIS ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Format Nomor <sup>*</sup></label>
                        <div class="col-md-8">
                            <input type="text" name="NOMOR" id="NOMOR" class="form-control form-control-sm" value="<?= $NOMOR ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Nama Pejabat <sup>*</sup></label>
                        <div class="col-md-8">
                            <input type="text" name="NAMA_PEGAWAI" id="NAMA_PEGAWAI" class="form-control form-control-sm" value="<?= $NAMA_PEGAWAI ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4">Jabatan <sup>*</sup></label>
                        <div class="col-md-8">
                            <input type="text" name="JABATAN" id="JABATAN" class="form-control form-control-sm" value="<?= $JABATAN ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">NIP <sup>*</sup></label>
                        <div class="col-md-8">
                            <input type="text" name="NIP" id="NIP" class="form-control form-control-sm" value="<?= $NIP ?>" required>
                        </div>
                    </div>
                    <p class="text-right">
                        <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button>
                    </p>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection