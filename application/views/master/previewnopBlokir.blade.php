@extends('page.master')
@section('judul')
<h1>
  Preview Blokir NOP
  <div class="pull-right">

  </div>
</h1>
@endsection
@section('content')

 
  <div class="box">

    <div class="box-body">
      

      <form method="POST" action="<?php echo base_url() . 'master/submitblokir' ?>">
        <input type="hidden" name="count" value="<?= $count ?>">
        <button class="btn btb-sm btn-success"><i class="fa fa-sign-in"></i> Submit</button>
        <a class="btn btn-warning" href="{{ base_url('master/blokirNop') }}">Batal</a>

        <table class="table table-bordered table-striped" id="example1">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>NOP</th>
              <th>Tahun</th>
              <th>Nama WP</th>


            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($data as $rk) { ?>

              <tr>
                <td align="center"><?php echo $no ?></td>
                <td> <?=

                        $rk->KD_PROPINSI . '.' .
                          $rk->KD_DATI2 . '.' .
                          $rk->KD_KECAMATAN . '.' .
                          $rk->KD_KELURAHAN . '.' .
                          $rk->KD_BLOK . '.' .
                          $rk->NO_URUT . '.' .
                          $rk->KD_JNS_OP
                      ?> </td>
                <td> <?= $rk->THN_PAJAK_SPPT ?> </td>
                <td> <?= $rk->NM_WP_SPPT ?>

                  <input type="hidden" name="KD_KECAMATAN[]" value="<?php echo $rk->KD_KECAMATAN ?>">
                  <input type="hidden" name="KD_KELURAHAN[]" value="<?php echo $rk->KD_KELURAHAN ?>">
                  <input type="hidden" name="KD_BLOK[]" value="<?php echo $rk->KD_BLOK ?>">
                  <input type="hidden" name="NO_URUT[]" value="<?php echo $rk->NO_URUT ?>">
                  <input type="hidden" name="KD_JNS_OP[]" value="<?php echo $rk->KD_JNS_OP ?>">
                  <input type="hidden" name="THN_PAJAK_SPPT[]" value="<?php echo $rk->THN_PAJAK_SPPT ?>">
                </td>




              </tr>
            <?php $no++;
            } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
@endsection