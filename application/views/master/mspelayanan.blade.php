@extends('page.master')
@section('judul')
<h1>
    Jenis Pelayanan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li class="active">Jenis Pelayanan</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Layanan</th>

                </tr>
            </thead>
            <tbody>
                <?php $no = 1;
                foreach ($data as $rk) { ?>
                    <tr>
                        <td align="center"><?php echo $no ?></td>
                        <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                    </tr>
                <?php $no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
@endsection