@extends('page.master')
@section('judul')
<h1>
    Form Permohonan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Pelayanan</a></li>
    <li class="active">Form Permohonan</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
            </div>
            <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">No Pelayanan </label>
                                    <div class="col-md-8">
                                        <table>
                                            <tr>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="4" name="THN_PELAYANAN" id="THN_PELAYANAN" placeholder="Tahun" value="<?php echo $THN_PELAYANAN; ?>" required /></td>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="4" name="BUNDEL_PELAYANAN" id="BUNDEL_PELAYANAN" placeholder="Bundel" required value="<?php echo $BUNDEL_PELAYANAN; ?>" /></td>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="3" name="NO_URUT_PELAYANAN" id="NO_URUT_PELAYANAN" placeholder="No urut" required value="<?php echo $NO_URUT_PELAYANAN; ?>" /></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">No Surat Permohonan </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control inputs" required name="NO_SRT_PERMOHONAN" id="NO_SRT_PERMOHONAN" placeholder="No surat permohonan" value="<?php echo $NO_SRT_PERMOHONAN; ?>" required />

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Status Kolektif </label>
                                    <div class="col-md-8">
                                        <select class="select2 form-control" required name="STATUS_KOLEKTIF" id="STATUS_KOLEKTIF" style="width: 100%;">
                                            <option value=""></option>
                                            <option selected value="0">1. Individu</option>
                                            <option value="1">2. Kolektif / Masal</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Jenis Pelayanan </label>
                                    <div class="col-md-8">
                                        <select class="form-control select2" required name="KD_JNS_PELAYANAN" id="KD_JNS_PELAYANAN" style="width: 100%;" data-placeholder="Silahkan Pilih">
                                            <option value=""></option>
                                            <?php foreach ($pelayanan as $rp) { ?>
                                                <option value="<?php echo $rp->KD_JNS_PELAYANAN ?>"><?php echo $rp->KD_JNS_PELAYANAN . ' - ' . $rp->NM_JENIS_PELAYANAN ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Tgl Terima </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input_tgl" required name="TGL_TERIMA_DOKUMEN_WP" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Tgl Surat Permohonan </label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control " name="TGL_SURAT_PERMOHONAN" data-inputmask="'mask': '99-99-9999'" id="TGL_SURAT_PERMOHONAN" placeholder="TGL SURAT PERMOHONAN" value="<?php echo $TGL_SURAT_PERMOHONAN; ?>" />

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Tgl Perkiraan Selesai </label>
                                    <div class="col-md-6">
                                        <?php
                                        $dt = date("d-m-Y");
                                        $ts = date("d-m-Y", strtotime("$dt +30 day"));   ?>
                                        <input type="text" class="form-control " required name="TGL_PERKIRAAN_SELESAI" data-inputmask="'mask': '99-99-9999'" id="TGL_PERKIRAAN_SELESAI" placeholder="TGL PERKIRAAN SELESAI" value="<?php echo $ts; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="idn">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2  control-label">Scan Barcode </label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="barcode" id="barcode">
                                            <span id="test"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2  control-label">NOP </label>
                                        <div class="col-md-2 dua">
                                            <input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" value="35">
                                        </div>
                                        <div class="col-md-2 dua">
                                            <input type="text" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" value="07">
                                        </div>
                                        <div class="col-md-2 tiga">
                                            <input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=''">
                                        </div>
                                        <div class="col-md-2 tiga">
                                            <input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=''">
                                        </div>
                                        <div class="col-md-2 tiga">
                                            <input type="text" maxlength="3" class="form-control" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=''">
                                        </div>
                                        <div class="col-md-2 empat">
                                            <input type="text" maxlength="4" class="form-control" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=''">
                                        </div>
                                        <div class="col-md-2 satu">
                                            <input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=''">
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2  control-label">Nama Pemohon </label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" required name="NAMA_PEMOHON" id="NAMA_PEMOHON" placeholder="Nama pemohon" value="<?php echo $NAMA_PEMOHON; ?>" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Alamat Pemohon </label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" required name="ALAMAT_PEMOHON" id="ALAMAT_PEMOHON" placeholder="Alamat" value="<?php echo $ALAMAT_PEMOHON; ?>" />

                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-8 col-md-offset-2"> -->

                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Lampiran </label>
                                    <div class="col-md-10">
                                        <select class="form-control select2" multiple name="lampiran[]" id="lampiran" data-placeholder="Silahkan pilih" required style="width: 100%;">
                                            <option value=""></option>
                                            <?php foreach ($dl as $dl) { ?>
                                                <option value="<?php echo str_replace(' ', '', $dl->KODE) ?>"><?php echo $dl->NAMA_LAMPIRAN ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Catatan </label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" required name="CATATAN_PST" id="CATATAN_PST" placeholder="catatan" value="<?php echo $CATATAN_PST; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">E Mail </label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control " name="IP_EMAIL" id="email" placeholder="Email" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">No. HP </label>
                                    <div class="col-md-10">
                                        <input maxlength="14" type="number" class="form-control" required name="IP_HP" id="IP_HP" placeholder="No. Hp" value="" />
                                    </div>
                                </div>
                                <div class="form-group" id="jml_berkas">
                                </div>
                                <div id="demo">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <div class="btn-group">
                                            <button type="submit" id="myBtn" disabled="" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Submit</button>
                                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="bayar"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<style type="text/css">
    .dua {
        width: 9.5%;
    }

    .tiga {
        width: 11%;
    }

    .empat {
        width: 12%;
    }

    .satu {
        width: 9%;
    }
</style>
@endsection
<!-- Select2 -->
@section('script')
<!-- Select2 -->
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        // select2
        $('.select2').select2({
            allowClear: true
        });
        kosongkan();
        var nomor = 0;
        var status = "";

        // satu
        $('#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON').on('keyup change', function() {
            dds = $('#KD_JNS_PELAYANAN').val();
            if (dds == '01') {
                $('#myBtn').prop('disabled', false);
            } else {
                $('#myBtn').prop('disabled', true);
            }
            getnopindividu();
        });

        // dua
        $("#STATUS_KOLEKTIF").on('change', function(e) {
            var kolek = $('#STATUS_KOLEKTIF').val();
            dds = $('#KD_JNS_PELAYANAN').val();
            if (dds == '01') {
                $('#myBtn').prop('disabled', false);
            } else {
                $('#myBtn').prop('disabled', true);
            }

            if (kolek == 1) {
                // kolektif
                kolektif();
                $('.chosen').select2({
                    allow_single_deselect: true
                });

                $(".form-control").keyup(function() {
                    if (this.value.length == this.maxLength) {
                        var nextIndex = $('input:text').index(this) + 1;
                        $('input:text')[nextIndex].focus();
                    }
                });

                // barcode
                $("#barcodea").keyup(function() {
                    var barcode = $("#barcodea").val();
                    getnopdecodea(barcode);
                });
                $("#barcodea").keydown(function() {
                    var barcode = $("#barcodea").val();
                    getnopdecodea(barcode);
                });
                //  alamat dan nama wp
                $('#KD_JNS_OP_PEMOHONA,#NO_URUT_PEMOHONA,#KD_BLOK_PEMOHONA,#KD_KELURAHAN_PEMOHONA,#KD_KECAMATAN_PEMOHONA,#KD_DATI2_PEMOHONA,#KD_PROPINSI_PEMOHONA').on('keyup change keydown', function() {
                    nopKolektif();
                });
                // cek bayar
                $("#KD_JNS_PELAYANAN,#KD_JNS_OP_PEMOHONA,#NO_URUT_PEMOHONA,#KD_BLOK_PEMOHONA,#KD_KELURAHAN_PEMOHONA,#KD_KECAMATAN_PEMOHONA,#KD_DATI2_PEMOHONA,#KD_PROPINSI_PEMOHONA").change(function() {
                    var kd_layanan = $('#KD_JNS_PELAYANAN').val();
                    var a = $('#KD_PROPINSI_PEMOHONA').val();
                    var b = $('#KD_DATI2_PEMOHONA').val();
                    var c = $('#KD_KECAMATAN_PEMOHONA').val();
                    var d = $('#KD_KELURAHAN_PEMOHONA').val();
                    var e = $('#KD_BLOK_PEMOHONA').val();
                    var f = $('#NO_URUT_PEMOHONA').val();
                    var g = $('#KD_JNS_OP_PEMOHONA').val();
                    cekriwayatbayar(kd_layanan, a, b, c, d, e, f, g);
                });

                $("#tambahdiag").click(function(e) {
                    e.preventDefault();
                    $('#tambahdiag').prop('disabled', true);
                    $('#kontentdiag').append(
                        '<tr class="barisdiag">' +
                        '<input type="hidden" value="' + $('#KD_PROPINSI_PEMOHONA').val() + '" class="form-control" name="KD_PROPINSI_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_DATI2_PEMOHONA').val() + '" class="form-control" name="KD_DATI2_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_KECAMATAN_PEMOHONA').val() + '" class="form-control" name="KD_KECAMATAN_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_KELURAHAN_PEMOHONA').val() + '" class="form-control" name="KD_KELURAHAN_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_BLOK_PEMOHONA').val() + '"  class="form-control" name="KD_BLOK_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#NO_URUT_PEMOHONA').val() + '" class="form-control" name="NO_URUT_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_JNS_OP_PEMOHONA').val() + '" class="form-control" name="KD_JNS_OP_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#KD_JNS_PELAYANAN').val() + '" class="form-control" name="KD_JNS_PELAYANANB[]">' +
                        '<input type="hidden" value="' + $('#NAMA_PEMOHONA').val() + '" class="form-control" name="NAMA_PEMOHONB[]">' +
                        '<input type="hidden" value="' + $('#ALAMAT_PEMOHONA').val() + '" class="form-control" name="ALAMAT_PEMOHONB[]">' +
                        '<td align=\"center\">' + $('.s').val() + '</td>' +
                        '<td>' + $('#KD_PROPINSI_PEMOHONA').val() + '.' + $('#KD_DATI2_PEMOHONA').val() + '.' + $('#KD_KECAMATAN_PEMOHONA').val() + '.' + $('#KD_KELURAHAN_PEMOHONA').val() + '.' + $('#KD_BLOK_PEMOHONA').val() + '.' + $('#NO_URUT_PEMOHONA').val() + '.' + $('#KD_JNS_OP_PEMOHONA').val() + '</td>' +
                        '<td>' + $('#NAMA_PEMOHONA').val() + '</td>' +
                        '<td>' + $('#ALAMAT_PEMOHONA').val() + '</td>' +
                        '<td>' + $('#KD_JNS_PELAYANAN').find("option:selected").text() + '</td>' +
                        '<td align=\"center\"><a href="#" class="remove_project_file"  ><i class="btn btn-xs btn-danger fa fa-trash"></i></a></td></tr>'
                    );

                    $("#urt").val((nomor + 1) + 1);
                    nomor++;
                    $('#KD_PROPINSI_PEMOHONA').val('35');
                    $('#KD_DATI2_PEMOHONA').val('07');
                    $('#KD_KECAMATAN_PEMOHONA').val(null);
                    $('#KD_KELURAHAN_PEMOHONA').val(null);
                    $('#KD_BLOK_PEMOHONA').val(null);
                    $('#NO_URUT_PEMOHONA').val(null);
                    $('#KD_JNS_OP_PEMOHONA').val(null);
                    $('#NAMA_PEMOHONA').val(null);
                    $('#ALAMAT_PEMOHONA').val(null);
                    $('#bayar').html('');
                });
                $('#kontentdiag').on('click', '.remove_project_file', function(e) {
                    e.preventDefault();
                    $(this).parents(".barisdiag").remove();
                });
            } else {
                e.preventDefault();

                individu();
                $(".form-control").keyup(function() {
                    if (this.value.length == this.maxLength) {
                        var nextIndex = $('input:text').index(this) + 1;
                        $('input:text')[nextIndex].focus();
                    }
                });

                $('#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON').on('keyup change', function() {
                    $('#myBtn').prop('disabled', true);
                    getnopindividu();
                    var kd_layanan = $('#KD_JNS_PELAYANAN').val();
                    var a = $('#KD_PROPINSI_PEMOHON').val();
                    var b = $('#KD_DATI2_PEMOHON').val();
                    var c = $('#KD_KECAMATAN_PEMOHON').val();
                    var d = $('#KD_KELURAHAN_PEMOHON').val();
                    var e = $('#KD_BLOK_PEMOHON').val();
                    var f = $('#NO_URUT_PEMOHON').val();
                    var g = $('#KD_JNS_OP_PEMOHON').val();
                    cekriwayatbayar(kd_layanan, a, b, c, d, e, f, g);
                });
            }
            $("#barcode").keyup(function() {
                var barcode = $("#barcode").val();
                getnopdecode(barcode);
            });
            $("#barcode").keydown(function() {
                var barcode = $("#barcode").val();
                getnopdecode(barcode);
            });
        });

        // tiga
        function cekriwayatbayar(kd_layanan, a, b, c, d, e, f, g) {
            var nop = a + "." + b + "." + c + "." + d + "." + e + "." + f + "." + g;
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/cekriwayatbayar' ?>",
                method: "POST",
                data: "var=" + kd_layanan + '_' + nop,
                cache: false,
                success: function(hasil) {
                    // alert(hasil);
                    $("#bayar").html(hasil);

                }
            });
        }

        // empat
        function deskripsia(nop) {
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/deskripsialert' ?>",
                method: "POST",
                data: "nop=" + nop,
                cache: false,
                success: function(response) {
                    $("#bayar").html('');
                    alert(response);

                }
            });
        }

        // lima
        function getnopdecodea(barcode) {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'permohonan/decodebarcode' ?>",
                data: {
                    'barcode': barcode
                },
                success: function(response) {
                    var datashow = JSON.parse(response);
                    $('#KD_PROPINSI_PEMOHONA').val(datashow[0].KD_PROPINSI_PEMOHON);
                    $('#KD_DATI2_PEMOHONA').val(datashow[0].KD_DATI2_PEMOHON);
                    $('#KD_KECAMATAN_PEMOHONA').val(datashow[0].KD_KECAMATAN_PEMOHON);
                    $('#KD_KELURAHAN_PEMOHONA').val(datashow[0].KD_KELURAHAN_PEMOHON);
                    $('#KD_BLOK_PEMOHONA').val(datashow[0].KD_BLOK_PEMOHON);
                    $('#NO_URUT_PEMOHONA').val(datashow[0].NO_URUT_PEMOHON);
                    $('#KD_JNS_OP_PEMOHONA').val(datashow[0].KD_JNS_OP_PEMOHON);

                    var a = datashow[0].KD_PROPINSI_PEMOHON;
                    var b = datashow[0].KD_DATI2_PEMOHON;
                    var c = datashow[0].KD_KECAMATAN_PEMOHON;
                    var d = datashow[0].KD_KELURAHAN_PEMOHON;
                    var e = datashow[0].KD_BLOK_PEMOHON;
                    var f = datashow[0].NO_URUT_PEMOHON;
                    var g = datashow[0].KD_JNS_OP_PEMOHON;
                    var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;

                    $.ajax({
                        url: "<?php echo base_url() . 'permohonan/getNop' ?>",
                        method: "POST",
                        data: "nop =" + nop,
                        cache: false,
                        success: function(response) {
                            if (response == 0) {

                                $("input#NAMA_PEMOHON").val('');
                                $("input#ALAMAT_PEMOHONA").val('');
                                $("#bayar").html('');
                                $('#KD_KECAMATAN_PEMOHONA').val('');
                                $('#KD_KELURAHAN_PEMOHONA').val('');
                                $('#KD_BLOK_PEMOHONA').val('');
                                $('#NO_URUT_PEMOHONA').val('');
                                $('#KD_JNS_OP_PEMOHONA').val('');
                                // deskripsi(nop);
                                $.ajax({
                                    url: "<?php echo base_url() . 'permohonan/deskripsialert' ?>",
                                    method: "POST",
                                    data: "nop=" + nop,
                                    cache: false,
                                    success: function(response) {
                                        alert(response);
                                        $("#bayar").html('');
                                    }
                                });
                            } else {
                                var datashow = JSON.parse(response);
                                $("input#NAMA_PEMOHONA").val(datashow[0].NAMA_PEMOHON);
                                $("input#ALAMAT_PEMOHONA").val(datashow[0].ALAMAT_PEMOHON);
                            }
                        }
                    });
                }
            });
        }

        // enam
        function getnopdecode(barcode) {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'permohonan/decodebarcode' ?>",
                data: {
                    'barcode': barcode
                },
                success: function(response) {
                    var datashow = JSON.parse(response);
                    $('#KD_PROPINSI_PEMOHON').val(datashow[0].KD_PROPINSI_PEMOHON);
                    $('#KD_DATI2_PEMOHON').val(datashow[0].KD_DATI2_PEMOHON);
                    $('#KD_KECAMATAN_PEMOHON').val(datashow[0].KD_KECAMATAN_PEMOHON);
                    $('#KD_KELURAHAN_PEMOHON').val(datashow[0].KD_KELURAHAN_PEMOHON);
                    $('#KD_BLOK_PEMOHON').val(datashow[0].KD_BLOK_PEMOHON);
                    $('#NO_URUT_PEMOHON').val(datashow[0].NO_URUT_PEMOHON);
                    $('#KD_JNS_OP_PEMOHON').val(datashow[0].KD_JNS_OP_PEMOHON);
                    // getnopindividu();
                    var a = datashow[0].KD_PROPINSI_PEMOHON;
                    var b = datashow[0].KD_DATI2_PEMOHON;
                    var c = datashow[0].KD_KECAMATAN_PEMOHON;
                    var d = datashow[0].KD_KELURAHAN_PEMOHON;
                    var e = datashow[0].KD_BLOK_PEMOHON;
                    var f = datashow[0].NO_URUT_PEMOHON;
                    var g = datashow[0].KD_JNS_OP_PEMOHON;
                    var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;

                    $.ajax({
                        url: "<?php echo base_url() . 'permohonan/getNop' ?>",
                        method: "POST",
                        data: "nop=" + nop,
                        cache: false,
                        success: function(response) {
                            if (response == 0) {
                                // alert('NOP sudah mengajukan permohonan');
                                $("input#NAMA_PEMOHON").val('');
                                $("input#ALAMAT_PEMOHON").val('');
                                $("#bayar").html('');
                                $('#KD_KECAMATAN_PEMOHON').val('');
                                $('#KD_KELURAHAN_PEMOHON').val('');
                                $('#KD_BLOK_PEMOHON').val('');
                                $('#NO_URUT_PEMOHON').val('');
                                $('#KD_JNS_OP_PEMOHON').val('');
                                // deskripsi(nop);
                                $.ajax({
                                    url: "<?php echo base_url() . 'permohonan/deskripsialert' ?>",
                                    method: "POST",
                                    data: "nop=" + nop,
                                    cache: false,
                                    success: function(response) {
                                        alert(response);
                                        $("#bayar").html('');
                                    }
                                });
                            } else {
                                var datashow = JSON.parse(response);
                                $("input#NAMA_PEMOHON").val(datashow[0].NAMA_PEMOHON);
                                $("input#ALAMAT_PEMOHON").val(datashow[0].ALAMAT_PEMOHON);
                            }
                        }
                    });
                }
            });
        }

        // tujuh
        function getnopindividu() {
            var a = $('#KD_PROPINSI_PEMOHON').val();
            var b = $('#KD_DATI2_PEMOHON').val();
            var c = $('#KD_KECAMATAN_PEMOHON').val();
            var d = $('#KD_KELURAHAN_PEMOHON').val();
            var e = $('#KD_BLOK_PEMOHON').val();
            var f = $('#NO_URUT_PEMOHON').val();
            var g = $('#KD_JNS_OP_PEMOHON').val();
            var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/getNop' ?>",
                method: "POST",
                data: "nop=" + nop,
                cache: false,
                success: function(response) {
                    if (response == 0) {
                        $("input#NAMA_PEMOHON").val('');
                        $("input#ALAMAT_PEMOHON").val('');
                        $("#bayar").html('');
                        $('#KD_KECAMATAN_PEMOHON').val('');
                        $('#KD_KELURAHAN_PEMOHON').val('');
                        $('#KD_BLOK_PEMOHON').val('');
                        $('#NO_URUT_PEMOHON').val('');
                        $('#KD_JNS_OP_PEMOHON').val('');
                        // deskripsi(nop);
                        $.ajax({
                            url: "<?php echo base_url() . 'permohonan/deskripsialert' ?>",
                            method: "POST",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(response) {
                                alert(response);
                                $("#bayar").html('');
                            }
                        });
                    } else {
                        var datashow = JSON.parse(response);
                        $("input#NAMA_PEMOHON").val(datashow[0].NAMA_PEMOHON);
                        $("input#ALAMAT_PEMOHON").val(datashow[0].ALAMAT_PEMOHON);
                    }
                }
            });
        }

        // delapan
        function kosongkan() {
            $('#NO_SRT_PERMOHONAN').val('-');
            $('#barcode').val('');
            $('#STATUS_KOLEKTIF').val('0').trigger('chosen:updated');
            $('#KD_JNS_PELAYANAN').val('').trigger('chosen:updated');
            $('#KD_PROPINSI_PEMOHON').val('35');
            $('#KD_DATI2_PEMOHON').val('07');
            $('#KD_KECAMATAN_PEMOHON').val('');
            $('#KD_KELURAHAN_PEMOHON').val('');
            $('#KD_BLOK_PEMOHON').val('');
            $('#NO_URUT_PEMOHON').val('');
            $('#KD_JNS_OP_PEMOHON').val('');
            $('#NAMA_PEMOHON').val('');
            $('#ALAMAT_PEMOHON').val('');
            $('#CATATAN_PST').val('');
            $('#email').val('');
            $('#IP_HP').val('');
            $('#jml_berkas').val('');
            $('#lampiran').val('').trigger('chosen:updated');
            $('#demo').html('');
        }

        // sembilan (get nop kolektif)
        function nopKolektif() {
            var a = $('#KD_PROPINSI_PEMOHONA').val();
            var b = $('#KD_DATI2_PEMOHONA').val();
            var c = $('#KD_KECAMATAN_PEMOHONA').val();
            var d = $('#KD_KELURAHAN_PEMOHONA').val();
            var e = $('#KD_BLOK_PEMOHONA').val();
            var f = $('#NO_URUT_PEMOHONA').val();
            var g = $('#KD_JNS_OP_PEMOHONA').val();
            var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/getNop' ?>",
                method: "POST",
                data: "nop=" + nop,
                cache: false,
                success: function(response) {
                    if (response == 0) {
                        // alert('NOP sudah mengajukan permohonan');
                        $("input#NAMA_PEMOHONA").val('');
                        $("input#ALAMAT_PEMOHONA").val('');
                        $("#bayar").html('');
                        $('#KD_KECAMATAN_PEMOHONA').val('');
                        $('#KD_KELURAHAN_PEMOHONA').val('');
                        $('#KD_BLOK_PEMOHONA').val('');
                        $('#NO_URUT_PEMOHONA').val('');
                        $('#KD_JNS_OP_PEMOHONA').val('');
                        deskripsia(nop);

                    } else {
                        var datashow = JSON.parse(response);
                        $("input#NAMA_PEMOHONA").val(datashow[0].NAMA_PEMOHON);
                        $("input#ALAMAT_PEMOHONA").val(datashow[0].ALAMAT_PEMOHON);
                    }
                }
            });
        }

        // sepuluh
        function kolektif() {
            var kl = $('#KD_JNS_PELAYANAN').val();
            $('#idn').html('');
            if (kl != '01') {
                $('#demo').html('<div class="form-group">' +
                    '<label class="col-md-2  control-label">Scan Barcode </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text"   class="form-control" name="barcode" id="barcodea" > ' +
                    '<span id="test"></span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">NOP Kolektif</label>' +
                    '<div class="col-md-1 dua">' +
                    '<input type="text" maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHONA" id="KD_PROPINSI_PEMOHONA" value="35">' +
                    '</div>' +
                    '<div class="col-md-1 dua">' +
                    '<input type="text" maxLength="2" class="form-control" name="KD_DATI2_PEMOHONA" id="KD_DATI2_PEMOHONA" value="07">' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text" maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHONA" id="KD_KECAMATAN_PEMOHONA" onfocus="this.value=(null)" >' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text" maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHONA" id="KD_KELURAHAN_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text"  maxLength="3" class="form-control" name="KD_BLOK_PEMOHONA" id="KD_BLOK_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 empat">' +
                    '<input type="text" maxLength="4" class="form-control" name="NO_URUT_PEMOHONA" id="NO_URUT_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 satu">' +
                    '<input type="text" maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHONA" id="KD_JNS_OP_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">Nama Pemohon </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text"   class="form-control"   name="NAMA_PEMOHONA" id="NAMA_PEMOHONA" placeholder="Nama pemohon kolektif"  />' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text" class="form-control"   name="ALAMAT_PEMOHONA" id="ALAMAT_PEMOHONA" placeholder="Alamat"  />' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<div class="col-md-8 col-md-offset-2">' +
                    '<button class="btn btn-success btn-xs" disabled="" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</button>' +
                    '<input id="urt" class="s" type="hidden" value="1">' +
                    '<table class="table table-bordered">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>NO</th>' +
                    '<th>NOP</th>' +
                    '<th>NAMA WP</th>' +
                    '<th>Alamat WP</th>' +
                    '<th>Jenis Layanan</th>' +
                    '<th>Aksi</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody id="kontentdiag"></tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>');
            } else {
                $('#demo').html('<div class="form-group">' +
                    '<label class="col-md-2  control-label">Scan Barcode </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text"   class="form-control" name="barcode" id="barcodea" > ' +
                    '<span id="test"></span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">NOP Kolektif</label>' +
                    '<div class="col-md-1 dua">' +
                    '<input type="text" maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHONA" id="KD_PROPINSI_PEMOHONA" value="35">' +
                    '</div>' +
                    '<div class="col-md-1 dua">' +
                    '<input type="text" maxLength="2" class="form-control" name="KD_DATI2_PEMOHONA" id="KD_DATI2_PEMOHONA" value="07">' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text" maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHONA" id="KD_KECAMATAN_PEMOHONA" onfocus="this.value=(null)" >' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text" maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHONA" id="KD_KELURAHAN_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 tiga">' +
                    '<input type="text"  maxLength="3" class="form-control" name="KD_BLOK_PEMOHONA" id="KD_BLOK_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 empat">' +
                    '<input type="text" maxLength="4" class="form-control" name="NO_URUT_PEMOHONA" id="NO_URUT_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '<div class="col-md-1 satu">' +
                    '<input type="text" maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHONA" id="KD_JNS_OP_PEMOHONA" onfocus="this.value=(null)">' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">Nama Pemohon </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text"   class="form-control"   name="NAMA_PEMOHONA" id="NAMA_PEMOHONA" placeholder="Nama pemohon kolektif"  />' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                    '<div class="col-md-10">' +
                    '<input type="text" class="form-control"   name="ALAMAT_PEMOHONA" id="ALAMAT_PEMOHONA" placeholder="Alamat"  />' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<div class="col-md-8 col-md-offset-2">' +
                    '<button class="btn btn-success btn-xs"  id="tambahdiag"><i class="fa fa-plus"></i> Tambah</button>' +
                    '<input id="urt" class="s" type="hidden" value="1">' +
                    '<table class="table table-bordered">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>NO</th>' +
                    '<th>NOP</th>' +
                    '<th>NAMA WP</th>' +
                    '<th>Alamat WP</th>' +
                    '<th>Jenis Layanan</th>' +
                    '<th>Aksi</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody id="kontentdiag"></tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>');
            }
        }

        // sebelas
        function individu() {
            $('#demo').html('');
            $('#idn').html('<div class="row">' +
                '<div class="col-md-12">' +
                '<div class="form-group">' +
                '<label class="col-md-2  control-label">Scan Barcode </label>' +
                '<div class="col-md-10">' +
                '<input type="text"   class="form-control" name="barcode" id="barcode" >  ' +
                '<span id="test"></span>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-2 control-label">NOP </label>' +
                '<div class="col-md-1 dua"><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" value="35"></div>' +
                '<div class="col-md-1 dua"><input type="text" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" value="07"></div>' +
                '<div class="col-md-1 tiga"><input type="text"  maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=(null)"></div>' +
                '<div class="col-md-1 tiga"><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=(null)"></div>' +
                '<div class="col-md-1 tiga"><input type="text" maxlength="3"  class="form-control" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=(null)"></div>' +
                '<div class="col-md-1 empat"><input type="text" maxlength="4" class="form-control" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=(null)"></div>' +
                '<div class="col-md-1 satu"><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=(null)"></div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-2  control-label">Nama Pemohon </label>' +
                '<div class="col-md-10">' +
                '<input type="text"   class="form-control" required name="NAMA_PEMOHON" id="NAMA_PEMOHON" placeholder="Nama pemohon" value="<?php echo $NAMA_PEMOHON; ?>" />' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                '<div class="col-md-10">' +
                '<input type="text" class="form-control" required name="ALAMAT_PEMOHON" id="ALAMAT_PEMOHON" placeholder="Alamat" value="<?php echo $ALAMAT_PEMOHON; ?>" />' +
                '</div>' +
                '</div></div></div></div>');

        }

        // duabelas
      

        // tigabelas
        function deskripsi(nop) {
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/deskripsialert' ?>",
                method: "POST",
                data: "nop=" + nop,
                cache: false,
                success: function(response) {
                    alert(response);
                    $("#bayar").html('');
                }
            });
        }

        // empat belas
        $('#KD_JNS_OPC,#NO_URUTC,#KD_BLOKC,#KD_KELURAHANC,#KD_KECAMATANC,#KD_DATI2C,#KD_PROPINSIC').on('change', function() {
            var a = $('#KD_PROPINSIC').val();
            var b = $('#KD_DATI2C').val();
            var c = $('#KD_KECAMATANC').val();
            var d = $('#KD_KELURAHANC').val();
            var e = $('#KD_BLOKC').val();
            var f = $('#NO_URUTC').val();
            var g = $('#KD_JNS_OPC').val();
            var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
            // alert(nop);
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/getNopsknjop' ?>",
                method: "POST",
                data: "nop=" + nop,
                cache: false,
                success: function(response) {

                    var datashow = JSON.parse(response);
                    $("input#NAMA_PEMOHONC").val(datashow[0].NAMA_PEMOHON);
                    $("input#ALAMAT_PEMOHONC").val(datashow[0].ALAMAT_PEMOHON);
                }
            });
        });

        // lima belas
        $('#NO_URUT_PELAYANAN').on('change', function() {

            var a = $('#THN_PELAYANAN').val();
            var b = $('#BUNDEL_PELAYANAN').val();
            var c = $('#NO_URUT_PELAYANAN').val();
            var no = a + "." + b + "." + c;
            // alert(no);

            $.ajax({
                url: "<?php echo base_url() . 'permohonan/ceknomerlayanan' ?>",
                method: "POST",
                data: "no=" + no,
                cache: false,
                success: function(hasil) {
                    var asd = hasil.split("|");
                    if (asd[0] == 1) {
                        alert("Nomer layanan " + asd[1] + " Sudah terdaftar !");
                        $('#THN_PELAYANAN').val(null);
                        $('#BUNDEL_PELAYANAN').val(null);
                        $('#NO_URUT_PELAYANAN').val(null);
                    }
                }
            });
        });

        // enam belas
        $('#barcode').focus();
        $('body').on('keypress', 'input, select, textarea', function(e) {
            var self = $(this),
                form = self.parents('form:eq(0)'),
                focusable, next;
            if (e.keyCode == 13) {
                focusable = form.find('input,select,button,textarea').filter(':visible');
                next = focusable.eq(focusable.index(this) + 1);
                if (next.length) {
                    next.focus();
                } else {
                    form.submit();
                }
                return false;

            }
        });

        // tujuh belas
        $(".form-control").keyup(function() {
            if (this.value.length == this.maxLength) {
                var nextIndex = $('input:text').index(this) + 1;
                $('input:text')[nextIndex].focus();
            }
        });

        // delapan belas
        //cek lampiran
        $("#KD_JNS_PELAYANAN").change(function(e) {
            e.preventDefault();
            var KD_JNS_PELAYANAN = $('#KD_JNS_PELAYANAN').val();

            if (KD_JNS_PELAYANAN == '01') {
                $('#myBtn').prop('disabled', false);
                $('#tambahdiag').prop('disabled', false);
            } else {
                $('#myBtn').prop('disabled', true);
                $('#tambahdiag').prop('disabled', true);
            }
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/cekmapiinglampiran' ?>",
                method: "POST",
                data: "KD_JNS_PELAYANAN=" + KD_JNS_PELAYANAN,
                cache: false,
                success: function(hasil) {
                    $("#lampiran").html(hasil);
                }
            });
            if (KD_JNS_PELAYANAN == 01) {
                document.getElementById("jml_berkas").innerHTML = '' +
                    '<label class="col-md-2 control-label">Jumlah Berkas</label>' +
                    '<div class="col-md-10">' +
                    '<input type="text" class="form-control" required name="JUMLAH_BERKAS" />' +
                    '</div>';
            } else {
                document.getElementById("jml_berkas").innerHTML = '';
            };
        });

        // sembilan belas
        // cek bayar
        $("#KD_JNS_PELAYANAN,#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON").on('keyup change', function() {
            var kd_layanan = $('#KD_JNS_PELAYANAN').val();
            var a = $('#KD_PROPINSI_PEMOHON').val();
            var b = $('#KD_DATI2_PEMOHON').val();
            var c = $('#KD_KECAMATAN_PEMOHON').val();
            var d = $('#KD_KELURAHAN_PEMOHON').val();
            var e = $('#KD_BLOK_PEMOHON').val();
            var f = $('#NO_URUT_PEMOHON').val();
            var g = $('#KD_JNS_OP_PEMOHON').val();
            var nop = a + "." + b + "." + c + "." + d + "." + e + "." + f + "." + g;
            // if(kd_layanan=='02'){
            $.ajax({
                url: "<?php echo base_url() . 'permohonan/cekriwayatbayar' ?>",
                method: "POST",
                data: "var=" + kd_layanan + '_' + nop,
                cache: false,
                success: function(hasil) {
                    $("#bayar").html(hasil);

                }
            });
        });

        // dua puluh
        $(".input_tgl").keyup(function() {
            if (this.value.length == this.maxLength) {
                $(this).next('.inputs').focus();
            }
        });

        // dua puluh satu
        // barcodea
        $("#barcode").keyup(function() {
            var barcode = $("#barcode").val();
            getnopdecode(barcode);
        });

        $("#barcode").keydown(function() {
            var barcode = $("#barcode").val();
            getnopdecode(barcode);
        });
    });

    function stopRKey(evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type == "text")) {
            return false;
        }
    }
    document.onkeypress = stopRKey;
</script>
@endsection