@extends('page.master')
@section('judul')
<h1>
    Detail Lampiran
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Data Permohonan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong>Nomor Pelayanan</strong>
                <p class="text-muted">
                    <?php echo $berkas->THN_PELAYANAN . '.' . $berkas->BUNDEL_PELAYANAN . '.' .
                        $berkas->NO_URUT_PELAYANAN; ?>
                </p>
                <strong>Jenis Layanan</strong>
                <p class="text-muted"><?= $permohonan->NM_JENIS_PELAYANAN ?></p>
                <strong>Pemohon</strong>
                <p class="text-muted">
                    <?= $berkas->NAMA_PEMOHON ?>
                </p>
                <strong>Alamat</strong>
                <p class="text-muted"><?= $berkas->ALAMAT_PEMOHON ?></p>
                <strong>Status</strong>
                <p class="text-muted">
                    <?= $berkas->KOLEKTIF ?>
                </p>
                <strong>Tanggal</strong>
                <p class="text-muted"><?= $berkas->TGL_MASUK ?></p>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <form method="post" action="<?php echo base_url() . 'permohonan/verifikasi_persyaratan'; ?>">
            <input type="hidden" name="THN_PELAYANAN" value="{{ $berkas->THN_PELAYANAN }}">
            <input type="hidden" name="BUNDEL_PELAYANAN" value="{{ $berkas->BUNDEL_PELAYANAN }}">
            <input type="hidden" name="NO_URUT_PELAYANAN" value="{{ $berkas->NO_URUT_PELAYANAN }}">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Verifikasi persyaratan berkas</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php
                                $count_bullets = count($persyaratan);
                                $col_items = ceil($count_bullets / 2);
                                $post_counter = 0;
                                foreach ($persyaratan as $rb) {
                                    $post_counter++;
                                ?>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="searchType" name="KODE[]" value="<?= $rb->KODE ?>">
                                            <?= $rb->NAMA_BERKAS ?>
                                        </label>
                                    </div>

                                    <?php
                                    if (0 == $post_counter % $col_items) {
                                        echo '</div></div><div class="col-md-6"><div class="form-group">';
                                    }
                                    ?>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Tujuan</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PELIMPAHAN" id="PELIMPAHAN0" value="0">
                                        Di kembalikan / Tidak lengkap
                                    </label>
                                </div>
                             
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PELIMPAHAN" id="PELIMPAHAN1" value="1">
                                        <!-- PEDANIL -->
                                        Penelitian Lapangan
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PELIMPAHAN" id="PELIMPAHAN2" value="2">
                                        Penelitian  Kantor
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Nomor Pelayanan</label>
                                <input type="text" value="<?= $berkas->THN_PELAYANAN . '.' . $berkas->BUNDEL_PELAYANAN . '.' .
                                $berkas->NO_URUT_PELAYANAN ?>" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="">Kode Berkas</label>
                                <input type="text" placeholder="Kode Berkas" name="NO_PERMOHONAN" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                    <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Proses</button>
                </div>
                </div>
            </div>
            
        
        </form>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $('.searchType').click(function() {
        var isi = $(this).attr('id');
        if (this.checked) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'permohonan/update_berkas1'; ?>",
                data: {
                    id: isi
                }, //--> send id of checked checkbox on other page
                success: function(data) {
                    // alert(data);
                    //$('#container').html(data);
                },
                /*error: function() {
                    alert('it broke');
                },
                complete: function() {
                    alert('it completed');
                }*/
            });
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'permohonan/update_berkas0'; ?>",
                data: {
                    id: isi
                }, //--> send id of checked checkbox on other page
                success: function(data) {
                    // alert(data);
                    //$('#container').html(data);
                },
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).delegate('.btn-warning', 'click', function() {
            var id = $(this).attr('id');
            var data = 'recordToDelete=' + id;
            var parent = $(this).parent().parent();
            //alert(id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('permohonan/proses_penomoran'); ?>",
                data: {
                    id: id
                },
                cache: false,
                success: function(data) {
                    //$('#'+id).hide();
                    //alert(data);
                    setTimeout(function() { // wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 1);
                    //alert(data);//parent.fadeOut('slow', function() {$(this).remove();});
                }
            });
        });
        $('table#userTabla tr:odd').css('background', ' #FFFFFF');
    });
</script>
@endsection