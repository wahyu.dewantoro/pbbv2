<html>

<head>
    <style>
        body {
            font-family: Courier New, Courier, monospace;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* page-break-after works, as well */
        }

        p {
  margin: 5px;
}

        @page {
            margin-top: 10mm;
            margin-bottom: 10mm;
            margin-left: 10mm;
            margin-right: 10mm;
        }

        .tengah {
            text-align: center;
        }

        .kanankiri {
            text-align: justify;
        }

        th,
        td {
            vertical-align: top;
        }
    </style>
</head>

<body>
    <?php
    $jum = count( (array) $rk);
    $i = 1;
    foreach ($rk as $rk) { ?>
        <table width="100%">
            <tbody>
                <tr>
                    <td rowspan="3"><img width="60px" src="<?= base_url('malang.png') ?>"></td>
                    <td align="center">PEMERINTAH KABUPATEN MALANG</td>
                </tr>
                <tr>
                    <td align="center"><b>BADAN PENDAPATAN DAERAH</b></td>
                </tr>
                <tr>
                    <td align="center">JL. K.H. AGUS SALIM 7 TELEPON (0341)367994 FAX (0341)355708<br> MALANG - 65119</td>
                </tr>
            </tbody>
        </table>
        <hr style="margin: 5px;">
        <p class="tengah"><b>SURAT KETERANGAN NJOP</b>
            <br>
            Nomor : <?= $rk->NOMOR_SK?></p>
        <p>Yang bertanda tangan di bawah ini :</p>
        <table>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><?= $rk->NAMA_PEGAWAI ?></td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td><?= $rk->JABATAN ?></td>
            </tr>
        </table>
        <p class="kanankiri">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan ketentuan Pasal 79 ayat (1) Undang - Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah dan Pasal 90 Ayat (1) Peraturan Daerah Kab. Malang Nomor 8 Tahun 2010 tentang Pajak Daerah, Sebagaimana telah diubah dengan peraturan daerah Kabupaten malang nomor 1 tahun 2019 tentang perubahan atas Peraturan Daerah nomor 8 tahun 2010 tentang Pajak Daerah Dengan ini menerangkan bahwa sesuai dengan basis data Badan Pendapatan Daerah atas objek pajak:</p>
        <table>
            <tr valign="top">
                <td>Nomor Objek Pajak</td>
                <td>:</td>
                <td><?= $rk->NOP ?></td>
            </tr>
            <tr valign="top">
                <td>Letak Objek Pajak</td>
                <td>:</td>
                <td><?= $rk->JALAN_OP  ?> <?php if (!empty($rk->BLOK_KAV_NO_OP) && $rk->BLOK_KAV_NO_OP != '-') {
                                                                                                echo $rk->BLOK_KAV_NO_OP;
                                                                                              }  ?></td>
            </tr>
        </table>
        <p>Diperoleh data sebagai berikut :</p>
        <table width=100%>
            <tr>
                <td>Luas Bumi</td>
                <td>:</td>
                <td align="right"> <?= $rk->LUAS_BUMI_SPPT ?> M<sup>2</sup></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td align="right"> <?= $rk->LUAS_BNG_SPPT ?> M<sup>2</sup></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>NJOP Bumi</td>
                <td>:</td>
                <td align="right">  <?= number_format(($rk->LUAS_BUMI_SPPT), '0', '', '.')  ?> M<sup>2</sup></td>
                <td align="center">X</td>
                <td>Rp. <?= number_format(($rk->NJOP_BUMI_SPPT) / ($rk->LUAS_BUMI_SPPT), '0', '', '.')  ?></td>
                <td>=</td>
                <td>Rp. <?= number_format(($rk->NJOP_BUMI_SPPT) , '0', '', '.')  ?></td>
            </tr>
            <tr>
                <td>NJOP Bangunan</td>
                <td>:</td>
                <td align="right">  <?= number_format(($rk->LUAS_BNG_SPPT), '0', '', '.')  ?> M<sup>2</sup></td>
                <td align="center">X</td>
                <td>Rp. <?php if($rk->NJOP_BNG_SPPT>0 ){echo number_format(($rk->NJOP_BNG_SPPT) / ($rk->LUAS_BNG_SPPT), '0', '', '.');}else{echo 0;}  ?></td>
                <td>=</td>
                <td>Rp. <?= number_format(($rk->NJOP_BNG_SPPT) , '0', '', '.')  ?></td>
            </tr>
            <tr>
                <td>NJOP Bangunan Bersama</td>
                <td>:</td>
                <td align="right">0</td>
                <td align="center">X</td>
                <td>Rp. 0</td>
                <td>=</td>
                <td>Rp. 0
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="6">Nilai Jual Objek Pajak Keseluruhan</td>
                <td>Rp. <?= number_format($rk->NJOP_BNG_SPPT + $rk->NJOP_BUMI_SPPT, '0', '', '.') ?></td>
            </tr>
            <tr>
                <td colspan="7"><b><?= terbilang($rk->NJOP_BNG_SPPT + $rk->NJOP_BUMI_SPPT) ?> Rupiah</b></td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <td>Nama Wajib Pajak</td>
                <td>:</td>
                <td><?= $rk->NM_WP_SPPT ?></td>
            </tr>
            <tr>
                <td>Alamat Wajib Pajak</td>
                <td>:</td>
                <td><?= $rk->JLN_WP_SPPT ?> <?= $rk->BLOK_KAV_NO_WP_SPPT ?> RT <?= $rk->RT_WP_SPPT ?> RW <?= $rk->RW_WP_SPPT ?> <?= $rk->KELURAHAN_WP_SPPT ?> <?= $rk->KOTA_WP_SPPT ?> </td>
            </tr>
            <tr>
                <td>NPWPD</td>
                <td>:</td>
                <td><?= $rk->NPWP_SPPT ?></td>
            </tr>
        </table>
        <p class="kanankiri">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Demikian surat keterangan NJOP ini dibuat untuk dapat dipergunakan seperlunya. apabila di kemudian hari terdapat kekeliruan akan dibetulkan dan ditindaklanjuti sesuai dengan ketentuan yang berlaku.
        </p>
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="45%" align="center">
                    Diterbitkan di MALANG<br>
                    pada tanggal <?= $rk->TANGGAL_SK?><br>
                    A.N KEPALA BADAN PENDAPATAN DAERAH<br>
                    Kepala Bidang PBB P2<br><br><br><br><br><br>
                    <b><?= $rk->NAMA_PEGAWAI ?></b> <br>
                    NIP <?= $rk->NIP ?>

                </td>
            </tr>
        </table>
        <p><i> <b>NJOP Hanya untuk kepentingan PBB P2</b></i></p>
        <?php if ($jum != $i) { ?>
            <div class="pagebreak"></div>
        <?php } ?>
    <?php
        $i++;
    } ?>
</body>

</html>