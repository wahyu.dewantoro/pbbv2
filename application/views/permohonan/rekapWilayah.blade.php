@extends('page.master')
@section('judul')
<h1>
    Ajuan Wilyah
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Laporan</a></li>
    <li class="active">Ajuan wilayah</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body no-padding">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'permohonan/rekapwilayah' ?>" autocomplete="off">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control tanggal" name="p_tanggal1" required value="<?php echo $p_tanggal1 ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control tanggal" name="p_tanggal2" required value="<?php echo $p_tanggal2 ?>">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                <a class="btn btn-sm btn-primary" href="<?= base_url() . 'permohonan/rekapwilayahexcel?t1=' . urlencode($p_tanggal1) . '&t2=' . urlencode($p_tanggal2) ?>"><i class="fa fa-file"></i> Excell</a>
            </div>
        </form>
        <hr>
        <table class="table table-bordered table-hover" id="example2">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>No Layanan</th>
                    <th>Nop</th>
                    <th>Alamat OP</th>
                    <th>Pemohon</th>
                    <th>Layanan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url() ?>permohonan/jsonRekapWilayah",
                "type": "POST",
                "data": {
                    "p_tanggal1": "<?= $p_tanggal1 ?>",
                    "p_tanggal2": "<?= $p_tanggal2 ?>"
                }
            },
            columns: [{
                    "data": "NO_LAYANAN",
                    "className": "text-center",
                    "orderable": false
                },
                {
                    "data": "NO_LAYANAN"
                },
                {
                    "data": "NO_LAYANAN",
                    render: function(data, type, row) {
                        return row.KD_PROPINSI_PEMOHON + '.' +
                            row.KD_DATI2_PEMOHON + '.' +
                            row.KD_KECAMATAN_PEMOHON + '.' +
                            row.KD_KELURAHAN_PEMOHON + '.' +
                            row.KD_BLOK_PEMOHON + '.' +
                            row.NO_URUT_PEMOHON + '.' +
                            row.KD_JNS_OP_PEMOHON;
                    }
                },
                {
                    "data": "NO_LAYANAN",
                    render: function(data, type, row) {
                        return 'Desa/Kel. ' + row.NM_KELURAHAN + ' , Kec. ' + row.NM_KECAMATAN;
                    }
                },
                {
                    "data": "WP"
                },
                {
                    "data": "NM_PENGAJUAN",
                    
                },
            ],
            order: [],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
@endsection