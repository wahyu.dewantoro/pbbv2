

@extends('page.master')
@section('judul')
<h1>
    Data Permohonan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
    <li><a href="<?= base_url().'permohonan'?>">Data Permohonan</a> </li>
    <li class="active"><?php echo $rk->NO_LAYANAN ?></li>
</ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="box-title">

            </div>
            <div class="box-tools">
                <?php echo anchor('permohonan/tambahNop/' . $id, '<i class="fa fa-plus"></i> Tambah NOP', 'class="btn btn-sm btn-info"'); ?>
                <?php echo anchor('permohonan/cetak/' . $id, '<i class="fa fa-print"></i> Cetak tanda terima', 'class="btn btn-success btn-sm" target=""'); ?>
                <?php echo anchor('permohonan', '<i class="fa fa-list"></i> List', 'class="btn btn-primary btn-sm"'); ?>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <table class="table">
                        <tr>
                            <td width="40%">No Layanan</td>
                            <td width="1%">:</td>
                            <td><?php echo $rk->NO_LAYANAN ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Pelayanan</td>
                            <td>:</td>
                            <td><?php echo date('d/m/Y', strtotime($rk->TGL_TERIMA_DOKUMEN_WP)) ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai (<small>Perkiraan</small>)</td>
                            <td>:</td>
                            <td><?php echo date('d/m/Y', strtotime($rk->TGL_PERKIRAAN_SELESAI)) ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Pelayanan </td>
                            <td>:</td>
                            <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo $rg[0]['LAYANAN'] . ' / Individual';
                                } else {
                                    echo " / Kolektif";
                                }   ?></td>
                        </tr>
                        <tr>
                            <td>N.O.P</td>
                            <td>:</td>
                            <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo $rg[0]['NOP'];
                                } else {
                                    echo "-";
                                }   ?></td>
                        </tr>
                        <tr>
                            <td>Nama Pemohon</td>
                            <td>:</td>
                            <td><?php echo $rk->NAMA_PEMOHON ?></td>
                        </tr>
                        <tr>
                            <td>Alamat Pemohon</td>
                            <td>:</td>
                            <td><?php echo $rk->ALAMAT_PEMOHON ?></td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-4">
                    <table class="table">
                        <tr>
                            <td width="40%">Telp</td>
                            <td width="1%">:</td>
                            <td><?php echo $rk->IP_HP ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><?php echo $rk->IP_EMAIL ?></td>
                        </tr>
                        <tr>
                            <td>Letak Objek Pajak</td>
                            <td>:</td>
                            <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo $rg[0]['JALAN_OP'];
                                } else {
                                    echo "-";
                                }   ?></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>:</td>
                            <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo $rg[0]['NM_KELURAHAN'];
                                } else {
                                    echo "-";
                                }   ?></td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>:</td>
                            <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo $rg[0]['NM_KECAMATAN'];
                                } else {
                                    echo "-";
                                }   ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td><?php echo $rk->CATATAN_PST ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <strong>Dokumen Terlampir :</strong>
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li>Pengajuan Permohonan <?php if (!empty($dok)) {
                                                                if ($dok->L_PERMOHONAN == '1') {
                                                                    echo "(<i class='fa fa-check'></i>)";
                                                                }
                                                            } ?></li>
                                <li>Surat Kuasa <?php if (!empty($dok)) {
                                                    if ($dok->L_SURAT_KUASA == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                                <li>FC KTP/SIM dan KK <?php if (!empty($dok)) {
                                                            if ($dok->L_KTP_WP == '1') {
                                                                echo "(<i class='fa fa-check'></i>)";
                                                            }
                                                        } ?></li>
                                <li>FC Sertifikat / Kepemilikan <?php if (!empty($dok)) {
                                                                    if ($dok->L_SERTIFIKAT_TANAH == '1') {
                                                                        echo "(<i class='fa fa-check'></i>)";
                                                                    }
                                                                } ?></li>
                                <li>Asli SPPT <?php if (!empty($dok)) {
                                                    if ($dok->L_SPPT == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                                <li>FC IMB <?php if (!empty($dok)) {
                                                if ($dok->L_IMB == '1') {
                                                    echo "(<i class='fa fa-check'></i>)";
                                                }
                                            } ?></li>
                                <li>FC akte jual beli/hibah <?php if (!empty($dok)) {
                                                                if ($dok->L_AKTE_JUAL_BELI == '1') {
                                                                    echo "(<i class='fa fa-check'></i>)";
                                                                }
                                                            } ?></li>
                                <li>FC SK Pensiun / Veteran <?php if (!empty($dok)) {
                                                                if ($dok->L_SK_PENSIUN == '1') {
                                                                    echo "(<i class='fa fa-check'></i>)";
                                                                }
                                                            } ?></li>
                                <li>FC SPPT / SSPD / Bukti lunas<?php if (!empty($dok)) {
                                                                    if ($dok->L_SPPT_STTS == '1') {
                                                                        echo "(<i class='fa fa-check'></i>)";
                                                                    }
                                                                } ?></li>
                                <li>Asli BBPD / Bukti Lunas<?php if (!empty($dok)) {
                                                                if ($dok->L_STTS == '1') {
                                                                    echo "(<i class='fa fa-check'></i>)";
                                                                }
                                                            } ?></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>FC SK Pengurangan<?php if (!empty($dok)) {
                                                            if ($dok->L_SK_PENGURANGAN == '1') {
                                                                echo "(<i class='fa fa-check'></i>)";
                                                            }
                                                        } ?></li>
                                <li>FC SK Keberatan<?php if (!empty($dok)) {
                                                        if ($dok->L_SK_KEBERATAN == '1') {
                                                            echo "(<i class='fa fa-check'></i>)";
                                                        }
                                                    } ?></li>
                                <li>FC SSPD BPHTB<?php if (!empty($dok)) {
                                                        if ($dok->L_SKKP_PBB == '1') {
                                                            echo "(<i class='fa fa-check'></i>)";
                                                        }
                                                    } ?></li>
                                <li>SKTM <?php if (!empty($dok)) {
                                                if ($dok->L_SPMKP_PBB == '1') {
                                                    echo "(<i class='fa fa-check'></i>)";
                                                }
                                            } ?></li>
                                <li>Sket tanah<?php if (!empty($dok)) {
                                                    if ($dok->L_SKET_TANAH == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                                <li>Sket Lurah <?php if (!empty($dok)) {
                                                    if ($dok->L_SKET_LURAH == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                                <li>NPWP/NPWPD<?php if (!empty($dok)) {
                                                    if ($dok->L_NPWPD == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                                <li>Rincian Penghasil <?php if (!empty($dok)) {
                                                            if ($dok->L_PENGHASILAN == '1') {
                                                                echo "(<i class='fa fa-check'></i>)";
                                                            }
                                                        } ?></li>
                                <li>SK Cagar budaya <?php if (!empty($dok)) {
                                                        if ($dok->L_CAGAR == '1') {
                                                            echo "(<i class='fa fa-check'></i>)";
                                                        }
                                                    } ?></li>
                                <li>Lain - lain <?php if (!empty($dok)) {
                                                    if ($dok->L_LAIN_LAIN == '1') {
                                                        echo "(<i class='fa fa-check'></i>)";
                                                    }
                                                } ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>DATA FILE UPLOAD</b>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>File</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $urut = 0;
                            $no = 1;
                            foreach ($file as $rgs) { ?>
                                <tr>
                                    <td align="center"><?php echo $no ?></td>
                                    <td><?php echo '<strong><a href="' . base_url('permohonan/cekfile') . '?file=' .urlencode( $rgs->NAMA_FILE ). '" target="_blank">' . $rgs->NAMA_FILE . '</a></strong> '; ?></td>

                                </tr>
                            <?php $no++;
                                $urut++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <?php if ($rk->STATUS_KOLEKTIF == '1') { ?>
                <br>
                <P><b>Lampiran tanda terima pelayanan kolektif</b></P>
                <table id="example3" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NO Layanan</th>
                            <th>NOP</th>
                            <th>Nama WP</th>
                            <th>Tahun</th>
                            <th>Jenis Pelayanan</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $urut = 0;
                        $no = 1;
                        foreach ($rg as $rg) { ?>
                            <tr>
                                <td align="center"><?php echo $no ?></td>
                                <td><?php echo $rg['NO_LAYANAN'] . '.' . sprintf('%03s', $urut) ?></td>
                                <td><?php echo $rg['NOP'] ?></td>
                                <td><?php echo $rg['NM_WP'] ?></td>
                                <td><?php echo $rg['THN_PELAYANAN'] ?></td>
                                <td><?php echo $rg['LAYANAN'] ?></td>
                                <td><?= $rg['STATUS_PELAYANAN'] ?></td>
                                <td align="center"><a href="<?= base_url() . 'permohonan/hapusDetail?nopel=' . urlencode($rg['NO_LAYANAN']) . '&nop=' . urlencode(str_replace('-', '.', $rg['NOP'])) ?>" onclick="return confirm('Apakah anda yakin?')"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php $no++;
                            $urut++;
                        } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
@endsection