@extends('page.master')
@section('judul')
<h1>
    Data Permohonan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
    <li class="active">Data Permohonan</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class="form-inline" method="post" action="<?php echo base_url() . 'permohonan' ?>">
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="p_tanggal1" required value="<?php echo $p_tanggal1 ?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control tanggal" name="p_tanggal2" required value="<?php echo $p_tanggal2 ?>">
            </div>
            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
            <a href="{{ base_url().'permohonan/cetakPermohonan' }}?awal={{ $p_tanggal1 }}&akhir={{ $p_tanggal2 }}" target="_blank" class="btn btn-info">Cetak</a>
        </form>
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>No Layanan</th>
                    <th>Pemohon</th>
                    <th>Status</th>
                    <th>Terima</th>
                    <th>Selesai (Perkiraan)</th>
                    <th>Entry By</th>
                    <th width="10%">Aksi</th>
                </tr>
            </thead>

        </table>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url() ?>permohonan/json",
                "type": "POST",
                "data":{'p_tanggal1':'<?= $p_tanggal1 ?>','p_tanggal2':'<?= $p_tanggal2 ?>' }
            },
            columns: [{
                    "data": "NO_LAYANAN",
                    "orderable": false,
                    "className": "text-center"
                }, {
                    "data": "NO_LAYANAN",
                    render: function(data, type, row) {
                        if (row.IS_ONLINE=='1') {
                            return data+'<br><badge>Online</badge>';
                        } else {
                            return data;
                        }
                    }
                }, {
                    "data": "NAMA_PEMOHON"
                }, {
                    "data": "STATUS_KOLEKTIF"
                }, {
                    "data": "TGL_TERIMA"
                }, {
                    "data": "TGL_SELESAI"
                },{
                    "data": "NAMA_PENERIMA"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
@endsection