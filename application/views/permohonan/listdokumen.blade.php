<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')
<h1>
    Dokumen
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
    <li class="active">Dokumen</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Dokumen Masuk</h3>
                <div class="box-tools">
                    <div class="mailbox-controls">


                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                            <button type="button" class="btn btn-primary btn-sm" id="proses"><i class="fa fa-share"></i></button>

                        </div>
                    </div>
                </div>

                <!-- /.box-tools -->
            </div>
            <div class="box-body">
                <form class="form-inline" method="post" action="<?php echo base_url() . 'permohonan/listdokumen' ?>">
                    <div class="form-group">
                        <label for="exampleInputName2"><b>Tahun Pajak</b></label>
                    </div>
                    <div class="form-group">
                        <select id="tahun" class="form-control" name="tahun">
                            <?php for ($q = date('Y'); $q > 2012; $q--) { ?>
                                <option <?php if ($q == $tahun) {
                                            echo "selected";
                                        } ?> value="<?= $q ?>"><?= $q ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
                    </div>
                    <input type="hidden" name="cari" value="1">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                </form>

                <div class="mailbox-messages">

                    <?php
                    if (isset($rk)) {

                    ?>
                        <table class="table  table-striped" id="example2">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th>Pengirim</th>
                                    <th>Layanan</th>
                                    <th>Tahun</th>
                                    <th>NO Layanan</th>
                                    <th>NOP</th>
                                    <th>Nama WP</th>
                                    <th>Kelurahan OP</th>
                                    <th>Tanggal Masuk</th>
                                </tr>
                            </thead>
                        </table>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url() . "permohonan/validasiCheckAll" ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Pelimpahan Dokumen</h4>
                </div>
                <div class="modal-body">

                    <input type='hidden' name='KD_KANWIL' value='01'>
                    <input type='hidden' name='KD_KANTOR' value='01'>

                    <input type="hidden" name="berkas_id" id="berkas_id">
                    <input type='hidden' name='NIP' value='<?php echo $CI->session->userdata('nip') ?>'>
                    <input type='hidden' name='TANGGAL_AWAL' value='<?php echo date('Y-m-d H:i:s'); ?>'>
                    <input type='hidden' name='TANGGAL_AKHIR' value='<?php echo date('Y-m-d H:i:s'); ?>'>
                    <div class="form-group">
                        <label>Unit Tujuan</label>
                        <select class="form-control" required name="KODE_GROUP">
                            <option value="">Pilih</option>
                            <?php foreach ($unit as $unit) { ?>
                                <option value="<?php echo $unit->KODE_GROUP ?>"><?php echo $unit->NAMA_GROUP ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" required name="STATUS_BERKAS">
                            <!-- <option value="">Pilih</option> -->
                            <?php $hdd = array('Proses', 'Disetujui', 'Ditolak', 'Selesai', 'Diambil');
                            foreach ($hdd as $hdd) { ?>
                                <option value="<?php echo $hdd ?>"><?php echo $hdd ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea name="KETERANGAN" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="Submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });


        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            /*   'oLanguage': {
                  "sProcessing": "Sedang memproses...",
                  "sLengthMenu": "",
                  "sZeroRecords": "Tidak ditemukan data yang sesuai",
                  "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                  "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                  "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                  "sInfoPostFix": "",
                  "sSearch": "Cari:",
                  "sUrl": "",
                  "oPaginate": {
                      "sFirst": "Pertama",
                      "sPrevious": "Sebelumnya",
                      "sNext": "Selanjutnya",
                      "sLast": "Terakhir"
                  }
              }, */
            processing: true,
            serverSide: false,
            // ordering: false,

            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ordering: false,
            ajax: {
                "url": "<?php echo base_url() ?>permohonan/jsonmonitoring",
                "type": "POST",
                "data": {
                    "where": "<?= $where ?>"
                }
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    "className": "text-center",
                    render: function(data, type, row) {
                        var check = '<div class="btn btn-group">';
                        check += '<input type="checkbox" class="chek_all" name="itemsPilih[]" value="' + data + '">';
                        check += '</div>';
                        return check;
                    }
                },
                {
                    "data": "LAST_DIVISI",
                    render: function(data, type, row) {
                        var base = '<?php echo base_url() ?>';

                        return "<a href='" + base + "permohonan/detaildua/" + row.NO_LAYANAN + "'><b>" + data + "</b></a>";
                    }
                },
                {
                    "data": "LAYANAN",
                    render: function(data, type, row) {
                        return '<b>' + data + '</b>';
                    }
                },
                {
                    "data": "TAHUN"
                },
                {
                    "data": "NO_LAYANAN"
                },
                {
                    "data": "NOP"
                },
                {
                    "data": "NAMA_PEMOHON"
                },
                {
                    "data": "NM_KELURAHAN"
                },
                {
                    "data": "TGL_MASUK"
                },

            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {

            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        /* $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        }); */

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function() {
            var clicks = $(this).data('clicks');
            if (clicks) {
                // alert('a');
                //Uncheck all checkboxes
                // $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');

                // $('#myCheckbox').prop('checked', true); // Checks it
                $(".mailbox-messages input[type='checkbox']").prop('checked', false); // Unchecks i
            } else {
                // alert('b');
                //Check all checkboxes
                // $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".mailbox-messages input[type='checkbox']").prop('checked', true); // Unchecks i
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        /* $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        }); */

        $("#proses").click(function() {
            var data = $('.chek_all:checked').serialize();
            var berkas_id = $(".chek_all:checked").map(function() {
                return $(this).val();
            }).get();

            $('#berkas_id').val(berkas_id);
            console.log(berkas_id);
            if (berkas_id != '') {
                $('#myModal').modal('show');
            } else {
                alert('belum ada item yang di pilih.');
            }



        });



    });
</script>
<script type="text/javascript">
    function checkLengthnopel(el) {
        b = el.value.replace(/[^\d]/g, "");

        if (b.length != 11) {
            // alert("Panjang nik 16 digit.")
            $("#VAL_NOPEL").html("<span style='color:red'>Panjang NO Layanan harus  11 angka.</span>");
        } else {
            $("#VAL_NOPEL").html("");
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function formatnopel(objek) {

        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 4) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 4 && panjang <= 8) {
            // 07. -> 2,2
            c = b.substr(0, 4) + '.' + b.substr(4, 4);
        } else if (panjang > 8 && panjang <= 11) {
            // 123 -> 4,3
            c = b.substr(0, 4) + '.' + b.substr(4, 4) + '.' + b.substr(8, 3);
        } else {
            // .0
            c = b.substr(0, 4) + '.' + b.substr(4, 4) + '.' + b.substr(8, 3);
            // alert(panjang);
        }
        //alert(panjang);
        objek.value = c;
    }
</script>

@endsection