 <!DOCTYPE html>
 <html>

 <head>
     <title>Tanda terima</title>
     <style type="text/css">
         body {
             font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
             font-size: 10px;
             line-height: 1.42857143;
             color: #333;
             background-color: #fff;
         }

         table {
             border: 1px solid #C6C6C6;
         }

         td {
             border-right: 1px solid #C6C6C6;
         }

         th {
             border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;
         }
     </style>
 </head>
 <body>
     <h3>
         LAMPIRAN TANDA TERIMA PELAYANAN KOLEKTIF</h3>
     <table width="100%" style="border:0px">
         <tr>
             <td style="border-right:0px">
                 <table width="100%" cellpadding="3" cellspacing="0" style="font-size:12px">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>NOP</th>
                             <th>Nama WP</th>
                             <th>Tahun</th>
                             <th>Jenis Pelayanan</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $no = 1;
                            foreach ($rg as $rg) { ?>
                             <tr>
                                 <td align="center"><?php echo $no ?></td>
                                 <td><?php echo $rg['NOP'] . ' (' . $no . ')' ?></td>
                                 <td><?php echo $rg['NM_WP'] ?></td>
                                 <td align="center"><?php echo $rg['THN_PELAYANAN'] ?></td>
                                 <td><?php echo $rg['LAYANAN'] ?></td>
                             </tr>
                         <?php $no++;
                            } ?>
                     </tbody>
                 </table>
             </td>
         </tr>
     </table>
 </body>
 </html>