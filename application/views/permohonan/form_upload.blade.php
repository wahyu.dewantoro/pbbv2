@extends('page.master')
@section('judul')
<h1>
    Form Upload
</h1>
@endsection
@section('content')
<div class="row">
    <form class='form-horizontal' action="<?php echo base_url('view/detail_wp'); ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-8">

        </div>
    </form>
</div><br>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Form Upload</a></li>
    </ul>
    <div class="tab-content">
        <!-- /.tab-pane -->
        <div class="tab-pane active" id="tab_1">
            <!-- form tab 2 -->
            <div class="box box-success">
                <h4 class='box-title'> <i class="fa fa-globe"></i> Form Upload</h4>
                <div class="box-body">
                    <div id="idn">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table" style="font-size:13.4px" width='100%'>
                                    <tbody>
                                        <tr>
                                            <td width="20%">No Layanan</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $rk->NO_LAYANAN ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pelayanan</td>
                                            <td>:</td>
                                            <td><?php echo date('d/m/Y', strtotime($rk->TGL_TERIMA_DOKUMEN_WP)) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Selesai (<small>Perkiraan</small>)</td>
                                            <td>:</td>
                                            <td><?php echo date('d/m/Y', strtotime($rk->TGL_PERKIRAAN_SELESAI)) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pemohon</td>
                                            <td>:</td>
                                            <td><?php echo $rk->NAMA_PEMOHON ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Pemohon</td>
                                            <td>:</td>
                                            <td><?php echo $rk->ALAMAT_PEMOHON ?></td>
                                        </tr>
                                        <tr>
                                            <td>Keterangan</td>
                                            <td>:</td>
                                            <td><?php echo $rk->KETERANGAN_PST ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <b>DATA FILE UPLOAD</b>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>File</th>
                                            <th>ukuran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $urut = 0;
                                        $no = 1;
                                        foreach ($file as $rg) { 
                                            if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/" . $rg->NAMA_FILE)) {

                                            ?>
                                            <tr>
                                                <td align="center"><?php echo $no ?></td>
                                                <td><?php echo '<strong><a href="' . base_url('uploads') . '/' . $rg->NAMA_FILE . '" target="_blank">' . $rg->NAMA_FILE . '</a></strong> '; ?></td>
                                                <td><?php
                                                    $file = 'uploads/' . $rg->NAMA_FILE;
                                                    echo fsize($file);
                                                    ?>
                                                </td>
                                                <td><button id="<?php echo $rg->NAMA_FILE; ?>" class="btn btn-sm btn-success trash"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                        <?php $no++;
                                            $urut++;
                                        }  } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <?php $exp = explode('.', $id) ?>
                                </form>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">File</label>
                                    <div class="col-lg-10">
                                        <form action="<?php echo base_url('permohonan/uploadimg'); ?>" id="my-dropzone" class="dropzone">
                                            <input type='hidden' id='THN_PELAYANAN' name='THN_PELAYANAN' value='<?php echo $exp[0] ?>'>
                                            <input type='hidden' id="BUNDEL_PELAYANAN" name='BUNDEL_PELAYANAN' value='<?php echo $exp[1] ?>'>
                                            <input type='hidden' id="NO_URUT_PELAYANAN" name='NO_URUT_PELAYANAN' value='<?php echo $exp[2] ?>'>
                                        </form>
                                        <font color="red" style='font-size: 11px;'>*MAX 20 MB</font><br>
                                        <a class="btn btn-sm btn-success" onClick="window.location.reload()"> <i class="fa fa-save"></i>SIMPAN</a>
                                        <?php echo anchor('permohonan/upload_berkas', '<i class="fa fa-list"></i> Kembali', 'class="btn btn-sm btn-info"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- body -->
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>

<?php
function fsize($file)
{
    $a = array("B", "KB", "MB", "GB", "TB", "PB");
    $pos = 0;
    $size = filesize($file);
    while ($size >= 1024) {
        $size /= 1024;
        $pos++;
    }
    return round($size, 2) . " " . $a[$pos];
}
?>
@endsection
@section('css')
<link href="<?php echo base_url(); ?>dropzone/dist/dropzone.css" rel="stylesheet">
@endsection
@section('script')
<script src="<?php echo base_url(); ?>dropzone/dist/dropzone.js"></script>

<script type="text/javascript">
    Dropzone.options.myDropzone = {
        init: function() {
            this.on("addedfile", function(file) {

                // Create the remove button
                var removeButton = Dropzone.createElement("<button>Remove file</button>");


                // Capture the Dropzone instance as closure.
                var _this = this;
                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                    //alert(file.name);
                    var t = $('#THN_PELAYANAN').val();
                    var b = $('#BUNDEL_PELAYANAN').val();
                    var u = $('#NO_URUT_PELAYANAN').val();
                    var nm = file.name;
                    var parse = t + b + u + '_' + nm;
                    //alert(parse);
                    $.ajax({
                        type: 'POST',
                        data: 'namagambar=' + parse,
                        url: '<?php echo base_url('permohonan/hapusimg'); ?>'
                    });
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });
        }
    };
    $(function() {
        $('.trash').click(function() {
            var del_id = $(this).attr('id');
            var $ele = $(this).parent().parent();
            //alert(del_id);
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('permohonan/hapus_data_img'); ?>',
                data: {
                    del_id: del_id
                },
                success: function(data) {
                    setTimeout(function() { // wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 1);
                }

            })
        })
    });
</script>
@endsection