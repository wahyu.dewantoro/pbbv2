 <span class="badge">Riwayat Pembayran SPPT</span>
 <button class="bnn btn-success btn-xs" id="lanjut"><i class="fa fa-check"></i> Memenuhi persyaratan </button>
 <div class="table-responsive">
     <table style='font-size:12px' class='table table-hover table-bordered'>
         <thead>
             <tr>
                 <th align="center">Tahun</th>
                 <th align="center">PBB</th>
                 <th align="center">Tanggal Bayar</th>
             </tr>
         </thead>
         <tbody>
             <?php foreach ($rk as $rk) { ?>
                 <tr>
                     <td align="center"><?php echo  $rk->TAHUN ?></td>
                     <td align="right"><?php echo  number_format($rk->BAYAR, '0', '', '.') ?></td>
                     <td align="center"><?php echo $rk->TANGGAL ?></td>
                 </tr>
             <?php } ?>
         </tbody>
     </table>
 </div>
 <script type="text/javascript">
     $(document).ready(function() {
        // $('#myBtn').prop('disabled', false);
        $('#tambahdiag').prop('disabled', true);
         $("#lanjut").click(function(e) {
             e.preventDefault();
             $('#myBtn').prop('disabled', false);
             $('#tambahdiag').prop('disabled', false);
         });
     });
 </script>