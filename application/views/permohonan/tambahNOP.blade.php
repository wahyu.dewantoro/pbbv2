@extends('page.master')
@section('judul')
<h1>
    <?php echo $button ?>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
    <li><a href="<?= base_url() . 'permohonan' ?>">Data Permohonan</a> </li>
    <li class="active"><?php echo anchor('permohonan/detail/' . $id, $id ); ?></li>
</ol>
@endsection
@section('content')

<form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">No Pelayanan </label>
                        <div class="col-md-3">
                            <input type="text" readonly class="form-control inputs" maxlength="4" name="THN_PELAYANAN" id="THN_PELAYANAN" placeholder="Tahun" value="<?php echo $THN_PELAYANAN; ?>" required />
                        </div>
                        <div class="col-md-3">
                            <input type="text" readonly class="form-control inputs" maxlength="4" name="BUNDEL_PELAYANAN" id="BUNDEL_PELAYANAN" placeholder="Bundel" required value="<?php echo $BUNDEL_PELAYANAN; ?>" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" readonly class="form-control inputs" maxlength="3" name="NO_URUT_PELAYANAN" id="NO_URUT_PELAYANAN" placeholder="No urut" required value="<?php echo $NO_URUT_PELAYANAN; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">No Surat Permohonan </label>
                        <div class="col-md-8">
                            <input type="text" readonly class="form-control inputs" required name="NO_SRT_PERMOHONAN" id="NO_SRT_PERMOHONAN" placeholder="No surat permohonan" value="<?php echo $NO_SRT_PERMOHONAN; ?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tanggal Terima </label>
                        <div class="col-md-8">
                            <input type="text" readonly class="form-control " required name="TGL_TERIMA_DOKUMEN_WP" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tanggal Perkiraan Selesai </label>
                        <div class="col-md-8">
                            <input type="text" readonly class="form-control " required name="TGL_PERKIRAAN_SELESAI" id="TGL_PERKIRAAN_SELESAI" placeholder="TGL PERKIRAAN SELESAI" value="<?php echo $TGL_PERKIRAAN_SELESAI; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tanggal Surat Permohonan </label>
                        <div class="col-md-8">
                            <input type="text" readonly required class="form-control " name="TGL_SURAT_PERMOHONAN" id="TGL_SURAT_PERMOHONAN" placeholder="TGL SURAT PERMOHONAN" value="<?php echo $TGL_SURAT_PERMOHONAN; ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-md-2 control-label">NOP Kolektif</label>
                <div class="col-md-1">
                    <input type="text" maxLength="2" class="form-control" value="35" name="KD_PROPINSI_PEMOHONA" id="KD_PROPINSI_PEMOHONA">
                </div>
                <div class="col-md-1">
                 <input type="text" maxLength="2" class="form-control" name="KD_DATI2_PEMOHONA" id="KD_DATI2_PEMOHONA" value="07">
                </div>
                <div class="col-md-1">
                    <input type="text" maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHONA" id="KD_KECAMATAN_PEMOHONA">
                </div>
                <div class="col-md-1">
                    <input type="text" maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHONA" id="KD_KELURAHAN_PEMOHONA">
                </div>
                <div class="col-md-1">
                    <input type="text" maxLength="3" class="form-control" name="KD_BLOK_PEMOHONA" id="KD_BLOK_PEMOHONA">
                </div>
                <div class="col-md-1">
                    <input type="text" maxLength="4" class="form-control" name="NO_URUT_PEMOHONA" id="NO_URUT_PEMOHONA">
                </div>
                <div class="col-md-1">
                    <input type="text" maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHONA" id="KD_JNS_OP_PEMOHONA">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nama Pemohon </label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="NAMA_PEMOHONA" id="NAMA_PEMOHONA" placeholder="Nama pemohon kolektif" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Alamat Pemohon </label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="ALAMAT_PEMOHONA" id="ALAMAT_PEMOHONA" placeholder="Alamat" />

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Jenis Pelayanan </label>
                <div class="col-md-8">
                    <select class="form-control chosen" name="KD_JNS_PELAYANANA" data-placeholder="Silahkan pilih" id="KD_JNS_PELAYANANA">
                        <option value=""></option>
                        <?php foreach ($pelayanan as $rp) { ?>
                            <option value="<?php echo $rp->KD_JNS_PELAYANAN ?>"><?php echo $rp->NM_JENIS_PELAYANAN ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a class="btn btn-success btn-sm" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</a>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>NOP</th>
                                <th>NAMA WP</th>
                                <th>Alamat WP</th>
                                <th>Jenis Layanan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="kontentdiag"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <div class="btn-group">
                    <input type="hidden" name="idurl" value="<?php echo $id ?>">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <?php echo anchor('permohonan/tambahNop/' . $id, '<i class="fa fa-refresh"></i> Reset', 'class="btn btn-success"') ?>
                    <?php echo anchor('permohonan/detail/' . $id, '<i class="fa fa-list"></i> Kembali', 'class="btn btn-info"'); ?>
                </div>
            </div>
        </div>

    </div>
    </div>
</form>
@endsection
@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
@endsection
@section('script')
<!-- Select2 -->
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    $('.chosen').select2({
        allowClear: true
    });

    $(".form-control").keyup(function() {
        if (this.value.length == this.maxLength) {
            var nextIndex = $('input:text').index(this) + 1;
            $('input:text')[nextIndex].focus();
        }
    });


    $('#KD_JNS_OP_PEMOHONA,#NO_URUT_PEMOHONA,#KD_BLOK_PEMOHONA,#KD_KELURAHAN_PEMOHONA,#KD_KECAMATAN_PEMOHONA,#KD_DATI2_PEMOHONA,#KD_PROPINSI_PEMOHONA').on('change', function() {
        var a = $('#KD_PROPINSI_PEMOHONA').val();
        var b = $('#KD_DATI2_PEMOHONA').val();
        var c = $('#KD_KECAMATAN_PEMOHONA').val();
        var d = $('#KD_KELURAHAN_PEMOHONA').val();
        var e = $('#KD_BLOK_PEMOHONA').val();
        var f = $('#NO_URUT_PEMOHONA').val();
        var g = $('#KD_JNS_OP_PEMOHONA').val();
        var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
        // alert(nop);
        $.ajax({
            url: "<?php echo base_url() . 'permohonan/getNop' ?>",
            method: "POST",
            data: "nop=" + nop,
            cache: false,
            success: function(response) {
                var datashow = JSON.parse(response);
                $("input#NAMA_PEMOHONA").val(datashow[0].NAMA_PEMOHON);
                $("input#ALAMAT_PEMOHONA").val(datashow[0].ALAMAT_PEMOHON);
            }
        });
    });


    $(document).ready(function() {
        var nomor = 0;
        var status = "";
        $("#tambahdiag").click(function() {

            $('#kontentdiag').append(
                '<tr class="barisdiag">' +
                '<input type="hidden" value="' + $('#KD_PROPINSI_PEMOHONA').val() + '" class="form-control" name="KD_PROPINSI_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_DATI2_PEMOHONA').val() + '" class="form-control" name="KD_DATI2_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_KECAMATAN_PEMOHONA').val() + '" class="form-control" name="KD_KECAMATAN_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_KELURAHAN_PEMOHONA').val() + '" class="form-control" name="KD_KELURAHAN_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_BLOK_PEMOHONA').val() + '"  class="form-control" name="KD_BLOK_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#NO_URUT_PEMOHONA').val() + '" class="form-control" name="NO_URUT_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_JNS_OP_PEMOHONA').val() + '" class="form-control" name="KD_JNS_OP_PEMOHONB[]">' +
                '<input type="hidden" value="' + $('#KD_JNS_PELAYANANA').val() + '" class="form-control" name="KD_JNS_PELAYANANB[]">' +
                '<td>' + $('#KD_PROPINSI_PEMOHONA').val() + '.' + $('#KD_DATI2_PEMOHONA').val() + '.' + $('#KD_KECAMATAN_PEMOHONA').val() + '.' + $('#KD_KELURAHAN_PEMOHONA').val() + '.' + $('#KD_BLOK_PEMOHONA').val() + '.' + $('#NO_URUT_PEMOHONA').val() + '.' + $('#KD_JNS_OP_PEMOHONA').val() + '</td>' +
                '<td>' + $('#NAMA_PEMOHONA').val() + '</td>' +
                '<td>' + $('#ALAMAT_PEMOHONA').val() + '</td>' +
                '<td>' + $('#KD_JNS_PELAYANANA').find("option:selected").text() + '</td>' +
                '<td><a href="#" class="remove_project_file"  ><i class="btn btn-xs btn-danger fa fa-trash"></i></a></td></tr>'
            );


            nomor++;
            $('#KD_PROPINSI_PEMOHONA').val(null);
            $('#KD_DATI2_PEMOHONA').val(null);
            $('#KD_KECAMATAN_PEMOHONA').val(null);
            $('#KD_KELURAHAN_PEMOHONA').val(null);
            $('#KD_BLOK_PEMOHONA').val(null);
            $('#NO_URUT_PEMOHONA').val(null);
            $('#KD_JNS_OP_PEMOHONA').val(null);
            $('#NAMA_PEMOHONA').val(null);
            $('#ALAMAT_PEMOHONA').val(null);

        });

        $('#kontentdiag').on('click', '.remove_project_file', function(e) {
            e.preventDefault();

            $(this).parents(".barisdiag").remove();
        });
        $("#form").validate({
            debug: false,
            rules: {},
            messages: {},
        });
    });



    $('input:text').bind("keydown", function(e) {
        var n = $("input:text").length;
        if (e.which == 13) { //Enter key
            e.preventDefault(); //to skip default behavior of the enter key
            var nextIndex = $('input:text').index(this) + 1;
            if (nextIndex < n)
                $('input:text')[nextIndex].focus();
            else {
                $('input:text')[nextIndex - 1].blur();
                $('#btnSubmit').click();
            }
        }
    });


    $(".form-control").keyup(function() {
        if (this.value.length == this.maxLength) {
            var nextIndex = $('input:text').index(this) + 1;
            $('input:text')[nextIndex].focus();
        }


    });
</script>
@endsection