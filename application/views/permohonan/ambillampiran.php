<?php $var = "<option value=''></option>";
foreach ($rk as $rk) {
   $var .= "<option " . $rk->ST . " value='" . $rk->KODE . "'>" . $rk->NAMA_LAMPIRAN . "</option>";
} ?>

<script type="text/javascript">
   $('#lampiran').select2({
      width: "100%"
   });
   $('#lampiran').empty(); //remove all child nodes
   var newOption = $("<?php echo $var ?>");
   $('#lampiran').append(newOption);
   $('#lampiran').trigger("select2:updated");
</script>