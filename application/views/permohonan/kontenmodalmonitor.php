 <table class="table table-bordered table-striped">
   <thead>
     <tr>
       <th>No</th>
       <th>Tanggal</th>
       <th>Posisi</th>
       <th>Status</th>
       <th>Keterangan</th>
       <!-- <th>Aksi</th> -->
     </tr>
   </thead>
   <tbody>
     <?php if (count($rn) > 0) {
        $no = 1;
        foreach ($rn as $rn) {   ?>
         <tr>
           <td align="center"><?php echo $no++; ?></td>
           <td><?php echo $rn->AWAL . ' - ' . $rn->AKHIR ?></td>
           <td><?php echo $rn->POSISI ?></td>
           <td><?php echo $rn->STATUS_BERKAS ?></td>
           <td><?php echo $rn->KETERANGAN ?></td>
           <!-- <td></td> -->
         </tr>
       <?php  }
      } else { ?>
       <tr>
         <td colspan="5">Tidak ada data</td>
       </tr>

     <?php  } ?>
   </tbody>
 </table>