@extends('page.master')
@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Permohonan Hari Ini</h3>
        </div>
        <div class="">
          <table class="table  table-striped">
            <thead>
              <tr>
                <th>Permohonan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($masuk as $masuk) { ?>
                <tr>
                  <td><?= $masuk->PERMOHONAN ?></td>
                  <td align="center"><?= $masuk->JUMLAH ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Permohonan Bulan Ini</h3>
        </div>
        <div class="">
          <table class="table  table-striped">
            <thead>
              <tr>
                <th>Permohonan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($masuk_bulan_ini as $masukb) { ?>
                <tr>
                  <td><?= $masukb->PERMOHONAN ?></td>
                  <td align="center"><?= $masukb->JUMLAH ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Permohonan Sampai Saat Ini</h3>
        </div>
        <div class="">
          <table class="table  table-striped">
            <thead>
              <tr>
                <th>Permohonan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($sampai as $masukc) { ?>
                <tr>
                  <td><?= $masukc->PERMOHONAN ?></td>
                  <td align="center"><?= $masukc->JUMLAH ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">

    <?php foreach ($monitoring as $row) { ?>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo $row->JUMLAH; ?></h3>
            <p><?php echo $row->NAMA_GROUP; ?></p>
          </div>
          <a href="#" class="icon">
            <i class="<?php echo $row->ICON ?>"></i>
          </a>
          <a href="<?php echo '#'; //echo base_url('permohonan/detail_monitoring/'.$row->KODE_GROUP);
                    ?>" class="small-box-footer">
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
    <?php } ?>

  </div>
@endsection
@section('css')
<style type="text/css">
  .layout-top-nav .content-wrapper,
  .layout-top-nav .right-side,
  .layout-top-nav .main-footer {
    padding-left: 5px;
  }

  .small-box .icon {
    top: 3px;
    right: 11px;
    font-size: 61px;
  }
</style>
@endsection