@extends('page.master')
@section('judul')
<h1>
    Data penelitian
    <div class="pull-right">
        <?= anchor('permohonan/verifikasi_berkas', '<i class="fa fa-file"></i> Kembali', 'class="btn btn-sm btn-info"') ?>
    </div>
</h1>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Nopel</th>
                    <th>Pemohon</th>
                    <th>Status</th>
                    <th>Tanggal Verifikasi</th>
                    <th width="8%">Aksi</th>
                </tr>
            </thead>

        </table>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#example2 input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url(); ?>permohonan/json_ferifikasi_berkas_list",
                "type": "post",
            },
            columns: [{
                    "data": "THN_PELAYANAN",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    data: "THN_PELAYANAN",
                    render: function(data, type, row) {
                        return data + '.' + row.BUNDEL_PELAYANAN + '.' + row.NO_URUT_PELAYANAN;
                    }
                },
                {
                    "data": "NAMA_PEMOHON"
                },
                {
                    "data": "PELIMPAHAN",
                    render: function(data, type, row) {
                        pelimpahan="";
                        if(data=='0'){
                            pelimpahan="Tidak memenuhi persyaratan";
                        }else if(data=='1'){
                            pelimpahan="Penelitian Lapangan";
                        }else{
                            pelimpahan="Penelitian Kantor";
                        }
                        return pelimpahan;
                    }
                },
                {
                    "data": "TGL_PENELITI"
                },
                {
                    "data": "THN_PELAYANAN",
                    render: function(data, type, row) {
                        this.url = '<?php echo base_url(); ?>permohonan/berkasverifikasi_url/';
                        var d = '<a target="_blank" href="' + this.url + row.THN_PELAYANAN + row.BUNDEL_PELAYANAN + row.NO_URUT_PELAYANAN + '" class="btn btn-xs btn-info " ><i class="fa fa-print"></i></a>';
                        return d;
                    }
                },
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

@endsection