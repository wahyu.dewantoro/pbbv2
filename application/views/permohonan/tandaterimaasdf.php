 <!DOCTYPE html>
 <html>

 <head>
     <title>Tanda terima</title>
     <style type="text/css">
         body {
             font-family: Courier New, Courier, monospace;
             line-height: 1.42857203;
             color: #333;
             background-color: #fff;
             font-weight: normal;
         }

         @page{
             margin-top: 2mm;
             margin-bottom: 2mm;
             margin-left: 2mm;
             margin-right: 2mm;
         }
     </style>
 </head>

 <body>

     <table cellpadding="3" width="100%" border="0" style=" font-size: 50px;">
         <tr>
             <td>
                 <br>
                 <p>&nbsp;&nbsp;FORMULIR PELAYANAN WAJIB PAJAK<br>
                     &nbsp;&nbsp;KABUPATEN MALANG<br>
                     &nbsp;&nbsp;JL. KH. AGUS SALIM NO.7</P>
                 <hr>
             </td>
         </tr>
         <tr>
             <td>
                 <br>
                 <!-- konten -->
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td width="50%"></td>
                         <td>
                             <table cellpadding="3" width="100%">
                                 <tbody>
                                     <tr>
                                         <td width="5%">1.</td>
                                         <td width="40%">NOMOR PELAYANAN</td>
                                         <td width="2%">:</td>
                                         <td><?php echo $rk->NO_LAYANAN ?> <?php if($rk->IS_ONLINE==1){ echo " ( ONLINE )";}?> </td>
                                     </tr>
                                     <tr>
                                         <td>2.</td>
                                         <td>TANGGAL PELAYANAN</td>
                                         <td>:</td>
                                         <td><?php echo date_indo($rk->TGL_TERIMA_DOKUMEN_WP) ?></td>
                                     </tr>
                                     <tr>
                                         <td>3.</td>
                                         <td>TGL SELESAI (PERKIRAAN)</td>
                                         <td>:</td>
                                         <td><?php echo date_indo($rk->TGL_PERKIRAAN_SELESAI) ?></td>
                                     </tr>
                                 </tbody>
                             </table>

                         </td>
                     </tr>
                 </table>
                 <br><br>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td width="3%">4.</td>
                         <td width="25%">JENIS PELAYANAN</td>
                         <td width="3%">:</td>
                         <td><?php echo $rg[0]['LAYANAN'];
                                if ($rk->STATUS_KOLEKTIF == '0') {
                                    echo ' / Individual';
                                } else {
                                    echo " / Kolektif";
                                }   ?></td>
                     </tr>
                     <tr>
                         <td>5.</td>
                         <td>N O P</td>
                         <td>:</td>
                         <td><?php echo $rg[0]['NOP']; ?></td>
                     </tr>
                 </table>
                 <br><br>
                 <hr>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td align="center">
                             A. DATA WAJIB / OBJEK PAJAK
                         </td>
                     </tr>
                 </table>
                 <hr>
                 <br><br>
                 <table cellpadding="3" width="100%">
                     <tbody>
                         <tr>
                             <td width="3%">6.</td>
                             <td width="25%">NAMA PEMOHON / NIK [No KTP]</td>
                             <td width="3%">:</td>
                             <td><?php echo $rk->NAMA_PEMOHON ?> <?= $rk->NIK<>''?' ['.$rk->NIK.']':'' ?> </td>
                         </tr>
                         <tr>
                             <td></td>
                             <td>ALAMAT PEMOHON</td>
                             <td>:</td>
                             <td><?php echo $rk->ALAMAT_PEMOHON ?></td>
                         </tr>
                         <tr>
                             <td></td>
                             <td>NO TELP</td>
                             <td>:</td>
                             <td><?php echo $rk->IP_HP ?></td>
                         </tr>
                         <tr>
                             <td></td>
                             <td>EMAIL</td>
                             <td>:</td>
                             <td><?php echo $rk->IP_EMAIL ?></td>
                         </tr>
                         <tr>
                             <td>7.</td>
                             <td>LETAK OBJEK PAJAK</td>
                             <td>:</td>
                             <td><?php echo $rg[0]['JALAN_OP']; ?></td>
                         </tr>
                         <tr>
                             <td></td>
                             <td>KELURAHAN</td>
                             <td>:</td>
                             <td><?php echo $rg[0]['NM_KELURAHAN']; ?></td>
                         </tr>
                         <tr>
                             <td></td>
                             <td>KECAMATAN</td>
                             <td>:</td>
                             <td><?php echo $rg[0]['NM_KECAMATAN']; ?></td>
                         </tr>
                         <tr>
                             <td>8.</td>
                             <td>KETERANGAN</td>
                             <td>:</td>
                             <td><?php echo $rk->CATATAN_PST ?></td>
                         </tr>
                     </tbody>
                 </table>
                 <br><br>
                 <hr>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td align="center">
                             B. PENERIMAAN BERKAS
                         </td>
                     </tr>
                 </table>
                 <hr>
                 <br><br>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td>9. DOKUMEN DILAMPIRKAN :</td>

                     </tr>
                 </table>
                 <br>
                 <table cellpadding="3" border="0">
                     <tr>
                         <td width="2%">&nbsp;&nbsp;</td>
                         <td width="30%" valign="top">
                             <table cellpadding="3" width="100%">
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_PERMOHONAN == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>1.</td>
                                     <td>Pengajuan Permohonan </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_SURAT_KUASA == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>2.</td>
                                     <td>Surat Kuasa </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_KTP_WP == '1') { ?> * <?php }
                                                                                        } ?></td>
                                     <td>3.</td>
                                     <td>FC KTP/SIM dan KK </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_SERTIFIKAT_TANAH == '1') { ?> * <?php }
                                                                                                } ?></td>
                                     <td>4.</td>
                                     <td>FC Sertifikat / Kepemilikan </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_SPPT == '1') { ?> * <?php }
                                                                                    } ?></td>
                                     <td>5.</td>
                                     <td>Asli SPPT </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_IMB == '1') { ?> * <?php }
                                                                                    } ?></td>
                                     <td>6.</td>
                                     <td>FC IMB </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_AKTE_JUAL_BELI == '1') { ?> * <?php }
                                                                                                } ?></td>
                                     <td>7.</td>
                                     <td>FC akte jual beli/hibah </td>
                                 </tr>
                             </table>
                         </td>
                         <td width="30%" valign="top">
                             <table cellpadding="3" width="100%">
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SK_PENSIUN == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>8.</td>
                                     <td> FC SK Pensiun / Veteran </td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SPPT_STTS == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>9.</td>
                                     <td> FC SPPT / SSPD / Bukti lunas</td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_STTS == '1') { ?> * <?php }
                                                                                    } ?></td>
                                     <td>10.</td>
                                     <td>Asli BBPD / Bukti Lunas</td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SK_PENGURANGAN == '1') { ?> * <?php }
                                                                                                } ?></td>
                                     <td>11.</td>
                                     <td>FC SK Pengurangan</td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SK_KEBERATAN == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>12.</td>
                                     <td>FC SK Keberatan</td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SKKP_PBB == '1') { ?> * <?php }
                                                                                        } ?></td>
                                     <td>13.</td>
                                     <td>FC SSPD BPHTB</td>
                                 </tr>
                                 <tr>
                                     <td align="right"><?php if (!empty($dok)) {
                                                            if ($dok->L_SPMKP_PBB == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>20.</td>
                                     <td>SKTM </td>
                                 </tr>
                             </table>
                         </td>
                         <td width="30%" valign="top">
                             <table cellpadding="3" width="100%">
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_SKET_TANAH == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>15.</td>
                                     <td>Sket tanah </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_SKET_LURAH == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>16.</td>
                                     <td>Sket Lurah </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_NPWPD == '1') { ?> * <?php }
                                                                                        } ?></td>
                                     <td>17.</td>
                                     <td>NPWP/NPWPD </td>
                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_PENGHASILAN == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>18.</td>
                                     <td>Rincian Penghasil </td>

                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_CAGAR == '1') { ?> * <?php }
                                                                                        } ?></td>
                                     <td>12.</td>
                                     <td>SK Cagar budaya </td>

                                 </tr>
                                 <tr>
                                     <td align="right"> <?php if (!empty($dok)) {
                                                            if ($dok->L_LAIN_LAIN == '1') { ?> * <?php }
                                                                                            } ?></td>
                                     <td>20.</td>
                                     <td>Lain - lain </td>

                                 </tr>
                             </table>
                         </td>
                     </tr>
                 </table>
                 <br><br>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td>10.</td>
                         <td> Catatan</td>
                     </tr>
                     <tr>
                         <td></td>
                         <td><?php echo $rk->CATATAN_PST ?> <?php
                                                            $er = $this->db->query(" select cek_lunas_sppt('" . substr($rk->NO_LAYANAN, 0, 4) . "','" . $rg[0]['NOP'] . "') st from dual")->row();
                                                            if ($er->ST == 1) {
                                                                echo " / SPPT akan diterbitkan tahun  depan (" . substr($rk->NO_LAYANAN, 0, 4) + 1 . "), karena sudah lunas sebelum pengajuan";
                                                            }
                                                            ?></td>
                     </tr>
                 </table>
                 <br><br>
                 <table cellpadding="3" width="100%">
                     <tr>
                         <td>PETUGAS PENERIMA BERKAS: <?php echo $rk->NAMA_PENGGUNA ?></td>
                         <td align="center">NIP: <?php echo $rk->NIP_PENERIMA ?></td>
                     </tr>
                     <tr>
                         <td align="center" colspan="2"><br><br>------------------------------------ Gunting Disini------------------------------------ <br><br><br><br></td>
                     </tr>
                 </table>
             </td>
         </tr>
         <tr>
             <td>
                 <!-- footer -->
                 <table cellpadding="3" width="100%">
                     <tr valign="top">
                         <td>
                             <!-- kiri-->
                             <br>
                             <table cellpadding="3" width="100%">
                                 <tr>
                                     <td>11.</td>
                                     <td>N O P</td>
                                     <td>:</td>
                                     <td><?php echo $rg[0]['NOP']; ?></td>
                                 </tr>
                                 <tr>
                                     <td>12.</td>
                                     <td>NOMOR PELAYANAN</td>
                                     <td>:</td>
                                     <td><?php echo $rk->NO_LAYANAN ?></td>
                                 </tr>
                                 <tr>
                                     <td></td>
                                     <td colspan="3">
                                         <br>
                                         <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                                             KABUPATEN MALANG<br>
                                             JL. KH. AGUS SALIM NO.7</P>
                                     </td>
                                 </tr>
                                 <tr>
                                     <td></td>
                                     <td colspan="3" align="center">
                                         <br>
                                         <hr>
                                         TANDA PENDAFTARAN PELAYANAN
                                         <hr>
                                         <br>
                                     </td>
                                 </tr>
                                 <tr>
                                     <td colspan="4"><br></td>
                                 </tr>
                                 <tr>
                                     <td>16.</td>
                                     <td>URUSAN</td>
                                     <td>:</td>
                                     <td><?php echo $rg[0]['LAYANAN'];
                                            if ($rk->STATUS_KOLEKTIF == '0') {
                                                echo ' [Individual]';
                                            } else {
                                                echo " [Kolektif]";
                                            }   ?></td>
                                 </tr>
                                 <tr>
                                     <td>17.</td>
                                     <td>CATATAN</td>
                                     <td>:</td>
                                     <td><?php echo $rk->CATATAN_PST ?> <?php if ($er->ST == 1) {
                                                                            echo " / SPPT akan diterbitkan tahun depan (" . (int) substr($rk->NO_LAYANAN, 0, 4) + 1 . "), karena sudah lunas sebelum pengajuan.";
                                                                        } ?></td>
                                 </tr>
                             </table>
                         </td>
                         <td>
                             <!-- kanan-->
                             <table cellpadding="3" width="100%" border="0">
                                 <tr>
                                     <td width="10px">13.</td>
                                     <td width="900px">TANGGAL PELAYANAN</td>
                                     <td width="10px">:</td>
                                     <td><?php echo date_indo($rk->TGL_TERIMA_DOKUMEN_WP) ?></td>
                                 </tr>
                                 <tr>
                                     <td>14.</td>
                                     <td>TGL SELESAI (PERKIRAAN)</td>
                                     <td>:</td>
                                     <td><?php echo date_indo($rk->TGL_PERKIRAAN_SELESAI) ?></td>
                                 </tr>
                                 <!-- <tr>
								<td colspan="4"><br></td>
							</tr> -->
                                 <tr>
                                     <td>15.</td>
                                     <td colspan="3">PETUGAS PENERIMA BERKAS</td>
                                 </tr>
                                 <tr>
                                     <td colspan="4"><br><br><br><br> </td>
                                 </tr>
                                 <tr>
                                     <td colspan="4"><?php echo $rk->NAMA_PENGGUNA ?></td>
                                 </tr>
                                 <tr>
                                     <td colspan="4"><?php echo $rk->NIP_PENERIMA ?></td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                 </table>
             </td>
         </tr>
     </table>
 </body>

 </html>