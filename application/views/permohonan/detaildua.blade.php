@extends('page.master')
@section('judul')
<h1>
  Data Permohonan
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>
  <li><a href="<?= base_url() . 'permohonan' ?>">Data Permohonan</a> </li>
  <li class="active"><?php echo $rk->NO_LAYANAN ?></li>
</ol>
@endsection
@section('content')

<div class="box">
  <div class="box-body">
    <?php //echo $this->session->flashdata('notif')??''; ?>
    <table class="table">
      <tbody>
        <tr>
          <td width="20%">No Layanan</td>
          <td width="1%">:</td>
          <td><?php echo $rk->NO_LAYANAN ?></td>
          <td rowspan="11">
            Dokumen Terlampir :
            <ol>
              <li>Pengajuan Permohonan <?php if ($dok->L_PERMOHONAN                == '1') {
                                          echo "( <i class='fa fa-check'></i> )";
                                        } ?></li>
              <li>Surat Kuasa <?php if ($dok->L_SURAT_KUASA                        == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
              <li>FC KTP/SIM dan KK <?php if ($dok->L_KTP_WP                       == '1') {
                                      echo "( <i class='fa fa-check'></i> )";
                                    } ?></li>
              <li>FC Sertifikat / Kepemilikan <?php if ($dok->L_SERTIFIKAT_TANAH == '1') {
                                                echo "( <i class='fa fa-check'></i> )";
                                              } ?></li>
              <li>Asli SPPT <?php if ($dok->L_SPPT                                 == '1') {
                              echo "( <i class='fa fa-check'></i> )";
                            } ?></li>
              <li>FC IMB <?php if ($dok->L_IMB                                     == '1') {
                            echo "( <i class='fa fa-check'></i> )";
                          } ?></li>
              <li>FC akte jual beli/hibah <?php if ($dok->L_AKTE_JUAL_BELI         == '1') {
                                            echo "( <i class='fa fa-check'></i> )";
                                          } ?></li>
              <li>FC SK Pensiun / Veteran <?php if ($dok->L_SK_PENSIUN             == '1') {
                                            echo "( <i class='fa fa-check'></i> )";
                                          } ?></li>
              <li>FC SPPT / SSPD / Bukti lunas<?php if ($dok->L_SPPT_STTS          == '1') {
                                                echo "( <i class='fa fa-check'></i> )";
                                              } ?></li>
              <li>Asli BBPD / Bukti Lunas<?php if ($dok->L_STTS                    == '1') {
                                            echo "( <i class='fa fa-check'></i> )";
                                          } ?></li>
              <li>FC SK Pengurangan<?php if ($dok->L_SK_PENGURANGAN                == '1') {
                                      echo "( <i class='fa fa-check'></i> )";
                                    } ?></li>
              <li>FC SK Keberatan<?php if ($dok->L_SK_KEBERATAN                    == '1') {
                                    echo "( <i class='fa fa-check'></i> )";
                                  } ?></li>
              <li>FC SSPD BPHTB<?php if ($dok->L_SKKP_PBB                          == '1') {
                                  echo "( <i class='fa fa-check'></i> )";
                                } ?></li>
              <li>SKTM <?php if ($dok->L_SPMKP_PBB                                 == '1') {
                          echo "( <i class='fa fa-check'></i> )";
                        } ?></li>
              <li>Sket tanah<?php if ($dok->L_SKET_TANAH                           == '1') {
                              echo "( <i class='fa fa-check'></i> )";
                            } ?></li>
              <li>Sket Lurah <?php if ($dok->L_SKET_LURAH                          == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
              <li>NPWP/NPWPD<?php if ($dok->L_NPWPD                                == '1') {
                              echo "( <i class='fa fa-check'></i> )";
                            } ?></li>
              <li>Rincian Penghasil <?php if ($dok->L_PENGHASILAN                  == '1') {
                                      echo "( <i class='fa fa-check'></i> )";
                                    } ?></li>
              <li>SK Cagar budaya <?php if ($dok->L_CAGAR                          == '1') {
                                    echo "( <i class='fa fa-check'></i> )";
                                  } ?></li>
              <li>Lain - lain <?php if ($dok->L_LAIN_LAIN                          == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
            </ol>
          </td>
        </tr>
        <tr>
          <td>Tanggal Pelayanan</td>
          <td>:</td>
          <td><?php echo $rk->TGL_TERIMA_DOKUMEN_WP ?></td>
        </tr>
        <tr>
          <td>Tanggal Selesai (<small>Perkiraan</small>)</td>
          <td>:</td>
          <td><?php echo $rk->TGL_PERKIRAAN_SELESAI ?></td>
        </tr>
        <tr>
          <td>Jenis Pelayanan </td>
          <td>:</td>
          <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                echo $rg[0]['LAYANAN'] . ' / Individual';
              } else {
                echo " / Kolektif";
              }   ?></td>
        </tr>
        <tr>
          <td>N.O.P</td>
          <td>:</td>
          <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                echo $rg[0]['NOP'];
              } else {
                echo "-";
              }   ?></td>
        </tr>
        <tr>
          <td>Nama Pemohon</td>
          <td>:</td>
          <td><?php echo $rk->NAMA_PEMOHON ?></td>
        </tr>
        <tr>
          <td>Alamat Pemohon</td>
          <td>:</td>
          <td><?php echo $rk->ALAMAT_PEMOHON ?></td>
        </tr>
        <tr>
          <td>Letak Objek Pajak</td>
          <td>:</td>
          <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                echo $rg[0]['JALAN_OP'];
              } else {
                echo "-";
              }   ?></td>
        </tr>
        <tr>
          <td>Kelurahan</td>
          <td>:</td>
          <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                echo $rg[0]['NM_KELURAHAN'];
              } else {
                echo "-";
              }   ?></td>
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>:</td>
          <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                echo $rg[0]['NM_KECAMATAN'];
              } else {
                echo "-";
              }   ?></td>
        </tr>
        <tr>
          <td>Keterangan</td>
          <td>:</td>
          <td><?php echo $rk->KETERANGAN_PST ?></td>
        </tr>

      </tbody>
    </table>
    <?php if ($rk->STATUS_KOLEKTIF == '1') { ?>
      <br>
      <P><b>Lampiran tanda terima pelayanan kolektif</b></P>
      <table id="example3" class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>NO Layanan</th>
            <th>NOP</th>
            <th>Nama WP</th>
            <th>Tahun</th>
            <th>Jenis Pelayanan</th>

          </tr>
        </thead>
        <tbody>
          <?php $urut = 0;
          $no = 1;
          foreach ($rg as $rg) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td><?php echo $rg['NO_LAYANAN'] . '.' . sprintf('%03s', $urut) ?></td>
              <td><?php echo $rg['NOP'] ?></td>
              <td><?php echo $rg['NM_WP'] ?></td>
              <td><?php echo $rg['THN_PELAYANAN'] ?></td>
              <td><?php echo $rg['LAYANAN'] ?></td>

            </tr>
          <?php $no++;
            $urut++;
          } ?>
        </tbody>
      </table>
    <?php } ?>
  </div>
</div>
@endsection