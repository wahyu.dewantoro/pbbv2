@extends('page.master')
@section('judul')
    <h1>
        Form Permohonan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Pelayanan</a></li>
        <li class="active">Form Permohonan</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Form</h3>
                </div>
                <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">No Pelayanan </label>
                                        <div class="col-md-8">
                                            <table>
                                                <tr>
                                                    <td><input type="text" readonly class="form-control inputs"
                                                            maxlength="4" name="THN_PELAYANAN" id="THN_PELAYANAN"
                                                            placeholder="Tahun" value="<?php echo $THN_PELAYANAN; ?>" required /></td>
                                                    <td><input type="text" readonly class="form-control inputs"
                                                            maxlength="4" name="BUNDEL_PELAYANAN" id="BUNDEL_PELAYANAN"
                                                            placeholder="Bundel" required value="<?php echo $BUNDEL_PELAYANAN; ?>" />
                                                    </td>
                                                    <td><input type="text" readonly class="form-control inputs"
                                                            maxlength="3" name="NO_URUT_PELAYANAN" id="NO_URUT_PELAYANAN"
                                                            placeholder="No urut" required value="<?php echo $NO_URUT_PELAYANAN; ?>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="col-md-4 control-label">NIK</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="text" name="NIK" id="NIK" class="form-control" required>
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default ">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4  control-label">Nama </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" required name="NAMA_PEMOHON"
                                                id="NAMA_PEMOHON" placeholder="Nama pemohon" value="<?php echo $NAMA_PEMOHON; ?>" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">E Mail </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control " name="IP_EMAIL" id="email"
                                                placeholder="Email" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">No. HP </label>
                                        <div class="col-md-8">
                                            <input maxlength="14" type="text" class="form-control" required name="IP_HP"
                                                id="IP_HP" placeholder="No. Hp" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Alamat</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" required name="ALAMAT_PEMOHON"
                                                id="ALAMAT_PEMOHON" placeholder="Alamat" value="<?php echo $ALAMAT_PEMOHON; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Status Kolektif </label>
                                        <div class="col-md-8">
                                            <select class="select2 form-control" required name="STATUS_KOLEKTIF"
                                                id="STATUS_KOLEKTIF" style="width: 100%;">
                                                <option value=""></option>
                                                <option selected value="0">1. Individu</option>
                                                <option value="1">2. Kolektif / Masal</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Pelayanan </label>
                                        <div class="col-md-8">
                                            <select class="form-control select2" required name="KD_JNS_PELAYANAN"
                                                id="KD_JNS_PELAYANAN" style="width: 100%;"
                                                data-placeholder="Silahkan Pilih">
                                                <option value=""></option>
                                                <?php foreach ($pelayanan as $rp) { ?>
                                                <option value="<?php echo $rp->KD_JNS_PELAYANAN; ?>"><?php echo $rp->KD_JNS_PELAYANAN . ' - ' . $rp->NM_JENIS_PELAYANAN; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Catatan </label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" required name="CATATAN_PST"
                                                id="CATATAN_PST" placeholder="catatan" value="<?php echo $CATATAN_PST; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4  control-label">NOP </label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="text" data-count='1' name="NOP[]" id="NOP1"
                                                    class="form-control nop" required>
                                                <div class="input-group-btn" id="divplus">
                                                    <button type="button" class="btn btn-info " id="tambahnop">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <span id="keterangannop1"></span>
                                        </div>
                                        <div id="reskolektif">
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-4  control-label">Scan Barcode </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="barcode" id="barcode">
                                                <span id="test"></span>
                                            </div>
                                        </div>
                                    </div> -->
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-2">
                                            <div class="btn-group">
                                                <button type="submit" id="myBtn" class="btn btn-sm btn-success"><i
                                                        class="fa fa-save"></i>
                                                    Submit</button>
                                                <button type="reset" class="btn btn-sm btn-warning"><i
                                                        class="fa fa-refresh"></i> Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="inputlampiran"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
    <style type="text/css">
        .dua {
            width: 9.5%;
        }

        .tiga {
            width: 11%;
        }

        .empat {
            width: 12%;
        }

        .satu {
            width: 9%;
        }

    </style>
@endsection
<!-- Select2 -->
@section('script')
    <!-- Select2 -->
    <script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            // select2
            $('.select2').select2({
                allowClear: true
            });
            kosongkan();

            // KD_JNS_PELAYANAN
            // inputlampiran

            $('#KD_JNS_PELAYANAN').on('change', function(e) {
                var get_ = $(this).val();
                $('.nop').trigger('keyup');
                if (get_ != '') {
                    $.ajax({
                        url: "<?= base_url('permohonan/daftarlampiran') ?>",
                        data: {
                            kode: get_
                        },
                        success: function(hasil) {
                            $('#inputlampiran').html(hasil);
                        },
                        error: function(rs) {
                            $('#inputlampiran').html('Sistem bermasalah');
                        }
                    });
                } else {
                    $('#inputlampiran').html('');
                }
            });

            // format nop
            $('.nop').on('keyup change', function() {
                var get = $(this).val();
                var res = formatnop(get);
                $(this).val(res);

                var cc = $(this).data('count');

                // cek riwayat pembayaran
                b = get.replace(/[^\d]/g, "");
                if (b.length == '18') {
                    pembayaranNop(b, cc);
                } else {
                    $('#keterangannop' + cc).html('');
                }
            });

            // divplus

            // KD_JNS_PELAYANAN

            // STATUS_KOLEKTIF

            $('#divplus').hide();

            $('#STATUS_KOLEKTIF').trigger('change');

            $('#STATUS_KOLEKTIF').on('change', function() {
                console.log($(this).val())
                if ($(this).val() == '1') {
                    $('#divplus').show();
                } else {
                    $('#divplus').hide();
                    $('#reskolektif').remove();
                }
                // formKolektif($(this).val());
            });

            function formKolektif(obj) {
                if (obj == '1') {
                    $('#divplus').hide();
                } else {
                    $('#divplus').show();
                }
            }


            $('#IP_HP').on('keyup', function() {
                var get = $(this).val();
                b = get.replace(/[^\d]/g, "");
                $(this).val(b);
            });

            function pembayaranNop(obj, cc) {
                var jns = $('#KD_JNS_PELAYANAN').val();

                $.ajax({
                    url: "<?= base_url('permohonan/riwayatpembayaran') ?>",
                    dataType: 'json',
                    cache: false,
                    data: {
                        'nop': obj
                    },
                    success: function(hasil) {
                        // console.log(hasil);
                        

                        if(hasil.status=='0'){

                            if(jns=='01'){
                                $('#keterangannop' + cc).html('');
                            }else{
                                 $('#NOP' + cc).val('');
                                 $('#keterangannop' + cc).html(hasil.keterangan);
                             }

                        }else{
                            $('#keterangannop' + cc).html(hasil.keterangan);
                        }

                            
                            



                        /* // '16', '15', '17'
                        console.log(' jenis '+jns);
                        if ( jns!='01' || jns!='16' || jns!='15' || jns!='17') {
                            // $('#NOP' + cc).val('');
                            console.log(jns +' -> '+hasil);
                        } */
                    },
                    error: function(error) {
                        console.log(error);
                        $('#keterangannop' + cc).html('Terjadi kesalahan sistem');
                    }
                });
            }

            $('#NIK').on('keyup change', function() {
                var get = $(this).val();
                b = get.replace(/[^\d]/g, "");
                $(this).val(b);

                // cek nik
                if (b.length == 16) {
                    $.ajax({
                        url: "<?= base_url('permohonan/ceknik') ?>",
                        data: {
                            nik: b
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(res) {
                            console.log(res.raw);
                            $('#NAMA_PEMOHON').val(res.raw.nama_lgkp);
                            $('#ALAMAT_PEMOHON').val(res.raw.alamat + ' ' + res.raw.kel_name +
                                ' ' + res.raw.kec_name + ' ' + res.raw.kab_name + ' ' + res
                                .raw.prop_name);

                        },
                        errror: function(e) {
                            $('#NAMA_PEMOHON').val('');
                            $('#ALAMAT_PEMOHON').val('');
                        }
                    });
                } else {
                    $('#NAMA_PEMOHON').val('');
                    $('#ALAMAT_PEMOHON').val('');
                }
            });

            var i = 2;
            $('#tambahnop').on('click', function(e) {
                console.log('clicked');
                e.preventDefault();
                ++i;
                $("#reskolektif").append('<div class="col-md-8 col-md-offset-4 barisan' + i + '">\
                                                                    <div class="input-group">\
                                                                        <input type="text" data-count="' + i +
                    '" name="NOP[]" id="NOP' + i +
                    '" class="form-control nop" required>\
                                                                        <div class="input-group-btn">\
                                                                            <button  type="button" class="btn btn-danger hapusnop" data-hapus="' +
                    i + '">\
                                                                                <i class="fa fa-trash"></i>\
                                                                            </button>\
                                                                          </div>\
                                                                      </div>\
                                                                    <span id="keterangannop' + i + '"></span>\
                                                                </div>');

                $('.nop').on('keyup change', function() {
                    var get = $(this).val();
                    var res = formatnop(get);
                    $(this).val(res);

                    var cc = $(this).data('count');

                    // cek riwayat pembayaran
                    b = get.replace(/[^\d]/g, "");
                    if (b.length == '18') {
                        pembayaranNop(b, cc);
                    } else {
                        $('#keterangannop' + cc).html('');
                    }
                });


                $('.hapusnop').on('click', function(e) {
                    e.preventDefault();
                    var this_ = $(this).data('hapus');
                    $(this).parent().parent().remove();
                    console.log($(this_).parent().parent());
                    // i--;
                });
            });





            var nomor = 0;
            var status = "";

            // satu
            /* $('#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON')
                .on('keyup change', function() {
                    dds = $('#KD_JNS_PELAYANAN').val();
                    if (dds == '01') {
                        $('#myBtn').prop('disabled', false);
                    } else {
                        $('#myBtn').prop('disabled', true);
                    }
                    getnopindividu();
                }); */

            // dua

            // tiga


            // empat
            /* function deskripsia(nop) {
                $.ajax({
                    url: "<?php echo base_url() . 'permohonan/deskripsialert'; ?>",
                    method: "POST",
                    data: "nop=" + nop,
                    cache: false,
                    success: function(response) {
                        $("#bayar").html('');
                        alert(response);

                    }
                });
            } */

            // lima
            /* function getnopdecodea(barcode) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() . 'permohonan/decodebarcode' ?>",
                    data: {
                        'barcode': barcode
                    },
                    success: function(response) {
                        var datashow = JSON.parse(response);
                        $('#KD_PROPINSI_PEMOHONA').val(datashow[0].KD_PROPINSI_PEMOHON);
                        $('#KD_DATI2_PEMOHONA').val(datashow[0].KD_DATI2_PEMOHON);
                        $('#KD_KECAMATAN_PEMOHONA').val(datashow[0].KD_KECAMATAN_PEMOHON);
                        $('#KD_KELURAHAN_PEMOHONA').val(datashow[0].KD_KELURAHAN_PEMOHON);
                        $('#KD_BLOK_PEMOHONA').val(datashow[0].KD_BLOK_PEMOHON);
                        $('#NO_URUT_PEMOHONA').val(datashow[0].NO_URUT_PEMOHON);
                        $('#KD_JNS_OP_PEMOHONA').val(datashow[0].KD_JNS_OP_PEMOHON);

                        var a = datashow[0].KD_PROPINSI_PEMOHON;
                        var b = datashow[0].KD_DATI2_PEMOHON;
                        var c = datashow[0].KD_KECAMATAN_PEMOHON;
                        var d = datashow[0].KD_KELURAHAN_PEMOHON;
                        var e = datashow[0].KD_BLOK_PEMOHON;
                        var f = datashow[0].NO_URUT_PEMOHON;
                        var g = datashow[0].KD_JNS_OP_PEMOHON;
                        var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;

                        $.ajax({
                            url: "<?php echo base_url() . 'permohonan/getNop'; ?>",
                            method: "POST",
                            data: "nop =" + nop,
                            cache: false,
                            success: function(response) {
                                if (response == 0) {

                                    $("input#NAMA_PEMOHON").val('');
                                    $("input#ALAMAT_PEMOHONA").val('');
                                    $("#bayar").html('');
                                    $('#KD_KECAMATAN_PEMOHONA').val('');
                                    $('#KD_KELURAHAN_PEMOHONA').val('');
                                    $('#KD_BLOK_PEMOHONA').val('');
                                    $('#NO_URUT_PEMOHONA').val('');
                                    $('#KD_JNS_OP_PEMOHONA').val('');
                                    // deskripsi(nop);
                                    $.ajax({
                                        url: "<?php echo base_url() . 'permohonan/deskripsialert'; ?>",
                                        method: "POST",
                                        data: "nop=" + nop,
                                        cache: false,
                                        success: function(response) {
                                            alert(response);
                                            $("#bayar").html('');
                                        }
                                    });
                                } else {
                                    var datashow = JSON.parse(response);
                                    $("input#NAMA_PEMOHONA").val(datashow[0].NAMA_PEMOHON);
                                    $("input#ALAMAT_PEMOHONA").val(datashow[0]
                                        .ALAMAT_PEMOHON);
                                }
                            }
                        });
                    }
                });
            } */

            // enam
            /* function getnopdecode(barcode) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() . 'permohonan/decodebarcode' ?>",
                    data: {
                        'barcode': barcode
                    },
                    success: function(response) {
                        var datashow = JSON.parse(response);
                        $('#KD_PROPINSI_PEMOHON').val(datashow[0].KD_PROPINSI_PEMOHON);
                        $('#KD_DATI2_PEMOHON').val(datashow[0].KD_DATI2_PEMOHON);
                        $('#KD_KECAMATAN_PEMOHON').val(datashow[0].KD_KECAMATAN_PEMOHON);
                        $('#KD_KELURAHAN_PEMOHON').val(datashow[0].KD_KELURAHAN_PEMOHON);
                        $('#KD_BLOK_PEMOHON').val(datashow[0].KD_BLOK_PEMOHON);
                        $('#NO_URUT_PEMOHON').val(datashow[0].NO_URUT_PEMOHON);
                        $('#KD_JNS_OP_PEMOHON').val(datashow[0].KD_JNS_OP_PEMOHON);
                        // getnopindividu();
                        var a = datashow[0].KD_PROPINSI_PEMOHON;
                        var b = datashow[0].KD_DATI2_PEMOHON;
                        var c = datashow[0].KD_KECAMATAN_PEMOHON;
                        var d = datashow[0].KD_KELURAHAN_PEMOHON;
                        var e = datashow[0].KD_BLOK_PEMOHON;
                        var f = datashow[0].NO_URUT_PEMOHON;
                        var g = datashow[0].KD_JNS_OP_PEMOHON;
                        var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;

                        $.ajax({
                            url: "<?php echo base_url() . 'permohonan/getNop'; ?>",
                            method: "POST",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(response) {
                                if (response == 0) {
                                    // alert('NOP sudah mengajukan permohonan');
                                    $("input#NAMA_PEMOHON").val('');
                                    $("input#ALAMAT_PEMOHON").val('');
                                    $("#bayar").html('');
                                    $('#KD_KECAMATAN_PEMOHON').val('');
                                    $('#KD_KELURAHAN_PEMOHON').val('');
                                    $('#KD_BLOK_PEMOHON').val('');
                                    $('#NO_URUT_PEMOHON').val('');
                                    $('#KD_JNS_OP_PEMOHON').val('');
                                    // deskripsi(nop);
                                    $.ajax({
                                        url: "<?php echo base_url() . 'permohonan/deskripsialert'; ?>",
                                        method: "POST",
                                        data: "nop=" + nop,
                                        cache: false,
                                        success: function(response) {
                                            alert(response);
                                            $("#bayar").html('');
                                        }
                                    });
                                } else {
                                    var datashow = JSON.parse(response);
                                    $("input#NAMA_PEMOHON").val(datashow[0].NAMA_PEMOHON);
                                    $("input#ALAMAT_PEMOHON").val(datashow[0]
                                        .ALAMAT_PEMOHON);
                                }
                            }
                        });
                    }
                });
            } */

            // tujuh


            // delapan
            function kosongkan() {
                $('#NO_SRT_PERMOHONAN').val('-');
                $('#barcode').val('');
                $('#STATUS_KOLEKTIF').val('0').trigger('chosen:updated');
                $('#KD_JNS_PELAYANAN').val('').trigger('chosen:updated');
                $('#KD_PROPINSI_PEMOHON').val('35');
                $('#KD_DATI2_PEMOHON').val('07');
                $('#KD_KECAMATAN_PEMOHON').val('');
                $('#KD_KELURAHAN_PEMOHON').val('');
                $('#KD_BLOK_PEMOHON').val('');
                $('#NO_URUT_PEMOHON').val('');
                $('#KD_JNS_OP_PEMOHON').val('');
                $('#NAMA_PEMOHON').val('');
                $('#ALAMAT_PEMOHON').val('');
                $('#CATATAN_PST').val('');
                $('#email').val('');
                $('#IP_HP').val('');
                $('#jml_berkas').val('');
                $('#lampiran').val('').trigger('chosen:updated');
                $('#demo').html('');
            }

            /* // sembilan (get nop kolektif)
                        function nopKolektif() {
                            var a = $('#KD_PROPINSI_PEMOHONA').val();
                            var b = $('#KD_DATI2_PEMOHONA').val();
                            var c = $('#KD_KECAMATAN_PEMOHONA').val();
                            var d = $('#KD_KELURAHAN_PEMOHONA').val();
                            var e = $('#KD_BLOK_PEMOHONA').val();
                            var f = $('#NO_URUT_PEMOHONA').val();
                            var g = $('#KD_JNS_OP_PEMOHONA').val();
                            var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
                            $.ajax({
                                url: "<?php echo base_url() . 'permohonan/getNop'; ?>",
                                method: "POST",
                                data: "nop=" + nop,
                                cache: false,
                                success: function(response) {
                                    if (response == 0) {
                                        // alert('NOP sudah mengajukan permohonan');
                                        $("input#NAMA_PEMOHONA").val('');
                                        $("input#ALAMAT_PEMOHONA").val('');
                                        $("#bayar").html('');
                                        $('#KD_KECAMATAN_PEMOHONA').val('');
                                        $('#KD_KELURAHAN_PEMOHONA').val('');
                                        $('#KD_BLOK_PEMOHONA').val('');
                                        $('#NO_URUT_PEMOHONA').val('');
                                        $('#KD_JNS_OP_PEMOHONA').val('');
                                        deskripsia(nop);

                                    } else {
                                        var datashow = JSON.parse(response);
                                        $("input#NAMA_PEMOHONA").val(datashow[0].NAMA_PEMOHON);
                                        $("input#ALAMAT_PEMOHONA").val(datashow[0].ALAMAT_PEMOHON);
                                    }
                                }
                            });
                        }

                        // sepuluh
                        function kolektif() {
                            var kl = $('#KD_JNS_PELAYANAN').val();
                            $('#idn').html('');
                            if (kl != '01') {
                                $('#demo').html('<div class="form-group">' +
                                    '<label class="col-md-2  control-label">Scan Barcode </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text"   class="form-control" name="barcode" id="barcodea" > ' +
                                    '<span id="test"></span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">NOP Kolektif</label>' +
                                    '<div class="col-md-1 dua">' +
                                    '<input type="text" maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHONA" id="KD_PROPINSI_PEMOHONA" value="35">' +
                                    '</div>' +
                                    '<div class="col-md-1 dua">' +
                                    '<input type="text" maxLength="2" class="form-control" name="KD_DATI2_PEMOHONA" id="KD_DATI2_PEMOHONA" value="07">' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text" maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHONA" id="KD_KECAMATAN_PEMOHONA" onfocus="this.value=(null)" >' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text" maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHONA" id="KD_KELURAHAN_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text"  maxLength="3" class="form-control" name="KD_BLOK_PEMOHONA" id="KD_BLOK_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 empat">' +
                                    '<input type="text" maxLength="4" class="form-control" name="NO_URUT_PEMOHONA" id="NO_URUT_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 satu">' +
                                    '<input type="text" maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHONA" id="KD_JNS_OP_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">Nama Pemohon </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text"   class="form-control"   name="NAMA_PEMOHONA" id="NAMA_PEMOHONA" placeholder="Nama pemohon kolektif"  />' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text" class="form-control"   name="ALAMAT_PEMOHONA" id="ALAMAT_PEMOHONA" placeholder="Alamat"  />' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<div class="col-md-8 col-md-offset-2">' +
                                    '<button class="btn btn-success btn-xs" disabled="" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</button>' +
                                    '<input id="urt" class="s" type="hidden" value="1">' +
                                    '<table class="table table-bordered">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>NO</th>' +
                                    '<th>NOP</th>' +
                                    '<th>NAMA WP</th>' +
                                    '<th>Alamat WP</th>' +
                                    '<th>Jenis Layanan</th>' +
                                    '<th>Aksi</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody id="kontentdiag"></tbody>' +
                                    '</table>' +
                                    '</div>' +
                                    '</div>');
                            } else {
                                $('#demo').html('<div class="form-group">' +
                                    '<label class="col-md-2  control-label">Scan Barcode </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text"   class="form-control" name="barcode" id="barcodea" > ' +
                                    '<span id="test"></span>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">NOP Kolektif</label>' +
                                    '<div class="col-md-1 dua">' +
                                    '<input type="text" maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHONA" id="KD_PROPINSI_PEMOHONA" value="35">' +
                                    '</div>' +
                                    '<div class="col-md-1 dua">' +
                                    '<input type="text" maxLength="2" class="form-control" name="KD_DATI2_PEMOHONA" id="KD_DATI2_PEMOHONA" value="07">' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text" maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHONA" id="KD_KECAMATAN_PEMOHONA" onfocus="this.value=(null)" >' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text" maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHONA" id="KD_KELURAHAN_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 tiga">' +
                                    '<input type="text"  maxLength="3" class="form-control" name="KD_BLOK_PEMOHONA" id="KD_BLOK_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 empat">' +
                                    '<input type="text" maxLength="4" class="form-control" name="NO_URUT_PEMOHONA" id="NO_URUT_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '<div class="col-md-1 satu">' +
                                    '<input type="text" maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHONA" id="KD_JNS_OP_PEMOHONA" onfocus="this.value=(null)">' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">Nama Pemohon </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text"   class="form-control"   name="NAMA_PEMOHONA" id="NAMA_PEMOHONA" placeholder="Nama pemohon kolektif"  />' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                                    '<div class="col-md-10">' +
                                    '<input type="text" class="form-control"   name="ALAMAT_PEMOHONA" id="ALAMAT_PEMOHONA" placeholder="Alamat"  />' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="form-group">' +
                                    '<div class="col-md-8 col-md-offset-2">' +
                                    '<button class="btn btn-success btn-xs"  id="tambahdiag"><i class="fa fa-plus"></i> Tambah</button>' +
                                    '<input id="urt" class="s" type="hidden" value="1">' +
                                    '<table class="table table-bordered">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>NO</th>' +
                                    '<th>NOP</th>' +
                                    '<th>NAMA WP</th>' +
                                    '<th>Alamat WP</th>' +
                                    '<th>Jenis Layanan</th>' +
                                    '<th>Aksi</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody id="kontentdiag"></tbody>' +
                                    '</table>' +
                                    '</div>' +
                                    '</div>');
                            }
                        }

                        // sebelas
                        function individu() {
                            $('#demo').html('');
                            $('#idn').html('<div class="row">' +
                                '<div class="col-md-12">' +
                                '<div class="form-group">' +
                                '<label class="col-md-2  control-label">Scan Barcode </label>' +
                                '<div class="col-md-10">' +
                                '<input type="text"   class="form-control" name="barcode" id="barcode" >  ' +
                                '<span id="test"></span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<label class="col-md-2 control-label">NOP </label>' +
                                '<div class="col-md-1 dua"><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" value="35"></div>' +
                                '<div class="col-md-1 dua"><input type="text" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" value="07"></div>' +
                                '<div class="col-md-1 tiga"><input type="text"  maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=(null)"></div>' +
                                '<div class="col-md-1 tiga"><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=(null)"></div>' +
                                '<div class="col-md-1 tiga"><input type="text" maxlength="3"  class="form-control" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=(null)"></div>' +
                                '<div class="col-md-1 empat"><input type="text" maxlength="4" class="form-control" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=(null)"></div>' +
                                '<div class="col-md-1 satu"><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=(null)"></div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<label class="col-md-2  control-label">Nama Pemohon </label>' +
                                '<div class="col-md-10">' +
                                '<input type="text"   class="form-control" required name="NAMA_PEMOHON" id="NAMA_PEMOHON" placeholder="Nama pemohon" value="<?php echo $NAMA_PEMOHON; ?>" />' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<label class="col-md-2 control-label">Alamat Pemohon  </label>' +
                                '<div class="col-md-10">' +
                                '<input type="text" class="form-control" required name="ALAMAT_PEMOHON" id="ALAMAT_PEMOHON" placeholder="Alamat" value="<?php echo $ALAMAT_PEMOHON; ?>" />' +
                                '</div>' +
                                '</div></div></div></div>');

                        }
             */
            // duabelas


            // tigabelas
            function deskripsi(nop) {
                $.ajax({
                    url: "<?php echo base_url() . 'permohonan/deskripsialert'; ?>",
                    method: "POST",
                    data: "nop=" + nop,
                    cache: false,
                    success: function(response) {
                        alert(response);
                        $("#bayar").html('');
                    }
                });
            }

            // empat belas
            /* $('#KD_JNS_OPC,#NO_URUTC,#KD_BLOKC,#KD_KELURAHANC,#KD_KECAMATANC,#KD_DATI2C,#KD_PROPINSIC').on('change',
                function() {
                    var a = $('#KD_PROPINSIC').val();
                    var b = $('#KD_DATI2C').val();
                    var c = $('#KD_KECAMATANC').val();
                    var d = $('#KD_KELURAHANC').val();
                    var e = $('#KD_BLOKC').val();
                    var f = $('#NO_URUTC').val();
                    var g = $('#KD_JNS_OPC').val();
                    var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g;
                    // alert(nop);
                    $.ajax({
                        url: "<?php echo base_url() . 'permohonan/getNopsknjop'; ?>",
                        method: "POST",
                        data: "nop=" + nop,
                        cache: false,
                        success: function(response) {

                            var datashow = JSON.parse(response);
                            $("input#NAMA_PEMOHONC").val(datashow[0].NAMA_PEMOHON);
                            $("input#ALAMAT_PEMOHONC").val(datashow[0].ALAMAT_PEMOHON);
                        }
                    });
                }); */

            // lima belas
            $('#NO_URUT_PELAYANAN').on('change', function() {

                var a = $('#THN_PELAYANAN').val();
                var b = $('#BUNDEL_PELAYANAN').val();
                var c = $('#NO_URUT_PELAYANAN').val();
                var no = a + "." + b + "." + c;
                // alert(no);

                $.ajax({
                    url: "<?php echo base_url() . 'permohonan/ceknomerlayanan'; ?>",
                    method: "POST",
                    data: "no=" + no,
                    cache: false,
                    success: function(hasil) {
                        var asd = hasil.split("|");
                        if (asd[0] == 1) {
                            alert("Nomer layanan " + asd[1] + " Sudah terdaftar !");
                            $('#THN_PELAYANAN').val(null);
                            $('#BUNDEL_PELAYANAN').val(null);
                            $('#NO_URUT_PELAYANAN').val(null);
                        }
                    }
                });
            });

            // enam belas
            $('#barcode').focus();
            $('body').on('keypress', 'input, select, textarea', function(e) {
                var self = $(this),
                    form = self.parents('form:eq(0)'),
                    focusable, next;
                if (e.keyCode == 13) {
                    focusable = form.find('input,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {
                        form.submit();
                    }
                    return false;

                }
            });

            // tujuh belas
            $(".form-control").keyup(function() {
                if (this.value.length == this.maxLength) {
                    var nextIndex = $('input:text').index(this) + 1;
                    $('input:text')[nextIndex].focus();
                }
            });

            // delapan belas
            //cek lampiran
            /* $("#KD_JNS_PELAYANAN").change(function(e) {
                e.preventDefault();
                var KD_JNS_PELAYANAN = $('#KD_JNS_PELAYANAN').val();

                if (KD_JNS_PELAYANAN == '01') {
                    $('#myBtn').prop('disabled', false);
                    $('#tambahdiag').prop('disabled', false);
                } else {
                    $('#myBtn').prop('disabled', true);
                    $('#tambahdiag').prop('disabled', true);
                }
                $.ajax({
                    url: "<?php echo base_url() . 'permohonan/cekmapiinglampiran'; ?>",
                    method: "POST",
                    data: "KD_JNS_PELAYANAN=" + KD_JNS_PELAYANAN,
                    cache: false,
                    success: function(hasil) {
                        $("#lampiran").html(hasil);
                    }
                });
                if (KD_JNS_PELAYANAN == 01) {
                    document.getElementById("jml_berkas").innerHTML = '' +
                        '<label class="col-md-2 control-label">Jumlah Berkas</label>' +
                        '<div class="col-md-10">' +
                        '<input type="text" class="form-control" required name="JUMLAH_BERKAS" />' +
                        '</div>';
                } else {
                    document.getElementById("jml_berkas").innerHTML = '';
                };
            }); */

            // sembilan belas
            // cek bayar
            /*             $("#KD_JNS_PELAYANAN,#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON")
                            .on('keyup change', function() {
                                var kd_layanan = $('#KD_JNS_PELAYANAN').val();
                                var a = $('#KD_PROPINSI_PEMOHON').val();
                                var b = $('#KD_DATI2_PEMOHON').val();
                                var c = $('#KD_KECAMATAN_PEMOHON').val();
                                var d = $('#KD_KELURAHAN_PEMOHON').val();
                                var e = $('#KD_BLOK_PEMOHON').val();
                                var f = $('#NO_URUT_PEMOHON').val();
                                var g = $('#KD_JNS_OP_PEMOHON').val();
                                var nop = a + "." + b + "." + c + "." + d + "." + e + "." + f + "." + g;
                                // if(kd_layanan=='02'){
                                $.ajax({
                                    url: "<?php echo base_url() . 'permohonan/cekriwayatbayar'; ?>",
                                    method: "POST",
                                    data: "var=" + kd_layanan + '_' + nop,
                                    cache: false,
                                    success: function(hasil) {
                                        $("#bayar").html(hasil);

                                    }
                                });
                            });
             */
            // dua puluh
            $(".input_tgl").keyup(function() {
                if (this.value.length == this.maxLength) {
                    $(this).next('.inputs').focus();
                }
            });

            // dua puluh satu
            // barcodea
            $("#barcode").keyup(function() {
                var barcode = $("#barcode").val();
                getnopdecode(barcode);
            });

            $("#barcode").keydown(function() {
                var barcode = $("#barcode").val();
                getnopdecode(barcode);
            });
        });

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) {
                return false;
            }
        }
        document.onkeypress = stopRKey;
    </script>
@endsection
