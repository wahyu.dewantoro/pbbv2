@extends('page.master')
@section('judul')
<h1>
    Exception NOP Dalam layanan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>

    <li class="active">Exception NOP Dalam layanan</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class="form-inline" role="form" method="post" action="<?= base_url() . 'permohonan/exceptionproses' ?>">
            <div class="form-group">
                NOP
            </div>
            <div class="form-group">
                <input type="text" onkeyup="formatnop(this)" autofocus="true" name="NOP" class="form-control" id="NOP" placeholder="" autofocus="true" required>
            </div>
            <div class="form-group">
                <input type="text" name="TAHUN" class="form-control" placeholder="Tahun Pajak" required>
            </div>
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
        </form>
        <form method="post" action="<?= base_url() . 'master/bukablokirmasal' ?>">
            <div class="mailbox-messages">

                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th>NOP</th>
                            <th>Tahun</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $rk) { ?>
                            <tr>
                                <td align="center"><?= $no; ?></td>
                                <td>
                                    <?php
                                    echo    $rk->KD_PROPINSI . '.' .
                                        $rk->KD_DATI2 . '.' .
                                        $rk->KD_KECAMATAN . '.' .
                                        $rk->KD_KELURAHAN . '.' .
                                        $rk->KD_BLOK . '.' .
                                        $rk->NO_URUT . '.' .
                                        $rk->KD_JNS_OP
                                    ?>
                                </td>
                                <td><?= $rk->THN_PAJAK_SPPT ?></td>
                                <td align="center">
                                    <a onclick="return confirm('Apakah anda yakin?')" href="<?php echo base_url() . 'permohonan/deleteexception?tahun='.trim($rk->THN_PAJAK_SPPT).'&nop=' . urlencode(trim($rk->KD_PROPINSI).'.'.trim($rk->KD_DATI2).'.'.trim($rk->KD_KECAMATAN).'.'.trim($rk->KD_KELURAHAN).'.'.trim($rk->KD_BLOK).'.'.trim($rk->NO_URUT).'.'.trim($rk->KD_JNS_OP)) ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>plugins/iCheck/flat/blue.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= base_url('lte/') ?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
    function formatnop(objek) {

        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 2) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 2 && panjang <= 4) {
            // 07. -> 2,2
            c = b.substr(0, 2) + '.' + b.substr(2, 2);
        } else if (panjang > 4 && panjang <= 7) {
            // 123 -> 4,3
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
        } else if (panjang > 7 && panjang <= 10) {
            // .123. ->
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
        } else if (panjang > 10 && panjang <= 13) {
            // 123.
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
        } else if (panjang > 13 && panjang <= 17) {
            // 1234
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
        } else {
            // .0
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
            // alert(panjang);
        }
        objek.value = c;
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example1').DataTable();
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function() {
            var clicks = $(this).data('clicks');
            if (clicks) {
                // alert('a');

                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                // alert('b');
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });
    });
</script>

@endsection