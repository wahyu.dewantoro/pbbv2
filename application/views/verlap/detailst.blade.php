@extends('page.master')
@section('judul')
<h1>
    {{ $title }}
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Informasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Kode / Tanggal</strong>
                <p class="text-muted">
                    <?= $surat->KODE ?> / <?= $surat->TGL_SURAT ?>
                </p>

                <hr>
                <strong><i class="fa fa-user margin-r-5"></i> Pegawai</strong>
                <p class="text-muted">
                    <?= $surat->NAMA_PEGAWAI ?>
                </p>
                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Lokasi</strong>
                <p class="text-muted"><?= $surat->KECAMATAN ?></p>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-8">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Objek</h3>
                <div class="pull-right">
                    <a target="_blank" class="btn btn-sm btn-info text-white" href="<?= base_url('verlap/cetakst/'.$surat->ID)?>"> <i class="fa fa-print"></i> Cetak Surat Tugas</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="">
                <table class="table table-bordered " id="table_objek">
                    <thead>
                        <tr>
                            <th>Nopel</th>
                            <th>NOP</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($objek as $obj) {
                            $id = $obj->THN_PELAYANAN . $obj->BUNDEL_PELAYANAN . $obj->NO_URUT_PELAYANAN . $obj->KD_PROPINSI . $obj->KD_DATI2 . $obj->KD_KECAMATAN . $obj->KD_KELURAHAN . $obj->KD_BLOK . $obj->NO_URUT . $obj->KD_JNS_OP;
                        ?>
                            <tr id='row<?= $no; ?>' class="<?= $id ?>">
                                <td><?= $obj->THN_PELAYANAN . '.' . $obj->BUNDEL_PELAYANAN . '.' . $obj->NO_URUT_PELAYANAN ?></td>
                                <td><?= $obj->KD_PROPINSI . '.' . $obj->KD_DATI2 . '.' . $obj->KD_KECAMATAN . '.' . $obj->KD_KELURAHAN . '.' . $obj->KD_BLOK . '.' . $obj->NO_URUT . '.' . $obj->KD_JNS_OP ?></td>
                                <td><?= $obj->KELURAHAN . ' - ' . $obj->KECAMATAN ?></td>
                            </tr>
                        <?php
                            $no++;
                        } ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->

@endsection
@section('script')

@endsection