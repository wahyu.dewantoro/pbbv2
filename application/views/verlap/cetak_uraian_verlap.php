<html>

<head>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* sheet-size: A4-L; */
        }

        p {
            margin: 5px;
        }

        @page {
            /* sheet-size: A4; */
            margin-top: 10mm;
            margin-bottom: 10mm;
            margin-left: 10mm;
            margin-right: 10mm;
        }

        .table {
            border-collapse: collapse;
            border: 1px solid black;
            padding: 5px;
        }

        .table tr td {
            border-collapse: collapse;
            border: 1px solid black;
        }


        .tengah {
            text-align: center;
        }

        .kanankiri {
            text-align: justify;
        }

        td.heading {
            vertical-align: middle;

        }

        th,
        td {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
            vertical-align: top;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* page-break-after works, as well */
        }
    </style>
    <title><?= $title ?></title>
</head>

<body>
    <p class="tengah"><b>URAIAN HASIL PENELITIAN LAPANGAN</b></p>
    <table class="table" width="100%" padding=5>

        <tr style="vertical-align:">
            <td class='heading' align="center" rowspan="2">No</td>
            <td class='heading' align="center" rowspan="2">Kec</td>
            <td class='heading' align="center" rowspan="2">Desa</td>
            <td class='heading' align="center" rowspan="2">No Berkas</td>
            <td class='heading' align="center" rowspan="2" width="100px">NOP</td>
            <td class='heading' align="center" colspan="11">Hasil Penelitian</td>

            <td class='heading' align="center" rowspan="2">Keterangan</td>
        </tr>
        <tr style="vertical-align:">

            <td width="50px" class="heading" align="center">Nama WP</td>
            <td width="50px" class="heading" align="center">Letak OP</td>
            <td width="50px" class="heading" align="center">Data Baru</td>
            <td width="50px" class="heading" align="center">Mutasi Penuh</td>
            <td width="50px" class="heading" align="center">Pecah</td>
            <td width="50px" class="heading" align="center">Pengga bungan</td>
            <td width="50px" class="heading" align="center">Pengu rangan</td>
            <td width="50px" class="heading" align="center">Pembe tulan</td>
            <td width="50px" class="heading" align="center">ZNT</td>
            <td width="50px" class="heading" align="center">Luas Tanah</td>
            <td width="50px" class="heading" align="center">Luas BNG</td>
        </tr>

        <tr style="vertical-align:">
            <?php for ($i = 1; $i <= 17; $i++) { ?>
                <td class="heading" align="center"><?= $i ?></td>
            <?php } ?>
        </tr>

        <?php $nomer = 1;
        $nomer_berkas = "";
        foreach ($objek as $index => $obj) {
            $pecah = JoinTable('surat_tugas_verlap', ['surat_tugas_objek_id' => $obj->ID])->result_array();

            if (count($pecah) > 1) {
                $jns = 'Mutasi Pecah';
            } else if (count($pecah) == 1 && $pecah[0]['NM_WP'] == '') {
                $jns = 'pembatalan';
            } else {
                $jns = 'Mutasi Penuh';
            }

        ?>
            <tr>
                <td align="center"><?= $nomer ?></td>
                <td><?= $obj->KECAMATAN ?></td>
                <td><?= $obj->KELURAHAN ?></td>
                <td align="center">
                    <?php
                    $nb = JoinTable('pelayanan_verifikasi', [
                        'thn_pelayanan' => $obj->THN_PELAYANAN,
                        'bundel_pelayanan' => $obj->BUNDEL_PELAYANAN,
                        'no_urut_pelayanan' => $obj->NO_URUT_PELAYANAN
                    ], 'NO_PERMOHONAN')->row();
                    if ($nb) {

                        if ($nomer_berkas <> $nb->NO_PERMOHONAN) {
                            echo $nb->NO_PERMOHONAN;
                            $nomer_berkas = $nb->NO_PERMOHONAN;
                        }
                    }
                    ?>
                </td>
                <td></td>
                <td align="center"><?= $pecah[0]['NM_WP'] <> '' ? 'v' : ''; ?></td> <!-- 6-->
                <td align="center"><?= $pecah[0]['NM_WP'] <> '' ? 'v' : ''; ?></td> <!-- 7-->
                <td>
                    <?php
                    $opl =
                        $obj->KD_KECAMATAN . '.' .
                        $obj->KD_KELURAHAN . '.' .
                        $obj->KD_BLOK . '.' .
                        $obj->NO_URUT . '.' .
                        $obj->KD_JNS_OP;

                    $opb = $pecah[0]['KD_KECAMATAN'] . '.' . $pecah[0]['KD_KELURAHAN'] . '.' . $pecah[0]['KD_BLOK'] . '.' . $pecah[0]['NO_URUT'] . '.' . $pecah[0]['KD_JNS_OP'];

                    $opb <> $opl ? 'v' : '';

                    if (count($pecah) == 1) {
                        $opb <> $opl ? 'v' : '';
                    }

                    ?>

                </td> <!-- 8 -->
                <td align="center">
                    <?php if (count($pecah) == 1 && $opl == $opb && $pecah[0]['NM_WP'] <> '') { ?>
                        v
                    <?php } ?>
                </td> <!-- 9-->
                <td align="center">
                    <?php if (count($pecah) > 1) { ?>
                        v
                    <?php } ?>
                </td> <!-- 10-->
                <td align="center">
                    <?php if (count($pecah) == 1 && $opl == $opb && $pecah[0]['NM_WP'] == '') { ?>
                        v
                    <?php } ?>
                </td>
                <td></td>
                <td></td>
                <td align="center">v</td>
                <td align="center"><?= $pecah[0]['L_BUMI'] > 0 ? 'v' : '';  ?></td>
                <td align="center"><?= $pecah[0]['L_BNG'] > 0 ? 'v' : '';  ?></td>
                <td align="center"><?= $jns ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td><?=
                    $obj->THN_PELAYANAN . '.' .
                        $obj->BUNDEL_PELAYANAN . '.' .
                        $obj->NO_URUT_PELAYANAN
                    ?></td>
                <td></td>
                <td align="center"><?=
                                    $pecah[0]['KD_BLOK'] . '.' .
                                        $pecah[0]['NO_URUT'] . '.' .
                                        $pecah[0]['KD_JNS_OP']
                                    ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="3" class="heading" align="center"><?= $pecah[0]['KETERANGAN'] ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
            <?php
            foreach ($pecah as $kp => $pch) {
                if ($kp != 0) { ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center"><?= $jns ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center"><?=
                                            $pch['KD_BLOK'] . '.' .
                                                $pch['NO_URUT'] . '.' .
                                                $pch['KD_JNS_OP']
                                            ?></td>
                        <td align="center"><?= $pch['NM_WP'] <> '' ? 'v' : ''; ?></td> <!-- 6-->
                        <td align="center"><?= $pch['NM_WP'] <> '' ? 'v' : ''; ?></td> <!-- 7-->
                        <td>
                            <?php
                            $opl =
                                $obj->KD_KECAMATAN . '.' .
                                $obj->KD_KELURAHAN . '.' .
                                $obj->KD_BLOK . '.' .
                                $obj->NO_URUT . '.' .
                                $obj->KD_JNS_OP;

                            $opb = $pch['KD_KECAMATAN'] . '.' . $pch['KD_KELURAHAN'] . '.' . $pch['KD_BLOK'] . '.' . $pch['NO_URUT'] . '.' . $pch['KD_JNS_OP'];

                            $opb <> $opl ? 'v' : '';

                            if (count($pecah) == 1) {
                                $opb <> $opl ? 'v' : '';
                            }

                            ?>

                        </td> <!-- 8 -->
                        <td align="center">
                            <?php if (count($pecah) == 1 && $opl == $opb) { ?>
                                v
                            <?php } ?>
                        </td> <!-- 9-->
                        <td align="center">
                            <?php if (count($pecah) > 1) { ?>
                                v
                            <?php } ?>
                        </td> <!-- 10-->
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center">v</td>
                        <td align="center"><?= $pch['L_BUMI'] > 0 ? 'v' : '';  ?></td>
                        <td align="center"><?= $pch['L_BNG'] > 0 ? 'v' : '';  ?></td>
                        <td rowspan="3" class="heading" align="center"><?= $pch['KETERANGAN'] ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

        <?php
                }
            }
            $nomer++;
        } ?>
    </table>

    <table width="100%">
        <tr>
            <td width="30%" align="center">

                Kasubid Pendataan dan Penilaian<br><br><br><br><br>

                <u>Budi Kurniawan, SE</u>
            </td>
            <td></td>
            <td width="30%" align="center">
                <?= $surat->TGL_SURAT ?><br>
                Yang Membuat<br><br><br><br>

                <?= $surat->NAMA_PEGAWAI ?>
            </td>
        </tr>
    </table>
</body>

</html>