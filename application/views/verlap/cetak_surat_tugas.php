<html>

<head>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* page-break-after works, as well */
        }

        p {
            margin: 5px;
        }

        @page {
            margin-top: 10mm;
            margin-bottom: 10mm;
            margin-left: 10mm;
            margin-right: 10mm;
        }

        .tengah {
            text-align: center;
        }

        .kanankiri {
            text-align: justify;
        }

        th,
        td {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
            vertical-align: top;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* page-break-after works, as well */
        }
    </style>
    <title><?= $surat->KODE ?></title>
</head>

<body>

    <table width="100%">
        <tbody>
            <tr>
                <td rowspan="3"><img width="60px" src="<?= base_url('malang.png') ?>"></td>
                <td align="center">PEMERINTAH KABUPATEN MALANG</td>
            </tr>
            <tr>
                <td align="center"><b>BADAN PENDAPATAN DAERAH</b></td>
            </tr>
            <tr>
                <td align="center">JL. K.H. AGUS SALIM 7 TELEPON (0341)367994 FAX (0341)355708<br> MALANG - 65119</td>
            </tr>
        </tbody>
    </table>
    <hr style="margin: 5px;">
    <p class="tengah"><b>SURAT PERINTAH TUGAS</b>
        <br>
        Nomor : 800 / ____ / 35 07 205 / <?= substr($surat->TGL_SURAT, -4) ?>
    </p>

    <ol>
        <li>Peraturan Daerah Kabupaten Malang Nomor 9 Tahun 2019 tentang Anggaran Pendapatan dan Belanja Daerah Kabupaten Malang Tahun Anggaran 2020</li>
        <li>Peraturan Bupati Malang Nomor 228 Tahun 2019 tentang Penjabaran Anggaran Pendapatan dan Belanja Daerah Kabupaten Malang Tahun Anggaran 2020</li>
        <li>Dokumen pelaksanaan perubahan Anggaran (DPPA) Badan Pendapatan Daerah Kabupaten Malang Nomor 930/281/DPPA/35 07 204/2020 Tanggal 02 Juni 2020</li>
    </ol>
    <p class="tengah"><b>MEMERINTAHKAN</b>
    <table width=100%>
        <tbody>
            <tr>
                <td width="200px">Nama</td>
                <td width="1px"></td>
                <td><?= $surat->NAMA_PEGAWAI ?></td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>:</td>
                <td><?= $surat->NIP_PEGAWAI ?></td>
            </tr>
            <tr>
                <td>Pangkat / Golongan</td>
                <td>:</td>
                <td><?= $surat->PANGKAT_GOLONGAN ?></td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td><?= $surat->JABATAN_PEGAWAI ?></td>
            </tr>
        </tbody>
    </table>
    <p class="kanankiri">Penelitian lapangan atas pengajuan wajib pajak di Kecamatan <b><?= ucwords(strtolower($surat->KECAMATAN)) ?></b> pada tanggal <?= $surat->TGL_SURAT ?></p>
    <p class="kanankiri">sesuai prosedur, setelah melaksanakan kegiatan dimaksud agar melaporkan hasilnya kepada pimpinan.</p>
    <p class="kanankiri">Demikian untuk dilaksanakan dengan penuh tanggung jawab.</p>

    <table border="0" width="100%">
        <tr>
            <td width="30%"></td>
            <td></td>
            <td width="40%" align="center">
                <table>
                    <tr>
                        <td style="text-align: left;">Dikeluarkan di</td>
                        <td width="1px">:</td>
                        <td style="text-align: left;">Malang</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Tanggal</td>
                        <td style="text-align: left;">:</td>
                        <td style="text-align: left;"><?= $surat->TGL_SURAT ?></td>
                    </tr>
                </table>
                <hr>
                <p class="tengah">
                    a.n. KEPALA BADAN PENDAPATAN DAERAH<br>
                    KABUPATEN MALANG<br>
                    Sekretaris<br>
                    u.b.<br>
                    Plh Kepala Bidang PBB p2<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b><u><?= $surat->NAMA_KABID ?></u></b><br>
                    <?= $surat->PANGKAT_GOLONGAN_KABID ?><br>
                    NIP. <?= $surat->NIP_KABID ?>
                </p>
            </td>
        </tr>
    </table>
    <div class="pagebreak"></div>
    <b>Daftar Lampiran Objek</b>
    <table width="100%" style="border-collapse: collapse;  border: 1px solid black; padding: 5px;">
        <thead>
            <tr>
                <th style="  border: 1px solid black;">No</th>
                <th style="  border: 1px solid black;">Nopel</th>
                <th style="  border: 1px solid black;">NOP</th>
                <th style="  border: 1px solid black;">Alamat</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            foreach ($objek as $obj) {
                $id = $obj->THN_PELAYANAN . $obj->BUNDEL_PELAYANAN . $obj->NO_URUT_PELAYANAN . $obj->KD_PROPINSI . $obj->KD_DATI2 . $obj->KD_KECAMATAN . $obj->KD_KELURAHAN . $obj->KD_BLOK . $obj->NO_URUT . $obj->KD_JNS_OP;
            ?>
                <tr id='row<?= $no; ?>' class="<?= $id ?>">
                    <td style="  border: 1px solid black; text-align:center;"> <?= $no ?></td>
                    <td style="  border: 1px solid black;"><?= $obj->THN_PELAYANAN . '.' . $obj->BUNDEL_PELAYANAN . '.' . $obj->NO_URUT_PELAYANAN ?></td>
                    <td style="  border: 1px solid black;"><?= $obj->KD_PROPINSI . '.' . $obj->KD_DATI2 . '.' . $obj->KD_KECAMATAN . '.' . $obj->KD_KELURAHAN . '.' . $obj->KD_BLOK . '.' . $obj->NO_URUT . '.' . $obj->KD_JNS_OP ?></td>
                    <td style="  border: 1px solid black;"><?= $obj->KELURAHAN . ' - ' . $obj->KECAMATAN ?></td>
                </tr>
            <?php
                $no++;
            } ?>

        </tbody>

    </table>
</body>

</html>