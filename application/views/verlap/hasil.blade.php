@extends('page.master')
@section('judul')
<h1>
    Hasil Verifikasi Lapangan
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example2">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Kode</th>
                            <th>Pegawai</th>
                            <th>Kecamatan</th>
                            <th width="8%">Aksi</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };


        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#example2 input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: false,
            ordering: false,
            ajax: {
                "url": "<?php echo base_url(); ?>verlap/json_hasil_verlap",
                "type": "post",
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    "data": "KODE",
                    render: function(data, type, row) {
                        return data + "<br><span class='text-muted'>" + row.TGL_SURAT + "</span>";
                    }
                },
                {
                    "data": "NAMA_PEGAWAI",

                },
                {
                    "data": "KECAMATAN",
                    render: function(data, type, row) {
                        return data + "<br><span class='text-muted'>" + row.OBJEK + " Objek</span>";
                    }
                },
                {
                    "data": "ID",
                    render: function(data, type, row) {
                        this.url = '<?php echo base_url(); ?>verlap/verlapdt/' + data;
                        var d = '<a  href="' + this.url + '" class="btn btn-xs btn-info " ><i class="fa fa-search"></i></a>';

                        url_e = '<?php echo base_url(); ?>verlap/print_uraian/' + data;
                        var e = '<a  targte="_blank" href="' + url_e + '" class="btn btn-xs btn-success " ><i class="fa fa-print"></i></a>';
                        return e + '' + d;
                    }
                },
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
@endsection