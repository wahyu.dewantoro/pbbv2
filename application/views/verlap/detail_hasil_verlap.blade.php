@extends('page.master')
@section('judul')
<h1>
    {{ $title }}
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Objek</h3>
                <div class="pull-right">
                    <a target="_blank" href="<?= base_url('verlap/print_uraian/' . $surat->ID) ?>" class="btn btn-sm btn-info">Cetak Uraian</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="">
                <table class="table table-bordered " id="table_objek">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th style="text-align:center" colspan="4">Data Lama</th>
                            <th style="text-align:center" colspan="5">Data Baru</th>
                        </tr>
                        <tr>
                            <th style="text-align:center">NOP</th>
                            <th style="text-align:center">Nama WP</th>
                            <th style="text-align:center">L. Tanah</th>
                            <th style="text-align:center">L. Bangunan</th>
                            <th style="text-align:center">NOP</th>
                            <th style="text-align:center">Nama WP</th>
                            <th style="text-align:center">L. Tanah</th>
                            <th style="text-align:center">L. Bangunan</th>
                            <th style="text-align:center">Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($objek as $obj) {
                            $dop = JoinTable(
                                'DAT_OBJEK_PAJAK',
                                [
                                    'KD_PROPINSI' => $obj->KD_PROPINSI,
                                    'KD_DATI2' => $obj->KD_DATI2,
                                    'KD_KECAMATAN' => $obj->KD_KECAMATAN,
                                    'KD_KELURAHAN' => $obj->KD_KELURAHAN,
                                    'KD_BLOK' => $obj->KD_BLOK,
                                    'NO_URUT' => $obj->NO_URUT,
                                    'KD_JNS_OP' => $obj->KD_JNS_OP,
                                ],
                                'TOTAL_LUAS_BUMI,TOTAL_LUAS_BNG,SUBJEK_PAJAK_ID'
                            )->row();

                            $pecah = JoinTable('surat_tugas_verlap', ['surat_tugas_objek_id' => $obj->ID])->result_array();

                        ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $obj->KD_PROPINSI . '.' . $obj->KD_DATI2 . '.' . $obj->KD_KECAMATAN . '.' . $obj->KD_KELURAHAN . '.' . $obj->KD_BLOK . '.' . $obj->NO_URUT . '.' . $obj->KD_JNS_OP ?></td>
                                <td><?= JoinTable('DAT_SUBJEK_PAJAK', ['SUBJEK_PAJAK_ID' => trim($dop->SUBJEK_PAJAK_ID)], 'NM_WP')->row()->NM_WP  ?></td>
                                <td><?php echo
                                    $dop->TOTAL_LUAS_BUMI
                                    ?></td>
                                <td><?php
                                    echo $dop->TOTAL_LUAS_BNG
                                    ?></td>

                                <td>
                                    <?=
                                    $pecah[0]['KD_PROPINSI'] . '.' .
                                        $pecah[0]['KD_DATI2'] . '.' .
                                        $pecah[0]['KD_KECAMATAN'] . '.' .
                                        $pecah[0]['KD_KELURAHAN'] . '.' .
                                        $pecah[0]['KD_BLOK'] . '.' .
                                        $pecah[0]['NO_URUT'] . '.' .
                                        $pecah[0]['KD_JNS_OP']
                                    ?>
                                </td>
                                <td>
                                    <?= $pecah[0]['NM_WP'] ?>
                                </td>
                                <td>
                                    <?= $pecah[0]['L_BUMI'] ?>
                                </td>
                                <td>
                                    <?= $pecah[0]['L_BNG'] ?>
                                </td>
                                <td>
                                    <?= $pecah[0]['KETERANGAN'] ?>
                                </td>

                            </tr>
                            <?php foreach ($pecah as $kp => $pch) {
                                if ($kp != 0) { ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <?=
                                            $pch['KD_PROPINSI'] . '.' .
                                                $pch['KD_DATI2'] . '.' .
                                                $pch['KD_KECAMATAN'] . '.' .
                                                $pch['KD_KELURAHAN'] . '.' .
                                                $pch['KD_BLOK'] . '.' .
                                                $pch['NO_URUT'] . '.' .
                                                $pch['KD_JNS_OP']
                                            ?>
                                        </td>
                                        <td>
                                            <?= $pch['NM_WP'] ?>
                                        </td>
                                        <td>
                                            <?= $pch['L_BUMI'] ?>
                                        </td>
                                        <td>
                                            <?= $pch['L_BNG'] ?>
                                        </td>
                                        <td>
                                            <?= $pch['KETERANGAN'] ?>
                                        </td>

                                    </tr>
                            <?php }
                            } ?>

                        <?php $no++;
                        } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->

@endsection
@section('script')

@endsection