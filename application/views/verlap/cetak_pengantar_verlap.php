<html>

<head>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }
            /* sheet-size: A4; */
            /* page-break-after works, as well */
        }

        p {
            margin: 5px;
        }

        @page {
            /* sheet-size: A4; */
            margin-top: 10mm;
            margin-bottom: 10mm;
            margin-left: 10mm;
            margin-right: 10mm;
        }

        .table {
            border-collapse: collapse;
            border: 1px solid black;
            padding: 5px;
        }

        .table tr td {
            border-collapse: collapse;
            border: 1px solid black;
        }


        .tengah {
            text-align: center;
        }

        .kanankiri {
            text-align: justify;
        }

        td.heading {
            vertical-align: middle;

        }

        th,
        td {
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.42857203;
            color: #333;
            background-color: #fff;
            font-weight: normal;
            font-size: 12px;
            vertical-align: top;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }

            /* page-break-after works, as well */
        }
    </style>
    <title><?= $title ?></title>
</head>

<body>
    <p class="tengah">
        <b>Laporan hasil penelitian<br>
            [jenis permohonan] pajak bumi dan bangunan perdesaan dan perkotaan<br>
            Secara [perorangan / kolektif]
        </b>
    </p>
</body>

</html>