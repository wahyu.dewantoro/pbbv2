@extends('page.master')
@section('judul')
<h1>
    {{ $title }}
    <div class="pull-right">

    </div>
</h1>
@endsection
@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Objek Penelitian</h3>
            </div>
            <div class="box-body">
                <table width="500px">
                    <tbody>
                        <tr>
                            <td with="150px">Kode / Tanggal Surat Tugas</td>
                            <td>:</td>
                            <td><?= $surat->KODE ?> / <?= $surat->TGL_SURAT ?></td>
                        </tr>
                        <tr>
                            <td>Pegawai</td>
                            <td>:</td>
                            <td><?= $surat->NAMA_PEGAWAI ?></td>
                        </tr>
                    </tbody>
                </table>
                <form action="<?= base_url('verlap/prosesentriverlap/' . $surat->ID) ?>" method="post">
                    <table class="table " id="table-hasil">
                        <thead>
                            <tr>
                                <th width="200px">NOP</th>
                                <th>Nama WP</th>
                                <th width="100px">L. Bumi (M)</th>
                                <th width="100px">L. BNG (M)</th>
                                <th>Keterangan</th>
                                <th width="80px"></th>
                            </tr>
                        </thead>


                        @foreach($objek as $objek)
                        <tbody id="tbody_{{ $objek->ID }}">
                            <tr>
                                <td>
                                    <input type="text" readonly name="nop[]" class="nop form-control form-control-sm" value=" <?= $objek->KD_PROPINSI . '.' . $objek->KD_DATI2 . '.' . $objek->KD_KECAMATAN . '.' . $objek->KD_KELURAHAN . '.' . $objek->KD_BLOK . '.' . $objek->NO_URUT . '.' . $objek->KD_JNS_OP ?>">
                                    <input type="hidden" name="SURAT_TUGAS_OBJEK_ID[]" value="{{ $objek->ID }}">
                                </td>
                                <td>
                                    <input id="nama_wp_{{ $objek->ID }}" type="text" name="nama_wp[]" class="form-control form-control-sm" value="<?= $objek->NM_WP ?>">
                                </td>
                                <td>
                                    <input id="lbumi_{{ $objek->ID }}" type="text" name="lbumi[]" class="luas form-control form-control-sm" value="<?= $objek->LUAS_BUMI ?>">
                                </td>
                                <td>
                                    <input id="lbng_{{ $objek->ID }}" type="text" name="lbng[]" class="luas form-control form-control-sm" value="<?= $objek->LUAS_BNG ?>">
                                </td>
                                <td>
                                    <input id="keterangan_{{ $objek->ID }}" type="text" required name="keterangan[]" class=" form-control form-control-sm" value="">
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button data-block="<?= $objek->KD_PROPINSI . '.' . $objek->KD_DATI2 . '.' . $objek->KD_KECAMATAN . '.' . $objek->KD_KELURAHAN . '.' . $objek->KD_BLOK . '.' ?>" type="button" id="pecah_{{ $objek->ID }}" data-id="{{ $objek->ID }}" class="btn btn-xs btn-warning pecah"><i class="fa  fa-clone"></i></button>
                                        <button data-nama="<?= $objek->NM_WP ?>" data-ltanah="<?= $objek->LUAS_BUMI ?>" data-lbng="<?= $objek->LUAS_BNG ?>" type="button" id="batal_{{ $objek->ID }}" data-id="{{ $objek->ID }}" class="btn btn-xs btn-danger batal"><i class="fa  fa-close"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach

                    </table>
                    <div class="pull-right">
                        <button class="btn btn-sm btn-success"> <i class='fa fa-save'></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->
<style>
    .borderless td,
    .borderless th {
        border: none;
    }
</style>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('.nop').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = formatnop(val_);
            $(this).val(nop);
        });
        // angka_koma
        $('.luas').on('keyup', function() {
            console.log('keyup');
            val_ = $(this).val();
            nop = angka_koma(val_);
            $(this).val(nop);
        });


        // pembatalan
        $('.batal').on('click', function() {
            // .prop("disabled", false)

            item = $(this).data('id');
            console.log('pembatalan ' + item);

            if (document.getElementById('nama_wp_' + item).disabled) {
                $('#nama_wp_' + item).prop("disabled", false).val($(this).data('nama'));
            } else {

                $('#nama_wp_' + item).prop("disabled", true).val('-');
            }

            if (document.getElementById('lbumi_' + item).disabled) {
                $('#lbumi_' + item).prop("disabled", false).val($(this).data('ltanah'));
            } else {
                $('#lbumi_' + item).val('0').prop("disabled", true);
            }

            if (document.getElementById('lbng_' + item).disabled) {
                $('#lbng_' + item).prop("disabled", false).val($(this).data('lbng'));
            } else {
                $('#lbng_' + item).prop("disabled", true).val('0');
            }

            if (document.getElementById('keterangan_' + item).disabled) {
                $('#keterangan_' + item).val('').prop("disabled", false);
            } else {
                $('#keterangan_' + item).val('-').prop("disabled", true);
            }
        });

        // pecah nop
        $('.pecah').click(function(e) {
            e.preventDefault();
            item = $(this).data('id');
            count = $('#tbody_' + item + ' tr').length;
            nop_blok = $(this).data('block');
            console.log(count);
            var tableobj = "<tr id='row_" + item + "_" + count + "' class='pecahan_" + item + "'  >\
                            <td><input type='text' required minlength='24' name='nop[]' class='nop form-control form-control-sm' value='" + nop_blok + "'>\
                            <input type='hidden' name='SURAT_TUGAS_OBJEK_ID[]' value='"+item+"'>\
                            </td>\
                            <td ><input type='text' required  name='nama_wp[]' class='form-control form-control-sm' value=''></td>\
                            <td><input required type='text' name='lbumi[]' class='luas form-control form-control-sm' value=''></td>\
                            <td><input type='text' name='lbng[]' class='luas form-control form-control-sm' value=''></td>\
                            <td><input required type='text' name='keterangan[]' class='form-control form-control-sm' value=''></td>\
                            <td><a class= 'btn btn-danger btn-xs remove flat' data-key='row_" + item + "_" + count + "' data-id='row_" + item + "_" + count + "' href='javascript:void(0)' title='Hapus'>\
                                    <i class='fa fa-trash'></i>\
                                </a></td>\
                        </tr>";
            count = count + 1;
            $('#tbody_' + item).append(tableobj);
            $('.nop').on('keyup', function() {
                console.log('keyup');
                val_ = $(this).val();
                nop = formatnop(val_);
                $(this).val(nop);
            });
            // angka_koma
            $('.luas').on('keyup', function() {
                console.log('keyup');
                val_ = $(this).val();
                nop = angka_koma(val_);
                $(this).val(nop);
            });

            $('.remove').click(function(e) {
                e.preventDefault();
                id = $(this).data('id');
                console.log(id);
                // $(this).parents(".pecahan").remove();
                $('#' + id).remove();
                /* $(this).closest('tr').css('background-color', 'transparent');
                $(this).closest('tr').find('button').attr('disabled', false); */
                // $(this).closest('tr').remove();

            });
        });






    });
</script>
@endsection