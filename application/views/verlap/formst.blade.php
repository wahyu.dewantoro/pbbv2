@extends('page.master')
@section('judul')
<h1>
    {{ $title }}
    <div class="pull-right">
        <?php anchor('verlap/surat', '<i class="fa fa-list></i> Kembali', 'class="btn btn-sm btn-info"') ?>
    </div>
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <form action="{{ $action }}" method="post">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Memerintahakan</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" value="<?= $surat->NAMA_PEGAWAI ?? '' ?>" name="nama" id="nama" class="form-control " required placeholder="Nama pegawai verlap">
                            </div>
                            <div class="form-group">
                                <label for="">NIP</label>
                                <input type="text" value="<?= $surat->NIP_PEGAWAI ?? '' ?>" name="nip" id="nip" class="form-control " required placeholder="NIP pegawai verlap">
                            </div>
                            <div class="form-group">
                                <label for="">Pangkat / Gol</label>
                                <input type="text" value="<?= $surat->PANGKAT_GOLONGAN ?? '' ?>" name="pangkat_golongan" id="pangkat_golongan" class="form-control " required placeholder="Pangkat / Golongan pegawai verlap">
                            </div>
                            <div class="form-group">
                                <label for="">Jabatan</label>
                                <input type="text" value="<?= $surat->JABATAN_PEGAWAI ?? '' ?>" name="jabatan" id="jabatan" class="form-control " required placeholder="Jabatan pegawai verlap">
                            </div>
                            <div class="pull-right">
                                <button id="Button" class="btn btn-sm btn-primary">Simpan</button>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3>
                                Objek
                                <div class="pull-right">
                                    <button type="button" class="btn btn-xs btn-success" id="tmbh" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i> Pilih objek</button>
                                </div>
                            </h3>

                            <table class="table table-bordered " id="table_objek">
                                <thead>
                                    <tr>
                                        <th>Nopel</th>
                                        <th>NOP</th>
                                        <th>ALAMAT</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $vid = "";
                                    if ($jenis == '0') {
                                    ?>
                                        <?php $no = 1;

                                        foreach ($objek as $obj) {
                                            $id = $obj->THN_PELAYANAN . $obj->BUNDEL_PELAYANAN . $obj->NO_URUT_PELAYANAN . $obj->KD_PROPINSI . $obj->KD_DATI2 . $obj->KD_KECAMATAN . $obj->KD_KELURAHAN . $obj->KD_BLOK . $obj->NO_URUT . $obj->KD_JNS_OP;
                                        ?>
                                            <tr id='row<?= $no; ?>' class="<?= $id ?>">
                                                <td><?= $obj->THN_PELAYANAN . '.' . $obj->BUNDEL_PELAYANAN . '.' . $obj->NO_URUT_PELAYANAN ?></td>
                                                <td><?= $obj->KD_PROPINSI . '.' . $obj->KD_DATI2 . '.' . $obj->KD_KECAMATAN . '.' . $obj->KD_KELURAHAN . '.' . $obj->KD_BLOK . '.' . $obj->NO_URUT . '.' . $obj->KD_JNS_OP ?></td>
                                                <td><?= $obj->KELURAHAN . ' - ' . $obj->KECAMATAN ?></td>
                                            </tr>

                                        <?php
                                            $vid .= $id . ',';
                                            $no++;
                                        } ?>
                                    <?php }

                                    $vid = substr($vid, 0, -1);

                                    ?>
                                </tbody>

                            </table>
                            <input type="hidden" id="list_objek" name="list_objek" value="<?= $vid ?>">
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Data Objek</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover" id="objekmodal" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th>Nopel</th>
                            <th>NOP</th>
                            <th>Kecamatan</th>
                            <th></th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<style>
    .dataTableLayout {
        table-layout: fixed;
        width: 100%;
    }
</style>
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        var objek = [],
            objekall = [],
            count = '';


        <?php if ($jenis == 0) { ?>
            str = $('#list_objek').val();
            exp = str.split(",");
            var i;
            for (i = 0; i < exp.length; i++) {

                objek.push(exp[i]);
            }
            console.log(objek);
        <?php } ?>

        $("#objekmodal").on("click", ".pilih", function() {
            // table_objek
            // list_objek
            count = $('#table_objek tbody tr').length;
            var tableobj = "<tr id='row" + count + "'  class='" + $(this).data('id') + "'>\
                            <td>" + $(this).data('nopel') + "</td>\
                            <td >" + $(this).data('nop') + "</td>\
                            <td>" + $(this).data('alamat') + "</td>\
                        </tr>";


            if ($(this).is(':checked')) {
                count = count + 1;
                objek.push($(this).data('id'));
                $('#list_objek').val(objek.join(','));
                $("#table_objek tbody").append(tableobj);
                $(this).closest('tr').css('background-color', '#f2dede');
            } else {
                var remove_Item = $(this).data('id');
                objek = $.grep(objek, function(value) {
                    return value != remove_Item;
                });
                $('#list_objek').val(objek);

                console.log(remove_Item);

                $("#table_objek tbody").find('.' + remove_Item).remove();
                $("#table_objek tbody").closest('tr').remove();


                $(this).closest('tr').css('background-color', 'transparent');
                count = count - 1;
            }
            objekall = objek;

        });


        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#objekmodal").dataTable({

            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            ordering: false,
            serverSide: false,
            ajax: {
                "url": "<?php echo base_url(); ?>verlap/objek_modal/<?= $jenis ?>",
                "type": "post",
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    'class': 'text-center'
                },
                {
                    data: "THN_PELAYANAN",
                    render: function(data, type, row) {
                        return row.THN_PELAYANAN + '.' + row.BUNDEL_PELAYANAN + '.' + row.NO_URUT_PELAYANAN;
                    }
                },
                {
                    "data": "KD_PROPINSI",
                    render: function(data, type, row) {
                        return row.KD_PROPINSI + '.' +
                            row.KD_DATI2 + '.' +
                            row.KD_KECAMATAN + '.' +
                            row.KD_KELURAHAN + '.' +
                            row.KD_BLOK + '.' +
                            row.NO_URUT + '.' +
                            row.KD_JNS_OP + '<br><span class="text-muted">' + row.NM_WP_SPPT + '</span>';
                    }
                },
                {
                    "data": "NAMA_KECAMATAN",
                    render: function(data, type, row) {
                        return data + '<br><span class="text-muted">' + row.NAMA_KELURAHAN + '</span>';
                    }
                },

                {
                    "data": "ID",
                    render: function(data, type, row) {

                        // 
                        return '<input type="checkbox" id="checkall" class="pilih" data-id="' + data + '"  data-alamat="' + row.NAMA_KELURAHAN + ' - ' + row.NAMA_KECAMATAN + '" data-nop="' + row.KD_PROPINSI + '.' +
                            row.KD_DATI2 + '.' +
                            row.KD_KECAMATAN + '.' +
                            row.KD_KELURAHAN + '.' +
                            row.KD_BLOK + '.' +
                            row.NO_URUT + '.' +
                            row.KD_JNS_OP + '" data-nopel="' + row.THN_PELAYANAN + '.' + row.BUNDEL_PELAYANAN + '.' + row.NO_URUT_PELAYANAN + '" >';
                    }
                },
            ],
            order: [
                [0, 'ASC']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

                for (var i = 0; i < objek.length; i++) {
                    var name = objek[i];
                    console.log(data.ID);

                    if (name == data.ID) {
                        status = 'Exist';
                        // $('td', row).closest('tr').css('display', 'none');
                        $('td', row).closest('tr').css('background-color', '#f2dede');
                        $('td', row).find('.pilih').prop('checked', true);
                        $('td', row).find('button').attr('disabled', true);
                        $('td', row).find('button').css('cursor', 'no-drop');
                        // $('#checkall').prop('checked', false);
                        break;
                    }
                }

            },

        });




    });
</script>
@endsection