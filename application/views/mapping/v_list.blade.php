@extends('page.master')
@section('judul')
<h1>
    Max NOP
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Permohonan</a></li>

    <li class="active">Max NOP</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form pencarian</h3>
            </div>
            <div class="box-body">
                <form action="<?= base_url() . 'mapping' ?>" method="GET" class="form-inline">
                    <div class="form-group">
                        <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                            <option value="">Kecamatan</option>
                            <?php foreach ($kec as $kec) { ?>
                                <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                            echo "selected";
                                        } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                            <option value="">kelurahan</option>
                            <?php
                            foreach ($kel as $kela) { ?>
                                <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                            echo "selected";
                                        } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                            <?php  } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="number" name="BLOK" id="BLOK" value="<?= $BLOK ?>" class="form-control" placeholder="BLOK">
                    </div>
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                </form>

                <?php if ($max <> '') { ?>
                    <div class="row">
                        <div class="col-md-8">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="7">List NOP</th>
                                    </tr>
                                    <tr>
                                        <th>Prop</th>
                                        <th>Dati2</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>Blok</th>
                                        <th>No Urut</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ln as $ln) { ?>
                                        <tr <?= $ln->CEK <> 0 ? '' : 'class="success"'; ?>>
                                            <td><?= $ln->PROP ?></td>
                                            <td><?= $ln->DATI2 ?></td>
                                            <td><?= $ln->KECAMATAN ?></td>
                                            <td><?= $ln->KELURAHAN ?></td>
                                            <td><?= $ln->BLOK ?></td>
                                            <td><?= $ln->NO_URUT ?></td>
                                            <td><?= $ln->CEK <> 0 ? 'Terpakai' : 'Kosong'; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-danger">
                                <?php $vnp = '35.07.' . $KD_KECAMATAN . '.' . $KD_KELURAHAN . '.' . $BLOK . '.'; ?>

                                <b>Max NOP : <?= $vnp . $max ?></b>
                            </div>
                            <div class="alert alert-success">
                                <b>NOP Baru :<?= $vnp . str_pad(((int) $max + 1), 4, "0", STR_PAD_LEFT); ?> </b>
                            </div>
                        </div>


                    </div>
                <?php } ?>

            </div><!-- /.box-body -->
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });
    });
</script>
@endsection