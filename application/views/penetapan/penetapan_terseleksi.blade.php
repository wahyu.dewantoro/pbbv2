<!-- <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" /> -->



@extends('page.master')
@section('content')

  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">PPENETAPAN TERSELEKSI</a></li>
      <!--  <li ><a href="#tab_2" data-toggle="tab">Form SK NJOP</a></li> -->
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="box box-success">
          <div class="box-header with-border">
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                  </div>
                  <div class="panel-body" style="padding:0px;border:0px;height:400px;overflow-y:auto">
                    <form class='form-horizontal'>
                      <div class="form-group">
                        <label class="col-md-6 control-label">Tahun Pajak</label>
                        <div class="col-md-6">
                          <input type="text" id="tahun" value='<?php echo date('Y') ?>' class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6 control-label">Tgl Jatuh tempo</label>
                        <div class="col-md-6">
                          <input type="text" id="" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6 control-label">Tgl Terbit</label>
                        <div class="col-md-6">
                          <input type="text" id="" class="form-control" />
                        </div>
                      </div><br>
                      <b>NOP</b>
                      <div class="form-group">
                        <label class="col-md-6  control-label">Propinsi</label>
                        <div class="col-md-6">
                          <select class="form-control chosen" id="propinsi">
                            <option value="35">35 - JAWA TIMUR </option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6  control-label">DATI II</label>
                        <div class="col-md-6">
                          <select class="form-control chosen" id="dati2">
                            <option value="07">07 - KABUPATEN MALANG </option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6  control-label">Kecamatan</label>
                        <div class="col-md-6">
                          <input type="text" id="kecamatan" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6  control-label">Kelurahan</label>
                        <div class="col-md-6">
                          <input type="text" id="kelurahan" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6  control-label">Blok</label>
                        <div class="col-md-6">
                          <input type="text" id="blok" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-6  control-label">No Urut</label>
                        <div class="col-md-6">
                          <input type="text" id="nourut" class="form-control" />
                        </div>
                      </div>
                    </form>
                    <div class="form-group">
                      <label class="col-md-6  control-label"></label>
                      <div class="col-md-6">
                        <button id="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="panel panel-default">
                  <div class="panel-heading"></div>
                  <div class="panel-body" style="padding:0;border:0px;height:437px;overflow-y:auto">
                    <table class="table table-bordered table-hover table-striped">
                      <thead>
                        <tr>
                          <th>Kelurahan</th>
                          <th>Blok</th>
                          <th>No urut</th>
                          <th>Jumlah OP</th>
                          <th>Proses</th>
                        </tr>
                      </thead>
                      <tbody id="content">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- body -->
        </div>
        </form>
      </div>
      <!-- /.tab-pane 2-->

      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
@endsection

@section('css')
<link rel="stylesheet" href="<?php echo base_url() . 'kendo_tree/' ?>kendo.common-material.min.css" />
<link rel="stylesheet" href="<?php echo base_url() . 'kendo_tree/' ?>kendo.material.min.css" />
@endsection

@section('script')
<script src="<?php echo base_url() . 'kendo_tree/' ?>kendo.all.min.js"></script>

<script type="text/javascript">
  
  $(document).ready(function() {
    $('#submit').click(function(e) {
      e.preventDefault(13);
      //alert("Thank you for your comment!"); 
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url() ?>penetapan/data_penetapan_terseleksi',
        data: {
          tahun: $('#tahun').val(),
          propinsi: $('#propinsi').val(),
          dati2: $('#dati2').val(),
          kecamatan: $('#kecamatan').val(),
          kelurahan: $('#kelurahan').val(),
          blok: $('#blok').val(),
          nourut: $('#nourut').val()
        },
        success: function(data) {
          //$("#content").html(data);
          alert(data);
        }
      });
    });
  });
 
  function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
      return false;
    }
  }
  document.onkeypress = stopRKey;


  $('body').on('keypress', 'input, select, textarea', function(e) {
    var self = $(this),
      form = self.parents('form:eq(0)'),
      focusable, next;
    if (e.keyCode == 13) {
      focusable = form.find('input,select,button,textarea').filter(':visible');
      next = focusable.eq(focusable.index(this) + 1);
      if (next.length) {
        next.focus();
      } else {
        $('#nourut').keypress(function(e) {
          if (e.which == 13) { //Enter key pressed
            $('#submit').click(); //Trigger search button click event
          }
        });
      }
      return false;

    }
  });
</script>
@endsection