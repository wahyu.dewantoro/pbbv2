@extends('page.master')
@section('judul')
<h1>
    Form Menu Mobile
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Menu Mobile</a></li>
    <li class="active"><?= $button  ?></li>
</ol>
@endsection
@section('content')

<div class="box">
    <div class="box-body">
        <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Menu </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
                            <?php echo form_error('nama_menu') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Parent Menu </label>
                        <div class="col-md-8">
                            <select class="form-control chosen" name="parent_menu" id="parent_menu">
                                <option value="0">Parent</option>
                                <?php foreach ($parent as $parent) { ?>
                                    <option <?php if ($parent_menu == $parent->ID_INC) {
                                                echo "selected";
                                            } ?> value="<?php echo $parent->ID_INC ?>"><?php echo $parent->NAMA_MENU ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('parent_menu') ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Active </label>
                        <div class="col-md-8">
                            <input type="radio" name="active" class="flat-red" value="1" <?php
                                                                                            if ($active == '1') {
                                                                                                echo "checked";
                                                                                            }
                                                                                            ?> /> Aktif<br>
                            <input type="radio" name="active" class="flat-red" value="0" <?php
                                                                                            if ($active == '0') {
                                                                                                echo "checked";
                                                                                            }
                                                                                            ?> /> Non Aktif
                            <?php echo form_error('active') ?>
                        </div>
                    </div>
                    <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('mobile/menu') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>










        </form>
    </div>
</div>

@endsection