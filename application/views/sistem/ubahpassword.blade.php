<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('judul')

<?php if(isset($_SESSION['pesan'])) { echo $_SESSION['pesan']; } ?>
<h1>
    Change Password
</h1>
@endsection
@section('content')

<div class="row">
    <div class="col-md-4">
        <div class="box box-success">
            <form role="form" id="form" method="post" action="#" enctype="multipart/form-data">
            <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputEmail1">Password Lama</label>
                    <input type="password" required="required" class="form-control form-control-sm" name="password_lama" id="password_lama" value="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password Baru</label>
                    <input type="password" id="password_baru" name="password_baru" id="password_baru" required="required" class="form-control form-control-sm" value="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Ulangi Password Baru</label>
                    <input type="password" id="ulangi_password" name="ulangi_password" id="ulangi_password" required="required" class="form-control form-control-sm" value="">
                  </div>
                </div>
            </div>
            </div>
            <div class="box-footer">
                <div class="form-group" style="margin-bottom:0rem">
                    <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<!-- DataTables -->
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).on('click', '#submit', function(e) {
        e.preventDefault();
        var p_lama = $('#password_lama').val();
        var p_baru = $('#password_baru').val();
        var ulangi_p = $('#ulangi_password').val();
        console.log(p_lama);
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("sistem/simpanpassword") ?>',
            dataType: 'json',
            type: 'POST',
            data: {password_lama: p_lama, password_baru: p_baru, ulangi_password: ulangi_p},
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    Swal.fire({
                        title: 'Success',
                        text: response.data,
                        icon: 'success',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })

                    $('#password_lama').val('');
                    $('#password_baru').val('');
                    $('#ulangi_password').val('');
                } else {
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>
@endsection

