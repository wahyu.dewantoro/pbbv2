@extends('page.master')
@section('judul')
<h1>
    Privilege
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>Group</li>
    <li>Privilege</li>
    <li class="active"><?= $ng ?></li>
</ol>
@endsection
@section('content')
<form method="post" action="<?php echo base_url() . 'sistem/do_role' ?>">
    <!-- Main content -->

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Akses Menu</h3>
            <div class="box-tools">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button> <?php echo anchor('group', '<i class="fa  fa-angle-double-left"></i> Kembali', ' class="btn btn-sm btn-success"'); ?>
            </div>
        </div>
        <div class="box-body no-padding">
            <input type="hidden" name="kode_group" value="<?php echo $kode_groupq; ?>">
            <table class='table table-bordered table-hover'>
                <thead>
                    <tr>
                        <th width="4%">No</th>

                        <th colspan="2">Menu</th>
                        <th>Parent</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($role as $row) {
                        $st = $no % 2;
                    ?>
                        <tr>
                            <td align="center"><?php echo $no; ?></td>
                            <td width="3%" align="center"><input <?php if ($row['STATUS_ROLE'] == 1) {
                                                                        echo "checked";
                                                                    } ?> type="checkbox" name="role[]" value="<?php echo $row['KODE_ROLE']; ?>"></td>
                            <td><?php echo ucwords($row['NAMA_MENU']); ?></td>
                            <td><?php echo ucwords($row['PARENT']); ?></td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>
</form>
@endsection