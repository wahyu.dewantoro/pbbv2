@extends('page.master')
@section('judul')

<h1> Print Preview</h1>
@if(isset($_SERVER['HTTP_REFERER']))
            @php $_url=$_SERVER['HTTP_REFERER']; @endphp
        @else
            @php $_url=base_url();  @endphp
        @endif
<ol class="breadcrumb">
    <li><a href="{{ $_url }}"><i class="fa fa-angle-double-left"></i> Kembali</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div id="tutorial-pdf-responsive" class="custom1">
            <div class="custom2">
                <iframe src="{{ $url }}"></iframe>
            </div>
        </div>
    </div>
</div>

@endsection
@section('css')
<style>
    .custom1 {
        margin-left: auto !important;
        margin-right: auto !important;
        margin: 20px;
        padding: 20px;
    }

    .custom2 {
        position: relative;
        height: 0;
        overflow: hidden;
        padding-bottom: 90%;
    }

    .custom2 iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    #tutorial-pdf-responsive {
        max-width: 100%;
        max-height: auto;
        overflow: hidden;
    }
</style>
@endsection