@extends('page.master')
@section('judul')

<section class="content-header">
    <h1>
        <?= $judul ?>
    </h1>

</section>
@endsection
@section('content')
<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form pencarian</h3>
            </div>
            <div class="box-body">
                <form action="<?= base_url() . 'baku/dataDHKP12' ?>" method="GET" class="form-inline">
                    <div class="form-group">
                        <!-- <label>Tahun Pajak <sup>*</sup></label> -->
                        <select class="form-control" required="" name="TAHUN" id="TAHUN">
                            <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                                <option <?php if ($q == $tahun) {
                                            echo "selected";
                                        } ?> value="<?= $q ?>"><?= $q ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                            <option value="">Pilih</option>
                            <?php foreach ($kec as $kec) { ?>
                                <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                            echo "selected";
                                        } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                            <option value="">Pilih</option>
                            <?php
                            foreach ($kel as $kela) { ?>
                                <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                            echo "selected";
                                        } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                            <?php  } ?>
                        </select>
                    </div>
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                    <button type="button" id="cetak" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                </form>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="example3">
                        <thead>
                            <tr>
                                <th rowspan="2">Kelurahan</th>
                                <th rowspan="2">Jumalah SPPT</th>
                                <th rowspan="2">Baku</th>
                                <th colspan="4">Realisasi</th>
                                <th rowspan="2">%</th>
                                <th colspan="2">Sisa</th>
                            </tr>
                            <tr>

                                <th>Sppt</th>
                                <th>Pokok</th>
                                <th>Denda</th>
                                <th>Jumlah</th>
                                <th>SPPT</th>
                                <th>Pokok</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $a = 0;
                                $b = 0;
                                $c = 0;
                                $d = 0;
                                $e = 0;
                                $f = 0;
                                $g = 0;
                                $h = 0;
                                $i = 0;

                                foreach ($data as $rk) { ?>
                                    <tr>
                                        <td><?php echo $rk->NM_KELURAHAN ?></td>
                                        <td align="center"><?php echo $rk->JUM_SPPT;
                                                            $a += $rk->JUM_SPPT; ?></td>
                                        <td align="right"><?php echo number_format($rk->BAKU, '0', '', '.');
                                                            $b += $rk->BAKU; ?></td>
                                        <td align="center"><?php echo $rk->SPPT_REALISASI;
                                                            $c += $rk->SPPT_REALISASI; ?></td>
                                        <td align="right"><?php echo number_format($rk->POKOK_REALISASI, '0', '', '.');
                                                            $d += $rk->POKOK_REALISASI; ?></td>
                                        <td align="right"><?php echo number_format($rk->DENDA_REALISASI, '0', '', '.');
                                                            $e += $rk->DENDA_REALISASI; ?></td>
                                        <td align="right"><?php echo number_format($rk->JUMLAH_REALISASI, '0', '', '.');
                                                            $f += $rk->JUMLAH_REALISASI; ?></td>
                                        <td align="center"><?php echo number_format($rk->PERSEN_REALISASI, '2', ',', '.');
                                                            $g += $rk->PERSEN_REALISASI; ?></td>
                                        <td align="center"><?php echo $rk->SPPT_SISA;
                                                            $h += $rk->SPPT_SISA; ?></td>
                                        <td align="right"><?php echo number_format($rk->POKOK_SISA, '0', '', '.');
                                                            $i += $rk->POKOK_SISA; ?></td>

                                    </tr>
                                <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Jumlah</th>
                                <td align="center"><b><?= number_format($a, 0, '', '.') ?></b></td>
                                <td align="right"><b><?= number_format($b, 0, '', '.') ?></b></td>
                                <td align="center"><b><?= number_format($c, 0, '', '.') ?></b></td>
                                <td align="right"><b><?= number_format($d, 0, '', '.') ?></b></td>
                                <td align="right"><b><?= number_format($e, 0, '', '.') ?></b></td>
                                <td align="right"><b><?= number_format($f, 0, '', '.') ?></b></td>
                                <td align="center"><b><?= number_format($g, 0, '', '.') ?> %</b></td>
                                <td align="right"><b><?= number_format($h, 0, '', '.') ?> </b></td>
                                <td align="right"><b><?= number_format($i, 0, '', '.') ?></b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    @endsection
    @section('css')
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    @endsection
    @section('script')
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#cetak").click(function() {
                if ($("#KD_KECAMATAN").val() != '') {
                    var base = "<?= base_url() ?>";
                    var url = base + 'baku/cetakBakuDHKP12?TAHUN=' + $("#TAHUN").val() + '&KD_KECAMATAN=' + $("#KD_KECAMATAN").val() + '&KD_KELURAHAN=' + $("#KD_KELURAHAN").val() ;
                    // alert(url);
                    window.location = url;
                }
            });

            $('.chosen').select2({
                allow_single_deselect: true
            });

            $('#example3').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });

            $("#KD_KECAMATAN").change(function() {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() . 'baku/getkelurahan' ?>",
                    data: {
                        kd_kec: $("#KD_KECAMATAN").val()
                    },
                    success: function(response) {
                        $("#KD_KELURAHAN").html(response);
                    }

                });
            });
        });
    </script>
    @endsection