@extends('page.master')
@section('judul')

<section class="content-header">
    <h1>
        <?= $judul ?>
        <div class="pull-right">
            <?php if ($upt == false) { ?>
                <a class="btn btn-sm btn-success" href="<?= base_url() . 'baku/cetakBakuDHKP?TAHUN=' . $tahun  ?>"><i class="fa fa-file"></i> DHKP 1 Kabupaten</a>
            <?php } ?>
        </div>
    </h1>

</section>
@endsection
@section('content')
<!-- Main content -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form pencarian</h3>
            </div>
            <div class="box-body">
                <form action="<?= base_url() . 'baku/dataDHKP' ?>" method="GET" class="form-inline">
                    <div class="form-group">
                        <!-- <label>Tahun Pajak <sup>*</sup></label> -->
                        <select class="form-control" required="" name="TAHUN" id="TAHUN">
                            <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                                <option <?php if ($q == $tahun) {
                                            echo "selected";
                                        } ?> value="<?= $q ?>"><?= $q ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                            <?php 
                            $select=(isset($kec)&&count($kec)>0)?"<option value=''>Pilih</option>":"";
                            echo $select;
                            foreach ($kec as $kec) { ?>
                                <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                            echo "selected";
                                        } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                            <?php
                            $select=(isset($kel)&&count($kel)>0)?"<option value=''>Pilih</option>":"";
                            echo $select;
                            foreach ($kel as $kela) { ?>
                                <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                            echo "selected";
                                        } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                            <?php  } ?>
                        </select>
                    </div>
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                    <button type="button" id="cetak" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                </form>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="example3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NOP</th>
                                <th>Alamat OP</th>
                                <th>RW/RT OP</th>
                                <th>Luas Bumi</th>
                                <th>Luas Bangunan</th>
                                <th>Nama wp</th>
                                <th>Alamat wp</th>
                                <th>RW/RT WP</th>
                                <th>Kelurahan WP</th>
                                <th>Kota WP</th>
                                <th>PBB</th>
                                <th>Terbit SPPT</th>
                                <th>Pembayaran SPPT</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $cv = 0;
                            $no = 1;
                            foreach ($data as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td><?= $rk->NOP ?></td>
                                    <td><?= $rk->ALAMAT_OP ?></td>
                                    <td><?= $rk->RWRT_OP ?></td>
                                    <td align="center"><?= number_format($rk->LUAS_BUMI_SPPT, 0, '', '.') ?> M<sup>2</sup></td>
                                    <td align="center"><?= number_format($rk->LUAS_BNG_SPPT, 0, '', '.') ?> M<sup>2</sup></td>
                                    <td><?= $rk->NM_WP_SPPT ?></td>
                                    <td><?= $rk->ALAMAT_WP ?></td>
                                    <td><?= $rk->RWRT_WP ?></td>
                                    <td><?= $rk->KELURAHAN_WP_SPPT ?></td>
                                    <td><?= $rk->KOTA_WP_SPPT ?></td>
                                    <td align="right"><?php $cv += $rk->PBB;
                                                        echo number_format($rk->PBB, 0, '', '.') ?></td>
                                    <td align="center"><?= $rk->TGL_TERBIT_SPPT ?></td>
                                    <td align="center"><?= $rk->TGL_PEMBAYARAN_SPPT ?></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                        <thead>
                            <tr>

                                <td colspan="11"><b>Jumlah</b></td>
                                <td align="right"><b><?php echo number_format($cv, 0, '', '.') ?></b></td>
                                <td colspan="2"></td>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    @endsection
    @section('css')
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    @endsection
    @section('script')
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#cetak").click(function() {
                if ($("#KD_KECAMATAN").val() != '') {
                    var base = "<?= base_url() ?>";
                    var url = base + 'baku/cetakBakuDHKP?TAHUN=' + $("#TAHUN").val() + '&KD_KECAMATAN=' + $("#KD_KECAMATAN").val() + '&KD_KELURAHAN=' + $("#KD_KELURAHAN").val() ;
                    // alert(url);
                    window.location = url;
                }
            });

            $('.chosen').select2({
                allow_single_deselect: true
            });

            $('#example3').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });

            $("#KD_KECAMATAN").change(function() {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() . 'baku/getkelurahan' ?>",
                    data: {
                        kd_kec: $("#KD_KECAMATAN").val()
                    },
                    success: function(response) {
                        $("#KD_KELURAHAN").html(response);
                    }

                });
            });
        });
    </script>
    @endsection