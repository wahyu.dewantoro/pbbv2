@extends('page.master')
@section('judul')

<section class="content-header">
    <h1>
        <?= $judul ?>
        <div class="pull-right">

        </div>
    </h1>

</section>
@endsection
@section('content')

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Form pencarian</h3>
                </div>
                <div class="box-body">
                    <form action="<?= base_url() . 'baku/ringkasan' ?>" method="GET" class="form-inline">
                        <div class="form-group">
                            <!-- <label>Tahun Pajak <sup>*</sup></label> -->
                            <select class="form-control" required="" name="TAHUN" id="TAHUN">
                                <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                                    <option <?php if ($q == $tahun) {
                                                echo "selected";
                                            } ?> value="<?= $q ?>"><?= $q ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <!-- <label>Kecamatan <sup>*</sup></label> -->
                            <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                <?php  if(isset($kec)&&count($kec)>1){
                                        echo "<option value=''>Pilih</option>";
                                    }
                                ?>
                                <?php foreach ($kec as $kec) { ?>
                                    <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                                echo "selected";
                                            } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <!-- <label>Kelurahan / Desa <sup>*</sup></label> -->
                            <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                                <?php  if(isset($kel)&&count($kel)>1){
                                        echo "<option value=''>Pilih</option>";
                                    }
                                ?>
                                <?php
                                foreach ($kel as $kela) { ?>
                                    <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                                echo "selected";
                                            } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->NM_KELURAHAN ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                        <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
                        <?php if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '') { ?>

                            <a class="btn btn-sm btn-success" href="<?= base_url() . 'baku/cetakRingkasanbaku?TAHUN=' . $tahun . '&KD_KECAMATAN=' . $KD_KECAMATAN . '&KD_KELURAHAN=' . $KD_KELURAHAN ?>"><i class="fa fa-file"></i> Excel</a>
                        <?php } else { ?>
                            <?php if ($upt == false) { ?> <a class="btn btn-sm btn-success" href="<?= base_url() . 'baku/cetakRingkasanbaku?TAHUN=' . $tahun ?>"><i class="fa fa-file"></i> Tarik 1 Kabupaten </a><?php } ?>
                        <?php } ?>
                    </form>
                </div><!-- /.box-body -->
            </div>
        </div>
        @if(!empty($data))
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="example3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>kelurahan</th>
                                <th>Buku</th>
                                <th>Jumlah Baku</th>
                                <th>Total Baku</th>
                                <th>Jumlah Realisasi</th>
                                <th>Total Realisasi</th>
                                <th>Jumlah Piutang</th>
                                <th>Total Piutang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $a = 0;
                            $b = 0;
                            $c = 0;
                            $d = 0;
                            $e = 0;
                            $f = 0;
                            $no = 1;
                            foreach ($data as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td> <?= $rk->KD_KECAMATAN.' - '. $rk->NM_KECAMATAN ?></td>
                                    <td> <?= $rk->KD_KELURAHAN.' - '. $rk->NM_KELURAHAN ?></td>
                                    <td> <?= $rk->BUKU ?></td>
                                    <td align="center"> <?php if (!empty($rk->SPPT)) {
                                                            echo number_format($rk->SPPT, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $a += $rk->SPPT;  ?></td>
                                    <td align="right"> <?php if (!empty($rk->BAKU)) {
                                                            echo number_format($rk->BAKU, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $b += $rk->BAKU;  ?></td>
                                    <td align="center"> <?php if (!empty($rk->SPPT_REALISASI)) {
                                                            echo number_format($rk->SPPT_REALISASI, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $c += $rk->SPPT_REALISASI;  ?></td>
                                    <td align="right"> <?php if (!empty($rk->REALISASI)) {
                                                            echo number_format($rk->REALISASI, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $d += $rk->REALISASI;  ?></td>
                                    <td align="center"> <?php if (!empty($rk->SPPT_PIUTANG)) {
                                                            echo number_format($rk->SPPT_PIUTANG, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $e += $rk->SPPT_PIUTANG;  ?></td>
                                    <td align="right"> <?php if (!empty($rk->PIUTANG)) {
                                                            echo number_format($rk->PIUTANG, '0', '', '.');
                                                        } else {
                                                            echo "0";
                                                        }
                                                        $f += $rk->PIUTANG;  ?></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>

                        <tfoot>
                            <tr>
                                <th colspan="4">Jumlah</th>
                                <td align="right"><b><?= number_format($a, 2, ',', '.'); ?></b></td>
                                <td align="right"><b><?= number_format($b, 2, ',', '.'); ?></b></td>
                                <td align="right"><b><?= number_format($c, 2, ',', '.'); ?></b></td>
                                <td align="right"><b><?= number_format($d, 2, ',', '.'); ?></b></td>
                                <td align="right"><b><?= number_format($e, 2, ',', '.'); ?></b></td>
                                <td align="right"><b><?= number_format($f, 2, ',', '.'); ?></b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        @endif
    </div>
</section><!-- /.content -->
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });
    });
</script>
@endsection