@extends('page.master')
@section('judul')
<h1>
    <?= $judul ?>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Baku</a></li>
    <li class="active"><?= $judul ?></li>
</ol>
@endsection
<!-- Main content -->
@section('content')
<!-- Default box -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form pencarian</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" action="<?= base_url() . 'baku/DownloadDhkpDesa' ?>" method="GET">
                    <div class="form-group">
                        Tanggal
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
                </form>
            </div><!-- /.box-body -->
        </div>
    </div>

    <?php if (!empty($data)) { ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <?php $dir='C:\xampp\htdocs\layanan200';
                    ?>
                    <table class="table table-bordered table-striped" id="example3">
                        <thead>
                            <tr>
                                <th width="3%">No</th>
                                <th>Kecamatan</th>
                                <th>DHKP File</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cc = 0;
                            $no = 1;
                            foreach ($data as $rk) { ?>
                                <?php if (file_exists($dir. "/" . $rk->FILE_DHKP)) { ?>
                                    <tr>
                                        <td align="center"><?= $no ?></td>
                                        <td><?= $rk->NM_KECAMATAN ?></td>
                                        <td align="center"><a href="<?= base_url().'baku/downloaddhkp?file='.str_replace('/','_', $rk->FILE_DHKP) ?>" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a></td>
                                    </tr>
                            <?php $no++;
                                }
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.chosen').select2({
            allow_single_deselect: true
        });

        $('.tanggal').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });


        $('#example3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })

        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }

            });
        });

        $("#cetak").click(function() {

            if ($("#KD_KECAMATAN").val() != '') {
                var base = "<?= base_url() ?>";
                var url = base + 'baku/cetakBakuDHKPdesa?TAHUN=' + $("#TAHUN").val() + '&KD_KECAMATAN=' + $("#KD_KECAMATAN").val() + '&KD_KELURAHAN=' + $("#KD_KELURAHAN").val() + '&BUKU=' + $("#BUKU").val();
                window.location = url;

            }
        });

    });
</script>
@endsection