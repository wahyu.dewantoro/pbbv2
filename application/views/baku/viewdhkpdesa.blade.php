@extends('page.master')
@section('judul')
<h1>
    <?= $judul ?>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Baku</a></li>
    <li class="active"><?= $judul ?></li>
</ol>
@endsection

<!-- Main content -->
@section('content')
<!-- Default box -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Form pencarian</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" action="<?= base_url() . 'baku/dhkpDesa' ?>" method="GET">
                    <div class="form-group">
                        <select class="form-control" required="" name="TAHUN" id="TAHUN">
                            <option value="">Tahun</option>
                            <?php $pp = date('Y');
                            for ($rp = $pp - 5; $rp <= $pp; $rp++) { ?>
                                <option <?php if ($tahun == $rp) {
                                            echo "selected";
                                        } ?> value="<?= $rp ?>"><?= $rp ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                            <?php  if(isset($kec)&&count($kec)>1){
                                    echo "<option value=''>Kecamatan</option>";
                                }
                            ?>
                            <?php foreach ($kec as $kec) { ?>
                                <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                            echo "selected";
                                        } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN" <?php echo $required;?>>
                            <?php  if(isset($kel)&&count($kel)>1){
                                    echo "<option value=''>Kelurahan</option>";
                                }
                            ?>
                            <?php
                            foreach ($kel as $kela) { ?>
                                <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                            echo "selected";
                                        } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                            <?php  } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control chosen" name="BUKU[]" id="BUKU" multiple="multiple" data-placeholder="Pilih Buku">
                            <?php foreach ($buku as $bb) { ?>
                                <option value="<?= $bb ?>" <?php if (in_array($bb, $lb)) { ?> selected="selected" <?php }; ?>><?= $bb ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Preview</button>
                    <button type="button" id="cetak" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                </form>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Data SPPT</h3>
                <div class="pull-right">
                    <!-- <a class="btn btn-sm btn-success" href="<?= base_url() . 'baku/cetakBakuDHKPdesa?CARI=1&TAHUN=' . $tahun . '&KD_KECAMATAN=' . $KD_KECAMATAN . '&KD_KELURAHAN=' . $KD_KELURAHAN . '&BUKU=' . $filter_buku ?>"><i class="fa fa-file"></i> Excel</a> -->
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="exampletab">
                        <thead>
                            <tr>
                                <th colspan="5">Objek Pajak</th>
                                <th colspan="4">Wajib Pajak</th>
                                <th rowspan="2">PBB Awal</th>
                                <th rowspan="2">PBB Akhir</th>
                                <th rowspan="2">Terbit SPPT</th>
                                <th rowspan="2">Lunas</th>
                                <th rowspan="2">Bayar SPPT</th>
                            </tr>
                            <tr>

                                <th width="12%">NOP</th>
                                <th>Alamat</th>
                                <th>Rw/RT</th>
                                <th>Luas Bumi</th>
                                <th>Luas Bangunan</th>
                                <th>Nama</th>
                                
                                <th>RW / RT</th>
                                <th>Kelurahan</th>
                                <th>Kota</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("ajaxStart.mine", function () {
            Swal.fire({
                title: "Sedang melakukan proses pencarian data.",
                html:'<div class="fa-3x"><i class="fa fa-spinner fa-pulse"></i></div>',
                showConfirmButton: false,
                allowOutsideClick: false,
            });
        });
        $(document).bind("ajaxStop.mine", function () {
            swal.close();
        });

        // server side
        <?php $ss = implode(',', $lb); ?>
        <?php 
        $check=true;
        if($sessionCheck=="13"){
            $check=($KD_KELURAHAN<> '')?$check:false;
        }
        if ($KD_KECAMATAN <> ''&&$check&&$direct_load_ajx) { ?>
            var t = $("#exampletab").dataTable({
                initComplete: function() {
                    var api = this.api();
                    $('#exampletab_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
                },
                'oLanguage': {
                    "sProcessing": "Sedang memproses...",
                    "sLengthMenu": "Tampilkan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext": "Selanjutnya",
                        "sLast": "Terakhir"
                    }
                },
                processing: true,
                serverSide: false,
                ajax: {
                    'url': '<?= base_url() ?>baku/jsondatabaku',
                    'type': 'POST',
                    "data": {
                        'KD_KECAMATAN': '<?= $KD_KECAMATAN ?>',
                        'KD_KELURAHAN': '<?= $KD_KELURAHAN ?>',
                        'tahun': '<?= $tahun ?>',
                        'jenis_buku': '<?PHP echo $ss ?>'
                    }
                },
                columns: [{
                        'data': 'NOP',
                        'className': 'text-center',
                        'orderable': false
                    },
                    {
                        'data': 'ALAMAT_OP'
                    },
                    {
                        'data': 'RWRT_OP'
                    },
                    {
                        'data': 'LUAS_BUMI_SPPT'
                    },
                    {
                        'data': 'LUAS_BNG_SPPT'
                    },
                    {
                        'data': 'NM_WP_SPPT'
                    },
                    {
                        'data': 'RWRT_WP'
                    },
                    {
                        'data': 'KELURAHAN_WP_SPPT'
                    },
                    {
                        'data': 'KOTA_WP_SPPT'
                    },
                    {
                        'data': 'PBB'
                    },
                    {
                        'data': 'PBB_AKHIR'
                    },
                    {
                        'data': 'TGL_TERBIT_SPPT'
                    },
                    {
                        'data': 'TGL_PEMBAYARAN_SPPT'
                    },
                    {
                        'data': 'TGL_PEMBAYARAN_SPPT'
                    },


                ],

            });
        <?php } ?>


        $('.chosen').select2({
            allow_single_deselect: true
        });


        $("#KD_KECAMATAN").change(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                    kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                    $("#KD_KELURAHAN").html(response);
                }
            });
        });
        $("#cetak").click(function() {
            if ($("#KD_KECAMATAN").val() != '') {
                var base = "<?= base_url() ?>";
                var url = base + 'baku/cetakBakuDHKPdesa?TAHUN=' + $("#TAHUN").val() + '&KD_KECAMATAN=' + $("#KD_KECAMATAN").val() + '&KD_KELURAHAN=' + $("#KD_KELURAHAN").val() + '&BUKU=' + $("#BUKU").val();
                // alert(url);
                window.location = url;
            }
        });



    });
</script>

@endsection