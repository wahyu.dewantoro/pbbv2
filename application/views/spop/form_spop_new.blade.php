<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Formulir</a></li>
                <li><a href="#tab_2" data-toggle="tab">SPOP</a></li>
                <li><a href="#tab_3" data-toggle="tab">LSPOP</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <!-- formulir -->
                    <form class='form-horizontal' action="<?php echo base_url('spop/proses_generate_nomor'); ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tahun</label>
                                    <div class="col-md-9">
                                        <select name="TAHUN_AJUAN" id="TAHUN_AJUAN" class="form-control" required>
                                            <option value="">Pilih Tahun</option>
                                            <?php $th = date('Y');
                                            for ($i = $th; $i > ($th - 2); $i--) { ?>
                                                <option <?php if ($tahun == $i) {
                                                            echo "selected";
                                                        } ?> value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jenis Formulir</label>
                                    <div class="col-md-9">
                                        <select class="form-control chosen" required name="KD_JNS_FORMULIR" id="KD_JNS_PELAYANAN" onchange="jenis(this.value)">
                                            <option value="">Pilih</option>
                                            <?php foreach ($kd_formulir as $key => $value) { ?>
                                                <option value="<?= $key ?>"><?= $key . ' - ' . $value ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3  control-label">NOP <br>
                                        <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                    </label>
                                    <div class="col-md-9" id='teste'>
                                        <table width='100%'>
                                            <tr>
                                                <td width='12%'><input type="text" required maxlength="2" class="form-control" value="35" name="KD_PROPINSI" id=""></td>
                                                <td width='12%'><input type="text" required maxlength="2" class="form-control" value="07" name="KD_DATI2" id=""></td>
                                                <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_KECAMATAN" id=""></td>
                                                <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_KELURAHAN" id=""></td>
                                                <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_BLOK" id=""></td>
                                                <td><input type="text" required maxlength="4" class="form-control" value="" name="NO_URUT_NOP" id=""></td>
                                                <td width='9%'><input type="text" required maxlength="1" class="form-control" value="" name="KD_JNS_OP" id=""></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jumlah Bangunan</label>
                                    <div class="col-md-9">
                                        <input type="text" tabIndex="24" required class="form-control " name="NO_BNG" value="1" placeholder="Jumlah Bangunan" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <button type="" id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <div id="nomerformulir">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <!-- spop -->

                    <form class='form-horizontal' action="<?php echo base_url('spop/create_spop'); ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="JENIS" value="SPOP">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jenis Transaksi </label>
                                    <div class="col-md-9">
                                        <select class="form-control chosen" required name="KD_JNS_PELAYANAN_SPOP" id="KD_JNS_PELAYANAN_SPOP" onchange="jenis(this.value)">
                                            <option value="">Pilih</option>
                                            <?php foreach ($jenis_spop as $skey => $svalue) { ?>
                                                <option value="<?= $skey ?>"><?= $skey . ' - ' . $svalue ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3  control-label">NOP <br>
                                        <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                    </label>
                                    <div class="col-md-9" id='test'>
                                        <table width='100%'>
                                            <tr>
                                                <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_PROPINSI_SPOP'); ?>" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON"></td>
                                                <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_DATI2_SPOP'); ?>" name="KD_DATI2" id="KD_DATI2_PEMOHON"></td>
                                                <td width='14%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KECAMATAN_SPOP'); ?>" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON"></td>
                                                <td width='14%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KELURAHAN_SPOP'); ?>" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON"></td>
                                                <td width='14%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_BLOK_SPOP'); ?>" name="KD_BLOK" id="KD_BLOK_PEMOHON"></td>
                                                <td><input type="text" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" value="<?php echo $CI->session->userdata('NO_URUT_NOP_SPOP'); ?>" name="NO_URUT_NOP" id="NO_URUT_PEMOHON"></td>
                                                <td width='11%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" value="<?php echo $CI->session->userdata('KD_JNS_OP_SPOP'); ?>" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nomor Formulir </label>
                                    <div class="col-md-9">
                                        <table>
                                            <tr>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('THN_PELAYANAN_SPOP'); ?>" name="THN_PELAYANAN" id="THN_PELAYANAN" placeholder="Tahun" required /></td>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('BUNDEL_PELAYANAN_SPOP'); ?>" name="BUNDEL_PELAYANAN" id="KATEGORI_PELAYANAN" placeholder="Bundel" required /></td>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_DALAM_SPOP'); ?>" name="NO_URUT_DALAM" id="NO_URUT_DALAM" placeholder="No urut Dalam" required /></td>
                                                <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_SPOP'); ?>" name="NO_URUT" id="NO_URUT" placeholder="No urut" required /></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3  control-label">NOP Bersama</label>
                                    <div class="col-md-9">
                                        <table width='100%'>
                                            <tr>
                                                <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON_BERSAMA" id=""></td>
                                                <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON_BERSAMA" id=""></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON_BERSAMA" id=""></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON_BERSAMA" id=""></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_BLOK_PEMOHON_BERSAMA" id=""></td>
                                                <td><input type="text" maxlength="4" class="form-control" name="NO_URUT_PEMOHON_BERSAMA" id=""></td>
                                                <td width='9%'><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON_BERSAMA" id=""></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3  control-label">NOP Asal</label>
                                    <div class="col-md-9">
                                        <table width='100%'>
                                            <tr>
                                                <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_ASAL" id="" value="<?php echo $CI->session->userdata('KD_PROPINSI_ASAL_SPOP'); ?>"></td>
                                                <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_DATI2_ASAL" id="" value="<?php echo $CI->session->userdata('KD_DATI2_ASAL_SPOP'); ?>"></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN_ASAL" id="" value="<?php echo $CI->session->userdata('KD_KECAMATAN_ASAL_SPOP'); ?>"></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_ASAL" id="" value="<?php echo $CI->session->userdata('KD_KELURAHAN_ASAL_SPOP'); ?>"></td>
                                                <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_BLOK_ASAL" id="" value="<?php echo $CI->session->userdata('KD_BLOK_ASAL_SPOP'); ?>"></td>
                                                <td><input type="text" maxlength="4" class="form-control" name="NO_URUT_ASAL" id="" value="<?php echo $CI->session->userdata('NO_URUT_ASAL_SPOP'); ?>"></td>
                                                <td width='9%'><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_ASAL" id="" value="<?php echo $CI->session->userdata('KD_JNS_OP_ASAL_SPOP'); ?>"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <!-- <button type="" id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="form_spop"></div>
                    </form>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    <form class='form-horizontal' action="<?php echo base_url('spop/create_lspop'); ?>" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Jenis Transaksi </label>
                                        <div class="col-md-9">
                                            <input type="hidden" name="JENIS" value="LSPOP">
                                            <select class="form-control  " tabIndex="1" required name="KD_JNS_PELAYANAN_LSPOP" id="KD_JNS_PELAYANAN_LSPOP">
                                                <option value="">Piliih</option>
                                                <?php foreach ($jenis_lspop as $kjl => $vjl) { ?>
                                                    <option value="<?= $kjl ?>"><?= $kjl . ' - ' . $vjl ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3  control-label">NOP </label>
                                        <div class="col-md-9" id="test_LSPOP">
                                            <table width='100%'>
                                                <tr>
                                                    <td width='12%'><input type="text" tabIndex="3" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_PROPINSI'); ?>" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON_LSPOP"></td>
                                                    <td width='12%'><input type="text" tabIndex="4" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_DATI2'); ?>" name="KD_DATI2" id="KD_DATI2_PEMOHON_LSPOP"></td>
                                                    <td width='15%'><input type="text" tabIndex="5" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KECAMATAN'); ?>" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON_LSPOP"></td>
                                                    <td width='15%'><input type="text" tabIndex="6" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KELURAHAN'); ?>" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON_LSPOP"></td>
                                                    <td width='15%'><input type="text" tabIndex="7" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_BLOK'); ?>" name="KD_BLOK" id="KD_BLOK_PEMOHON_LSPOP"></td>
                                                    <td><input type="text" tabIndex="8" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" value="<?php echo $CI->session->userdata('NO_URUT_NOP'); ?>" name="NO_URUT_NOP" id="NO_URUT_PEMOHON_LSPOP"></td>
                                                    <td width='9%'><input type="text" tabIndex="9" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" value="<?php echo $CI->session->userdata('KD_JNS_OP'); ?>" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON_LSPOP"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor Formulir </label>
                                        <div class="col-md-9">
                                            <table>
                                                <tr>
                                                    <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('THN_PELAYANAN'); ?>" name="THN_PELAYANAN" id="THN_PELAYANAN_LSPOP" placeholder="Tahun" required /></td>
                                                    <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('BUNDEL_PELAYANAN'); ?>" name="BUNDEL_PELAYANAN" id="KATEGORI_PELAYANAN_LSPOP" placeholder="Bundel" required /></td>
                                                    <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_DALAM'); ?>" name="NO_URUT_DALAM" id="NO_URUT_DALAM_LSPOP" placeholder="No urut Dalam" required /></td>
                                                    <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT'); ?>" name="NO_URUT" id="NO_URUT_LSPOP" placeholder="No urut" required /></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3  control-label">NOP Bersama</label>
                                        <div class="col-md-9">
                                            <table>
                                                <tr>
                                                    <td width='13%'><input type="text" tabIndex="10" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON_B" id=""></td>
                                                    <td width='13%'><input type="text" tabIndex="11" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON_B" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="12" maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON_B" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="13" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON_B" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="14" maxlength="3" class="form-control" name="KD_BLOK_PEMOHON_B" id=""></td>
                                                    <td><input type="text" tabIndex="15" maxlength="4" class="form-control" name="NO_URUT_PEMOHON_B" id=""></td>
                                                    <td width='9%'><input type="text" tabIndex="16" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON_B" id=""></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3  control-label">NOP Asal</label>
                                        <div class="col-md-9">
                                            <table>
                                                <tr>
                                                    <td width='13%'><input type="text" tabIndex="17" maxlength="2" class="form-control" name="KD_PROPINSI_ASAL" value="<?php echo $CI->session->userdata('KD_PROPINSI_ASAL'); ?>" id=""></td>
                                                    <td width='13%'><input type="text" tabIndex="18" maxlength="2" class="form-control" name="KD_DATI2_ASAL" value="<?php echo $CI->session->userdata('KD_DATI2_ASAL'); ?>" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="19" maxlength="3" class="form-control" name="KD_KECAMATAN_ASAL" value="<?php echo $CI->session->userdata('KD_KECAMATAN_ASAL'); ?>" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="20" maxlength="3" class="form-control" name="KD_KELURAHAN_ASAL" value="<?php echo $CI->session->userdata('KD_KELURAHAN_ASAL'); ?>" id=""></td>
                                                    <td width='16%'><input type="text" tabIndex="21" maxlength="3" class="form-control" name="KD_BLOK_ASAL" value="<?php echo $CI->session->userdata('KD_BLOK_ASAL'); ?>" id=""></td>
                                                    <td><input type="text" tabIndex="22" maxlength="4" class="form-control" name="NO_URUT_ASAL" value="<?php echo $CI->session->userdata('NO_URUT_ASAL'); ?>" id=""></td>
                                                    <td width='9%'><input type="text" tabIndex="23" maxlength="1" class="form-control" name="KD_JNS_OP_ASAL" value="<?php echo $CI->session->userdata('KD_JNS_OP_ASAL'); ?>" id=""></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor Bangunan</label>
                                        <div class="col-md-9" id='list_nomor'>
                                            <input type="text" tabIndex="24" required class="form-control " id="NO_BNG" name="NO_BNG" value="<?php echo $CI->session->userdata('NO_BNG'); ?>" placeholder="No Bangunan" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="form_lspop"></div>
                        </div>
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    @endsection
    @section('script')
    <script>
        $(document).ready(function() {
            getformulir($('#TAHUN_AJUAN').val());

            // proses togle tab (proses isian)
            $(".aksi").click(function(e) {
                e.preventDefault();
                alert($(this).attr('data-jenis'));
                nomer = $(this).attr('data-id');
                jenis = $(this).attr('data-jenis');
                if (jenis == 'SPOP') {
                    key = 1;
                } else if (jenis == 'LSPOP') {
                    key = 2;
                } else {
                    key = 0;
                }
                $('#myTab li a').eq(key).tab('show');
            });

            // TAHUN_AJUAN list nomer formulir
            $("#TAHUN_AJUAN").change(function(e) {
                tahun = $(this).val();
                getformulir(tahun);
            });

            function getformulir(tahun) {
                $.ajax({
                    url: "<?= base_url() . 'spop/listformulir' ?>",
                    data: {
                        'tahun': tahun
                    },
                    type: 'get',
                    success: function(result) {
                        $('#nomerformulir').html(result);
                    }
                });
            }

            $(".form-control").keyup(function() {
                if (this.value.length == this.maxLength) {
                    var nextIndex = $('input:text').index(this) + 1;
                    $('input:text')[nextIndex].focus();
                }
            });

            $(window).keydown(function(event) {
                if (13 === event.which) {
                    event.stopPropagation();
                    $(this).next('input:text').focus();
                }

                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }

                if (e.key === "Enter") {
                    var self = $(this),
                        form = self.parents('form:eq(0)'),
                        focusable, next;
                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {
                        form.submit();
                    }
                    return false;
                }
            });

            $("#KD_JNS_PELAYANAN_SPOP").change(function(e) {
                kode = $(this).val();
                kd_propinsi = $('#KD_PROPINSI_PEMOHON').val();
                kd_dati2 = $('#KD_DATI2_PEMOHON').val();
                kd_kecamatan = $('#KD_KECAMATAN_PEMOHON').val();
                kd_kelurahan = $('#KD_KELURAHAN_PEMOHON').val();
                kd_blok = $('#KD_BLOK_PEMOHON').val();
                no_urut = $('#NO_URUT_PEMOHON').val();
                kd_jns_op = $('#KD_JNS_OP_PEMOHON').val();
                $.ajax({
                    url: "<?= base_url() . 'spop/getFormSpop' ?>",
                    data: {
                        'kode': kode,
                        'kd_propinsi': kd_propinsi,
                        'kd_dati2': kd_dati2,
                        'kd_kecamatan': kd_kecamatan,
                        'kd_kelurahan': kd_kelurahan,
                        'kd_blok': kd_blok,
                        'no_urut': no_urut,
                        'kd_jns_op': kd_jns_op
                    },
                    type: 'get',
                    success: function(result) {
                        $('#form_spop').html(result);
                    }
                });
            });

            // KD_JNS_PELAYANAN_LSPOP
            $("#KD_JNS_PELAYANAN_LSPOP").change(function(e) {
                kode = $(this).val();
                if (kode != '') {
                    kd_propinsi = $('#KD_PROPINSI_PEMOHON_LSPOP').val();
                    kd_dati2 = $('#KD_DATI2_PEMOHON_LSPOP').val();
                    kd_kecamatan = $('#KD_KECAMATAN_PEMOHON_LSPOP').val();
                    kd_kelurahan = $('#KD_KELURAHAN_PEMOHON_LSPOP').val();
                    kd_blok = $('#KD_BLOK_PEMOHON_LSPOP').val();
                    no_urut = $('#NO_URUT_PEMOHON_LSPOP').val();
                    kd_jns_op = $('#KD_JNS_OP_PEMOHON_LSPOP').val();
                    $.ajax({
                        url: "<?= base_url() . 'spop/getFormLspop' ?>",
                        data: {
                            'kode': kode,
                            'kd_propinsi': kd_propinsi,
                            'kd_dati2': kd_dati2,
                            'kd_kecamatan': kd_kecamatan,
                            'kd_kelurahan': kd_kelurahan,
                            'kd_blok': kd_blok,
                            'no_urut': no_urut,
                            'kd_jns_op': kd_jns_op
                        },
                        type: 'get',
                        success: function(result) {
                            $('#form_lspop').html(result);
                        }
                    });
                } else {
                    $('#form_lspop').html('');
                }
            });


        });
    </script>

    @endsection