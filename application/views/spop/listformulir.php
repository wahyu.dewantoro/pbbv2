<table id="example2" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="3%">No</th>
            <th>Nomor</th>
            <th>NOP</th>
            <!-- <th>Keterangan</th> -->
            <!-- <th>Tanggal</th> -->
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($nomor as $rk) { ?>
            <tr>
                <td align="center"><?php echo $no ?></td>
                <td><?php echo $rk->NO_FORMULIR ?></td>
                <td><?php echo formatnop($rk->NOP) ?></td>
                <!-- <td> <small><span class="badge  <?php if ($rk->KETERANGAN_NOMOR == 'LSPOP') {
                                            echo "bg-yellow";
                                        } else {
                                            echo "bg-blue";
                                        } ?> "><?php echo $rk->KETERANGAN_NOMOR ?></span></small> </td> -->
                <!-- <td><?php echo $rk->TGL_PEREKAMAN ?></td> -->
                <td> <button class="aksi badge  <?php if ($rk->KETERANGAN_NOMOR == 'LSPOP') {
                                            echo "bg-yellow";
                                        } else {
                                            echo "bg-blue";
                                        } ?> " data-nop="<?= $rk->NOP ?>" data-KD_PROPINSI="<?= $rk->KD_PROPINSI ?>" data-KD_DATI2="<?= $rk->KD_DATI2 ?>" data-KD_KECAMATAN="<?= $rk->KD_KECAMATAN ?>" data-KD_KELURAHAN="<?= $rk->KD_KELURAHAN ?>" data-KD_BLOK="<?= $rk->KD_BLOK ?>" data-NO_URUT_OP="<?= $rk->NO_URUT_OP ?>" data-KD_JNS_OP="<?= $rk->KD_JNS_OP ?>" data-jenis="<?= $rk->KETERANGAN_NOMOR ?>" data-id="<?= $rk->NO_FORMULIR ?>"> <small><?= $rk->KETERANGAN_NOMOR ?></small> 
             </button> </td>
            </tr>
        <?php $no++;
        } ?>
    </tbody>
</table>
<script>
    $(".aksi").click(function(e) {
        e.preventDefault();
        // alert($(this).attr('data-jenis'));
        nomer = $(this).attr('data-id');
        nop = $(this).attr('data-nop');
        jenis = $(this).attr('data-jenis');

        KD_PROPINSI = $(this).attr('data-KD_PROPINSI');
        KD_DATI2 = $(this).attr('data-KD_DATI2');
        KD_KECAMATAN = $(this).attr('data-KD_KECAMATAN');
        KD_KELURAHAN = $(this).attr('data-KD_KELURAHAN');
        KD_BLOK = $(this).attr('data-KD_BLOK');
        NO_URUT_OP = $(this).attr('data-NO_URUT_OP');
        KD_JNS_OP = $(this).attr('data-KD_JNS_OP');
        if (jenis == 'SPOP') {
            // spop
            key = 1;
            $('#KD_PROPINSI_PEMOHON').val(KD_PROPINSI);
            $('#KD_DATI2_PEMOHON').val(KD_DATI2);
            $('#KD_KECAMATAN_PEMOHON').val(KD_KECAMATAN);
            $('#KD_KELURAHAN_PEMOHON').val(KD_KELURAHAN);
            $('#KD_BLOK_PEMOHON').val(KD_BLOK);
            $('#NO_URUT_PEMOHON').val(NO_URUT_OP);
            $('#KD_JNS_OP_PEMOHON').val(KD_JNS_OP);
            nomerspop(nop);

        } else if (jenis == 'LSPOP') {
            $('#KD_PROPINSI_PEMOHON_LSPOP').val(KD_PROPINSI);
            $('#KD_DATI2_PEMOHON_LSPOP').val(KD_DATI2);
            $('#KD_KECAMATAN_PEMOHON_LSPOP').val(KD_KECAMATAN);
            $('#KD_KELURAHAN_PEMOHON_LSPOP').val(KD_KELURAHAN);
            $('#KD_BLOK_PEMOHON_LSPOP').val(KD_BLOK);
            $('#NO_URUT_PEMOHON_LSPOP').val(NO_URUT_OP);
            $('#KD_JNS_OP_PEMOHON_LSPOP').val(KD_JNS_OP);
            key = 2;
            nomerlspop(nop);
        } else {
            key = 0;
        }
        $('#myTab li a').eq(key).tab('show');
        $('#KD_JNS_PELAYANAN_LSPOP').val();
        $('#KD_JNS_PELAYANAN').val();
    });


    // nomer spop
    function nomerspop(nop) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Spop/getformulir'); ?>",
            data: "nop=" + nop,
            cache: false,
            success: function(data) {
                var z = data.split('|');
                if (z[0] != '') {
                    $('#THN_PELAYANAN').attr('value', z[0]);
                    $('#KATEGORI_PELAYANAN').attr('value', z[1]);
                    $('#NO_URUT_DALAM').attr('value', z[2]);
                    $('#NO_URUT').attr('value', z[3]);
                } else {
                    alert("NOP ini belum punya Nomor formulir/nomor formulir sudah digunakan");
                    $('#mySubmit').prop("disabled", true);
                    $('#KD_KELURAHAN_PEMOHON').val("");
                    $('#KD_BLOK_PEMOHON').val("");
                    $('#NO_URUT_PEMOHON').val("");
                    $('#KD_JNS_OP_PEMOHON').val("");
                };
                //alert(z[3]);
            }
        });
    }
    // nomer lspop
    function nomerlspop(nop) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Spop/getformulirlspop'); ?>",
            data: "nop=" + nop,
            cache: false,
            success: function(data) {
                var z = data.split('|');
                if (z[0] != '') {
                    $('#THN_PELAYANAN_LSPOP').attr('value', z[0]);
                    $('#KATEGORI_PELAYANAN_LSPOP').attr('value', z[1]);
                    $('#NO_URUT_DALAM_LSPOP').attr('value', z[2]);
                    $('#NO_URUT_LSPOP').attr('value', z[3]);
                } else {
                    alert("NOP ini belum punya Nomor formulir/nomor formulir sudah digunakan");
                    $('#mySubmit').prop("disabled", true);
                    $('#KD_KELURAHAN_PEMOHON_LSPOP').val("");
                    $('#KD_BLOK_PEMOHON_LSPOP').val("");
                    $('#NO_URUT_PEMOHON_LSPOP').val("");
                    $('#KD_JNS_OP_PEMOHON_LSPOP').val("");
                };
                //alert(z[3]);
            }
        });
    }
</script>