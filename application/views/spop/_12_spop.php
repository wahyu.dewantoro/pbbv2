<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-3  control-label">Nomor KTP/ID <br>
                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
            </label>
            <div class="col-md-9">
                <?php $sbj = !empty($isi->SUBJEK_PAJAK_ID) ? $isi->SUBJEK_PAJAK_ID : null;  ?>
                <input type="text" class="form-control" required name="KTP_ID" value="<?php echo $sbj ?>" placeholder="Nomor KTP/ID" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Pekerjaan </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="PEKERJAAN">
                    <?php foreach ($pekerjaan as $kp => $kpv) { ?>
                        <option value="<?= $kp ?>"><?= $kp . ' - ' . $kpv ?> </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">NPWP </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="NPWP" value="" placeholder="NPWP" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jalan <br>
                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
            </label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="JALAN" value="" placeholder="Jalan" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">RW /RT </label>
            <div class="col-md-3">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RW_WP" value="<?php echo !empty($isi->RW_WP) ? $isi->RW_WP : null; ?>" placeholder="RW" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RT_WP" value="<?php echo !empty($isi->RT_WP) ? $isi->RT_WP : null; ?>" placeholder="RT" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kota / DATI II </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="DATI" value="<?php echo !empty($isi->KOTA_WP) ? $isi->KOTA_WP : null; ?>" placeholder="Kota / DATI II" id="dati2" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="col-md-3  control-label">No. Persil </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="PERSIL" value="<?php echo !empty($isi->NO_PERSIL) ? $isi->NO_PERSIL : null; ?>" placeholder="No. Persil" maxlength="5" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jalan-Blok/Kav/No </label>
            <div class="col-md-6">
                <input type="text" class="form-control" requiredd name="JALAN_OP" value="<?php echo !empty($isi->JALAN_OP) ? $isi->JALAN_OP : null; ?>" maxlength="15" placeholder="Jalan OP" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" requiredd name="BLOK_KAV_OP" value="<?php echo !empty($isi->BLOK_KAV_NO_OP) ? $isi->BLOK_KAV_NO_OP : null; ?>" maxlength="15" placeholder="Blok/Kav/No" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">ZNT </label>
            <div class="col-md-2">
                <select class="form-control chosen" style="width:100px" requiredd name="ZNT">
                    <option value="">Pilih</option>
                    <?php foreach ($znt as $ra) { ?>
                        <option value="<?php echo $ra->KD_ZNT; ?>" <?php $znt = !empty($bumi->KD_ZNT) ? $bumi->KD_ZNT : null;
                                                                    if ($znt == $ra->KD_ZNT) {
                                                                        echo "selected";
                                                                    } ?>><?php echo $ra->KD_ZNT; ?></option>
                    <?php } ?>
                </select>
            </div>
            <label class="col-md-3  control-label">Luas Tanah </label>
            <div class="col-md-4">
                <input type="number" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="LUAS_TANAH" value="<?php echo !empty($bumi->LUAS_BUMI) ? $bumi->LUAS_BUMI : null ?>" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3  control-label">Tanggal Pendataan </label>
            <div class="col-md-9">
                <input type="text" class="form-control " required name="TGL_PENDATAAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Penelitian </label>
            <div class="col-md-9">
                <input type="text" class="form-control " required name="TGL_PENELITIAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-3  control-label">Status WP </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="STATUS_WP">
                    <option value="">Pilih</option>
                    <?php foreach ($status_wp as $sw => $swv) { ?>
                        <option value="<?= $sw ?>"><?= $sw . ' - ' . $swv ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nama <br>
                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
            </label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="NAMA" value="<?php echo !empty($isi->NM_WP) ? $isi->NM_WP : null; ?>" placeholder="Nama" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">No. telp </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="TELP" value="<?php echo !empty($isi->TELP_WP) ? $isi->TELP_WP : null; ?>" placeholder="No. telp" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Blok / Kav /No </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="BLOK_KAV_WP" maxlength="15" placeholder="Blok / Kav /No" value="<?php echo !empty($isi->BLOK_KAV_NO_WP) ? $isi->BLOK_KAV_NO_WP : null; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="KELURAHAN" value="<?php echo !empty($isi->KELURAHAN_WP) ? $isi->KELURAHAN_WP : null; ?>" placeholder="Kelurahan" id="kelurahan" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kode Pos </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd value="<?php echo !empty($isi->KD_POS_WP) ? $isi->KD_POS_WP : null; ?>" name="KODE_POS" maxlength="5" placeholder="Kode Pos" />
            </div>
        </div><br>
        <div class="form-group">
            <label class="col-md-3  control-label">RW/RT </label>
            <div class="col-md-3">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RW_OP" value="<?php echo !empty($isi->RW_OP) ? $isi->RW_OP : null; ?>" placeholder="" />
            </div>
            <div class="col-md-3">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RT_OP" value="<?php echo !empty($isi->RT_OP) ? $isi->RT_OP : null; ?>" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Cabang </label>
            <div class="col-md-9">
                <input type="checkbox" name="CABANG" value='<?php $cbg = !empty($isi->KD_STATUS_CABANG) ? $isi->KD_STATUS_CABANG : null;
                                                            if ($cbg == 1) {
                                                                echo "1";
                                                            } else {
                                                                echo "0";
                                                            } ?>' <?php if ($cbg == 1) {
                                                                        echo "checked";
                                                                    } ?> class="minimal">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jenis Tanah </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="JENIS_TANAH">
                    <option value="">Pilih</option>
                    <?php $jb = !empty($bumi->JNS_BUMI) ? $bumi->JNS_BUMI : null; ?>
                    <?php foreach($jenis_tanah as $jt=>$vjt){?>
                        <option value="<?= $jt ?>"><?= $jt.' - '.$vjt?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3  control-label">NIP Pendata </label>
            <div class="col-md-9">
                <input type="text" class="form-control karyawan" requiredd name="NIP_PENDATA" id="" placeholder="NIP Pendata" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">NIP Peneliti </label>
            <div class="col-md-9">
                <input type="text" class="form-control karyawan" requiredd name="NIP_PENELITI" id="" placeholder="NIP Peneliti" />
            </div>
        </div><br>
        <div class="form-group">
            <div class="btn-group">
                <button type="" onclick="return confirm('KONFIRMASI PENYIMPANAN DATA?')" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Batal</button>
            </div>
        </div>
    </div>
</div>