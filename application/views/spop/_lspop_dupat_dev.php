<hr>
<div class="row">
<div class="col-md-12">
        <div class="row">
            <div class="col-md-12" style=" margin-bottom: 20px; text-align: center"><span style="background-color: green; font-size: 14px" class="badge badge-warning">Rincian Data Bangunan</span></div>
        </div>
        <div class="row">
        <div class="col-md-6">
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Jenis Bangunan<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" tabIndex="26" required name="KD_JPB">
                                        <option value="">Pilih</option>
                                        <option value="01" <?php if($KD_JPB=='01'){ echo 'selected'; }?>>01 - PERUMAHAN</option>
                                        <option value="02" <?php if($KD_JPB=='02'){ echo 'selected'; }?>>02 - PERKANTORAN SWASTA</option>
                                        <option value="03" <?php if($KD_JPB=='03'){ echo 'selected'; }?>>03 - PABRIK</option>
                                        <option value="04" <?php if($KD_JPB=='04'){ echo 'selected'; }?>>04 - TOKO/APOTEK/PASAR/RUKO</option>
                                        <option value="05" <?php if($KD_JPB=='05'){ echo 'selected'; }?>>05 - RUMAH SAKIT/KLINIK</option>
                                        <option value="06" <?php if($KD_JPB=='06'){ echo 'selected'; }?>>06 - OLAH RAGA/REKREASI</option>
                                        <option value="07" <?php if($KD_JPB=='07'){ echo 'selected'; }?>>07 - HOTEL/WISMA</option>
                                        <option value="08" <?php if($KD_JPB=='08'){ echo 'selected'; }?>>08 - BENGKEL/GUDANG</option>
                                        <option value="09" <?php if($KD_JPB=='09'){ echo 'selected'; }?>>09 - GEDUNG PEMERINTAH</option>
                                        <option value="10" <?php if($KD_JPB=='10'){ echo 'selected'; }?>>10 - LAIN - LAIN</option>
                                        <option value="11" <?php if($KD_JPB=='11'){ echo 'selected'; }?>>11 - BANGUNAN TIDAK KENA PAJAK</option>
                                        <option value="12" <?php if($KD_JPB=='12'){ echo 'selected'; }?>>12 - BANGUNAN PARKIR</option>
                                        <option value="13" <?php if($KD_JPB=='13'){ echo 'selected'; }?>>13 - APARTEMEN</option>
                                        <option value="14" <?php if($KD_JPB=='14'){ echo 'selected'; }?>>14 - POMPA BENSIN</option>
                                        <option value="15" <?php if($KD_JPB=='15'){ echo 'selected'; }?>>15 - TANGKI MINYAK</option>
                                        <option value="16" <?php if($KD_JPB=='16'){ echo 'selected'; }?>>16 - GEDUNG SEKOLAH</option>
                       </select>
                    </div>
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Luas Bangunan<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-4">
                        <input type="number" tabIndex="27"  class="form-control" required name="LUAS_BNG" placeholder="Luas Bng" value="<?= $LUAS_BNG ?>" />
                    </div>
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Jumlah Lantai<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-3">
                        <input type="number" tabIndex="28" class="form-control"  required name="JML_LANTAI_BNG" placeholder="Jml Lantai" value="<?= $JML_LANTAI_BNG ?>" />
                    </div>
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Tahun Dibangun<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-2">
                        <input type="text" tabIndex="29" maxlength="4" class="form-control" required name="THN_DIBANGUN_BNG" placeholder="Tahun" value="<?= $THN_DIBANGUN_BNG ?>" />
                    </div>
                    <label class="col-md-4 control-label">Tahun Renovasi<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" maxlength="4" required  name="THN_RENOVASI_BNG" placeholder="Tahun" value="<?= $THN_RENOVASI_BNG ?>" />
                    </div>
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Kondisi Bangunan<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" required name="KONDISI_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($KONDISI_BNG=='1'){ echo 'selected'; }?>>1 - SANGAT BAIK</option>
                                        <option value="2" <?php if($KONDISI_BNG=='2'){ echo 'selected'; }?>>2 - BAIK</option>
                                        <option value="3" <?php if($KONDISI_BNG=='3'){ echo 'selected'; }?>>3 - SEDANG</option>
                                        <option value="4" <?php if($KONDISI_BNG=='4'){ echo 'selected'; }?>>4 - JELEK</option>
                                    </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Konstruksi<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" required  name="JNS_KONSTRUKSI_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($JNS_KONSTRUKSI_BNG=='1'){ echo 'selected'; }?>>1 - BAJA</option>
                                        <option value="2" <?php if($JNS_KONSTRUKSI_BNG=='2'){ echo 'selected'; }?>>2 - BETON</option>
                                        <option value="3" <?php if($JNS_KONSTRUKSI_BNG=='3'){ echo 'selected'; }?>>3 - BATU BATA</option>
                                        <option value="4" <?php if($JNS_KONSTRUKSI_BNG=='4'){ echo 'selected'; }?>>4 - KAYU</option>
                                    </select>
                    </div>
                    
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Atap<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" required  name="JNS_ATAP_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($JNS_ATAP_BNG=='1'){ echo 'selected'; }?>>1 - DECRABOM/BETON/GTG GLAZUR</option>
                                        <option value="2" <?php if($JNS_ATAP_BNG=='2'){ echo 'selected'; }?>>2 - GTG BETON/ALUMUNIUM</option>
                                        <option value="3" <?php if($JNS_ATAP_BNG=='3'){ echo 'selected'; }?>>3 - GTG BIASA/SIRAP</option>
                                        <option value="4" <?php if($JNS_ATAP_BNG=='4'){ echo 'selected'; }?>>4 - ASBES</option>
                                        <option value="5" <?php if($JNS_ATAP_BNG=='5'){ echo 'selected'; }?>>5 - SENG</option>
                                    </select>
                    </div>
                    
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Dinding<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" required  name="KD_DINDING">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($KD_DINDING=='1'){ echo 'selected'; }?>>1 - KACA/ALUMUNIUM</option>
                                        <option value="2" <?php if($KD_DINDING=='2'){ echo 'selected'; }?>>2 - BETON</option>
                                        <option value="3" <?php if($KD_DINDING=='3'){ echo 'selected'; }?>>3 - BATU BATA/CONBLOK</option>
                                        <option value="4" <?php if($KD_DINDING=='4'){ echo 'selected'; }?>>4 - KAYU</option>
                                        <option value="5" <?php if($KD_DINDING=='5'){ echo 'selected'; }?>>5 - SENG</option>
                                        <option value="6" <?php if($KD_DINDING=='6'){ echo 'selected'; }?>>6 - TIDAK ADA</option>
                                    </select>
                    </div>
                    
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Lantai<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" required  name="KD_LANTAI">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($KD_LANTAI=='1'){ echo 'selected'; }?>>1 - MARMER </option>
                                        <option value="2" <?php if($KD_LANTAI=='2'){ echo 'selected'; }?>>2 - KERAMIK</option>
                                        <option value="3" <?php if($KD_LANTAI=='3'){ echo 'selected'; }?>>3 - TERASO</option>
                                        <option value="4" <?php if($KD_LANTAI=='4'){ echo 'selected'; }?>>4 - UBIN PC/PAPAN</option>
                                        <option value="5" <?php if($KD_LANTAI=='5'){ echo 'selected'; }?>>5 - SEMEN</option>
                                    </select>
                    </div>
                    
                </div>
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Langit-langit<br>
                        <font color="red" style='font-size: 11px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <select class="form-control chosen"  required name="KD_LANGIT_LANGIT">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if($KD_LANGIT_LANGIT=='1'){ echo 'selected'; }?>>1 - AKUSTIK/JATI </option>
                                        <option value="2" <?php if($KD_LANGIT_LANGIT=='2'){ echo 'selected'; }?>>2 - TRIPLRK/ASBES BAMBU</option>
                                        <option value="3" <?php if($KD_LANGIT_LANGIT=='3'){ echo 'selected'; }?>>3 - TIDAK ADA</option>
                                    </select>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
    
    <hr>
        <div class="row">
            <div class="col-md-12" style=" margin-bottom: 20px; text-align: center"><span style="background-color: blue; font-size: 14px" class="badge badge-warning">Penilaian Individu</span></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Nilai Sistem Bangun<br>
                        <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control " required name="NILAI_SISTEM" data-inputmask="'mask': '99-99-9999'" id="NILAI_SISTEM" value="<?= $NILAI_SISTEM ?>" />
                    </div>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row" style="margin-bottom: 0px">
                    <label class="col-md-4 control-label">Nilai Individu<br>
                        <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" requiredd name="NILAI_INDIVIDU" id="" placeholder="" value="<?= $NILAI_INDIVIDU ?>" />
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</div>
<hr>
<style>
  /*   .biru {
        outline: 1px solid blue;
    }

    .hijau {
        outline: 1px solid green;
    } */
</style>