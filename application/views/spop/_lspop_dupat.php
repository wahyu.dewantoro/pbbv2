<div class="row">

    <div class="col-md-12">
        <div class="">
            <p class="text-center">----------------------<b>Data Bangunan</b>----------------------</p>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6  control-label">Jenis <br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" tabIndex="26" required name="KD_JPB">
                                        <option value="">Pilih</option>
                                        <option value="01">01 - PERUMAHAN</option>
                                        <option value="02">02 - PERKANTORAN SWASTA</option>
                                        <option value="03">03 - PABRIK</option>
                                        <option value="04">04 - TOKO/APOTEK/PASAR/RUKO</option>
                                        <option value="05">05 - RUMAH SAKIT/KLINIK</option>
                                        <option value="06">06 - OLAH RAGA/REKREASI</option>
                                        <option value="07">07 - HOTEL/WISMA</option>
                                        <option value="08">08 - BENGKEL/GUDANG</option>
                                        <option value="09">09 - GEDUNG PEMERINTAH</option>
                                        <option value="10">10 - LAIN - LAIN</option>
                                        <option value="11">11 - BANGUNAN TIDAK KENA PAJAK</option>
                                        <option value="12">12 - BANGUNAN PARKIR</option>
                                        <option value="13">13 - APARTEMEN</option>
                                        <option value="14">14 - POMPA BENSIN</option>
                                        <option value="15">15 - TANGKI MINYAK</option>
                                        <option value="16">16 - GEDUNG SEKOLAH</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Kondisi<br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" required name="KONDISI_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - SANGAT BAIK</option>
                                        <option value="2">2 - BAIK</option>
                                        <option value="3">3 - SEDANG</option>
                                        <option value="4">4 - JELEK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">L. Bangunan <br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-6">
                                    <input type="number" tabIndex="27"  class="form-control" required name="LUAS_BNG" id="" placeholder="Luas Bng" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Jml Lantai <br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-6">
                                    <input type="number" tabIndex="28" class="form-control"  required name="JML_LANTAI_BNG" id="" placeholder="Jml Lantai" value="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">----------------------<b>Tahun</b>----------------------</p>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-6 control-label"> Dibangun <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-6">
                                <input type="text" tabIndex="29" maxlength="4" class="form-control" required name="THN_DIBANGUN_BNG" id="" placeholder="Tahun" value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Renovasi </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" maxlength="4" requiredd name="THN_RENOVASI_BNG" id="" placeholder="Tahun" value="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Konstruksi </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" requiredd name="JNS_KONSTRUKSI_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - BAJA</option>
                                        <option value="2">2 - BETON</option>
                                        <option value="3">3 - BATU BATA</option>
                                        <option value="4">4 - KAYU</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Atap </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" requiredd name="JNS_ATAP_BNG">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - DECRABOM/BETON/GTG GLAZUR</option>
                                        <option value="2">2 - GTG BETON/ALUMUNIUM</option>
                                        <option value="3">3 - GTG BIASA/SIRAP</option>
                                        <option value="4">4 - ASBES</option>
                                        <option value="5">5 - SENG</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Dinding </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" requiredd name="KD_DINDING">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - KACA/ALUMUNIUM</option>
                                        <option value="2">2 - BETON</option>
                                        <option value="3">3 - BATU BATA/CONBLOK</option>
                                        <option value="4">4 - KAYU</option>
                                        <option value="5">5 - SENG</option>
                                        <option value="6">6 - TIDAK ADA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Lantai </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" requiredd name="KD_LANTAI">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - MARMER </option>
                                        <option value="2">2 - KERAMIK</option>
                                        <option value="3">3 - TERASO</option>
                                        <option value="4">4 - UBIN PC/PAPAN</option>
                                        <option value="5">5 - SEMEN</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Langit - langit </label>
                                <div class="col-md-6">
                                    <select class="form-control chosen" requiredd name="KD_LANGIT_LANGIT">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - AKUSTIK/JATI </option>
                                        <option value="2">2 - TRIPLRK/ASBES BAMBU</option>
                                        <option value="3">3 - TIDAK ADA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="">
            <p class="text-center">----------------------<b>Penilaian Individu</b>----------------------</p>
            <div class="form-group">
                <label class="col-md-2 control-label">Nilai Sistem Bangunan </label>
                <div class="col-md-3">
                    <input type="text" class="form-control " required name="NILAI_SISTEM" data-inputmask="'mask': '99-99-9999'" id="NILAI_SISTEM" value="" />
                </div>
                <label class="col-md-3 control-label">Nilai Individu</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" requiredd name="NILAI_INDIVIDU" id="" placeholder="" value="" />
                </div>
            </div>
        </div>
    </div>
</div>
<style>
  /*   .biru {
        outline: 1px solid blue;
    }

    .hijau {
        outline: 1px solid green;
    } */
</style>