<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('content')
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" id='myTab'>
        <li class="<?php echo $CI->session->userdata('t_formulir'); ?>"><a href="#tab_3" data-toggle="tab">Generate Formulir</a></li>
        <li class="<?php echo $CI->session->userdata('t_spop'); ?>"><a href="#tab_1" data-toggle="tab">SPOP</a></li>
        <li class="<?php echo $CI->session->userdata('t_lspop'); ?>"><a href="#tab_2" data-toggle="tab">LSPOP</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane <?php echo $CI->session->userdata('t_formulir'); ?>" id="tab_3">
            <div class="box-body">
                <div class="row">
                    <form class='form-horizontal' action="<?php echo base_url('spop/proses_generate_nomor'); ?>" method="post" enctype="multipart/form-data">
                        <div class="col-md-8" style='border-right:1px solid red; border-bottom:1px solid red;'>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tahun</label>
                                <div class="col-md-9">
                                    <select name="TAHUN_AJUAN" id="TAHUN_AJUAN" class="form-control" required>
                                        <option value="">Pilih Tahun</option>
                                        <?php $th = date('Y');
                                        for ($i = $th; $i > ($th - 2); $i--) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jenis Formulir</label>
                                <div class="col-md-9">
                                    <select class="form-control chosen" required name="KD_JNS_FORMULIR" id="KD_JNS_PELAYANAN" onchange="jenis(this.value)">
                                        <option value="">Pilih</option>
                                        <option value="0001">0001-Hasil Verifikasi Lapangan</option>
                                        <option value="0501">0501-Hasil Verifikasi Kantor</option>
                                        <option value="1001">1001-Pembatalan</option>
                                        <option value="2001">2001-Pemuktahiran</option>
                                        <option value="3001">3001-SISMIOP</option>
                                        <option value="5001">5001-Individu & Semi Individu</option>
                                        <option value="6001">6001-Keberatan & Banding</option>
                                        <option value="7001">7001-Fasum</option>
                                        <option value="8001">8001-Penghapusan Bangunan</option>
                                        <option value="9001">9001-ZNT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP <br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-9" id='teste'>
                                    <table width='100%'>
                                        <tr>
                                            <td width='12%'><input type="text" required maxlength="2" class="form-control" value="35" name="KD_PROPINSI" id=""></td>
                                            <td width='12%'><input type="text" required maxlength="2" class="form-control" value="" name="KD_DATI2" id=""></td>
                                            <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_KECAMATAN" id=""></td>
                                            <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_KELURAHAN" id=""></td>
                                            <td width='15%'><input type="text" required maxlength="3" class="form-control" value="" name="KD_BLOK" id=""></td>
                                            <td><input type="text" required maxlength="4" class="form-control" value="" name="NO_URUT_NOP" id=""></td>
                                            <td width='9%'><input type="text" required maxlength="1" class="form-control" value="" name="KD_JNS_OP" id=""></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jumlah Bangunan</label>
                                <div class="col-md-9">
                                    <input type="text" tabIndex="24" required class="form-control " name="NO_BNG" value="1" placeholder="Jumlah Bangunan" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <button type="" id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <table id="example2" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">No</th>
                                <th>Nomor Formulir</th>
                                <th>NOP</th>
                                <th>Keterangan</th>
                                <th>Tanggal</th>
                                <th>STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($nomor as $rk) { ?>
                                <tr>
                                    <td align="center"><?php echo $no ?></td>
                                    <td><?php echo $rk->NO_FORMULIR ?></td>
                                    <td><?php echo $rk->NOP ?></td>
                                    <td>NOMOR <?php echo $rk->KETERANGAN_NOMOR ?></td>
                                    <td><?php echo $rk->TGL_PEREKAMAN ?></td>
                                    <td><?php if ($rk->STATUS == 1) {
                                            echo "Terpakai";
                                        } else {
                                            echo "Belum Terpakai";
                                        } ?></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane <?php echo $CI->session->userdata('t_spop'); ?>" id="tab_1">

            <div class="box-body">
                <div class="row">
                    <form class='form-horizontal' action="<?php echo base_url('spop/form_spop'); ?>" method="get" enctype="multipart/form-data">
                        <input type="hidden" name="JENIS" value="SPOP">
                        <div class="col-md-8" style='border-right:1px solid red; border-bottom:1px solid red;'>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jenis Transaksi </label>
                                <div class="col-md-9">
                                    <select class="form-control chosen" required name="KD_JNS_PELAYANAN" id="KD_JNS_PELAYANAN" onchange="jenis(this.value)">
                                        <option value="">Pilih</option>
                                        <option value="1" <?php if ($CI->session->userdata('JNS_PELAYANAN_SPOP') == '1') {
                                                                echo "selected";
                                                            } ?>>11 - PEREKAMAN DATA OP </option>
                                        <option value="2" <?php if ($CI->session->userdata('JNS_PELAYANAN_SPOP') == '2') {
                                                                echo "selected";
                                                            } ?>>12 - PEMUTAKHIRAN DATA OP</option>
                                        <option value="3" <?php if ($CI->session->userdata('JNS_PELAYANAN_SPOP') == '3') {
                                                                echo "selected";
                                                            } ?>>13 - Pengahpusan OP</option>
                                        <option value="4" <?php if ($CI->session->userdata('JNS_PELAYANAN_SPOP') == '4') {
                                                                echo "selected";
                                                            } ?>>14 - Penghapusan Status OP Bersama</option>


                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP <br>
                                    <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                </label>
                                <div class="col-md-9" id='test'>
                                    <table width='100%'>
                                        <tr>
                                            <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_PROPINSI_SPOP'); ?>" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON"></td>
                                            <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_DATI2_SPOP'); ?>" name="KD_DATI2" id="KD_DATI2_PEMOHON"></td>
                                            <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KECAMATAN_SPOP'); ?>" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON"></td>
                                            <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KELURAHAN_SPOP'); ?>" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON"></td>
                                            <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_BLOK_SPOP'); ?>" name="KD_BLOK" id="KD_BLOK_PEMOHON"></td>
                                            <td><input type="text" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" value="<?php echo $CI->session->userdata('NO_URUT_NOP_SPOP'); ?>" name="NO_URUT_NOP" id="NO_URUT_PEMOHON"></td>
                                            <td width='9%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" value="<?php echo $CI->session->userdata('KD_JNS_OP_SPOP'); ?>" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Formulir </label>
                                <div class="col-md-9">
                                    <table>
                                        <tr>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('THN_PELAYANAN_SPOP'); ?>" name="THN_PELAYANAN" id="THN_PELAYANAN" placeholder="Tahun" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('BUNDEL_PELAYANAN_SPOP'); ?>" name="BUNDEL_PELAYANAN" id="KATEGORI_PELAYANAN" placeholder="Bundel" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_DALAM_SPOP'); ?>" name="NO_URUT_DALAM" id="NO_URUT_DALAM" placeholder="No urut Dalam" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_SPOP'); ?>" name="NO_URUT" id="NO_URUT" placeholder="No urut" required /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP Bersama</label>
                                <div class="col-md-9">
                                    <table width='100%'>
                                        <tr>
                                            <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON_BERSAMA" id=""></td>
                                            <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON_BERSAMA" id=""></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON_BERSAMA" id=""></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON_BERSAMA" id=""></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_BLOK_PEMOHON_BERSAMA" id=""></td>
                                            <td><input type="text" maxlength="4" class="form-control" name="NO_URUT_PEMOHON_BERSAMA" id=""></td>
                                            <td width='9%'><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON_BERSAMA" id=""></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP Asal</label>
                                <div class="col-md-9">
                                    <table width='100%'>
                                        <tr>
                                            <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI_ASAL" id="" value="<?php echo $CI->session->userdata('KD_PROPINSI_ASAL_SPOP'); ?>"></td>
                                            <td width='13%'><input type="text" maxlength="2" class="form-control" name="KD_DATI2_ASAL" id="" value="<?php echo $CI->session->userdata('KD_DATI2_ASAL_SPOP'); ?>"></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN_ASAL" id="" value="<?php echo $CI->session->userdata('KD_KECAMATAN_ASAL_SPOP'); ?>"></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN_ASAL" id="" value="<?php echo $CI->session->userdata('KD_KELURAHAN_ASAL_SPOP'); ?>"></td>
                                            <td width='16%'><input type="text" maxlength="3" class="form-control" name="KD_BLOK_ASAL" id="" value="<?php echo $CI->session->userdata('KD_BLOK_ASAL_SPOP'); ?>"></td>
                                            <td><input type="text" maxlength="4" class="form-control" name="NO_URUT_ASAL" id="" value="<?php echo $CI->session->userdata('NO_URUT_ASAL_SPOP'); ?>"></td>
                                            <td width='9%'><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP_ASAL" id="" value="<?php echo $CI->session->userdata('KD_JNS_OP_ASAL_SPOP'); ?>"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <button type="" id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($CI->session->userdata('JNS_PELAYANAN_SPOP') != '') { ?>
                                <form class='form-horizontal' action="<?php echo base_url('spop/create_spop'); ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value='<?php echo $CI->session->userdata('JNS_PELAYANAN_SPOP'); ?>' name='KD_JNS_PELAYANAN'>
                                    <input type="hidden" value='<?php echo $NO_FORMULIR_SPOP; ?>' name='NO_FORMULIR_SPOP'>
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_PROPINSI_SPOP'); ?>" name="KD_PROPINSI">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_DATI2_SPOP'); ?>" name="KD_DATI2">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_KECAMATAN_SPOP'); ?>" name="KD_KECAMATAN">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_KELURAHAN_SPOP'); ?>" name="KD_KELURAHAN">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_BLOK_SPOP'); ?>" name="KD_BLOK">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('NO_URUT_NOP_SPOP'); ?>" name="NO_URUT_NOP">
                                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_JNS_OP_SPOP'); ?>" name="KD_JNS_OP">
                                    <input type="hidden" value='<?php echo $nop_asal; ?>' name='NOP_ASAL'>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">Nomor KTP/ID <br>
                                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                            </label>
                                            <div class="col-md-9">
                                                <?php $sbj = !empty($isi->SUBJEK_PAJAK_ID) ? $isi->SUBJEK_PAJAK_ID : null;  ?>
                                                <input type="text" class="form-control" required name="KTP_ID" value="<?php echo $sbj ?>" placeholder="Nomor KTP/ID" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Pekerjaan </label>
                                            <div class="col-md-9">
                                                <select class="form-control chosen" requiredd name="PEKERJAAN">
                                                    <?php $spw = !empty($isi->STATUS_PEKERJAAN_WP) ? $isi->STATUS_PEKERJAAN_WP : 0; ?>
                                                    <option value="0" <?php if ($spw == '0') {
                                                                            echo "selected";
                                                                        } ?>>0 - LAINNYA</option>
                                                    <option value="1" <?php if ($spw == '1') {
                                                                            echo "selected";
                                                                        } ?>>1 - PNS</option>
                                                    <option value="2" <?php if ($spw == '2') {
                                                                            echo "selected";
                                                                        } ?>>2 - ABRI</option>
                                                    <option value="3" <?php if ($spw == '3') {
                                                                            echo "selected";
                                                                        } ?>>3 - PENSIUNAN</option>
                                                    <option value="4" <?php if ($spw == '4') {
                                                                            echo "selected";
                                                                        } ?>>4 - BADAN</option>
                                                    <option value="5" <?php if ($spw == '5') {
                                                                            echo "selected";
                                                                        } ?>>5 - LAINNYA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">NPWP </label>
                                            <div class="col-md-9">
                                                <?php $npwp = !empty($isi->NPWP) ? $isi->NPWP : null; ?>
                                                <input type="text" class="form-control" requiredd name="NPWP" value="<?= $npwp ?>" placeholder="NPWP" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jalan <br>
                                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required name="JALAN" value="<?php echo !empty($isi->JALAN_WP) ? $isi->JALAN_WP : null; ?>" placeholder="Jalan" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">RW /RT </label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RW_WP" value="<?php echo !empty($isi->RW_WP) ? $isi->RW_WP : null; ?>" placeholder="RW" />
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RT_WP" value="<?php echo !empty($isi->RT_WP) ? $isi->RT_WP : null; ?>" placeholder="RT" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kota / DATI II </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" requiredd name="DATI" value="<?php echo !empty($isi->KOTA_WP) ? $isi->KOTA_WP : null; ?>" placeholder="Kota / DATI II" id="dati2" />
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">No. Persil </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" requiredd name="PERSIL" value="<?php echo !empty($isi->NO_PERSIL) ? $isi->NO_PERSIL : null; ?>" placeholder="No. Persil" maxlength="5" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jalan-Blok/Kav/No </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" requiredd name="JALAN_OP" value="<?php echo !empty($isi->JALAN_OP) ? $isi->JALAN_OP : null; ?>" maxlength="15" placeholder="Jalan OP" />
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" requiredd name="BLOK_KAV_OP" value="<?php echo !empty($isi->BLOK_KAV_NO_OP) ? $isi->BLOK_KAV_NO_OP : null; ?>" maxlength="15" placeholder="Blok/Kav/No" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ZNT </label>
                                            <div class="col-md-2">
                                                <select class="form-control chosen" style="width:100px" requiredd name="ZNT">
                                                    <option value="">Pilih</option>
                                                    <?php foreach ($znt as $ra) { ?>
                                                        <option value="<?php echo $ra->KD_ZNT; ?>" <?php $znt = !empty($bumi->KD_ZNT) ? $bumi->KD_ZNT : null;
                                                                                                    if ($znt == $ra->KD_ZNT) {
                                                                                                        echo "selected";
                                                                                                    } ?>><?php echo $ra->KD_ZNT; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <label class="col-md-3  control-label">Luas Tanah </label>
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="LUAS_TANAH" value="<?php echo !empty($bumi->LUAS_BUMI) ? $bumi->LUAS_BUMI : null ?>" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">Tanggal Pendataan </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control " required name="TGL_PENDATAAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tanggal Penelitian </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control " required name="TGL_PENELITIAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y'); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">Status WP </label>
                                            <div class="col-md-9">
                                                <select class="form-control chosen" requiredd name="STATUS_WP">
                                                    <option value="">Pilih</option>
                                                    <?php $swpa = !empty($isi->KD_STATUS_WP) ? $isi->KD_STATUS_WP : null; ?>
                                                    <option value="1" <?php if ($swpa == '1') {
                                                                            echo "selected";
                                                                        } ?>>1 - PEMILIK</option>
                                                    <option value="2" <?php if ($swpa == '2') {
                                                                            echo "selected";
                                                                        } ?>>2 - PENYEWA</option>
                                                    <option value="3" <?php if ($swpa == '3') {
                                                                            echo "selected";
                                                                        } ?>>3 - PENGELOLA</option>
                                                    <option value="4" <?php if ($swpa == '4') {
                                                                            echo "selected";
                                                                        } ?>>4 - PEMAKAI</option>
                                                    <option value="5" <?php if ($swpa == '5') {
                                                                            echo "selected";
                                                                        } ?>>5 - SENGKETA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Nama <br>
                                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required name="NAMA" value="<?php echo !empty($isi->NM_WP) ? $isi->NM_WP : null; ?>" placeholder="Nama" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">No. telp </label>
                                            <div class="col-md-9">
                                                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="TELP" value="<?php echo !empty($isi->TELP_WP) ? $isi->TELP_WP : null; ?>" placeholder="No. telp" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Blok / Kav /No </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" requiredd name="BLOK_KAV_WP" maxlength="15" placeholder="Blok / Kav /No" value="<?php echo !empty($isi->BLOK_KAV_NO_WP) ? $isi->BLOK_KAV_NO_WP : null; ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kelurahan </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" requiredd name="KELURAHAN" value="<?php echo !empty($isi->KELURAHAN_WP) ? $isi->KELURAHAN_WP : null; ?>" placeholder="Kelurahan" id="kelurahan" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kode Pos </label>
                                            <div class="col-md-9">
                                                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd value="<?php echo !empty($isi->KD_POS_WP) ? $isi->KD_POS_WP : null; ?>" name="KODE_POS" maxlength="5" placeholder="Kode Pos" />
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">RW/RT </label>
                                            <div class="col-md-3">
                                                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RW_OP" value="<?php echo !empty($isi->RW_OP) ? $isi->RW_OP : null; ?>" placeholder="" />
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RT_OP" value="<?php echo !empty($isi->RT_OP) ? $isi->RT_OP : null; ?>" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Cabang </label>
                                            <div class="col-md-9">
                                                <input type="checkbox" name="CABANG" value='<?php $cbg = !empty($isi->KD_STATUS_CABANG) ? $isi->KD_STATUS_CABANG : null;
                                                                                            if ($cbg == 1) {
                                                                                                echo "1";
                                                                                            } else {
                                                                                                echo "0";
                                                                                            } ?>' <?php if ($cbg == 1) {
                                                                                                    echo "checked";
                                                                                                } ?> class="minimal">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis Tanah </label>
                                            <div class="col-md-9">
                                                <select class="form-control chosen" requiredd name="JENIS_TANAH">
                                                    <option value="">Pilih</option>
                                                    <?php $jb = !empty($bumi->JNS_BUMI) ? $bumi->JNS_BUMI : null; ?>
                                                    <option value="1" <?php if ($jb == '1') {
                                                                            echo "selected";
                                                                        } ?>>1 - TANAH + BANGUNAN</option>
                                                    <option value="2" <?php if ($jb == '2') {
                                                                            echo "selected";
                                                                        } ?>>2 - KAVLING SIAP BANGUN</option>
                                                    <option value="3" <?php if ($jb == '3') {
                                                                            echo "selected";
                                                                        } ?>>3 - TANAH KOSONG</option>
                                                    <option value="4" <?php if ($jb == '4') {
                                                                            echo "selected";
                                                                        } ?>>4 - FASILITAS UMUM</option>
                                                    <option value="5" <?php if ($jb == '5') {
                                                                            echo "selected";
                                                                        } ?>>5 - LAIN-LAIN</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3  control-label">NIP Pendata </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control karyawan" requiredd name="NIP_PENDATA" id="" placeholder="NIP Pendata" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">NIP Peneliti </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control karyawan"     requiredd name="NIP_PENELITI" id="" placeholder="NIP Peneliti" />
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <div class="btn-group">
                                                <button type="" onclick="return confirm('KONFIRMASI PENYIMPANAN DATA?')" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            <?php } else {
                                echo "";
                            } ?>
                        </div>
                    </div>
                </div>
            </div> <!-- body -->

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane  <?php echo $CI->session->userdata('t_lspop'); ?>" id="tab_2">
            <form class='form-horizontal' action="<?php echo base_url('spop/form_spop'); ?>" method="get" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8" style='border-bottom:1px solid red;border-right:1px solid red;'>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jenis Transaksi </label>
                                <div class="col-md-9">
                                    <input type="hidden" name="JENIS" value="LSPOP">
                                    <select class="form-control chosen" tabIndex="1" required name="KD_JNS_PELAYANAN" id="KD_JNS_PELAYANAN_LSPOP" onchange="jenis_lspop(this.value)">
                                        <option value="">Piliih</option>
                                        <option value="1" <?php if ($CI->session->userdata('KD_JNS_PELAYANAN_LSPOP') == '1') {
                                                                echo "selected";
                                                            } ?>>21 - PEREKAMAN DATA BANGUNAN</option>
                                        <option value="2" <?php if ($CI->session->userdata('KD_JNS_PELAYANAN_LSPOP') == '2') {
                                                                echo "selected";
                                                            } ?>>22 - PEMUTAKHIRAN DATA BANGUNAN</option>
                                        <option value="3" <?php if ($CI->session->userdata('KD_JNS_PELAYANAN_LSPOP') == '3') {
                                                                echo "selected";
                                                            } ?>>23 - pengapusan data bangunan</option>
                                        <option value="4" <?php if ($CI->session->userdata('KD_JNS_PELAYANAN_LSPOP') == '4') {
                                                                echo "selected";
                                                            } ?>>24 - penilaian individu</option>


                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP </label>
                                <div class="col-md-9" id="test_LSPOP">
                                    <table width='100%'>
                                        <tr>
                                            <td width='12%'><input type="text" tabIndex="3" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_PROPINSI'); ?>" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON_LSPOP"></td>
                                            <td width='12%'><input type="text" tabIndex="4" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="<?php echo $CI->session->userdata('KD_DATI2'); ?>" name="KD_DATI2" id="KD_DATI2_PEMOHON_LSPOP"></td>
                                            <td width='15%'><input type="text" tabIndex="5" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KECAMATAN'); ?>" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON_LSPOP"></td>
                                            <td width='15%'><input type="text" tabIndex="6" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_KELURAHAN'); ?>" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON_LSPOP"></td>
                                            <td width='15%'><input type="text" tabIndex="7" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="<?php echo $CI->session->userdata('KD_BLOK'); ?>" name="KD_BLOK" id="KD_BLOK_PEMOHON_LSPOP"></td>
                                            <td><input type="text" tabIndex="8" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" value="<?php echo $CI->session->userdata('NO_URUT_NOP'); ?>" name="NO_URUT_NOP" id="NO_URUT_PEMOHON_LSPOP"></td>
                                            <td width='9%'><input type="text" tabIndex="9" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" value="<?php echo $CI->session->userdata('KD_JNS_OP'); ?>" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON_LSPOP"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Formulir </label>
                                <div class="col-md-9">
                                    <table>
                                        <tr>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('THN_PELAYANAN'); ?>" name="THN_PELAYANAN" id="THN_PELAYANAN_LSPOP" placeholder="Tahun" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="4" value="<?php echo $CI->session->userdata('BUNDEL_PELAYANAN'); ?>" name="BUNDEL_PELAYANAN" id="KATEGORI_PELAYANAN_LSPOP" placeholder="Bundel" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT_DALAM'); ?>" name="NO_URUT_DALAM" id="NO_URUT_DALAM_LSPOP" placeholder="No urut Dalam" required /></td>
                                            <td><input type="text" readonly class="form-control inputs" maxlength="3" value="<?php echo $CI->session->userdata('NO_URUT'); ?>" name="NO_URUT" id="NO_URUT_LSPOP" placeholder="No urut" required /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP Bersama</label>
                                <div class="col-md-9">
                                    <table>
                                        <tr>
                                            <td width='13%'><input type="text" tabIndex="10" maxlength="2" class="form-control" name="KD_PROPINSI_PEMOHON_B" id=""></td>
                                            <td width='13%'><input type="text" tabIndex="11" maxlength="2" class="form-control" name="KD_DATI2_PEMOHON_B" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="12" maxlength="3" class="form-control" name="KD_KECAMATAN_PEMOHON_B" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="13" maxlength="3" class="form-control" name="KD_KELURAHAN_PEMOHON_B" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="14" maxlength="3" class="form-control" name="KD_BLOK_PEMOHON_B" id=""></td>
                                            <td><input type="text" tabIndex="15" maxlength="4" class="form-control" name="NO_URUT_PEMOHON_B" id=""></td>
                                            <td width='9%'><input type="text" tabIndex="16" maxlength="1" class="form-control" name="KD_JNS_OP_PEMOHON_B" id=""></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  control-label">NOP Asal</label>
                                <div class="col-md-9">
                                    <table>
                                        <tr>
                                            <td width='13%'><input type="text" tabIndex="17" maxlength="2" class="form-control" name="KD_PROPINSI_ASAL" value="<?php echo $CI->session->userdata('KD_PROPINSI_ASAL'); ?>" id=""></td>
                                            <td width='13%'><input type="text" tabIndex="18" maxlength="2" class="form-control" name="KD_DATI2_ASAL" value="<?php echo $CI->session->userdata('KD_DATI2_ASAL'); ?>" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="19" maxlength="3" class="form-control" name="KD_KECAMATAN_ASAL" value="<?php echo $CI->session->userdata('KD_KECAMATAN_ASAL'); ?>" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="20" maxlength="3" class="form-control" name="KD_KELURAHAN_ASAL" value="<?php echo $CI->session->userdata('KD_KELURAHAN_ASAL'); ?>" id=""></td>
                                            <td width='16%'><input type="text" tabIndex="21" maxlength="3" class="form-control" name="KD_BLOK_ASAL" value="<?php echo $CI->session->userdata('KD_BLOK_ASAL'); ?>" id=""></td>
                                            <td><input type="text" tabIndex="22" maxlength="4" class="form-control" name="NO_URUT_ASAL" value="<?php echo $CI->session->userdata('NO_URUT_ASAL'); ?>" id=""></td>
                                            <td width='9%'><input type="text" tabIndex="23" maxlength="1" class="form-control" name="KD_JNS_OP_ASAL" value="<?php echo $CI->session->userdata('KD_JNS_OP_ASAL'); ?>" id=""></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Bangunan</label>
                                <div class="col-md-9" id='list_nomor'>
                                    <input type="text" readonly tabIndex="24" required class="form-control " id="NO_BNG" name="NO_BNG" value="<?php echo $CI->session->userdata('NO_BNG'); ?>" placeholder="No Bangunan" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">List Nomor Formulir</label>
                                <div class="col-md-9" id=''>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <button type="submit" tabIndex="25" id="mySubmit_lspop" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php if (!empty($CI->session->userdata('KD_JNS_PELAYANAN_LSPOP'))) { ?>
                <form class='form-horizontal' action="<?php echo base_url('Lspop/create_lspop'); ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" value='<?php echo $CI->session->userdata('KD_JNS_PELAYANAN_LSPOP'); ?>' name='KD_JNS_PELAYANAN'>
                    <input type="hidden" value='<?php echo $NO_FORMULIR_SPOP; ?>' name='NO_FORMULIR_SPOP'>
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_PROPINSI'); ?>" name="KD_PROPINSI">
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_DATI2'); ?>" name="KD_DATI2">
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_KECAMATAN'); ?>" name="KD_KECAMATAN">
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_KELURAHAN'); ?>" name="KD_KELURAHAN">
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_BLOK'); ?>" name="KD_BLOK">
                    <input type="hidden" value="<?php echo $CI->session->userdata('NO_URUT_NOP'); ?>" name="NO_URUT_NOP">
                    <input type="hidden" value="<?php echo $CI->session->userdata('KD_JNS_OP'); ?>" name="KD_JNS_OP">
                    <input type="hidden" value='<?php echo $nop_asal; ?>' name='NOP_ASAL'>
                    <input type="hidden" value='<?php echo $CI->session->userdata['NO_BNG']; ?>' name='NO_BNG'>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4  control-label">Jenis Bangunan <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-8">
                                <select class="form-control chosen" tabIndex="26" onchange="jenis_bangunan(this.value)" required name="KD_JPB">
                                    <option value="">Pilih</option>
                                    <option value="01">01 - PERUMAHAN</option>
                                    <option value="02">02 - PERKANTORAN SWASTA</option>
                                    <option value="03">03 - PABRIK</option>
                                    <option value="04">04 - TOKO/APOTEK/PASAR/RUKO</option>
                                    <option value="05">05 - RUMAH SAKIT/KLINIK</option>
                                    <option value="06">06 - OLAH RAGA/REKREASI</option>
                                    <option value="07">07 - HOTEL/WISMA</option>
                                    <option value="08">08 - BENGKEL/GUDANG</option>
                                    <option value="09">09 - GEDUNG PEMERINTAH</option>
                                    <option value="10">10 - LAIN - LAIN</option>
                                    <option value="11">11 - BANGUNAN TIDAK KENA PAJAK</option>
                                    <option value="12">12 - BANGUNAN PARKIR</option>
                                    <option value="13">13 - APARTEMEN</option>
                                    <option value="14">14 - POMPA BENSIN</option>
                                    <option value="15">15 - TANGKI MINYAK</option>
                                    <option value="16">16 - GEDUNG SEKOLAH</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Luas Bangunan <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-8">
                                <input type="number" tabIndex="27" onkeypress="return isNumberKey(event)" class="form-control" required name="LUAS_BNG" id="" placeholder="Luas Bangunan" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Jumlah Lantai <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-8">
                                <input type="number" tabIndex="28" class="form-control" onkeypress="return isNumberKey(event)" required name="JML_LANTAI_BNG" id="" placeholder="Jumlah Lantai" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tahun Dibangun <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-2">
                                <input type="text" tabIndex="29" maxlength="4" class="form-control" required name="THN_DIBANGUN_BNG" id="" placeholder="Tahun" value="" />
                            </div>
                            <label class="col-md-4 control-label">Tahun Renovasi </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" maxlength="4" requiredd name="THN_RENOVASI_BNG" id="" placeholder="Tahun" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Kondisi Bangunan <br>
                                <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                            </label>
                            <div class="col-md-8">
                                <select class="form-control chosen" required name="KONDISI_BNG">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - SANGAT BAIK</option>
                                    <option value="2">2 - BAIK</option>
                                    <option value="3">3 - SEDANG</option>
                                    <option value="4">4 - JELEK</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Konstruksi </label>
                            <div class="col-md-9">
                                <select class="form-control chosen" requiredd name="JNS_KONSTRUKSI_BNG">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - BAJA</option>
                                    <option value="2">2 - BETON</option>
                                    <option value="3">3 - BATU BATA</option>
                                    <option value="4">4 - KAYU</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Atap </label>
                            <div class="col-md-9">
                                <select class="form-control chosen" requiredd name="JNS_ATAP_BNG">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - DECRABOM/BETON/GTG GLAZUR</option>
                                    <option value="2">2 - GTG BETON/ALUMUNIUM</option>
                                    <option value="3">3 - GTG BIASA/SIRAP</option>
                                    <option value="4">4 - ASBES</option>
                                    <option value="5">5 - SENG</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dinding </label>
                            <div class="col-md-9">
                                <select class="form-control chosen" requiredd name="KD_DINDING">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - KACA/ALUMUNIUM</option>
                                    <option value="2">2 - BETON</option>
                                    <option value="3">3 - BATU BATA/CONBLOK</option>
                                    <option value="4">4 - KAYU</option>
                                    <option value="5">5 - SENG</option>
                                    <option value="6">6 - TIDAK ADA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Lantai </label>
                            <div class="col-md-9">
                                <select class="form-control chosen" requiredd name="KD_LANTAI">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - MARMER </option>
                                    <option value="2">2 - KERAMIK</option>
                                    <option value="3">3 - TERASO</option>
                                    <option value="4">4 - UBIN PC/PAPAN</option>
                                    <option value="5">5 - SEMEN</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Langit - langit </label>
                            <div class="col-md-9">
                                <select class="form-control chosen" requiredd name="KD_LANGIT_LANGIT">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - AKUSTIK/JATI </option>
                                    <option value="2">2 - TRIPLRK/ASBES BAMBU</option>
                                    <option value="3">3 - TIDAK ADA</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="id_1">
                    </div>
                    <div class="col-md-4" id="id_2">
                    </div>
                    <div class='col-md-12' id="id_3">
                    </div>
                    <div class='col-md-12'><br>
                        <div class="col-md-4" style='border-right:1px solid red;'>
                            <div class="form-group">
                                <label class="col-md-3  control-label">Daya Listrik(watt) </label>
                                <div class="col-md-9">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="DAYA_LISTRIK" id="" placeholder="Daya Listrik(watt)" value="" />
                                </div>
                            </div>
                            <h5 style="text-align:center;">AC</h5>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Split </label>
                                <div class="col-md-2">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="ACSPLIT" id="" placeholder="" value="" />
                                </div>
                                <label class="col-md-2 control-label">Windows </label>
                                <div class="col-md-2">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="ACWINDOW" id="" placeholder="" value="" />
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" class="minimal" name='ACSENTRAL' value='1'> <b>AC Central</b>
                                </div>
                            </div>
                            <h5 style="text-align:center;">KOLAM RENANG</h5>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Luas(m2) </label>
                                <div class="col-md-3">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />
                                </div>
                                <label class="col-md-2 control-label">Finishing </label>
                                <div class="col-md-4">
                                    <select class="form-control chosen" requiredd name="FINISHING_KOLAM">
                                        <option value="">Pilih</option>
                                        <option value="1">1 - DIPLESTER </option>
                                        <option value="2">2 - DENGAN PELAPIS</option>
                                    </select>
                                </div>
                            </div>
                            <h5 style="text-align:center;">IDENTITAS PENDATA</h5>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tgl Pendataan </label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control " required name="TGL_PENDATAAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" value="<?php echo date('d-m-Y'); ?>" />
                                </div>
                                <label class="col-md-3 control-label">NIP Pendata </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" requiredd name="NIP_PENDATA" id="" placeholder="" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tgl Pemeriksaan</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control " required name="TGL_PEMERIKSAAN" data-inputmask="'mask': '99-99-9999'" id="TGL_TERIMA_DOKUMEN_WP" value="<?php echo date('d-m-Y'); ?>" />
                                </div>
                                <label class="col-md-3 control-label">NIP Pemeriksa</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" requiredd name="NIP_PEMERIKSA" id="" placeholder="" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style='border-right:1px solid red;'>
                            <h5 style="text-align:center;">JUMLAH LIFT</h5>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Penumpang </label>
                                <div class="col-md-2">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LIFT_PENUMPANG" id="" placeholder="" value="" />
                                </div>
                                <label class="col-md-1 control-label">Kapsul </label>
                                <div class="col-md-2">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LIFT_KAPSUL" id="" placeholder="" value="" />
                                </div>
                                <label class="col-md-1 control-label">Barang </label>
                                <div class="col-md-2">
                                    <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LIFT_BARANG" id="" placeholder="" value="" />
                                </div>
                            </div>
                            <h5 style="text-align:center;">JUMLAH TANGGA BERJALAN DENGAN LEBAR</h5>
                            <div class="form-group">
                                <label class="col-md-3 control-label">
                                    <=80 m </label> <div class="col-md-3">
                                        <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="TGG_BERJALAN_A" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-3 control-label"> > = 80 m </label>
                            <div class="col-md-3">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="TGG_BERJALAN_B" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"> Panjang Pagar </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="PJG_PAGAR" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-2 control-label"> Bahan Pagar </label>
                            <div class="col-md-5">
                                <select class="form-control chosen" requiredd name="BHN_PAGAR">
                                    <option value="">Pilih</option>
                                    <option value="1">1 - BAJA/BESI</option>
                                    <option value="2">2 - BATA/BATAKO</option>
                                </select>
                            </div>
                        </div>
                        <h5 style="text-align:center;">PEMADAM KEBAKARAN</h5>
                        <div class="form-group">
                            <label class="col-md-3 control-label"> </label>
                            <div class="col-md-3">
                                <input type="checkbox" class="minimal" name="HYDRANT" value='0'> <b>Hydrant</b>
                            </div>
                            <div class="col-md-3">
                                <input type="checkbox" class="minimal" name="SPRINKLER" value='0'> <b>Sprinkler</b>
                            </div>
                            <div class="col-md-3">
                                <input type="checkbox" class="minimal" name="FIRE_ALARM" value='0'> <b>Fire Alarm</b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"> PABX </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JML_PABX" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label"> Kedalaman Sumur Artesis </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="SUMUR_ARTESIS" id="" placeholder="" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5 style="text-align:center;">LUAS PERKERASAN HALAMAN</h5>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Ringan </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_PERKERASAN_RINGAN" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label">Berat </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_PERKERASAN_BERAT" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Sedang </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_PERKERASAN_SEDANG" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label">Dengan Penutup Lantai </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_PERKERASAN_DG_TUTUP" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <h5 style="text-align:center;">JUMLAH LAPANGAN TENIS</h5>
                        <h5 style="text-align:left;"> --------------------------DENGAN LAMPU---------------------------TANPA LAMPU</h5>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Beton </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_LAMPU_BETON" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label">Beton </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_BETON" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Aspal </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_LAMPU_ASPAL" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label">Aspal </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_ASPAL" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tanah Liat/Rumput </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_LAMPU_RUMPUT" id="" placeholder="" value="" />
                            </div>
                            <label class="col-md-4 control-label">Tanah Liat/Rumput </label>
                            <div class="col-md-2">
                                <input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LAP_TENIS_RUMPUT" id="" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn-group">
                                <button type="" onclick="return confirm('KONFIRMASI PENYIMPANAN DATA?')" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Batal</button>
                            </div>
                        </div>
                    </div>

                </form>

            <?php } ?>

        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {

        $("#KD_JNS_PELAYANAN").change(function(e) {

            e.preventDefault();
            alert($(this).value());

            /* nomer = $(this).attr('data-id');
            jenis = $(this).attr('data-jenis');
            if (jenis == 'SPOP') {
                key = 1;
            } else if (jenis == 'LSPOP') {
                key = 2;
            } else {
                key = 0;
            } */
            // $('#myTab li a').eq(key).tab('show');

        });


        $('#KD_JNS_PELAYANAN').val("");

        $('#KD_JNS_PELAYANAN').change(function() {
            selectVal = $('#KD_JNS_PELAYANAN').val();

            if (selectVal == "") {
                $('#mySubmit').prop("disabled", true);
            } else {
                $('#mySubmit').prop("disabled", false);
            }
        })

    });



    function jenis(id) {
        //alert(id);
        if (id != '') {
            document.getElementById("test").innerHTML = '' +
                '<table width="100%">' +
                '<tr>' +
                '<td width="12%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON" value="35"></td>' +
                '<td width="12%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_DATI2" id="KD_DATI2_PEMOHON"></td>' +
                '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON"></td>' +
                '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON"></td>' +
                '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_BLOK" id="KD_BLOK_PEMOHON"></td>' +
                '<td><input type="text" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" name="NO_URUT_NOP" id="NO_URUT_PEMOHON"></td>' +
                '<td width="9%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON"></td>' +
                '</tr>' +
                '</table>';
            $(document).ready(function() {
                $(".form-control").keyup(function() {
                    if (this.value.length == this.maxLength) {
                        var nextIndex = $('input:text').index(this) + 1;
                        $('input:text')[nextIndex].focus();
                    }
                });
                /*document.getElementById('KD_PROPINSI_PEMOHON').value= "" ;*/
                $('#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON').on('change', function() {
                    var a = $('#KD_PROPINSI_PEMOHON').val();
                    var b = $('#KD_DATI2_PEMOHON').val();
                    var c = $('#KD_KECAMATAN_PEMOHON').val();
                    var d = $('#KD_KELURAHAN_PEMOHON').val();
                    var e = $('#KD_BLOK_PEMOHON').val();
                    var f = $('#NO_URUT_PEMOHON').val();
                    var g = $('#KD_JNS_OP_PEMOHON').val();
                    /*var h   =$('#KD_JNS_PELAYANAN').val();*/
                    if (g == '') {
                        var nop = '';
                    } else {
                        var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g + "=" + id;;
                        //alert(nop);
                        /////formulir/////
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('Spop/getformulir'); ?>",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(data) {
                                //alert(data);   
                                var z = data.split('|');
                                if (z[0] != '') {
                                    $('#THN_PELAYANAN').attr('value', z[0]);
                                    $('#KATEGORI_PELAYANAN').attr('value', z[1]);
                                    $('#NO_URUT_DALAM').attr('value', z[2]);
                                    $('#NO_URUT').attr('value', z[3]);
                                } else {
                                    alert("NOP ini belum punya Nomor formulir/nomor formulir sudah digunakan");
                                    $('#mySubmit').prop("disabled", true);
                                    $('#KD_KELURAHAN_PEMOHON').val("");
                                    $('#KD_BLOK_PEMOHON').val("");
                                    $('#NO_URUT_PEMOHON').val("");
                                    $('#KD_JNS_OP_PEMOHON').val("");
                                };
                                //alert(z[3]);
                            }
                        });
                        /////////////
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('Spop/getNop'); ?>",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(data) {
                                var sp = data.split('|');
                                //alert(data);                              
                                if (sp[0] == 1 && sp[1] == 1) {
                                    alert("NOP SUDAH TERDAFTAR");
                                    $('#KD_KELURAHAN_PEMOHON').val("");
                                    $('#KD_BLOK_PEMOHON').val("");
                                    $('#NO_URUT_PEMOHON').val("");
                                    $('#KD_JNS_OP_PEMOHON').val("");
                                    $('#mySubmit').prop("disabled", true);
                                } else if (sp[0] == 0 && sp[1] == 2) {
                                    alert("NOP BELUM TERDAFTAR");
                                    $('#KD_KELURAHAN_PEMOHON').val("");
                                    $('#KD_BLOK_PEMOHON').val("");
                                    $('#NO_URUT_PEMOHON').val("");
                                    $('#KD_JNS_OP_PEMOHON').val("");
                                    $('#mySubmit').prop("disabled", true);
                                    //document.getElementById('KD_JNS_OP_PEMOHON').value= "" ;
                                } else {
                                    $('#mySubmit').prop("disabled", false);
                                };
                            }
                        });
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('Spop/getform'); ?>",
                        data: "nop=" + nop,
                        cache: false,
                        success: function(data) {
                            //alert(data);
                            $('#detail_from').html(data);
                        }
                    });
                });
            });


        } else {

        };


    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(this).ready(function() {
        $("#kelurahan").autocomplete({
            minLength: 1,
            source: function(req, add) {
                $.ajax({
                    url: "<?php echo base_url(); ?>spop/lookup_kelurahan",
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function(data) {
                        if (data.response == "true") {
                            add(data.message);
                        }
                    },
                });
            },
        });
    });
    $(this).ready(function() {
        $("#dati2").autocomplete({
            minLength: 1,
            source: function(req, add) {
                $.ajax({
                    url: "<?php echo base_url(); ?>spop/lookup_dati2",
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function(data) {
                        if (data.response == "true") {
                            add(data.message);
                        }
                    },
                });
            },
        });
    });
    $(this).ready(function() {
        $(".karyawan").autocomplete({
            minLength: 1,
            source: function(req, add) {
                $.ajax({
                    url: "<?php echo base_url(); ?>spop/lookup_karyawan",
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function(data) {
                        if (data.response == "true") {
                            add(data.message);
                        }
                    },
                });
            },
        });
    });

    function stopRKey(evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type == "text")) {
            return false;
        }
    }
    document.onkeypress = stopRKey;


    $('body').on('keypress', 'input, select, textarea', function(e) {
        var self = $(this),
            form = self.parents('form:eq(0)'),
            focusable, next;
        if (e.keyCode == 13) {
            focusable = form.find('input,select,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this) + 1);
            if (next.length) {
                next.focus();
            } else {
                form.submit();
            }
            return false;

        }
    });

    $(document).ready(function() {
        $(':input').keydown(function(e) {
            if ((e.which == 8 || e.which == 46) && $(this).val() == '') {
                $(this).prev('input').focus();
                alert('lanjut kiri');
            }
        });

        $('#KD_JNS_PELAYANAN_LSPOP').val("");

        $('#KD_JNS_PELAYANAN_LSPOP').change(function() {
            selectVal = $('#KD_JNS_PELAYANAN_LSPOP').val();

            if (selectVal == "") {
                $('#mySubmit_lspop').prop("disabled", true);
            } else {
                $('#mySubmit_lspop').prop("disabled", false);
            }
        })

    });

    function formulir(kategori) {
        //alert(id);

    }

    function jenis_lspop(id) {
        //alert(id);
        if (id != '') {
            document.getElementById("test_LSPOP").innerHTML = '' +
                '<table width="100%">' +
                '<tr>' +
                '<td width="12%"><input type="text" tabIndex="3" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON_LSPOP"></td>' +
                '<td width="12%"><input type="text" tabIndex="4" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_DATI2" id="KD_DATI2_PEMOHON_LSPOP"></td>' +
                '<td width="15%"><input type="text" tabIndex="5" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON_LSPOP"></td>' +
                '<td width="15%"><input type="text" tabIndex="6" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON_LSPOP"></td>' +
                '<td width="15%"><input type="text" tabIndex="7" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_BLOK" id="KD_BLOK_PEMOHON_LSPOP"></td>' +
                '<td><input type="text" tabIndex="8" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" name="NO_URUT_NOP" id="NO_URUT_PEMOHON_LSPOP"></td>' +
                '<td width="9%"><input type="text" tabIndex="9" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON_LSPOP"></td>' +
                '</tr>' +
                '</table>';
            $(document).ready(function() {
                $(".form-control").keyup(function() {
                    if (this.value.length == this.maxLength) {
                        var nextIndex = $('input:text').index(this) + 1;
                        $('input:text')[nextIndex].focus();
                    }
                });
                /*document.getElementById('KD_PROPINSI_PEMOHON').value= "" ;*/
                $('#KD_JNS_OP_PEMOHON_LSPOP,#NO_URUT_PEMOHON_LSPOP,#KD_BLOK_PEMOHON_LSPOP,#KD_KELURAHAN_PEMOHON_LSPOP,#KD_KECAMATAN_PEMOHON_LSPOP,#KD_DATI2_PEMOHON_LSPOP,#KD_PROPINSI_PEMOHON_LSPOP').on('change', function() {
                    var a = $('#KD_PROPINSI_PEMOHON_LSPOP').val();
                    var b = $('#KD_DATI2_PEMOHON_LSPOP').val();
                    var c = $('#KD_KECAMATAN_PEMOHON_LSPOP').val();
                    var d = $('#KD_KELURAHAN_PEMOHON_LSPOP').val();
                    var e = $('#KD_BLOK_PEMOHON_LSPOP').val();
                    var f = $('#NO_URUT_PEMOHON_LSPOP').val();
                    var g = $('#KD_JNS_OP_PEMOHON_LSPOP').val();
                    /*var h   =$('#KD_JNS_PELAYANAN_LSPOP').val();*/


                    if (g == '') {
                        var nop = '';
                    } else {
                        var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g + "=" + id;;
                        //alert(nop);
                        //list formulir///
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('lspop/getformulir'); ?>",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(data) {
                                // alert(data);
                                var za = data.split('|');
                                if (za[0] != '') {
                                    $('#THN_PELAYANAN_LSPOP').attr('value', za[0]);
                                    $('#KATEGORI_PELAYANAN_LSPOP').attr('value', za[1]);
                                    $('#NO_URUT_DALAM_LSPOP').attr('value', za[2]);
                                    $('#NO_URUT_LSPOP').attr('value', za[3]);
                                    $('#list_nomor').attr('value', za[3]);
                                    $('#NO_BNG').attr('value', za[4]);
                                } else {
                                    alert("NOP ini belum punya Nomor formulir/nomor formulir sudah digunakan");
                                    $('#KD_KELURAHAN_PEMOHON_LSPOP').val("");
                                    $('#KD_BLOK_PEMOHON_LSPOP').val("");
                                    $('#NO_URUT_PEMOHON_LSPOP').val("");
                                    $('#KD_JNS_OP_PEMOHON_LSPOP').val("");
                                }
                            }
                        });
                        ///////list formulir///// 
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('lspop/getNop'); ?>",
                            data: "nop=" + nop,
                            cache: false,
                            success: function(data) {
                                var sp = data.split('|');
                                //alert(sp);                              
                                if (sp[0] == 1 && sp[1] == 2) {
                                    //alert("ok");
                                    /*$('#KD_KELURAHAN_PEMOHON_LSPOP').val("");
                                    $('#KD_BLOK_PEMOHON_LSPOP').val("");
                                    $('#NO_URUT_PEMOHON_LSPOP').val("");
                                    $('#KD_JNS_OP_PEMOHON_LSPOP').val("");*/
                                    $('#mySubmit_lspop').prop("disabled", false);
                                } else if (sp[0] == 0 && sp[1] == 1) {
                                    alert("NOP BELUM TERDAFTAR");
                                    $('#KD_KELURAHAN_PEMOHON_LSPOP').val("");
                                    $('#KD_BLOK_PEMOHON_LSPOP').val("");
                                    $('#NO_URUT_PEMOHON_LSPOP').val("");
                                    $('#KD_JNS_OP_PEMOHON_LSPOP').val("");
                                    $('#mySubmit_lspop').prop("disabled", true);
                                    //document.getElementById('KD_JNS_OP_PEMOHON_LSPOP').value= "" ;
                                } else {
                                    $('#mySubmit_lspop').prop("disabled", false);
                                };
                            }
                        });
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('Spop/getform'); ?>",
                        data: "nop=" + nop,
                        cache: false,
                        success: function(data) {
                            //alert(data);
                            $('#detail_from').html(data);
                        }
                    });
                });
            });


        } else {

        };


    }


    function jenis_bangunan(id) {
        if (id == '02' || id == '04' || id == '06' || id == '09' || id == '12' || id == '16') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 2,4,6,9,12,16------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Kelas Bangunan </label>' +
                '<div class="col-md-8">' +
                '<select class="form-control chosen" requiredd name="KD_LANGIT_LANGIT">' +
                '<option value="">Pilih</option>' +
                '<option value="1">1 - KELAS 1</option>' +
                '<option value="2">2 - KELAS 2</option>' +
                '<option value="3">3 - KELAS 3</option>' +
                '<option value="4">4 - KELAS 4</option>' +
                '</select>' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '';
            $('.chosen').chosen({
                allow_single_deselect: true
            });
        } else if (id == '03' || id == '08') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 3,8------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-3 control-label">Tinggi Kolom</label>' +
                '<div class="col-md-3">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB3_8_TINGGI_KOLOM" id="" placeholder="" value="" />' +
                '</div>' +
                '<label class="col-md-3 control-label">Daya dukung Lantai (kg/m<sup>2</sup>)</label>' +
                '<div class="col-md-3">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB3_8_DD_LANTAI" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-3 control-label">Lebar Bentang(m)</label>' +
                '<div class="col-md-3">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB3_8_LEBAR_BENTANG" id="" placeholder="" value="" />' +
                '</div>' +
                '<label class="col-md-3 control-label">Keliling Dinding(m)</label>' +
                '<div class="col-md-3">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB3_8_KEL_DINDING" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 3,8------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Luas Mezzanine(M<sup>2</sup>)</label>' +
                '<div class="col-md-8">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB3_8_MEZZANINE" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
        } else if (id == '05') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 5------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Kelas Bangunan </label>' +
                '<div class="col-md-8">' +
                '<select class="form-control chosen" requiredd name="JPB5_KLS_BNG">' +
                '<option value="">Pilih</option>' +
                '<option value="1">1 - KELAS 1</option>' +
                '<option value="2">2 - KELAS 2</option>' +
                '<option value="3">3 - KELAS 3</option>' +
                '<option value="4">4 - KELAS 4</option>' +
                '</select>' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 5------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Kamar dg AC Sentral(m<sup>2</sup>)</label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB5_LUAS_KAMAR" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Ruang Lain dg AC Sentral(m<sup>2</sup>)</label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="JPB5_LUAS_RNG_LAIN" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
            $('.chosen').chosen({
                allow_single_deselect: true
            });
        } else if (id == '07') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 7------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-3 control-label">Jenis Hotel</label>' +
                '<div class="col-md-3">' +
                '<select class="form-control chosen" requiredd name="JPB5_KLS_BNG">' +
                '<option value="">Pilih</option>' +
                '<option value="1">1 - NON RESORT</option>' +
                '<option value="2">2 - RESORT</option>' +
                '</select>' +
                '</div>' +
                '<label class="col-md-3 control-label">Jumlah Bintang</label>' +
                '<div class="col-md-3">' +
                '<select class="form-control chosen" requiredd name="JPB5_KLS_BNG">' +
                '<option value="">Pilih</option>' +
                '<option value="0">0 - NON BINTANG</option>' +
                '<option value="1">1 - BINTANG 5</option>' +
                '<option value="2">2 - BINTANg</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-3 control-label">Jumlah Kamar</label>' +
                '<div class="col-md-3">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 7------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Kamar dg AC Sentral(m<sup>2</sup>)</label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Ruang Lain dg AC Sentral(m<sup>2</sup>)</label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
        } else if (id == '13') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 13------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Kelas Bangunan</label>' +
                '<div class="col-md-8">' +
                '<select class="form-control chosen" requiredd name="KD_LANGIT_LANGIT">' +
                '<option value="">Pilih</option>' +
                '<option value="1">1 - DI ATAS TANAH</option>' +
                '<option value="2">2 - DI BAWAH TANAH</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Apartemen dg AC Sentral(m<sup>2</sup>) </label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 13------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Jumlah Apartemen</label>' +
                '<div class="col-md-8">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="col-md-7 control-label">Luas Ruang Lain dg AC Sentral(m<sup>2</sup>)</label>' +
                '<div class="col-md-5">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
        } else if (id == '15') {
            document.getElementById("id_1").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 15------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Letak Tangki </label>' +
                '<div class="col-md-8">' +
                '<select class="form-control chosen" requiredd name="KD_LANGIT_LANGIT">' +
                '<option value="">Pilih</option>' +
                '<option value="1">1 - DI ATAS TANAH</option>' +
                '<option value="2">2 - DI BAWAH TANAH</option>' +
                '</select>' +
                '</div>' +
                '</div>';
            document.getElementById("id_2").innerHTML = '' +
                '<br>' +
                '<h5 style="text-align:center;">--------Data Tambahan Untuk JPB = 15------</h5>' +
                '<div class="form-group">' +
                '<label class="col-md-4 control-label">Kapasitas Tangki(M<sup>2</sup>)</label>' +
                '<div class="col-md-8">' +
                '<input type="number" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="LUAS_KOLAM" id="" placeholder="" value="" />' +
                '</div>' +
                '</div>';
            $('.chosen').chosen({
                allow_single_deselect: true
            });
        } else {
            document.getElementById("id_1").innerHTML = '';
            document.getElementById("id_2").innerHTML = '';
        };
    }
</script>
@endsection