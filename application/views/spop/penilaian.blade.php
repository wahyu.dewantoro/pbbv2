<?php
$CI = &get_instance();
$CI->load->library('session');
?>
@extends('page.master')
@section('css')
<link rel="stylesheet" href="<?php echo base_url() . 'kendo_tree/' ?>kendo.common-material.min.css" />
<link rel="stylesheet" href="<?php echo base_url() . 'kendo_tree/' ?>kendo.material.min.css" />
@endsection


@section('judul')

<h1>
  <?php echo $button ?>

</h1>

@endsection
@section('content')
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">PENILAIAN MASSAL</a></li>

  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="box box-success">
        <div class="box-header with-border">
          <?php echo $CI->session->flashdata('notif'); ?>

        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <label class="control-label col-sm-2">TAHUN:</label>
                      <div class="col-sm-9">
                        <input type="text" style="width:65px;" required class="form-control" id='tahun' name="tahun" value="<?php echo date('Y'); ?>" />
                      </div>
                    </div>
                  </form>
                </div>
                <div class="panel-body" style="padding:0px;border:0px;height:400px;overflow-y:auto">
                  <div class="form-group">
                    <div id="treeview"></div>

                  </div>
                </div>
              </div>
              <input type='hidden' id="result" value='' name='param'>
              <input type="checkbox" id="njoptkp" value="Yes" /> Penetapan NJOPTKP
              <button id="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
            </div>

            <div class="col-md-9">
              <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body" style="padding:0;border:0px;height:437px;overflow-y:auto">
                  <table class="table table-bordered table-hover table-striped">
                    <thead>
                      <tr>
                        <th>Kecamatan</th>
                        <th>Kelurahan</th>
                        <th>Jumlah OP</th>
                        <th>Proses</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody id="content">

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- body -->
      </div>
      </form>
    </div>
    <!-- /.tab-pane 2-->

    <!-- /.tab-pane -->
  </div>
  <!-- /.tab-content -->
</div>
@endsection

@section('script')
<script src="<?php echo base_url() . 'kendo_tree/' ?>kendo.all.min.js"></script>
<script>
  $("#treeview").kendoTreeView({
    checkboxes: {
      checkChildren: true
    },

    check: onCheck,
    dataSource: [{
      text: "KABUPATEN MALANG",
      expanded: true,
      /*spriteCssClass: "",*/
      items: [
        <?php foreach ($kecamatan as $rp) { ?> {

            /*id: "<?php echo $rp['KD_KECAMATAN'] ?>",*/
            text: "<?php echo $rp['NAMA'] ?>",
            expanded: false,
            items: [
              <?php $kelurahan = $CI->db->query("select kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||'.'||kd_kelurahan kode,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||'.'||kd_kelurahan||' -'||nm_kelurahan nama
                                                                    from ref_kelurahan where kd_kecamatan='" . $rp['KD_KECAMATAN'] . "'")->result_array();
              foreach ($kelurahan as $rt) { ?> {
                  id: "<?php echo $rt['KODE'] ?>",
                  text: "<?php echo $rt['NAMA'] ?>",
                  spriteCssClass: ""
                },
              <?php } ?>
            ]
          },
        <?php } ?>
      ]
    }]
  });

  // function that gathers IDs of checked nodes
  function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
      if (nodes[i].checked) {
        checkedNodes.push(nodes[i].id);
      }

      if (nodes[i].hasChildren) {
        checkedNodeIds(nodes[i].children.view(), checkedNodes);
      }
    }
  }

  // show checked node IDs on datasource change
  function onCheck() {
    var checkedNodes = [],
      treeView = $("#treeview").data("kendoTreeView"),
      message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length > 0) {
      message = /*"checked nodes: " +*/ checkedNodes.join("|");
    } else {
      message = "";
    }

     
    $('#result').attr('value', message);
    $(document).ready(function() {
      $('#submit').click(function(e) {
        e.preventDefault();
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url() ?>spop/tes_jason',
          data: {
            tahun: $('#tahun').val(),
            param: $('#result').val()
          },
          success: function(data) {
            $("#content").html(data);
            $.ajax({
              type: 'POST',
              url: '<?php echo base_url() ?>spop/proses_penilaian',
              data: {
                param: $('#result').val(),
                tahun: $('#tahun').val(),
                njoptkp: $('#njoptkp').prop("checked")
              },
              beforeSend: function() {
                $('#submit').prop("disabled", true)
                $(".ket_proses").html("prosesing");
                
              },
              success: function(data, status) {
                $('#submit').prop("disabled", false)
                $(".ket_proses").html("completed " + status);
              }
            });
          }
        });
      });
    });
    
  }
</script>
 
@endsection