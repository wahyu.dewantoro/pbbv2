@extends('page.master')
@section('judul')
<h1>
    Form Menu
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
    <li>Menu</li>
    <li class="active"><?= $button  ?></li>
</ol>
@endsection
@section('content')

<div class="box">
    <div class="box-body">
        <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Menu </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
                            <?php echo form_error('nama_menu') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Link Menu </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>" />
                            <?php echo form_error('link_menu') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Parent Menu </label>
                        <div class="col-md-8">
                            <select class="form-control chosen" name="parent_menu" id="parent_menu">
                                <option value="0">Parent</option>
                                <?php foreach ($parent as $parent) { ?>
                                    <option <?php if ($parent_menu == $parent->KODE_MENU) {
                                                echo "selected";
                                            } ?> value="<?php echo $parent->KODE_MENU ?>"><?php echo $parent->NAMA_MENU ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('parent_menu') ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Sort Menu </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="sort_menu" id="sort_menu" placeholder="Sort Menu" value="<?php echo $sort_menu; ?>" />
                            <?php echo form_error('sort_menu') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Icon Menu </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control chosen" name="icon_menu" id="icon_menu" value="<?= $icon_menu ?>">
                            <?php echo form_error('icon_menu') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Active </label>
                        <div class="col-md-8">
                            <input type="radio" name="active" class="flat-red" value="1" <?php
                                                                                            if ($active == '1') {
                                                                                                echo "checked";
                                                                                            }
                                                                                            ?> /> Aktif<br>
                            <input type="radio" name="active" class="flat-red" value="0" <?php
                                                                                            if ($active == '0') {
                                                                                                echo "checked";
                                                                                            }
                                                                                            ?> /> Non Aktif
                            <?php echo form_error('active') ?>
                        </div>
                    </div>
                    <input type="hidden" name="kode_menu" value="<?php echo $kode_menu; ?>" />
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('menu') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>










        </form>
    </div>
</div>

@endsection