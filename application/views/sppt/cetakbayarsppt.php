	<style type="text/css">
		body {
			font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
			font-size: 10px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		}

		.inti {
			border: 1px solid #C6C6C6;
			margin: auto;
		}

		.inti td {
			border-right: 1px solid #C6C6C6;
		}

		.inti th {
			border-right: 1px solid #C6C6C6;
			border-bottom: 1px solid #C6C6C6;
		}
	</style>
	<style type="text/css" media="print">
		@page {
			size: landscape;
		}
	</style>

	<h1 align="center">REKAPITULASI PEMBAYARAN SPPT</h1>
	<h2 align="center">NOP : <?= $NOPX ?></h2>
	<table class="inti" width="100%" cellpadding="3" cellspacing="0" style="font-size:12px">
		<thead>
			<tr>
				<th>NO</th>
				<th>Tahun</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>NJOP Bumi</th>
				<th>NJOP Bangunan</th>
				<th>Pokok</th>
				<th>Denda</th>
				<th>Total</th>
				<th>Lunas</th>
				<th>Tanggal Bayar</th>
				<th>Ket</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
			$jp = 0;
			$jd = 0;
			$jt = 0;
			foreach ($res as $res) {
				$jp += $res->PBB_YG_HARUS_DIBAYAR_SPPT;
				$jd += $res->DENDA;
				$jt += $res->PBB_YG_HARUS_DIBAYAR_SPPT + $res->DENDA; 
				$ket = '-';
                                if($res->UNFLAG_DESC != null){
                                    $ket = $res->UNFLAG_DESC;
                                }

                                if($res->KOBIL != null){
                                    if($res->UNFLAG_DESC != null){
                                        $ket .= ', Masuk di Dafnom : '.$res->KOBIL;
                                    }else{
                                        $ket = 'Masuk di Dafnom : '.$res->KOBIL;
                                    }
                                }
				?>
				<tr>
					<td align="center"><?php echo $no; ?></td>
					<td align="center"><?= $res->THN_PAJAK_SPPT ?></td>
					<td><?= $res->NM_WP_SPPT ?></td>
					<td><?= $res->ALAMAT ?></td>
					<td align="right"><?= number_format($res->NJOP_BUMI_SPPT, '0', '', '.'); ?></td>
					<td align="right"><?= number_format($res->NJOP_BNG_SPPT, '0', '', '.') ?></td>
					<td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT- $res->DENDA, '0', '', '.') ?></td>
					<td align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
					<td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT , '0', '', '.') ?></td>
					<td align="center"><?php if ($res->STATUS_PEMBAYARAN_SPPT == 1) {
											echo "Lunas";
										} else if ($res->STATUS_PEMBAYARAN_SPPT == 3) {
											echo "Dihapus";
										} else {
											echo "Belum";
										} ?></td>
					<td align="center"><?= $res->TGL_BAYAR ?></td>
					<td align="left"><?= $ket ?></td>
				</tr>
			<?php $no++;
			} ?>
		</tbody>

		<thead border=1>
			<tr>
				<th colspan="6">Total</th>
				<th align="right"><?= number_format($jp, 0, '', '.') ?></th>
				<th align="right"><?= number_format($jd, 0, '', '.') ?></th>
				<th align="right"><?= number_format($jt, 0, '', '.') ?></th>
				<th></th>
				<th></th>
			</tr>
		</thead>

	</table>
	<br><br>
	<table width="100%">
		<tr>
			<td></td>
			<td align="center" width="40%">
				MALANG, <?= date('d M Y') ?>
				<br>
				<br>
				<br>
				<?php echo $this->session->userdata('pbb_nama') ?>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	
		window.print();
		// window.open();
		// window.print();
		// window.close();
	</script>