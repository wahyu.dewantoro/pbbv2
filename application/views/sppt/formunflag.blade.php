@extends('page.master')
@section('judul')
<h1>
    Unflag Pembayaran
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>
    <li class="active">Unflag Pembayaran</li>
</ol>
@endsection
@section('content')
<div class="box">
    <div class="box-body">
        <form class="form-inline" role="form" method="get" action="<?= base_url('sppt/unflag') ?>">
            <div class="form-group">
                NOP
            </div>
            <div class="form-group">
                <input type="text" onkeyup="formatnop(this)" autofocus="true" name="NOP" class="form-control" id="NOP" placeholder="" autofocus="true" required value="{{ $nop }}">
            </div>
            <div class="form-group">
                <input type="text" name="TAHUN" class="form-control" placeholder="Tahun Pajak" required value="{{ $tahun }}">
            </div>
            <button type="submit" class="btn btn-info">Cek</button>
            @if(!empty($pb))
            <button id="btn_reversal" data-nop="<?= $nop ?>" data-tahun="<?= $tahun ?>" data-merchant="1234" title="Reversal" class="btn btn-danger" onclick="return confirm('Apakah anda yakin reversal data ini ?')">Unflag</button>
            <!-- <a onclick="return confirm('Apakah anda yakin?')" href="{{ base_url('sppt/unflagprosess') }}?nop={{ $nop }}&tahun={{ $tahun }}&merchat=1234" class="btn btn-danger">Unflag</a> -->
            @endif
        </form>

        @if(!empty($pb))
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Nama WP</th>
                    <th>Nop</th>
                    <th>Tahun</th>
                    <th>Jumlah Bayar</th>
                    <th>Tanggal Bayar</th>
                    <th>Rekam Bayar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pb as $rr)
                <tr>
                    <td>{{ $rr->NM_WP_SPPT }}</td>
                    <td>{{ $rr->KD_PROPINSI }}
                        .{{ $rr->KD_DATI2 }}
                        .{{ $rr->KD_KECAMATAN }}
                        .{{ $rr->KD_KELURAHAN }}
                        .{{ $rr->KD_BLOK }}
                        .{{ $rr->NO_URUT }}
                        .{{ $rr->KD_JNS_OP }}</td>
                    <td align="center">{{ $rr->THN_PAJAK_SPPT }}</td>
                    <td align="right">{{ number_format($rr->JML_SPPT_YG_DIBAYAR,0,'','.') }}</td>
                    <td>{{ $rr->TGL_PEMBAYARAN_SPPT }}</td>
                    <td>{{ $rr->TGL_REKAM_BYR_SPPT }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @endif

    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {


    });

    function checkLengthnop(el) {
        b = el.value.replace(/[^\d]/g, "");

        if (b.length != 18) {
            // alert("Panjang nik 16 digit.")
            $("#VAL_NOP").html("<span style='color:red'>Panjang NOP harus  18 angka.</span>");
        } else {
            $("#VAL_NOP").html("");
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function formatnop(objek) {

        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 2) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 2 && panjang <= 4) {
            // 07. -> 2,2
            c = b.substr(0, 2) + '.' + b.substr(2, 2);
        } else if (panjang > 4 && panjang <= 7) {
            // 123 -> 4,3
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
        } else if (panjang > 7 && panjang <= 10) {
            // .123. ->
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
        } else if (panjang > 10 && panjang <= 13) {
            // 123.
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
        } else if (panjang > 13 && panjang <= 17) {
            // 1234
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
        } else {
            // .0
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
            // alert(panjang);
        }
        objek.value = c;
    }
</script>
<script>
    $(document).on('click', '#btn_reversal', function(e) {
        e.preventDefault();
        var nop = $('#btn_reversal').attr("data-nop");
        var tahun = $('#btn_reversal').attr("data-tahun");
        var merchant = $('#btn_reversal').attr("data-merchant");
        Swal.fire({
            title: 'Sedang di proses',
            text: "Mohon bersabar ...",
            icon: 'warning',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })
        $.ajax({
            url: '<?php echo site_url("sppt/unflagprosess?nop=") ?>'+nop+'&tahun='+tahun+'&merchat='+merchant,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(response) {
                console.log(response);
                swal.close();

                if (response.status == '1') {
                    // console.log(response.data);
                    Swal.fire({
                        title: 'Sukses',
                        text: response.data,
                        icon: 'success',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    }).then(function() {
                        // location.reload();
                        window.location = "<?= base_url('sppt/unflag'); ?>";
                    });
                } else {
                    Swal.fire({
                        title: 'Oppss',
                        text: response.data,
                        icon: 'warning',
                        showConfirmButton: true,
                        allowOutsideClick: false,
                    })
                }
            },
            error: function(data) {
                console.log(data.responseText);
                Swal.fire({
                    title: 'Oppss',
                    text: "Terjadi kesalahan",
                    icon: 'warning',
                    showConfirmButton: true,
                    allowOutsideClick: false,
                    // timer: 3000,?
                })
            }
        });
    })
</script>
@endsection