 @extends('page.master')
 @section('judul')
 <h1>
     Cek SPPT
 </h1>
 <ol class="breadcrumb">
     <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>

     <li class="active">Cek SPPT</li>
 </ol>
 @endsection

 @section('content')
 <div class="row">

     <div class="col-md-12">

         <div class="box">
             <div class="box-body">
                 <div class="row">
                     <div class="col-sm-3 col-md-offset-3">
                         <input type="text" id="NOP" onkeyup="formatnop(this)" autofocus="true" class="form-control" placeholder="Masukan NOP">
                     </div>
                     <div class="col-sm-3">

                         <div class="input-group ">
                             <select id="THN_SPPT" class="form-control" placeholder="Tahun">
                                 <?php $angka = date('Y'); ?>
                                 <?php for ($tt = $angka - 10; $tt <= $angka; $tt++) { ?>
                                     <option <?php if ($tt == $angka) {
                                                    echo "selected";
                                                } ?> value="<?= $tt ?>"><?= $tt ?></option>
                                 <?php  } ?>
                             </select>
                             <div class="input-group-btn">
                                 <button id="esppt" disabled type="button" class="btn btn-warning">E- SPPT</button>
                             </div>
                         </div>
                     </div>

                 </div>
                 <hr>
                 <div class="row">
                     <div class="col-md-6">
                         <form class="form-horizontal" role="form">
                             <div class="form-group">
                                 <label class="control-label col-md-4">Letak Objek Pajak</label>
                                 <div class="col-md-8">
                                     <textarea id="ALAMAT_OP" placeholder="letak objek pajak" readonly class="form-control" rows="3"></textarea>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label col-md-4">RT / RW</label>
                                 <div class="col-md-4">
                                     <input type="text" class="form-control" id="RT_OP" placeholder="RT" readonly="">
                                 </div>
                                 <div class="col-md-4">
                                     <input type="text" class="form-control" id="RW_OP" placeholder="RW" readonly="">
                                 </div>
                             </div>
                         </form>
                     </div>
                     <div class="col-md-6">
                         <form class="form-horizontal" role="form">
                             <div class="form-group">
                                 <label class="col-md-3 control-label">Nama WP</label>
                                 <div class="col-md-9">
                                     <input type="text" readonly="" id="NM_WP_SPPT" class="form-control" placeholder="nama wajib pajak">
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-md-3 control-label">Alamat WP</label>
                                 <div class="col-md-9">
                                     <textarea id="ALAMAT_WP" placeholder="Alamat wajib pajak" readonly class="form-control" rows="3"></textarea>
                                 </div>
                             </div>
                         </form>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-md-12">
                         <table width="100%" cellpadding="5" cellspacing="5" border="0">
                             <tbody>
                                 <tr>
                                     <td></td>
                                     <th width="20%">Luas (M<sup>2</sup>)</th>
                                     <th width="20%">Kelas</th>
                                     <th width="20%">NJOP / M<sup>2</sup></th>
                                     <th width="20%">Total NJOP</th>
                                 </tr>
                                 <tr>
                                     <td><b>Tanah</b></td>
                                     <td><input type="text" id="LUAS_BUMI_SPPT" class="form-control" readonly=""></td>
                                     <td><input type="text" id="KD_KLS_TANAH" class="form-control" readonly=""></td>
                                     <td><input type="text" id="njop_tanah" class="form-control" readonly=""></td>
                                     <td><input type="text" id="NJOP_BUMI_SPPT" class="form-control" readonly=""></td>
                                 </tr>
                                 <tr>
                                     <td width="15%"><b>Bangunan</b></td>
                                     <td><input type="text" id="LUAS_BNG_SPPT" class="form-control" readonly=""></td>
                                     <td><input type="text" id="KD_KLS_BNG" class="form-control" readonly=""></td>
                                     <td><input type="text" id="njop_bangunan" class="form-control" readonly=""></td>
                                     <td><input type="text" id="NJOP_BNG_SPPT" class="form-control" readonly=""></td>
                                 </tr>
                             </tbody>
                         </table>
                         <table width="100%" cellpadding="5" cellspacing="5" border="0">
                             <tr>
                                 <td width="20%">Jumlah NJOP Bumi</td>
                                 <td width="40%"><input type="text" readonly="" id="jumlah_njop_bumi" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Jumlah NJOP Bangunan</td>
                                 <td><input type="text" readonly="" id="jumlah_njop_bangunan" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>NJOP sebagai dasar pengenaan PBB</td>
                                 <td><input type="text" readonly="" id="NJOP_SPPT" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>NJOPTKP</td>
                                 <td><input type="text" readonly="" id="NJOPTKP_SPPT" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Pajak Bumi dan Bangunan Terhutang</td>
                                 <td><input type="text" readonly="" id="PBB_TERHUTANG_SPPT" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Faktor Pengurang</td>
                                 <td><input type="text" readonly="" id="FAKTOR_PENGURANG_SPPT" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Pajak Bumi dan Bangunan Yang Harus di bayar</td>
                                 <td><input type="text" readonly="" id="PBB_YG_HARUS_DIBAYAR_SPPT" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Denda yang telah dibayar</td>
                                 <td><input type="text" readonly="" id="denda_terbayar" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Pajak Bumi dan Bangunan yang telah di bayar</td>
                                 <td><input type="text" readonly="" id="pbb_terbayar" class="form-control"></td>
                             </tr>
                             <tr>
                                 <td>Selisih</td>
                                 <td><input type="text" readonly="" id="selisih" class="form-control"></td>
                             </tr>
                         </table>
                         <table width="100%" cellpadding="5" cellspacing="5" border="0">
                             <tr>
                                 <td colspan="3">Tanggal Jatuh tempo / Tempat Pembayaran</td>
                                 <td width="20%"><input type="text" readonly="" class="form-control" id="tanggal_jatuh_tempo"></td>
                                 <td width="20%"><input type="text" readonly="" class="form-control" id="tempat_bayar"></td>
                             </tr>
                         </table>
                         <table width="100%" cellpadding="5" cellspacing="5" border="0">
                             <tr>
                                 <td align="right">Tanggal Terbit</td>
                                 <td><input type="text" id="TGL_TERBIT_SPPT" class="form-control" readonly=""></td>
                                 <td align="right">Tanggal cetak </td>
                                 <td><input type="text" id="TGL_CETAK_SPPT" class="form-control" readonly=""></td>
                                 <td align="right">NIP Pencetak</td>
                                 <td><input type="text" id="NIP_PENCETAK_SPPT" class="form-control" readonly=""></td>
                             </tr>
                         </table>
                     </div>
                 </div>

             </div>
         </div>
     </div>
 </div>
 @endsection
 @section('script')
 <script type="text/javascript">
     function formatnop(objek) {
         a = objek.value;
         b = a.replace(/[^\d]/g, "");
         c = "";
         panjang = b.length;
         if (panjang <= 2) {
             // 35 -> 0,2
             c = b;
         } else if (panjang > 2 && panjang <= 4) {
             // 07. -> 2,2
             c = b.substr(0, 2) + '.' + b.substr(2, 2);
         } else if (panjang > 4 && panjang <= 7) {
             // 123 -> 4,3
             c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
         } else if (panjang > 7 && panjang <= 10) {
             // .123. ->
             c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
         } else if (panjang > 10 && panjang <= 13) {
             // 123.
             c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
         } else if (panjang > 13 && panjang <= 17) {
             // 1234
             c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
         } else {
             // .0
             c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
             // alert(panjang);
         }
         objek.value = c;
     }

     $(".form-control").keyup(function(event) {
         if (event.keyCode == 13) {
             textboxes = $("input.form-control");
             currentBoxNumber = textboxes.index(this);
             if (textboxes[currentBoxNumber + 1] != null) {
                 nextBox = textboxes[currentBoxNumber + 1];
                 nextBox.focus();
                 nextBox.select();
             }
             event.preventDefault();
             return false;
         }
     });

     $(document).ready(function() {


         $("#NOP,#THN_SPPT").keyup(function() {
             cekdatasppt();
         });
         $("#NOP,#THN_SPPT").change(function() {
             cekdatasppt();
         });

         $("#esppt").click(function() {
          nop=$("#NOP").val().replace(/[^\d]/g, "")
          tahun=$("#THN_SPPT").val().replace(/[^\d]/g, "");
            
          window.open("<?= base_url() ?>sppt/esppt?nop="+nop+"&tahun="+tahun, '_blank');

         });



         function cekdatasppt() {
             $('#esppt').prop('disabled', true);
             var vnop = $('#NOP').val();
             var nopv = vnop.replace(/[^\d]/g, "");
             var vth = $('#THN_SPPT').val();
             var thv = vth.replace(/[^\d]/g, "");

             $.ajax({
                 url: "<?= base_url() . 'sppt/getDataNop' ?>",
                 type: "GET",
                 data: 'nop=' + nopv + thv,
                 dataType: "html",
                 success: function(result) {


                     var datashow = JSON.parse(result);
                     $('#ALAMAT_WP').val(datashow[0].JLN_WP_SPPT + ' ' + datashow[0].BLOK_KAV_NO_WP_SPPT);
                     $('#KD_PROPINSI').val(datashow[0].KD_PROPINSI);
                     $('#KD_DATI2').val(datashow[0].KD_DATI2);
                     $('#KD_KECAMATAN').val(datashow[0].KD_KECAMATAN);
                     $('#KD_KELURAHAN').val(datashow[0].KD_KELURAHAN);
                     $('#KD_BLOK').val(datashow[0].KD_BLOK);
                     $('#NO_URUT').val(datashow[0].NO_URUT);
                     $('#KD_JNS_OP').val(datashow[0].KD_JNS_OP);
                     $('#THN_PAJAK_SPPT').val(datashow[0].THN_PAJAK_SPPT);
                     $('#SIKLUS_SPPT').val(datashow[0].SIKLUS_SPPT);
                     $('#KD_KANWIL').val(datashow[0].KD_KANWIL);
                     $('#KD_KANTOR').val(datashow[0].KD_KANTOR);
                     $('#KD_TP').val(datashow[0].KD_TP);
                     $('#NM_WP_SPPT').val(datashow[0].NM_WP_SPPT);
                     $('#JLN_WP_SPPT').val(datashow[0].JLN_WP_SPPT);
                     $('#BLOK_KAV_NO_WP_SPPT').val(datashow[0].BLOK_KAV_NO_WP_SPPT);
                     $('#RW_WP_SPPT').val(datashow[0].RW_WP_SPPT);
                     $('#RT_WP_SPPT').val(datashow[0].RT_WP_SPPT);
                     $('#KELURAHAN_WP_SPPT').val(datashow[0].KELURAHAN_WP_SPPT);
                     $('#KOTA_WP_SPPT').val(datashow[0].KOTA_WP_SPPT);
                     $('#KD_POS_WP_SPPT').val(datashow[0].KD_POS_WP_SPPT);
                     $('#NPWP_SPPT').val(datashow[0].NPWP_SPPT);
                     $('#NO_PERSIL_SPPT').val(datashow[0].NO_PERSIL_SPPT);
                     $('#KD_KLS_TANAH').val(datashow[0].KD_KLS_TANAH);
                     $('#THN_AWAL_KLS_TANAH').val(datashow[0].THN_AWAL_KLS_TANAH);
                     $('#KD_KLS_BNG').val(datashow[0].KD_KLS_BNG);
                     $('#THN_AWAL_KLS_BNG').val(datashow[0].THN_AWAL_KLS_BNG);
                     $('#TGL_JATUH_TEMPO_SPPT').val(datashow[0].TGL_JATUH_TEMPO_SPPT);
                     $('#LUAS_BUMI_SPPT').val(datashow[0].LUAS_BUMI_SPPT);
                     $('#LUAS_BNG_SPPT').val(datashow[0].LUAS_BNG_SPPT);
                     $('#NJOP_BUMI_SPPT').val(datashow[0].NJOP_BUMI_SPPT);
                     $('#NJOP_BNG_SPPT').val(datashow[0].NJOP_BNG_SPPT);
                     $('#NJOP_SPPT').val(datashow[0].NJOP_SPPT);
                     $('#NJOPTKP_SPPT').val(datashow[0].NJOPTKP_SPPT);
                     $('#PBB_TERHUTANG_SPPT').val(datashow[0].PBB_TERHUTANG_SPPT);
                     $('#FAKTOR_PENGURANG_SPPT').val(datashow[0].FAKTOR_PENGURANG_SPPT);
                     $('#PBB_YG_HARUS_DIBAYAR_SPPT').val(datashow[0].PBB_YG_HARUS_DIBAYAR_SPPT);
                     $('#STATUS_PEMBAYARAN_SPPT').val(datashow[0].STATUS_PEMBAYARAN_SPPT);
                     $('#STATUS_TAGIHAN_SPPT').val(datashow[0].STATUS_TAGIHAN_SPPT);
                     $('#STATUS_CETAK_SPPT').val(datashow[0].STATUS_CETAK_SPPT);
                     $('#TGL_TERBIT_SPPT').val(datashow[0].TGL_TERBIT_SPPT);
                     $('#TGL_CETAK_SPPT').val(datashow[0].TGL_CETAK_SPPT);
                     $('#NIP_PENCETAK_SPPT').val(datashow[0].NIP_PENCETAK_SPPT);

                     $('#njop_tanah').val(datashow[0].NJOP_BUMI_SPPT.replace(/\D+/g, "") / datashow[0].LUAS_BUMI_SPPT.replace(/\D+/g, ""));
                     if (datashow[0].LUAS_BNG_SPPT != 0) {
                         $('#njop_bangunan').val(datashow[0].NJOP_BNG_SPPT.replace(/\D+/g, "") / datashow[0].LUAS_BNG_SPPT.replace(/\D+/g, ""));
                     } else {
                         $('#njop_bangunan').val(datashow[0].LUAS_BNG_SPPT);
                     }

                     $('#ALAMAT_OP').val(datashow[0].ALAMAT_OP);
                     $('#RT_OP').val(datashow[0].RT_OP);
                     $('#RW_OP').val(datashow[0].RW_OP);

                     $('#jumlah_njop_bumi').val(datashow[0].NJOP_BUMI_SPPT);
                     $('#jumlah_njop_bangunan').val(datashow[0].NJOP_BNG_SPPT);

                     if (datashow[0].KD_PROPINSI != '') {
                         $('#esppt').prop('disabled', false);
                     } else {
                         $('#esppt').prop('disabled', true);
                     }


                 },
                 error: function() {
                     $('#esppt').prop('disabled', true);
                     Swal.fire({
                         title: 'Error',
                         text: 'Terjadi kesalahan sistem',
                         icon: 'warning',
                         showConfirmButton: false,
                         allowOutsideClick: false,
                         timer: 3000,
                     })
                 }
             }, );
         }
     });
 </script>
 @endsection