<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data pembayaran</h3>
        <div class="pull-right">
        <a target="_blank" class="btn btn-success btn-sm btn-success" href="<?php echo base_url().'sppt/cetaksppt/' . $nop ?>"><i class="fa fa-print"></i> Cetak</a>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NOP</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>NJOP Bumi</th>
                        <th>NJOP Bangunan</th>
                        <th>Tahun</th>
                        <th>Pokok</th>
                        <th>Denda</th>
                        <th>Total</th>
                        <th>Lunas</th>
                        <th>Tanggal Bayar</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <?php $no = 1;
                if (!empty($res)) {
                    foreach ($res as $res) {  ?>
                        <tr>
                            <td align="center"><?php echo $no; ?></td>
                            <td><?= $res->NOP ?></td>
                            <td><?= $res->NM_WP_SPPT ?></td>
                            <td><?= $res->ALAMAT ?></td>
                            <td align="right"><?= number_format($res->NJOP_BUMI_SPPT, '0', '', '.'); ?></td>
                            <td align="right"><?= number_format($res->NJOP_BNG_SPPT, '0', '', '.') ?></td>
                            <td align="center"><?= $res->THN_PAJAK_SPPT ?></td>
                            <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT, '0', '', '.') ?></td>
                            <td align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
                            <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT + $res->DENDA, '0', '', '.') ?></td>
                            <td align="center"><?php
                                                if ($res->STATUS_PEMBAYARAN_SPPT == 1) {
                                                    echo "Lunas";
                                                } else if ($res->STATUS_PEMBAYARAN_SPPT == 3) {
                                                    echo "Dihapus";
                                                } else {
                                                    echo "Belum";
                                                } ?></td>
                            <td align="center"><?= $res->TGL_BAYAR ?></td>
                            <td align="left"><?= $res->UNFLAG_DESC ?></td>
                        </tr>
                    <?php $no++;
                    }
                } else { ?>
                    <tr>
                        <td colspan="13">Tidak ada data</td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>