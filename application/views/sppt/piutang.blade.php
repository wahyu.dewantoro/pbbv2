@extends('page.master')
@section('judul')
<h1>
    Piutang
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>
    <li class="active">Piutang</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Data</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm hidden-xs" style="width: 350px;">
                        <input type="number" name="tahun" id="tahun" class="form-control pull-right" value="{{ $tahun }}" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="button" id="cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                            <button type="button" id="excel" class="btn btn-success"><i class="fa fa-download"></i> Download</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped">
                    <thead class="text-white bg-info">
                        <tr>
                            <th width="200px">NOP</th>
                            <th>Nama Wp</th>
                            <th>Alamat</th>
                            <th>Jalan</th>
                            <th>Kelurahan</th>
                            <th>Kecamatan</th>
                            <th>Tahun</th>
                            <th>PBB</th>
                            <th>Piutang</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr>
                            <td>
                                {{ $item->KD_PROPINSI }}.
                                {{ $item->KD_DATI2 }}.
                                {{ $item->KD_KECAMATAN }}.
                                {{ $item->KD_KELURAHAN }}.
                                {{ $item->KD_BLOK }}.
                                {{ $item->NO_URUT }}.
                                {{ $item->KD_JNS_OP }}
                            </td>
                            <td>{{ $item->NAMA_WP }} </td>
                            <td>{{ $item->ALAMAT_WP }} </td>
                            <td>{{ $item->JALAN_OP }} </td>
                            <td>{{ $item->KELURAHAN }} </td>
                            <td>{{ $item->KECAMATAN }} </td>
                            <td>{{ $item->TAHUN_PAJAK }} </td>
                            <td>{{ number_format($item->BAYAR,0,'','.') }} </td>
                            <td>{{ number_format($item->PIUTANG,0,'','.') }} </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="row">
                    <div class="col-md-6">
                        <b>Jumlah Data</b> : <?php echo number_format($total_rows, 0, '', '.') ?>
                    </div>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box -->
    </div>

</div>
@endsection
@section('script')
<script type="text/javascript">
    $('#cari').click(function() {
        window.location = "<?= base_url() . 'sppt/piutang' ?>/" + $('#tahun').val();
    });

    // excel
    $('#excel').click(function() {
        window.location = "<?= base_url() . 'sppt/piutang_excel' ?>/" + $('#tahun').val();
    });
</script>
@endsection