@extends('page.master')
@section('judul')
<h1>
    Riwayat Pembayaran
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>

    <li class="active">Riwayat Pembayaran</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="get" action="<?= base_url('sppt/riwayat') ?>" class="form-inline" role="form">
                            <div class="form-group">
                                <select class="form-control" name="var" id="var">
                                    <option value="nop">NOP</option>
                                    <option value="scan">Scan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" onkeyup="formatnop(this)" autofocus="true" class="form-control" name="NOP" id="NOP" placeholder="" autofocus="true" required value="{{ $nop??'' }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" id="cariii" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </form>

                    </div>
                    <div class="col-md-2">
                        <!-- <p class="pull-right"><strong>NOP</strong></p> -->
                    </div>
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-2">
                        <!-- <button id="cari" type="button" class='btn btn-sm btn-primary'><i class="fa fa-search"></i> Cari</button> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <?php if (!empty($res)) { ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Data pembayaran</h3>
                    <div class="pull-right">
                        <?= anchor('sppt/cetaksppt/' . $nop, '<i class="fa fa-print"></i> Cetak', 'class="btn btn-success btn-sm btn-success" target="_blank"') ?>

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NOP</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>NJOP Bumi</th>
                                    <th>NJOP Bangunan</th>
                                    <th>Tahun</th>
                                    <th>Pokok</th>
                                    <th>Denda</th>
                                    <th>Total</th>
                                    <th>Lunas</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <?php $no = 1;
                            foreach ($res as $res) {  
                                $ket = '-';
                                if($res->UNFLAG_DESC != null){
                                    $ket = $res->UNFLAG_DESC;
                                }

                                if($res->KOBIL != null){
                                    if($res->UNFLAG_DESC != null){
                                        $ket .= ', Masuk di Dafnom : '.$res->KOBIL;
                                    }else{
                                        $ket = 'Masuk di Dafnom : '.$res->KOBIL;
                                    }
                                }
                                ?>
                                <tr>
                                    <td align="center"><?php echo $no; ?></td>
                                    <td><?= $res->NOP ?></td>
                                    <td><?= $res->NM_WP_SPPT ?></td>
                                    <td><?= $res->ALAMAT ?></td>
                                    <td align="right"><?= number_format($res->NJOP_BUMI_SPPT, '0', '', '.'); ?></td>
                                    <td align="right"><?= number_format($res->NJOP_BNG_SPPT, '0', '', '.') ?></td>
                                    <td align="center"><?= $res->THN_PAJAK_SPPT ?></td>
                                    <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT - $res->DENDA, '0', '', '.') ?></td>
                                    <td align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
                                    <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT , '0', '', '.') ?></td>
                                    <td align="center"><?php
                                                        if ($res->STATUS_PEMBAYARAN_SPPT == 1) {
                                                            echo "Lunas";
                                                        } else if ($res->STATUS_PEMBAYARAN_SPPT == 3) {
                                                            echo "Dihapus";
                                                        } else {
                                                            echo "Belum";
                                                        } ?></td>
                                    <td align="center"><?= $res->TGL_BAYAR ?></td>
                                    <td align="left"><?= $ket ?></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>


    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {

        var input = document.getElementById("NOP");
        input.addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("cari").click();
            }
        });

        $("#cari").click(function() {
            swal.fire({
                type: 'question',
                text: "Sedang memproses...",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $('#result').html('');
            var sc = $("#var").val();
            var nop = $("#NOP").val();

            $.post("<?= base_url() . 'sppt/ceknop' ?>", {
                    nop: nop,
                    var: sc
                },
                function(data) {
                    swal.close();
                    // $('#result').html(data);
                    if (data != 0) {
                        window.open(data, '_blank');
                    } else {
                        swal.fire({
                            type: 'warning',
                            text: "Data tidak di temukan",
                            // showConfirmButton: false,
                            // allowOutsideClick: false
                        });
                    }
                });
        });

        $("#NOP").keyup(function() {

            var sc = $("#var").val();
            var nop = $("#NOP").val();

            // cekscan(sc, nop);
        });

        $("#NOP").keydown(function() {

            var sc = $("#var").val();
            var nop = $("#NOP").val();

            // cekscan(sc, nop);
        });

        function cekscan(sc, nop) {
            swal.fire({
                type: 'question',
                text: "Sedang memproses...",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $('#result').html('');
            $.ajax({
                type: "post",
                async: false,
                url: "<?= base_url() . 'sppt/ceknop' ?>",
                data: {
                    'nop': nop,
                    'var': sc
                },
                success: function(data) {
                    swal({
                        type: 'success',
                        text: "Pencarian Selesai",
                        timer: 1500,
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });

                    $('#result').html(data);
                },
                error: function(data) {
                    swal({
                        type: 'warning',
                        text: "Sistem error",
                        timer: 1500,
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });

                }
            });
        }
    });

    function checkLengthnop(el) {
        b = el.value.replace(/[^\d]/g, "");

        if (b.length != 18) {
            // alert("Panjang nik 16 digit.")
            $("#VAL_NOP").html("<span style='color:red'>Panjang NOP harus  18 angka.</span>");
        } else {
            $("#VAL_NOP").html("");
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function formatnop(objek) {

        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 2) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 2 && panjang <= 4) {
            // 07. -> 2,2
            c = b.substr(0, 2) + '.' + b.substr(2, 2);
        } else if (panjang > 4 && panjang <= 7) {
            // 123 -> 4,3
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
        } else if (panjang > 7 && panjang <= 10) {
            // .123. ->
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
        } else if (panjang > 10 && panjang <= 13) {
            // 123.
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
        } else if (panjang > 13 && panjang <= 17) {
            // 1234
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
        } else {
            // .0
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
            // alert(panjang);
        }
        objek.value = c;
    }
</script>
@endsection