@extends('page.master')
@section('judul')
<h1>
    Pelunasan Bank Jatim
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>
    <li class="active">Pelunasan Bank Jatim</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <div class="box-title">File Dari Bank Jatim</div>
            </div>
            <div class="box-body">
                <form enctype="multipart/form-data" method="post" id="data">
                    <div class="form-group">
                        <input type="file" name="file" id="file" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="hasil"></div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    $("form#data").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        Swal.fire({
            title: 'Proses',
            text: 'data sedang di proses',
            icon: 'info',
            showConfirmButton: false,
            allowOutsideClick: false,
            // timer: 3000,
        })

        $.ajax({
            url: "<?= base_url() . 'pelunasan/prosesupload' ?>",
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                // data = $.parseJSON(data);
                // alert(data);
                Swal.fire({
                    title: data.title,
                    text: 'telah di proses',
                    icon: data.type,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    timer: 1000,
                })

                $('#hasil').html(data.pesan);
            },
            error: function(res) {
                // data = $.parseJSON(res);
                Swal.fire({
                    title: 'Error',
                    text: 'error sistem',
                    icon: 'warning',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    timer: 2000,
                })
            },

        });
    });
</script>
@endsection