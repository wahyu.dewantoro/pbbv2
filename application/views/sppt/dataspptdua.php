<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data pembayaran</h3>
        <div class="pull-right">
            <?= anchor('sppt/cetakspptdua/' . $nop, '<i class="fa fa-print"></i> Cetak', 'class="btn btn-success btn-sm btn-success" target="_blank"') ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NOP</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>NJOP Bumi</th>
                        <th>NJOP Bangunan</th>
                        <th>Tahun</th>
                        <th>Pokok</th>
                        <th>Denda</th>
                        <th>Total</th>
                        <th>Lunas</th>
                        <th>Tanggal Bayar</th>
                    </tr>
                </thead>
                <?php $no = 1;
                foreach ($res as $res) {  ?>
                    <tr>
                        <td align="center"><?php echo $no; ?></td>
                        <td><?= $res->NOP ?></td>
                        <td><?= $res->NM_WP_SPPT ?></td>
                        <td><?= $res->ALAMAT ?></td>
                        <td align="right"><?= number_format($res->NJOP_BUMI_SPPT, '0', '', '.'); ?></td>
                        <td align="right"><?= number_format($res->NJOP_BNG_SPPT, '0', '', '.') ?></td>
                        <td align="center"><?= $res->THN_PAJAK_SPPT ?></td>
                        <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT, '0', '', '.') ?></td>
                        <td align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
                        <td align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT + $res->DENDA, '0', '', '.') ?></td>
                        <td align="center">
                            <?php if ($res->STATUS_PEMBAYARAN_SPPT == 1) {
                                echo "Lunas";
                            } else {
                                echo "Belum";
                            } ?></td>
                        <td align="center"><?= $res->TGL_BAYAR ?></td>
                    </tr>
                <?php $no++;
                } ?>
            </table>
        </div>
    </div>
</div>