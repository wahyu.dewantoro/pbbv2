@extends('page.master')
@section('judul')
<h1>
    SPPT Tanpa Potongan
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-cogs"></i> SPPT</a></li>

    <li class="active">SPPT Tanpa Potongan</li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Form Entri</h3>
            </div>
            <div class="box-body">
                <form action="{{ base_url('sppt/prosestanpapotongan') }}" method="post">
                    <div class="form-group">
                        <label>Tahun Pajak</label>
                        <input value="{{ date('Y') }}" type="text" name="THN_PAJAK_SPPT" id="THN_PAJAK_SPPT" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label>NOP</label>
                        <input type="text" onkeyup="formatnop(this)" autofocus="true" name="NOP" class="form-control" id="NOP" placeholder="" autofocus="true" required>
                    </div>
                    <button type="submit" class="btn btn-sm btn-success">Submit</button>


                </form>


            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-success">
            <div class="box-table">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="example3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NOP</th>
                                <th>Alamat</th>
                                <th>Tahun Pajak</th>
                                <th>Insert</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cv = 0;
                            $no = 1;
                            foreach ($data as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td><?=
                                            $rk->KD_PROPINSI . '.' .
                                                $rk->KD_DATI2 . '.' .
                                                $rk->KD_KECAMATAN . '.' .
                                                $rk->KD_KELURAHAN . '.' .
                                                $rk->KD_BLOK . '.' .
                                                $rk->NO_URUT . '.' .
                                                $rk->KD_JNS_OP
                                        ?><br><code>{{ $rk->NM_WP_SPPT }}</code> </td>
                                    <td>
                                        {{
                                            $rk->JLN_WP_SPPT.' '.
$rk->BLOK_KAV_NO_WP_SPPT.' ,'.
$rk->KELURAHAN_WP_SPPT }} <br>{{$rk->KOTA_WP_SPPT}}
                                    </td>
                                    <td><?= $rk->THN_PAJAK_SPPT ?></td>
                                    <td><?= $rk->TANGGAL ?></td>
                                    <td align="center">
                                        <a href="<?= base_url() . 'sppt/hapustanpapotongan?nop=' . urlencode($rk->KD_PROPINSI . '.' .
                                                        $rk->KD_DATI2 . '.' .
                                                        $rk->KD_KECAMATAN . '.' .
                                                        $rk->KD_KELURAHAN . '.' .
                                                        $rk->KD_BLOK . '.' .
                                                        $rk->NO_URUT . '.' .
                                                        $rk->KD_JNS_OP) . '&tahun=' . $rk->THN_PAJAK_SPPT ?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda yakin?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>

                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url('lte/') ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('lte/') ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {


        $('#example3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false
        });


    });

    function formatnop(objek) {

        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;

        if (panjang <= 2) {
            // 35 -> 0,2
            c = b;
        } else if (panjang > 2 && panjang <= 4) {
            // 07. -> 2,2
            c = b.substr(0, 2) + '.' + b.substr(2, 2);
        } else if (panjang > 4 && panjang <= 7) {
            // 123 -> 4,3
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
        } else if (panjang > 7 && panjang <= 10) {
            // .123. ->
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
        } else if (panjang > 10 && panjang <= 13) {
            // 123.
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
        } else if (panjang > 13 && panjang <= 17) {
            // 1234
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
        } else {
            // .0
            c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
            // alert(panjang);
        }
        objek.value = c;
    }
</script>
@endsection

@section('css')
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('lte/') ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection