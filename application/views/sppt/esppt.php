<html>

<head>
    <style>
        body {
            font-size: 8pt;
            line-height: 1.2;
        }

        p {
            font-size: 8pt;
            line-height: 1.2;
        }

        .satuempat {
            font-size: 14pt;
        }

        .satudua {
            font-size: 12pt;
        }



        .tengah {
            text-align: center;
        }



        table td,
        table td * {
            vertical-align: top;
            padding: 1px;
            font-size: 10pt;
            line-height: 1.5;
            spacn
        }

        table th,
        table th * {
            text-align: center;
            vertical-align: top;
            padding: 1px;
            font-size: 10pt;
            line-height: 1.5;
        }

        .border {
            border: 1px solid grey;
            border-spacing: 1px;
            border-collapse: collapse;
            /* border-collapse: separate; */
        }

        #watermark {
            position: fixed;
            top: 97mm;
            left: 10mm;
            width: 100%;
            height: 200px;
            opacity: 1;
        }
    </style>
    <title>E- SPPT</title>
</head>

<body>

    <!-- <p class="tengah">SURAT PEMBERITAHUAN PAJAK TERHUTANG <br>
        PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN TAHUN <?= $sppt->THN_PAJAK_SPPT ?>
    </p> -->
    <table width="100%">
        <tr>
            <th><img width="100px" height="100px" src="<?= base_url('kabmalang.png') ?>"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    JL. K.H. AGUS SALIM 7 TELEPON (0341)367994 FAX (0341)355708<br> MALANG - 65119
                </P>
            </th>
            <th><img width="100px" src="<?= base_url($qr) ?>"></th>
        </tr>
    </table>
    <hr style="margin: 5px;">
    <p class="tengah">SURAT PEMBERITAHUAN PAJAK TERHUTANG ELEKTRONIK (E-SPPT)<br>
        PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN TAHUN <?= $sppt->THN_PAJAK_SPPT ?></p>

    <table width=100% border=0>
        <tr>
            <td colspan="2">
                <p><b>NOP : <?= $sppt->KD_PROPINSI ?>.<?= $sppt->KD_DATI2 ?>.<?= $sppt->KD_KECAMATAN ?>.<?= $sppt->KD_KELURAHAN ?>.<?= $sppt->KD_BLOK ?>.<?= $sppt->NO_URUT ?>.<?= $sppt->KD_JNS_OP ?></b></p>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <p><b><u>Objek Pajak</u></b></p>
                <p><?= $sppt->ALAMAT_OP ?><br>
                    RT : <?= $sppt->RT_OP ?> RW : <?= $sppt->RW_OP ?><br>
                    <?= $sppt->KELURAHAN_OP ?><br>
                    <?= $sppt->KECAMATAN_OP ?>
                    KABUPATEN MALANG </p>
            </td>
            <td>
                <p> <b><u>Subjek Pajak </u></b></p>
                <p><?= $sppt->NM_WP_SPPT ?><br>
                    <?= $sppt->JLN_WP_SPPT ?> ,<?= $sppt->BLOK_KAV_NO_WP_SPPT ?><br>
                    RT : <?= $sppt->RT_WP_SPPT ?> RW : <?= $sppt->RW_WP_SPPT ?><br>
                    <?= $sppt->KELURAHAN_WP_SPPT ?><br>
                    <?= $sppt->KOTA_WP_SPPT ?></p>
            </td>
        </tr>
    </table>
    <table width="100%" border=1 class="border">
        <tr>
            <th>
                <p>OBJEK PAJAK</p>
            </th>
            <th>
                <p>LUAS (M<sup>2</sup>)</p>
            </th>
            <th>
                <p>KELAS</p>
            </th>
            <th>
                <p>NJOP PER M<sup>2</sup></p>
            </th>
            <th>
                <p>TOTAL NJOP (Rp.)</p>
            </th>
        </tr>
        <tr>
            <td width="25%">
                <p>BUMI<br>
                    BANGUNAN
                </p>
            </td>
            <td align="right">
                <p><?= angka($sppt->LUAS_BUMI_SPPT) ?><br>
                    <?= angka($sppt->LUAS_BNG_SPPT) ?>
                </p>
            </td>
            <td align="center" width="10%">
                <p>
                    <?= $sppt->KD_KLS_TANAH ?><br>
                    <?= $sppt->KD_KLS_BNG <> 'XXX' ? $sppt->KD_KLS_BNG : '' ?>
                </p>
            </td>
            <td align="right">
                <p><?= angka($sppt->NJOP_BUMI_SPPT / $sppt->LUAS_BUMI_SPPT) ?><br>
                    <?= $sppt->NJOP_BNG_SPPT <> 0 ? angka($sppt->NJOP_BNG_SPPT / $sppt->LUAS_BNG_SPPT) : '' ?>
                </p>
            </td>
            <td width="30%" align="right">
                <p><?= angka($sppt->NJOP_BUMI_SPPT) ?><br>
                    <?= $sppt->NJOP_BNG_SPPT <> 0 ? angka($sppt->NJOP_BNG_SPPT) : '' ?></p>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="40%">
                <p>NJOP sebagai dasar pengenaan PBB P2<br>
                    NJOPTKP (NJOP Tidak Kena Pajak)<br>
                    NJOP untuk penghitung PBB P2<br>
                    PBB P2 yang terhutang </p>
            </td>
            <td align="left" width="1%">
                <p>=<br>
                    =<br>
                    =<br>
                    =</p>
            </td>
            <td align="right" width="59%">
                <p><?= angka($sppt->NJOP_SPPT) ?><br>
                    <?= angka($sppt->NJOPTKP_SPPT) ?><br>
                    <?= angka($sppt->NJOP_SPPT) ?><br>
                    <?= '0.1% x ' . angka($sppt->NJOP_SPPT) . '=' . angka($sppt->PBB_TERHUTANG_SPPT) ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <p>PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN TAHUN <?= $sppt->THN_PAJAK_SPPT ?> YANG HARUS DI BAYAR <b>Rp. <?= angka($sppt->PBB_YG_HARUS_DIBAYAR_SPPT)  ?></b><br>
                    <i><?= strtoupper(penyebut($sppt->PBB_YG_HARUS_DIBAYAR_SPPT)) ?></i></p>
            </td>
        </tr>
    </table>
    <table width="100%" class="border">
        <tr>
            <td width="50%" class="border">
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <p>Tgl Jatuh Tempo</p>
                        </td>
                        <td>
                            <p>: <?= date_indo($sppt->JATUH_TEMPO) ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Tempat Pembayaran</p>
                        </td>
                        <td>
                            <p>: <?= $tb ?></p>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <?php foreach ($lb as $rr) { ?>
                            <td><img src="<?= base_url() . $rr->LOGO ?>" width="100px" width="70px"></td>
                        <?php } ?>
                    </tr>
                </table>

            </td>
            <td width="50%" class="border" style="vertical-align: middle;">
                <p><b>MALANG, <?= date_indo($sppt->TGL_TERBIT_SPPT) ?></b><br>
                    Dokumen ini telah di tandatangani secara elektronik oleh :<br>
                    <?php if ($plt == true) {
                        echo "Plt ";
                    } ?> KEPALA
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    <b><?= $ttd->NM_PEGAWAI ?></b></p>
            </td>
        </tr>
    </table>
    <p class="tengah"><b>Imformasi Pembayaran PBB-P2</b></p>
    <table class="border"  width=100% >
        
            <tr>
                <th class="border" >NO</th>
                <th class="border" >Tahun</th>
                <th class="border" >Pokok</th>
                <th class="border" >Denda</th>
                <th class="border" >Total</th>
                <th class="border" >Lunas</th>
            </tr>
        
        <?php $no = 1;
        foreach ($riwayat as $res) {  ?>
            <tr>
                <td class="border"  align="center"><?php echo $no; ?></td>
                <td class="border"  align="center"><?= $res->THN_PAJAK_SPPT ?></td>
                <td class="border"  align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT, '0', '', '.') ?></td>
                <td class="border"  align="right"><?= number_format($res->DENDA, '0', '', '.') ?></td>
                <td class="border"  align="right"><?= number_format($res->PBB_YG_HARUS_DIBAYAR_SPPT + $res->DENDA, '0', '', '.') ?></td>
                <td class="border"  align="center"><?php if ($res->STATUS_PEMBAYARAN_SPPT == 1) {
                                        echo "Lunas";
                                    } else {
                                        echo "Belum";
                                    } ?></td>
            </tr>
        <?php $no++;
        } ?>
    </table>
</body>

</html>