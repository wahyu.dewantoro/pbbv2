<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['usertp'] = 'Pengguna/usertpctrl';
$route['usertp/create'] = 'Pengguna/usertpctrlcreate';
$route['userupt'] = 'Pengguna/useruptctrl';
$route['userupt/create'] = 'Pengguna/useruptctrlcreate';
$route['e-sppt'] = 'sppt/espptdua';
$route['e-sppt-ntpd'] = 'dafnom/esppt_ntpd';
$route['tandaterima/(:any)'] = 'permohonan/cetakurl/$1';
