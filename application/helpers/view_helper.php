<?php

use Jenssegers\Blade\Blade;

if (!function_exists('view')) {
  function view($view, $data = [])
  {
    $path = APPPATH . 'views';
    $blade = new Blade($path, $path . '/cache');
    echo $blade->make($view, $data);
  }
}

if (!function_exists('inisial')) {
  function inisial($string)
  {
    $var = trim($string);

    $exp = explode(' ', $var);

    $a = '';
    foreach ($exp as $exp) {

      $a .= substr($exp, 0, 1);
    }

    return $a;
  }
}


if (!function_exists('terbilang')) {
  function terbilang($angka)
  {
    $angka = (float) $angka;
    $bilangan = array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan', 'Sepuluh', 'Sebelas');
    if ($angka < 12) {
      return $bilangan[$angka];
    } else if ($angka < 20) {
      return $bilangan[$angka - 10] . ' Belas';
    } else if ($angka < 100) {
      $hasil_bagi = (int) ($angka / 10);
      $hasil_mod = $angka % 10;
      return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
    } else if ($angka < 200) {
      return sprintf('Seratus %s', terbilang($angka - 100));
    } else if ($angka < 1000) {
      $hasil_bagi = (int) ($angka / 100);
      $hasil_mod = $angka % 100;
      return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
    } else if ($angka < 2000) {
      return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
    } else if ($angka < 1000000) {
      $hasil_bagi = (int) ($angka / 1000);
      $hasil_mod = $angka % 1000;
      return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
    } else if ($angka < 1000000000) {
      $hasil_bagi = (int) ($angka / 1000000);
      $hasil_mod = $angka % 1000000;
      return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000) {
      $hasil_bagi = (int) ($angka / 1000000000);
      $hasil_mod = fmod($angka, 1000000000);
      return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000000) {
      $hasil_bagi = $angka / 1000000000000;
      $hasil_mod = fmod($angka, 1000000000000);
      return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else {
      return 'Data Salah';
    }
  }
}

if (!function_exists('formatnop')) {
  function formatnop($angkab)
  {
    // return $angkab;
    $angkaa = $angkab;

    $c = "";
    $panjang = strlen($angkaa);

    if ($panjang <= 2) {
      $c = $angkaa;
    } else if ($panjang > 2 && $panjang <= 4) {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2);
    } else if ($panjang > 4 && $panjang <= 7) {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3);
    } else if ($panjang > 7 && $panjang <= 10) {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3);
    } else if ($panjang > 1 && $panjang <= 14) {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3);
    } else if ($panjang > 14 && $panjang <= 17) {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3) . '.' . substr($angkaa, 13, 4);
    } else {
      $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3) . '.' . substr($angkaa, 13, 4) . '.' . substr($angkaa, 17, 1);
    }


    return $c;
  }
}

function dd($var)
{
  echo "<pre>";
  print_r($var);
  echo "</pre>";
  die();
}

function getFlashdata($var)
{
  $ci = get_instance();
  return $ci->session->flashdata($var);
}

function angka($nomnial)
{
  return number_format($nomnial, 0, '', '.');
}

function penyebut($nilai)
{
  $nilai = abs($nilai);
  $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
  if ($nilai < 12) {
    $temp = " " . $huruf[$nilai];
  } else if ($nilai < 20) {
    $temp = penyebut($nilai - 10) . " belas";
  } else if ($nilai < 100) {
    $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
  } else if ($nilai < 200) {
    $temp = " seratus" . penyebut($nilai - 100);
  } else if ($nilai < 1000) {
    $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
  } else if ($nilai < 2000) {
    $temp = " seribu" . penyebut($nilai - 1000);
  } else if ($nilai < 1000000) {
    $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
  } else if ($nilai < 1000000000) {
    $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
  } else if ($nilai < 1000000000000) {
    $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
  } else if ($nilai < 1000000000000000) {
    $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
  }
  return $temp;
}
function penyebut_cap($nilai)
{
  $nilai = abs($nilai);
  $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  $temp = "";
  if ($nilai < 12) {
    $temp = " " . $huruf[$nilai];
  } else if ($nilai < 20) {
    $temp = penyebut_cap($nilai - 10) . " Belas";
  } else if ($nilai < 100) {
    $temp = penyebut_cap($nilai / 10) . " Puluh" . penyebut_cap($nilai % 10);
  } else if ($nilai < 200) {
    $temp = " Seratus" . penyebut_cap($nilai - 100);
  } else if ($nilai < 1000) {
    $temp = penyebut_cap($nilai / 100) . " Ratus" . penyebut_cap($nilai % 100);
  } else if ($nilai < 2000) {
    $temp = " Seribu" . penyebut_cap($nilai - 1000);
  } else if ($nilai < 1000000) {
    $temp = penyebut_cap($nilai / 1000) . " Ribu" . penyebut_cap($nilai % 1000);
  } else if ($nilai < 1000000000) {
    $temp = penyebut_cap($nilai / 1000000) . " Juta" . penyebut_cap($nilai % 1000000);
  } else if ($nilai < 1000000000000) {
    $temp = penyebut_cap($nilai / 1000000000) . " Milyar" . penyebut_cap(fmod($nilai, 1000000000));
  } else if ($nilai < 1000000000000000) {
    $temp = penyebut_cap($nilai / 1000000000000) . " Trilyun" . penyebut_cap(fmod($nilai, 1000000000000));
  }
  return $temp;
}
if (!function_exists('date_indo')) {
  function date_indo($tgl)
  {
    if (!empty($tgl)) {
      $tgl=date('Y-m-d',strtotime($tgl));

      $ubah    = gmdate($tgl, time() + 60 * 60 * 8);
      $pecah   = explode("-", $ubah);
      $tanggal = $pecah[2];
      $bulan   = bulan($pecah[1]);
      $tahun   = $pecah[0];
      return $tanggal . ' ' . $bulan . ' ' . $tahun;
    } else {
      return null;
    }
  }
}

if (!function_exists('bulan')) {
  function bulan($bln)
  {
    switch ($bln) {
      case 1:
        return "Januari";
        break;
      case 2:
        return "Februari";
        break;
      case 3:
        return "Maret";
        break;
      case 4:
        return "April";
        break;
      case 5:
        return "Mei";
        break;
      case 6:
        return "Juni";
        break;
      case 7:
        return "Juli";
        break;
      case 8:
        return "Agustus";
        break;
      case 9:
        return "September";
        break;
      case 10:
        return "Oktober";
        break;
      case 11:
        return "November";
        break;
      case 12:
        return "Desember";
        break;
    }
  }
}

//Format Shortdate
if (!function_exists('shortdate_indo')) {
  function shortdate_indo($tgl)
  {
    $ubah = gmdate($tgl, time() + 60 * 60 * 8);
    $pecah = explode("-", $ubah);
    $tanggal = $pecah[2];
    $bulan = short_bulan($pecah[1]);
    $tahun = $pecah[0];
    return $tanggal . '/' . $bulan . '/' . $tahun;
  }
}

if (!function_exists('short_bulan')) {
  function short_bulan($bln)
  {
    switch ($bln) {
      case 1:
        return "01";
        break;
      case 2:
        return "02";
        break;
      case 3:
        return "03";
        break;
      case 4:
        return "04";
        break;
      case 5:
        return "05";
        break;
      case 6:
        return "06";
        break;
      case 7:
        return "07";
        break;
      case 8:
        return "08";
        break;
      case 9:
        return "09";
        break;
      case 10:
        return "10";
        break;
      case 11:
        return "11";
        break;
      case 12:
        return "12";
        break;
    }
  }
}

//Format Medium date
if (!function_exists('mediumdate_indo')) {
  function mediumdate_indo($tgl)
  {
    $ubah = gmdate($tgl, time() + 60 * 60 * 8);
    $pecah = explode("-", $ubah);
    $tanggal = $pecah[2];
    $bulan = medium_bulan($pecah[1]);
    $tahun = $pecah[0];
    return $tanggal . '-' . $bulan . '-' . $tahun;
  }
}

if (!function_exists('medium_bulan')) {
  function medium_bulan($bln)
  {
    switch ($bln) {
      case 1:
        return "Jan";
        break;
      case 2:
        return "Feb";
        break;
      case 3:
        return "Mar";
        break;
      case 4:
        return "Apr";
        break;
      case 5:
        return "Mei";
        break;
      case 6:
        return "Jun";
        break;
      case 7:
        return "Jul";
        break;
      case 8:
        return "Ags";
        break;
      case 9:
        return "Sep";
        break;
      case 10:
        return "Okt";
        break;
      case 11:
        return "Nov";
        break;
      case 12:
        return "Des";
        break;
    }
  }
}

//Long date indo Format
if (!function_exists('longdate_indo')) {
  function longdate_indo($tanggal)
  {
    $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
    $pecah = explode("-", $ubah);
    $tgl = $pecah[2];
    $bln = $pecah[1];
    $thn = $pecah[0];
    $bulan = bulan($pecah[1]);

    $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
    $nama_hari = "";
    if ($nama == "Sunday") {
      $nama_hari = "Minggu";
    } else if ($nama == "Monday") {
      $nama_hari = "Senin";
    } else if ($nama == "Tuesday") {
      $nama_hari = "Selasa";
    } else if ($nama == "Wednesday") {
      $nama_hari = "Rabu";
    } else if ($nama == "Thursday") {
      $nama_hari = "Kamis";
    } else if ($nama == "Friday") {
      $nama_hari = "Jumat";
    } else if ($nama == "Saturday") {
      $nama_hari = "Sabtu";
    }
    return $nama_hari . ',' . $tgl . ' ' . $bulan . ' ' . $thn;
  }
}


function encrypt_url($string)
{

  return $string;
  die();
  $output = false;
  /*
  * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
  */
  $security       = parse_ini_file("security.ini");
  $secret_key     = $security["encryption_key"];
  $secret_iv      = $security["iv"];
  $encrypt_method = $security["encryption_mechanism"];

  // hash
  $key    = hash("sha256", $secret_key);

  // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
  $iv     = substr(hash("sha256", $secret_iv), 0, 16);

  //do the encryption given text/string/number
  $result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
  $output = base64_encode($result);
  return $output;
}



function decrypt_url($string)
{

  $output = false;
  /*
  * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
  */

  $security       = parse_ini_file("security.ini");
  $secret_key     = $security["encryption_key"];
  $secret_iv      = $security["iv"];
  $encrypt_method = $security["encryption_mechanism"];

  // hash
  $key    = hash("sha256", $secret_key);

  // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
  $iv = substr(hash("sha256", $secret_iv), 0, 16);

  //do the decryption given text/string/number

  $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
  return $output;
}


function formatnopel($id)
{
  // 2020.0022.032
  // $id=strlen($id);
  if (strlen($id) == 11) {
    $vv[] = substr($id, 0, 4);
    $vv[] = substr($id, 4, 4);
    $vv[] = substr($id, 8, 4);
    $vv = implode('.', $vv);
    return $vv;
  } else {
    return $id;
  }
}


function splitNopelNop($number)
{

  $nopel = substr($number, 0, 11);
  $nop = substr($number, 11, 18);
  $pecah = [
    'all' => $number,
    'thn_pelayanan' => substr($nopel, 0, 4),
    'bundel_pelayanan' => substr($nopel, 4, 4),
    'no_urut_pelayanan' => substr($nopel, 8, 3),
    'kd_propinsi' => substr($nop, 0, 2),
    'kd_dati2' => substr($nop, 2, 2),
    'kd_kecamatan' => substr($nop, 4, 3),
    'kd_kelurahan' => substr($nop, 7, 3),
    'kd_blok' => substr($nop, 10, 3),
    'no_urut' => substr($nop, 13, 4),
    'kd_jns_op' => substr($nop, 17, 1)
  ];
  return $pecah;
}

function splitNop($nop)
{
  $res = [
    'kd_propinsi' => substr($nop, 0, 2),
    'kd_dati2' => substr($nop, 2, 2),
    'kd_kecamatan' => substr($nop, 4, 3),
    'kd_kelurahan' => substr($nop, 7, 3),
    'kd_blok' => substr($nop, 10, 3),
    'no_urut' => substr($nop, 13, 4),
    'kd_jns_op' => substr($nop, 17, 1)
  ];
  return $res;
}


function JoinTable($table, $where = array(), $select = null)
{
  if (empty($where)) {
    return 'Whee clause kosong';
  }

  $CI = &get_instance();
  $CI->load->library('session', 'database');

  foreach ($where as $key => $wh) {
    $CI->db->where(strtoupper($key), $wh);
  }

  if ($select != null) {
    $CI->db->select($select, false);
  }


  return $CI->db->get(strtoupper($table), false);
}
